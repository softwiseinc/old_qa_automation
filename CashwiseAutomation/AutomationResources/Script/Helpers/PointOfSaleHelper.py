﻿from BasePage import *
from CashHelper import *
from SystemSetupHelper import *
from uuid import uuid4
#import CloseCashDrawer
#import EditTypeILC
#import EnableAllowClosedDueDay
#import OpenSystemSetupOption
#import RollForward

    
def SetTitleInformation():
  #Author Pat Holman 12/23/2020
  #Last Modified by Pat Holman 12/23/2020
  Indicator.PushText("Set Car Title details")
  cashwise = Sys.Process("Cashwise")
  tfrmDeferredCollateralTitleEdit = cashwise.frmDeferredCollateralTitleEdit
  pageControl = tfrmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1
  pageControl.ClickTab("Title Details")
  TitleDetails = pageControl.tsTitleDetails
  Make = TitleDetails.edMake
  Make.Click(86, 8)
  Make.SetText("Mercedes-Benz")
  Make.Keys("[Tab]")
  Model = TitleDetails.edModel
  Model.SetText("AMG GT")
  Model.Keys("[Tab]")
  Vin = TitleDetails.edVIN
  Vin.Click(70, 9)
  Vin.SetText("WDDYJ7HA6LA028492")
  HiValue = TitleDetails.edHiValue
  HiValue.Click(10, 16)
  HiValue.SetText("111987")
  LowValue = TitleDetails.edLowValue
  LowValue.Click(23, 16)
  LowValue.SetText("100000")
  TradeValue = TitleDetails.edTradeValue
  TradeValue.Click(48, 9)
  TradeValue.SetText("97000")
  TradeValue.Keys("[Tab]")
  ModelYear = TitleDetails.edYear
  ModelYear.Click(48, 12)
  ModelYear.SetText("2020")
  TitleDetails.edEvaluationDate.TAdvancedSpeedButton.Click(11, 5)
  CalendarEdit = cashwise.frmCalendarEdit
  Calendar = CalendarEdit.pnlMain.Calendar
  Calendar.Click(281, 33)
  advancedSpeedButton = CalendarEdit.btnSave
  advancedSpeedButton.Click(27, 10)
  PolicyNumber = TitleDetails.edPolicyNumber
  PolicyNumber.Click(61, 9)
  PolicyNumber.SetText("356M59557")
  PolicyNumber.Keys("[Tab]")
  Color = TitleDetails.edColor
  Color.SetText("grey")
  Color.Keys("[Tab]")
  Odometer = TitleDetails.edOdometer
  Odometer.SetText("11000")
  Odometer.Keys("[Tab]")
  TitleDetails.edLicenseNumber.SetText("JAN")
  TitleDetails.edLicenseExpirationDate.TAdvancedSpeedButton.Click(8, 9)
  CalendarEdit.Panel1.btnNextYear.Click(19, 21)
  Calendar.Click(233, 34)
  ButtonSave = cashwise.frmCalendarEdit.btnSave
  ButtonSave.Click(66, 8)
  SpeedButton = cashwise.frmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1.tsTitleDetails.edCondition.TAdvancedSpeedButton
  SpeedButton.Click(8, 11)
  ButtonSave = cashwise.frmCollateralConditionBrowse.btnSave
  ButtonSave.Click(59, 12)
  Style = TitleDetails.VCLObject("edStyle").Window("TCustomDBEdit")
  Style.Click(25, 3)
  Style.Keys("Coupe")
  Description = TitleDetails.edDescription
  Description.Click(44, 11)
  Description.SetText("Grey with black rims")
  Notes = TitleDetails.mNotes
  Notes.Click(70, 30)
  Notes.Keys("Wonderful Ride")
  State = TitleDetails.edState
  State.Click(78, 12)
  State.SetText("UT")
  ButtonSave = tfrmDeferredCollateralTitleEdit.btnSave
  ButtonSave.Click(60, 9)
  Log.Checkpoint("SUCCESS: Set Care Title details")
  Indicator.PopText()
    
def SetAutoPayACH():
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set Auto pay ACH")
  DeferredTypeAutoPayType = cashwise.FrmDeferredTypeAutoPayType
  DeferredTypeAutoPayType.Click(273, 17)
  DeferredTypeAutoPayType.pnlMain.dbgMain.Keys("[Down]")
  ButtonSave = DeferredTypeAutoPayType.btnSave
  ButtonSave.Click(49, 3)
  ExplicitWait(1)
  ButtonSave = cashwise.frmContactAccountList.btnSave
  ButtonSave.Click(30, 7)
  Log.Checkpoint("SUCCESS: Set Auto pay ACH")
  Indicator.PopText()

def SetTLTLoanAmount(loanAmount):  
  #Author Pat Holman 12/28/2020
  #Last Modified by Pat Holman 12/28/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set Title Loan principal amount")
  PaymentPanel = cashwise.VCLObject("frmDeferredNewLoanEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlInstallmentLoan").VCLObject("pnlPaymentInformation").VCLObject("pnlStandardPattern")
  LoanPrincipal = PaymentPanel.VCLObject("dbedLoanPrincipal")
  LoanPrincipal.Click(38, 13)
  LoanPrincipal.Keys("565")
  LoanPrincipal.Keys("[Tab]")
  BankSpeedButton = PaymentPanel.VCLObject("edRoutAcct").VCLObject("TAdvancedSpeedButton")
  BankSpeedButton.Click(9, 8)
  SaveBank = cashwise.VCLObject("frmContactAccountList").VCLObject("pnlMain").VCLObject("dbgMain")
  SaveBank.Keys("[F9]")
  LoanPrincipal.Keys("[F9]")
  Log.Checkpoint("SUCCESS: Title Loan principal amount")
  Indicator.PopText()
  
def SetPASLoanAmount(loanAmount):  
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set PAS loan amount")
  PaymentPanel = cashwise.VCLObject("frmDeferredNewLoanEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlInstallmentLoan").VCLObject("pnlPaymentInformation").VCLObject("pnlStandardPattern")
  LoanPrincipal = PaymentPanel.VCLObject("dbedLoanPrincipal")
  LoanPrincipal.Keys(loanAmount)
  LoanPrincipal.Keys("[Tab]")
  BankSpeedButton = PaymentPanel.VCLObject("edRoutAcct").VCLObject("TAdvancedSpeedButton")
  BankSpeedButton.Click(8, 4)
  SaveBank = cashwise.VCLObject("frmContactAccountList").VCLObject("btnSave")
  SaveBank.Click(39, 6)
  LoanPrincipal.Keys("[F9]")
  Log.Checkpoint("SUCCESS: Set PAS loan amount")
  Indicator.PopText()
        
def SetPABLoanAmount(loanAmount):  
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set PAB loan amount")
  LoanPrincipal = cashwise.frmDeferredNewLoanEdit.pnlMain.sbData.pnlInstallmentLoan.pnlPaymentInformation.pnlStandardPattern.dbedLoanPrincipal
  ExplicitWait(2)
  LoanPrincipal.Keys(loanAmount)
  LoanPrincipal.Keys("[Tab]")
  LoanPrincipal.Keys("[F9]")
  Log.Checkpoint("SUCCESS: Set PAB loan amount")
  Indicator.PopText()
  
def SetD89BankCollateral():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set PAB Bank Collateral")
  collateralButton = Cashwise.frmDeferredNewEdit.btnCollateral
  collateralButton.Click()
  collateralListAddButton = Cashwise.frmDeferredCollateralList.btnAdd
  collateralListAddButton.Click()
  Cashwise.frmDeferredTypeCollateralTypeList.pnlMain.dbgMain.Keys("[PageDown]")
  collateralTypeSaveButton = Cashwise.frmDeferredTypeCollateralTypeList.btnSave
  collateralTypeSaveButton.Click()
  storedBankCardButton = Cashwise.frmDeferredCollateralCreditCardEdit.pnlMain.sbData.pnlCardData.edBankCardLookup.TAdvancedSpeedButton
  storedBankCardButton.Click()
  storedBankCardSaveButton = Cashwise.frmThirdPartyCardList.btnSave
  storedBankCardSaveButton.Click()
  bankCardEditSaveButton = Cashwise.frmDeferredCollateralCreditCardEdit.btnSave
  bankCardEditSaveButton.Click()
  collateralListSaveButton = Cashwise.frmDeferredCollateralList.btnSave
  collateralListSaveButton.Click()
  Log.Checkpoint("SUCCESS: Set PAB Bank Collateral")
  Indicator.PopText()
  

def SelectBankAccount():
  #Author Pat Holman 12/21/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Select Bank Account")
  BankAccountSelectButton = cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton
  BankAccountSelectButton.Click(11, 4)
  BankAccountSaveButton = cashwise.frmContactAccountList.btnSave
  BankAccountSaveButton.Click(56, 11)
  Log.Checkpoint("SUCCESS: Select Bank Account")
  Indicator.PopText()           
  
def ClosePOS():
  #Author Pat Holman 12/21/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Close Point of Sale")
  SaleCloseButton = cashwise.frmSaleEdit.btnClose
  SaleCloseButton.Click(49, 13)
  Log.Checkpoint("SUCCESS: Close Point of Sale")
  Indicator.PopText()   

def ProcessSale():
  #Author Pat Holman 12/21/2020
  #Last Modified by Adam Gibbons 04/21/2021
  #make DeferredValidataionOverride conditional
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Process Sale")
  #DeferredValidationOverride = cashwise.VCLObject("frmSecurityOverride").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edPassword")
  #DeferredValidationOverride.Keys("own")
  #cashwise.VCLObject("frmSecurityOverride").VCLObject("btnSave").Click()
  ProcessSale = cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlProcessContainer.btnProcess
  ProcessSale.Click(35, 30)
  PayoutType = cashwise.frmAliasBrowse
  PayoutType.pnlMain.sbData.lbAliases.ClickItem("Cash")
  PayoutType.btnSave.Click(43, 8)
  cashwise.frmSaleConfirmPayout.btnSave.Click(61, 12)
  Log.Checkpoint("SUCCESS: Process Sale")
  Indicator.PopText()  
      
def CollectCheck(customer):
  #Author Pat Holman 12/21/2020
  #Last Modified by Adam Gibbons 04/21/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Collect Check from Customer")
  bit_size = 1
  checkNumber = str(uuid4().int >> bit_size)
  checkNumber = checkNumber[35:] 
  CheckNumberField = cashwise.frmDeferredCollateralCheckEdit
  CheckNumberFieldEdit = CheckNumberField.pnlMain.sbData.pnlCheckInfo.edNum
  CheckNumberFieldEdit.Click(45, 10)
  CheckNumberFieldEdit.SetText(checkNumber)
  SaveCollateralCheckButton = cashwise.frmDeferredCollateralCheckEdit.btnSave
  SaveCollateralCheckButton.Click(52, 12)
  ExplicitWait(1)
  #ButtonOk = cashwise.Window("TMessageForm", "Confirm").VCLObject("OK")
  #ButtonOk.ClickButton()
  Log.Checkpoint("SUCCESS: Collect Check from Customer")
  Indicator.PopText()  


def SetCashAdvanceLoanAmount(loanAmount):
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set Cash Advance Loan Amount: " + str(loanAmount))
  LoanAmountEdit = cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlAmounts.pnlBaseFields.edAdvance
  LoanAmountEdit.Keys(loanAmount)
  LoanAmountEdit.Keys("[Tab]")  
  Log.Checkpoint("Set Cash Advance Loan Amount: " + str(loanAmount))
  Indicator.PopText()  
  
def SaveCloseCashAdvancePanel():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Save and Close Cash Advance Panel")
  SaveButton = cashwise.frmDeferredNewEdit.btnSave
  SaveButton.Click()
  Log.Checkpoint("SUCCESS: Save and Close Cash Advance Panel")
  Indicator.PopText()
  
def SelectLoanType(loanType):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("Select Loan Type: " + loanType)
  cashwise = Sys.Process("Cashwise")
  cashwise.frmDeferredSelect.btnDeferredAdd.Click(18, 21)
  LoanTypeSelection = cashwise.frmLoanTypeSelection
  LoanTypeSelection.pnlMain.tvMain.ClickItem(loanType)
  LoanTypeSelection.btnSave.Click(45, 7)     
  Log.Checkpoint("SUCCESS: Select Loan Type: " + loanType)
  Indicator.PopText()
  
def SkipLoanWarnings():
  #Author Brian Johnson 1/7/2021
  #Last Modified by Brian Johnson 1/7/2021
  #these should be conditional
  Indicator.PushText("Skipping Loan Warnings")  
  cashwise = Sys.Process("Cashwise")
  if(cashwise.VCLObject("frmSecurityOverride").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edPassword").Exists == True):
    DeferredValidationOverride = cashwise.VCLObject("frmSecurityOverride").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edPassword")
    DeferredValidationOverride.Keys("own")
    cashwise.VCLObject("frmSecurityOverride").VCLObject("btnSave").Click()
    ExplicitWait(1)
  else:
    ExplicitWait(1)
  if(cashwise.Window("TMessageForm", "Confirm", 1).Exists == True):
    FirstTimeCustomerWarning = cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK")
    FirstTimeCustomerWarning.Click()
    ExplicitWait(1)
  else:
    ExplicitWait(1)
  if(cashwise.Window("TMessageForm", "Information", 1).Exists == True):
    Scenario1BWarning = cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK")
    Scenario1BWarning.Click()
    ExplicitWait(1)
  else:
    ExplicitWait(1)
  if(cashwise.waitWindow("TMessageForm", "Information", 1).Exists == True):
    cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK").Click()
  else:
    ExplicitWait(1)
  Log.Checkpoint("SUCCESS: Skipping Loan Warnings")
  Indicator.PopText()
  
def SetDueDate(days):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/18/2020
  Indicator.PushText("Select Due Date for Loan")  
  cashwise = Sys.Process("Cashwise")
  cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlLeft.pnlDetail.edDepositDate.TAdvancedSpeedButton.Click(8, 9)
  CalendarEdit = cashwise.frmCalendarEdit
  AdvancedButton = CalendarEdit.Panel1.btnNextDay
  for i in range(0, days):
    AdvancedButton.Click(9, 18)  
  CalendarEdit.btnSave.Click(60, 9)  
  Log.Checkpoint("SUCCESS: Select Due Date for Loan")
  Indicator.PopText()
   
def CashAdvanceLoanAmount(loanAmount):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Enter Loan Amount")
  DeferredNewEdit = cashwise.frmDeferredNewEdit.dbtAdvance
  DeferredNewEdit.Click(38, 6)
  LoanAmountEdit = DeferredNewEdit.pnlMain.sbData.pnlAmounts.pnlBaseFields.edAdvance
  LoanAmountEdit.SetText(loanAmount)
  LoanAmountEdit.Keys("[Tab]")
  Log.Checkpoint("SUCCESS: Enter Loan Amount")
  Indicator.PopText()
        
def ClickCashAdvanceButton():
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Click 'Cash Advance Button'")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    CashAdvanceButton = cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Window("TAdvancedButton", "&1 Cash Advance", 10)
  else:
    CashAdvanceButton = cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Window("TAdvancedButton", "&1 Cash Advance")
  CashAdvanceButton.click()
  Log.Checkpoint("SUCCESS: Click 'Cash Advance Button' is successful.")
  Indicator.PopText()
    
def CloseSalesTrasaction():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Close Sales Trasaction")
  cashwise.Find("ObjectIdentifier", "btnClose",10).Click()
  Log.Checkpoint("SUCCESS: Close Sales Trasaction is successful.")
  Indicator.PopText()

def OpenPOSForTheDayClosePOS():
   #Author Phil 8/3/2019
   #Last Modified by Phil 8/3/2019 
    cashwise = Sys.Process("Cashwise")
    cashwise.frmMain.TGraphicButton.Click()
    if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
    if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
      cashwise.frmCashOpenConfirm.Button1.Click()
    cashwise.frmSaleEdit.btnClose.Click()
     
    
def CashwiseMainMenu_Sales(action):
   #Author Phil 8/3/2019
   #Last Modified by Phil 8/3/2019 
    cashwise = Sys.Process("Cashwise")
    cashwiseMainMenu = cashwise.frmMain.MainMenu
    #action = "Line of Credit"
    if(action == "Cash Advance Manager"):
            cashwiseMainMenu.Click("Sales|Cash Advance|Cash Advance Manager...")
    elif(action == "Line of Credit Manager"):
        cashwiseMainMenu.Click("Sales|Line of Credit|Line of Credit Manager...")   
    Log.Message("Sales "+action+" open")
    
def CashwiseMainMenu_Collections(action):
   #Author Phil 8/3/2019
   #Last Modified by Phil 8/3/2019 
    cashwise = Sys.Process("Cashwise")
    cashwiseMainMenu = cashwise.frmMain.MainMenu
    action = "Line of Credit Debt"
    if(action == "Cash Advance Manager"):
            cashwiseMainMenu.Click("Sales|Cash Advance|Cash Advance Manager...")

    elif(action == "Line of Credit Debt"):
        cashwiseMainMenu.Click("Collections|New Debt|Line of Credit Debt...")
        
    
    Log.Message("Sales "+action+" open")

  
def CashwiseMainMenu_Window(action):
 cashwise = Sys.Process("Cashwise"); 
 cashwiseMain =  cashwise.frmMain.MainMenu;   
 if(action == "Top"):
        cashwise.frmMain.Keys("~W1")
        
def SetBankCardCollateralSinglePayment():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Set Bank Card Collateral")
  cashwise.VCLObject("frmDeferredNewEdit").VCLObject("btnCollateral").Click()
  cashwise.VCLObject("frmDeferredCollateralList").VCLObject("btnAdd").Click()
