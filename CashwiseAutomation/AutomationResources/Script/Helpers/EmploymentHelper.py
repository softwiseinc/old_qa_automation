﻿from BasePage import * 
from CustomerHelper import *
 
def FindCustomerById(customerId):
  cashwise = Sys.Process("Cashwise")
  tfrmCustomerBrowse = Aliases.Cashwise.frmCustomerBrowse
  #tfrmCustomerBrowse.Click(157, 14)
  incrementalSearch = tfrmCustomerBrowse.pnlSearch.isMain
  #incrementalSearch.Click(76, 8)
  incrementalSearch.Keys(customerId)
  ExplicitWait(3)

def OpenEmployerContact():
  #Author Pat Holman 5/24/2019
  #Last Modified by Brian Johnson 1/8/2021  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenEmployerContact()")
  mainMenu = cashwise.frmCustomerEdit.MainMenu
  mainMenu.Click("[2]|[14]")
  Log.Checkpoint("SUCCESS: OpenEmployerContact() is successful.")
  Indicator.PopText()

def UpdateEmploymentBiWeekly():# on customer edit screen known bi-weekly pay period
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerEdit.btnEmployers.Click()
  cashwise.frmContactEmployerList.btnEdit.Click()
  cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect.Click()
  cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateTwo.ClickButton()
  nextPayDate = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.dlbledPayDate1.wText
  cashwise.frmContactBiweeklyEdit.btnSave.Click()
  cashwise.frmContactEmployerEdit.btnSave.Click()
  cashwise.frmContactEmployerList.btnSave.Click()
  return nextPayDate
  
def OpenAddEmployerContact():
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenAddEmployerContact()")
  tfrmContactEmployerList = cashwise.frmContactEmployerList
  tfrmContactEmployerList.pnlMain.dbgMain.SetFocus()
  tfrmContactEmployerList.pnlMain.dbgMain.Keys("[F12]")
  Log.Checkpoint("SUCCESS: OpenAddEmployerContact() is successful.")
  Indicator.PopText()

def SetTypeOfIncome():
  #Author Brian Johnson 1/8/2021
  #Last Modified by Brian Johnson 1/8/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SetTypeOfIncome")    
  TypeOfIncomeButton = cashwise.VCLObject("frmContactEmployerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("dblefEmploymentTypeID").VCLObject("TAdvancedSpeedButton")
  TypeOfIncomeButton.Click()
  TypeOfIncomeField = cashwise.VCLObject("frmEmploymentTypeBrowse").VCLObject("pnlSearch").VCLObject("isMain")
  TypeOfIncomeField.Keys("std")
  cashwise.VCLObject("frmEmploymentTypeBrowse").VCLObject("btnSave").Click()
  Log.Checkpoint("SUCCESS: SetTypeOfIncome is successful.")
  Indicator.PopText()
  
def SelectEmployerName(customer):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectEmployerName(customer)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.edEmployer.TAdvancedSpeedButton.Click(10, 7)
  cashwise.frmEmployerBrowse.pnlSearch.isMain.Keys(customer["EmployerName"])
  ExplicitWait(3)
  cashwise.frmEmployerBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectEmployerName(customer) is successful.")
  Indicator.PopText()

def SelectStartDate(years):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectStartDate(years)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedStartDate.TAdvancedSpeedButton.Click(8, 6)
  advancedButton = cashwise.frmCalendarEdit.Panel1.btnPriorYear
  for x in range(years):
    advancedButton.Click(14, 17)
    ExplicitWait(1)
  advancedButton.Keys("[F9]")
  
    
  
  Log.Checkpoint("SUCCESS: SelectStartDate(years) is successful.")
  Indicator.PopText()

def SelectPosition(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectPosition(customer)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.TAdvancedSpeedButton.Click(7, 8)
  incrementalSearch = cashwise.frmEmploymentPositionBrowse.pnlSearch.isMain
  incrementalSearch.Click()
  incrementalSearch.DblClick()
  incrementalSearch.Keys(customer["Position"])
  ExplicitWait(3)
  incrementalSearch.Keys("[F9]")

  if(cashwise.WaitWindow("#32770", "Cashwise", 1,5000).Exists):
    cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
    cashwise.frmEmploymentPositionBrowse.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
    
    
    cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.Keys("[Home]![End][Del]")
    cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.Keys(customer["Position"])
    ExplicitWait(3)
   
  Log.Checkpoint("SUCCESS: SelectPosition(customer) is successful.")
  Indicator.PopText()
       
def SelectPayPeriod(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Phil 8/2/2019 
  GetRandomBirthDate() 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectPayPeriod(customer)")  
  payPeriod = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod
  biweekly = payPeriod.Window("TGroupButton", "Bi-Weekly", 4)
  semiMonthly = payPeriod.Window("TGroupButton", "Semi-Monthly", 3)
  monthly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlMonthly
  weekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings
  
  if customer["PayPeriod"] == "Bi-Weekly":
    biweekly.ClickButton()
    dayOfWeekPaid = GetRadomDayOfWeek()
    weekDay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly

    
    if dayOfWeekPaid == "Monday":
      weekDay.cbPaydayMonday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Tuesday":
      weekDay.cbPaydayTuesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Wednesday":
      weekDay.cbPaydayWednesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Thursday":
      weekDay.cbPaydayThursday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Friday":
      weekDay.cbPaydayFriday.ClickButton(cbChecked)
    
    nextPayDayButton = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect
    nextPayDayButton.Click()
    secondPayDayRadial = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateTwo
    secondPayDayRadial.Click()
    payDaySelectButton = cashwise.frmContactBiweeklyEdit.btnSave
    payDaySelectButton.Click()
    

  elif customer["PayPeriod"] == "Semi-Monthly":
    cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod.Window("TGroupButton", "Semi-Monthly", 3).ClickButton()    
    ExplicitWait(3)
    semiMonthly.pnlSemiDay1.cbSemiDay1.ClickItem("5")
    semiMonthly.pnlSemiDay2.cbSemiDate2.ClickItem("20")

  elif customer["PayPeriod"] == "Monthly":
    Sys.Process("Cashwise").VCLObject("frmContactEmployerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlPayPeriodSettings").VCLObject("gbPayPeriod").Window("TGroupButton", "Monthly", 2).Click()
    Sys.Process("Cashwise").VCLObject("frmContactEmployerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlPayPeriodSettings").VCLObject("gbPayPeriodPattern").VCLObject("pnlMonthly").VCLObject("rgMonthlyPatternType").Window("TGroupButton", "The", 1).Click()
    #panel = Sys.Process("Cashwise").VCLObject("frmContactEmployerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlPayPeriodSettings").VCLObject("gbPayPeriodPattern").VCLObject("pnlMonthly")
    monthly.VCLObject("cbMonthPatternName").ClickItem("Second")
    monthly.VCLObject("cbPatternMonthDay").ClickItem("Wednesday")
    #monthly.VCLObject("cbPatternMonthDay").edDayOfMonth.ClickItem("1")
    cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlMonthly.edDayOfMonth.ClickItem("1")
  

  elif customer["PayPeriod"] == "Weekly":
    weekly.gbPayPeriod.Window("TGroupButton", "Weekly").ClickButton()
    weekly.gbPayPeriodPattern.pnlBiWeeklyWeekly.cbPaydayTuesday.ClickButton(cbChecked)
    weekly.gbPayPeriodPattern.pnlBiWeeklyWeekly.dlbledPayDate1.Click(32, 3)

  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectPayPeriod(customer) is successful.")
  Indicator.PopText()

def SaveAndCloseContactEmployers():
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveAndCloseContactEmployers()")
  cashwise.frmContactEmployerList.btnSave.Click(53, 10)
  Log.Checkpoint("SUCCESS: SaveAndCloseContactEmployers() is successful.")
  Indicator.PopText()
    

def EditSelectedEmployer():
  cashwise = Sys.Process("Cashwise")
  employerEditForm = cashwise.frmContactEmployerEdit
  editButton = cashwise.frmContactEmployerList.btnEdit
  editButton.Click()
  employerEditForm.WaitWindow(5)

#-----------------------------------------
def GetCalendarDateString(dateString):

 #  Monday, September 25, 2017      "Sunday, January 14,2018"
      #dateString = "Wednesday, January 31, 2018"
   
      
      NumOfWord = aqString.GetListLength(dateString)
      aqString.ListSeparator = ","
      wordArray = Sys.OleObject["Scripting.Dictionary"]

      for x in range(NumOfWord):
          y = aqConvert.IntToStr(x)
          wordArray.Add(y, aqString.GetListItem(dateString,x))
          s = wordArray.Item[y];
          Log.Message(s)
      dateInMonth = wordArray.Item["1"] #---January 14
      dateInMonth =  aqString.Trim(dateInMonth, aqString.stAll)
      dateInMonth = aqString.Replace(dateInMonth, " ", ",")  #'January,31'
      NumOfWord2 = aqString.GetListLength(dateInMonth)
      aqString.ListSeparator = ","
      
      wordArray2 = Sys.OleObject["Scripting.Dictionary"]
      x = 0
      y = ""
      for x in range(NumOfWord2):
                y = aqConvert.IntToStr(x)
                wordArray2.Add(y, aqString.GetListItem(dateInMonth,x))
                s = wordArray2.Item[y];
                Log.Message(s)
      dateInMonth2 = wordArray2.Item["1"]
      Log.Message(dateInMonth2)

def StringDateParse(dateString): #--Converts Sunday, January 14, 2018 to 1/14/2018

   
      # dateString = Sys.Process"]("Cashwise").VCLObject"]("frmCalendarEdit").VCLObject"]("txtDate").Caption"]
      #dateString = "Sunday, January 14, 2018"
      dateString = aqString.Replace(dateString, ",", "")
      aqString.ListSeparator = " "
      NumOfWord = aqString.GetListLength(dateString)
     
      wordArray = Sys.OleObject["Scripting.Dictionary"]

      for x in range(NumOfWord):
          y = aqConvert.IntToStr(x)
          wordArray.Add(y, aqString.GetListItem(dateString,x))
          s = wordArray.Item[y];
          Log.Message("number = "+aqConvert.IntToStr(x)+"word = "+s) 
      
      dayOfTheWeek = wordArray.Item["0"]
      monthWord = wordArray.Item["1"]
      dayOfMonth = wordArray.Item["2"]
      year = wordArray.Item["3"]
          
     
      monthDate = switchMonthToNumber(monthWord)
 
      dateStringNum = monthDate+"/"+dayOfMonth+"/"+year
      Log.Message("Converted "+dateString+" to "+dateStringNum)
      
      return dateStringNum

      
      #--------------------------------------
    
def switchMonthToNumber(monthString):
    switcher = {
        "January": "1",
        "February": "2",
        "March": "3",
        "April": "4",
        "May": "5",
        "June": "6",
        "July": "7",
        "August": "8",
        "September": "9",
        "October": "10",
        "November": "11",
        "December": "12"
        
    }
    month = switcher.get(monthString, "Invalid month")
    return month
    
    
    
   #--------------
   #------
    

    
def  EditEmploymentPayAmount(customerId, direction):
         #Author Phil Ivey 9/04/2019
         #Last Modified by Phil Ivey 9/04/2019
         #customerId = "001-0000841"
         #direction = "Increase"
        cashwise = Sys.Process("Cashwise")
        CustomerBrowseSearchMainScreen(customerId,"ID","Edit")
        cashwise.frmCustomerEdit.btnEmployers.Click()
        cashwise.frmContactEmployerList.btnEdit.Click()
        payPath = cashwise.frmContactEmployerEdit.pnlMain.sbData
        oldGross = payPath.dbedGrossPay.wText
        oldNet = payPath.dbedNetPay.wText
        if(direction == "Increase"):
          newFlGross = aqConvert.StrToFloat(oldGross)
          newFlNet = aqConvert.StrToFloat(oldNet)
          newFlGross = newFlGross + (newFlGross*.015)
          newFlNet = newFlNet + (newFlNet*.015)
               
        if(direction == "Decrease"):
          newFlGross = aqConvert.StrToFloat(oldGross)
          newFlNet = aqConvert.StrToFloat(oldNet)
          newFlGross = newFlGross - (newFlGross * .015)
          newFlNet = newFlNet - (newFlNet * .015)
          
        newStGross = aqConvert.CurrencyToFormatStr(newFlGross,2,0,0,0)
        newStNet = aqConvert.CurrencyToFormatStr(newFlNet,2,0,0,0)
        payPath.dbedGrossPay.Keys(newStGross)
        payPath.dbedNetPay.Keys(newStNet)
        payPath.dbedNetPay.Keys("[Tab]")
        cashwise.frmContactEmployerEdit.btnSave.Click()
        ExplicitWait(2)
        if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,5000).Exists):
           cashwise.frmCalendarEdit.btnSave.Click()
      
def  EditEmploymentPayDate(cusID,testResults):
      #Author Phil Ivey 9/04/2019
     #Last Modified by Phil Ivey 01/13/2020
        #customerId = "001-0000494"
        cashwise = Sys.Process("Cashwise")
        CustomerBrowseSearchMainScreen(cusID,"ID","Edit")
        cashwise.frmCustomerEdit.btnEmployers.Click()
        cashwise.frmContactEmployerList.btnEdit.Click()
        PayPeriod = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod
        payPeriod = ""
        cashwiseDate = GetLocationDate("001")
       
        if(PayPeriod.Find("Caption","Bi-Weekly",10).Checked == True):
          payPeriod = "B"
        if(PayPeriod.Find("Caption","Semi-Monthly",10).Checked == True):
               payPeriod = "S"
        if(PayPeriod.Find("Caption","Monthly",10).Checked == True):
          payPeriod = "M"
        if(PayPeriod.Find("Caption","Weekly",10).Checked == True):
          payPeriod = "W"
          
          #-----  Weekly Bi-Weekly   
        if(payPeriod == "B" or payPeriod == "W"):
           nextPayBox = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.dlbledPayDate1
           payDayOfWeek = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly
           weeklyBiWeeklyButton = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect
           nextPayPeriod = nextPayBox.WndCaption # Next Pay Date
           if(payPeriod == "B"):
             nexPayDateButton = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect
         
           theDate = aqConvert.StrToDate(nextPayPeriod)
           dayNumberOfWeek = aqDateTime.GetDayOfWeek(theDate) # Day of week for Next pay date
           
           if(dayNumberOfWeek == 2):  #------Monday to Wednesday
                     newPayDate = aqDateTime.AddDays(nextPayPeriod,2)
                     payDayOfWeek.cbPaydayWednesday.ClickButton(cbChecked)
                     if(payPeriod == "B"):
                       nexPayDateButton.Click()
                       newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                       cashwise.frmContactBiweeklyEdit.btnSave.Click()
               
           if(dayNumberOfWeek == 3):  #------Tuesday to Thursday
                     newPayDate = aqDateTime.AddDays(nextPayPeriod,2)
                     payDayOfWeek.cbPaydayThursday.ClickButton(cbChecked)
                     if(payPeriod == "B"):
                       nexPayDateButton.Click()
                       newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                       cashwise.frmContactBiweeklyEdit.btnSave.Click()
          
           if(dayNumberOfWeek == 4):  #------Wednesday to Thursday
                     newPayDate = aqDateTime.AddDays(nextPayPeriod,1)
                     payDayOfWeek.cbPaydayThursday.ClickButton(cbChecked)
                     if(payPeriod == "B"):
                       nexPayDateButton.Click()
                       newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                       cashwise.frmContactBiweeklyEdit.btnSave.Click()
            
           if(dayNumberOfWeek == 5):  #------ Thursday to Friday
                     newPayDate = aqDateTime.AddDays(nextPayPeriod,1)
                     payDayOfWeek.cbPaydayFriday.ClickButton(cbChecked)
                     if(payPeriod == "B"):
                       nexPayDateButton.Click()
                       newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                       cashwise.frmContactBiweeklyEdit.btnSave.Click()
       
           if(dayNumberOfWeek == 6):  #------ Friday to Monday
                      newPayDate = aqDateTime.AddDays(nextPayPeriod,3)
                      payDayOfWeek.cbPaydayMonday.ClickButton(cbChecked)
                      if(payPeriod == "B"):
                       nexPayDateButton.Click()
                       newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                       cashwise.frmContactBiweeklyEdit.btnSave.Click()
   
   #------------------  Monthly   
                   
        if(payPeriod == "M"):
          #nextPayBox = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.edPayDayOne
          recurrencePattern = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType
          if(recurrencePattern.Window("TGroupButton", "Day", 2).Checked == True):
             patternType = "Day"
          if(recurrencePattern.Window("TGroupButton", "The", 1).Checked == True):
             patternType = "The" 
   
          if(patternType == "Day"):
            dayPath = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.edDayOfMonth  
            newDate = randrange(1,25)
            dayPath.Keys("1")
            for x in range(newDate):
             dayPath.Keys("[Down]")
            dayPath.Keys("[Enter]")
            
          
          if(patternType == "The"): # 0 = first, 1 = second, 2 = third, 3 = forth
            thePath = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.cbMonthPatternName
            itemCount = thePath.wItemCount
            selectedItem = thePath.wSelectedItem
            newSelect = selectedItem - 1
            if(newSelect < 0):
              newSelect = 3
            thePath.Click()
            thePath.Keys("[Up][Up][Up][Up]")
            for x in range(newSelect):
              thePath.Keys("[Down]")
            thePath.Keys("[Enter]")
            
         #------------------ Semi--Monthly    
            
        if(payPeriod == "S"):
         firstSemiPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlSemiMonthly.pnlSemiDay1.cbSemiDay1
         secondSemiPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlSemiMonthly.pnlSemiDay2.cbSemiDate2
         newDate = randrange(1,15)
         secondSemiPay.Keys("[Del]")
         firstSemiPay.Keys("1")
         for x in range(newDate):
            firstSemiPay.Keys("[Down]")
         firstSemiPay.Keys("[Enter]")
      
        #-- MAKE SURE NEXT PAYDATE IS GREATER THAN CASHWISE DATE
        nextPayDateString = cashwise.frmContactEmployerEdit.lblNextPayDateInfo.Caption
        employNextPayDate = StringDateParse(nextPayDateString)
        dateEmployNextPay = aqConvert.StrToDate(employNextPayDate)
        dateCashwiseDate = aqConvert.StrToDate(cashwiseDate)
        if(dateEmployNextPay <= dateCashwiseDate):
          paySelect = cashwise.frmContactEmployerEdit.pnlMain.sbData
          paySelect.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect.Click()
          ExplicitWait(1)
          cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
          cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateTwo.Click()
          #cashwise.frmContactEmployerEdit.pnlMain.sbData.VCLObject("pnlDates").VCLObject("rbDateTwo").Click()
          cashwise.frmContactBiweeklyEdit.btnSave.Click()
        
        cashwise.frmContactEmployerEdit.btnSave.Click()   
        ExplicitWait(2)
        if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1500).Exists):
           cashwise.frmCalendarEdit.btnSave.Click()
        if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists): 
         testResults = CheckForOptionalPayAuth(cusID,testResults)      
         ExplicitWait(6)
        if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists):
           cashwise.RavePreviewForm.Keys("~fx")
        ExplicitWait(6)
        if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists):
           cashwise.RavePreviewForm.Keys("~fx")
        cashwise.frmContactEmployerList.btnSave.Click()
        cashwise.frmCustomerEdit.btnSave.Click()
        cashwise.frmCustomerBrowse.btnClose.Click()
        return testResults 

def CustomerBrowser():
  Aliases.Cashwise.frmMain.TGraphicButton2.Click(39, 26)

def SearchName(customer):
  incrementalSearch = Aliases.Cashwise.frmCustomerBrowse.pnlSearch.isMain
  incrementalSearch.Click(51, 10)
  cusName = customer["LastName"]+", "+customer["FirstName"]
  incrementalSearch.Keys(cusName)
  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectCustomerByName(customer) is successful.")
  #Indicator.PopText()

def Test1():
  Aliases.Cashwise.frmCustomerBrowse.btnEdit.Click(29, 21)

def Test2():
  cashwise = Aliases.Cashwise
  cashwise.frmCustomerEdit.btnSave.Click(63, 5)
  cashwise.frmCustomerBrowse.btnSave.Click(48, 9)
