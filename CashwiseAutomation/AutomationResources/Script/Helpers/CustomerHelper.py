﻿from BasePage import *
from Helpers import *
from LoanHelper import *
#from LineOfCreditHelper import *
#from DatabaseQuery import *
#from Customers import *
#cashwise = Sys.Process("Cashwise")

def GenerateUniqueSSN():
    #Author Brian Johnson 3/22/2021
    #Last Modified by Pat Holman 3/22/2021  
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: GenerateUniqueSSN")
    randomSSN = GenerateRandomSSN()
    ssnStatus = DoesSSNExistInDatabase(randomSSN)  #Exists and NotExists
    while ssnStatus == "Exists":
      randomSSN = GenerateRandomSSN()
      ssnStatus = DoesSSNExistInDatabase(randomSSN)
    Log.Checkpoint("SUCCESS: OpenCustomerBrowser() is successful.")
    Indicator.PopText()
    return randomSSN

def OpenCustomerBrowser():
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenCustomerBrowser()")
  
  if cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists:
    cashwise.Window("TMessageForm","Warning",1).VCLObject("OK").Click()
  
  cashwise.VCLObject("frmMain").VCLObject("TGraphicButton_2").Click()
  #Sys.Process("Cashwise").VCLObject("frmMain").VCLObject("pnlButtons")
  
  Log.Checkpoint("SUCCESS: OpenCustomerBrowser() is successful.")
  Indicator.PopText()
  
def CustomerBrowseSearchMainScreen(criteria,search_by,action):
  #Author Phil Ivey 8/19/2019
  #Last Modified by Phil Ivey 8/19/2019  
  cashwise = Sys.Process("Cashwise")
  #search_by = "Name"  criteria = "Adams,Jenna"   action = "Edit"
  
  cashwise.frmMain.TGraphicButton_2.Click()
  
  
  text = cashwise.frmCustomerBrowse.pnlSearch.icMain.Window("TEdit", "*", 1).wText
  down = 4
  while(text != search_by):
      cashwise.frmCustomerBrowse.pnlSearch.icMain.TAdvancedSpeedButton.Click()
      cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home]")
      for x in range(down):
        cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Down]")
      cashwise.frmAbstractList.btnSave.Click()
      down = down +(-1)
      text = cashwise.frmCustomerBrowse.pnlSearch.icMain.Window("TEdit", "*", 1).wText
      
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(criteria)
  
  if(action == "Add"):
    cashwise.frmCustomerBrowse.btnAdd.Click()
  elif(action == "Edit"):
    cashwise.frmCustomerBrowse.btnEdit.Click()
  elif(action == "Delete"):
    cashwise.frmCustomerBrowse.btnDelete.Click()
  elif(action == "Save & Close"):
    cashwise.frmCustomerBrowse.Keys("~s")
  
def CustomerBrowseSearchBrowseScreen(criteria,search_by,action):
  #Author Phil Ivey 8/19/2019
  #Last Modified by Phil Ivey 8/19/2019  
  cashwise = Sys.Process("Cashwise")
  #search_by = "Name"  criteria = "Adams,Jenna"   action = "Edit"
  
   
  text = cashwise.frmCustomerBrowse.pnlSearch.icMain.Window("TEdit", "*", 1).wText
  down = 4
  while(text != search_by):
      cashwise.frmCustomerBrowse.pnlSearch.icMain.TAdvancedSpeedButton.Click()
      cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home]")
      for x in range(down):
        cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Down]")
      cashwise.frmAbstractList.btnSave.Click()
      down = down +(-1)
      text = cashwise.frmCustomerBrowse.pnlSearch.icMain.Window("TEdit", "*", 1).wText
      
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(criteria)
  
  if(action == "Add"):
    cashwise.frmCustomerBrowse.btnAdd.Click()
  elif(action == "Edit"):
    cashwise.frmCustomerBrowse.btnEdit.Click()
  elif(action == "Delete"):
    cashwise.frmCustomerBrowse.btnDelete.Click()
  elif(action == "Save & Close"):
    cashwise.frmCustomerBrowse.Keys("~s")
  

    
    
def ClickAddCustomerButton():
  #Author Pat Holman 5/22/2019
  #Last Modified by Phil Ivey 01/08/2020  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickAddCustomerButton()")
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("btnAdd").Click()
  #if not (cashwise.WaitWindow("TfrmCustomerAdd","Customer",1,800).Exists):
    #cashwise.frmCustomerBrowse.Keys("~a")
  
    
  Indicator.PopText()

def SaveNewCustomer():
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019 
  localConfig = GetLocalConfigSettings() 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveNewCustomer()")
  if(localConfig["Database"] == "UtahTest"):
    formCustomerEdit = cashwise.frmCustomerEdit
    formCustomerEdit.Keys("[F9]")
  else:
    formCustomerAdd = cashwise.frmCustomerAdd
    formCustomerAdd.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SaveNewCustomer() is successful.")
  Indicator.PopText()
  
def DeleteCustomer(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteCustomer(customer)")
  OpenCustomerBrowser()
  customerBrowseTable = cashwise.frmCustomerBrowse
  SelectUseSmartSearch()
  searchInput = customerBrowseTable.pnlSearch.isMain
  searchInput.Keys(customer["LastName"])
  ExplicitWait(3)
  customerBrowseTable.btnDelete.Click(14, 23)
  searchInput.DblClick(77, 12)
  searchInput.Keys("[Del]")
  customerBrowseTable.MainMenu.Click("[0]|[5]")
  Log.Checkpoint("SUCCESS: DeleteCustomer(customer) is successful.")
  Indicator.PopText()
  
def SearchCustomerFirstName(customer):
  #Author Pat Holman 12/18/2020
  #Last Modified by Adam Gibbons 04/20/2021
  SelectSmartSearch()
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Enter search text")
  tfrmCustomerBrowse = cashwise.frmCustomerBrowse
  incrementalSearch = tfrmCustomerBrowse.pnlSearch.isMain
  incrementalSearch.Click(115, 6)
  customerFullName = customer["LastName"] + ", " + customer["FirstName"]
  incrementalSearch.Keys(customerFullName)
  #cashwise.frmCustomerBrowse.btnSave.Click(58, 12)
  ExplicitWait(1)
  cashwise.frmCustomerBrowse.VCLObject("btnSave").Click()
  Log.Checkpoint("SUCCESS: Enter search text is successful.")
  Indicator.PopText()
  
def SelectSearchByFirstName():
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/18/2020
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerBrowse.pnlSearch.icMain.TAdvancedSpeedButton.Click(8, 9)
  tfrmAbstractList = cashwise.frmAbstractList
  browseGrid = tfrmAbstractList.pnlMain.dbgMain
  browseGrid.HScroll.Pos = 0
  browseGrid.VScroll.Pos = 9
  browseGrid.Click(39, 97)
  tfrmAbstractList.btnSave.Click(30, 10)
    
def SelectSmartSearch():
  #Author Pat Holman 5/22/2019
  #Last Modified by Adam Gibbons 4/14/2021  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectSearchByName()")
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("pnlSearch").VCLObject("icMain").VCLObject("TAdvancedSpeedButton").Click(9, 7)
  browseGrid = cashwise.VCLObject("frmAbstractList").VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.Keys("[PageUp][PageUp][PageUp]")
  saveandcloseButton = cashwise.VCLObject("frmAbstractList").VCLObject("btnSave")
  saveandcloseButton.Click()
  Log.Checkpoint("SUCCESS: SelectSearchByName() is successful.")
  Indicator.PopText()

def EnterSocialSecurityNumber(SSN):  
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019 
  localConfig = GetLocalConfigSettings() 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterSocialSecurityNumber(customer)")
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlSSN.edSSN.Click()
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlSSN.edSSN.Click()
  ssnEdit = cashwise.frmCustomerSSN.pnlMain.sbData.edSSN
  ssnEdit.Keys(SSN)
  ssnEdit.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: EnterSocialSecurityNumber(customer) is successful.")
  Indicator.PopText()

def SelectEyeColor(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectEyeColor(customer)")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dblefEyeColor.TAdvancedSpeedButton.Click()
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.dblefEyeColor.TAdvancedSpeedButton.Click()
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(customer["EyeColor"])
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dblefEyeColor
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.dblefEyeColor
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,1800).Exists):
    cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
    cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Home]![End][Del]")
    cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("BRO")
    cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectEyeColor(customer) is successful.")
  Indicator.PopText()

def SelectHairColor(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  Indicator.PushText("In SelectHairColor(customer)")
  cashwise = Sys.Process("Cashwise")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dblefHairColor.TAdvancedSpeedButton.Click()
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.dblefHairColor.TAdvancedSpeedButton.Click()
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(customer["HairColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,1800).Exists):
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
      cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Home]![End][Del]")
      cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("BRO")
      cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")

  Log.Checkpoint("SUCCESS: SelectHairColor(customer) is successful.")
  Indicator.PopText()

def SelectGender(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectGender(customer)")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    if customer["Gender"] == "Male":
      cashwise.VCLObject("frmCustomerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "M", 3).Click()
    elif customer["Gender"] == "Female":
      cashwise.VCLObject("frmCustomerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "F", 2).Click()
  else:
    if customer["Gender"] == "Male":
      cashwise.VCLObject("frmCustomerAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "M", 3).Click()
    elif customer["Gender"] == "Female":
      cashwise.VCLObject("frmCustomerAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "F", 2).Click()
  Log.Checkpoint("SUCCESS: SelectGender(customer) is successful.")
  Indicator.PopText()
  
def SelectMarketingID(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Phil Ivey 10/11/2019
  Indicator.PushText("In SelectMarketingID(customer)")
  cashwise = Sys.Process("Cashwise")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMarketing.TAdvancedSpeedButton.Click()
  else: 
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMarketing.TAdvancedSpeedButton.Click()
  incrementalSearch = cashwise.VCLObject("frmMarketingBrowse").VCLObject("pnlSearch").VCLObject("isMain")
  incrementalSearch.Keys(customer["MarketingID"])
  ExplicitWait(3)
  incrementalSearch.Keys("[Enter]")
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,1800).Exists):
        cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
        incrementalSearch.Keys("[Home]![End][Del]")
        incrementalSearch.Keys("[Down][Down]")
        incrementalSearch.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectMarketingID(customer) is successful.")
  Indicator.PopText()

def SetMembershipExpertation(months):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  Indicator.PushText("In SetMembershipExpertation(months)")
  cashwise = Sys.Process("Cashwise")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMembershipExpirationDate.TAdvancedSpeedButton.Click(10, 9)
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMembershipExpirationDate.TAdvancedSpeedButton.Click(10, 9)
  tfrmCalendarEdit = cashwise.frmCalendarEdit
  advancedButton = tfrmCalendarEdit.Panel1.btnNextMonth
  for x in range(months):
    advancedButton.Click(16, 24)
    ExplicitWait(1)
  advancedButton.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SetMembershipExpertation(months) is successful.")
  Indicator.PopText()

def SelectID1Type(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectID1Type(customer)")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    #cashwise.VCLObject("frmCustomerEdit").MainMenu.Click("[2]|[8]")
    #panel = cashwise.frmCustomerEdit.pnlMain.sbData.pnlAdditional.pnlID1
    #cashwise.frmCustomerAdditionalEdit
    Log.Checkpoint("Skip Additional Information")
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlID1.edID1Type.TAdvancedSpeedButton.Click(11, 6)
    panel = cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlID1
    TDBLookupEditForm = panel.edID1Type
    cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys(customer["ID1"])
    ExplicitWait(3)
    cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
    Log.Checkpoint("SUCCESS: SelectID1Type(customer) is successful.")
  Indicator.PopText()
  
def SelectID2Type(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectID2Type(customer)")
  localConfig = GetLocalConfigSettings() 
  if(localConfig["Database"] == "UtahTest"):
    Log.Checkpoint("Skip Additional Information")
  else:
    cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlID2.edID2Type.TAdvancedSpeedButton.Click(10, 7)
    cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys(customer["ID2"])
    ExplicitWait(3)
    cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
    Log.Checkpoint("SUCCESS: SelectID2Type(customer) is successful.")
  Indicator.PopText() 

def SelectReferenceRelation(customer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectReferenceRelation(customer)")
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlReference.edReference1Relation.TAdvancedSpeedButton.Click(7, 5)
  cashwise.frmContactReferenceTypeBrowse.pnlSearch.isMain.Keys(customer["ReferenceRelation"])
  ExplicitWait(3)
  cashwise.frmContactReferenceTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectReferenceRelation(customer) is successful.")
  Indicator.PopText() 

def CloseCustomerBrowse():
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 1/5/2021  
  localConfig = GetLocalConfigSettings()
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseCustomerBrowse()")
  if(localConfig["Database"] == "UtahTest"):
    cashwise.RavePreviewForm.Close()
  tfrmCustomerBrowse = cashwise.frmCustomerBrowse
  tfrmCustomerBrowse.Click(273, 16)
  tfrmCustomerBrowse.pnlMain.dbgMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: CloseCustomerBrowse() is successful.")
  Indicator.PopText()

def BiWeeklyPaydaySelect():
  #Author Pat Holman 10/09/2020
  #Last Modified by Pat Holman 10/09/2020  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In BiWeeklyPaydaySelect()")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.VCLObject("gbPayPeriod").Window("TGroupButton", "Bi-Weekly").ClickButton()
  cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly.cbPaydayTuesday.ClickButton(cbChecked)
  cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Bi-Weekly", 4).Click()
  cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
  CloseBiweeklyPopUp()
  Log.Checkpoint("SUCCESS: BiWeeklyPaydaySelect() is successful.")
  Indicator.PopText()
  
def CloseBiweeklyPopUp():
  #Author Pat Holman 10/09/2020
  #Last Modified by Pat Holman 10/09/2020  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseBiweeklyPopUp()")
  cashwise.frmContactBiweeklyEdit.btnSave.Click()
  Log.Checkpoint("SUCCESS: CloseBiweeklyPopUp() is successful.")
  Indicator.PopText()
     
def OpenCustomerEdit():
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenCustomerEdit()")
  #OpenCustomerBrowser()
  tfrmCustomerBrowse = cashwise.frmCustomerBrowse
  tfrmCustomerBrowse.Click(323, 13)
  tfrmCustomerBrowse.pnlSearch.isMain.Keys("[F11]")    
  Log.Checkpoint("SUCCESS: OpenCustomerEdit() is successful.")
  Indicator.PopText()

def SaveAndCloseCustomerEdit():
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveAndCloseCustomerEdit()")
  cashwise.frmCustomerEdit.btnSave.Click(65, 10)
  Log.Checkpoint("SUCCESS: SaveAndCloseCustomerEdit() is successful.")
  Indicator.PopText()

def SelectCustomerByName(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectCustomerByName(customer)")
  #cashwise.VCLObject("frmCustomerBrowse").VCLObject("pnlSearch").VCLObject("icMain").VCLObject("TAdvancedSpeedButton")
  #CashwiseMainMenu_Window("Top")
  
  incrementalSearch = cashwise.frmCustomerBrowse.pnlSearch.isMain
  #SelectSearchByName()
  cusName = customer["LastName"]+", "+customer["FirstName"]
  incrementalSearch.Keys(cusName)
  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectCustomerByName(customer) is successful.")
  Indicator.PopText()

def EditPrimaryPhoneType(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditPrimaryPhoneType(editCustomer)")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID.TAdvancedSpeedButton.Click(8, 7)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    YesButton = cashwise.Window("TMessageForm", "Confirm").VCLObject("Yes")
    aqObject.CheckProperty(YesButton, "Exists", cmpEqual, True)
    YesButton.Click(58, 9)
  ExplicitWait(1)
  ButtonSave = cashwise.VCLObject("frmPhoneTypeBrowse").VCLObject("btnSave")
  ButtonSave.Click(46, 10)
  Log.Checkpoint("SUCCESS: EditPrimaryPhoneType(editCustomer) is successful.")
  Indicator.PopText()

def EditSecondaryPhoneType(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditSecondaryPhoneType(editCustomer)")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edSecondaryPhoneTypeID.TAdvancedSpeedButton.Click(11, 6)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editCustomer["SecondaryPhoneType"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    YesButton = cashwise.Window("TMessageForm", "Confirm").VCLObject("Yes")
    aqObject.CheckProperty(YesButton, "Exists", cmpEqual, True)
    YesButton.Click(58, 9)
  ExplicitWait(1)
  Log.Checkpoint("SUCCESS: EditSecondaryPhoneType(editCustomer) is successful.")
  Indicator.PopText()

def EditSocialSecurityNumber(editCustomer):  
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditSocialSecurityNumber(editCustomer)")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlSSN.edSSN.Click(40, 10)
  TDBEdit = cashwise.frmCustomerSSN.pnlMain.sbData.edSSN
  TDBEdit.Click()
  TDBEdit.Keys(editCustomer["SSN"])
  TDBEdit.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditSocialSecurityNumber(editCustomer) is successful.")
  Indicator.PopText()

def EditBirthDate(days):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditBirthDate(days)")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.edBirthdate.TAdvancedSpeedButton.Click(8, 4)
  calendarEdit = cashwise.frmCalendarEdit
  for x in range(days):
    calendarEdit.Panel1.btnPriorDay.Click(10, 30)
    ExplicitWait(1)
  Log.Checkpoint("SUCCESS: EditBirthDate(days) is successful.")
  Indicator.PopText()
  calendarEdit.pnlMain.Calendar.Keys("[F9]")

def EditEthnicity(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditEthnicity(editCustomer)")
  groupBox = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal
  groupBox.edDescription.TAdvancedSpeedButton.Click(8, 5)
  ethnicity = cashwise.frmContactDescriptionBrowse.pnlSearch.isMain
  ethnicity.Keys(editCustomer["Ethnicity"])
  ExplicitWait(3)
  ethnicity.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditEthnicity(editCustomer) is successful.")
  Indicator.PopText()


def EditPrimaryLanguage(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditPrimaryLanguage(editCustomer)")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.edPrimaryLanguage.TAdvancedSpeedButton.Click(6, 7)
  languageBrowse = cashwise.frmLanguageBrowse.pnlSearch.isMain
  languageBrowse.Keys(editCustomer["PrimaryLanguage"])
  ExplicitWait(3)
  languageBrowse.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditPrimaryLanguage(editCustomer) is successful.")
  Indicator.PopText()    

def EditEyeColor(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditEyeColor(editCustomer)")
  speedButton = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dblefEyeColor.TAdvancedSpeedButton
  speedButton.Click(11, 8)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(editCustomer["EyeColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditEyeColor(editCustomer) is successful.")
  Indicator.PopText()    
    
def EditHairColor(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditHairColor(editCustomer)")
  speedButton = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dblefHairColor.TAdvancedSpeedButton
  speedButton.Click(8, 5)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(editCustomer["HairColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditHairColor(editCustomer) is successful.")
  Indicator.PopText()

def EditGender(editCustomer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditGender(editCustomer)")
  if editCustomer["Gender"] == "Male":
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender.Window("TGroupButton", "M", 3).ClickButton()
  elif editCustomer["Gender"] == "Female":
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender.Window("TGroupButton", "F", 2).ClickButton()
  else:
    cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender.Window("TGroupButton", "U", 1).ClickButton()
  Log.Checkpoint("SUCCESS: EditGender(editCustomer) is successful.")
  Indicator.PopText()

def EditMarketing(editCustomer):
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditMarketing(editCustomer)")
  speedButton = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMarketing.TAdvancedSpeedButton
  speedButton.Click(11, 6)
  cashwise.frmMarketingBrowse.pnlSearch.isMain.Keys(editCustomer["MarketingID"])
  ExplicitWait(3)
  cashwise.frmMarketingBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditMarketing(editCustomer) is successful.")
  Indicator.PopText()

def EditNotes():
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditNotes()")
  notes = cashwise.frmCustomerEdit.pnlMain.pnlNotes.dbmNotes
  notes.Keys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed iaculis quam. Morbi tempus consectetur.")
  Log.Checkpoint("SUCCESS: EditNotes() is successful.")
  Indicator.PopText()   
  
def EditPrimaryPhone(customer):
  #Author Phil IVey 8/19/2019
  #Last Modified by Phil Ivey 8/19/2019  
  cashwise = Sys.Process("Cashwise")
  #CustomerBrowseSearch(customer["FullName"],"Name","Edit")
  CustomerBrowseSearchMainScreen(customer["FullName"],"Name","Edit")
  phone = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  phone.edPrimaryPhone.Keys(customer["PrimaryPhone"])
  Indicator.PushText("In EditPrimaryPhone(customer)")
  ExplicitWait(1)
  phone.edPrimaryPhoneTypeID.Window("TCustomDBEdit", "*", 1).Keys(customer["PrimaryPhoneType"])
  cashwise.frmCustomerEdit.btnSave.Click()
  cashwise.frmCustomerBrowse.btnSave.Click()
  Log.Checkpoint("SUCCESS: EditPrimaryPhone is successful.")
  Indicator.PopText()    
 

def ClickRefrenceButton():
  #Author Pat Holman 5/30/2019
  #Last Modified by Pat Holman 5/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickRefrenceButton()")
  cashwise.frmCustomerEdit.btnReferences.Click()
  cashwise.frmContactReferenceList.btnEdit.Click()
  Log.Checkpoint("SUCCESS: ClickRefrenceButton() is successful.")
  Indicator.PopText()    

def SelectStatusID():
  #Author Pat Holman 6/7/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  customerEditButtonReferences = cashwise.frmCustomerEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectStatusID()")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.dblStatus.TAdvancedSpeedButton.Click(7, 6)
  statusList = cashwise.frmAddressStatusList.pnlMain.dbgMain
  statusList.VScroll.Pos = 0
  statusList.Click(49, 97)
  statusList.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectStatusID() is successful.")

def SelectPrimaryPhoneTypeID(editCustomer):
  #Author Pat Holman 6/7/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  customerEditButtonReferences = cashwise.frmCustomerEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectPrimaryPhoneTypeID(editCustomer)")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.edPrimaryPhoneTypeID.TAdvancedSpeedButton.Click(6, 7)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editCustomer["ReferencePrimaryPhoneTypeID"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectPrimaryPhoneTypeID(editCustomer) is successful.")

def SelectSecondaryPhoneTypeID(editCustomer):
  #Author Pat Holman 6/7/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  customerEditButtonReferences = cashwise.frmCustomerEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectSecondaryPhoneTypeID(editCustomer)")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.edSecondaryPhoneTypeID.TAdvancedSpeedButton.Click(9, 10)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editCustomer["ReferenceSecondaryPhoneTypeID"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectSecondaryPhoneTypeID(editCustomer) is successful.")

def SelectCustomerFromBrowser(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectCustomerFromBrowser(customer)")
  cashwise.frmMain.TGraphicButton.Click()
  if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
    cashwise.frmCashOpenConfirm.Button1.Click()
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
      cashwise.frmCashOpenConfirm.Button1.Click()

  tfrmCustomerBrowse = cashwise.frmCustomerBrowse
  cusName = customer["LastName"]+", "+customer["FirstName"]
  tfrmCustomerBrowse.pnlSearch.isMain.Keys(cusName)
  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectCustomerFromBrowser(customer) is successful.")

  
def CreatCustomerData():
     #Author Phil Ivey 8/20/2019
     #Last Modified by Phil Ivey 8/20/2019  
     locationID = GetLocationIDFromfrmMainWndCap()
     cashwise = Sys.Process("Cashwise")
     firstName = GetRandomFirstName()
     lastName = GetRandomLastName()
     randomText = GenerateRandomString(8)
     locID = GetLocationIDFromfrmMainWndCap()
     randomMiddleInitial = GenerateRandomString(1)
     randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
     randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
     ReferencePrimaryPhone = GenerateRandomPhoneNumber()
     randomSSN = GenerateRandomSSN()
     
     today = aqDateTime.Today()
     birthDay = GetRandomBirthDate()
     zip = GetRandomZip()
     city = GetCityFromZip(zip)
     address1 = GetRamdomAddressInCity(city)
     state = GetStateFromZip(zip)
     email = lastName+firstName+"@ivey.pro"
     employerName = GetRandomCustomerEmplorerName()
     department = GetRandomDepartment()
     position = GetRandomPostiion(department)
     payPeriod = RandomPayFreq()
     if payPeriod == "M":
       payPeriod = "Monthly"
     elif payPeriod == "S":
       payPeriod = "Semi-Monthly"
     elif payPeriod == "B":
       payPeriod = "Bi-Weekly"
     elif payPeriod == "W":
       payPeriod = "Weekly"
     payPeriod = "Bi-Weekly"
     newContact = {}
     newContact["FirstName"] = firstName
     newContact["LastName"] = lastName
     newContact["MiddleInitial"] = randomMiddleInitial
     newContact["FullName"] = lastName+", "+firstName
     newContact["PrimaryPhone"] = randomPrimaryPhoneNumber
     newContact["PrimaryPhoneType"] = "HOM"
     newContact["SecondaryPhone"] = randomSecondaryPhoneNumber
     newContact["SecondaryPhoneType"] = "CEL"
     newContact["SSN"] = randomSSN
     newContact["AddressType"] = "HOM"
     newContact["Address1"] = address1
     newContact["Address2"] = "Apt. 312"  
     newContact["City"] = city  
     newContact["State"] = state 
     newContact["ZipCode"] = zip
     newContact["BirthDate"] = birthDay  
     newContact["Ethnicity"] = "CC" 
     newContact["PrimaryLanguage"] = "ENG" 
     newContact["Height"] = "511" 
     newContact["Weight"] = "200" 
     newContact["EyeColor"] = "BRN" 
     newContact["HairColor"] = "BRN" 
     newContact["Gender"] = "Male" 
     newContact["MarketingID"] = "TV" 
     newContact["Email"] = email
     newContact["ID1"] = "ID"
     newContact["ID1_State"] = state
     newContact["ID1_Value"] = randomText
     newContact["ID2"] = "DL"
     newContact["ID2_State"] = "UT"
     newContact["ID2_Value"] = randomText
     newContact["Password"] = randomText
     newContact["ReferenceRelation"] = "OTH"
     newContact["ReferenceName"] = "Reference_Name" + randomText
     newContact["ReferencePrimaryPhone"] = ReferencePrimaryPhone
     newContact["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
     newContact["ReferenceAddress1"] = "512 N 200 E"
     newContact["ReferenceCity"] = "Orem"
     newContact["ReferenceState"] = "UT"
     newContact["EmployerName"] = employerName
     newContact["Department"] = department
     newContact["Position"] = position
     newContact["WorkPhone"] = "8013611123"
     newContact["WorkPhoneExtention"] = "1123"
     newContact["ReferenceZipCode"] = "84057"
     newContact["Supervisor"] = "RaNay Ash"
     newContact["SupervisorPhone"] = "8013611122"
     newContact["SupervisorPhoneExtention"] = "1122"
     newContact["PayPeriod"] = payPeriod
     newContact["GrossPay"] = "4000"
     newContact["NetPay"] = "3000"
     newContact["Garnishment"] = "1000"
     newContact["WorkStartTime"] = "8:00 AM"
     newContact["WorkStopTime"] = "5:00 PM"
     newContact["Overide"] = "~s"
     newContact["isRandomType"] = True  
     newContact["locationID"] = locationID
     newContact["cusID"] = ""
     Log.Message("Customer Name = "+newContact["FullName"]) 
     Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
     return newContact

def AddCustomerNoData():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer
    

def GetRandomFirstName():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
    aQuery2 = "SELECT TOP 1 FirstName FROM Contact where FirstName is Not NULL ORDER BY NEWID()"
    columnName2 = "FirstName" 
    #Project.Variables.AddVariable("firstName","string")
    
    firstName = RunSingleQuery(aQuery2,columnName2)
    Log.Message("First Name = "+firstName)
    return firstName

def GetRandomLastName():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  aQuery = "SELECT TOP 1 c.LastName FROM Contact c where c.LastName is Not NULL and c.LastName not like '%[0-9]%' ORDER BY NEWID()"
  Log.Message(aQuery)
  columnName = "LastName" 
  #Project.Variables.AddVariable("lastName","string")
  lastName = RunSingleQuery(aQuery,columnName)
  Log.Message("Last Name = "+lastName)
  return lastName    
     
def RunSingleQuery(aQuery,columnName):
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
 #   dbConnection = DatabaseValue("Ca")  # connectionValues["Server"],    ["Database"] , ["User"], ["Pass"]
#  server_name = dbConnection["Server"]
#    database_name = dbConnection["Database"]
    
  localConfig = GetLocalConfigSettings()
  server_name = localConfig["Server"]
  database_name = localConfig["Database"]
  #computer_name = "ccc7sqltest.checkcity.local\ccc7sql2017"
   
  if(database_name == "UtahTest"):
        pw = "pa$$0ut"
  else:
        pw = "softwise"

          
  Log.Message("Run Single Query = "+aQuery)
  AConnection = ADO.CreateADOConnection()
    # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+""
  Log.Message("Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+"")
    # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
    # Iterate through query results and insert data into the test log
  
  if(RecSet.EOF != True):
      RecSet.MoveFirst()
  
      while not RecSet.EOF:
        returnName = RecSet.Fields.Item[columnName].Value
        RecSet.MoveNext()
  else:
        returnName = "Empty"
  
  
  AConnection.Close()
  varType = aqObject.GetVarType(returnName)
  if varType == 0:
      returnName = "Empty"
  Log.Message(returnName)
  return returnName

       
def AddCustomer():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer   
    

def Test1():
  cashwise = Sys.Process("Cashwise")
  button = cashwise.Window("TMessageForm", "Confirm").VCLObject("Yes")
  aqObject.CheckProperty(button, "Exists", cmpEqual, True)
  button.ClickButton()
  cashwise.VCLObject("frmPhoneTypeBrowse").VCLObject("btnSave").Click(58, 9)

def Test2():
  Sys.Process("Cashwise").Window("TMessageForm", "Confirm").VCLObject("Yes").ClickButton()

def Test3():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmCustomerEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlBasic").VCLObject("pnlPhone").VCLObject("edPrimaryPhoneTypeID").Window("TCustomDBEdit", "HOM").Keys("[F9]")
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("pnlSearch").VCLObject("isMain").Keys("[F9]")

def Test4():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmPhoneTypeBrowse").VCLObject("btnSave").Click(46, 10)
  cashwise.VCLObject("frmCustomerEdit").VCLObject("btnSave").Click(80, 10)
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("btnSave").Click(58, 11)

def Test5():
  Sys.Process("Cashwise").VCLObject("frmCustomerEdit").MainMenu.Click("[2]|[8]")

def Test6():
  Sys.Process("Cashwise").VCLObject("RavePreviewForm").Close()

def Test7():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("pnlSearch").VCLObject("icMain").VCLObject("TAdvancedSpeedButton").Click(9, 7)
  tfrmAbstractList = cashwise.VCLObject("frmAbstractList")
  browseGrid = tfrmAbstractList.VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.HScroll.Pos = 0
  browseGrid.VScroll.Pos = 2
  browseGrid.Click(32, 155)
  tfrmAbstractList.VCLObject("btnSave").Click(64, 15)
  