﻿
import random
import string
import time
import re
from DatabaseQuery import *
from Helpers import *
from random import *
from ContactHelper import *
from LineOfCreditHelper import *




def Test1():
   accountStatues = GetLocAccountStatus("001-0000999")
   Log.Message(accountStatues)

 
def DictionaryDemo():
  d = Sys.OleObject["Scripting.Dictionary"]

  # Add keys and items.
  d.Add("a", "Alphabet");
  d.Add("b", "Book");
  d.Add("c", "Coffee");

  # Get the item by key.
  s = d.Item["c"];
  Log.Message(s);

  # Assign a new item to the same key.
  d.Item["c"] = "Cookie";
  s = d.Item["c"]; 
  Log.Message(s);

  # Assign a new key to the same item.
  d.Key["c"] = "Co";

  # Remove second pair.
  d.Remove("b");
  if d.Exists("b"):
    Log.Message("The pair exists.") 
  else:
    Log.Message("The pair does not exist.")
    
    
def switch_Num1(argument):
    switcher = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }
    month = switcher.get(argument, "Invalid month")
    return month
    
    
def switchMonthToNumber(monthString):
    switcher = {
        "January": "1",
        "February": "2",
        "March": "3",
        "April": "4",
        "May": "5",
        "June": "6",
        "July": "7",
        "August": "8",
        "September": "9",
        "October": "10",
        "November": "11",
        "December": "12"
        
    }
    month = switcher.get(monthString, "Invalid month")
    return month

    
    
def TestSwitch():
  month = switch_Num2("November")
  Log.Message(month)

def numbers_to_months(argument):
    switcher = {
        1: one,
        2: two,
        3: three,
        4: four,
        5: five,
        6: six,
        7: seven,
        8: eight,
        9: nine,
        10: ten,
        11: eleven,
        12: twelve
    }
    month = switcher.get(argument, "Invalid")
    return month
  
  
def test22():
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickInitialDrawButton()")
  thisURL = cashwise.frmBrowser.url.OleValue
  thisURL = aqString.Replace(thisURL,"\\","")
  thisURL = thisURL + "*"
 
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1)
  ExplicitWait(1)
  panel = page.Window("Chrome_WidgetWin*", "", 1).Page(thisURL)
  #drawButton = page.Find("contentText","attach*Initial Draw Amount",15)
  drawButton = panel.Find("ObjectLabel","attach_moneyInitial Draw Amount",15)
  ExplicitWait(2)
  drawButton.Click()
  Log.Checkpoint("SUCCESS: ClickInitialDrawButton() is successful.")
  Indicator.PopText()

   
def pat23():

  caption = Sys.Process("Cashwise").WinFormsObject("frmBrowser").WndCaption
  EndPos = aqString.GetLength(caption) 
  StartPos = 30 #start  postion
  ExplicitWait(1)
  Res = aqString.Remove(caption, StartPos, EndPos)
  aqString.ListSeparator = "-"
  NumOfWord = aqString.GetListLength(Res)
  database = ""
  for x in range(0,NumOfWord):
      if(x == 3):
        database = aqString.GetListItem(Res, x)
 
  database = aqConvert.VarToStr(database)
  database = aqString.Trim( database, aqString.stAll )
  Log.Message("database is "+database)
  return database

def RemoveReportFromEvent():
  cashwise = Sys.Process("Cashwise")
  CashwiseMainMenu_Reports("Report Events")
  cashwise.frmReportEventList.pnlSearch.isMain.Keys("LCA")
  cashwise.frmReportEventList.btnDetailDelete.Click()
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()
  cashwise.frmReportEventList.btnSave.Click()
  
  
def myran():
  ranNum = randrange(500,2500)
  Log.Message(ranNum)


  
def GetRandomBirthDate():
  locDate = GetLocationDate("001")
  days = IntRandomRange(6480,18720)   #-27692
  days = (days * -1)
  Log.Message("days = "+str(days))
  newDate = aqDateTime.AddDays(locDate,days)
  Log.Message("newDate = "+str(newDate))
  snewDate = aqConvert.DateTimeToFormatStr(newDate,"%m/%d/%Y")
  Log.Message("this b-date is "+snewDate)
  return snewDate

  
  
  
    
def GetLocationDate(locID):
  #locID = "001"
  aQuery = "select System_Date from Location where ID = '"+locID+"'"
  columnName = "System_Date"
  locDate = RunSingleQuery(aQuery,columnName)
  locDate = aqConvert.DateTimeToStr(locDate)
  Log.Message("Date for "+locID+" is "+locDate)  
  return locDate
  
def IntRandomRange(min,max):
  ranNum = randrange(min,max)
  Log.Message(ranNum)
  return ranNum 

  
def AddBankAccountToCustomer():
  
   cusID = "001-0001170"
   loactionID = "001"
   localInfo = GetLocalConfigSettings()
   databaseName = localInfo["Database"]
   serverName = localInfo["Server"]
   SProc = ADO.CreateADOStoredProc()
   SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
   #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
   Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
   Log.Message(Cstring)
   #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
   SProc.ProcedureName = "dbo.AddBankAccountToCustomer"
   
   # Adding a return parameter 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[0].name = "RETURN_VALUE"
   SProc.Parameters.Items[0].DataType = ftInteger 
   SProc.Parameters.Items[0].Direction = pdReturnValue
   SProc.Parameters.Items[0].Value = None
   
   
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[1].name = "@ContactID"
   SProc.Parameters.Items[1].DataType = ftFixedChar
   SProc.Parameters.Items[1].Size = 14
   #SProc.Parameters.Items[1].Direction = pdInput
   SProc.Parameters.Items[1].Value = cusID
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[2].name = "@Location_ID"
   SProc.Parameters.Items[2].DataType = ftFixedChar
   SProc.Parameters.Items[2].Size = 4
   #SProc.Parameters.Items[2].Direction = pdInput
   SProc.Parameters.Items[2].Value = loactionID
   # Adding an out parameter  
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[3].name = "@RoutingAccountNum"
   SProc.Parameters.Items[3].DataType = ftFixedChar
   SProc.Parameters.Items[3].Size = 31
   SProc.Parameters.Items[3].Direction = pdOutput  
   SProc.Parameters.Items[3].Value =  None
    # Running the procedure 
   SProc.ExecProc()
   Log.Message("Bank Account Num is: " + SProc.Parameters.Items(3).Value)
   return SProc.Parameters.Items[3].Value
   
   
def test():
  databaseName = "QA01"
  serverName = "SWDPQABuild01"
  #cstring = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
  cstring = "Provider=SQLNCLI11;Server="+serverName+"; + Database="+databaseName 
  Log.Message(cstring)
  
  
  
  
def AddBankAccountToCustomer2():
  
   cusID = "001-0001124"
   loactionID = "001"
   localInfo = GetLocalConfigSettings()
   databaseName = localInfo["Database"]
   serverName = localInfo["Server"]
   SProc = ADO.CreateADOStoredProc()
   SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
   #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
   Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
   Log.Message(Cstring)
   #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
   SProc.ProcedureName = "dbo.AddBankAccountToCustomer"
   
   # Adding a return parameter 
#   SProc.Parameters.AddParameter()
#   SProc.Parameters.Items[0].name = "RETURN_VALUE"
#   SProc.Parameters.Items[0].DataType = ftInteger 
#   SProc.Parameters.Items[0].Direction = pdReturnValue
#   SProc.Parameters.Items[0].Value = None
   
   
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[0].name = "@ContactID"
   SProc.Parameters.Items[0].DataType = ftFixedChar
   SProc.Parameters.Items[0].Size = 14
   #SProc.Parameters.Items[0].Direction = pdInput
   SProc.Parameters.Items[0].Value = cusID
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[1].name = "@Location_ID"
   SProc.Parameters.Items[1].DataType = ftFixedChar
   SProc.Parameters.Items[1].Size = 4
   #SProc.Parameters.Items[1].Direction = pdInput
   SProc.Parameters.Items[1].Value = loactionID
   # Adding an out parameter  
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[2].name = "@RoutingAccountNum"
   SProc.Parameters.Items[2].DataType = ftFixedChar
   SProc.Parameters.Items[2].Size = 32
   SProc.Parameters.Items[2].Direction = adParamOutput  
   SProc.Parameters.Items[2].Value =  None
    # Running the procedure 
   CreateConnectionSession()
   SProc.ExecProc()
   Log.Message("Bank Account Num is: " + SProc.Parameters.Items[2].Value)
   return SProc.Parameters.Items[2].Value

   
def ExecLOCPaymentPlanSchedule():
  
   cusID = "001-0000924"
   accountID = GetLocAccountNun(cusID)
   loactionID = "001"
   localInfo = GetLocalConfigSettings()
   databaseName = localInfo["Database"]
   serverName = localInfo["Server"]
   SProc = ADO.CreateADOStoredProc()
   SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
   #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
   Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
   Log.Message(Cstring)
   #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
   SProc.ProcedureName = "LOCPercentOfCreditLimit"
   
   # Adding a return parameter 
#   SProc.Parameters.AddParameter()
#   SProc.Parameters.Items[0].name = "RETURN_VALUE"
#   SProc.Parameters.Items[0].DataType = ftInteger 
#   SProc.Parameters.Items[0].Direction = pdReturnValue
#   SProc.Parameters.Items[0].Value = None
   
   
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[0].name = "@AccountID"
   SProc.Parameters.Items[0].DataType = ftFixedChar
   SProc.Parameters.Items[0].Size = 14
   #SProc.Parameters.Items[0].Direction = pdInput
   SProc.Parameters.Items[0].Value = accountID
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[1].name = "@Location_ID"
   SProc.Parameters.Items[1].DataType = ftFixedChar
   SProc.Parameters.Items[1].Size = 4
   #SProc.Parameters.Items[1].Direction = pdInput
   SProc.Parameters.Items[1].Value = loactionID
   # Adding an out parameter  
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[2].name = "@RoutingAccountNum"
   SProc.Parameters.Items[2].DataType = ftFixedChar
   SProc.Parameters.Items[2].Size = 32
   SProc.Parameters.Items[2].Direction = adParamOutput  
   SProc.Parameters.Items[2].Value =  None
    # Running the procedure 
   CreateConnectionSession()
   SProc.ExecProc()
   Log.Message("Bank Account Num is: " + SProc.Parameters.Items[2].Value)
   return SProc.Parameters.Items[2].Value   
   
   
def CreateConnectionSession():
  
   userName = "OWN"
   loactionID = "001"
   localInfo = GetLocalConfigSettings()
   databaseName = localInfo["Database"]
   serverName = localInfo["Server"]
   SProc = ADO.CreateADOStoredProc()
   SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
   #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
   Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
   Log.Message(Cstring)
   #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
   SProc.ProcedureName = "dbo.SessionCreateAPI"
   
   # Adding a return parameter 
#   SProc.Parameters.AddParameter()
#   SProc.Parameters.Items[0].name = "RETURN_VALUE"
#   SProc.Parameters.Items[0].DataType = ftInteger 
#   SProc.Parameters.Items[0].Direction = pdReturnValue
#   SProc.Parameters.Items[0].Value = None
   
   
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[0].name = "@UserName"
   SProc.Parameters.Items[0].DataType = ftFixedChar
   SProc.Parameters.Items[0].Size = 5
   #SProc.Parameters.Items[0].Direction = pdInput
   SProc.Parameters.Items[0].Value = userName
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[1].name = "@LocationID"
   SProc.Parameters.Items[1].DataType = ftFixedChar
   SProc.Parameters.Items[1].Size = 4
   #SProc.Parameters.Items[1].Direction = pdInput
   SProc.Parameters.Items[1].Value = loactionID
   # Adding an out parameter  
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[2].name = "@ComputerName"
   SProc.Parameters.Items[2].DataType = ftFixedChar
   SProc.Parameters.Items[2].Size = 32
   #SProc.Parameters.Items[2].Direction = adParamOutput  
   SProc.Parameters.Items[2].Value =  None
    # Running the procedure 
    
   SProc.ExecProc()
#   Log.Message("Bank Account Num is: " + SProc.Parameters.Items[2].Value)
#   return SProc.Parameters.Items[2].Value


#def test66():
#    cashwise = Sys.Process("Cashwise")
#    pageOpen = False
# 
#    while pageOpen == False:
#      aqUtils.Delay(500) 
#      if(cashwise.WaitWindow("WindowsForms10.Window.8.app.0.297b065_r72_ad1","Cashwise*",1,800).Exists):
#          pageOpen = True
#      else:
#          pageOpen = False
#  #      #pageOpen = page = cashwise.frmBrowser.WinFormsObject("pnlBrowser").Enabled
#  #      
#    
#  
#    Log.Message("up to here")
  
def test67():
    cashwise = Sys.Process("Cashwise")
    if(cashwise.frmContactEmployerList.Exists):
       Sys.Process("Cashwise").VCLObject("frmContactEmployerList").VCLObject("btnSave").Click()
    else:
     if(cashwise.WaitWindow("TfrmContactEmployerList","Contact Employers",1,1200).Exists == False):
           Sys.Process("Cashwise").VCLObject("frmContactEmployerList").VCLObject("btnSave").Click()
      
  
  
def test68():
  cashwise = Sys.Process("Cashwise")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
 # widget = page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1)
  drawButton = page.Find("ContentText","Draw*",25)  
  drawButton.Click()
  
  
  
  
  
 


    

def Test2():
    cashwise = Sys.Process("Cashwise")
    cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod.Window("TGroupButton", "Bi-Weekly", 4).ClickButton()
    dayOfWeekPaid = GetRadomDayOfWeek()
    biweekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly
    if dayOfWeekPaid == "Monday":
      biweekly.cbPaydayMonday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Tuesday":
      biweekly.cbPaydayTuesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Wednesday":
      biweekly.cbPaydayWednesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Thursday":
      biweekly.cbPaydayThursday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Friday":
      biweekly.cbPaydayFriday.ClickButton(cbChecked)
    
    biweekly.btnBiweeklySelect.Click()
    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
    cashwise.frmContactBiweeklyEdit.btnSave.Click()
    
     
    ExplicitWait(3)
    biWeekly.Click(19,16)
    ExplicitWait(3)
    radioButton.ClickButton()
    radioButton.Keys("[F9]")
    
    
def WaitLOCBrowser():
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  

      se = Sys.Process("Cashwise")
      pageOpen = False
      while pageOpen == False:
        aqUtils.Delay(500) 
        if(Sys.Process("Cashwise").WaitWindow("WindowsForms10.Window.8.app.0.297b065*", "Cashwise*", 1, 800).Exists):
            ExplicitWait(1)
            pageOpen = True
            Log.Message("Cashwise Browser is up")
        else:                       
            pageOpen = False
      pageOpen = False  
      while pageOpen == False:
        aqUtils.Delay(5000) 
        #page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
        #if(page.Find("contentText","*Draw*",15).Exists):
       
        #if(cashwise.frmBrowser.WinFormsObject("pnlBrowser").Find("contentText","*Draw*",15).Exists):
        if(cashwise.frmBrowser.Find("contentText","*Draw*",15).Exists):
           pageOpen = True
        else:
            pageOpen = False
      count = 0
      pageOpen = False
      while pageOpen == False:
        aqUtils.Delay(100)   
        #page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
        #if(page.Find("contentText","Last Statement Date:",25).Exists):
        count = count +1
        if(cashwise.frmBrowser.WinFormsObject("pnlBrowser").Find("contentText","Last Statement Date:",25).Exists):
          pageOpen = True
        else:
          pageOpen = False
        if count == 500: # if test complete cannot see the buttons close cashwise and reopen
           cashwise.frmBrowser.Close()
           cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
           cashwise.frmSaleEdit.btnClose.Click()
           cashwise.frmMain.Close()
           #TestedApps.Cashwise.Close()
           ExplicitWait(5)
           TestedApps.Cashwise.Run()
           managerUser = GetManager002()
           StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])  
           Sys.Process("Cashwise").VCLObject("frmMain").VCLObject("TGraphicButton").Click()
           if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
              cashwise.frmCashOpenConfirm.Button1.Click()
           #cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("Name","*Line of Credit*",15).Click()
           cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("WndCaption","*Line of Credit*",15).Click()
           ExplicitWait(1)
           cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(customer["LastName"] + ", " + customer["FirstName"])
           ExplicitWait(3)
           cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[Enter]")
  
def CheckPDF():
    if Sys.Process("AcroRd32").Exists:
      Log.Message("Can View PDF in Cashwise")
    else:
      Log.Message("===--Can not View PDF--------====")
    cap = Sys.Process("AcroRd32", 2).Window("AcrobatSDIWindow").WndCaption
    Log.Message(cap)
    Sys.Process("AcroRd32",2).Window("AcrobatSDIWindow").Close()
    # cap == 'August2010Statement_001-0001137.pdf - Adobe Acrobat Reader DC'
    capLength = aqString.GetLength(cap)
    startPDF = aqString.Find(cap,".pdf",0,False)
    name = aqString.SubString(cap,0,startPDF)
    name =  aqString.Replace(name, "'", "")
    Log.Message(name)
      
def GetMonthFromString(): # On Calendar after rollforward, get date on Calendar 
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  
    writtenDate = "'Wednesday, August 11, 2010'"
    prevSep = aqString.ListSeparator
    aqString.ListSeparator = ", "
    month = aqString.GetListItem(writtenDate, 1)
    monthLength = aqString.GetLength(month)
    regEx = "([a-zA-Z])"
    Matches = re.findall(regEx,month)
    if Matches:
      ResStr = ""
      for i in Matches:
        ResStr=ResStr + str(i) 
    Log.Message("extracted = "+ResStr)
    return ResStr


  
  
def TestPopUop():
  button = "Yes"
  cashwise = Sys.Process("Cashwise")
  webPage = "https://qa-loc-1.softwise.com*"
  
  page = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser")
  overlay =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage).Panel(2).Panel(1).Panel("cdk_overlay_1")
 
  if(button == "No"):
    cancelAutoPayButton = overlay.Find("contentText","Cancel",10)
    ExplicitWait(1)
    cancelAutoPayButton.Click()
  if(button == "Yes"):
    yesAutoPayButton = page.Find("ObjectLabel","checkYes",15)
    yesAutoPayButton.Click()
    #overlay2 =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage).Panel(2).Panel(1).Panel("cdk_overlay_2")
    overlay2 =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage)
    verifyButton = overlay2.Find("ObjectLabel","checkVerify",10)
    verifyButton.Click()
    ExplicitWait(1)
    overlay2.Click() # clicks next autopay date
  ExplicitWait(1)  
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
            cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
              cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
              cashwise.RavePreviewForm.Keys("~fx")



  

def CalendarWait():
    cashwise = Sys.Process("Cashwise")
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Please confirm the location date..."):
      ExplicitWait(2)

      activeWindow = Sys.Desktop.ActiveWindow()


def SelectDatabase():
 
  database = "QA01"
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectDatabase(database)")
  databaseList = cashwise.Window("TfrmAliasBrowse", "Database Aliases", 1)
  listBox = databaseList.Find("Name", "Window(\"TListBox\", \"\", 1)",10)
  listBox.ClickItem(database)

def test55():
  cashwise = Sys.Process("Cashwise")
  cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("WndCaption","*Line of Credit*",20).Click()
  

  

#
#def CloseAllLOCAccounts():
#
#    cashwise = Sys.Process("Cashwise")
#    locID = "001"
#    columnName = "Count"
#    columnName2 = "ID"
#    
#    qQuery = "Select COUNT(*) Count from LOCAccount where Location_ID = '"+locID+"' and Status_ID <> 'CLO'"
# 
#    count = runSingleQuery(qQuery,columnName)
# # locAccts = new Array(count)
#    if(count > 0):
#    
#        aQuery2 = "Select ID from LOCAccount where Location_ID = '"+locID+"' and Status_ID <> 'CLO'"
#        locAccts = runArrayQuery(aQuery2,columnName2)
# 
#        Log.Message(locAccts[0])
#        Log.Message(locAccts[1])
# 
#        CashwiseMainMenu_Sales("Line of Credit Manager")  
#        Sleep(200)
#  
#        for x in range(count):
#                 cashwise.frmLOCManager.pnlSearch.isMain.Keys(locAccts[i])
#                 cashwise.frmLOCManager.pnlSearch.isMain.Keys("[Tab]")
#                 aqUtils.Delay(500)
#                 cashwise.frmLOCManager.btnCloseAccount.Click()
#                 aqUtils.Delay(1000)
#                 if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
#                    Log.Message("Confirm_Yes()")
#                 aqUtils.Delay(500)
#                 cashwise.frmLOCCloseAccountEdit.pnlMain.sbData.dbleReasonID.TAdvancedSpeedButton.Click()
#                 cashwise.frmReversalReasonBrowse.btnSave.Click()
#                 cashwise.frmLOCCloseAccountEdit.btnSave.Click()  #-----------------This button will close the account
#                 cashwise.frmLOCManager.pnlSearch.isMain.Keys("[Home]![End][Del]")	
#            
#     cashwise.frmLOCManager.btnSave.Click()
#
##-------------------------------------------------------------------------------------------------
#  
#
#
def CheckForOptionalPayAuth():
  testPassed = False  
  Indicator.PushText("In CheckForOptionalPayAuth")
  cashFilePath  = "C:\\temp\\OptionalPaymentAuth.txt"
  cashwise = Sys.Process("Cashwise")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists == False):
    return testPassed  #----testPassed = False returned
  elif(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists):
        cashwise.RavePreviewForm.TImage.Keys("~fs")
  ExplicitWait(1)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys(cashFilePath)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys("[Tab]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down][Down][Down][Down]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down][Down]")
  cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  if(cashwise.WaitWindow("#32770", "Save File", 1,500).Exists):
        cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  cashwise.RavePreviewForm.TImage.Keys("~fx")
  reportSubString = "Optional      Payment      Authorization"
  testPassed = CheckForFile(cashFilePath,reportSubString)
  if testPassed == True:
          Log.Message("CheckForOptionalPayAuth PASSED")
  return testPassed




def Test3():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").MainMenu.Click("Window|1 Sales Transaction")
  cashwise.VCLObject("frmSaleEdit").Click(320, 25)
