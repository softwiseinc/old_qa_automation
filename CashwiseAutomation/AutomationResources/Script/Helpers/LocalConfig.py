﻿# Items in this file are use as local configuration variable.  
# This file is meant to change often 
# Changes based on who or where test are being run.

def TestConfiguration():
    #########################################
    # Comment only one item is the list below
    #########################################
    
    testDatabase = "QA01"
    #testDatabase = "QA03"
    #testDatabase = "Utah3"
    #testDatabase = "UtahTest"
    
    #########################################
    # Comment only one item is the list above
    #########################################
    
  
  
    config = {}
          
    if(testDatabase == "QA01"):
      config["Server"] = "swdpqabuild01"
      config["Database"] = testDatabase
      config["DataBaseUser"] = "sa"
      config["DataBasePassword"] = "softwise"
      config["LoginUser"] = "man"
      config["LoginPassword"] = "man"
      config["StoreLocation"] = "001"
      config["EmailDomainName"] = "msgnext.com"
    
    if(testDatabase == "QA03"):
      config["Server"] = "swdpqabuild01"
      config["Database"] = testDatabase
      config["DataBaseUser"] = "sa"
      config["DataBasePassword"] = "softwise"
      config["LoginUser"] = "man"
      config["LoginPassword"] = "man"
      config["StoreLocation"] = "001"
      config["EmailDomainName"] = "nostromorp.com"
        
    if(testDatabase == "Utah3"):
      #config["Server"] = "10.127.77.100"
      config["Server"] = "ccqastgsql1.checkcity.local"
      config["Database"] = testDatabase
      config["DataBaseUser"] = "sa"
      config["DataBasePassword"] = "pa$$0ut"
      config["LoginUser"] = "own"
      config["LoginPassword"] = "own"    
      config["StoreLocation"] = "001"  

    if(testDatabase == "UtahTest"):
      config["Server"] = "ccc7sqltest.checkcity.local\ccc7sql2017"
      config["Database"] = testDatabase
      config["DataBaseUser"] = "sa"
      config["DataBasePassword"] = "pa$$0ut"
      config["LoginUser"] = "own"
      config["LoginPassword"] = "own"                
      config["StoreLocation"] = "001"
    
    
    return config
    
  
def DatabaseValue(setting):
      # Ca = AppIntergration UtahTest
      # Cb = AppIntergration for Utah3
      # setting = "Ca"
      connectionValues = {}
      connectionValues["Server"] = ""
      connectionValues["Database"] = ""
      connectionValues["User"] = ""
      connectionValues["Pass"] = ""
      
      if(setting == "Ca"):
        #connectionValues["Server"] = "10.127.192.99"  ccc7sqltest.checkcity.local\ccc7sql2017
        connectionValues["Server"] = "ccc7sqltest.checkcity.local\ccc7sql2017"
        connectionValues["Database"] = "UtahTest"
        connectionValues["User"] = "sa"
        connectionValues["Pass"] = "pa$$0ut"
    
      if(setting == "Cb"):
          connectionValues["Server"] = "10.127.77.100"
          connectionValues["Database"] = "Utah3"
          connectionValues["User"] = "sa"
          connectionValues["Pass"] = "pa$$0ut"

      if(setting == "Cc"):
         # connectionValues["Server"] = "10.1.43.86"
          connectionValues["Server"] = "swdpqabuild01"
          connectionValues["Database"] = "Qa01"
          connectionValues["User"] = "sa"
          connectionValues["Pass"] = "softwise"
          
      return connectionValues 
    
    

def GetLocalConfigSettings():
    #Author Pat Holman 5/7/2019
    #Last Modified by Phil Ivey 8/16/2019
    return(TestConfiguration())
  
def GetLocalConfigSettingsDataBase():
      #Author Pat Holman 5/7/2019
      #Last Modified by Phil Ivey 8/16/2020
     # WriteServer("swdpqabuild01")r("UtahTest")   
      #WriteDatabase("QA01")
      server = GetServer() 
      dataBase = GetDatabase()                  
      localConfig = {}
      localConfig["Database"] = dataBase
     # dataBase =  localConfig["Database"]
      #localConfig["Database"] = "UtahTest"-------
      #localConfig["Database"] = "SmokeFiveOneCurrent"
      localConfig["Server"]    = server
      theServer = localConfig["Server"] 
      Log.Message("this is the server "+theServer)
      Log.Checkpoint("SUCCESS: GetLocalConfigSettings() is successful.")
      return localConfig
  
def GetTestResultContainer():
    #Author Phil Ivey 01/03/2020
    #Last Modified by Phil Ivey 01/03/2020
    testResults = {}
    testResults["Results1"] = "Name1 Results"
    testResults["Results2"] = "Name2 Results"
    testResults["Results3"] = "Name3 Results"
    return testResults

def GetServer():
    #f = open("F:\\Temp\\TestServer.txt")
    f = open("..\\TestServer.txt") # same directory as project
    server = f.read()
    f.close()
    return server
    
def WriteServer(sName):
    #f = open("F:\\Temp\\TestServer.txt")
    f = open("..\\TestServer.txt","w") # same directory as project
    f.write(sName)
    Delay(100)
    f.close()

def GetDatabase():
    #f = open("F:\\Temp\\TestServer.txt")
    f = open("..\\TestDbase.txt") # same directory as project
    dBase = f.read()
    f.close()
    return dBase
    
def WriteDatabase(dName):
    #f = open("F:\\Temp\\TestServer.txt")
    f = open("..\\TestDbase.txt","w") # same directory as project
    f.write(dName)
    Delay(100)
    f.close()