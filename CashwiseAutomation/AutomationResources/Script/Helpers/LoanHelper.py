﻿from BasePage import *

def SelectLoanTypeCategory():
  # edited Phil 8/16/2019 if LOC does not exists add it
  cashwise = Sys.Process("Cashwise")
  cashwise.frmDynamicEdit.pnlMain.sbData.dbleCategory_ID.TAdvancedSpeedButton.Click(11, 4)
  cashwise.frmLoanTypeCategoryBrowse.pnlSearch.isMain.Keys("LOC")
  ExplicitWait(3)
  cashwise.frmLoanTypeCategoryBrowse.pnlSearch.isMain.Keys("[F9]")
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,500).Exists):
    cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
    cashwise.frmLoanTypeCategoryBrowse.btnAdd.Click()
    cashwise.frmDynamicEdit_1.pnlMain.sbData.dbeDescription.Keys("Line of Credit")
    cashwise.frmDynamicEdit_1.btnSave.Click()
    locLimit = randrange(500,200)
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDeferredLimit.Keys(locLimit)
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDeferredCount.Keys("1")
    cashwise.frmDynamicEdit.btnSave.Click()
    cashwise.frmCustomerLimitEdit.btnSave.Click()
    
    

def PerformSecurityOveride(userName):
  cashwise = Sys.Process("Cashwise")
  overideForm = cashwise.frmSecurityOverride.pnlMain.sbData.edPassword
  overideForm.SetText(userName)
  overideForm.Keys("[F9]")
  
  
def TitleCollateralAdd():
  cashwise = Sys.Process("Cashwise")
  pageControl = cashwise.frmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1
  titleDetails = pageControl.tsTitleDetails

  titleDetails.edMake.Keys("Honda")
  titleDetails.edModel.Keys("Civic")
  titleDetails.edVIN.Keys("1233d321485d221d")
  titleDetails.edHiValue.Keys("17000")
  titleDetails.edLowValue.Keys("11000")
  titleDetails.edTradeValue.Keys("12000")
  titleDetails.edYear.Keys("2018")
  titleDetails.edPolicyNumber.Keys("154887")
  titleDetails.edColor.Keys("Blue")
  titleDetails.edOdometer.Keys("15545")
  titleDetails.edLicenseNumber.Keys("s454s")
  titleDetails.edDescription.Keys("4 door")
  titleDetails.mNotes.Keys("OK")
  titleDetails.edState.Keys("UT")
  titleDetails.edLicenseExpirationDate.TAdvancedSpeedButton.Click()
  cashwise.frmCalendarEdit.Panel1.btnNextYear.Click()
  cashwise.frmCalendarEdit.btnSave.Click()
  titleDetails.edCondition.TAdvancedSpeedButton.Click()
  
  condition = cashwise.frmCollateralConditionBrowse
  condition.pnlSearch.isMain.Keys("Fair")
  condition.pnlSearch.isMain.Keys("[Tab]")
  
  if(condition.btnEdit.Enabled == True):
    condition.btnSave.Click()
  else:
    condition.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
    
  titleDetails.edStyle.TAdvancedSpeedButton.Click()
  
  style = cashwise.frmCollateralStyleBrowse
  style.pnlSearch.isMain.Keys("4 door")
  
  if(style.btnEdit.Enabled == True):
    style.btnSave.Click()
  else:
    style.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
  titleDetails.edEvaluationDate.TAdvancedSpeedButton.Click()
  cashwise.frmCalendarEdit.btnSave.Click()
  
 # titleDetails.dbeMonthlyDisposableIncome.Keys("250")
  #titleDetails.edAdvance.Keys("5000")
  
  pageControl.ClickTab("Insurance Information")
  
  zip = GetRandomZipA()
  state = GetStateFromZip(zip)
  city = GetCityFromZip(zip)
  address = GetAddressFromCity(city)
  
  insureInfo = cashwise.frmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1.tsInsuranceInfo
  insureInfo.edInsuranceCompany.Keys("AllState")
  insureInfo.edInsAddressLine1.Keys(address)
  insureInfo.edInsCity.Keys(city)
  insureInfo.edInsState.Keys(state)
  insureInfo.edInsZip.Keys(zip)
  
  ExplicitWait(1)
  pageControl.ClickTab("Title Details")
  cashwise.frmDeferredCollateralTitleEdit.btnSave.Click()
  
  
#put get teletrack single pay loan type here
def GetSinglePayTeletrackLoanType():
     #Author Phil Ivey 08/01/2020
  #Last Modified by 
  aQuery = " Select ID from DeferredTypeView " +\
  "where ReportToDecisionProc is not NULL and RegisterTeleTrack = 1 and RegisterTeletrackWithoutTransaction = 1 and LoanModule_ID = 'DEF' "

  columnName = "ID"
  Log.Message(aQuery)
  loanType  = RunSingleQuery(aQuery,columnName)
  Log.Message("Single Pay Teletrack loan type  is "+loanType)
  return loanType
   
def GetInstallmentTeletrackLoanType():
     #Author Phil Ivey 08/01/2020
  #Last Modified by 
  aQuery = " Select ID from DeferredTypeView " +\
  "where ReportToDecisionProc is not NULL and RegisterTeleTrack = 1 and RegisterTeletrackWithoutTransaction = 1 and LoanModule_ID = 'INS' "

  columnName = "ID"
  Log.Message(aQuery)
  loanType  = RunSingleQuery(aQuery,columnName)
  Log.Message("Single Pay Teletrack loan type  is "+loanType)
  return loanType  
  
def AddSinglePayTeletrackLoanType():
       #Author Phil Ivey 08/01/2020
  #Last Modified by 
  newLoanType = "TS3"
  newLoanTypeDescription = "Teletrack SinglePay Three"
  decisionProc = "DecideReportLoanToTeletrack"
  cashwise = Sys.Process("Cashwise")
  CashwisaMainMenu_File("Setup")
  SystemSetupTree("Loan Types")
  cashwise.frmDeferredTypeSetup.btnAdd.Click()
#  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
#     cashwise.Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
  cashwise.frmDeferredLoanTypeAdd.btnSave.Click()  # SINGLE PAYMENT IS ALREADY HIGHLIGHTED
  cashwise.frmDeferredLoanTypeEdit.pnlMain.sbData.dbedID.Keys(newLoanType)
  cashwise.frmDeferredLoanTypeEdit.pnlMain.sbData.dbedDescription.Keys(newLoanTypeDescription)
  cashwise.frmDeferredLoanTypeEdit.btnSave.Click()
  Delay(3000)
  
  pControl = cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings
                    
  pControl.tsGeneral.cdRegisterTeleTrack.ClickButton(cbChecked)
  pControl.tsGeneral.dbcRegisterTeletrackWithoutTransaction.ClickButton(cbChecked)
  pControl.tsGeneral.edReportToDecisionProc.Keys(decisionProc)
  
  pControl.ClickTab("&2 Limits")
  pControl.tsLimits.dbckbPromptForAge.ClickButton(cbChecked)
  
  pControl.ClickTab("&3 Duration")
  pControl.tsDuration.pnlDurationLeft.gbDelayedFunding.dbcFundingDateEditable.ClickButton(cbChecked)
  pControl.tsDuration.pnlDurationLeft.edtDuration.Keys(7)
  
  pControl.ClickTab("&4 Collateral")
  pControl.tsCollateral.dbckbNewCollateralWF.ClickButton(cbChecked)
  pControl.tsCollateral.TfraDeferredTypeCollateral1.rgCheckDateCode.Window("TGroupButton", "Follows Due Date", 1).ClickButton(cbChecked)
  
  pControl.ClickTab("&5 Fees")
  pControl.tsFees.TfraDeferredTypeFeeType1.btnAdd.Click()
  grid = cashwise.frmDeferredFeeTypeList.pnlMain.dbgMain
  grid.Keys("[Home]")
  for x in range(12):
    grid.Keys("[Down]")
  cashwise.frmDeferredFeeTypeList.btnSave.Click()
  
  pControl.ClickTab("&8 Extensions")
  pControl.tsExtension.TfraDeferredTypeExtension1.gbFreeExtension.DBCheckBox1.ClickButton(cbUnchecked)
  pControl.tsExtension.TfraDeferredTypeExtension1.gbFreeExtension.DBCheckBox2.ClickButton(cbUnchecked)
  pControl.tsExtension.TfraDeferredTypeExtension1.cbSettleFees.ClickButton(cbChecked)
  
  #-- CLOSE LOAN TYPE
  cashwise.frmDeferredSetup.btnSave.Click()
  # -- CLOSE SETUP
  cashwise.frmSystemSetup.btnSave.Click()
  
  return newLoanType
  
def AddInstallmentTeletrackLoanType():
         #Author Phil Ivey 08/04/2020
    #Last Modified by 
    newLoanType = "TI3"
    newLoanTypeDescription = "Teletrack Install Three"
    decisionProc = "DecideReportLoanToTeletrack"
    cashwise = Sys.Process("Cashwise")
    CashwisaMainMenu_File("Setup")
    SystemSetupTree("Loan Types")
    cashwise.frmDeferredTypeSetup.btnAdd.Click()
  #  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
  #     cashwise.Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
    browseGrid = cashwise.frmDeferredLoanTypeAdd.pnlMain.dbgMain
    browseGrid.VScroll.Pos = 0
    browseGrid.Click(86, 42)
    cashwise.frmDeferredLoanTypeAdd.pnlMain.dbgMain.Keys("[Down") #ONE DOWN FOR INSTLLMENT LOAN
   # cashwise.frmDeferredLoanTypeAdd.pnlMain.dbgMain.Cli
    Delay(500)
    cashwise.frmDeferredLoanTypeAdd.btnSave.Click()  #
    cashwise.frmDeferredLoanTypeEdit.pnlMain.sbData.dbedID.Keys(newLoanType)
    cashwise.frmDeferredLoanTypeEdit.pnlMain.sbData.dbedDescription.Keys(newLoanTypeDescription)
    cashwise.frmDeferredLoanTypeEdit.btnSave.Click()
    Delay(3000)
    
    pControl = cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings
    
    pControl.tsGeneral.cdPickupDefault.ClickButton(cbChecked)                  
    pControl.tsGeneral.cdRegisterTeleTrack.ClickButton(cbChecked)
    pControl.tsGeneral.dbcRegisterTeletrackWithoutTransaction.ClickButton(cbChecked)
    pControl.tsGeneral.edReportToDecisionProc.Keys(decisionProc)
    
    pControl.ClickTab("&2 Limits") #-------------------------------------------TAB 2
    pControl.tsLimits.dbckbPromptForAge.ClickButton(cbChecked)
    
    pControl.ClickTab("&3 Duration") #-------------------------------------------TAB 3
    pControl.tsDuration.pnlDurationLeft.gbDelayedFunding.dbcFundingDateEditable.ClickButton(cbChecked)
    pControl.tsDuration.pnlDurationLeft.edtDuration.Keys(7)
    
    # ADD COLLATERAL TYPES
    pControl.ClickTab("&4 Collateral") #-------------------------------------------TAB 4
    pControl.tsCollateral.TfraDeferredTypeCollateral1.AdvancedSpeedButton2.Click() # ADD COLLATERAL 1
    cashwise.frmDeferredCollateralTypeList.btnSave.Click() #  ACH ON TOP JUST SAVE AND CLOSE
   
    pControl.tsCollateral.TfraDeferredTypeCollateral1.AdvancedSpeedButton2.Click() # ADD COLLATERAL 2
    cashwise.frmDeferredCollateralTypeList.pnlMain.dbgMain.Keys("Down]")
    cashwise.frmDeferredCollateralTypeList.btnSave.Click()  #  BANK CARD==========
    
    pControl.tsCollateral.TfraDeferredTypeCollateral1.AdvancedSpeedButton2.Click() # ADD COLLATERAL 3
    cashwise.frmDeferredCollateralTypeList.pnlMain.dbgMain.Keys("[Down][Down")
    cashwise.frmDeferredCollateralTypeList.btnSave.Click()  #  CHECK ==========
    
    #COLLATERAL PROTOCOL
    cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings.tsCollateral.TfraDeferredTypeCollateral1.edCollateralValidationProtocol.Keys("GRA")
    # NEW WORK FLOW
    cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings.tsCollateral.dbckbCollateralPlusDEFirst.ClickButton(cbChecked)
    pControl.tsCollateral.TfraDeferredTypeCollateral1.rgCheckDateCode.Window("TGroupButton", "Follows Due Date").ClickButton()
    #pageControl.VCLObject("tsCollateral").VCLObject("TfraDeferredTypeCollateral1").VCLObject("rgCheckDateCode").Window("TGroupButton", "Follows Due Date").ClickButton()
    
  # FOLLOWS DUE DATE
    cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings.tsCollateral.TfraDeferredTypeCollateral1.rgCheckDateCode.Window("TGroupButton", "Follows Due Date", 1).Click()
  
    pControl.ClickTab("&5 Installment Loan") #-------------------------------------------TAB 5 INSTALLMENT
    pControl.tsInstallmentLoan.fraDeferredTypeInstallmentLoan1.dblefIntervalType.Keys("W") #--PERIOD TYPE
    pControl.tsInstallmentLoan.fraDeferredTypeInstallmentLoan1.dbckbExcessToFuture.ClickButton(cbUnchecked)
    
    pControl.ClickTab("&6 Fees") #-------------------------------------------TAB 5 FEES
    pControl.tsInstallmentLoanFees.fraInstallmentLoanTypeFeeType1.btnFeeAdd.Click() # ADD FEE TYPE TO LOAN
    cashwise.frmInstallmentLoanFeeTypeBrowse.pnlSearch.isMain.Keys("WIF") #  ENTER FEE TYPE ID
    cashwise.frmInstallmentLoanFeeTypeBrowse.btnAdd.Click() # CLICK THE ADD BUTTON
    isThere =  cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dbedCode.wText
    if(isThere == "WIF"):
      Log.Message("Will have to add WIF fee type")
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dbedDescription.Keys("Weekly Installment Fee")
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dblefAmountType.Keys("INT")
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dblefIntervalType.Keys("W")
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dbrgFlatOrRate.Window("TGroupButton", "Rate", 1).Click()
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.pnlRateType.dblefRateType.Keys("PPR")
      cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dbedAmount.Keys("10")
      browseGrid = cashwise.frmInstallmentLoanFeeTypeEdit.pnlMain.sbData.pnlAmountTypes.bgAmountTypes
      browseGrid.VScroll.Pos = 6
      browseGrid.Click(284, 116)
      cashwise.frmInstallmentLoanFeeTypeEdit.btnSave.Click()
    else:
      cashwise.frmInstallmentLoanFeeTypeEdit.btnClose.Click()
      cashwise.frmInstallmentLoanFeeTypeBrowse.pnlSearch.isMain.Keys("WIF")
      
    cashwise.frmInstallmentLoanFeeTypeBrowse.btnSave.Click()  # CLOSE INSTALLMENT LOAN FEE TYPES
    
    pControl.ClickTab("&7 Payment")
    pageControl2 = pControl.VCLObject("tsInstallmentPayment").VCLObject("GroupBox1").VCLObject("pgACH")
    pageControl2.ClickTab("Non ACH Payment Settings")
    pageControl2.ClickTab("AutoPay Logic")
    #pageControl2.tsPaymentLogic.dbckbNewAutoPayWF.
    cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings.tsInstallmentPayment.GroupBox1.pgACH.tsPaymentLogic.dbckbNewAutoPayWF.Click()
    #CLOSE LOAN TYPE EDIT
    cashwise.frmDeferredSetup.btnSave.Click()
    #CLOSE SETUP
    cashwise.frmSystemSetup.btnSave.Click()
    Delay(200)
    return newLoanType
  
  
def OpenPOS():
#Author Phil Ivey 08/01/2020
  #Last Modified by 
    cashwise = Sys.Process("Cashwise")
    cashwise.frmMain.TGraphicButton.Click()
    if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,1000).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,2000).Exists):
      cashwise.VCLObject("frmCashOpenConfirm").VCLObject("Button1").Click()

  
def StartNewCashAdvanceInPOS(cusID,teleTrackLoanType,loanAmount,collatr):  #  collate=ACH
 #Author Phil Ivey 08/03/2020
  #Last Modified by  
#    cusName = "Chaney, Amy W"
#    teleTrackLoanType = "TsN"
    Log.Message("Loan type is "+teleTrackLoanType)
    loanAmount = aqConvert.IntToStr(loanAmount)
    cashwise = Sys.Process("Cashwise")
    cusName = GetCusNameFromID(cusID)
   # cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Window("TAdvancedButton", "&0 Cash Advance", 26)
    buttons =  cashwise.frmSaleEdit.pnlMain.pnlPOSButtons
    buttons.Find("RecordCaption","&1 Cash Advance",20).Click()
#     VCLClass    TAdvancedButton
#           cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("WndCaption","*Line of Credit*",15).Click()
    Delay(500)
    searchBy =  cashwise.frmCustomerBrowse.pnlSearch.icMain.Window("TEdit", "*", 1).wText
    if(searchBy == "ID"):
      cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(cusID)
    else:
      cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(cusName)
      
    cashwise.frmCustomerBrowse.btnSave.Click()
    
    # if warning
    
    if(cashwise.WaitWindow("TfrmWarningDisplayNew","Warnings",1,1200).Exists):
      cashwise.frmWarningDisplayNew.btnSave.Click()
      
    PendingCashAdvanceSingle("Add New")
    while(cashwise.WaitWindow("TfrmDeferredTypeList","Cash Advance Type List",1,1200).Exists == False):
      Delay(500)
    cashwise.frmDeferredTypeList.pnlSearch.icMain.TAdvancedSpeedButton.Click()
    cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home][Down]")
    cashwise.frmAbstractList.btnSave.Click()
    Delay(1000)
    cashwise.frmDeferredTypeList.pnlSearch.icMain.TAdvancedSpeedButton.Click()
    cashwise.frmAbstractList.btnSave.Click()
    Delay(500)
    browseGrid = cashwise.frmDeferredTypeList.pnlMain.dbgMain
    browseGrid.HScroll.Pos = 0
    browseGrid.VScroll.Pos = 2
    browseGrid.Click(213, 137)
    cashwise.frmDeferredTypeList.pnlSearch.isMain.Click(54,5)
    Delay(500)
    cashwise.frmDeferredTypeList.pnlSearch.isMain.Keys(teleTrackLoanType)
    Delay(500)
    cashwise.frmDeferredTypeList.btnSave.Click()

   
   
   # IF COLLATERAL FIRST
    Delay(500)
    if(cashwise.WaitWindow("TfrmDeferredTypeCollateralTypeList","Cash Advance Type Collateral Type List",1,2200).Exists):
      cashwise.frmDeferredTypeCollateralTypeList.pnlMain.dbgMain.Keys("[Home]")
      cashwise.frmDeferredTypeCollateralTypeList.btnSave.Click()
      
    if(cashwise.WaitWindow("TfrmDeferredCollateralCheckEdit","Check Collateral Edit",1,1200).Exists):
         cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
         cashwise.frmContactAccountList.btnSave.Click()
      
    if(cashwise.WaitWindow("TfrmDeferredCollateralACHEdit","ACH Collateral Edit",1,1200).Exists):
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edtValue.Keys()
    
    if(cashwise.WaitWindow("TfrmDeferredCollateralCheckEdit","Check Collateral Edit",1,1200).Exists):
         cashwise.frmDeferredCollateralCheckEdit.btnSave.Click()
      
    cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlAmounts.pnlBaseFields.edAdvance.Keys("[Home]![End][Del]")
    cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlAmounts.pnlBaseFields.edAdvance.SetText(loanAmount)
    cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlAmounts.pnlBaseFields.edAdvance.Keys("[Tab]")

    cashwise.frmDeferredNewEdit.btnSave.Click()
    SinglePayCollateral(collatr)#-----------------------------collate=ACH
  #----------------------------------------------------------------------------------------------------------------------------
    
def StartNewInstallmentnPOS(cusJID,teleTrackLoanType,loanAmount,collate):
 #Author Phil Ivey 08/03/2020
  #Last Modified by  
#    cusName = "Chaney, Amy W"
#    teleTrackLoanType = "TsN"
    cusName = GetCusNameFromID(cusJID)
    checkNum = IntRandomRange(99,999)
    loanAmount = aqConvert.IntToStr(loanAmount)
    cashwise = Sys.Process("Cashwise")
   # cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Window("TAdvancedButton", "&0 Cash Advance", 26)
    buttons =  cashwise.frmSaleEdit.pnlMain.pnlPOSButtons
    buttons.Find("RecordCaption","&0 Cash Advance",20).Click()
#     VCLClass    TAdvancedButton
#           cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("WndCaption","*Line of Credit*",15).Click()
    cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(cusName)
    cashwise.frmCustomerBrowse.btnSave.Click()
    PendingCashAdvanceSingle("Add New")
    while(cashwise.WaitWindow("TfrmDeferredTypeList","Cash Advance Type List",1,1200).Exists == False):
      Delay(500)
    cashwise.frmDeferredTypeList.pnlSearch.icMain.TAdvancedSpeedButton.Click()
    cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home][Down]")
    cashwise.frmAbstractList.btnSave.Click()
    cashwise.frmDeferredTypeList.pnlSearch.isMain.Keys(teleTrackLoanType)
    cashwise.frmDeferredTypeList.btnSave.Click()
   # IF COLLATERAL FIRST
   
#    while(cashwise.WaitWindow("TfrmDeferredCollateralC*","*",1,500).Exists == False):
#        Delay(500)
    Delay(1500)   
    if(cashwise.WaitWindow("TfrmDeferredCollateralCheckEdit","Check Collateral Edit",1,120).Exists):
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edNum.Keys(checkNum)
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edCheckAmount.Keys(loanAmount)
      cashwise.frmDeferredCollateralCheckEdit.btnSave.Click()
      cashwise.frmDeferredCollateralList.btnSave.Click()
   
    if(cashwise.WaitWindow("TfrmDeferredTypeCollateralTypeList","Cash Advance Type Collateral Type List",1,1200).Exists):
      cashwise.frmDeferredTypeCollateralTypeList.pnlMain.dbgMain.Keys("[Home]")
      cashwise.frmDeferredTypeCollateralTypeList.btnSave.Click()
      
    if(cashwise.WaitWindow("TfrmDeferredCollateralACHEdit","ACH Collateral Edit",1,1200).Exists):
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edtValue.Keys(loanAmount)
      cashwise.frmDeferredCollateralACHEdit.btnSave.Click()
      cashwise.frmDeferredCollateralList.btnSave.Click()
      
    Delay(2000)
      
    if(cashwise.WaitWindow("TFrmDeferredTypeAutoPay*","Select Auto Pay*",1,1800).Exists):
      Delay(100)
      cashwise.FrmDeferredTypeAutoPayType.btnSave.Click()
      
    Delay(1500)
    
    # NEW INSTALLMENT LOAN PAGE  
    cashwise.WaitWindow("TfrmDeferredNewLoanEdit","New Installment Loan",1,1200)   
    installPayPattern = cashwise.frmDeferredNewLoanEdit.pnlMain.sbData.pnlInstallmentLoan.pnlPaymentInformation.pnlStandardPattern
    
    installPayPattern.dbedLoanPrincipal.Keys(loanAmount)
    installPayPattern.dbedLoanPrincipal.Keys("[Tab")
    Delay(1500)
    installPayPattern.dbedPaymentCount1.Keys("![Tab]")
    installPayPattern.dbedLoanPrincipal.Keys("[Tab")
    installPayPattern.dbedPaymentCount1.Keys("4")
    installPayPattern.dbedPaymentCount1.Keys("[Tab]")
    Delay(1000)
    cashwise.frmDeferredNewLoanEdit.btnSave.Click()
    Delay(500)
    
    # MORE COLLATERAL
    if(cashwise.WaitWindow("TfrmDeferredCollateralCheckEdit","Check Collateral Edit",1,120).Exists):
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edNum.Keys(checkNum)
      cashwise.frmDeferredCollateralList.btnSave.Click()
  #  if(cashwise.WaitWindow("TfrmDeferredCollateralList","Cash Advance Collateral List",1,1500).Exists):
  #   cashwise.frmDeferredCollateralList.btnSave.Click()
    if(cashwise.WaitWindow("TfrmDeferredCollateralTitleEdit","Title Collateral Add",1,1500).Exists):
      TitleCollateralAdd()
  #-------------------------------------------------------------------------------------------------------------------------------  
def PendingCashAdvanceSingle(button):
 #Author Phil Ivey 08/03/2020
  #Last Modified by  
  #button = "Add New"
  cashwise = Sys.Process("Cashwise")    
  deferredPending = cashwise.frmDeferredSelect
  
  if(button == "Add New"):
    deferredPending.btnDeferredAdd.Click()
  elif(button == "Refinance"):
    deferredPending.btnRefinanceLoan.Click()
  elif(button == "Extend"):
    deferredPending.btnDeferredExtend.Click() 
  elif(button == "Hold"):
    deferredPending.btnDeferredFreeExtension.Click()
  elif(button == "Pickup"):
    deferredPending.btnDeferredPickup.Click() 
  elif(button == "Pay Down"):
    deferredPending.btnDeferredPayDown.Click()
  elif(button == "Rescind"):
    deferredPending.btnRescind.Click() 
  elif(button == "Early Payoff Inquiry"):
    deferredPending.btnEarlyPayoff.Click()
  elif(button == "Free Extend"):
    deferredPending.btnDeferredFreeExtend.Click() 
     
    
    
def LoanTypeSelection(type):
       #Author Phil Ivey 08/01/2020
  #Last Modified by 
  #type = "classic"
  cashwise = Sys.Process("Cashwise")
  CashwisaMainMenu_File("Setup")
  scrollBox = cashwise.VCLObject("frmSystemSetup").VCLObject("pnlMain").VCLObject("sbData")
  scrollBox.VCLObject("tvMain").ClickItem("|Sales|Cash Advance Setup|Loan Type Selection")
  pageControl = scrollBox.VCLObject("pnlRight").VCLObject("pnlSetup").VCLObject("PageControl1")
  pageControl.ClickTab("&Location")
  TDBRadioGroup = pageControl.VCLObject("tsLocation").VCLObject("DBRadioGroup1")

  if(type == "classic"):
     TDBRadioGroup.Window("TGroupButton", "Use classic form for this location").ClickButton()  
  elif(type == "tree"):  
    TDBRadioGroup.Window("TGroupButton", "Use tree view form for this location").ClickButton()
  cashwise.frmSystemSetup.btnSave.Click()
    
  
def SinglePayCollateral(collate):#-------------------collate=ACH
  checkNum = str(random.randint(100,999))
  cashwise = Sys.Process("Cashwise")
 
  # CHECK COLLATERAL
  if(cashwise.WaitWindow("TfrmDeferredCollateralCheckEdit","Check Collateral Edit",1,1200).Exists):
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edNum.Keys(checkNum)
      dateThere = cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edDate.Window("TDBDateEdit", "", 1).RecordCaption
      if(dateThere == ""):
        cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edDate.TAdvancedSpeedButton.Click()
        cashwise.frmCalendarEdit.btnSave.Click()
  
  #cash advance type collateral type list  WndClass=TfrmDeferredTypeCollateralTypeList  WndCaption=Cash Advance Type Collateral Type List
  
  # ****************************COLLATERAL AFTER
 
 
  if(cashwise.WaitWindow("TfrmDeferredTypeCollateralTypeList","Cash Advance Type Collateral Type List",1,1200).Exists):
    if(collate =="CHK"):
      Log.Message("Check collateral")
      cashwise.frmDeferredTypeCollateralTypeList.btnSave.Click()      
      #cashwise.frmDeferredCollateralCheckEdit.btnSave.Click()      
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralCheckEdit.pnlMain.sbData.pnlCheckInfo.edNum.Keys(checkNum)
      cashwise.frmDeferredCollateralCheckEdit.btnSave.Click()
    
    
    if(collate == "ACH"):
      cashwise.frmDeferredTypeCollateralTypeList.pnlMain.dbgMain.Keys("[Home][Down]")
      cashwise.frmDeferredTypeCollateralTypeList.btnSave.Click()      
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton.Click()
      cashwise.frmContactAccountList.btnSave.Click()
      cashwise.frmDeferredCollateralACHEdit.btnSave.Click()
      

       
    
    if(collate == "CC"):
      cashwise.frmDeferredTypeCollateralTypeList.pnlMain.dbgMain.Keys("[Home][Down][Down]")
      cashwise.frmDeferredTypeCollateralTypeList.btnSave.Click()
   
  
  if(collate == "TL"):
         TitleCollateralAdd()
          
  
  
  cashwise.frmDeferredCollateralList.btnSave.Click()  
  Delay(1000)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,1200).Exists):    
       Sys.Process("Cashwise").Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()  
  
  
    # ACH COLLATERAL
  if(cashwise.WaitWindow("TfrmDeferredCollateralACHEdit","ACH Collateral Edit",1,1200).Exists):
      cashwise.frmDeferredCollateralACHEdit.pnlMain.sbData.edRoutAcct.TAdvancedSpeedButton
  
  
      # BANK CARD  COLLATERAL
  
  
  
def ProcessTransaction():  
  cashwise = Sys.Process("Cashwise")
  cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlProcessContainer.btnProcess.Click()
  cashwise.frmAliasBrowse.pnlMain.sbData.lbAliases.ClickItem("Cash")
  cashwise.frmAliasBrowse.btnSave.Click()
  cashwise.frmSaleConfirmPayout.btnSave.Click()
  Delay(10000)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):
    cashwise.RavePreviewForm.Keys("~fx")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):
    cashwise.RavePreviewForm.Keys("~fx") 
  cashwise.frmSaleEdit.btnClose.Click()
  
def GetRandomLoanAmount():
     dollar = IntRandomRange(100,999)
     cent = IntRandomRange(1,99)
     dollar = aqConvert.IntToStr(dollar)
     cent = aqConvert.IntToStr(cent)
     amount = dollar + "."+ cent
     amountleng = aqString.GetLength(amount)
     if (amountleng < 6):
      amount = amount + "0"
     
     return amount
  
  
  
  
  
    
  
  
  
  
  