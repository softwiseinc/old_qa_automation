﻿from BasePage import *
from CashHelper import *

def OpenCustomerAccount():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenCustomerAccount()")
  cashwise.frmCustomerEdit.Keys("~[Release]ta")
  #cashwise.frmCustomerEdit.MainMenu.Click("[2]|[1]")
  Log.Checkpoint("SUCCESS: OpenCustomerAccount() is successful.")
  Indicator.PopText()
  
def GetUniqueAccountNumber():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019 
  Indicator.PushText("RUNNING: GetUniqueAccountNumber")
  today =  aqDateTime.Today()
  bit_size = 1
  accountNumber = str(uuid4().int >> bit_size)
  accountNumber = accountNumber[28:]
  Log.Checkpoint("SUCCESS: GetUniqueAccountNumber")
  Indicator.PopText()
  return accountNumber
  
  

def BankClickAddButton():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In BankClickAddButton()")
  cashwise.frmContactAccountList.btnAdd.Click()
  Log.Checkpoint("SUCCESS: BankClickAddButton() is successful.")
  Indicator.PopText()

def OpenBankAccountBrowser():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenBankAccountBrowser()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TAdvancedSpeedButton.Click()
  Log.Checkpoint("SUCCESS: OpenBankAccountBrowser() is successful.")
  Indicator.PopText()

def AddNewAccountNumber():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddNewAccountNumber()")
  accountNumber = "041212815" + GenerateRandomNumberString(10)
  cashwise.frmContactAccountEdit.Click()
  customDBEdit = cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TCustomDBEdit
  ExplicitWait(1)
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.Window("TCustomDBEdit", "", 1).Keys(accountNumber)
  #customDBEdit.Keys(accountNumber)
  Log.Checkpoint("SUCCESS: AddNewAccountNumber() is successful.")
  Indicator.PopText()
  
def AddAccountNumber():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddAccount()")
  accountNumberFirst = "0"
  accountNumberSecond = "41212815" + GenerateRandomNumberString(10)
  routingNumberFirst = "1"
  routingNumberSecond = "24000054"
  routing = routingNumberFirst + routingNumberSecond
  account = accountNumberFirst + accountNumberSecond
  routingField = cashwise.VCLObject("frmBankAccountEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlColumn1").VCLObject("pnlRouting").VCLObject("edRouting").Window("TCustomDBEdit", "", 1)
  routingField.Keys(routingNumberFirst)
  routingEnter = Sys.Process("Cashwise").VCLObject("frmDoubleEnterRoutingNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit1")
  routingEnter.Keys(routingNumberSecond)
  routingReEnter = cashwise.frmDoubleEnterRoutingNumber.pnlMain.sbData.DBEdit2
  routingReEnter.Keys(routing)
  routingSave = cashwise.VCLObject("frmDoubleEnterRoutingNumber").VCLObject("btnSave")
  routingSave.Click()
  accountField = cashwise.frmBankAccountEdit.pnlMain.sbData.pnlFields.pnlColumn1.pnlRouting.edAcctNum
  accountField.Keys(accountNumberFirst)
  accountEnter = cashwise.frmDoubleEnterAccountNumber.pnlMain.sbData.DBEdit1
  accountEnter.Keys(accountNumberSecond)
  accountReEnter = cashwise.VCLObject("frmDoubleEnterAccountNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit2")
  accountReEnter.Keys(account)
  cashwise.frmDoubleEnterAccountNumber.btnSave.Click()
  cashwise.frmBankAccountEdit.btnSave.Click()
  searchAccount = cashwise.frmBankAccountBrowse.pnlSearch.isMain
  searchAccount.Keys(routing + account)
  ExplicitWait(1)  
  Log.Checkpoint("SUCCESS: AddAccount() is successful.")
  Indicator.PopText()
  
def EditDeleteAccountNumber():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditDeleteAccountNumber()")
  cashwise.frmBankAccountBrowse.btnEdit.Click()
  accountNumberFirst = "0"
  accountNumberSecond = "55212315" + GenerateRandomNumberString(10)
  routingNumberFirst = "0"
  routingNumberSecond = "11100106"
  routing = routingNumberFirst + routingNumberSecond
  account = accountNumberFirst + accountNumberSecond
  routingField = cashwise.VCLObject("frmBankAccountEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlColumn1").VCLObject("pnlRouting").VCLObject("edRouting").Window("TCustomDBEdit", "", 1)
  routingField.Keys(routingNumberFirst)
  routingEnter = Sys.Process("Cashwise").VCLObject("frmDoubleEnterRoutingNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit1")
  routingEnter.Keys(routing)
  routingReEnter = cashwise.frmDoubleEnterRoutingNumber.pnlMain.sbData.DBEdit2
  routingReEnter.Keys(routing)
  routingSave = cashwise.VCLObject("frmDoubleEnterRoutingNumber").VCLObject("btnSave")
  routingSave.Click()
  accountField = cashwise.frmBankAccountEdit.pnlMain.sbData.pnlFields.pnlColumn1.pnlRouting.edAcctNum
  accountField.Keys(accountNumberFirst)
  accountEnter = cashwise.frmDoubleEnterAccountNumber.pnlMain.sbData.DBEdit1
  accountEnter.Keys(account)
  accountReEnter = cashwise.VCLObject("frmDoubleEnterAccountNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit2")
  accountReEnter.Keys(account)
  cashwise.frmDoubleEnterAccountNumber.btnSave.Click()
  cashwise.frmBankAccountEdit.btnSave.Click()
  searchAccount = cashwise.frmBankAccountBrowse.pnlSearch.isMain
  searchAccount.Keys(routing + account)
  ExplicitWait(1)  
  cashwise.frmBankAccountBrowse.btnDelete.Click()
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()
  cashwise.frmBankAccountBrowse.btnClose.Click()
  Log.Checkpoint("SUCCESS: EditDeleteAccountNumbert() is successful.")
  Indicator.PopText()

def ClickAddAccountSpeedButton():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickAddAccountSpeedButton()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TAdvancedSpeedButton.Click(8, 8)
  cashwise.frmBankAccountBrowse.btnAdd.Click(27, 22)
  Log.Checkpoint("SUCCESS: ClickAddAccountSpeedButton() is successful.")
  Indicator.PopText()

def SaveNewAccount():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveNewAccount()")
  cashwise.frmBankAccountEdit.pnlMain.sbData.pnlFields.pnlColumn1.pnlRouting.edAcctNum.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SaveNewAccount() is successful.")
  Indicator.PopText()

def SetAccountOpenDate():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SetAccountOpenDate()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dbledDateAccountOpened.TAdvancedSpeedButton.Click(10, 4)
  advancedButton = cashwise.frmCalendarEdit.Panel1.btnPriorYear
  advancedButton.Click()
  advancedButton.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SetAccountOpenDate() is successful.")
  Indicator.PopText()

def CloseAddAccountWindows():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseAddAccountWindows()")
  cashwise.frmContactAccountEdit.btnSave.Click(68, 11)
  cashwise.frmContactAccountList.btnSave.Click(44, 9)
  cashwise.frmCustomerEdit.btnSave.Click(43, 11)
  cashwise.frmCustomerBrowse.btnSave.Click(34, 14)
  Log.Checkpoint("SUCCESS: CloseAddAccountWindows() is successful.")
  Indicator.PopText()
