﻿from BasePage import *
from Panel_Contact import *

def OpenContactBrowser():
  #Author Phil Ivey 6/22/2019
  #Last Modified by Brian Johnson 5/3/2021 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenContactBrowser()")
  cashwise.VCLObject("frmMain").VCLObject("TGraphicButton_3").click()
  Log.Checkpoint("SUCCESS: OpenContactBrowser() is successful.")
  Indicator.PopText()

def ClickAddContactButton():
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickAddContactButton()")
  contactBrowse = cashwise.frmContactBrowse
  contactBrowseMenu = contactBrowse.pnlSearch.isMain
  contactBrowse.Click(188, 18)
  contactBrowseMenu.Keys("[F12]")    
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickAddContactButton()")
  
def AddContactCompany(contact):
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddContactCompany")
  companyName = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlName.edCompanyName
  companyPhone = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhone
  typeID = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID.TAdvancedSpeedButton
  address = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlAddress.edAddr1
  zip = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlAddress.edZip
  email = cashwise.frmContactCompanyAdd.pnlMain.sbData.pnlFields.pnlEmailAddress.edEmailAddress
  contactSave = cashwise.frmContactCompanyAdd.btnSave
  companyName.Keys(contact["CompanyName"])
  companyPhone.Keys(contact["PrimaryPhone"])
  typeID.Click()
  phoneTypeSearch = cashwise.frmPhoneTypeBrowse.pnlSearch.isMain
  phoneTypeSave = cashwise.frmPhoneTypeBrowse.btnSave
  phoneTypeSearch.Keys(contact["PrimaryPhoneType"])
  phoneTypeSave.Click()
  address.Keys(contact["Address1"])
  zip.Keys(contact["ZipCode"])
  email.Keys(contact["Email"])
  contactSave.Click()
  ExplicitWait(2)
  Log.Checkpoint("SUCCESS: AddContactCompany is successful.")
  Indicator.PopText()
  
def EditContactCompany(editContact):
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditContactCompany")
  OpenContactEdit()
  companyName = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlName.edCompanyName
  companyPhone = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhone
  typeID = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID.TAdvancedSpeedButton
  address = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlAddress.edAddr1
  zip = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlAddress.edZip
  email = cashwise.frmContactCompanyEdit.pnlMain.sbData.pnlFields.pnlEmailAddress.edEmailAddress
  contactSave = cashwise.frmContactCompanyEdit.btnSave
  companyName.Keys(editContact["CompanyName"])
  companyPhone.Keys(editContact["PrimaryPhone"])
  typeID.Click()
  phoneTypeSearch = cashwise.frmPhoneTypeBrowse.pnlSearch.isMain
  phoneTypeSave = cashwise.frmPhoneTypeBrowse.btnSave
  phoneTypeSearch.Keys(editContact["PrimaryPhoneType"])
  phoneTypeSave.Click()
  address.Keys(editContact["Address1"])
  zip.Keys(editContact["ZipCode"])
  email.Keys(editContact["Email"])
  contactSave.Click()
  ExplicitWait(2)
  Log.Checkpoint("SUCCESS: EditContactCompany is successful.")
  Indicator.PopText()
  
def GetCompanyContact():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddContactCompany")
  newCompany = {}
  newCompany["CompanyName"] = GenerateRandomString(8)
  newCompany["PrimaryPhone"] = GenerateRandomPhoneNumber()
  newCompany["PrimaryPhoneType"] = "BUS"
  newCompany["Address1"] = "520 N Main St"
  newCompany["City"] = "Orem"  
  newCompany["State"] = "UT"  
  newCompany["ZipCode"] = "84057"
  newCompany["Email"] = newCompany["CompanyName"] +  "@nostromorp.com"
  Log.Checkpoint("SUCCESS:  GetCompanyContact() is successful")
  return newCompany

def GetCompanyContact2():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddContactCompany")
  newCompany = {}
  newCompany["CompanyName"] = GenerateRandomString(8)
  newCompany["PrimaryPhone"] = GenerateRandomPhoneNumber()
  newCompany["PrimaryPhoneType"] = "BUS"
  newCompany["Address1"] = "120 N Main St"
  newCompany["City"] = "Provo"  
  newCompany["State"] = "UT"  
  newCompany["ZipCode"] = "84606"
  newCompany["Email"] = newCompany["CompanyName"] +  "@nostromorp.com"
  Log.Checkpoint("SUCCESS:  GetCompanyContact() is successful")
  return newCompany
  
def ClickAddContactCompany():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddContactCompany")
  cashwise.frmContactBrowse.btnAddCompany.Click()
  Log.Checkpoint("SUCCESS: AddContactCompany is successful.")
  Indicator.PopText()
    
def AddContactPerson(contact):
  #Author Brian Johnson 5/3/2021
  #Last Modified by Brian Johnson 5/3/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddContact(contact)")
  lastName = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlName").VCLObject("edLast")
  firstName = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlName").VCLObject("edFirst")
  lastName.SetText(contact["LastName"])
  firstName.SetText(contact["FirstName"])
  EnterSocialSecurityNumber(contact)
  SelectEyeColor(contact)
  SelectHairColor(contact)
  SelectGender(contact)
  primaryPhone = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlBasic").VCLObject("pnlPhone").VCLObject("edPrimaryPhone")
  primaryPhone.SetText(contact["PrimaryPhone"])
  primaryPhoneTypeIDButton = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlBasic").VCLObject("pnlPhone").VCLObject("edPrimaryPhoneTypeID").VCLObject("TAdvancedSpeedButton")
  primaryPhoneTypeIDButton.click()
  phoneTypesField = cashwise.VCLObject("frmPhoneTypeBrowse").VCLObject("pnlSearch").VCLObject("isMain")
  phoneTypesField.Keys("cel")
  phoneTypesSaveandClose = cashwise.VCLObject("frmPhoneTypeBrowse").VCLObject("btnSave")
  phoneTypesSaveandClose.click()
  addressTypeID = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlAddress").VCLObject("dblefAddressTypeID").VCLObject("TAdvancedSpeedButton")
  addressTypeID.click()
  addressTypeIDSaveButton = cashwise.VCLObject("frmAddressTypeBrowse").VCLObject("btnSave")
  addressTypeIDSaveButton.Click()
  addressField = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlAddress").VCLObject("edAddr1")
  addressField.Keys(contact["Address1"])
  zipField = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlAddress").VCLObject("edZip")
  zipField.Keys(contact["ZipCode"])
  birthdateField = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("edBirthdate").Window("TDBDateEdit", "", 1)
  birthdateField.Keys(contact["BirthDate"])
  email = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlEmailAddress").VCLObject("edEmailAddress")
  email.Keys(contact["Email"])
  contactSaveAndClose = cashwise.VCLObject("frmContactPersonAdd").VCLObject("btnSave")
  contactSaveAndClose.Click()
  Log.Checkpoint("SUCCESS: AddContact(contact) is successful.")
  Indicator.PopText()
  
def EditContactPerson(editContact):
  #Author Brian Johnson 5/3/2021
  #Last Modified by Brian Johnson 5/3/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditContactPerson(editContact)")
  OpenContactEdit()
  EditPrimaryPhoneType(editContact)
  EditSocialSecurityNumber(editContact)
  EditBirthDate(editContact)
  #EditEthnicity(editContact)
  #EditPrimaryLanguage(editContact)
  EditEyeColor(editContact)
  EditHairColor(editContact)
  #SelectGender(editContact)
  EditGender(editContact)
  #EditMarketing(editContact)
  #EditNotes()
  SaveAndCloseContactEdit()
  Log.Checkpoint("SUCCESS: EditContactPerson(editContact) is successful.")
  Indicator.PopText()

def SaveNewContact():
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveNewContact()")
  cashwise.frmContactPersonAdd.btnSave.Click()
  Log.Checkpoint("SUCCESS: SaveNewContact() is successful.")
  Indicator.PopText()
  
def DeleteContact(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteContact(contact)")
  cashwise.frmMain.TGraphicButton_2.Click(32, 41)
  contactBrowseTable = cashwise.frmCustomerBrowse
  SelectSearchByName()
  searchInput = contactBrowseTable.pnlSearch.isMain
  name = contact["LastName"]+", "+contact["FirstName"]
  searchInput.Keys(name)
  ExplicitWait(3)
  contactBrowseTable.btnDelete.Click(14, 23)
  searchInput.DblClick(77, 12)
  searchInput.Keys("[Home]![End][Del]")
  contactBrowseTable.MainMenu.Click("[0]|[5]")
  Log.Checkpoint("SUCCESS: DeleteContact(contact) is successful.")
  Indicator.PopText()
  
def DeleteCompanyContact():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteCompanyContact()")
  cashwise.frmContactBrowse.btnDelete.Click()
  cashwise.frmContactBrowse.btnSave.Click()
  Log.Checkpoint("SUCCESS: DeleteCompanyContact() is successful.")
  Indicator.PopText()

def SelectSearchByName():
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  return;
  Indicator.PushText("In SelectSearchByName()")
  cashwise.frmContactBrowse.pnlSearch.icMain.TAdvancedSpeedButton.Click(6, 10)
  browseGrid = cashwise.frmAbstractList.pnlMain.dbgMain
  browseGrid.HScroll.Pos = 0
  browseGrid.VScroll.Pos = 1
  browseGrid.Click(20, 79)
  browseGrid.VScroll.Pos = 4
  browseGrid.DblClick(20, 79)
  Log.Checkpoint("SUCCESS: SelectSearchByName() is successful.")
  Indicator.PopText()

def EnterSocialSecurityNumber(contact):  
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterSocialSecurityNumber(contact)")
  ssnEdit = cashwise.frmContactPersonAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlSSN.edSSN
  ssnEdit.Click()
  ssnEdit.Keys("[Home]![End][Del]")
  ssnEdit.Keys(contact["SSN"])
  ssnEdit.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: EnterSocialSecurityNumber(contact) is successful.")
  Indicator.PopText()

def SelectEyeColor(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectEyeColor(contact)")
  eyeColorButton = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dblefEyeColor").VCLObject("TAdvancedSpeedButton")
  eyeColorButton.Click()
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(contact["EyeColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectEyeColor(contact) is successful.")
  Indicator.PopText()

def SelectHairColor(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectHairColor(contact)")
  hairColorButton = cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dblefHairColor").VCLObject("TAdvancedSpeedButton")
  hairColorButton.Click()
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(contact["HairColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectHairColor(contact) is successful.")
  Indicator.PopText()

def SelectGender(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectGender(contact)")
  if contact["Gender"] == "Male":
    cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "M", 3).click()
  elif contact["Gender"] == "Female":
    cashwise.VCLObject("frmContactPersonAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "F", 2).Click()
  else:
    pass
  Log.Checkpoint("SUCCESS: SelectGender(contact) is successful.")
  Indicator.PopText()
  
def SelectMarketingID(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectMarketingID(contact)")
  cashwise.frmContactAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMarketing.TAdvancedSpeedButton.Click(11, 7)
  incrementalSearch = cashwise.frmMarketingBrowse.pnlSearch.isMain
  incrementalSearch.Keys(contact["MarketingID"])
  ExplicitWait(3)
  incrementalSearch.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectMarketingID(contact) is successful.")
  Indicator.PopText()

def SetMembershipExpertation(months):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SetMembershipExpertation(months)")
  cashwise.frmContactAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMembershipExpirationDate.TAdvancedSpeedButton.Click(10, 9)
  tfrmCalendarEdit = cashwise.frmCalendarEdit
  advancedButton = tfrmCalendarEdit.Panel1.btnNextMonth
  for x in range(months):
    advancedButton.Click(16, 24)
    ExplicitWait(1)
  advancedButton.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SetMembershipExpertation(months) is successful.")
  Indicator.PopText()

def SelectID1Type(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectID1Type(contact)")
  cashwise.frmContactAdd.pnlMain.sbData.pnlAdditional.pnlID1.edID1Type.TAdvancedSpeedButton.Click(11, 6)
  panel = cashwise.frmContactAdd.pnlMain.sbData.pnlAdditional.pnlID1
  TDBLookupEditForm = panel.edID1Type
  cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys(contact["ID1"])
  ExplicitWait(3)
  cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectID1Type(contact) is successful.")
  Indicator.PopText()
  
def SelectID2Type(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectID2Type(contact)")
  cashwise.frmContactAdd.pnlMain.sbData.pnlAdditional.pnlID2.edID2Type.TAdvancedSpeedButton.Click(10, 7)
  cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys(contact["ID2"])
  ExplicitWait(3)
  cashwise.frmIdentificationTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectID2Type(contact) is successful.")
  Indicator.PopText() 

def SelectReferenceRelation(contact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectReferenceRelation(contact)")
  cashwise.frmContactAdd.pnlMain.sbData.pnlAdditional.pnlReference.edReference1Relation.TAdvancedSpeedButton.Click(7, 5)
  cashwise.frmContactReferenceTypeBrowse.pnlSearch.isMain.Keys(contact["ReferenceRelation"])
  ExplicitWait(3)
  cashwise.frmContactReferenceTypeBrowse.pnlSearch.isMain.Keys("[Enter]")
  Log.Checkpoint("SUCCESS: SelectReferenceRelation(contact) is successful.")
  Indicator.PopText() 

def CloseContactBrowse():
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseContactBrowse()")
  tfrmContactBrowse = cashwise.frmContactBrowse
  tfrmContactBrowse.Click(273, 16)
  tfrmContactBrowse.pnlMain.dbgMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: CloseContactBrowse() is successful.")
  Indicator.PopText()
     
def OpenContactEdit():
  #Author Phil Ivey 6/26/2019
  #Last Modified by Phil Ivey 6/26/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenContactEdit()")
  tfrmContactBrowse = cashwise.frmContactBrowse
  tfrmContactBrowse.Click(323, 13)
  tfrmContactBrowse.pnlSearch.isMain.Keys("[F11]")    
  Log.Checkpoint("SUCCESS: OpenContactEdit() is successful.")
  Indicator.PopText()

def SaveAndCloseContactEdit():
  #Author Phil Ivey 6/28/2019
  #Last Modified by Phil Ivey 6/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveAndCloseContactEdit()")
  cashwise.VCLObject("frmContactPersonEdit").VCLObject("btnSave").Click()
  Log.Checkpoint("SUCCESS: SaveAndCloseContactEdit() is successful.")
  Indicator.PopText()

def SelectContactByName(contact):
  #Author Phil Ivey 6/28/2019
  #Last Modified by Phil Ivey 6/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectContactByName(contact)")
  incrementalSearch = cashwise.frmContactBrowse.pnlSearch.isMain
  SelectSearchByName()
  incrementalSearch.Keys(contact["LastName"])
  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectContactByName(contact) is successful.")
  Indicator.PopText()

def EditPrimaryPhoneType(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditPrimaryPhoneType(editContact)")
  cashwise.frmContactPersonEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID.TAdvancedSpeedButton.Click()
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editContact["PrimaryPhoneType"])
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  ExplicitWait(3)
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  ExplicitWait(1)
  Log.Checkpoint("SUCCESS: EditPrimaryPhoneType(editContact) is successful.")
  Indicator.PopText()

def EditSecondaryPhoneType(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditSecondaryPhoneType(editContact)")
  cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edSecondaryPhoneTypeID.TAdvancedSpeedButton.Click(11, 6)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editContact["SecondaryPhoneType"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditSecondaryPhoneType(editContact) is successful.")
  Indicator.PopText()

def EditSocialSecurityNumber(editContact):  
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditSocialSecurityNumber(editContact)")
  cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlBasic").VCLObject("pnlSSN").VCLObject("edSSN").Click()
  TDBEdit = cashwise.VCLObject("frmCustomerSSN").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edSSN")
  TDBEdit.Click()
  TDBEdit.Keys(editContact["SSN"])
  TDBEdit.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditSocialSecurityNumber(editContact) is successful.")
  Indicator.PopText()

def EditBirthDate(days):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditBirthDate(days)")
  editContact = GetTempContact()
  #cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("edBirthdate").VCLObject("TAdvancedSpeedButton").Click()
  #calendarEdit = cashwise.frmCalendarEdit
  birthdateField = cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("edBirthdate").Window("TDBDateEdit", "2/23/1960", 1)
  birthdateField.Keys(editContact["BirthDate"])
  #cashwise.VCLObject("frmCalendarEdit").VCLObject("pnlMain").VCLObject("Calendar")
  #for x in range(days):
    #calendarEdit.Panel1.btnPriorDay.Click(10, 30)
    #ExplicitWait(1)
  Log.Checkpoint("SUCCESS: EditBirthDate(days) is successful.")
  Indicator.PopText()
  #calendarEdit.pnlMain.Calendar.Keys("[F9]")

def EditEthnicity(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditEthnicity(editContact)")
  groupBox = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal
  groupBox.edDescription.TAdvancedSpeedButton.Click(8, 5)
  ethnicity = cashwise.frmContactDescriptionBrowse.pnlSearch.isMain
  ethnicity.Keys(editContact["Ethnicity"])
  ExplicitWait(3)
  ethnicity.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditEthnicity(editContact) is successful.")
  Indicator.PopText()


def EditPrimaryLanguage(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditPrimaryLanguage(editContact)")
  cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal.edPrimaryLanguage.TAdvancedSpeedButton.Click(6, 7)
  languageBrowse = cashwise.frmLanguageBrowse.pnlSearch.isMain
  languageBrowse.Keys(editContact["PrimaryLanguage"])
  ExplicitWait(3)
  languageBrowse.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditPrimaryLanguage(editContact) is successful.")
  Indicator.PopText()    

def EditEyeColor(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditEyeColor(editContact)")
  speedButton = cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dblefEyeColor").VCLObject("TAdvancedSpeedButton")
  speedButton.Click(11, 8)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(editContact["EyeColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditEyeColor(editContact) is successful.")
  Indicator.PopText()    
    
def EditHairColor(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditHairColor(editContact)")
  speedButton = cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dblefHairColor").VCLObject("TAdvancedSpeedButton")
  speedButton.Click()
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys(editContact["HairColor"])
  ExplicitWait(3)
  cashwise.frmContactDescriptionColorBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditHairColor(editContact) is successful.")
  Indicator.PopText()
  
  

def EditGender(editContact):
  #Author Phil Ivey 6/22/2019
  #Last Modified by Phil Ivey 6/22/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmContactPersonEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditGender(editContact)")
  if editContact["Gender"] == "Male":
    cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "M", 3).ClickButton()
  elif editContact["Gender"] == "Female":
    cashwise.VCLObject("frmContactPersonEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlPersonal").VCLObject("dbrgGender").Window("TGroupButton", "M", 3).ClickButton()
  else:
    selectGender.TGroupButton.ClickButton()
  Log.Checkpoint("SUCCESS: EditGender(editContact) is successful.")
  Indicator.PopText()

def EditMarketing(editContact):
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditMarketing(editContact)")
  speedButton = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing.edMarketing.TAdvancedSpeedButton
  speedButton.Click(11, 6)
  cashwise.frmMarketingBrowse.pnlSearch.isMain.Keys(editContact["MarketingID"])
  ExplicitWait(3)
  cashwise.frmMarketingBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EditMarketing(editContact) is successful.")
  Indicator.PopText()

def EditNotes():
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  selectGender = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal.dbrgGender
  Indicator.PushText("In EditNotes()")
  notes = cashwise.frmContactEdit.pnlMain.pnlNotes.dbmNotes
  notes.Keys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed iaculis quam. Morbi tempus consectetur.")
  Log.Checkpoint("SUCCESS: EditNotes() is successful.")
  Indicator.PopText()    

def ClickRefrenceButton():
  #Author Phil Ivey 6/30/2019
  #Last Modified by Phil Ivey 6/30/2019  
  cashwise = Sys.Process("Cashwise")
  contactEditButtonReferences = cashwise.frmContactEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In ClickRefrenceButton()")
  contactEditButtonReferences.Click()
  editContactButton.Click()
  Log.Checkpoint("SUCCESS: ClickRefrenceButton() is successful.")
  Indicator.PopText()    

def SelectStatusID():
  #Author Phil Ivey 6/7/2019
  #Last Modified by Phil Ivey 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  contactEditButtonReferences = cashwise.frmContactEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectStatusID()")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.dblStatus.TAdvancedSpeedButton.Click(7, 6)
  statusList = cashwise.frmAddressStatusList.pnlMain.dbgMain
  statusList.VScroll.Pos = 0
  statusList.Click(49, 97)
  statusList.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectStatusID() is successful.")

def SelectPrimaryPhoneTypeID(editContact):
  #Author Phil Ivey 6/7/2019
  #Last Modified by Phil Ivey 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  contactEditButtonReferences = cashwise.frmContactEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectPrimaryPhoneTypeID(editContact)")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.edPrimaryPhoneTypeID.TAdvancedSpeedButton.Click(6, 7)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editContact["ReferencePrimaryPhoneTypeID"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectPrimaryPhoneTypeID(editContact) is successful.")

def SelectSecondaryPhoneTypeID(editContact):
  #Author Phil Ivey 6/7/2019
  #Last Modified by Phil Ivey 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  contactEditButtonReferences = cashwise.frmContactEdit.btnReferences
  editContactButton = cashwise.frmContactReferenceList.btnEdit
  Indicator.PushText("In SelectSecondaryPhoneTypeID(editContact)")
  cashwise.frmContactReferenceEdit.pnlMain.sbData.edSecondaryPhoneTypeID.TAdvancedSpeedButton.Click(9, 10)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys(editContact["ReferenceSecondaryPhoneTypeID"])
  ExplicitWait(3)
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectSecondaryPhoneTypeID(editContact) is successful.")
