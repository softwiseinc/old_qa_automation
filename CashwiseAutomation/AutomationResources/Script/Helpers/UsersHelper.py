﻿from BasePage import *

def SelectUser(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectUser(user)")
  searchInputField = cashwise.frmUserManageCheck.pnlSearch.isMain
  SelectSearchUsersByType("ID")
  searchInputField.Keys(user["ID"])
  ExplicitWait(5)
  Indicator.PopText()
  
def AddNewUser(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddNewUser(user)")
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption != "Users":
    OpenUserMaintenance()
  cashwise.frmUserManageCheck.btnAdd.Click()
  userFields = cashwise.frmUserAdd.pnlMain.sbData
  saveButton = cashwise.frmUserManageCheck.btnSave 
  userFields.edID.SetText(user["ID"])
  userFields.edFirstName.Keys(user["FirstName"])
  userFields.edLastName.Keys(user["LastName"])
  groupButton = cashwise.VCLObject("frmUserAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edGroup").VCLObject("TAdvancedSpeedButton")
  groupButton.Click()
  browseGrid = cashwise.VCLObject("frmGroupBrowse").VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.VScroll.Pos = 0
  browseGrid.Click(140, 64)
  saveAndCloseButton = cashwise.VCLObject("frmGroupBrowse").VCLObject("btnSave")
  saveAndCloseButton.Click()
  userFields.edPassword.Keys(user["Password"])
  userFields.edPasswordConfirm.Keys(user["Password"])
  userFields.edOverrideKey.Keys(user["Overide"])
  saveButton.Click()
  Log.Event("SUCCESS: AddNewUser(user) New User added")
  Indicator.PopText()
  return user["ID"]
  
def CreateUser(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/4/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CreateUser(user)")
  OpenUserMaintenance()
  cashwise.frmUserManageCheck.btnAdd.Click()
  userFields = cashwise.frmUserAdd.pnlMain.sbData
  saveButton = cashwise.frmUserManageCheck.btnSave 
  userFields.edID.SetText(user["ID"])
  userFields.edFirstName.Keys(user["FirstName"])
  userFields.edLastName.Keys(user["LastName"])
  userFields.edGroup.Keys(user["Group"])
  userFields.edPassword.Keys(user["Password"])
  userFields.edPasswordConfirm.Keys(user["Password"])
  userFields.edOverrideKey.Keys(user["Overide"])
  saveButton.Click()
  Log.Checkpoint("SUCCESS: CreateUser(user) is successful")
  Indicator.PopText()

def OpenUserMaintenance():
  #Author Pat Holman 4/24/2019
  #Last Modified by Phil Ivey 10/31/2019 HAD TO USE Keys
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenUserMaintenance()")
  userManagerForm = cashwise.frmUserManageCheck
  #cashwiseApp = cashwise.frmMainEditEmploymentPayDate
#  mainMenu = cashwiseApp.MainMenu
#  mainMenu.Click("User|User Maintenance...")User|
  cashwiseApp = cashwise.frmMain
#  cashwiseApp.Click("User")
#  cashwiseApp.Click("|User Maintenance...")
 #cashwise.VCLObject("frmMain").MainMenu.Click("User|User Maintenance...")
  cashwiseApp.Keys("~uu")
  ExplicitWait(1)
  EnterUserId(Owner["userId"])
  EnterUserPassword(Owner["Password"])  
  if(aqObject.CheckProperty(userManagerForm, "WndCaption", cmpEqual, "Users")):
    Log.Checkpoint("SUCCESS: Open User maintenance window open is Successful")
  Indicator.PopText();

def DeleteUser(userName):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteUser(userName)")  
  searchInputField = cashwise.frmUserManageCheck.pnlSearch.isMain
  SelectSearchUsersByType("Name")
  searchInputField.Keys(userName)
  cashwise.frmUserManageCheck.btnDelete.Click(47, 17)
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  Log.Checkpoint("SUCCESS: DeleteUser is successful")
  Indicator.PopText()    

def ValidateNewUserCreated(customer):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ValidateNewUserCreated(customer)")
  OpenUserMaintenance()
  userEditWindow = cashwise.frmUserEdit
  SelectSearchUsersByType("Name")
  searchInputField = cashwise.frmUserManageCheck.pnlSearch.isMain
  searchInputField.Click()
  searchInputField.Keys(customer["FirstName"])
  Log.Checkpoint("SUCCESS: ValidateNewUserCreated is successful")
  Indicator.PopText()
  ExplicitWait(2)
  Sys.Process("Cashwise").VCLObject("frmUserManageCheck").VCLObject("btnEdit").Click(36, 24)    
  if cashwise.VCLObject("frmUserEdit").VCLObject("edFirstName").wText == customer["FirstName"]:
    isFound = True
  else:
    isFound = False
  if isFound == True:
    Log.Message("SUCCESS: "+customer["FirstName"]+" is found")
  else:
    Log.Error(str(e))
    Log.Error("ERROR: "+customer["FirstName"]+" is NOT found")
  cashwise.VCLObject("frmUserEdit").VCLObject("btnClose").Click(44, 11)
  return isFound
  
def DeleteUserIfRandom(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteUserIfRandom(user)")
  if (user["isRandomType"]):
    DeleteUser(user["FirstName"])
    Log.Checkpoint("SUCCESS: "+user["FirstName"]+" is deleted")
  Indicator.PopText()
    
def OpenUserRightsTree():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/4/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenUserRightsTree()")
  buttonRights = cashwise.frmUserManageCheck.btnRights
  userRightsForm = cashwise.frmUserRightManage
  buttonRights.Click()
  
  while  cashwise.WaitWindow("TfrmUserRightManage","User Rights",1,900).Exists == False:
       aqUtils.Delay(1000)
  
  Uncheck_view_unmaskedSSN()
  aqUtils.Delay(1000)
  AllowAccessLocation_001()
  userManage = cashwise.frmUserManageCheck
  userManage.MainMenu.Click("[0]|[4]")
  Log.Checkpoint("SUCCESS: OpenUserRightsTree() is successful")
  Indicator.PopText()

def Uncheck_view_unmaskedSSN():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/4/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In Uncheck_view_unmaskedSSN()")
  tfrmUserRightManage = cashwise.frmUserRightManage
  tfrmUserRightManage.Click(110, 16)
  aqUtils.Delay(500)
  checkOutline = tfrmUserRightManage.pnlMain.coMain
  checkOutline.VScroll.Pos = 0
  checkOutline.DblClick(17, 45)
  aqUtils.Delay(500)
  checkOutline.Click(42, 95)
  aqUtils.Delay(500)
  tfrmUserRightManage.btnSave.Click(41, 13)
  Log.Checkpoint("SUCCESS: ValidateNewUserCreated is successful")
  Indicator.PopText()

def AllowAccessLocation_001():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/4/2019  
  cashwise = Sys.Process("Cashwise")
  aqUtils.Delay(500)
  Indicator.PushText("In AllowAccessLocation_001()")
  cashwise.frmUserManageCheck.btnLocations.Click(17, 14)
  userLocationEdit = cashwise.frmUserLocationEdit
  userLocationEdit.btnSelectAll.Click(48, 7)
  userLocationEdit.btnSave.Click(62, 7)
  Log.Checkpoint("SUCCESS: AllowAccessLocation_001() is successful")
  Indicator.PopText()

  
def DeleteTempUser(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Phil Ivey 10/31/2031    
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In DeleteTempUser(user)")
  tfrmMain = cashwise.frmMain
  tfrmMain.Click(526, 10)
  #tfrmMain.MainMenu.Click("User|User Maintenance...")
  tfrmMain.Keys("~uu")
  TDBEdit = cashwise.frmCPLogin.pnlMain.sbData.edPassword
  TDBEdit.Click(14, 10)
  TDBEdit.SetText(Owner["Password"])
  TDBEdit.Keys("[Enter]")
  tfrmUserManageCheck = cashwise.frmUserManageCheck
  incrementalSearch = tfrmUserManageCheck.pnlSearch.isMain
  incrementalSearch.Click(64, 6)
  incrementalSearch.Keys(user["ID"])
  ExplicitWait(5)
  tfrmUserManageCheck.btnDelete.Click(20, 16)
  cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  tfrmUserManageCheck.MainMenu.Click("[0]|[5]")
  Log.Checkpoint("SUCCESS: DeleteTempUser(user) is successful")
  Indicator.PopText()

def IsTrueUserLoginNewLocation(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/4/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In IsTrueUserLoginNewLocation(user)")
  mainWindow = cashwise.frmMain
  if(aqObject.CheckProperty(mainWindow, "WndCaption", cmpContains, user["ID"])):
    result = True
  else:
    result = False        
  Log.Checkpoint("SUCCESS: IsTrueUserLoginNewLocation(user) is successful")
  Indicator.PopText()
  return result     
  

def AssignLocation(user):
  #Author Pat Holman 6/4/2019
  #Last Modified by Pat Holman 6/4/2019
  #CreateUser(user)
  AddNewUser(user)
  OpenUserMaintenance()
  SelectUser(user)  
  OpenUserRightsTree()
  OpenStoreLocation(user["Location"])   