﻿from BasePage import *
from CashHelper import *
from CustomerHelper import *
from DatabaseQuery import *
from EmploymentHelper import *
#from LineOfCredit import *
from SystemSetupHelper import *
from Helpers import *

  #Author Pat Holman 7/3/2019
  #Last Modified by Pat Holman 7/15/2019  
def OpenLineOfCredit(customer):
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenLineOfCredit(customer)")
  Sys.Process("Cashwise").VCLObject("frmMain").VCLObject("TGraphicButton").Click()
  if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,500).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
    cashwise.frmCashOpenConfirm.Button1.Click()
  cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("Name","*Line of Credit*",15).Click()
  cusName = customer["FullName"]
  ExplicitWait(1)
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(cusName)
  ExplicitWait(3)
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[Enter]")
  #WaitLOCBrowser()
  WaitLOCBrowser()
  pageOpen = False
  Log.Checkpoint("SUCCESS: GetDrawAmount(amount) is successful.")
  Indicator.PopText()
  
def OpenLineOfCreditWithCusID(cusID):
  #Author Phil Ivey 09/05/2019
  #Last Modified by Phil Ivey 09/05/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenLineOfCredit(customer)")
  Sys.Process("Cashwise").VCLObject("frmMain").VCLObject("TGraphicButton").Click()
  
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
    cashwise.frmCashOpenConfirm.Button1.Click()

  cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("Name","*Line of Credit*",20).Click()
  ExplicitWait(1)
  #cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(customer["LastName"] + ", " + customer["FirstName"])
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
  ExplicitWait(3)
  #cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[Enter]")
  #WaitLOCBrowser()
  WaitLOCBrowser()
  #pageOpen = False
  Log.Checkpoint("SUCCESS: OpenLineOfCreditWithCusID is successful.")
  Indicator.PopText()

  
def GetDrawAmount(amount):
  #Author Pat Holman 7/15/2019
  #Last Modified by Pat Holman 7/15/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In GetDrawAmount(amount)")
  LOCWindow = cashwise.frmBrowser.pnlBrowser.ChromiumWebBrowser.CefBrowserWindow.Chrome_WidgetWin_0.pageSoftwiseLineOfCredit
  LineOfCreditWindow.panel2.textnode.Click()
  LineOfCreditWindow.panel.panel.panel.panel3.textboxMatInput0.Click()
  LineOfCreditWindow.panel.panel.panel.panel3.textboxMatInput0.DblClick()
  LineOfCreditWindow.panel.panel.panel.panel3.textboxMatInput0.Keys(amount)  
  LineOfCreditWindow.panel.panel.panel.panel3.textboxMatInput0.Keys("[F9]")  
  LineOfCreditWindow.textnode.Click()
  ExplicitWait(5)
  ProcessTrasaction()
  Log.Checkpoint("SUCCESS: GetDrawAmount(amount) is successful.")
  Indicator.PopText()
  
def ClickInitialDrawButton():
  #Author Pat Holman 7/15/2019
  #Last Modified by Phil Ivey 12/11/2019 
  #url = "https://qa-loc-1.softwise.com/loc"
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickInitialDrawButton()")
  thisURL = cashwise.frmBrowser.url.OleValue
  thisURL = aqString.Replace(thisURL,"\\","")
  thisURL = thisURL + "*"
  ExplicitWait(1)
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1)
  ExplicitWait(1)
  panel = page.Window("Chrome_WidgetWin*", "", 1).Page(thisURL)
  #drawButton = page.Find("contentText","attach*Initial Draw Amount",15)
  drawButton = panel.Find("ObjectLabel","attach_moneyInitial Draw Amount",15)
  ExplicitWait(2)
  drawButton.Click()
  Log.Checkpoint("SUCCESS: ClickInitialDrawButton() is successful.")
  Indicator.PopText()
  
def ClickDrawButton(drawAmount):
  #Author Phil Ivey 8/5/2019
  #Last Modified by Phil Ivey 8/5/2019 
  #WaitLOCBrowser() 
  #drawAmount = 1089
  cashwise = Sys.Process("Cashwise")
  thisURL = cashwise.frmBrowser.url.OleValue
  thisURL = aqString.Replace(thisURL,"\\","")
  thisURL = thisURL + "*"
  drawAmounts = aqConvert.FloatToStr(drawAmount)
  drawAmountc = aqConvert.StrToCurrency(drawAmounts)
  drawAmount = aqConvert.CurrencyToFormatStr(drawAmountc,2,0,0,-2)
  textValue = "Draw "+drawAmount+"*"
  Indicator.PushText("In ClickDrawButton()")
  page = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1)
  pannel = page.Window("Chrome_WidgetWin*", "", 1).Page(thisURL)
  drawButton = pannel.Find("contentText",textValue,10)
  drawButton.Click()
  Log.Checkpoint("SUCCESS: ClickDrawButton() is successful.")
  Indicator.PopText()
  
def ClickHoldButton():
  #Author Phil Ivey 8/5/2019
  #Last Modified by Phil Ivey 8/5/2019 
  #WaitLOCBrowser() 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickHoldButton")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  
  if((page.Find("contentText","Hold",15).Exists) == True):
    drawButton = page.Find("contentText","Hold",15)
    drawButton.Click()
    Log.Checkpoint("SUCCESS: ClickHoldButton() is successful.")
  else:
    Log.Error("Can't find Hold button")
  Indicator.PopText()


def ClickPaymentAmount():
  #Author Phil Ivey 8/5/2019
  #Last Modified by Phil Ivey 12/10/2019 changed to contentText
  cashwise = Sys.Process("Cashwise")
  thisURL = cashwise.frmBrowser.url.OleValue
  thisURL = aqString.Replace(thisURL,"\\","")
  thisURL = thisURL + "*"
  GetCurrentDatabaseOnCashwiseBrowser()
 
  Indicator.PushText("In ClickPaymentAmount")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin*", "", 1).Page(thisURL)
  ExplicitWait(1)
  buttonNumber = page.Panel(0).Panel(0).Panel(0).Panel(3)
  payButton = buttonNumber.Find("ObjectLabel","paymentPayment Amount",35)
  ExplicitWait(1)
 # payButton.Keys("[Tab]")
  payButton.Click()
  ExplicitWait(1)
  if( (page.Find("ObjectIdentifier","mat_input_*",25)).Exists):
     Log.Checkpoint("SUCCESS: ClickPaymentAmount is successful.")
  Indicator.PopText()

def ClickPay(amount):
  #Author Phil Ivey 8/5/2019
  #Last Modified by Phil Ivey 8/5/2019 
  #WaitLOCBrowser() GetLOCCusIdWith_LimitChangeHighest

  textValue = "Pay $"+amount
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickPaymentAmount")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  payButton = page.Find("ObjectLabel",textValue,15)
  payButton.Click()
  Log.Checkpoint("SUCCESS: ClickPaymentAmount is successful.")
  Indicator.PopText()  
  
def EnterDrawAmount(amount):
  #Author Pat Holman 7/15/2019
  #Last Modified by Phil Ivey 12/11/2019
  cashwise = Sys.Process("Cashwise") 
  thisURL = cashwise.frmBrowser.url.OleValue
  thisURL = aqString.Replace(thisURL,"\\","")
  thisURL = thisURL + "*"
  Indicator.PushText("In EnterDrawAmount(amount)")
  cBrowser = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1)
  page = cBrowser.Page(thisURL)
  inputField = page.Find("ObjectIdentifier","mat_input_0",15) 
  ExplicitWait(1) 
  inputField.DblClick()
  inputField.Keys("[Home]![End][Del]")
  inputField.Keys(amount)
  Log.Checkpoint("SUCCESS: EnterDrawAmount(amount) is successful.")
  Indicator.PopText()
  
  
def EnterPaymentAmount(amount):
  #amount = "15.00"
  #Author Phil Ivey 8/20/2019
  #Last Modified by Phil Ivey 8/20/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterPaymentAmount")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  inputField = page.Find("ObjectIdentifier","mat_input_*",15) 
  ExplicitWait(1) 
  inputField.DblClick()
  inputField.Keys("[Home]![End][Del]")
  inputField.Keys(amount)
  Log.Checkpoint("SUCCESS: EnterPaymentAmount is successful.")
  Indicator.PopText()


def ClickPayButton(amount): # MUST PASS THE AMOUNT BECAUSE THERE ARE TOO MANY OBJECTS THAT START WITH "Pay $"
  #Author Phil Ivey 8/20/2019
  #Last Modified by Phil Ivey 8/20/2019  
  #amount = "55.00"
  objLabelValue = "Pay $"+amount
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickPayButton")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  panel = page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1)
  payButton = panel.Find("contentText",objLabelValue,15) 
  ExplicitWait(1) 
  payButton.Click()
  
  #--------------- PUT IF STATEMENT HERE BEFORE LOGGING SUCCESS----------------
  Log.Checkpoint("SUCCESS: ClickPayButton is successful.")
  Indicator.PopText()

  
def ClickPayInFull(): 
  #Author Phil Ivey 12/10/2019
  #Last Modified by Phil Ivey 12/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickPayInFull")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  payButton = page.Find("contentText","*Pay in Full)",15) 
  ExplicitWait(1) 
  payButton.Click()
  Log.Checkpoint("SUCCESS: ClickPayInFull is successful.")
  Indicator.PopText()
  
    
def ClickMaximumDrawButton():
  #Author Pat Holman 7/15/2019
  #Last Modified by Pat Holman 7/15/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickMaximumDrawButton()")
  #Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(0).Panel(0).Panel(0).Panel(3).Panel(1).Button(0).Click()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  drawMaxButton = page.Find("contentText","Draw*",15)
  drawMaxButton.Click()
  Log.Checkpoint("SUCCESS: ClickMaximumDrawButton() is successful.")
  Indicator.PopText()

def ClickMinimumDrawButton():
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickMinimumDrawButton")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  drawMinButton = page.Find("contentText","Draw*",15)
  drawMinButton.Click()
  Log.Checkpoint("SUCCESS: ClickMinimumDrawButton is successful.")
  Indicator.PopText()

def HowPaymentApplied():
  #Author Phil Ivey 8/22/2019
  #Last Modified by Phil Ivey 8/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In HowPaymentApplied")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  payFull = IsPaymentOverFullAmount()
  ExplicitWait(1) 
  if(cashwise.frmBrowser.Find("contentText","How Payment Will*",15).Exists): #IF LABLE IS THERE
          paymentApplied = page.Find("contentText","How Payment Will*",15)
          paymentApplied.Click()  # CLICK ON THE LABEL OF THE BOX
          paymentApplied.Keys("[Tab]") #TAB DOWN INTO THE BOX
          paymentApplied.Keys("[Enter]") # HIT ENTER TO OPEN THE DROP DOWN
          paymentApplied.Keys("[Down]") #GO DOWN ONE TO SELECT DO NOT APPLY TO NEXT AUTO
          paymentApplied.Keys("[Enter]") #HIT ENTER TO SELECT DO NOT APPLY TO NEXT AUTO
  else:
        Log.Message("No Autopay")
  return payFull
 
def IsPaymentOverFullAmount():
  #Author Phil Ivey 12/06/2019
  #Last Modified by Phil Ivey 12/06/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In IsPaymentOK")
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  ExplicitWait(1) 
  if(cashwise.frmBrowser.Find("contentText","Payment cannot exceed*",15).Exists):  
    paymentOver = "Yes"
  else:
    paymentOver = "No"
  return paymentOver 

  
def ClickConfirmButton():
  #Author Pat Holman 7/15/2019
  #Last Modified by Phil Ivey 8/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickConfirmButton()")
#  Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_0").Panel(2).Panel(1).Button(0).Click2()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  confirmButton = page.Find("ObjectLabel","checkConfirm*",15)
  confirmButton.Click()
  ExplicitWait(1)
  if(cashwise.frmBrowser.Find("contentText","check*",15).Exists):
    cancelButton = page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_1").Panel(2).Panel(0).Button(0)
    cancelButton.Click()
  #Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_1").Panel(2).Panel(0).Button(0).Click()
  Log.Checkpoint("SUCCESS: ClickConfirmButton() is successful.")
  Indicator.PopText()
  
def ClickConfirmButtonNoCancel():
  #Author Phil Ivey 12/10/2019
  #Last Modified by Phil Ivey 12/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickConfirmButton()")
#  Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_0").Panel(2).Panel(1).Button(0).Click2()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  confirmButton = page.Find("ObjectLabel","checkConfirm*",15)
  confirmButton.Click()
  ExplicitWait(1)
  Log.Checkpoint("SUCCESS: ClickConfirmButtonNoCancel () is successful.")
  Indicator.PopText()

def ClickCancelMainWebPage():
  #Author Phil Ivey 1/02/2020
  #Last Modified by Phil Ivey 1/02/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickCancelMainWebPage")
#  Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_0").Panel(2).Panel(1).Button(0).Click2()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  confirmButton = page.Find("ObjectLabel","closeCancel",15)
  confirmButton.Click()
  ExplicitWait(1)
  Log.Checkpoint("SUCCESS: ClickCancelMainWebPage () is successful.")
  Indicator.PopText()
    
def ClickConfirmButtonOnly():
  #Author Phil Ivey 9/04/2019  
  #Last Modified by Phil Ivey 9/04/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickConfirmButton()")
#  Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("*").Panel(2).Panel(1).Panel("cdk_overlay_0").Panel(2).Panel(1).Button(0).Click2()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
  confirmButton = page.Find("ObjectLabel","checkConfirm*",15)
  confirmButton.Click()
  ExplicitWait(1)
  Log.Checkpoint("SUCCESS: ClickConfirmButton() is successful.")
  Indicator.PopText()
    
def SignCustomerUpForAutoPay(cusID,button):
  cashwise = Sys.Process("Cashwise")
  GetLocationIDFromfrmMainWndCap()
  webPage = "https://qa-loc-*"
  testPassed = ""
  while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 
  # if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if Other report appears
              Log.Message("------------------------Rave Report Preview Appeared ------------------------------------------")
              cashwise.RavePreviewForm.Keys("~fx")
              ExplicitWait(1)
#  isThere = cashwise.Find("ObjectIdentifier","btnProcess",15).Exists
#  if(isThere == True):
#    return True
  
  page = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser")
  #overlay =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage).Panel(2).Panel(1).Panel("cdk_overlay*")
  overlay =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage).Panel(2)
  #overlay =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1)
 
  if(button == "No"):
    cancelAutoPayButton = overlay.Find("contentText","Cancel",15)
    ExplicitWait(1)
    cancelAutoPayButton.Click()
  if(button == "Yes"):
    yesAutoPayButton = page.Find("ObjectLabel","checkYes",15)
    yesAutoPayButton.Click()
    #overlay2 =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage).Panel(2).Panel(1).Panel("cdk_overlay_2")
    #overlay2 =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page(webPage)
    overlay2 =   page.WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1)
    verifyButton = overlay2.Find("ObjectLabel","checkVerify",10)
    verifyButton.Click()
    ExplicitWait(1)
    overlay2.Click() # clicks next autopay date
  ExplicitWait(1)  
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
      # SAVE REPORT      
      testPassed = CheckForOptionalPayAuth(cusID)
      cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if Other report appears
              cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
              cashwise.RavePreviewForm.Keys("~fx")
  return testPassed
  
def EditTransaction(): # start on Transaction page Edit go back to tranaction page
  amount = "550.00"
  cashwise = Sys.Process("Cashwise")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
              cashwise.RavePreviewForm.Keys("~fx")
  cashwise.frmSaleEdit.btnEditSaleItem.Click()
  WaitLOCBrowser()
  ClickInitialDrawButton()
  EnterDrawAmount(amount)
  ClickDrawButton(amount)
  ClickConfirmButtonOnly()
    
  
def ProcessTrasaction():
  #Author Pat Holman 7/15/2019
  #Last Modified by Phil Ivey 8/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ProcessTrasaction()")
  tfrmSaleEdit = cashwise.Window("TfrmSaleEdit", "Sales Transaction", 1)
  
     
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # to Collect
        cashwise.RavePreviewForm.Keys("~fx")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # to Collect
        cashwise.RavePreviewForm.Keys("~fx")
  tfrmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlProcessContainer.btnProcess.Click(37, 34)
  
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # to Collect
        cashwise.RavePreviewForm.Keys("~fx")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # to Collect
        cashwise.RavePreviewForm.Keys("~fx")
  listBox = cashwise.frmAliasBrowse.pnlMain.sbData.lbAliases
  listBox.ClickItem("Cash")
  ExplicitWait(1)
  listBox.Keys("[F9]")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TfrmTransactionCashTotalEdit","Transaction Cash Amount",1,500).Exists):
    cap = cashwise.frmTransactionCashTotalEdit.lblAmount.Caption
    cap = aqString.Replace(cap,"$","",False)
    cashwise.frmTransactionCashTotalEdit.pnlMain.sbData.Panel1.dbceTotalCash.Keys(cap)
    cashwise.frmTransactionCashTotalEdit.btnSave.Click()
    ExplicitWait(3)
    if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # to Collect
      cashwise.RavePreviewForm.Keys("~fx")
    ExplicitWait(3)
    if(cashwise.WaitWindow("TfrmSaleConfirmPayout","Payout",1,2000).Exists) :  # for Payout
      cashwise.frmSaleConfirmPayout.Keys("[F9]")
    ExplicitWait(2)
  
  # conditioal statments added by Phil

  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): #if POS receipt appears
        cashwise.RavePreviewForm.Keys("~fx")
  if(cashwise.WaitWindow("#32770", "Save Print Output As", 1,800).Exists):
         cashwise.Window("#32770", "Save Print Output As", 1).Window("Button", "Cancel", 2).Click()
  
  if(cashwise.WaitWindow("TfrmSaleConfirmPayout","Payout",1,2000).Exists) :  # for Payout
        cashwise.frmSaleConfirmPayout.Keys("[F9]")
  ExplicitWait(1)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # If Rave Report other
        cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(3)      
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # If Rave Report Receipt
          cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(3)      
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,5000).Exists):  # If Rave Report Receipt
          cashwise.RavePreviewForm.Keys("~fx")

  #cashwise.frmSaleConfirmStatus.pnlMain.btnProcess.Click()
  Log.Checkpoint("SUCCESS: ProcessTrasaction() is successful.")
  Indicator.PopText()

def WaitLOCBrowser():
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  
      count = 0
      cashwise = Sys.Process("Cashwise")
      pageOpen = False
      while pageOpen == False:
        aqUtils.Delay(3000)  
        if(Sys.Process("Cashwise").WaitWindow("WindowsForms10.Window.8.app*", "Cashwise*", 1, 800).Exists):
            aqUtils.Delay(1000) 
            pageOpen = True
            Log.Message("Cashwise Browser is up")
        else:                       
            pageOpen = False
      pageOpen = False  
      while pageOpen == False:
        aqUtils.Delay(3000) 
        if(cashwise.frmBrowser.Find("contentText","close*",15).Exists):
           pageOpen = True
        else:
            pageOpen = False
      
      pageOpen = False
      while pageOpen == False:
        aqUtils.Delay(3000) 
        count = count +1
        if(cashwise.frmBrowser.WinFormsObject("pnlBrowser").Find("contentText","Last Statement Date:",25).Exists):
          pageOpen = True
          Log.Message("Found Last Statement Date inside Cashwise Browser")
        else:
          pageOpen = False
        if count == 500: # if test complete cannot see the buttons close cashwise and reopen
           cashwise.frmBrowser.Close()
           cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
           cashwise.frmSaleEdit.btnClose.Click()
           cashwise.frmMain.Close()
           #TestedApps.Cashwise.Close()
           aqUtils.Delay(5000) 
           TestedApps.Cashwise.Run()
           managerUser = GetManager002()
           StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])  
           Sys.Process("Cashwise").VCLObject("frmMain").VCLObject("TGraphicButton").Click()
           if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,500).Exists):
              cashwise.frmCashOpenConfirm.Button1.Click()
           #cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("Name","*Line of Credit*",15).Click()
           cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Find("WndCaption","*Line of Credit*",15).Click()
           aqUtils.Delay(3000) 
           cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys(customer["LastName"] + ", " + customer["FirstName"])
           aqUtils.Delay(3000) 
           cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[Enter]")
           Log.Message("Last Line of WaitLOCBrowser")
  
  

#def ConvertStrToDate(startDateAfterRollForword): # On Calendar after rollforward, get date on Calendar 
#    #Author Phil Ivey 8/1/2019
#    #Last Modified by Phil Ivey 8/1/2019  
#    #prevSep = aqString.ListSeparator
#    #startDateAfterRollForword = "'Wednesday, August 11, 2010'"
#    prevSep = aqString.ListSeparator
#    aqString.ListSeparator = "  "
#    startDateAfterRollForword= aqString.Replace(startDateAfterRollForword, ",", "")     
#    
#    NumOfWord = aqString.GetListLength(startDateAfterRollForword)
#    
#    wordArray = Sys.OleObject["Scripting.Dictionary"]
#    
#    dayOfWeek = aqString.GetListItem(startDateAfterRollForword, 0)
#    
#    dayOfWeekLength = aqString.GetLength(dayOfWeek)
#    
#    startDateAfterRollForword = aqString.Remove(startDateAfterRollForword, 0, dayOfWeekLength+1)
#    
#    snewDate = aqConvert.DateTimeToFormatStr(startDateAfterRollForword, "%m/%d/%Y") 
#    Log.Message(snewDate)
#    
#    aqString.ListSeparator = prevSep
#    
#    dnewDate = aqConvert.StrToDate(snewDate)
#    return dnewDate


def ConvertStrToDate(startDateAfterRollForword): #--Converts Sunday, January 14, 2018 to 1/14/2018
 
       # startDateAfterRollForword = Sys.Process"]("Cashwise").VCLObject"]("frmCalendarEdit").VCLObject"]("txtDate").Caption"]
      # startDateAfterRollForword = "Thu Sep 23 2010"
       startDateAfterRollForword = aqString.Replace(startDateAfterRollForword, ",", "")
       aqString.ListSeparator = " "
       NumOfWord = aqString.GetListLength(startDateAfterRollForword)
      
       wordArray = Sys.OleObject["Scripting.Dictionary"]
 
       for x in range(NumOfWord):
           y = aqConvert.IntToStr(x)
           wordArray.Add(y, aqString.GetListItem(startDateAfterRollForword,x))
           s = wordArray.Item[y];
           Log.Message("number = "+aqConvert.IntToStr(x)+"word = "+s) 
       
       dayOfTheWeek = wordArray.Item["0"]
       monthWord = wordArray.Item["1"]
       dayOfMonth = wordArray.Item["2"]
       year = wordArray.Item["3"]
           
       monthWord = MonthToMonth(monthWord)
       monthDate = switchMonthToNumber(monthWord)
  
       startDateAfterRollForwordNum = monthDate+"/"+dayOfMonth+"/"+year
       Log.Message("Converted "+startDateAfterRollForword+" to "+startDateAfterRollForwordNum)
       
       startDateAfterRollForwordNum = aqConvert.StrToDate(startDateAfterRollForwordNum)
       
       return startDateAfterRollForwordNum

def GetNextLOCRollForwardDate(locID):
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  
    #locID = "001";
    aQuery = ";WITH OpenAccounts AS (SELECT lcv.AccountId FROM LineOfCreditView lcv WHERE lcv.IsOpen = 1 AND lcv.AccountId like '"+locID+"%') " +\
    ",AllDates (RollForwardDate) " +\
    "AS ( SELECT la.NextPaymentDueDate FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId "+\
    "UNION " +\
    "SELECT la.NextBillingCycleDate FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId  "+\
    "UNION " +\
    "SELECT lpps.PaymentDate FROM LOCPaymentPlanSchedule lpps INNER JOIN OpenAccounts oa ON lpps.Account_ID = oa.AccountId) "+\
    "SELECT MIN(RollForwardDate) as RollForwardDate FROM AllDates ad where ad.RollForwardDate > (select System_Date from Location where ID = '"+locID+"')"
    Log.Message(aQuery);
    columnName = "RollForwardDate";

    rDate = RunSingleQuery(aQuery,columnName);
    return rDate;
 
def GetLOCCusIdWithNoLimitChangeLowest():  # Returns "Empty" if no results  xxxxxx  No History  xxxxxx
   #Author Phil Ivey 8/1/2019
   #Last Modified by Phil Ivey 8/1/2019 
    #"and clc.Customer_ID = '001-0000188'  
  aQuery = "select top 1 clc.Customer_ID from CustomerLimitByLoanTypeCategory clc " +\
  "join LOCAccount la on clc.Customer_ID = la.Customer_ID " +\
  "where la.Status_ID <> 'CLO' " +\
  "and la.ID like '001%' " +\
  "and clc.Customer_ID not in " +\
  "(select clcd.Customer_ID from CustomerLimitByLoanTypeCategoryDuring clcd join LOCAccount la on clcd.Customer_ID = la.Customer_ID where la.Status_ID <> 'CLO') " +\
  "and exists (select Contact_ID, Count(AccountNumber) numOfAcct from ContactAccount ca where ca.Contact_ID = clc.Customer_ID group by Contact_ID having Count(AccountNumber) = 1)"
  "and exists (Select 1 from LOCPaymentPlan where AutoPayType <> 'ACH' and Account_ID = la.ID)order by clc.DeferredLimit"
  Log.Message(aQuery)
  columnName = "Customer_ID"
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message(cusID)
  return cusID
  
def GetLocCusIdWithAutoPay():# Returns "Empty" if no results
   #Author Phil Ivey 9/11/2019
   #Last Modified by Phil Ivey 9/11/2019 
    aQuery = "select top 1 la.Customer_ID from LOCPaymentPlan lpp join LOCAccount la on lpp.Account_ID = la.ID " +\
    "and exists (select Contact_ID, Count(*) numOfAcct from ContactAccount ca where ca.Contact_ID = la.Customer_ID group by Contact_ID having Count(*) = 1) " +\
    "where AutoPayType = 'ACH' and la.Status_ID <> 'CLO'"
    Log.Message(aQuery)
    columnName = "Customer_ID"
    cusID = RunSingleQuery(aQuery,columnName)
    return cusID 
  

def GetLOCCusIdWith_LimitChangeLowest():  # Returns "Empty" if no results xxxxxxxxxxx With History xxxxxx
   #Author Phil Ivey 8/1/2019
   #Last Modified by Phil Ivey 01/13/2020
    #"and clc.Customer_ID = '001-0000188'  
  aQuery = "select top 1 clc.Customer_ID from CustomerLimitByLoanTypeCategory clc " +\
  "join LOCAccount la on clc.Customer_ID = la.Customer_ID " +\
  "where la.Status_ID <> 'CLO' " +\
  "and la.ID like '001%' " +\
  "and exists (select Contact_ID, Count(AccountNumber) numOfAcct from ContactAccount ca where ca.Contact_ID = clc.Customer_ID group by Contact_ID having Count(AccountNumber) = 1) " +\
  "and clc.Customer_ID in " +\
  "(select clcd.Customer_ID from CustomerLimitByLoanTypeCategoryDuring clcd join LOCAccount la on clcd.Customer_ID = la.Customer_ID where la.Status_ID <> 'CLO') " +\
  "order by clc.DeferredLimit"
  Log.Message(aQuery)
  columnName = "Customer_ID"
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message(cusID)
  return cusID

def GetLOCCusIdWith_LimitChangeHighest():  # Returns "Empty" if no results xxxxxxxxxxx With History xxxxxx
   #Author Phil Ivey 8/1/2019
   #Last Modified by Phil Ivey 01/13/2020
    #"and clc.Customer_ID = '001-0000188'  
  aQuery = "select top 1 clc.Customer_ID from CustomerLimitByLoanTypeCategory clc  " +\
  "join LOCAccount la on clc.Customer_ID = la.Customer_ID and la.Status_ID <> 'CLO' and la.ID like '001%'  " +\
  "join LOCPaymentPlan lpp on la.ID = lpp.Account_ID " +\
  "and exists (select Contact_ID, Count(AccountNumber) numOfAcct from ContactAccount ca where ca.Contact_ID = clc.Customer_ID group by Contact_ID having Count(AccountNumber) = 1)  " +\
  "and clc.Customer_ID in  " +\
  "(select clcd.Customer_ID from CustomerLimitByLoanTypeCategoryDuring clcd join LOCAccount la on clcd.Customer_ID = la.Customer_ID where la.Status_ID <> 'CLO')  " +\
  "and lpp.AutoPayType = 'ACH' and la.Balance > 50 and la.Status_ID not like 'P%' order by clc.DeferredLimit desc"
  Log.Message(aQuery)
  columnName = "Customer_ID"
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message(cusID)
  return cusID

  
  
def GetLOCCusIdWithNoLimitChangeHighest():  # Returns "Empty" if no results  xxxxx  NO History xxxxxxxxx
   #Author Phil Ivey 8/21/2019
   #Last Modified by Phil Ivey 01/13/2020
   
  aQuery = "select top 1 clc.Customer_ID from CustomerLimitByLoanTypeCategory clc " +\
  "join LOCAccount la on clc.Customer_ID = la.Customer_ID " +\
  "where la.Status_ID <> 'CLO' " +\
  "and la.ID like '001%' " +\
  "and exists (select Contact_ID, Count(AccountNumber) numOfAcct from ContactAccount ca where ca.Contact_ID = clc.Customer_ID group by Contact_ID having Count(AccountNumber) = 1) " +\
  "and clc.Customer_ID not in " +\
  "(select clcd.Customer_ID from CustomerLimitByLoanTypeCategoryDuring clcd join LOCAccount la on clcd.Customer_ID = la.Customer_ID where la.Status_ID <> 'CLO') " +\
  "order by clc.DeferredLimit desc"
  Log.Message(aQuery)
  columnName = "Customer_ID"
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message(cusID)
  return cusID



def GetLocMinPaymentDue(cusID):
  #cusID = "001-0000421"
  aQuery = "select MinimumPaymentDue from LOCAccount where Customer_ID = '"+cusID+"'"
  columnName = "MinimumPaymentDue"
  minDue = RunSingleQuery(aQuery,columnName)
  return minDue  #float


def GetLocLimit(cusID):
  #cusID = "001-0000421"
  aQuery = "select DeferredLimit from CustomerLimitByLoanTypeCategory where Customer_ID = '"+cusID+"' and Category_ID = 'LOC'"
  columnName = "DeferredLimit"
  locLimit = RunSingleQuery(aQuery,columnName)
  return locLimit  #float
  
def UpdateLocLimit(cusID):
  #cusID = "001-0000003"
  oldLocLimit = GetLocLimit(cusID)
  anUpDate = "Update CustomerLimitByLoanTypeCategory set DeferredLimit = "+newLocLimit+" where Customer_ID = '"+cusID+"' and Category_ID = 'LOC'"
  RunUpdate(anUpDate)
  afterLocUpdateLimit = GetLocLimit(cusID)
  Log.Message("Before update "+oldLocLimit+" After update "+afterLocUpdateLimit)
  
def IsButtonThere(cusID,button):
#  Draw: Available Credit > 0, Not Past Due, Is Proposed or (Not OriginationDate and Not Past Due)
#  Hold: Enrolled in AutoPay and no existing Hold
#  Skip Payment: Enrolled in AutoPay and no existing Skip
#  Make Payment: No pending proposal or current in a payment proposal
# and balance >= 1
# button = "Draw"
   
  accountStatus = GetLocAccountStatus(cusID)
  accountBalance = GetLocAccountBalance(cusID)
  if(accountBalance == "Empty"):
    accountBalance = 0.00
  locLimit = GetLocLimit(cusID)
  if(locLimit == "Empty"):
    locLimit = 0.00
  availableCredit = (locLimit - accountBalance)
  minDue = GetLocMinPaymentDue(cusID)
  if(minDue == "Empty"):
    minDue = 0.00
  
  if button == "Draw"  :
      if accountStatus == "OPE" and availableCredit > 0:
        Log.Message("There is a Draw button")
        return True
      elif accountStatus == "PRO" and availableCredit > 0:
        Log.Message("There is a Draw button")
        return True
      else:
        Log.Message("No Draw Button")
        return False
  
  if button == "Pay":
      if minDue > 1 and availableCredit > 0:
        Log.Message("Payment Button is there")
        return True
      else:
        Log.Message("Payment button is not there") 
        return False
  
def GetLocAccountStatus(cusID):
  aQuery = "select Status_ID from LOCAccount where Customer_ID = '"+cusID+"' and Status_ID <> 'CLO'"
  columnName = "Status_ID"
  statusID = RunSingleQuery(aQuery,columnName)
  return statusID
  
def GetLocAccountBalance(cusID):
  aQuery = "select Balance from LOCAccount where Customer_ID = '"+cusID+"' and Status_ID <> 'CLO'"
  columnName = "Balance"
  balance = RunSingleQuery(aQuery,columnName)
  return balance

def GetLocAccountNum(cusID):
    #cusID = "001-0000841"
    aQuery = "select ID from LOCAccount where Customer_ID = '"+cusID+"' and Status_ID <> 'CLO'"
    columnName = "ID"
    accountID = RunSingleQuery(aQuery,columnName)
    return accountID

def GetNumOfLOCAcctsOpen(locID):
  #locID = "001"
  aQuery = "select count(*) NumOpenLOCAcct from LOCAccount where Status_ID = 'OPE' and Location_ID = '"+locID+"'"
  columnName = "NumOpenLOCAcct"
  numOpenAccts = RunSingleQuery(aQuery,columnName)
  return numOpenAccts
  
def test():
  DeleteRecreateLOCfileShare("2014")
  
def DeleteRecreateLOCfileShare(yearDirectory):

  batObject = Sys.OleObject["WScript.Shell"]
  runBatchFile = "F:\_code\QA\CashwiseAutomation\DeleteRecreateLOCfileShare.bat.lnk "+yearDirectory+""
  Log.Message(runBatchFile)
  batObject.Run(runBatchFile)
 # Sys.OleObject["WScript.Shell"].Exec("cmd /c ""F:\_code\QA\CashwiseAutomation\DeleteRecreateLOCfileShare.bat")
  Log.Message("Ran batch file")


def GetNextBillingDateCus(cusID):
  #cusID  = "001-0000494"
  aQuery = "Select convert(varchar(50),NextBillingCycleDate,101) NextBillingDate " +\
  "from  LOCAccount where Status_ID <> 'CLO' and Customer_ID = '"+cusID+"'"
  columnName = "NextBillingDate"
  nextBillDate = RunSingleQuery(aQuery,columnName)
  return nextBillDate
  
def GetNextACHPaymentCus(cusID):
  #cusID  = "001-0000494"
  aQuery = "Select top 1 convert(varchar,PaymentDate,101) PaymentDate from LOCPaymentPlanSchedule " +\
  "where  Account_ID = (select MAX(Account_ID) from LOCPaymentPlanSchedule where Contact_ID  = '"+cusID+"') "+\
  "and PaymentDate >= (select System_Date from Location where ID = '001' )  "+\
  "order by ID"
  columnName = "PaymentDate"
  nextACHDate = RunSingleQuery(aQuery,columnName)
  return nextACHDate
  

def MonthToMonth(mIn):
  
  mOUT  = ""
     
  if mIn == "Jan":  
               mOUT = "January"
           
  if(mIn == "Feb") :
               mOUT = "February"
           
  if(mIn == "Mar"):
               mOUT = "March"
           
  if(mIn == "Apr"):      
            mOUT = "April"
           
  if(mIn == "May"):       
               mOUT = "May"
           
  if(mIn == "Jun"):       
               mOUT = "June"
           
  if(mIn == "Jul"):       
              mOUT = "July"
           
  if(mIn == "Aug"):       
               mOUT = "August"
           
  if(mIn == "Sep"):       
               mOUT = "September"
           
  if(mIn == "Oct"):       
               mOUT = "October"
           
  if(mIn == "Nov"):       
               mOUT = "November"
           
  if(mIn == "Dec"):       
               mOUT = "December"            
  Log.Message(mOUT)       
    
  return mOUT