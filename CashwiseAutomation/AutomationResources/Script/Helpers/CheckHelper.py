﻿from BasePage import *

def OpenCheckPanel():
  #Author Pat Holman 12/29/2020
  #Last Modified by Pat Holman 12/29/2020
  Indicator.PushText("Run OpenCheckPanel()")
  cashwise = Sys.Process("Cashwise")
  CheckButton = cashwise.VCLObject("frmSaleEdit").VCLObject("pnlMain").VCLObject("pnlPOSButtons").Window("TAdvancedButton", "&1 Check")
  CheckButton.Click(50, 9) 
  Log.Checkpoint("SUCCESS: OpenCheckPanel()")
  Indicator.PopText()
  
def CloseCheckHistory():
  #Author Pat Holman 12/29/2020
  #Last Modified by Pat Holman 12/29/2020
  Indicator.PushText("Run CloseCheckHistory()")
  cashwise = Sys.Process("Cashwise")
  CloseSaveButton = cashwise.VCLObject("frmCheckHistory").VCLObject("btnSave")
  CloseSaveButton.Click(70, 12)
  Log.Checkpoint("SUCCESS: CloseCheckHistory()")
  Indicator.PopText()
  
def AddCheckMakerAccount():
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run AddCheckMakerAccount()")
  cashwise = Sys.Process("Cashwise")
  BankSpeedButton = cashwise.VCLObject("frmCheckEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlMainCheck").VCLObject("pnlCheck").VCLObject("edRoutAcct").VCLObject("TAdvancedSpeedButton")
  BankSpeedButton.Click(13, 8)
  BankBrowse = cashwise.VCLObject("frmBankAccountCustomerBrowse")
  AddButton = BankBrowse.VCLObject("btnAdd")
  AddButton.Click(21, 20)
  Log.Checkpoint("SUCCESS: AddCheckMakerAccount()")
  Indicator.PopText()  

def CheckMakerRoutingNumber(routingNumber):
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run CheckMakerRoutingNumber(routingNumber)")
  cashwise = Sys.Process("Cashwise")
  RoutingID = cashwise.VCLObject("frmBankAccountEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlColumn1").VCLObject("pnlRouting").VCLObject("edRouting").Window("TCustomDBEdit", "", 1)
  RoutingID.Keys(6)
  RoutingNumber = cashwise.VCLObject("frmDoubleEnterRoutingNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit1")
  RoutingNumber.SetText(routingNumber)
  ConfirmRoutingNumber = cashwise.VCLObject("frmDoubleEnterRoutingNumber").VCLObject("pnlMain").VCLObject("sbData").VCLObject("DBEdit2")
  ConfirmRoutingNumber.SetText(routingNumber)
  ButtonSave = cashwise.VCLObject("frmDoubleEnterRoutingNumber").VCLObject("btnSave")
  ButtonSave.Click()
  Log.Checkpoint("SUCCESS: CheckMakerRoutingNumber(routingNumber)")
  Indicator.PopText()  

def CheckMakerAccountNumber(accountNumber):
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run CheckMakerAccountNumber(accountNumber)")
  cashwise = Sys.Process("Cashwise")
  AccountNumber = cashwise.VCLObject("frmBankAccountEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlFields").VCLObject("pnlColumn1").VCLObject("pnlRouting").VCLObject("edAcctNum")
  AccountNumber.SetText("640623118")
  ExplicitWait(1)
  ButtonSave = cashwise.VCLObject("frmBankAccountEdit").VCLObject("btnSave")
  ButtonSave.Click()
  Log.Checkpoint("SUCCESS: CheckMakerAccountNumber(accountNumber) :" + accountNumber)
  Indicator.PopText()   
   
def SetCheckMaker():
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run SetCheckMaker()")
  cashwise = Sys.Process("Cashwise")
  AddCheckMakerAccount()
  CheckMakerRoutingNumber("307074519")
  CheckMakerAccountNumber("640623118")
  ButtonSave = cashwise.VCLObject("frmBankAccountCustomerBrowse").VCLObject("btnSave")
  ButtonSave.Click()
  ConfirmYes = cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes")
  ConfirmYes.Click()
  Log.Checkpoint("SUCCESS: SetCheckMaker()")
  Indicator.PopText()  
  
def SetCheckNumber(checkNumber):
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run SetCheckNumber(checkNumber)")
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(1)
  CheckDetails = cashwise.VCLObject("frmCheckEdit")
  scrollBox = CheckDetails.VCLObject("pnlMain").VCLObject("sbData")
  CheckNumber = scrollBox.VCLObject("pnlMainCheck").VCLObject("pnlCheck").VCLObject("edNum")
  CheckNumber.Click(57, 15)
  CheckNumber.SetText(checkNumber)
  Log.Checkpoint("SUCCESS: SetCheckNumber(checkNumber)")
  Indicator.PopText()  

def SetCheckAmount(checkAmount):
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run SetCheckAmount(checkAmount)")
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(1)
  CheckDetails = cashwise.VCLObject("frmCheckEdit")
  scrollBox = CheckDetails.VCLObject("pnlMain").VCLObject("sbData")
  CheckAmount = scrollBox.VCLObject("pnlMainCheck").VCLObject("pnlCheck").VCLObject("edNum")
  CheckAmount = scrollBox.VCLObject("edAmount")
  CheckAmount.Click(20, 8)
  CheckAmount.SetText(checkAmount)
  CheckAmount.Keys("[Tab]")
  Log.Checkpoint("SUCCESS: SetCheckAmount(checkAmount)")
  Indicator.PopText()  
  
def CloseCheckPanel():
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run CloseCheckPanel()")
  cashwise = Sys.Process("Cashwise")
  CheckDetails = cashwise.VCLObject("frmCheckEdit")
  ButtonSave = CheckDetails.VCLObject("btnSave")
  ButtonSave.Click(61, 12)  
  Log.Checkpoint("SUCCESS: CloseCheckPanel()")
  Indicator.PopText()
  
def ProcessCashCheck():
  #Author Pat Holman 12/30/2020
  #Last Modified by Pat Holman 12/30/2020
  Indicator.PushText("Run ProcessCashCheck()")
  cashwise = Sys.Process("Cashwise")
  PointOfSale = cashwise.VCLObject("frmSaleEdit")
  ProcessButton = PointOfSale.VCLObject("pnlMain").VCLObject("pnlSaleItems").VCLObject("pnlBottom").VCLObject("pnlProcess").VCLObject("pnlProcessContainer").VCLObject("btnProcess")
  ProcessButton.Click(30, 32)
  PayoutPopUp = cashwise.VCLObject("frmAliasBrowse")
  PayoutType = PayoutPopUp.VCLObject("pnlMain").VCLObject("sbData").VCLObject("lbAliases")
  PayoutType.ClickItem("Cash")
  SaveButton = PayoutPopUp.VCLObject("btnSave")
  SaveButton.Click(30, 10)
  SaveButton = cashwise.VCLObject("frmSaleConfirmPayout").VCLObject("btnSave")
  SaveButton.Click(51, 12)
  PreviewForm = cashwise.VCLObject("RavePreviewForm")
  PreviewForm.Close()
  CloseButton = PointOfSale.VCLObject("btnClose")
  CloseButton.Click(34, 12)  
  Log.Checkpoint("SUCCESS: OpenCheckPanel()")
  Indicator.PopText()
 

