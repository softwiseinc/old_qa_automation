﻿TestedApps.Cashwise.Run()
from IntergrationHelper import *
#from TestUsers import *

#from SalesHelper import *
#from CustomerHelper import *
#from EmploymentHelper import *
#from TestUsers import *
 #---------------------------------------------------------------------------------THIS TESTCASE HAS TO USE A DATABASE WITH IIS (UTAHTEST)                                
def TestAPR_OriginAndFund():
  #Author Phil Ivey 02/26/2020
  #Last Modified by Phil Ivey 03/05/2020
  
  Indicator.PushText("RUNNING: APR_Funding_Date")
  
  managerUser = GetManager001Database("UtahTest") # managerUser["Database"],["Server"],["userPass"],["001"]
  #managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])  

#----GET 1 SINGLE PAYMENT LOAN TYPE TO SET APR TO FUNDING DATE  
  paydayType = "D89"  #GetSinglePayWithACH()
  personalType = "PAB"  #GetInstallWithACH()
  cashwise = Sys.Process("Cashwise")
  cusID = GetAppCusNoLoan("B")
  nextPayDate =  UpdateCustomerEmploymentBiWeekly(cusID)
  
  SetupAPRtoCalculateOnFunding(paydayType,personalType)  ##--------------------------------------------------------------Setup APR for Funding Date, Allow Edit Funding Date--------
  
  crossPlatformObj = GetCashwiseLoanData(cusID,nextPayDate,paydayType,personalType) ##---------------------Open Cashwise Return Data----------------------------------------------

  #-- TEST FUNDING DATE ------- TEST FUNDING DATE ------- TEST FUNDING DATE ------- TEST FUNDING DATE ------- TEST FUNDING DATE -----
  crossPlatformObj["loanType"] = "payday"
  CCOapplyForLoanFunding(crossPlatformObj)
                                
  crossPlatformObj["loanType"] = "personal"
  CCOapplyForLoanFunding(crossPlatformObj)

# ------------------------------------------------SETUP APR TO START AT ORIGINATION DATE
  CashwiseMainMenu_File("Setup")
  SystemSetupTreeView("Location Settings")
  funDate = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.pgDeferredLocation.tsMain.DBRadioGroup1
  funDate.Window("TGroupButton", "Origination Date").ClickButton()

  SystemSetupTreeView("Loan Type Selection")
  pControl = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.PageControl1
  pControl.ClickTab("&Location")
  pControl.tsLocation.DBRadioGroup1.Window("TGroupButton", "Use tree view form for this location").ClickButton()
  SystemSetupTreeView("Save & Close")
 
 # ---- TEST ORIGINATION DATE ----- ---- TEST ORIGINATION DATE --------- TEST ORIGINATION DATE --------- TEST ORIGINATION DATE --------- TEST ORIGINATION DATE -----
 
  crossPlatformObj["loanType"] = "payday"
  CCOapplyForLoanOrigination(crossPlatformObj)
  
  crossPlatformObj["loanType"] = "personal"
  CCOapplyForLoanOrigination(crossPlatformObj)
  
  crossPlatformObj["loanType"] = "payday"
  CCOapplyForLoanOrigination(crossPlatformObj)

  crossPlatformObj["completeLoan"] = True
  crossPlatformObj["loanType"] = "payday"
  CCOapplyForLoanOrigination(crossPlatformObj)
  Log.Message("TestAPR_OriginAndFund Completed")
##---------------------------------------------------------------------------------------------------------------------