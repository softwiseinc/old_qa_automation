﻿from SalesHelper import *
from CustomerHelper import *
from EmploymentHelper import *



#---------------------------------------------------------------------------------------------------------------------    
def CCOapplyForLoanFunding(crossPlatformObj):
  
  
#  crossPlatformObj = {}
#  crossPlatformObj["loanType"] = "payday"
#  crossPlatformObj["cusID"] = "UTO-124901828"
#  crossPlatformObj["webName"] = "aqa16@brianjohnson.work"
#  crossPlatformObj["webPass"] = "Diehard1!"
#  crossPlatformObj["sameDaySingleAPR"] = ""
#  crossPlatformObj["twoDaySingleAPR"] = "464.05"
#  crossPlatformObj["sameDayInstallAPR"] = ""
#  crossPlatformObj["same2DayInstallAPR"] = "387.92"
#  crossPlatformObj["sameDayInstallTotalInterest"] = ""
#  crossPlatformObj["same2DayInstallTotalInterest"] = "1192.77"
#  crossPlatformObj["sameDaySingleTotalInterest"] = ""
#  crossPlatformObj["nextDaySingleTotalInterest"] = "132.86"
  
  
  cusID = crossPlatformObj["cusID"]
  webName = crossPlatformObj["webName"]
  webPword = crossPlatformObj["webPass"]
  loanType =  crossPlatformObj["loanType"]
  cashInstallFundingAPR = crossPlatformObj["same2DayInstallAPR"]
  cashInstallFundingCharge = crossPlatformObj["same2DayInstallTotalInterest"]
  cashwiseSinglePayFundingAPR = crossPlatformObj["twoDaySingleAPR"]
  cashwiseSinglePayFundingCharge = crossPlatformObj["nextDaySingleTotalInterest"]
  
  Browsers.Item[btChrome].Navigate("http://members.ccoc7.com/memberlogin.aspx")  
  Delay(500)
 
  browser = Sys.Browser("chrome")
  page = browser.Page("http://members.ccoc7.com/memberlogin.aspx")
  boxUserName = page.Find("ObjectIdentifier","textUsername",12)
  Delay(200)
  boxUserName.SetText(webName)
  nextButton = page.Find("ObjectIdentifier","btnNext",12)
  Delay(200)
  nextButton.Click()
  Delay(1000)
  boxUserPassWord = page.Panel(0).Panel(0).Panel("content").Panel("formWrapper").Form("aspnetForm").Panel("secEmail").Find("ObjectIdentifier","textPassword",5)
  Delay(1000)
  boxUserPassWord.SetText(webPword)
  loginButton = page.Find("ObjectIdentifier","logMeBtn",12)
  Delay(200)
  loginButton.Click()
  
  if(Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/MemberSecurity.aspx", 1000).Exists):
    
    page = Sys.Browser("chrome").Page("http://members.ccoc7.com/MemberSecurity.aspx")
    # COMPLETE YOUR SECURITY SETUP BELOW
 
    answer1 = page.Find("ObjectIdentifier","A1",9)
    answer1.SetText("cat")
  
    answer2 = page.Find("ObjectIdentifier","A2",9)
    answer2.SetText("cat")
  
    answer3 = page.Find("ObjectIdentifier","A3",9)
    answer3.SetText("cat")
  
    answer4 = page.Find("ObjectIdentifier","A4",9)
    answer4.SetText("cat")
  
    subButton = page.Find("ObjectIdentifier","secCreateSubmit",7)
    subButton.Click()
  
  Delay(500)
   
  # SELECT A LOAN
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/member.aspx")


  if(crossPlatformObj["loanType"] == "payday"):
    payDayLoanButton = page.Find("ObjectIdentifier","hlNewLoanButtonsPaydayUrl",9)
    Delay(200)
    payDayLoanButton.Click()
    if(payDayLoanButton.Exists == True):
      Delay(500)     
      payDayLoanButton.Click()
  else:
    personalLoanButton = page.Find("ObjectIdentifier","hlNewLoanButtonsPersonalUrl",9)  
    Delay(500)
    personalLoanButton.Click()
    Delay(500)
    while(personalLoanButton.Exists == True):
      Delay(500)
      personalLoanButton.Click()
     
  #CONFIRM BANK ACCOUNT
  
  cusRoutingAccountNum = GetCustomerBankAccount(cusID)
  
  stringLen = aqString.GetLength(cusRoutingAccountNum)
  aqString.SubString(cusRoutingAccountNum,0,8)
  route = aqString.SubString(cusRoutingAccountNum,0,9)
  acct = aqString.SubString(cusRoutingAccountNum,9,stringLen)
  
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/store-bank-edit.aspx?type=req&lt=*")
  
  routeBox = page.Find("ObjectIdentifier","routingNumber",9)
  Delay(200)

  if(routeBox.readOnly == False):
    routeBox.SetText(route)
    routeBox.Keys("[Tab]")
  
    acctBox = page.Find("ObjectIdentifier","bankAccount",9)
    Delay(200)
    acctBox.SetText(acct) 
    acctBox.Keys("[Tab]")
  
    acctBoxcheck = page.Find("ObjectIdentifier","bankAccountCheck",9)
    Delay(200)  
    acctBoxcheck.SetText(acct) 
    acctBoxcheck.Keys("[Tab]")
  
    vselect = page.Find("ObjectIdentifier","bankAccountLengthYear",9)
    vselect.ClickItem("4")
    vselect.Keys("[Tab]")
    vselect = page.Find("ObjectIdentifier","bankAccountLengthMonth",9)
    Delay(200)
    vselect.ClickItem("4")
    vselect.Keys("[Tab]")
  
  #BANK ACCOUNT FOR PAYMENTS
  
  if(page.Find("contentText","Yes",11).Exists):
    Delay(200)
    yesButton = page.Find("contentText","Yes",11)
    yesButton.Click()
  
  #CONFIRM BANK ACCOUNT BUTTON
  
  confirmButton = page.Find("ObjectIdentifier","confirmBank",7)
  confirmButton.Click()
  
  #Congratulations!
  
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Approval.aspx?id="+cusID+"&state=UT&ach=1*", 5000)
       
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Approval.aspx?id="+cusID+"&state=UT&ach=1*")
  Delay(800)
  loanAmountBox = page.Find("ObjectIdentifier","txtLoanAmount",7)
  Delay(800)
  loanAmountBox.SetText("950")
  
  getButton = page.Find("ObjectIdentifier","btnCreateLoan",7)
  Delay(800)
  getButton.Click()
  Delay(1000)
  
  
  #Loan Details
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Details.aspx?cid="+cusID+"&lid=UTO*", 2000)
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Details.aspx?cid="+cusID+"&lid=UTO*")
  Delay(800)
  continueButton = page.Find("contentText","Continue",8)
  Delay(800)
  continueButton.Click()
  Delay(500)
  if(continueButton.Exists == True):
       continueButton.Click()
  
  #Review and sign the following disclosures
  #Personal Loan Agreement
  Delay(2000)

  
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Disclosure.aspx*", 5000)
  panel = Sys.Browser().Page("http://members.ccoc7.com/Loan_Disclosure.aspx*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1").Summary(0).Panel("disclosureNum1")#.Panel(0).Panel(1).Panel(0).Frame("pdfdoc1").Panel("viewer").Panel("canvasContainer")
  panel2 = panel.Find("ObjectIdentifier","canvasContainer",6)
  panel2.Click(830, 54)
  Delay(500)
  page2 = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Disclosure.aspx?cid="+cusID+"&refi=0&lid=UTO-*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1").Summary(0).Panel("disclosureNum1").Panel(0).Panel(1).Panel(0).Frame("pdfdoc1").Panel("viewer").Panel("canvasContainer").Panel("pageContainer1")#.Panel("textLayer1")#.Panel(25)  
  
  if(crossPlatformObj["loanType"] == "personal"):
      aprBox = page2.Find("Name","Panel(25)",2)
      Delay(200)
      readAPR = aprBox.contentText
      readAPRLen = aqString.GetLength(readAPR)
      readAPR = aqString.Remove(readAPR,6,readAPRLen)
      Delay(200)
      Log.Message("Agreement APR is "+readAPR)
      Log.Message("APR from Cashwise is "+cashInstallFundingAPR)
      if(readAPR == cashInstallFundingAPR):
        Log.Message("-------------  Passed  Cashwise and CCO are the Same------------")
      else:
        Log.Error("--------- FAILED  APR's are different-----------")
  
      chargeBox = page2.Find("Name","Panel(30)",2)
      Delay(200)
      readTotalCharge = chargeBox.contentText
      readTotalCharge = aqString.Replace(readTotalCharge,"$","")
      readTotalCharge = aqString.Replace(readTotalCharge,"(e","")
      readTotalCharge = aqString.Replace(readTotalCharge,")","")
  
      Log.Message("----------")
      Log.Message("Cashwise Installment Funding Charge = "+cashInstallFundingCharge)
      Log.Message("Agreement Charge = "+readTotalCharge)
  
      if(cashInstallFundingCharge == readTotalCharge):
        Log.Message("------------- PASSED --Total Charge for CCO is the same as Cashwise-------")
      else:
        Log.Error("-----------FAILED CCO Charge is not the same as Cashwise -------------")
    
  if(crossPlatformObj["loanType"] == "payday"):
      aprBox = page2.Find("Name","Panel(40)",2)
      Delay(200)
      readAPR = aprBox.contentText
      readAPRLen = aqString.GetLength(readAPR)
      readAPR = aqString.Remove(readAPR,6,readAPRLen)
      Delay(200)
      Log.Message("Agreement APR is "+readAPR)
      Log.Message("APR from Cashwise is "+cashwiseSinglePayFundingAPR)
      if(readAPR == cashwiseSinglePayFundingAPR):
        Log.Message("-------------  Passed  Cashwise APR and CCO APR are the Same------------")
      else:
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
      chargeBox = page2.Find("Name","Panel(45)",2)
      Delay(200)
      readTotalCharge = chargeBox.contentText
      readTotalCharge = aqString.Replace(readTotalCharge,"$","")
      readTotalCharge = aqString.Replace(readTotalCharge,"(e","")
      readTotalCharge = aqString.Replace(readTotalCharge,")","")
      Log.Message("Agreement Total Charge is "+readTotalCharge)
      Log.Message("Charge from Cashwise is "+cashwiseSinglePayFundingCharge)
      if(readTotalCharge == cashwiseSinglePayFundingCharge):
        Log.Message("-------------  Passed  Cashwise and CCO Interest Charges are the Same------------")
      else:
        Log.Error("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
     
  page3 = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Disclosure.aspx?cid="+cusID+"&*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1")
   
  Delay(200)
  cancelButton = page3.Find("contentText","Cancel",4)
  cancelButton.Click()
  
  page.Panel("cancelLoanModal").Panel(2).Link(0).Click()  # Sorry to see you go YES
  
  logout = Sys.Browser("chrome").Page("http://members.ccoc7.com/member.aspx").Header(0).Panel(0).Panel(0).Panel("topNav").Panel("loginTab").TextNode(1).Link(1)
  Delay(500)
  logout.Click()
  Delay(500)
  while(logout.Exists == True):
    Delay(500)
    logout.Click()
    
  
    
def CCOapplyForLoanOrigination():
  
  
  crossPlatformObj = {}
  crossPlatformObj["loanType"] = "personal"
  crossPlatformObj["cusID"] = "UTO-124901828"
  crossPlatformObj["webName"] = "aqa16@brianjohnson.work"
  crossPlatformObj["webPass"] = "Diehard1!"
  crossPlatformObj["sameDaySingleAPR"] = "464.06"
  crossPlatformObj["sameDayInstallAPR"] = "387.92"
  crossPlatformObj["sameDayInstallTotalInterest"] = "1213.22"
  crossPlatformObj["sameDaySingleTotalInterest"] = "144.94"
  crossPlatformObj["completeLoan"] = True

  
  completeLoan = crossPlatformObj["completeLoan"]
  cusID = crossPlatformObj["cusID"]
  webName = crossPlatformObj["webName"]
  webPword = crossPlatformObj["webPass"]
  loanType =  crossPlatformObj["loanType"]
  sameDaySingleAPR = crossPlatformObj["sameDaySingleAPR"] 
  sameDayInstallAPR = crossPlatformObj["sameDayInstallAPR"] 
  sameDayInstallTotalInterest = crossPlatformObj["sameDayInstallTotalInterest"]
  
  sameDaySingleTotalInterest = crossPlatformObj["sameDaySingleTotalInterest"] 
  
  Browsers.Item[btChrome].Navigate("http://members.ccoc7.com/memberlogin.aspx")  
  Delay(500)
 
  browser = Sys.Browser("chrome")
  page = browser.Page("http://members.ccoc7.com/memberlogin.aspx")
  boxUserName = page.Find("ObjectIdentifier","textUsername",12)
  Delay(200)
  boxUserName.SetText(webName)
  nextButton = page.Find("ObjectIdentifier","btnNext",12)
  Delay(200)
  nextButton.Click()
  Delay(1000)
  boxUserPassWord = page.Panel(0).Panel(0).Panel("content").Panel("formWrapper").Form("aspnetForm").Panel("secEmail").Find("ObjectIdentifier","textPassword",5)
  Delay(1000)
  boxUserPassWord.SetText(webPword)
  loginButton = page.Find("ObjectIdentifier","logMeBtn",12)
  Delay(200)
  loginButton.Click()
  
  if(Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/MemberSecurity.aspx", 1000).Exists):
    
    page = Sys.Browser("chrome").Page("http://members.ccoc7.com/MemberSecurity.aspx")
    # COMPLETE YOUR SECURITY SETUP BELOW
 
    answer1 = page.Find("ObjectIdentifier","A1",9)
    answer1.SetText("cat")
  
    answer2 = page.Find("ObjectIdentifier","A2",9)
    answer2.SetText("cat")
  
    answer3 = page.Find("ObjectIdentifier","A3",9)
    answer3.SetText("cat")
  
    answer4 = page.Find("ObjectIdentifier","A4",9)
    answer4.SetText("cat")
  
    subButton = page.Find("ObjectIdentifier","secCreateSubmit",7)
    subButton.Click()
  
  Delay(500)
   
  # SELECT A LOAN
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/member.aspx")


  if(crossPlatformObj["loanType"] == "payday"):
    payDayLoanButton = page.Find("ObjectIdentifier","hlNewLoanButtonsPaydayUrl",9)
    Delay(200)
    payDayLoanButton.Click()
    Delay(200)
    if(payDayLoanButton.Exists == True):
      Delay(500)     
      payDayLoanButton.Click()
  else:
    personalLoanButton = page.Find("ObjectIdentifier","hlNewLoanButtonsPersonalUrl",9)  
    Delay(500)
    personalLoanButton.Click()
    Delay(500)
    while(personalLoanButton.Exists == True):
      Delay(500)
      personalLoanButton.Click()
     
  #CONFIRM BANK ACCOUNT
  
  cusRoutingAccountNum = GetCustomerBankAccount(cusID)
  
  stringLen = aqString.GetLength(cusRoutingAccountNum)
  aqString.SubString(cusRoutingAccountNum,0,8)
  route = aqString.SubString(cusRoutingAccountNum,0,9)
  acct = aqString.SubString(cusRoutingAccountNum,9,stringLen)
  
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/store-bank-edit.aspx?type=req&lt=*")
  
  routeBox = page.Find("ObjectIdentifier","routingNumber",9)
  Delay(200)

  if(routeBox.readOnly == False):
    routeBox.SetText(route)
    routeBox.Keys("[Tab]")
  
    acctBox = page.Find("ObjectIdentifier","bankAccount",9)
    Delay(200)
    acctBox.SetText(acct) 
    acctBox.Keys("[Tab]")
  
    acctBoxcheck = page.Find("ObjectIdentifier","bankAccountCheck",9)
    Delay(200)  
    acctBoxcheck.SetText(acct) 
    acctBoxcheck.Keys("[Tab]")
  
    vselect = page.Find("ObjectIdentifier","bankAccountLengthYear",9)
    vselect.ClickItem("4")
    vselect.Keys("[Tab]")
    vselect = page.Find("ObjectIdentifier","bankAccountLengthMonth",9)
    Delay(200)
    vselect.ClickItem("4")
    vselect.Keys("[Tab]")
  
  #BANK ACCOUNT FOR PAYMENTS
  
  if(page.Find("contentText","Yes",11).Exists):
    Delay(200)
    yesButton = page.Find("contentText","Yes",11)
    yesButton.Click()
  
  #CONFIRM BANK ACCOUNT BUTTON
  
  confirmButton = page.Find("ObjectIdentifier","confirmBank",7)
  confirmButton.Click()
  
  #Congratulations!
  
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Approval.aspx?id="+cusID+"&state=UT&ach=1*", 5000)
       
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Approval.aspx?id="+cusID+"&state=UT&ach=1*")
  Delay(800)
  loanAmountBox = page.Find("ObjectIdentifier","txtLoanAmount",7)
  Delay(800)
  loanAmountBox.SetText("950")
  
  getButton = page.Find("ObjectIdentifier","btnCreateLoan",7)
  Delay(800)
  getButton.Click()
  Delay(1000)
  
  
  #Loan Details
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Details.aspx?cid="+cusID+"&lid=UTO*", 2000)
  page = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Details.aspx?cid="+cusID+"&lid=UTO*")
  Delay(800)
  continueButton = page.Find("contentText","Continue",8)
  Delay(800)
  continueButton.Click()
  Delay(500)
  if(continueButton.Exists == True):
       continueButton.Click()
  
  #Review and sign the following disclosures
  #Personal Loan Agreement
  Delay(3000)

  
  Sys.Browser("chrome").WaitPage("http://members.ccoc7.com/Loan_Disclosure.aspx*", 9000)
  panel = Sys.Browser().Page("http://members.ccoc7.com/Loan_Disclosure.aspx*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1").Summary(0).Panel("disclosureNum1")#.Panel(0).Panel(1).Panel(0).Frame("pdfdoc1").Panel("viewer").Panel("canvasContainer")
  panel2 = panel.Find("ObjectIdentifier","canvasContainer",6)
  panel2.Click(830, 54)
  Delay(1000)
  page2 = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Disclosure.aspx?cid="+cusID+"&refi=0&lid=UTO-*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1").Summary(0).Panel("disclosureNum1").Panel(0).Panel(1).Panel(0).Frame("pdfdoc1").Panel("viewer").Panel("canvasContainer").Panel("pageContainer1")#.Panel("textLayer1")#.Panel(25)  
  
  if(crossPlatformObj["loanType"] == "personal"):
      aprBox = page2.Find("Name","Panel(25)",2)
      Delay(200)
      readAPR = aprBox.contentText
      readAPRLen = aqString.GetLength(readAPR)
      readAPR = aqString.Remove(readAPR,6,readAPRLen)
      Delay(200)
      Log.Message("Agreement APR is "+readAPR)
      Log.Message("APR from Cashwise is "+sameDayInstallAPR)
      if(readAPR == sameDayInstallAPR):
        Log.Message("-------------  Passed  Cashwise and CCO are the Same------------")
      else:
        Log.Error("--------- FAILED  APR's are different-----------")
  
      chargeBox = page2.Find("Name","Panel(30)",2)
      Delay(200)
      readTotalCharge = chargeBox.contentText
      readTotalCharge = aqString.Replace(readTotalCharge,"$","")
      readTotalCharge = aqString.Replace(readTotalCharge,"(e","")
      readTotalCharge = aqString.Replace(readTotalCharge,")","")
  
      Log.Message("----------")
      Log.Message("Cashwise Installment Funding Charge = "+sameDayInstallTotalInterest)
      Log.Message("Agreement Charge = "+readTotalCharge)
  
      if(sameDayInstallTotalInterest == readTotalCharge):
        Log.Message("------------- PASSED --Total Charge for CCO is the same as Cashwise-------")
      else:
        Log.Message("-----------FAILED CCO Charge is not the same as Cashwise -------------")
        Log.Message("-----------FAILED CCO Charge is not the same as Cashwise -------------")
        Log.Message("-----------FAILED CCO Charge is not the same as Cashwise -------------")
        Log.Message("-----------FAILED CCO Charge is not the same as Cashwise -------------")
        Log.Message("-----------FAILED CCO Charge is not the same as Cashwise -------------")
    
  if(crossPlatformObj["loanType"] == "payday"):
      aprBox = page2.Find("Name","Panel(40)",2)
      Delay(200)
      readAPR = aprBox.contentText
      readAPRLen = aqString.GetLength(readAPR)
      readAPR = aqString.Remove(readAPR,6,readAPRLen)
      Delay(200)
      Log.Message("Agreement APR is "+readAPR)
      Log.Message("APR from Cashwise is "+sameDaySingleAPR)
      if(readAPR == sameDaySingleAPR):
        Log.Message("-------------  Passed  Cashwise APR and CCO APR are the Same------------")
      else:
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
        Log.Message("--APR FAILED------- FAILED  APR's are different---APR FAILED--------")
      chargeBox = page2.Find("Name","Panel(45)",2)
      Delay(200)
      readTotalCharge = chargeBox.contentText
      readTotalCharge = aqString.Replace(readTotalCharge,"$","")
      readTotalCharge = aqString.Replace(readTotalCharge,"(e","")
      readTotalCharge = aqString.Replace(readTotalCharge,")","")
      Log.Message("Agreement Total Charge is "+readTotalCharge)
      Log.Message("Charge from Cashwise is "+sameDaySingleTotalInterest)
      if(readTotalCharge == sameDaySingleTotalInterest):
        Log.Message("-------------  Passed  Cashwise and CCO Interest Charges are the Same------------")
      else:
        Log.Message("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
        Log.Message("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
        Log.Message("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
        Log.Message("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
        Log.Message("Charges FAILED------- FAILED  Charges are different---Charges FAILED--------")
     
  
      if(completeLoan == False):      
        page3 = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Disclosure.aspx?cid="+cusID+"&*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1")
   
        Delay(200)
        cancelButton = page3.Find("contentText","Cancel",4)
        cancelButton.Click()
  
        page.Panel("cancelLoanModal").Panel(2).Link(0).Click()  # Sorry to see you go YES
  
        logout = Sys.Browser("chrome").Page("http://members.ccoc7.com/member.aspx").Header(0).Panel(0).Panel(0).Panel("topNav").Panel("loginTab").TextNode(1).Link(1)
        Delay(500)
        logout.Click()
        Delay(500)
        while(logout.Exists == True):
          Delay(500)
          logout.Click()

      elif(completeLoan == True): 
       page3 = Sys.Browser("chrome").Page("http://members.ccoc7.com/Loan_Disclosure.aspx?cid="+cusID+"&*").Panel(0).Panel(0).Panel(0).Panel("ReviewTerm").Details("detSign1")
       Delay(200)
       signBox = page3.Find("ObjectIdentifier","loandoc1",4)
       signBox.SetText("Cussign")
       clickAgree = page3.Find("ObjectIdentifier","loanbutton1",9)
       clickAgree.Click()
      else:
        Log.Message("This test has completeded") 
          
def UpdateCustomerEmploymentBiWeekly(cusID):
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Edit")
  nextPayDate =  UpdateEmploymentBiWeekly()
  cashwise.frmCustomerEdit.btnSave.Click()
  CustomerBrowseSearchBrowseScreen("","ID","Save & Close")
  return nextPayDate  
  
  
def SetupAPRtoCalculateOnFunding(paydayType,personalType):
  CashwiseMainMenu_File("Setup")
  paydayIndexInfo = GetDefaultIndexInfo(paydayType )# tableIndex has info for quering, aQuery,Column,ColoumnValue
#----GET 1 INSTALLMENT LOAN TYPE TO SET APR TO FUNDING DATE
  
  personalIndexInfo = GetDefaultIndexInfo(personalType)
# GET BOTH LOAN TYPE INDEX FOR EDITING
  payIndex = GetTableIndex(paydayIndexInfo)
  perIndex = GetTableIndex(personalIndexInfo)
#  payIndex = aqConvert.StrToInt(GetTableIndex(paydayIndexInfo))
#  perIndex = aqConvert.StrToInt(GetTableIndex(personalIndexInfo))
  SystemSetupTreeView("Location Settings")
  funDate = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.pgDeferredLocation.tsMain.DBRadioGroup1
  funDate.Window("TGroupButton", "Funding Date").ClickButton()
  SystemSetupTreeView("Loan Types")
  loanGrid = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.Panel1.BrowseGrid1
# EDIT PAYDAY TYPE 
  loanGrid.Keys("[Home]")
  for x in range(0,payIndex):
    loanGrid.Keys("[Down]")
  Delay(200)
  cashwise.frmDeferredTypeSetup.btnEdit.Click()
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
    cashwise.Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
  pControl = cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings
  pControl.ClickTab("&3 Duration")
  pControl.tsDuration.gbDelayedFunding.DBCheckBox7.ClickButton(cbChecked)
  cashwise.frmDeferredSetup.btnSave.Click()
  # EDIT PAYDAY TYPE 
  loanGrid.Keys("[Home]")
  for x in range(0,perIndex):
    loanGrid.Keys("[Down]")
  Delay(200)
  cashwise.frmDeferredTypeSetup.btnEdit.Click()
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
    cashwise.Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
  pControl = cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings
  pControl.ClickTab("&3 Duration")
  pControl.tsDuration.gbDelayedFunding.DBCheckBox7.ClickButton(cbChecked)
  cashwise.frmDeferredSetup.btnSave.Click()
  Delay(200)
  SystemSetupTreeView("Loan Type Selection")
  pControl = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.PageControl1
  pControl.ClickTab("&Location")
  pControl.tsLocation.DBRadioGroup1.Window("TGroupButton", "Use classic*").ClickButton()
  SystemSetupTreeView("Save & Close")
# 
#  # END# ------------------------------------------------SETUP APR TO START AT FUNDING DATE, ALLOW EDIT FUNDING DATE

def GetCashwiseLoanData(cusID,nextPayDate,paydayType,personalType): 

#   ---------APR by funding date-------Single Payment Cashwise
  locID = GetLocationIDFromfrmMainWndCap()
  cashDate = GetLocationSystemDate(locID)
  Log.Message("Current Cashwise Date is "+cashDate)
  fundingDateToday = cashDate
  fundingDateTomorrow = aqDateTime.AddDays(cashDate,1)
  fundingDate2Tomorrow = aqDateTime.AddDays(cashDate,2)
  webLoginName = GetWebCusEmail(cusID)
  webLoginPassword = GetWebCusPassword(cusID)
  OpenPOS()
  SalesTransactionButton("Cash Advance")
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
  Delay(300)
  cashwise.frmDeferredSelect.btnDeferredAdd.Click()
  Delay(800)
  cashwise.WaitWindow("TfrmDeferredTypeList","Cash Advance Type List",1,1000)
  cashwise.frmDeferredTypeList.pnlSearch.icMain.TAdvancedSpeedButton.Click()
  cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home]")
  cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Down]")
  cashwise.frmAbstractList.btnSave.Click()
  cashwise.frmDeferredTypeList.pnlSearch.isMain.Keys(paydayType)
  cashwise.frmDeferredTypeList.btnSave.Click()
  Delay(800)
 #  while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 
  while(cashwise.WaitWindow("TMessageForm", "*",1,800).Exists):
      Delay(200)
      cashwise.Window("TMessageForm", "*", 1).VCLObject("OK").Click()
      Delay(400)
  Log.Message("Start Cash Advance")
  caPage = cashwise.frmDeferredNewEdit.pnlMain.sbData.pnlLeft.pnlDetail
  caPage.edDepositDate.Window("TDBDateEdit", "", 1).Keys(nextPayDate)

  caPage.edFundingDate.Window("TDBDateEdit", "", 1).Keys(fundingDateToday)
  caPage.edFundingDate.Window("TDBDateEdit", "", 1).Keys("[Tab]")
  Delay(300)
  Sys.Process("Cashwise").VCLObject("frmDeferredNewEdit").VCLObject("dbtAdvance").Click()
  cashwise.frmDeferredNewEdit.dbtAdvance.Keys("[Home]![End][Del]")
  cashwise.frmDeferredNewEdit.dbtAdvance.Keys("950")
  cashwise.frmDeferredNewEdit.dbtAdvance.Keys("[Tab]")
  Delay(300)
  sameDaySingleAPR = caPage.dbtApr.wText  #464.0700
  sameDaySingleTotalInterest = GetDeltaFeesRateCurrentProposal()#169.10
  sameDaySingleTotalInterest = aqConvert.FloatToStr(sameDaySingleTotalInterest)
  #Edit the funding date should change the APR
  caPage.edFundingDate.Window("TDBDateEdit", "", 1).Keys(fundingDateTomorrow)
  caPage.edFundingDate.Window("TDBDateEdit", "", 1).Keys("[Tab]")
  twoDaySingleAPR = caPage.dbtApr.wText  #464.0500
  nextDaySingleTotalInterest = GetDeltaFeesRateCurrentProposal()
  nextDaySingleTotalInterest = aqConvert.FloatToStr(nextDaySingleTotalInterest)
  if(sameDaySingleAPR != twoDaySingleAPR):
    Log.Message("-------APR by funding date on single payment loans passed in Cashwise---------")
  else:
    Log.Error("------------APR on funding on single payment loans date did not change, test failed----------")
  
  
  sameDaySingleAPR = aqString.Replace(sameDaySingleAPR,"%","")  
  fsameDaySingleAPR = aqConvert.StrToFloat(sameDaySingleAPR)
  fsameDaySingleAPR = round(fsameDaySingleAPR,2)
  sameDaySingleAPR = aqConvert.FloatToStr(fsameDaySingleAPR)  
    
  twoDaySingleAPR = aqString.Replace(twoDaySingleAPR,"%","")  
  ftwoDaysingleAPR = aqConvert.StrToFloat(twoDaySingleAPR)
  ftwoDaysingleAPR = round(ftwoDaysingleAPR,2)
  twoDaySingleAPR = aqConvert.FloatToStr(ftwoDaysingleAPR)  
  
  cashwise.frmDeferredNewEdit.btnClose.Click()
  cashwise.frmSaleEdit.btnClose.Click()
 # -END  --------APR by funding date-------Single Payment Cashwise
# ########################################################################################################################## 
# 
# */
  # ---------APR by funding date------- Installment Cashwise
  
  OpenPOS()
  SalesTransactionButton("Cash Advance")
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
  cashwise.frmDeferredSelect.btnDeferredAdd.Click()
  Delay(800)
  cashwise.WaitWindow("TfrmDeferredTypeList","Cash Advance Type List",1,1000)
  cashwise.frmDeferredTypeList.pnlSearch.icMain.TAdvancedSpeedButton.Click()
  cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Home]")
  cashwise.frmAbstractList.pnlMain.dbgMain.Keys("[Down]")
  cashwise.frmAbstractList.btnSave.Click()
  cashwise.frmDeferredTypeList.pnlSearch.isMain.Keys(personalType)
  cashwise.frmDeferredTypeList.btnSave.Click()
  Delay(2000)
 #  while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 
  while(cashwise.WaitWindow("TMessageForm", "*",1,800).Exists):
      Delay(200)
      cashwise.Window("TMessageForm", "*", 1).VCLObject("OK").Click()
      Delay(400)
  Log.Message("Start New Installment Loan")
  
  newInstall = cashwise.frmDeferredNewLoanEdit.pnlMain.sbData.pnlInstallmentLoan.pnlPaymentInformation.pnlStandardPattern
  newInstall.dbedLoanPrincipal.Click()
  newInstall.dbedLoanPrincipal.Keys("950")
  newInstall.dbedLoanPrincipal.Keys("[Tab]")
  
  Delay(800)
#  sameDayAPRpic = cashwise.frmDeferredNewLoanEdit.dbtAPR.Picture()   #387.9161
  InstallAPRBox = cashwise.frmDeferredNewLoanEdit.pnlMain.sbData.pnlInstallmentLoan.pnlPaymentInformation.pnlStaticDetailOne.dbtAPR
  sameDayInstallAPR = InstallAPRBox.wText
  
  newLoanIDBox = cashwise.frmDeferredNewLoanEdit.pnlMain.sbData.txtLoanID
  newLoanID = newLoanIDBox.wText
  
  sameDayInstallTotalInterest = GetTotalInterestFromSchedule(newLoanID) #1233.78
  sameDayInstallTotalInterest = aqConvert.FloatToStr(sameDayInstallTotalInterest)
  
  newInstall.edFundingDate.TAdvancedSpeedButton.Click()
  cashwise.frmCalendarEdit.Panel1.btnNextDay.Click()
  #cashwise.frmCalendarEdit.Panel1.btnNextDay.Click() # 2 day
 # cashwise.frmCalendarEdit.Panel1.btnNextDay.Click()
  cashwise.frmCalendarEdit.btnSave.Click()
  Delay(500)
    
#  same2DayInstallAPRpic = cashwise.frmDeferredNewLoanEdit.dbtAPR.Picture() # 387.9343

  same2DayInstallAPR = InstallAPRBox.wText
  
  same2DayInstallTotalInterest = GetTotalInterestFromSchedule(newLoanID) #1213.22
  same2DayInstallTotalInterest = aqConvert.FloatToStr(same2DayInstallTotalInterest)
  
  if(sameDayInstallAPR != same2DayInstallAPR):
    Log.Message("-------APR by funding date on Installment loans passed in Cashwise---------")
  else:
    Log.Error("-------APR by funding date on Installment loans FAILED in Cashwise---------")
  
  cashwise.frmDeferredNewLoanEdit.btnClose.Click()
  cashwise.frmSaleEdit.btnClose.Click()
  
  sameDayInstallAPR = aqString.Replace(sameDayInstallAPR,"%","")  
  sameDayInstallAPR = aqConvert.StrToFloat(sameDayInstallAPR)
  sameDayInstallAPR = round(sameDayInstallAPR,2)
  sameDayInstallAPR = aqConvert.FloatToStr(sameDayInstallAPR)  
  
  same2DayInstallAPR = aqString.Replace(same2DayInstallAPR,"%","")  
  same2DayInstallAPR = aqConvert.StrToFloat(same2DayInstallAPR)
  same2DayInstallAPR = round(same2DayInstallAPR,2)
  same2DayInstallAPR = aqConvert.FloatToStr(same2DayInstallAPR)  
  
       
  Log.Message("CusID = "+cusID)
  Log.Message("Same day single APR = "+sameDaySingleAPR)
  Log.Message("Two day single APR = "+twoDaySingleAPR) 
  Log.Message("Same day install APR = "+sameDayInstallAPR)
  Log.Message("Same day plus 1 install APR = "+same2DayInstallAPR)
  
  crossPlatformObj = {}
  crossPlatformObj["loanType"] = ""
  crossPlatformObj["cusID"] = cusID
  crossPlatformObj["webName"] = webLoginName
  crossPlatformObj["webPass"] = webLoginPassword
  crossPlatformObj["sameDaySingleAPR"] = sameDaySingleAPR
  crossPlatformObj["twoDaySingleAPR"] = twoDaySingleAPR
  crossPlatformObj["sameDayInstallAPR"] = sameDayInstallAPR
  crossPlatformObj["same2DayInstallAPR"] = same2DayInstallAPR 
  crossPlatformObj["sameDayInstallTotalInterest"] = sameDayInstallTotalInterest #1233.78
  crossPlatformObj["same2DayInstallTotalInterest"] = same2DayInstallTotalInterest#1213.22
  crossPlatformObj["sameDaySingleTotalInterest"] = sameDaySingleTotalInterest #169.10
  crossPlatformObj["nextDaySingleTotalInterest"] = nextDaySingleTotalInterest #157.02
  crossPlatformObj["completeLoan"] = False
  return crossPlatformObj