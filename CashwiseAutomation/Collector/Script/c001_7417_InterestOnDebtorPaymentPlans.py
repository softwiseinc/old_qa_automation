﻿from Helpers import *



def c001_InterestOnDebtorPaymentPlans():  #Author Phil Ivey 8/12/2019
    #Last Modified by Phil Ivey 8/12/2019 
    cashwise = Sys.Process("Cashwise")
    
    CashwisaMainMenu_File("Setup")
    #SystemSetupTree("SMS")
    setupTree = cashwise.VCLObject("frmSystemSetup").VCLObject("pnlMain").VCLObject("sbData").VCLObject("tvMain")
    setupTree.ClickItem("|Collections|Configuration")
  
   # Add new debt amount type
    cashwise.frmCollectorConfigurationSetup.btnFeeTypes.Click()
    cashwise.frmDebtFeeTypeBrowse.btnAdd.Click()
    cashwise.frmDebtFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType.dblefAmountType.TAdvancedSpeedButton.Click()
 # Check to see if CCC is there
    cashwise.frmDebtAmountTypeBrowse.pnlSearch.isMain.Keys("CCC")
    cashwise.frmDebtAmountTypeBrowse.btnSave.Click()
    if not (cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists):
       # LOG SOMETHING GO TO ADD DEBT FEE TYPE
      Log.Message("CCC already exists")
    else:
      #ADD 
        cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click() 
        cashwise.frmDebtAmountTypeBrowse.btnAdd.Click()
        cashwise.frmDynamicEdit.pnlMain.sbData.dbeCode.Keys("CCC")
        cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("15 Day Collection Charge")
        cashwise.frmDynamicEdit.btnSave.Click()
    #    Sys.Process("Cashwise").VCLObject("frmDebtFeeTypeEdit").VCLObject("btnClose")
    #    cashwise.frmDebtAmountTypeBrowse.btnClose.Click()
       
    cashwise.frmDebtFeeTypeEdit.btnClose.Click()
    cashwise.frmDebtFeeTypeBrowse.btnClose.Click()
    
      
    cashwise.frmCollectorConfigurationSetup.btnDebtTypes.Click()
    
    cashwise.frmDebtTypeBrowse.pnlSearch.isMain.Keys("DEF")  #DEBT TYPES
    cashwise.frmDebtTypeBrowse.btnEdit.Click()
    
    cashwise.frmDebtTypeEdit.pnlMain.sbData.pnlInfo.dblefPaymentApplicationOrder.TAdvancedSpeedButton.Click()  # DEBT TYPE  PAYMENT APPLICATION ORDER
    cashwise.frmDebtPaymentPlanTypeBrowse.pnlSearch.isMain.Keys("CC")
    cashwise.frmDebtPaymentPlanTypeBrowse.btnSave.Click()
    
    if not (cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists):
       # LOG SOMETHING GO TO ADD DEBT FEE TYPE
      Log.Message("CC already exists")
    else:
      #ADD APP ORDERr
         varNameArray = ["CRE","FEE",	"INT",	"RET",	"CCC",	"LAT",	"PRI"]
         cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  # click ok to window where cc is not there
         cashwise.frmDebtPaymentPlanTypeBrowse.btnAdd.Click()# THE FOLLOWING FEES WILL BE----ADD
         
         
        
         for x in range(6): 
             
             cashwise.frmDebtPaymentPlanTypeEdit.btnAddApplicationOrder.Click()# ADD AMOUNT TYPES (CREDIT, INTEREST...)
             cashwise.frmDynamicEdit.pnlMain.sbData.dbleAmountType_Code.TAdvancedSpeedButton.Click() #AMOUNT TYPE LOOKUP
             cashwise.frmDebtAmountTypeBrowse.pnlSearch.isMain.Keys(varNameArray[x])
             cashwise.frmDebtAmountTypeBrowse.btnSave.Click()
             cashwise.frmDynamicEdit.btnSave.Click()
        
             
         cashwise.frmDebtPaymentPlanTypeEdit.pnlMain.sbData.pnlPaymentPlan.dbedDescription.Keys("Check City")
         cashwise.frmDebtPaymentPlanTypeEdit.btnSave.Click()
         
         
         
         
    while cashwise.frmDebtTypeEdit.btnDeleteFeeType.Enabled == True:
        cashwise.frmDebtTypeEdit.btnDeleteFeeType.Click()
        cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
        
    
    
     #  ADD DEBT FEE TYPE CC
    cashwise.frmDebtTypeEdit.btnAddFeeType.Click()
    cashwise.frmDebtFeeTypeBrowse.pnlSearch.isMain.Keys("CC")
    cashwise.frmDebtFeeTypeBrowse.btnSave.Click()
    
    if not (cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists):
       # LOG SOMETHING GO TO ADD DEBT FEE TYPE
      Log.Message("CC already exists")
    else:
      #ADD
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  # click o
      cashwise.frmDebtFeeTypeBrowse.btnAdd.Click()
      
      feeType = cashwise.frmDebtFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType
      feeType.dbedDescription.Keys("15 Day Collection Charge")
     # feeType.dblefAmountType.TAdvancedSpeedButton
      feeType.dblefAmountType.Keys("CCC")
      feeType.dblefIntervalType.Keys("D")
      feeType.dbedAmount.Keys("20")
      feeType.dbedMaxPeriods.Keys("15")
      feeType.dbedCalculationProc.Keys("UtahCollectionCharge")
      feeType.DBEdit1.Keys("DebtorCheckCityPaymentplanProjectFees")
      amountType = cashwise.frmDebtFeeTypeEdit.pnlMain.sbData.pnlAmountTypes.bgAmountTypes
      
      tableIndex = {}
      tableIndex["SqlStatement"] = "SELECT * FROM DebtAmountType"
      tableIndex["Coloumn"] = "Code"
      tableIndex["ColoumnValue"] = "PRI"  
      index = GetTableIndex(tableIndex)
      Log.Message(index)  
      index = aqConvert.StrToInt(index)
      index = index - 1
      amountType.Keys("[Home]")
      for x in range(index): 
        amountType.Keys("[Down]")
      amountType.HScroll.Pos = 0
      amountType.VScroll.Pos = 17
      amountType.Click(335, 260)
      cashwise.frmDebtFeeTypeEdit.btnSave.Click()
#      cashwise.frmDebtFeeTypeBrowse.btnSave.Click()
    

  
       #  ADD DEBT FEE TYPE DIN
    cashwise.frmDebtTypeEdit.btnAddFeeType.Click()
    cashwise.frmDebtFeeTypeBrowse.pnlSearch.isMain.Keys("DIN")
    cashwise.frmDebtFeeTypeBrowse.btnSave.Click()
     
    if not (cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists):
       # LOG SOMETHING GO TO ADD DEBT FEE TYPE
      Log.Message("CC already exists")
    else:
      #ADD
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  # click o
      cashwise.frmDebtFeeTypeBrowse.btnAdd.Click()
      
      feeType = cashwise.frmDebtFeeTypeEdit.pnlMain.sbData.pnlLoanFeeType
      feeType.dbedDescription.Keys("Daily Interest")
     # feeType.dblefAmountType.TAdvancedSpeedButton
      feeType.dblefAmountType.Keys("INT")
      feeType.dblefIntervalType.Keys("D")
      feeType.dbrgFlatOrRate.Window("TGroupButton", "Rate", 1).Click()
      feeType.dbedAmount.Keys("0")
      feeType.dbedMaxPeriods.Keys("70")
      feeType.dbedCalculationProc.Keys("DebtCheckCityFeeCalculatorDaily")
      feeType.DBEdit1.Keys("DebtorCheckCityPaymentplanProjectInterest")
      amountType = cashwise.frmDebtFeeTypeEdit.pnlMain.sbData.pnlAmountTypes.bgAmountTypes
      
      tableIndex = {}
      tableIndex["SqlStatement"] = "SELECT * FROM DebtAmountType"
      tableIndex["Coloumn"] = "Code"
      tableIndex["ColoumnValue"] = "PRI"  
      index = GetTableIndex(tableIndex)
      Log.Message(index)  
      index = aqConvert.StrToInt(index)
      amountType.Keys("[Home]")
      for x in range(index): 
        amountType.Keys("[Down]")
      amountType.HScroll.Pos = 0
      amountType.VScroll.Pos = 17
      amountType.Click(335, 255)
      cashwise.frmDebtFeeTypeEdit.btnSave.Click()
    
    # CLICK THE CHARGE TAB  cashwise.frmDebtTypeEdit.pnlMain.sbData.pcDetails
    pageControl = cashwise.frmDebtTypeEdit.pnlMain.sbData.pcDetails
    pageControl.ClickTab("Charges")
    cashwise.frmDebtTypeEdit.btnAddCharge.Click()
    
    
    cashwise.frmDebtTypeEdit.btnAddFeeType.Click()
    
  
    
#     
#  tfrmDebtTypeBrowse = cashwise.VCLObject("frmDebtTypeBrowse")
#  incrementalSearch = tfrmDebtTypeBrowse.VCLObject("pnlSearch").VCLObject("isMain")
#  incrementalSearch.Click(42, 20)
#  incrementalSearch.Click(48, 17)
#  incrementalSearch.Keys("def")
#  tfrmDebtTypeBrowse.VCLObject("btnEdit").Click(27, 22)
#  cashwise.VCLObject("frmDebtTypeEdit").VCLObject("btnAddFeeType").Click(24, 20)
#  cashwise.VCLObject("frmDebtFeeTypeBrowse").VCLObject("btnAdd").Click(5, 28)