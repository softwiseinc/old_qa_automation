﻿from Panel_Employment import *
TestedApps.Cashwise.Run()

def Test_AddEmployment():
  #Author Brian Johnson 1/8/2021
  #Last Modified by Brian Johnson 1/8/2021  
  Indicator.PushText("RUNNING: Test_AddEmployment()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  CreateCustomer(customer)
  AddNewEmployer(customer)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: Test_AddEmployment() New Employment Added")
  Indicator.PopText()