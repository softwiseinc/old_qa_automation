﻿from Panel_PointOfSale import *
TestedApps.Cashwise.Run()

def Test_SellLoanTypeD89UtahTest():
    #Author Brian Johnson 1/7/2021
    #Last Modified by Brian Johnson 1/8/2021
    Indicator.PushText("RUNNING: Test_SellLoanType89()")
    managerUser = GetManagerLocation001()
    StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
    customer = GetApprovedCustomer()
    CreateCustomer(customer)
    AddBankAccount(customer)
    OpenPOS()
    ClickCashAdvanceButton()  
    ExplicitWait(2)
    SearchCustomerFirstName(customer)
    SellLoanTypeD89(customer)
    Log.Event("PASSED TEST: Test_SellLoanType89()")
    Indicator.PopText()
    CloseCashwiseApp()