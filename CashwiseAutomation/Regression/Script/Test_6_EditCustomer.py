﻿from Panel_Customer import *
TestedApps.Cashwise.Run()

def Test_EditCustomer():
  #Author Pat Holman 5/30/2019
  #Last Modified by Brian Johnson 5/3/2021
  Indicator.PushText("RUNNING: TestEditCustomer()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  editCustomer = GetEditCustomer()
  CreateCustomer(customer)
  EditCustomer(customer, editCustomer)
  DeleteCustomer(editCustomer)
  CloseCashwiseApp()  
  Log.Event("SUCCESS: AddCustomer() New Customer Added")
  Indicator.PopText()