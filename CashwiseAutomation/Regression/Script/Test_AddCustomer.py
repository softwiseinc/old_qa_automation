﻿from Panel_Customer import *
TestedApps.Cashwise.Run()


def Test_AddCustomer():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/17/2019
  Indicator.PushText("RUNNING: TestAddCustomer()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  CreateCustomer(customer)
  DeleteCustomer(customer)
  CloseCashwiseApp()  
  Log.Event("PASSED TEST: AddCustomer() New Customer Added")
  Indicator.PopText()