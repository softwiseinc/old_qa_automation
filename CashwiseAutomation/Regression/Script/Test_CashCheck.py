﻿from Panel_Check import *
TestedApps.Cashwise.Run()

def Test_CashCheck():
  #Author <name> 12/18/2020
  #Last Modified by <name> 12/22/2020
  Indicator.PushText("what is running")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetApprovedCustomer()
  CreateCustomer(customer)
  AddBankAccount(customer)
  OpenPOS()
  CashCheck(customer)
  CloseCashwiseApp() 
  Log.Checkpoint("SUCCESS: what ran was successful")
  Indicator.PopText()
