﻿from BasePage import *
from Panel_Repay import *

def GetBankCardCustomer(customer):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("GetBankCardCustomer()")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  Indicator.PopText()
  
def AddStoredBankCard(card, customer):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddStoredBankCard()")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  cashwise.VCLObject("frmCustomerEdit").MainMenu.Click("[2]|[20]")
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnAdd").Click()
  ExplicitWait(3)
  AddCardHolderName(card)
  AddDebitCardNumber(card)
  AddSecurityCode(card)
  AddExpirationMonth(card)
  AddExpirationYear(card)
  AddZip(card)
  ClickSave()
  ClickDone()
  Indicator.PopText()
  
def DeleteStoredCard():
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddStoredBankCard()")
  cashwise.VCLObject("frmCustomerEdit").MainMenu.Click("[2]|[20]")
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnDelete").Click()
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()
  Indicator.PopText()
  
def BackToCustomerBrowseFromStoredCards(): 
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In BackToCustomerBrowseFromStoredCards()")
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnSave").Click()
  cashwise.VCLObject("frmCustomerEdit").VCLObject("btnSave").Click()
  Indicator.PopText()
  
def BackToMainFromStoredCards(): 
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In BackToCustomerBrowseFromStoredCards()")
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnSave").Click()
  cashwise.VCLObject("frmCustomerEdit").VCLObject("btnSave").Click()
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("btnSave").Click()
  Indicator.PopText()
  
def TestDebitCard():
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  customer = GetTempCustomer()
  newCard = {}
  newCard["Name"] = customer["FirstName"] + " " + customer["LastName"]
  newCard["DebitCardNumber"] = "4024140000000008"
  newCard["SecurityCode"] = "989"
  newCard["Month"] = "09"
  newCard["Year"] = "25"
  newCard["Zip"] = "22222"
  return newCard
  Indicator.PopText()
  