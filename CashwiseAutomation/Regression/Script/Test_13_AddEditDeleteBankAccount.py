﻿from Panel_Bank import *
TestedApps.Cashwise.Run()

def Test_AddEditDeleteBankAccount():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  Indicator.PushText("RUNNING: AddEditDeleteBankAccounty()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  OpenAccounts()
  AddAccount()
  EditDeleteAccountNumber()  
  CloseCashwiseApp()   
  Log.Event("SUCCESS: AddEditDeleteBankAccount")
  Indicator.PopText()