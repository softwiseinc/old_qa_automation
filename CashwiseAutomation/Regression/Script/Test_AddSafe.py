﻿from Panel_CashLoctaions import *
TestedApps.Cashwise.Run()

def Test_AddSafe():
  #Author Pat Holman 4/212021
  #Last Modified by Pat Holman 04/21/2021  
  Indicator.PushText("RUNNING: Test_AddSafe")
  safeName = "zzz This is a cool safe"
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  OpenCashManagement()
  AddSafeToCashLocation(safeName)
  DeleteNewSafe(safeName)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: Test_AddSafe")
  Indicator.PopText()


