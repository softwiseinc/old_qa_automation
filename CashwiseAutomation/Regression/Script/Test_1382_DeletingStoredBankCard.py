﻿from BankCardHelper import *
from ContactHelper import *
from Panel_Customer import *
from Panel_PointOfSale import *
from Panel_Employment import *
TestedApps.Cashwise.Run()

def Test_1382_DeletingStoredBankCard():
  #Author Brian Johnson 5/6/2021
  #Last Modified by Brian Johnson 5/6/2021
  #This test ensures that a bank card cannot be deleted if is associated with an active debtor payment plan or loan
  Indicator.PushText("RUNNING: Test_1382_DeletingStoredBankCard()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  customer = GetTempCustomer()
  card = TestDebitCard()
  CreateCustomer(customer)
  AddNewEmployer(customer)
  AddStoredBankCard(card, customer)
  BackToMainFromStoredCards()
  OpenPOS()
  ClickCashAdvanceButton()  
  ExplicitWait(2)
  SearchCustomerFirstName(customer)
  SellLoanTypeD89BankCard(customer)
  
  
  
#  GetBankCardCustomer(customer)
#  DeleteStoredCard()
#  BackToCustomerBrowseFromStoredCards()
#  DeleteCustomer(customer)
#  CloseCashwiseApp()   
  Log.Event("SUCCESS: Test_1382_DeletingStoredBankCard() Stored Bank Card Deleted")
  Indicator.PopText()