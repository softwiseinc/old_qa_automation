﻿from Panel_PointOfSale import *
TestedApps.Cashwise.Run()

def Test_SellLoanTypeTLT():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("RUNNING: SellLoanTypeTLTTest()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetApprovedCustomer()
  CreateCustomer(customer)
  AddBankAccount(customer)
  OpenPOS()
  ClickCashAdvanceButton()  
  ExplicitWait(2)
  SearchCustomerFirstName(customer)
  SellLoanTypeTLT(customer)
  Log.Event("PASSED TEST: SellLoanTypeTLTTest()")
  Indicator.PopText()
  CloseCashwiseApp()