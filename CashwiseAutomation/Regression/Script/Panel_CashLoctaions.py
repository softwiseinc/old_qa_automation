﻿from CashLocationsHelper import *

def AddSafeToCashLocation(safeName):
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021  
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: GenerateUniqueSSN")
    ClickAddButton()
    AddDescription(safeName)
    SetMaxManagers("2")
    SelectSafe()
    ClickEditAmountsCheckBox()
    ClickEditCountsCheckBox()
    open = True
    close = True
    AuditInventory(open, close)
    SaveCloseAddCashStorageType()
    SaveCloseCashLocations()
    Log.Checkpoint("SUCCESS: Add safe to location is successful.")
    Indicator.PopText()
  
def AddDescription(description):
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: AddDescription(description)")
    CashAddForm = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData")
    DescriptionInput = CashAddForm.VCLObject("edDescription")
    DescriptionInput.Click()
    DescriptionInput.SetText(description)
    Log.Checkpoint("SUCCESS: Cash location description is set to: " + description)
    Indicator.PopText()
   
def SetMaxManagers(maxNumber):
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: SetMaxManagers(maxNumber)")
    CashAddForm = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData")
    MaxManagerInput = CashAddForm.VCLObject("edMaxControllers")
    MaxManagerInput.Click()
    MaxManagerInput.SetText(maxNumber)
    ClickTypeSpeedButton()
    Log.Checkpoint("SUCCESS: Max number of managers set to: " + maxNumber)
    Indicator.PopText()
    
def ClickTypeSpeedButton():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: ClickTypeSpeedButton()")
    typeSpeedButton = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edType").VCLObject("TAdvancedSpeedButton")
    typeSpeedButton.Click()
    Log.Checkpoint("SUCCESS: Click Speed button to add cash storage type")
    Indicator.PopText()
    
def SelectSafe():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: SelectSafe()")
    BrowseTable = cashwise.VCLObject("frmCashTypeBrowse").VCLObject("pnlSearch").VCLObject("isMain")
    #set focut to top row
    BrowseTable.Keys("[PageUp][PageUp]")
    # select 3rd item
    BrowseTable.Keys("[Down][Down][Enter]")
    ExplicitWait(1)
    Log.Checkpoint("SUCCESS: Select type Safe")
    Indicator.PopText()
    
def ClickEditAmountsCheckBox():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: ClickEditAmountsCheckBox()")
    EditAmountCheckBox = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("chkEditAmounts")
    EditAmountCheckBox.ClickButton(cbChecked)
    Log.Checkpoint("SUCCESS: Clicked Checkbox to edit amounts")
    Indicator.PopText()

def ClickEditCountsCheckBox():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: ClickEditCountsCheckBox()")
    EditCountsCheckBox = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("chkEditCounts")
    EditCountsCheckBox.ClickButton(cbChecked)
    Log.Checkpoint("SUCCESS: Clicked Checkbox to edit counts")
    Indicator.PopText()

def AuditInventory(open, close):
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: AuditInventory(open, close)")
    AuditGroupBox = cashwise.VCLObject("frmCashAdd").VCLObject("pnlMain").VCLObject("sbData").VCLObject("grbAuditInventory")
    if open:
        button = AuditGroupBox.VCLObject("chkAuditInventoryOnOpen")
        button.ClickButton(cbChecked)
    if close:
            button = AuditGroupBox.VCLObject("chkAuditInventoryOnClose")
            button.ClickButton(cbChecked)
    Log.Checkpoint("SUCCESS: AuditInventory checked for open & close)")
    Indicator.PopText()
    
def SaveCloseAddCashStorageType():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: CloseLocationAdd()")
    CloseSaveButton = cashwise.VCLObject("frmCashAdd").VCLObject("btnSave")
    CloseSaveButton.Click()
    Log.Checkpoint("SUCCESS: Save and Close the add cash storage type dialog box")
    Indicator.PopText()
    
def SaveCloseCashLocations():
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: CloseLocationAdd()")
    CloseSaveButton = cashwise.VCLObject("frmCashBrowse").VCLObject("btnSave")
    CloseSaveButton.Click()
    Log.Checkpoint("SUCCESS: Save and Close the add cash location panel")
    Indicator.PopText()

def DeleteNewSafe(safeName):
    #Author Pat Holman 4/21/2021
    #Last Modified by Pat Holman 4/21/2021
    cashwise = Sys.Process("Cashwise")
    Indicator.PushText("RUNNING: DeleteNewSafe()")
    cashwise.VCLObject("frmMain").MainMenu.Click("Cash|Cash Management...")
    BrowseCashLocations = cashwise.VCLObject("frmCashBrowse")
    BrowseCashLocations.VCLObject("pnlSearch").VCLObject("isMain")
    # set focus to the bottom of the list
    BrowseCashLocations.Keys("[PageDown][PageDown][PageDown][PageDown][PageDown][PageDown][PageDown][PageDown]")
    DeleteButton = BrowseCashLocations.VCLObject("btnDelete")
    DeleteButton.Click()
    ConfirmYesButton = cashwise.Window("TMessageForm", "Confirm").VCLObject("Yes")
    ConfirmYesButton.ClickButton()
    SaveCloseCashLocation = BrowseCashLocations.VCLObject("btnSave")
    SaveCloseCashLocations()
    Log.Checkpoint("SUCCESS: New safe was deleted")
    Indicator.PopText()