﻿from Panel_Customer import *
TestedApps.Cashwise.Run()


def Test_AddCustomer():
  #Author Pat Holman 4/24/2019
  #Last Modified by Brian Johnson 5/3/2021
  Indicator.PushText("RUNNING: TestAddCustomer()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  CreateCustomer(customer)
  OpenCustomerBrowser()
  customerBrowseTable = cashwise.frmCustomerBrowse
  SelectUseSmartSearch()
  searchInput = customerBrowseTable.pnlSearch.isMain
  searchInput.Keys(customer["LastName"])
  DeleteCustomer(customer)
  CloseCashwiseApp()  
  Log.Event("PASSED TEST: AddCustomer() New Customer Added")
  Indicator.PopText()