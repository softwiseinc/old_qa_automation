﻿TestedApps.Cashwise.Run()
from UsersHelper import *

def Test_AddUser():
  #Author Pat Holman 4/24/2019
  #Last Modified by Brian Johnson 5/3/2021
  Indicator.PushText("RUNNING: TestAddNewUser()")
  managerUser = GetManagerLocation001()
  cashwise = StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])    
  OpenUserMaintenance()
  cashwiseUser = GetUser001()
  AddNewUser(cashwiseUser)      
  ValidateNewUserCreated(cashwiseUser)
  DeleteUserIfRandom(cashwiseUser)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestAddUser()")
  Indicator.PopText()