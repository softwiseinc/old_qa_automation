﻿from Panel_User import *
TestedApps.Cashwise.Run()

def Test_AssignUserLocation():
  #Author Pat Holman 4/24/2019
  #Last Modified by Brian Johnson 4/29/2021
  Indicator.PushText("RUNNING: TestAssignLocation()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])    
  tempUser = GetTempUser()
  AssignLocation(tempUser);
  LoginUser(tempUser)
  IsTrueUserLoginNewLocation(tempUser)
  LoginUser(managerUser)
  DeleteTempUser(tempUser)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: A new user was created")
  Indicator.PopText()