﻿TestedApps.Cashwise.Run()
from Panel_Customer import *

def Test_AddAdditionalCustomerInformation():
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019
  Indicator.PushText("RUNNING: TestAddAdditionalCustomerInformation()")   
  managerUser = GetManager001()
  Log.Message("This is before start cashwise, database = "+managerUser["database"])
  Log.Message("This is before start cashwise, database = "+managerUser["Password"])
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  customer = GetTempCustomer()
  CreateCustomer(customer)
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  AddAdditionalInformation(customer)
  SaveAndCloseCustomerEdit()
  CloseCustomerBrowse()
  DeleteCustomer(customer)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: AddAdditionalCustomerInformation()Information is Added")
  Indicator.PopText()