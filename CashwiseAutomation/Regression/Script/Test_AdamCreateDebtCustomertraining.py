﻿from Panel_Employment import *
from Panel_Debt import *
from Panel_Bank import *
TestedApps.Cashwise.Run()
def Test_CreateDebtCustomer():
  #Author Brian Johnson 1/8/2021
  #Last Modified by Adam Gibbons 04/14/2021
    Indicator.PushText("RUNNING: Test_CreateDebtCustomer()")
    managerUser = GetManagerLocation001()
    StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
    customer = GetDebtCustomer()
    CreateCustomer(customer)
    AddBankAccount(customer)
    AddManualDebt(customer)
    Log.Event("PASSED TEST: Test_CreateDebtCustomer()")
    Indicator.PopText()
    CloseCashwiseApp()