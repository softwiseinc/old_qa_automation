﻿from Panel_PointOfSale import *
TestedApps.Cashwise.Run()


def Test_SellLoanType89():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("RUNNING: Test_SellLoanType89()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetApprovedCustomer()
  CreateCustomer(customer)
  AddBankAccount(customer)
  OpenPOS()
  ClickCashAdvanceButton()  
  ExplicitWait(2)
  SearchCustomerFirstName(customer)
  SellLoanTypeD89(customer)
  Log.Event("PASSED TEST: Test_SellLoanType89()")
  Indicator.PopText()
  CloseCashwiseApp()