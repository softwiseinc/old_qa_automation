﻿from Panel_PointOfSale import *
TestedApps.Cashwise.Run()

def Test_SellLoanType_PAB():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("RUNNING: Test_SellLoanTypePAB()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetApprovedCustomer()
  CreateCustomer(customer)
  AddBankAccount(customer)
  OpenPOS()
  ClickCashAdvanceButton()  
  ExplicitWait(2)
  SearchCustomerFirstName(customer)
  SellLoanTypePAB(customer)
  Log.Event("PASSED TEST: Test_SellLoanTypePAB()")
  Indicator.PopText()
  CloseCashwiseApp()