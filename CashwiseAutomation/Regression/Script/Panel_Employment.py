﻿from EmploymentHelper import *
from Panel_Customer import CreateCustomer

def AddNewEmployer(customer):
     #Author Pat Holman 5/24/2019
     #Last Modified by Brian Johnson 5/18/2021  
     cashwise = Sys.Process("Cashwise")
     OpenCustomerBrowser()
     SelectCustomerByName(customer)
     OpenCustomerEdit()
     OpenEmployerContact()
     OpenAddEmployerContact()
     # Field inputs 
     department = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedDepartment
     workPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedPhone
     workPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedExtension
     supervisor = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisor
     supervisorPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorPhone
     supervisorPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorExtension
     grossPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGrossPay
     netPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedNetPay
     garnishmet = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGarnishments
     hasDirectDeposit = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbHasDirectDeposit
     preferredEmployer = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.cbPreferredEmployer
     workStartTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStartTime
     workStopTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStopTime
     workDayMonday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayMonday
     workDayTuesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayTuesday
     workDayWednesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayWednesday
     workDayThursday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayThursday
     workDayFriday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayFriday
     saveAndCloseButton = cashwise.frmContactEmployerEdit.btnSave
     # Set Employer information
     SelectEmployerName(customer)
     SetTypeOfIncome()
     SelectStartDate(3)  
     department.SetText(customer["Department"])
     SelectPosition(customer)
     workPhone.SetText(customer["WorkPhone"])
     workPhoneExtention.SetText(customer["WorkPhoneExtention"])
     supervisor.SetText(customer["Supervisor"])
     supervisorPhone.SetText(customer["SupervisorPhone"])
     supervisorPhoneExtention.SetText(customer["SupervisorPhoneExtention"])
     SelectPayPeriod(customer)
     
     if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
       cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
       
     
     grossPay.SetText(customer["GrossPay"])
     netPay.SetText(customer["NetPay"])
     garnishmet.SetText(customer["Garnishment"])
     ExplicitWait(1)
     cashwise.frmContactEmployerEdit.Click(7,7)
     ExplicitWait(2) 
     hasDirectDeposit.Click()
     preferredEmployer.Click()
     workStartTime.SetText(customer["WorkStartTime"])
     workStopTime.SetText(customer["WorkStopTime"])
     workDayMonday.ClickButton(cbChecked)
     workDayTuesday.ClickButton(cbChecked)
     workDayWednesday.ClickButton(cbChecked)
     workDayThursday.ClickButton(cbChecked)
     workDayFriday.ClickButton(cbChecked)
     saveAndCloseButton.Click()
     SaveAndCloseContactEmployers()
     cashwise.frmCustomerEdit.btnSave.Click(63, 5)
     cashwise.frmCustomerBrowse.btnSave.Click(48, 9)
       
def EditCustomerEmployment(customer, editCustomer):
     #Author Pat Holman 5/24/2019
     #Last Modified by Pat Holman 5/28/2019  
     cashwise = Sys.Process("Cashwise")
     OpenCustomerBrowser()
     SelectCustomerByName(customer)
     OpenCustomerEdit() 
     OpenEmployerContact()
     EditSelectedEmployer()
     employerData = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings
     tableIndex = {}
     tableIndex["SqlStatement"] = "SELECT * FROM DeferredType"
     tableIndex["Coloumn"] = "ID"
     tableIndex["ColoumnValue"] = "ILC"  
     SelectPayPeriod(editCustomer)
     SelectPosition(editCustomer)
     employerData.dbedGrossPay.SetText(editCustomer["GrossPay"])
     employerData.dbedNetPay.SetText(editCustomer["NetPay"])
     employerData.dbedGarnishments.SetText(editCustomer["Garnishment"])
     employerData.dbckbHasDirectDeposit.ClickButton(cbUnchecked)
     employerData.Keys("[F9]")
     cashwise.frmContactEmployerList.btnSave.Click()
     SaveAndCloseCustomerEdit()
     CloseCustomerBrowse()
