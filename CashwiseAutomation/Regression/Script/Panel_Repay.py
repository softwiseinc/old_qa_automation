﻿from BankCardHelper import *

def AddCardHolderName(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddCardHolderName()")
  CardHolderName = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(2).Textbox("checkout_*")
  CardHolderName.Keys(card["Name"])
  Indicator.PopText()
  
def AddDebitCardNumber(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddDebitCardNumber()")
  debitCardNumber = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(3).Textbox("checkout_*")
  debitCardNumber.Keys(card["DebitCardNumber"])
  Indicator.PopText()
  
def AddSecurityCode(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddSecurityCode()")
  securityCode = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(4).Textbox("checkout_*")
  securityCode.Keys(card["SecurityCode"])
  Indicator.PopText()
  
def AddExpirationMonth(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddExpirationMonth()")
  expirationMonth = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(5).Panel(0).Select("card_expiration_month")
  expirationMonth.Keys(card["Month"])
  Indicator.PopText()
  
def AddExpirationYear(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddExpirationYear()")
  expirationYear = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(5).Panel(0).Select("card_expiration_year")
  expirationYear.Keys(card["Year"])
  Indicator.PopText()
  
def AddZip(card):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("AddZip()")
  zip = cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(1).Panel(0).Fieldset(6).Textbox("checkout_*")
  zip.Keys(card["Zip"])
  Indicator.PopText()
  
def ClickSave():
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("ClickSave()")
  cashwise.VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout*").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Form(0).Panel(4).Button(0).Click()
  Indicator.PopText()
  
def ClickDone():
  #Author Brian Johnson 5/18/2021
  #Last Modified by Brian Johnson 5/18/2021
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("ClickSave()")
  Sys.Process("Cashwise").VCLObject("frmRepay").VCLObject("Browser").Window("Shell DocObject View", "", 1).Window("Internet Explorer_Server", "", 1).Page("https://qaccifinancial.sandbox.repay.io/checkout/#/success").Panel("app").Panel(0).Panel(0).Panel(0).Panel(0).Link(0).Click()
  Indicator.PopText()