﻿from PointOfSaleHelper import *
from LoanHelper import *
from CustomerHelper import *
from Panel_Bank import AddBankAccount
from Panel_Customer import CreateCustomer

def Panel_Template_Function():
  #Author <name> 12/18/2020
  #Last Modified by <name> 12/22/2020
  Indicator.PushText("what is running")
  
  # code
  
  Log.Checkpoint("SUCCESS: what ran was successful")
  Indicator.PopText()


def SellLoanTypeTLT(customer):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("Sell PAB - Personal ACH Bi-Weekly")
  SelectLoanType("|TLT - Title Loan Installment ACH AutoPay")
  SetTitleInformation()
  SetTLTLoanAmount(505)
  ProcessSale()
  Log.Checkpoint("SUCCESS: PAB - Personal ACH Bi-Weekly")
  Indicator.PopText()
  

def SellLoanTypeD89(customer):
  #Author Pat Holman 12/18/2020
  #Last Modified by Adam Gibbons 04/21/2021
  Indicator.PushText("Sell Loan Type 89")
  SelectLoanType("|Payday Loans|Deferred 8.9%")
  #SkipLoanWarnings()
  SetDueDate(7)
  SetCashAdvanceLoanAmount(502)
  SaveCloseCashAdvancePanel()
  SelectBankAccount()
  CollectCheck(customer)
  ProcessSale()
  Log.Checkpoint("SUCCESS: Sell Loan Type 89")
  Indicator.PopText()           

def SellLoanTypePAS(customer):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("Sell PAB - Personal ACH Bi-Weekly")
  SelectLoanType("|PAS - Personal ACH Semi-Monthly")
  SetPASLoanAmount(504)
  ProcessSale()
  Log.Checkpoint("SUCCESS: PAB - Personal ACH Bi-Weekly")
  Indicator.PopText()

def SellLoanTypePAB(customer):
  #Author Pat Holman 12/18/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("Sell PAB - Personal ACH Bi-Weekly")
  SelectLoanType("|PAB - Personal ACH Bi-Weekly")
  SetAutoPayACH()
  SetPABLoanAmount(503)  
  ProcessSale()
  Log.Checkpoint("SUCCESS: PAB - Personal ACH Bi-Weekly")
  Indicator.PopText()
  
def SellLoanTypeD89BankCard(customer):
  #Author Brian Johnson 5/17/2021
  #Last Modified by Brian Johnson 5/17/2021
  Indicator.PushText("Sell Loan Type 89 with Bank Card")
  SelectLoanType("|Payday Loans|Deferred 8.9%")
  #SkipLoanWarnings()
  SetDueDate(7)
  CashAdvanceLoanAmount(250)
  SetD89BankCollateral()
  SaveCloseCashAdvancePanel()
  CollectCheck(customer)
  ProcessSale()
  Log.Checkpoint("SUCCESS: Sell Loan Type 89 with Bank Card")
  Indicator.PopText()