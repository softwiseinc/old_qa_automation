﻿from ContactHelper import *
from Panel_Customer import *
TestedApps.Cashwise.Run()

def Test_EditContactCompany():
  #Author Brian Johnson 5/6/2021
  #Last Modified by Brian Johnson 5/6/2021
  Indicator.PushText("RUNNING: EditContactCompany()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  contact = GetCompanyContact()
  editContact = GetCompanyContact2()
  OpenContactBrowser()
  ClickAddContactCompany()
  AddContactCompany(contact)
  EditContactCompany(editContact)
  DeleteCompanyContact()
  CloseCashwiseApp()   
  Log.Event("SUCCESS: EditContactCompany() Company Added and Edited")
  Indicator.PopText()