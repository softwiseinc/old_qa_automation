﻿from BasePage import *
from Helpers import *

def OpenCashManagement():
  #Author Pat Holman 4/21/2021
  #Last Modified by Pat Holman 4/21/2021  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("RUNNING:  OpenCashManagement()")
  TopMenu = cashwise.VCLObject("frmMain").MainMenu
  TopMenu.Click("Cash|Cash Management...")
  Log.Checkpoint("SUCCESS: Open Cash Manager Panel")
  Indicator.PopText()
  
def ClickAddButton():
   #Author Pat Holman 4/21/2021
   #Last Modified by Pat Holman 4/21/2021  
   cashwise = Sys.Process("Cashwise")
   Indicator.PushText("RUNNING: ClickAddButton")
   AddButton = cashwise.VCLObject("frmCashBrowse").VCLObject("btnAdd")
   AddButton.Click()
   ExplicitWait(1) 
   Log.Checkpoint("SUCCESS: Click the 'Add' button")
   Indicator.PopText()