﻿from ContactHelper import *
from Panel_Customer import *
TestedApps.Cashwise.Run()

def Test_EditContactPerson():
  #Author Brian Johnson 5/6/2021
  #Last Modified by Brian Johnson 5/6/2021
  Indicator.PushText("RUNNING: EditContactPerson()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  contact = GetTempContact2()
  editContact = GetTempContact()
  OpenContactBrowser()
  ClickAddContactButton()
  AddContactPerson(contact)
  EditContactPerson(editContact)
  DeleteContact(contact)
  CloseCashwiseApp()   
  Log.Event("SUCCESS: EditContactPerson() Contact Added and Edited")
  Indicator.PopText()