﻿from ContactHelper import *
TestedApps.Cashwise.Run()


def Test_AddContactPerson():
  #Author Brian Johnson 5/3/2021
  #Last Modified by Brian Johnson 5/6/2021
  Indicator.PushText("RUNNING: TestAddContactPerson()")
  managerUser = GetManagerLocation001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  contact = GetTempContact2()
  OpenContactBrowser()
  ClickAddContactButton()
  AddContactPerson(contact)
  DeleteContact(contact)
  CloseCashwiseApp()  
  Log.Event("PASSED TEST: AddContactPerson() New Contact Added")
  Indicator.PopText()