﻿from CheckHelper import *
from LoanHelper import *
from PointOfSaleHelper import *
from CustomerHelper import *
from Panel_Customer import CreateCustomer
from Panel_Bank import AddBankAccount


def CashCheck(customer):
  #Author Pat Holman 12/29/2020
  #Last Modified by Pat Holman 12/29/2020
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("Cash a check for a customer")
  OpenCheckPanel()
  SearchCustomerFirstName(customer)
  CloseCheckHistory()
  SetCheckMaker()
  SetCheckNumber(TimeBaseRandomNumer())
  SetCheckAmount(555)
  CloseCheckPanel()
  ProcessCashCheck()
  Log.Checkpoint("SUCCESS: Cash a check for a customer")  
  Indicator.PopText()  


