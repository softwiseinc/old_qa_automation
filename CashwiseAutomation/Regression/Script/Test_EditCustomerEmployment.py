﻿from Panel_Employment import *
TestedApps.Cashwise.Run()

def Test_EditCustomerEmployment():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/17/2019 
  database = "QA03" 
  Indicator.PushText("RUNNING: TestEditCustomerEmployment()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  #customerId = GetExistingCustomer("PreviousLoanNotPending")
  customer = GetTempCustomer()
  editCustomer = GetEditCustomer()
  CreateCustomer(customer)
  AddNewEmployer(customer)
  EditCustomerEmployment(customer, editCustomer)
  DeleteCustomer(customer)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestEditCustomerEmployment()")
  Indicator.PopText()