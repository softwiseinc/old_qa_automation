﻿#-UserStory 5809
#-CashwiseAuto>Smoke>LOC>Functionally<all> 


TestedApps.Cashwise.Run()
from Employment import *
from Customers import *
from LineOfCredit import *
from Bank import *

def TestAutoPayChangeBankReport():
  #Author Phil Ivey 9/03/2019
  #Last Modified by Phil Ivey 9/03/2019  
  Indicator.PushText("RUNNING: TestAutoPayChangeBankReport()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  cusID = GetCustomerID_WithLocDataAndAutoPay()
  
  
  if(cusID == "Empty"):
    customer = GetTempContact()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLOC(customer)
    AddBankAccount(customer)
    LineOfCreditRandomWithAutoPay(customer)  
    cusID = customer["cusID"] 

 # LineOfCreditRandomWithAutoPayWithCusID(cusID)  #testPassed = CheckForLOCAgreementAndPlan()
  newRouteAcctNum = EditCustomerBankAccount(cusID)
  testResults = CheckLOCAutoPayAccountNumber(cusID,newRouteAcctNum)
  if(testResults == "Passed"):
    Log.Event("PASSED TEST: TestAutoPayChangeBankReport() Report Printed")
  Indicator.PopText()
  CloseCashwiseApp()
