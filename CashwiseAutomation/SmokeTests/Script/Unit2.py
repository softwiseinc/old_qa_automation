﻿TestedApps.Cashwise.Run()
import random
import string
import time
import re
from DatabaseQuery import *
from Helpers import *
from random import *
from ContactHelper import *
from LineOfCreditHelper import *
from LineOfCredit import *
from Bank import AddBankAccount
from Customers import *

def TEST1():
  cashwise = Sys.Process("Cashwise")
  if(cashwise.WaitWndow("TMessageForm","Error",900).Exists):
    Log.Message("does not NOT exists")   
  else:
    Log.Message("does exists")
    
    
def GetYearFromString(): # On Calendar after rollforward, get date on Calendar 
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  ----------'Thu Sep 23 2010'
    writtenDate = "'Thu Sep 23 2010'"
    prevSep = aqString.ListSeparator
    aqString.ListSeparator = " "
    year = aqString.GetListItem(writtenDate, 3)
    year = aqString.Replace(year, "'", "")
    Log.Message("Year = "+year)   
    year = aqConvert.VarToStr(year)   
    Log.Message(year)
    return year
    

def MonthToMonth():
  
  mIn = "Jan"
  mOUT  = ""
     
  if mIn == "Jan":  
               mOUT = "January"
           
  if(mIn == "Feb") :
               mOUT = "February"
           
  if(mIn == "Mar"):
               mOUT = "March"
           
  if(mIn == "Apr"):      
            mOUT = "April"
           
  if(mIn == "May"):       
               mOUT = "May"
           
  if(mIn == "Jun"):       
               mOUT = "June"
           
  if(mIn == "Jul"):       
              mOUT = "July"
           
  if(mIn == "Aug"):       
               mOUT = "August"
           
  if(mIn == "Sep"):       
               mOUT = "September"
           
  if(mIn == "Oct"):       
               mOUT = "October"
           
  if(mIn == "Nov"):       
               mOUT = "November"
           
  if(mIn == "Dec"):       
               mOUT = "December"            
  Log.Message(mOUT)       
    
  return mOUT
    
   
    
    
    
    
    
    
    
    
    

def Test1():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").MainMenu.Click("Contact|Customer Maintenance...")
  cashwise.VCLObject("frmCustomerBrowse").VCLObject("btnEdit").Click(16, 18)

def Test2():
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[6]")
  aqUtils.Delay(1000)
  if not (cashwise.WaitWindow("TfrmCustomerLimitEdit","Customer Limit",1,800).Exists):
    Log.Message("Menu did not work")
    cashwise.frmCustomerEdit.Keys("~[Release]tl")
  aqUtils.Delay(1000)
  ExplicitWait()

  Log.Message("Menu did not work")
  



def Test3():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").VCLObject("TGraphicButton").Click(23, 29)
  cashwise.VCLObject("frmSaleEdit").VCLObject("pnlMain").VCLObject("pnlPOSButtons").Window("TAdvancedButton", "&0 Cash Advance").Click(88, 11)
  tfrmCustomerBrowse = cashwise.VCLObject("frmCustomerBrowse")
  incrementalSearch = tfrmCustomerBrowse.VCLObject("pnlSearch").VCLObject("isMain")
  incrementalSearch.Click(41, 9)
  incrementalSearch.ClickR(41, 9)
  incrementalSearch.PopupMenu.Click("Paste")
  tfrmCustomerBrowse.VCLObject("btnSave").Click(52, 16)
  cashwise.VCLObject("frmDeferredSelect").VCLObject("btnDeferredAdd").Click(28, 15)
  treeView = cashwise.VCLObject("frmLoanTypeSelection").VCLObject("pnlMain").VCLObject("tvMain")
  treeView.SelectItem("|076 - d5076 Loan Type")
  treeView.Keys("[PageDown][PageDown][Up][Up][Up][Up][Enter]")
  tfrmDeferredTypeCollateralTypeList = cashwise.VCLObject("frmDeferredTypeCollateralTypeList")
  browseGrid = tfrmDeferredTypeCollateralTypeList.VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.VScroll.Pos = 0
  browseGrid.Click(142, 45)
  tfrmDeferredTypeCollateralTypeList.VCLObject("btnSave").Click(62, 3)
  cashwise.VCLObject("frmDeferredCollateralCreditCardEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlCardData").VCLObject("edThirdPartyCard").VCLObject("TAdvancedSpeedButton").Click(9, 6)
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnAdd").Click(41, 21)

  

def HasStoreCard():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").VCLObject("TGraphicButton").Click(47, 21)
  tfrmSaleEdit = cashwise.VCLObject("frmSaleEdit")
  panel = tfrmSaleEdit.VCLObject("pnlMain")
  panel.VCLObject("pnlPOSButtons").Window("TAdvancedButton", "&0 Cash Advance").Click(70, 13)
  tfrmCustomerBrowse = cashwise.VCLObject("frmCustomerBrowse")
  incrementalSearch = tfrmCustomerBrowse.VCLObject("pnlSearch").VCLObject("isMain")
  incrementalSearch.Click(68, 8)
  incrementalSearch.Keys("peter")
  tfrmCustomerBrowse.VCLObject("btnSave").Click(27, 14)
  cashwise.VCLObject("frmDeferredSelect").VCLObject("btnDeferredAdd").Click(20, 23)
  treeView = cashwise.VCLObject("frmLoanTypeSelection").VCLObject("pnlMain").VCLObject("tvMain")
  treeView.SelectItem("|076 - d5076 Loan Type")
  treeView.Keys("[PageDown][PageDown][Up][Up][Up][Up][Enter]")
  tfrmDeferredTypeCollateralTypeList = cashwise.VCLObject("frmDeferredTypeCollateralTypeList")
  browseGrid = tfrmDeferredTypeCollateralTypeList.VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.VScroll.Pos = 0
  browseGrid.Click(71, 37)
  tfrmDeferredTypeCollateralTypeList.VCLObject("btnSave").Click(40, 21)
  tfrmDeferredCollateralCreditCardEdit = cashwise.VCLObject("frmDeferredCollateralCreditCardEdit")
  tfrmDeferredCollateralCreditCardEdit.VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlCardData").VCLObject("edThirdPartyCard").VCLObject("TAdvancedSpeedButton").Click(12, 4)
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnSave").Click(47, 15)
  tfrmDeferredCollateralCreditCardEdit.VCLObject("btnSave").Click(52, 12)
  cashwise.VCLObject("frmDeferredCollateralList").VCLObject("btnSave").Click(58, 19)
  tfrmDeferredNewEdit = cashwise.VCLObject("frmDeferredNewEdit")
  tfrmDeferredNewEdit.VCLObject("dbtAdvance").Drag(57, 14, -98, 6)
  TDBEdit = tfrmDeferredNewEdit.VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlAmounts").VCLObject("pnlBaseFields").VCLObject("edAdvance")
  TDBEdit.SetText("252")
  TDBEdit.Keys("[Tab]")
  tfrmDeferredNewEdit.VCLObject("btnSave").Click(50, 9)
  tfrmDeferredTypeCollateralTypeList = cashwise.VCLObject("frmDeferredTypeCollateralTypeList")
  browseGrid = tfrmDeferredTypeCollateralTypeList.VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.VScroll.Pos = 0
  browseGrid.Click(36, 53)
  tfrmDeferredTypeCollateralTypeList.VCLObject("btnSave").Click(54, 19)
  tfrmDeferredCollateralCreditCardEdit = cashwise.VCLObject("frmDeferredCollateralCreditCardEdit")
  scrollBox = tfrmDeferredCollateralCreditCardEdit.VCLObject("pnlMain").VCLObject("sbData")
  scrollBox.VCLObject("pnlCardData").VCLObject("edThirdPartyCard").VCLObject("TAdvancedSpeedButton").Click(12, 4)
  cashwise.VCLObject("frmThirdPartyCardList").VCLObject("btnSave").Click(61, 14)
  scrollBox.VCLObject("pnlDepositDate").VCLObject("edDepositDate").VCLObject("TAdvancedSpeedButton").Click(6, 5)
  tfrmCalendarEdit = cashwise.VCLObject("frmCalendarEdit")
  tfrmCalendarEdit.VCLObject("pnlMain").VCLObject("Calendar").Click(338, 66)
  tfrmCalendarEdit.VCLObject("btnSave").Click(37, 22)
  tfrmDeferredCollateralCreditCardEdit.VCLObject("btnSave").Click(45, 18)
  cashwise.VCLObject("frmDeferredCollateralList").VCLObject("btnSave").Click(47, 23)
  panel.VCLObject("pnlSaleItems").VCLObject("pnlBottom").VCLObject("pnlProcess").VCLObject("pnlProcessContainer").VCLObject("btnProcess").Click(35, 33)
  tfrmAliasBrowse = cashwise.VCLObject("frmAliasBrowse")
  tfrmAliasBrowse.VCLObject("pnlMain").VCLObject("sbData").VCLObject("lbAliases").ClickItem("Cash")
  tfrmAliasBrowse.VCLObject("btnSave").Click(83, 12)
  cashwise.VCLObject("frmSaleConfirmPayout").VCLObject("btnSave").Click(37, 12)
  ravePreviewForm = cashwise.VCLObject("RavePreviewForm")
  ravePreviewForm.VCLObject("ToolBar1").Click(16, 1)
  ravePreviewForm.MainMenu.Click("File|[4]")
  tfrmSaleEdit.VCLObject("btnClose").Click(35, 10)

def Test4():
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").VCLObject("TGraphicButton_6").Click(38, 27)
  tfrmDrawerManager = cashwise.VCLObject("frmDrawerManager")
  tfrmDrawerManager.VCLObject("btnCloseCash").Click(41, 11)
  cashwise.Window("TMessageForm", "Cashwise").VCLObject("OK").ClickButton()
  tfrmDrawerManager.VCLObject("btnClose").Click(48, 13)
  
  
  
  
  
  


 
    

  
  
  
  
  
  
  
  
  
  
  
  
  
  

def Test5():
  pageControl = Sys.Process("Cashwise").VCLObject("frmDeferredSetup").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pgSettings")
  pageControl.ClickTab("&4 Collateral")
  pageControl.VCLObject("tsCollateral").VCLObject("TfraDeferredTypeCollateral1").VCLObject("rgCheckDateCode").Window("TGroupButton", "Follows Due Date").ClickButton()

def Test6():
  pageControl = Sys.Process("Cashwise").VCLObject("frmDeferredSetup").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pgSettings")
  pageControl.ClickTab("&7 Payment")
  pageControl2 = pageControl.VCLObject("tsInstallmentPayment").VCLObject("GroupBox1").VCLObject("pgACH")
  pageControl2.ClickTab("Non ACH Payment Settings")
  pageControl2.ClickTab("AutoPay Logic")

def Test7():
  panel = Sys.Process("Cashwise").VCLObject("frmDeferredNewLoanEdit").VCLObject("pnlMain").VCLObject("sbData").VCLObject("pnlInstallmentLoan").VCLObject("pnlPaymentInformation").VCLObject("pnlStandardPattern")
  TDBEdit = panel.VCLObject("dbedLoanPrincipal")
  TDBEdit.Click(57, 11)
  TDBEdit.Drag(54, 9, -119, -11)
  TDBEdit.SetText("490")
  TDBEdit.Keys("[Tab]")
  panel.VCLObject("dbedPaymentCount1").Keys("![Tab]")
  TDBEdit.Keys("[Tab]")

def Test8():
  tfrmDeferredSetup = Sys.Process("Cashwise").VCLObject("frmDeferredSetup")
  pageControl = tfrmDeferredSetup.VCLObject("pnlMain").VCLObject("sbData").VCLObject("pgSettings")
  pageControl.ClickTab("&4 Collateral")
  tabSheet = pageControl.VCLObject("tsCollateral")
  tabSheet.VCLObject("dbckbCollateralPlusDEFirst").ClickButton(cbChecked)
  TDBRadioGroup = tabSheet.VCLObject("TfraDeferredTypeCollateral1").VCLObject("rgCheckDateCode")
  TDBRadioGroup.Window("TGroupButton", "Today's Date").ClickButton()
  TDBRadioGroup.Window("TGroupButton", "Follows Due Date").ClickButton()
  tabSheet.VCLObject("dbckbEnableTitleCollateralCoOwner").ClickButton(cbChecked)
  tfrmDeferredSetup.VCLObject("btnSave").Click(49, 5)

def Test9():
  cashwise = Sys.Process("Cashwise")
  tfrmDeferredTypeList = cashwise.VCLObject("frmDeferredTypeList")
  panel = tfrmDeferredTypeList.VCLObject("pnlSearch")
  panel.VCLObject("icMain").VCLObject("TAdvancedSpeedButton").Click(11, 8)
  cashwise.VCLObject("frmAbstractList").VCLObject("btnSave").Click(43, 16)
  browseGrid = tfrmDeferredTypeList.VCLObject("pnlMain").VCLObject("dbgMain")
  browseGrid.HScroll.Pos = 0
  browseGrid.VScroll.Pos = 2
  browseGrid.Click(213, 137)
  incrementalSearch = panel.VCLObject("isMain")
  incrementalSearch.Click(54, 5)
  incrementalSearch.Keys("ts!!")
  
  
def TestFileRead():
  serverName = FileRead()
  Log.Message(serverName)
  
def FileRead():
    #f = open("F:\\Temp\\TestServer.txt")
    f = open("..\\TestServer.txt") # same directory as project
    server = f.read()
    return server

                                                                                               