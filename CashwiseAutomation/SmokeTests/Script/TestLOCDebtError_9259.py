﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
from Customers import *
                                           
def TestLOCDebtError():
  #Author Phil Ivey 12/04/2019
  #Last Modified by Phil Ivey 12/04/2019
  Indicator.PushText("RUNNING: TestLOCDebtError")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  locationID = GetLocationIDFromfrmMainWndCap()
  locationYear = GetCashwiseLocationYear(locationID)
  DeleteRecreateLOCfileShare(locationYear)
  accountID = GetOpenLOCWithACHPayments()
  
  if accountID == "Empty":
      Log.Message("No existing accounts looking for customer with info")
      cusID = GetCustomerID_WithLocData()      
      if cusID == "Empty":
        customer = AddLocCustomer()
        cusID = GetCustomerIDFromName(customer["FullName"])
        AddBankAccount(customer)
      else:
        customer = LoadCustomerObject2(cusID)
        cusID = GetCustomerIDFromName(customer["FullName"])
        
      LineOfCreditInitialDrawAmountAutoPay(customer,"700")
      CloseCashDrawer()
      RollForwardNoReports(1)
      nextACHPayment = GetNextACHPaymentCus(cusID)
      RollForwardTo(nextACHPayment)

  else:
      cusID = GetCustomerIDFromLOCAccount(accountID)
      customer = LoadCustomerObject2(cusID)
      CheckCustomerValuesLog(customer)
      
  
  Log.Message("Customer name = "+customer["FullName"])
  
  
  LOCMakeDebtError(cusID)
  
  # IF THE CODE MADE IT TO THIS POINT IT PASSED
  Log.Event("PASSED TEST: TestLOCDebtError() DEBT LOC")
  
  Indicator.PopText()
  CloseCashwiseApp()