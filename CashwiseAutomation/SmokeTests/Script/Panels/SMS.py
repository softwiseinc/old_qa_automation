﻿from SMSHelper import *


def SMSSetup(): 
    #Author Phil Ivey 8/12/2019
    #Last Modified by Phil Ivey 8/12/2019 
    cashwise = Sys.Process("Cashwise")
    CashwisaMainMenu_File("Setup")
    
    
    
    SystemSetupTree("SMS")
    smsForm = cashwise.frmSMSSetup
    
    smsForm.btnProviders.Click()
    cashwise.frmSMSProviderList.btnEdit.Click()
    providerEdit = cashwise.frmDynamicEdit.pnlMain.sbData
   
    providerEdit.dbeID.Keys("SBT")
    providerEdit.dbeDescription.Keys("Solutions By Text")
    
    #CheckCity
    providerEdit.dbeAPIKey.Keys("VBEIIEWXVwzeDtBRlhgkETp0L/zuweEbY1HPd98wvuchF/4oyzmIrIGGy2JWFd9C")
    providerEdit.dbeUserName.Keys("checkcity1@sbt.com")
    providerEdit.dbePassword.Keys("Y2s92zZexT4P!")
    
    #Softwise
#    providerEdit.dbeAPIKey.Keys("9lwGKyUHbjaTuFQADIvKsW7r0woqQnngDOitRCiDzU1SUVbZRuBEfs9Tcau/1ZHQ")
#    providerEdit.dbeUserName.Keys("softwisestag@solutionsbytext.com")
#    providerEdit.dbePassword.Keys("Q39BB6b7ZWmt!")
    
    providerEdit.dbeCode.Keys("")
    providerEdit.dbeAuthenticationURL.Keys("https://test.solutionsbytext.com/SBT.App.Setup/wsservices/LoginapiService.svc?singlewsdl")
    providerEdit.dbeGeneralURL.Keys("https://test.solutionsbytext.com/SBT.App.SetUp/wsservices/GeneralWSService.svc?singlewsdl")
    providerEdit.dbeGroupURL.Keys("https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/GroupWSService.svc?singlewsdl")
    providerEdit.dbeMessageURL.Keys("https://test.solutionsbytext.com/SBT.App.SetUp/wsservices/MessageWSService.svc?singlewsdl")
    providerEdit.dbeReportURL.Keys("https://test.solutionsbytext.com/Sbt.App.SetUp/WSServices/ReportWSService.svc?singlewsdl")
    providerEdit.dbeSubscriberURL.Keys("https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/subscriberWSService.svc?singlewsdl")
    providerEdit.dbeTemplateURL.Keys("https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/templateWSService.svc?singlewsdl")
    providerEdit.dbeMaxMessageBatchSize.Keys("100")
    providerEdit.dbeBetweenBatchDelay.Keys("5")
    cashwise.frmDynamicEdit.btnSave.Click()
    cashwise.frmSMSProviderList.btnSave.Click()
    
    sysSetupSMS = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup
    sysSetupSMS.dblefSMSProviderID.TAdvancedSpeedButton.Click()
    cashwise.frmSMSProviderList.btnSave.Click()
    
    #CheckCity
    sysSetupSMS.dbeLocationAPIKey.Keys("VBEIIEWXVwzeDtBRlhgkETp0L/zuweEbY1HPd98wvuchF/4oyzmIrIGGy2JWFd9C")
    sysSetupSMS.dbeLocationUserName.Keys("checkcity1@sbt.com")
    sysSetupSMS.dbeLocationPassword.Keys("Y2s92zZexT4P!")
    
    #Softwise
#    sysSetupSMS.dbeLocationAPIKey.Keys("9lwGKyUHbjaTuFQADIvKsW7r0woqQnngDOitRCiDzU1SUVbZRuBEfs9Tcau/1ZHQ")
#    sysSetupSMS.dbeLocationUserName.Keys("softwisestag@solutionsbytext.com")
#    sysSetupSMS.dbeLocationPassword.Keys("Q39BB6b7ZWmt!")
    
    SystemSetupTree("Save & Close")
    ExplicitWait(1) 
    CashwisaMainMenu_File("Setup")
    SystemSetupTree("SMS")
    
    smsForm.btnSubscriptionCodes.Click()
    
    smsCode = cashwise.frmSMSSubscriptionCodeBrowse
    
    smsCode.btnAdd.Click()
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("kd29d67")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Brand 1")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
    cashwise.frmDynamicEdit.btnSave.Click()
    
    if(cashwise.WaitWindow("#32770", "Cashwise", 1, 500).Exists):
      Log.Message("Subscriptions Codes Exists")
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
      cashwise.frmDynamicEdit.btnClose.Click()
      while cashwise.frmSMSSubscriptionCodeBrowse.btnDelete.Enabled == True:
          cashwise.frmSMSSubscriptionCodeBrowse.btnDelete.Click()
          cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
          ExplicitWait(1)
      smsCode.btnAdd.Click()
      cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("kd29d67")
      cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Brand 1")
      cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
      cashwise.frmDynamicEdit.btnSave.Click()    
    
    
    smsCode.btnAdd.Click()
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("7jt67e2")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Account Division")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
    cashwise.frmDynamicEdit.btnSave.Click()
    
    smsCode.btnAdd.Click()
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("i0b99y6")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Account Group")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
    cashwise.frmDynamicEdit.btnSave.Click()
    
    smsCode.btnAdd.Click()
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("53d86q5")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Marketing Division")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
    cashwise.frmDynamicEdit.btnSave.Click()
    
    smsCode.btnAdd.Click()
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeSubCode.Keys("w916711")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbeDescription.Keys("Marketing Group")
    cashwise.frmDynamicEdit.pnlMain.sbData.dbceHasTemplate.ClickButton(cbUnchecked)
    cashwise.frmDynamicEdit.btnSave.Click()
    
   
    
    
    smsCode.btnSMSRefreshAll.Click()
    if(cashwise.WaitWindow("#32770", "Cashwise", 1,500).Exists):
         cashwiseMessage = cashwise.Window("#32770", "Cashwise", 1).Window("Static", "*", 2).WndCaption
         Log.Message("SMS Refresh All Button Results "+cashwiseMessage)
         cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()

    smsCode.btnSave.Click()
#     
#    smsForm.btnSMSPointQueryBrowse.Click()
#    cashwise.frmSMSPointQueryBrowse.btnEdit.Click()
#    captionText = "Sent during a closing roll forward"
#    queryDescrioption = cashwise.frmSMSPointQueryEdit.pnlMain.sbData.edDescription.WndCaption
#    answer = aqString.Find(queryDescrioption,captionText,0,False)  # -1 is not found
#    if answer != -1:
#      Log.Message("the roll forward query step is there")
#    else:
#      Log.Error("the roll forward step query was not found")
#    cashwise.frmSMSPointQueryEdit.btnSave.Click()
#    cashwise.frmSMSPointQueryBrowse.btnSave.Click()
#    
    
    
    SystemSetupTree("Save & Close")
    
#https://test.solutionsbytext.com/SBT.App.Setup/wsservices/LoginapiService.svc?singlewsdl
#https://test.solutionsbytext.com/SBT.App.SetUp/wsservices/GeneralWSService.svc?singlewsdl
#https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/GroupWSService.svc?singlewsdl
#https://test.solutionsbytext.com/SBT.App.SetUp/wsservices/MessageWSService.svc?singlewsdl
#https://test.solutionsbytext.com/Sbt.App.SetUp/WSServices/ReportWSService.svc?singlewsdl
#https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/subscriberWSService.svc?singlewsdl
#https://test.solutionsbytext.com/Sbt.App.SetUp/wsservices/templateWSService.svc?singlewsdl
#    7jt67e2    Account Division
    
    