﻿from UsersHelper import *

def AddNewUser(user):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddNewUser(newCustomer)")
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption != "Users":
    OpenUserMaintenance()
  #Sys.Process("Cashwise").VCLObject("frmUserManageCheck").VCLObject("btnAdd").Click()
  cashwise.frmUserManageCheck.btnAdd.Click()
  userFields = cashwise.frmUserAdd.pnlMain.sbData
  saveButton = cashwise.frmUserManageCheck.btnSave 
  userFields.edID.SetText(user["ID"])
  userFields.edFirstName.Keys(user["FirstName"])
  userFields.edLastName.Keys(user["LastName"])
  userFields.edGroup.Keys(user["Group"])
  userFields.edPassword.Keys(user["Password"])
  userFields.edPasswordConfirm.Keys(user["Password"])
  userFields.edOverrideKey.Keys(user["Overide"])
  saveButton.Click()
  Indicator.PopText()
  return user["ID"]
    
def AssignLocation(user):
  #Author Pat Holman 6/4/2019
  #Last Modified by Pat Holman 6/4/2019
  CreateUser(user)
  OpenUserMaintenance()
  SelectUser(user)  
  OpenUserRightsTree()
  OpenStoreLocation(user["Location"])