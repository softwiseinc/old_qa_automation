﻿from ContactHelper import *

def AddContact(contact):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CreateContact(custome)")
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
    cashwise.Window("TMessageForm","Warning",1).OK.Click()
  cashwise.frmMain.TGraphicButton_3.Click()  
  cashwise.frmContactBrowse.pnlSearch.isMain.Click()
  cashwise.frmContactBrowse.btnAddPerson.Click()
  contactPerson = cashwise.frmContactPersonAdd.pnlMain.sbData.pnlFields
  contactName = contactPerson.pnlName
  contactPhone = contactPerson.pnlBasic.pnlPhone
  contactEmail = contactPerson.pnlEmailAddress.edEmailAddress
  doNotContact = contactPerson.pnlPersonal.dbcDoNotContact
  activeMilitary = contactPerson.pnlPersonal.cbIsActiveMilitary
  contactAddress = contactPerson.pnlAddress
  personalInformation = contactPerson.pnlPersonal
  contactName.edLast.SetText(contact["LastName"])
  contactName.edFirst.SetText(contact["FirstName"])
  contactName.edMiddle.SetText(contact["MiddleInitial"])
  contactPhone.edPrimaryPhone.Keys(contact["PrimaryPhone"])
  contactPhone.edPrimaryPhone.Keys("[Tab]")
  cashwise.frmContactPersonAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID.Keys("[Ins]")
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("BUS")
  cashwise.frmPhoneTypeBrowse.btnSave.Click()
  contactPhone.dbedSecondaryPhone.Keys(contact["SecondaryPhone"])
  contactPhone.dbedSecondaryPhone.Keys("[Tab]")
  cashwise.frmContactPersonAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edSecondaryPhoneTypeID.Keys("[Ins]")
  cashwise.frmPhoneTypeBrowse.pnlSearch.isMain.Keys("CEL")
  cashwise.frmPhoneTypeBrowse.btnSave.Click()
  contactAddress.edAddr1.SetText(contact["Address1"])
  contactAddress.edCity.SetText(contact["City"])
  contactAddress.edState.SetText(contact["State"])
  contactAddress.edZip.SetText(contact["ZipCode"])
  personalInformation.edBirthdate.Click();
  personalInformation.edBirthdate.Keys(contact["BirthDate"])
  personalInformation.edPrimaryLanguage.Keys(contact["PrimaryLanguage"])
  personalInformation.edHeight.Keys(contact["Height"])
  personalInformation.edWeight.Keys(contact["Weight"])
  EnterSocialSecurityNumber(contact)
  contactEmail.SetText(contact["Email"])
  SaveNewContact()
  CloseContactBrowse()
  Indicator.PopText()

def AddAdditionalInformation(contact):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddAdditionalInformation(contact)")
  contactAdditionalEdit = cashwise.frmContactPersonAdditionalEdit
  mainMenu = cashwise.frmContactEdit.MainMenu
  formData = cashwise.frmContactPersonAdditionalEdit.pnlMain.sbData
  mainMenu.Click("[2]|[8]")
  telephoneVerified = cashwise.frmContactPersonAdditionalEdit.pnlMain.sbData.dbckbTelephoneVerified
  phoneBillCurrent =  cashwise.frmContactPersonAdditionalEdit.pnlMain.sbData.dbckbTelephoneCurrent
  phoneInName = cashwise.frmContactPersonAdditionalEdit.pnlMain.sbData.dbckbTelephoneInName
  telephoneVerified.ClickButton(cbChecked)
  phoneBillCurrent.Click()
  phoneInName.Click()
  formData.dbedTelephoneBalance.SetText("100")
  formData.dbckbBankruptcy.ClickButton(cbChecked)
  formData.dbckbResidenceOwned.ClickButton(cbChecked)
  formData.dbedResidenceMonths.SetText("55")
  formData.dbedResidenceAmount.SetText("1200")
  contactAdditionalEdit.pnlMain.sbData.dbckbResidenceOwned.Keys("[F9]")
  Indicator.PopText()
  
def EditContact(contact, editContact):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditContact()")
  OpenContactBrowser()
  SelectContactByName(contact)
  OpenContactEdit()
  contactName = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlName
  contactPhone = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  contactEmail = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.edEmailAddress
  doNotContact = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlBasic.dbcDoNotContact
  activeMilitary = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlBasicOptions.pnlIsActiveMilitary.cbIsActiveMilitary
  contactAddress = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlAddress
  personalInformation = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlPersonal
  marketingInformation = cashwise.frmContactEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing
  additionalInformation = cashwise.frmContactEdit.pnlMain.sbData.pnlAdditional
  contactReference = cashwise.frmContactEdit.pnlMain.sbData.pnlAdditional.pnlReference
  contactName.edLast.SetText(editContact["LastName"])
  contactName.edFirst.SetText(editContact["FirstName"])
  contactName.edMiddle.SetText(editContact["MiddleInitial"])
  contactPhone.edPrimaryPhone.Keys(editContact["PrimaryPhone"])
  contactPhone.dbedSecondaryPhone.Keys(editContact["SecondaryPhone"])
  EditPrimaryPhoneType(editContact)
  EditSecondaryPhoneType(editContact)
  EditSocialSecurityNumber(editContact)
  contactAddress.edAddr1.SetText(editContact["Address1"])
  contactAddress.edCity.SetText(editContact["City"])
  contactAddress.edState.SetText(editContact["State"])
  contactAddress.edZip.SetText(editContact["ZipCode"])
  EditBirthDate(10)
  EditEthnicity(editContact)
  EditPrimaryLanguage(editContact)
  personalInformation.edHeight.Keys(editContact["Height"])
  personalInformation.edWeight.Keys(editContact["Weight"])
  EditEyeColor(editContact)
  EditHairColor(editContact)
  EditGender(editContact)
  EditMarketing(editContact)
  EditNotes()
  SaveAndCloseContactEdit()
  Indicator.PopText()
  
def EditContactReferences(contact, editContact): 
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditContactReferences(contact, editContact)")
  contactName = cashwise.frmContactReferenceEdit.pnlMain.sbData.dbeName
  contactPanel = cashwise.frmContactReferenceEdit.pnlMain.sbData
  OpenContactBrowser()
  SelectContactByName(contact)
  OpenContactEdit()
  ClickRefrenceButton()
  contactReferenceEdit = cashwise.frmContactReferenceEdit
  SelectStatusID()
  contactPanel.dbeName.SetText(editContact["FirstName"] + " " +  editContact["LastName"])
  contactPanel.pnlAddress.edAddress.SetText(editContact["ReferenceAddress1"])
  contactPanel.pnlAddress.edCity.SetText(editContact["ReferenceCity"])
  contactPanel.pnlAddress.edState.SetText(editContact["ReferenceState"])
  contactPanel.pnlAddress.edZip.SetText(editContact["ReferenceState"])
  contactPanel.edPrimaryPhone.Keys(editContact["ReferencePrimaryPhone"])
  contactPanel.dbedSecondaryPhone.Keys(editContact["ReferencePrimaryPhone"])
  contactPanel.dbeEmailAddress.Keys(editContact["ReferenceEmail"])
  SelectPrimaryPhoneTypeID(editContact)
  SelectSecondaryPhoneTypeID(editContact)
  doNotContact = contactPanel.dbcbDoNotContact
  doNotContact.ClickButton(cbChecked)
  doNotContact.Keys("[F9]")
  cashwise.frmContactReferenceList.btnSave.Click()
  cashwise.frmContactEdit.btnSave.Click()
  CloseContactBrowse()  
  Indicator.PopText()
