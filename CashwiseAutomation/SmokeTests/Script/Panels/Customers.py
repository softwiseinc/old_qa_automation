﻿from CustomerHelper import *
cashwise = Sys.Process("Cashwise")

def AddCustomer(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  #cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CreateCustomer(customer)")
  if(customer["ReferenceState"] == "EM"):
    customer["ReferenceState"] = "UT"
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  ClickAddCustomerButton()  
  customerFields = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields
  lastName = customerFields.pnlName.edLast
  firstName = customerFields.pnlName.edFirst
  middleInitial = customerFields.pnlName.edMiddle

  customerPhone = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  customerEmail = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.edEmailAddress
  doNotContact = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.dbcDoNotContact
  activeMilitary = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlBasicOptions.pnlIsActiveMilitary.cbIsActiveMilitary
  customerAddress = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlAddress
  personalInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal
  marketingInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing
  additionalInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional
  customerReference = cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlReference
  
  lastName.SetText(customer["LastName"])
  firstName.SetText(customer["FirstName"])
  middleInitial.Keys(customer["MiddleInitial"])
  
  customerPhone.edPrimaryPhone.Keys(customer["PrimaryPhone"])
  customerPhone.dbedSecondaryPhone.Keys(customer["SecondaryPhone"])
  PrimaryPhoneTypeID = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID
  PrimaryPhoneTypeID.Keys(customer["PrimaryPhoneType"])
  SecondaryPhoneTypeID = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edSecondaryPhoneTypeID
  SecondaryPhoneTypeID.Keys(customer["SecondaryPhoneType"])
  EnterSocialSecurityNumber(customer["SSN"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlAddress.dblefAddressTypeID.Keys(customer["AddressType"])
  customerAddress.edAddr1.SetText(customer["Address1"])
  customerAddress.edCity.SetText(customer["City"])
  customerAddress.edState.SetText(customer["State"])
  customerAddress.edZip.SetText(customer["ZipCode"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.edBirthdate.Keys(customer["BirthDate"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.edDescription.Keys(customer["Ethnicity"])
  personalInformation.edPrimaryLanguage.Keys(customer["PrimaryLanguage"])
  personalInformation.edHeight.Keys(customer["Height"])
  personalInformation.edWeight.Keys(customer["Weight"])
  SelectEyeColor(customer)
  SelectHairColor(customer)
  SelectGender(customer)
  SelectMarketingID(customer)
  SetMembershipExpertation(6)  
  customerEmail.SetText(customer["Email"])
  SelectID1Type(customer)
  SelectID2Type(customer)
  #additionalInformation.pnlID1.edID1State.Keys(customer["ID1_State"])
  additionalInformation.pnlID1.edID1State.Keys("UT")
  additionalInformation.pnlID1.edID1Value.Keys(customer["ID1_Value"])
  additionalInformation.pnlID2.edID2State.Keys(customer["ID2_State"])
  additionalInformation.pnlID2.edID2Value.Keys(customer["ID2_Value"])
  customerReference.edReference1Name.SetText(customer["ReferenceName"])
  SelectReferenceRelation(customer)
  customerReference.edReference1Address.SetText(customer["ReferenceAddress1"])
  customerReference.edReference1PrimaryPhone.SetText(customer["ReferencePrimaryPhone"])
  customerReference.dbedReferenceEmailAddress.SetText(customer["ReferenceEmail"])
  customerReference.edReference1City.SetText(customer["ReferenceCity"])
  customerReference.edReference1Zip.Keys(customer["ReferenceZipCode"])
  customerReference.VCLObject("edReference1State").Keys(customer["ReferenceState"])
  
  SaveNewCustomer()
  CloseCustomerBrowse()
  cusID = GetCustomerIDFromName(customer["FullName"])
  customer["cusID"] = cusID
  Log.Checkpoint(customer["LastName"]+", "+customer["FirstName"])  
  Indicator.PopText()

def AddAdditionalInformation(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[8]")
  Indicator.PushText("In AddAdditionalInformation(customer)")
  formData = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData
  telephoneVerified = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneVerified
  phoneBillCurrent =  cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneCurrent
  phoneInName = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneInName
  telephoneVerified.ClickButton(cbChecked)
  phoneBillCurrent.Click()
  phoneInName.Click()
  formData.dbedTelephoneBalance.SetText("100")
  formData.dbckbBankruptcy.ClickButton(cbChecked)
  formData.dbckbResidenceOwned.ClickButton(cbChecked)
  formData.dbedResidenceMonths.SetText("55")
  formData.dbedResidenceAmount.SetText("1200")
  cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbResidenceOwned.Keys("[F9]")
  Indicator.PopText()
  
def EditCustomer(customer, editCustomer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditCustomer()")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  customerName = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlName
  customerPhone = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  customerEmail = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.edEmailAddress
  doNotContact = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.dbcDoNotContact
  activeMilitary = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlBasicOptions.pnlIsActiveMilitary.cbIsActiveMilitary
  customerAddress = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlAddress
  personalInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal
  marketingInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing
  additionalInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlAdditional
  customerReference = cashwise.frmCustomerEdit.pnlMain.sbData.pnlAdditional.pnlReference
  customerName.edLast.SetText(editCustomer["LastName"])
  customerName.edFirst.SetText(editCustomer["FirstName"])
  customerName.edMiddle.SetText(editCustomer["MiddleInitial"])
  customerPhone.edPrimaryPhone.Keys(editCustomer["PrimaryPhone"])
  customerPhone.edPrimaryPhoneTypeID.Window("TCustomDBEdit", "*", 1).Keys(editCustomer["PrimaryPhoneType"])
  customerPhone.dbedSecondaryPhone.Keys(editCustomer["SecondaryPhone"])
  EditPrimaryPhoneType(editCustomer)
  EditSecondaryPhoneType(editCustomer)
  EditSocialSecurityNumber(editCustomer)
  customerAddress.edAddr1.SetText(editCustomer["Address1"])
  customerAddress.edCity.SetText(editCustomer["City"])
  customerAddress.edState.SetText(editCustomer["State"])
  customerAddress.edZip.SetText(editCustomer["ZipCode"])
  EditBirthDate(10)
  EditEthnicity(editCustomer)
  EditPrimaryLanguage(editCustomer)
  personalInformation.edHeight.Keys(editCustomer["Height"])
  personalInformation.edWeight.Keys(editCustomer["Weight"])
  EditEyeColor(editCustomer)
  EditHairColor(editCustomer)
  EditGender(editCustomer)
  EditMarketing(editCustomer)
  EditNotes()
  SaveAndCloseCustomerEdit()
  Indicator.PopText()
 
def EditCustomerReferences(customer, editCustomer): 
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditCustomerReferences(customer, editCustomer)")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  ClickRefrenceButton()
  contactName = cashwise.frmContactReferenceEdit.pnlMain.sbData.dbeName
  contactPanel = cashwise.frmContactReferenceEdit.pnlMain.sbData
  contactReferenceEdit = cashwise.frmContactReferenceEdit
  SelectStatusID()
  contactPanel.dbeName.SetText(editCustomer["FirstName"] + " " +  editCustomer["LastName"])
  contactPanel.pnlAddress.edAddress.SetText(editCustomer["ReferenceAddress1"])
  contactPanel.pnlAddress.edCity.SetText(editCustomer["ReferenceCity"])
  contactPanel.pnlAddress.edState.SetText(editCustomer["ReferenceState"])
  contactPanel.pnlAddress.edZip.SetText(editCustomer["ReferenceState"])
  contactPanel.edPrimaryPhone.Keys(editCustomer["ReferencePrimaryPhone"])
  contactPanel.dbedSecondaryPhone.Keys(editCustomer["ReferencePrimaryPhone"])
  contactPanel.dbeEmailAddress.Keys(editCustomer["ReferenceEmail"])
  SelectPrimaryPhoneTypeID(editCustomer)
  SelectSecondaryPhoneTypeID(editCustomer)
  doNotContact = contactPanel.dbcbDoNotContact
  doNotContact.ClickButton(cbChecked)
  doNotContact.Keys("[F9]")
  cashwise.frmContactReferenceList.btnSave.Click()
  cashwise.frmCustomerEdit.btnSave.Click()
  CloseCustomerBrowse()  
  Indicator.PopText()

    
def CheckCustomerValuesLog(customer):
   varNameArray = ["cusID","FirstName",	"LastName",	"MiddleInitial",	"FullName",	"PrimaryPhone",	"PrimaryPhoneType",	"SecondaryPhone",	"SecondaryPhoneType",	"SSN",	"AddressType",	"Address1",	"Address2",	"City",	"State",	"ZipCode",	"BirthDate",	"Ethnicity",	"PrimaryLanguage",	"Height",	"Weight",	"EyeColor", "HairColor",	"Gender",	"MarketingID",	"Email",	"ID1",	"ID1_State",	"ID1_Value",	"ID2",	"ID2_State",	"ID2_Value",	"ReferenceRelation",	"ReferenceName",	"ReferencePrimaryPhone",	"ReferenceEmail",	"ReferenceAddress1",	"ReferenceCity",	"ReferenceState",	"ReferenceZipCode",	"EmployerName",	"Department",	"Position",	"WorkPhone",	"WorkPhoneExtention",	"Supervisor",	"SupervisorPhone",	"SupervisorPhoneExtention",	"PayPeriod",	"GrossPay", "NetPay",	"Garnishment",	"WorkStartTime",	"WorkStopTime"]
   x = len(varNameArray)
   numOfColumns = (x - 1) 
       
   i = 0
   while  i <= numOfColumns:
      aqConvert.IntToStr(i)
      colName = customer[varNameArray[i]]
      colValue = aqConvert.VarToStr(colName)
      number =  aqConvert.IntToStr(i)
      Log.Message("Checking values for number "+number+" "+varNameArray[i]+" is "+colValue) 
      if(i == 49):
               Log.Message("this is PayPeriod") 
      i = i + 1
  

  
def CreatCustomerData1():
     #Author Phil Ivey 8/20/2019
     #Last Modified by Phil Ivey 12/04/2019 
     firstName = "" 
     locationID = GetLocationIDFromfrmMainWndCap()
     cashwise = Sys.Process("Cashwise")
     firstName = GetRandomFirstName()
     lastName = GetRandomLastName()
     randomText = GenerateRandomString(8)
     locID = GetLocationIDFromfrmMainWndCap()
     randomMiddleInitial = GenerateRandomString(1)
     randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
     randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
     ReferencePrimaryPhone = GenerateRandomPhoneNumber()
     
     randomSSN = GenerateRandomSSN()
#     ssnStatus = DoesSSNExistInDatabase(randomSSN)  #Exists and NotExists
#     while ssnStatus == "Exists":
#        randomSSN = GenerateRandomSSN()
#        ssnStatus = DoesSSNExistInDatabase(randomSSN)
        
     today = aqDateTime.Today()
     birthDay = GetRandomBirthDate()
     zip = GetRandomZip("UT","846")
     city = GetCityFromZip(zip)
     address1 = GetRamdomAddressInCity(city)
     state = GetStateFromZip(zip)
     email = lastName+firstName+"@ivey.pro"
     employerName = GetRandomCustomerEmplorerName()
     department = GetRandomDepartment()
     position = GetRandomPostiion(department)
     payPeriod = RandomPayFreq()
     if payPeriod == "M":
       payPeriod = "Monthly"
     elif payPeriod == "S":
       payPeriod = "Semi-Monthly"
     elif payPeriod == "B":
       payPeriod = "Bi-Weekly"
     elif payPeriod == "W":
       payPeriod = "Weekly"
     payPeriod = "Bi-Weekly"
     newContact = {}
     newContact["FirstName"] = firstName
     newContact["LastName"] = lastName
     newContact["MiddleInitial"] = randomMiddleInitial
     newContact["FullName"] = lastName+", "+firstName
     newContact["PrimaryPhone"] = randomPrimaryPhoneNumber
     newContact["PrimaryPhoneType"] = "HOM"
     newContact["SecondaryPhone"] = randomSecondaryPhoneNumber
     newContact["SecondaryPhoneType"] = "CEL"
     newContact["SSN"] = randomSSN
     newContact["AddressType"] = "HOM"
     newContact["Address1"] = address1
     newContact["Address2"] = "Apt. 312"  
     newContact["City"] = city  
     newContact["State"] = state 
     newContact["ZipCode"] = zip
     newContact["BirthDate"] = birthDay  
     newContact["Ethnicity"] = "CC" 
     newContact["PrimaryLanguage"] = "ENG" 
     newContact["Height"] = "511" 
     newContact["Weight"] = "200" 
     newContact["EyeColor"] = "BRN" 
     newContact["HairColor"] = "BRN" 
     newContact["Gender"] = "Male" 
     newContact["MarketingID"] = "TV" 
     newContact["Email"] = email
     newContact["ID1"] = "ID"
     newContact["ID1_State"] = state
     newContact["ID1_Value"] = randomText
     newContact["ID2"] = "DL"
     newContact["ID2_State"] = "UT"
     newContact["ID2_Value"] = randomText
     newContact["Password"] = randomText
     newContact["ReferenceRelation"] = "OTH"
     newContact["ReferenceName"] = "Reference_Name" + randomText
     newContact["ReferencePrimaryPhone"] = ReferencePrimaryPhone
     newContact["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
     newContact["ReferenceAddress1"] = "512 N 200 E"
     newContact["ReferenceCity"] = "Orem"
     newContact["ReferenceState"] = "UT"
     newContact["EmployerName"] = employerName
     newContact["Department"] = department
     newContact["Position"] = position
     newContact["WorkPhone"] = "8013611123"
     newContact["WorkPhoneExtention"] = "1123"
     newContact["ReferenceZipCode"] = "84057"
     newContact["Supervisor"] = "RaNay Ash"
     newContact["SupervisorPhone"] = "8013611122"
     newContact["SupervisorPhoneExtention"] = "1122"
     newContact["PayPeriod"] = payPeriod
     newContact["GrossPay"] = "4000"
     newContact["NetPay"] = "3000"
     newContact["Garnishment"] = "1000"
     newContact["WorkStartTime"] = "8:00 AM"
     newContact["WorkStopTime"] = "5:00 PM"
     newContact["Overide"] = "~s"
     newContact["isRandomType"] = True  
     newContact["locationID"] = locationID
     newContact["cusID"] = ""
     Log.Message("Customer Name = "+newContact["FullName"]) 
     Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
     return newContact
     
def GetNewCustomerInfo():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer
