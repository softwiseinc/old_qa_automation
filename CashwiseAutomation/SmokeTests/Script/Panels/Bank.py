﻿from BankHelper import *
from Customers import SelectCustomerByName, OpenCustomerEdit, AddCustomer, OpenCustomerBrowser
#from Employment import AddNewEmployer
#from LineOfCredit import AddCategoryLOC
from CustomerHelper import *

def AddBankAccount(customer):
  cusID = GetCustomerIDFromName(customer["FullName"])
  numOfBankAccts = GetNumberOfCustomerBankAccounts(cusID)
  if(numOfBankAccts < 1):
    OpenCustomerBrowser()
    SelectCustomerByName(customer)
    OpenCustomerEdit()
    OpenCustomerAccount()
    BankClickAddButton()
    AddNewAccountNumber()
    ClickAddAccountSpeedButton()
    SaveNewAccount()
    SetAccountOpenDate()
    CloseAddAccountWindows()
    
  
def  EditCustomerBankAccount(customerId):
   #Author Phil Ivey 9/05/2019
  #Last Modified by Phil Ivey 9/05/2019
     #customerId = "001-0000494"
     cashwise = Sys.Process("Cashwise")
     CustomerBrowseSearchMainScreen(customerId,"ID","Edit")
     OpenCustomerAccount()
     cusBankAcct = cashwise.frmContactAccountList
     
     # TEST THE DELETE BUTTON
     cusBankAcct.btnDelete.Click()
     if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,800).Exists):
        cap = cashwise.Window("TMessageForm", "Confirm", 1).Message.Caption
        res = aqString.Find(cap,"LOC Autopay",0,True)
        if(res != -1):
                  Log.Message("This Account is used for AutoPay")
                  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("No").Click()
                  cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
        else:
          Log.Error("No AutoPay in Message")
          cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("No").Click()
     
     # TEST THE EDIT BUTTON
     cusBankAcct.btnEdit.Click()
     if(cashwise.WaitWindow("#32770", "Cashwise", 1,800).Exists):
        wndCap = cashwise.Window("#32770", "Cashwise", 1).Window("Static","*",2).WndCaption
        res = aqString.Find(wndCap,"before changing this one",0,True)
        if(res != -1):
                  Log.Message("Correct Window for Edit Button")
                  cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
        else:
          Log.Error("This Message is not correct for the Edit Button")
         
     # TEST THE Add BUTTON
     cusBankAcct.btnAdd.Click()
     if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,500).Exists):
        cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()

     cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TAdvancedSpeedButton.Click()
     routeID = GetRandomRouteNum()
     acctNum = GetRandomAcctNum()
     NewRouteAcctNum = routeID + acctNum
     acctBrowse = cashwise.frmBankAccountBrowse
     acctBrowse.pnlSearch.isMain.Keys(routeID)
     acctBrowse.btnAdd.Click()
     
     cashwise.frmBankAccountEdit.pnlMain.sbData.pnlFields.pnlColumn1.pnlRouting.edAcctNum.Keys("3")
     cashwise.frmDoubleEnterAccountNumber.pnlMain.sbData.DBEdit1.Keys("[Home]![End][Del]")
     cashwise.frmDoubleEnterAccountNumber.pnlMain.sbData.DBEdit1.Keys(acctNum)
     cashwise.frmDoubleEnterAccountNumber.pnlMain.sbData.DBEdit2.Keys(acctNum)
     cashwise.frmDoubleEnterAccountNumber.btnSave.Click()
     
     cashwise.frmBankAccountEdit.btnSave.Click()
     cashwise.frmContactAccountEdit.btnSave.Click()
     ExplicitWait(1)
     #--CHECK 1ST RAVE REPORT
     raveIsUp = IsRaveUp()
     if(raveIsUp == True):
        Log.Message("Check Rave 1st Time Passed")
        cashwise.VCLObject("RavePreviewForm").Close()
     else:
      Log.Message("Can't Find Rave")
     ExplicitWait(2) 
     #--CHECK 2ND RAVE REPORT
     raveIsUp = IsRaveUp()
     if(raveIsUp == True):
        Log.Message("Check Rave 2ND Time Passed")
        cashwise.VCLObject("RavePreviewForm").Close()
     else:
      Log.Message("Can't Find Rave")
     ExplicitWait(1) 
     cusBankAcct.pnlMain.dbgMain.Keys("[Up]")
     # DELETE THE OLD ACCOUNT
     cusBankAcct.btnDelete.Click()
     if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,800).Exists):
        cap = cashwise.Window("TMessageForm", "Confirm", 1).Message.Caption
        res = aqString.Find(cap,"LOC Autopay",0,True)
        if(res != -1):
                  Log.Message("This Account is used for AutoPay")
                  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("No").Click()
                  cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
                  cusBankAcct.pnlMain.dbgMain.Keys("[Down]")
                  cusBankAcct.btnDelete.Click()
                  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
        else:
                  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
                  
     cashwise.frmContactAccountList.btnSave.Click()
     cashwise.frmCustomerEdit.btnSave.Click()
     cashwise.frmCustomerBrowse.btnSave.Click()
     
     return NewRouteAcctNum
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     


