﻿from Customers import *
from CustomerHelper import SelectCustomerByName, OpenCustomerEdit, SaveAndCloseCustomerEdit 
from CustomerHelper import CloseCustomerBrowse, OpenCustomerBrowser, DeleteCustomer
from DatabaseQuery import GetTableIndex
from EmploymentHelper import *
from ReportHelper import CheckForOptionalPayAuth





def AddNewEmployer(customer):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  OpenEmployerContact()
  OpenAddEmployerContact()

  department = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedDepartment
  workPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedPhone
  workPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedExtension
  supervisor = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisor
  supervisorPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorPhone
  supervisorPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorExtension
  grossPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGrossPay
  netPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedNetPay
  garnishmet = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGarnishments
  hasDirectDeposit = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbHasDirectDeposit
  preferredEmployer = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.cbPreferredEmployer
  workStartTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStartTime
  workStopTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStopTime
  workDayMonday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayMonday
  workDayTuesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayTuesday
  workDayWednesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayWednesday
  workDayThursday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayThursday
  workDayFriday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayFriday
  saveAndCloseButton = cashwise.frmContactEmployerEdit.btnSave
  SelectEmployerName(customer)
  SelectStartDate(3)  
  department.SetText(customer["Department"])
  SelectPosition(customer)
  workPhone.SetText(customer["WorkPhone"])
  workPhoneExtention.SetText(customer["WorkPhoneExtention"])
  supervisor.SetText(customer["Supervisor"])
  supervisorPhone.SetText(customer["SupervisorPhone"])
  supervisorPhoneExtention.SetText(customer["SupervisorPhoneExtention"])
  SelectPayPeriod(customer)
  
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    
  
  grossPay.SetText(customer["GrossPay"])
  netPay.SetText(customer["NetPay"])
  garnishmet.SetText(customer["Garnishment"])
  ExplicitWait(1)
  cashwise.frmContactEmployerEdit.Click(7,7)
  BiWeeklyPaydaySelect()
  hasDirectDeposit.ClickButton(cbChecked)
  preferredEmployer.ClickButton(cbChecked)
  workStartTime.SetText(customer["WorkStartTime"])
  workStopTime.SetText(customer["WorkStopTime"])
  workDayMonday.ClickButton(cbChecked)
  workDayTuesday.ClickButton(cbChecked)
  workDayWednesday.ClickButton(cbChecked)
  workDayThursday.ClickButton(cbChecked)
  workDayFriday.ClickButton(cbChecked)
  saveAndCloseButton.Click()
  
  SaveAndCloseContactEmployers()
  SaveAndCloseCustomerEdit()
  CloseCustomerBrowse()
    
def EditCustomerEmployment(customer, editCustomer):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit() 
  OpenEmployerContact()
  EditSelectedEmployer()
  employerData = cashwise.frmContactEmployerEdit.pnlMain.sbData
  tableIndex = {}
  tableIndex["SqlStatement"] = "SELECT * FROM DeferredType"
  tableIndex["Coloumn"] = "ID"
  tableIndex["ColoumnValue"] = "ILC"  
  SelectPayPeriod(editCustomer)
  SelectPosition(editCustomer)
  employerData.dbedGrossPay.SetText(editCustomer["GrossPay"])
  employerData.dbedNetPay.SetText(editCustomer["NetPay"])
  employerData.dbedGarnishments.SetText(editCustomer["Garnishment"])
  employerData.dbckbHasDirectDeposit.ClickButton(cbUnchecked)
  employerData.Keys("[F9]")
  cashwise.frmContactEmployerList.btnSave.Click()
  SaveAndCloseCustomerEdit()
  CloseCustomerBrowse()
  GetTableIndex(tableIndex)
 
def  EditEmploymentPayAmount(customerId, direction):
      #Author Phil Ivey 9/04/2019
      #Last Modified by Phil Ivey 9/04/2019
      #customerId = "001-0000841"
      #direction = "Increase"
     cashwise = Sys.Process("Cashwise")
     CustomerBrowseSearchMainScreen(customerId,"ID","Edit")
     cashwise.frmCustomerEdit.btnEmployers.Click()
     cashwise.frmContactEmployerList.btnEdit.Click()
     payPath = cashwise.frmContactEmployerEdit.pnlMain.sbData
     oldGross = payPath.dbedGrossPay.wText
     oldNet = payPath.dbedNetPay.wText
     if(direction == "Increase"):
       newFlGross = aqConvert.StrToFloat(oldGross)
       newFlNet = aqConvert.StrToFloat(oldNet)
       newFlGross = newFlGross + (newFlGross*.015)
       newFlNet = newFlNet + (newFlNet*.015)
            
     if(direction == "Decrease"):
       newFlGross = aqConvert.StrToFloat(oldGross)
       newFlNet = aqConvert.StrToFloat(oldNet)
       newFlGross = newFlGross - (newFlGross * .015)
       newFlNet = newFlNet - (newFlNet * .015)
       
     newStGross = aqConvert.CurrencyToFormatStr(newFlGross,2,0,0,0)
     newStNet = aqConvert.CurrencyToFormatStr(newFlNet,2,0,0,0)
     payPath.dbedGrossPay.Keys(newStGross)
     payPath.dbedNetPay.Keys(newStNet)
     payPath.dbedNetPay.Keys("[Tab]")
     cashwise.frmContactEmployerEdit.btnSave.Click()
     ExplicitWait(2)
     if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,5000).Exists):
        cashwise.frmCalendarEdit.btnSave.Click()
   
def  EditEmploymentPayDate(cusID,testResults):
   #Author Phil Ivey 9/04/2019
  #Last Modified by Phil Ivey 01/13/2020
     #customerId = "001-0000494"
     cashwise = Sys.Process("Cashwise")
     CustomerBrowseSearchMainScreen(cusID,"ID","Edit")
     cashwise.frmCustomerEdit.btnEmployers.Click()
     cashwise.frmContactEmployerList.btnEdit.Click()
     PayPeriod = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod
     payPeriod = ""
     cashwiseDate = GetLocationDate("001")
    
     if(PayPeriod.Find("Caption","Bi-Weekly",10).Checked == True):
       payPeriod = "B"
     if(PayPeriod.Find("Caption","Semi-Monthly",10).Checked == True):
            payPeriod = "S"
     if(PayPeriod.Find("Caption","Monthly",10).Checked == True):
       payPeriod = "M"
     if(PayPeriod.Find("Caption","Weekly",10).Checked == True):
       payPeriod = "W"
       
       #-----  Weekly Bi-Weekly   
     if(payPeriod == "B" or payPeriod == "W"):
        nextPayBox = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.dlbledPayDate1
        payDayOfWeek = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly
        weeklyBiWeeklyButton = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect
        nextPayPeriod = nextPayBox.WndCaption # Next Pay Date
        if(payPeriod == "B"):
          nexPayDateButton = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect
      
        theDate = aqConvert.StrToDate(nextPayPeriod)
        dayNumberOfWeek = aqDateTime.GetDayOfWeek(theDate) # Day of week for Next pay date
        
        if(dayNumberOfWeek == 2):  #------Monday to Wednesday
                  newPayDate = aqDateTime.AddDays(nextPayPeriod,2)
                  payDayOfWeek.cbPaydayWednesday.ClickButton(cbChecked)
                  if(payPeriod == "B"):
                    nexPayDateButton.Click()
                    newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                    cashwise.frmContactBiweeklyEdit.btnSave.Click()
            
        if(dayNumberOfWeek == 3):  #------Tuesday to Thursday
                  newPayDate = aqDateTime.AddDays(nextPayPeriod,2)
                  payDayOfWeek.cbPaydayThursday.ClickButton(cbChecked)
                  if(payPeriod == "B"):
                    nexPayDateButton.Click()
                    newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                    cashwise.frmContactBiweeklyEdit.btnSave.Click()
       
        if(dayNumberOfWeek == 4):  #------Wednesday to Thursday
                  newPayDate = aqDateTime.AddDays(nextPayPeriod,1)
                  payDayOfWeek.cbPaydayThursday.ClickButton(cbChecked)
                  if(payPeriod == "B"):
                    nexPayDateButton.Click()
                    newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                    cashwise.frmContactBiweeklyEdit.btnSave.Click()
         
        if(dayNumberOfWeek == 5):  #------ Thursday to Friday
                  newPayDate = aqDateTime.AddDays(nextPayPeriod,1)
                  payDayOfWeek.cbPaydayFriday.ClickButton(cbChecked)
                  if(payPeriod == "B"):
                    nexPayDateButton.Click()
                    newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                    cashwise.frmContactBiweeklyEdit.btnSave.Click()
    
        if(dayNumberOfWeek == 6):  #------ Friday to Monday
                   newPayDate = aqDateTime.AddDays(nextPayPeriod,3)
                   payDayOfWeek.cbPaydayMonday.ClickButton(cbChecked)
                   if(payPeriod == "B"):
                    nexPayDateButton.Click()
                    newSelectedPayDate = cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Caption
                    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
                    cashwise.frmContactBiweeklyEdit.btnSave.Click()

#------------------  Monthly   
                
     if(payPeriod == "M"):
       #nextPayBox = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.edPayDayOne
       recurrencePattern = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType
       if(recurrencePattern.Window("TGroupButton", "Day", 2).Checked == True):
          patternType = "Day"
       if(recurrencePattern.Window("TGroupButton", "The", 1).Checked == True):
          patternType = "The" 

       if(patternType == "Day"):
         dayPath = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.edDayOfMonth  
         newDate = randrange(1,25)
         dayPath.Keys("1")
         for x in range(newDate):
          dayPath.Keys("[Down]")
         dayPath.Keys("[Enter]")
         
       
       if(patternType == "The"): # 0 = first, 1 = second, 2 = third, 3 = forth
         thePath = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.cbMonthPatternName
         itemCount = thePath.wItemCount
         selectedItem = thePath.wSelectedItem
         newSelect = selectedItem - 1
         if(newSelect < 0):
           newSelect = 3
         thePath.Click()
         thePath.Keys("[Up][Up][Up][Up]")
         for x in range(newSelect):
           thePath.Keys("[Down]")
         thePath.Keys("[Enter]")
         
      #------------------ Semi--Monthly    
         
     if(payPeriod == "S"):
      firstSemiPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlSemiMonthly.pnlSemiDay1.cbSemiDay1
      secondSemiPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlSemiMonthly.pnlSemiDay2.cbSemiDate2
      newDate = randrange(1,15)
      secondSemiPay.Keys("[Del]")
      firstSemiPay.Keys("1")
      for x in range(newDate):
         firstSemiPay.Keys("[Down]")
      firstSemiPay.Keys("[Enter]")
   
     #-- MAKE SURE NEXT PAYDATE IS GREATER THAN CASHWISE DATE
     nextPayDateString = cashwise.frmContactEmployerEdit.lblNextPayDateInfo.Caption
     employNextPayDate = StringDateParse(nextPayDateString)
     dateEmployNextPay = aqConvert.StrToDate(employNextPayDate)
     dateCashwiseDate = aqConvert.StrToDate(cashwiseDate)
     if(dateEmployNextPay <= dateCashwiseDate):
       paySelect = cashwise.frmContactEmployerEdit.pnlMain.sbData
       paySelect.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect.Click()
       ExplicitWait(1)
       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
       cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateTwo.Click()
       #cashwise.frmContactEmployerEdit.pnlMain.sbData.VCLObject("pnlDates").VCLObject("rbDateTwo").Click()
       cashwise.frmContactBiweeklyEdit.btnSave.Click()
     
     cashwise.frmContactEmployerEdit.btnSave.Click()   
     ExplicitWait(2)
     if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1500).Exists):
        cashwise.frmCalendarEdit.btnSave.Click()
     if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists): 
      testResults = CheckForOptionalPayAuth(cusID,testResults)      
      ExplicitWait(6)
     if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists):
        cashwise.RavePreviewForm.Keys("~fx")
     ExplicitWait(6)
     if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1000).Exists):
        cashwise.RavePreviewForm.Keys("~fx")
     cashwise.frmContactEmployerList.btnSave.Click()
     cashwise.frmCustomerEdit.btnSave.Click()
     cashwise.frmCustomerBrowse.btnClose.Click()
     return testResults
     
    

    
    
     
     
     

  
