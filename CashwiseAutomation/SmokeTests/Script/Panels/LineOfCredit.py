﻿#import AddBankAccount
#import AddNewEmployer
import string
import re

from Bank import *
#from CashHelper import *
from LoanHelper import *
from ReportHelper import *
from SalesHelper import *
from Customers import *
#from SystemSetupHelper import *
from LineOfCreditHelper import *
#from Employment import *

#def  OpenCustomerBrowser(customer):
#  SelectCustomerFromBrowser(customer)
#  OpenCustomerEdit()
#  
#  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[7]|[0]")
#  customerLimitEdit = cashwise.frmCustomerLimitEdit
#  customerLimitEdit.btnAdd.Click()
#  #if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
#    #PerformSecurityOveride("own")
#  SelectLoanTypeCategory()
#  loanLimitForm = cashwise.frmDynamicEdit.pnlMain.sbData
#  loanLimitForm.dbeDeferredLimit.SetText("5000")
#  loanLimitForm.dbeDeferredCount.SetText("1")
#  loanLimitForm.Keys("[F9]")
#  if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
#    PerformSecurityOveride(Owner["Password"])
#  cashwise.frmCustomerLimitEdit.pnlMain.sbData.gbCustomerLimit.dbedLimitAmount.Keys("[F9]")
#  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlName.edLast.Keys("[F9]")
#  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[F9]")
#  Log.Checkpoint("SUCCESS: OpenEditCashAdvanceLimit() is successful.")
TestedApps.Cashwise.Run()
  
def AddCategoryLocRandomLimit(customer):
  #Author Phil Ivey 8/20/2019
  #Last Modified by Phil Ivey 8/20/2019 
  locLimit = randrange(500,3500) 
  managerUser = GetManager001()
  #cashwise = Sys.Process("Cashwise")
  cashwise = StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])    
  Indicator.PushText("In OpenEditCashAdvanceLimit()")
  OpenCustomerBrowser()
  SelectCustomerFromBrowser(customer)
  OpenCustomerEdit() 
  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[6]")
  #cashwise.frmCustomerEdit.MainMenu.Click("[2]|[7]|[0]")
  customerLimitEdit = cashwise.frmCustomerLimitEdit
  customerLimitEdit.btnAdd.Click()
  #if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
    #PerformSecurityOveride("own")
  SelectLoanTypeCategory()
  loanLimitForm = cashwise.frmDynamicEdit.pnlMain.sbData
  loanLimitForm.dbeDeferredLimit.SetText(locLimit)
  loanLimitForm.dbeDeferredCount.SetText("1")
  loanLimitForm.Keys("[F9]")
  if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
    PerformSecurityOveride(Owner["Password"])
  cashwise.frmCustomerLimitEdit.pnlMain.sbData.gbCustomerLimit.dbedLimitAmount.Keys("[F9]")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlName.edLast.Keys("[F9]")
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: OpenEditCashAdvanceLimit() is successful.")
  return locLimit

def AddCategoryLocPassLimit(customer,locLimit):
  #Author Phil Ivey 8/20/2019
  #Last Modified by Phil Ivey 8/20/2019 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenEditCashAdvanceLimit()")
  OpenCustomerBrowser()
  SelectCustomerFromBrowser(customer)
  OpenCustomerEdit()

    #cashwise.frmCustomerEdit.MainMenu.Click("[2]|[7]|[0]")
  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[6]")
  aqUtils.Delay(1000)
  if not (cashwise.WaitWindow("TfrmCustomerLimitEdit","Customer Limit",1,800).Exists):
    Log.Message("Menu did not work")
    cashwise.frmCustomerEdit.Keys("~[Release]tl")
  aqUtils.Delay(1000)
 
 
  cashwise.frmCustomerLimitEdit.btnAdd.Click()
  #if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
    #PerformSecurityOveride("own")
  SelectLoanTypeCategory()
  loanLimitForm = cashwise.frmDynamicEdit.pnlMain.sbData
  loanLimitForm.dbeDeferredLimit.SetText(locLimit)
  loanLimitForm.dbeDeferredCount.SetText("1")
  loanLimitForm.Keys("[F9]")
  if(cashwise.WaitWindow("TfrmDynamicEdit","Add*",1,2000).Exists):
    PerformSecurityOveride(Owner["Password"])
  cashwise.frmCustomerLimitEdit.pnlMain.sbData.gbCustomerLimit.dbedLimitAmount.Keys("[F9]")
  cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlName.edLast.Keys("[F9]")
  cashwise.frmCustomerBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: OpenEditCashAdvanceLimit() is successful.")

  
  
def LineOfCreditMaxLoan(customer):
  #Author Pat Holman 6/25/2019
  #Last Modified by Pat Holman 7/10/2019
  cashwise = Sys.Process("Cashwise")
  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  EnterDrawAmount("5000")
  ClickMaximumDrawButton()
  ClickConfirmButton()
  ProcessTrasaction()
  
def LineOfCreditRandomWithAutoPay(customer):
  #Author Phil Ivey 0/04/2019
  #Last Modified by Phil Ivey 0/04/2019
  cashwise = Sys.Process("Cashwise")
  cusName = customer["FullName"]
  cusID = GetCustomerIDFromName(cusName)
  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  amount = randrange(250,1100)
  EnterDrawAmount(amount)
  ClickMaximumDrawButton()
  ClickConfirmButtonOnly()
  SignCustomerUpForAutoPay(cusID,"Yes")
  ProcessTrasaction()

def LineOfCreditAutoPayEdit(customer):
  #Author Phil Ivey 12/2/2019
  #Last Modified by Phil Ivey 12/2/2019
  cashwise = Sys.Process("Cashwise")
  cusName = customer["FullName"]
  cusID = GetCustomerIDFromName(cusName)
  
  
  
  

  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  EnterDrawAmount("500")
  ClickMinimumDrawButton()
  ClickConfirmButtonOnly()
  SignCustomerUpForAutoPay(cusID,"Yes")
  ExplicitWait(3) 
  EditTransaction()
  ExplicitWait(3) 
  ProcessTrasaction() 
  ExplicitWait(3) 
  OpenLineOfCredit(customer)
  IsActive = IsAutoPayActive()
  CancelCashwiseBrowser()
  cashwise.frmSaleEdit.btnClose.Click()
  return IsActive  

def LOCMakeDebtError(cusID):
  cashwise = Sys.Process("Cashwise")
  #cusID = "001-0000841"
  
  accountID = GetLocAccountNum(cusID)
  CashwiseMainMenu_Collections("Line of Credit Debt")
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  cashwise.frmLOCDebtEdit.pnlMain.sbData.pnlTop.dblefAccountID.TAdvancedSpeedButton.Click()
  cashwise.frmLOCAccountBrowse.pnlFilter.gbFilter.DBRadioGroup1.Window("TGroupButton", "Open").ClickButton()
  cashwise.frmLOCAccountBrowse.pnlSearch.isMain.Keys(accountID)
  cashwise.frmLOCAccountBrowse.pnlFilter.gbFilter.btnApplyFilter.Click()
  cashwise.frmLOCAccountBrowse.btnSave.Click()
  controlPage = cashwise.frmLOCDebtEdit.pnlMain.sbData.pcData.tsTransaction.bgTransactions
  controlPage.Keys("[Home]")
  controlPage.HScroll.Pos = 0
  controlPage.VScroll.Pos = 0
  controlPage.Click(363,23)
  
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,800).Exists):
        wndCap = cashwise.Window("#32770", "Cashwise", 1).Window("Static","*",2).WndCaption
        res = aqString.Find(wndCap,"must have a value",0,True)
        if(res == -1):
            cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
            Log.Error("Must have a value error appeared")
  cashwise.frmLOCDebtEdit.asbTransactionEdit.Click()
  cashwise.frmDynamicEdit.pnlMain.sbData.dbceIsReturned.ClickButton(cbChecked)
  cashwise.frmDynamicEdit.pnlMain.sbData.dbleReturnReasonID.TAdvancedSpeedButton.Click()
  cashwise.frmReturnReasonBrowse.pnlMain.dbgMain.Keys("[Down][Down]")
  cashwise.frmReturnReasonBrowse.btnSave.Click()
  cashwise.frmDynamicEdit.btnSave.Click()
  theText = cashwise.frmLOCDebtEdit.pnlMain.sbData.pnlTop.edTransferAmount.wText
  if(theText == ""):
    Log.Error("No ACH Amount will be transfered to the collector")
  cashwise.frmLOCDebtEdit.btnSave.Click()
  
  
def LineOfCreditRandomWithAutoPayWithCusID(cusID):
  #Author Phil Ivey 0/04/2019
  #Last Modified by Phil Ivey 0/04/2019
  cashwise = Sys.Process("Cashwise")
  OpenLineOfCreditWithCusID(cusID) 
  ClickInitialDrawButton()
  amount = randrange(250,1100)
  EnterDrawAmount(amount)
  ClickMaximumDrawButton()
  ClickConfirmButtonOnly()
  SignCustomerUpForAutoPay("Yes")
  ProcessTrasaction()
  cashwise.frmSaleEdit.btnClose.Click()

   
def LineOfCreditMinLoan(customer):
  #Author Phil Ivey 7/10/2019
  #Last Modified by Phil Ivey 7/10/2019
  cashwise = Sys.Process("Cashwise")
  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  EnterDrawAmount("200")
  ClickMinimumDrawButton()
  ClickConfirmButton()
  ProcessTrasaction()
  
def NewLineOfCreditLoanWithAutoPay(customer):
  #Author Phil Ivey 7/10/2019
  #Last Modified by Phil Ivey 7/10/2019
  cashwise = Sys.Process("Cashwise")
  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  amount = randrange(300,999)
  EnterDrawAmount(amount)
  ClickMinimumDrawButton()
  ClickConfirmButtonOnly()
  Sa
  ProcessTrasaction()

def LineOfCreditInitialDrawAmount(customer,amount):
    #Author Phil Ivey 7/10/2019
    #Last Modified by Phil Ivey 7/10/2019
    cashwise = Sys.Process("Cashwise")
    OpenLineOfCredit(customer)  
    ClickInitialDrawButton()
    EnterDrawAmount(amount)
    ClickDrawButton(amount)
    ClickConfirmButton()
    ProcessTrasaction()
    cashwise.frmSaleEdit.btnClose.Click()
    
def LineOfCreditInitialDrawAmountAutoPay(customer,amount):
    #Author Phil Ivey 7/10/2019
    #Last Modified by Phil Ivey 7/10/2019
    cashwise = Sys.Process("Cashwise")
    OpenLineOfCredit(customer)  
    ClickInitialDrawButton()
    EnterDrawAmount(amount)
    ClickDrawButton(amount)
    ClickConfirmButtonOnly()
    # THIS NEEDS TO KNOW WHAT WEB
    SignCustomerUpForAutoPay(customer,"Yes")
    ProcessTrasaction()
    Delay(800)
    cashwise.frmSaleEdit.btnClose.Click()
    

  
def LineOfCreditInitialDraw(customer):
  #Author Phil Ivey 7/10/2019
  #Last Modified by Phil Ivey 7/10/2019
  cashwise = Sys.Process("Cashwise")
  OpenLineOfCredit(customer)  
  ClickInitialDrawButton()
  amount = IntRandomRange(250,2500)
  EnterDrawAmount(amount)
  ClickDrawButton(amount)
  ClickConfirmButton()
  ProcessTrasaction()
  cashwise.frmSaleEdit.btnClose.Click()
  
  
def RollForwardManual():  #<<<<<<<<<<<<<<<<<< ROLL FORWARD TO THE NEXT DUE DATE OR BILLING CYCLE DATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  
       locID = "001"
   
       toDateFromData = "03/14/2014"
       #toDateFromData = GetNextLOCRollForwardDate(locID)
   
   
       #toDate = aqDateTime.AddDays(toDateFromData,-1)
       toDate = toDateFromData
       stopOnDate = aqConvert.DateTimeToFormatStr(toDate, "%m/%d/%Y") 
       cashwise = Sys.Process("Cashwise")
       CloseCashDrawer()

       RollForwardLOCNoReportsCalendarOpen()
      
       startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
       newDate = ConvertStrToDate(startDateAfterRollForword)
       cashwise.frmCalendarEdit.btnSave.Click()
       cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
       while newDate <= stopOnDate:
      
          #SetFocus(cashwise("frmMain"))
          Sleep(100)
          RollForwardLOCNoReportsCalendarOpen()
          Sleep(100)
          startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
          newDate = ConvertStrToDate(startDateAfterRollForword)
          if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1000).Exists):
              cashwise.frmCalendarEdit.btnSave.Click()
          cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
          
          
def RollForwardTo(toDateFromData):  #<<<<<<<<<<<<<<<<<< ROLL FORWARD TO THE NEXT DUE DATE OR BILLING CYCLE DATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #Author Phil Ivey 11/19/2019
    #Last Modified by Phil Ivey 11/19/2019  
       locID = "001"
   
       #toDateFromData = "10/01/2010"
       #toDateFromData = GetNextLOCRollForwardDate(locID)
       #toDate = aqDateTime.AddDays(toDateFromData,-1)
       toDate = toDateFromData
       varType = aqObject.GetVarType(toDate)
       #if(varType == 7):
       #stopOnDate = aqConvert.DateTimeToFormatStr(toDate, "%m/%d/%Y") 
       if(varType == 8):
         stopOnDate = aqConvert.StrToDate(toDate)
        #stopOnDate = toDate
       cashwise = Sys.Process("Cashwise")
       CashHelper.CloseCashDrawer()

       RollForwardLOCNoReportsCalendarOpen()
      
       startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
       newDate = ConvertStrToDate(startDateAfterRollForword)
       cashwise.frmCalendarEdit.btnSave.Click()
       cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
       while newDate <= stopOnDate:
      
          #SetFocus(cashwise("frmMain"))
          Sleep(100)
          RollForwardLOCNoReportsCalendarOpen()
          Sleep(100)
          startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
          newDate = ConvertStrToDate(startDateAfterRollForword)
          while(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,500).Exists == False):
              aqUtils.Delay(500)
          if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1000).Exists):
              cashwise.frmCalendarEdit.btnSave.Click()
          cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
  
def RollForwardToManual():  #<<<<<<<<<<<<<<<<<< ROLL FORWARD TO THE NEXT DUE DATE OR BILLING CYCLE DATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #Author Phil Ivey 11/19/2019
    #Last Modified by Phil Ivey 11/19/2019  
       locID = "001"
   
       toDateFromData = "01/03/2015"
       #toDateFromData = GetNextLOCRollForwardDate(locID)
       #toDate = aqDateTime.AddDays(toDateFromData,-1)
       toDate = toDateFromData
       varType = aqObject.GetVarType(toDate)
       #if(varType == 7):
       #stopOnDate = aqConvert.DateTimeToFormatStr(toDate, "%m/%d/%Y") 
       if(varType == 8):
         stopOnDate = aqConvert.StrToDate(toDate)
        #stopOnDate = toDate
       cashwise = Sys.Process("Cashwise")
       CashHelper.CloseCashDrawer()

       RollForwardLOCNoReportsCalendarOpen()
      
       startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
       newDate = ConvertStrToDate(startDateAfterRollForword)
       cashwise.frmCalendarEdit.btnSave.Click()
       cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
       while newDate <= stopOnDate:
      
          #SetFocus(cashwise("frmMain"))
          Sleep(100)
          RollForwardLOCNoReportsCalendarOpen()
          Sleep(100)
          startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
          newDate = ConvertStrToDate(startDateAfterRollForword)
          if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1000).Exists):
              cashwise.frmCalendarEdit.btnSave.Click()
          cashwise.Window("TMessageForm", "Information", 1).OK.Click()
          
def ViewFirstLOCStatement(cusName):  #From LOC Account Manager 
   #Author Phil 8/6/2019
   #Last Modified by Phil 8/6/2019 
    #cusName = "Clarity, Cher"
    browser = ""
    cusID = GetCustomerIDFromName(cusName)
    cashwise = Sys.Process("Cashwise")
    cashwise.frmLOCManager.MainMenu.Click("[5]|[0]") #Statements Print Statements
    cashwise.frmLOCDocumentBrowse.gbFilter.edFromFiscalDate.TAdvancedSpeedButton.Click()
    #cashwise.frmCalendarEdit.Panel1.btnPriorMonth.Click() On first statement cycle is one day
    writtenDate = cashwise.frmCalendarEdit.txtDate.Caption 
    cashwise.frmCalendarEdit.btnClose.Click()
    month = GetMonthFromString(writtenDate)
    year = GetYearFromString(writtenDate)
    expectedFileName = month+year+"Statement_"+cusID
    
    ExplicitWait(1)
    printStatements = cashwise.frmLOCDocumentBrowse.gbFilter
    printStatements.cbFilterByCustomerName.ClickButton(cbChecked)
    printStatements.edCustomerName.Keys(cusName)
    printStatements.DBCheckBox2.ClickButton(cbUnchecked)
    printStatements.pnlFilterTopButtons.btnApplyFilter.Click()
    
    ExplicitWait(1)
    cashwise.frmLOCDocumentBrowse.asbView.Click()
    ExplicitWait(1)
#    if Sys.Process("AcroRd32").Exists:
    if Sys.Process("FoxitReader").Exists:
      Log.Message("Can View PDF in Cashwise")
      browser = Sys.Process("FoxitReader") 
    else:
      Log.Message("===--Can not View PDF--------====")
    
    #cap = Sys.Process("AcroRd32", 2).Window("AcrobatSDIWindow").WndCaption
    cap = Sys.Process("FoxitReader").Window("classFoxitReader", "*Foxit Reader", 1).WndCaption
    Log.Message(cap)
    
    browser.Close()
    ExplicitWait(1)
    if(browser.WaitWindow("#32770","Foxit Reader",1).Exists):
      browser.Window("#32770", "Foxit Reader", 1).Window("Button", "Close a&ll tabs", 1).Click()
    
    #Sys.Process("AcroRd32",2).Window("AcrobatSDIWindow").Close()
    # cap == 'August2010Statement_001-0001137.pdf - Adobe Acrobat Reader DC'
    capLength = aqString.GetLength(cap)
    startPDF = aqString.Find(cap,".pdf",0,False)
    fileName = aqString.SubString(cap,0,startPDF)
    fileName =  aqString.Replace(fileName, "'", "")
    Log.Message("Expected file name = "+expectedFileName)
    Log.Message("Actual file name = "+fileName)
    ExplicitWait(1)
    cashwise.frmLOCDocumentBrowse.btnClose.Click()
    ExplicitWait(1)
    #Code crashes here----Sys.Process("Cashwise").VCLObject("frmLOCDocumentBrowse")
    if(cashwise.WaitWindow("TfrmLOCDocumentBrowse","Print Statements",1,800).Exists):
      cashwise.frmLOCDocumentBrowse.btnClose.Click()
    cashwise.frmLOCManager.btnClose.Click()
    
    if fileName == expectedFileName:
      Log.Checkpoint("PDF has the correct file name in the UI")
    else:
      Log.Error("PDF file names do not match")
            


def GetMonthFromString(writtenDate): # On Calendar after rollforward, get date on Calendar
   #Author Phil 8/6/2019
   #Last Modified by Phil 8/6/2019  
  #  writtenDate = "Mon Sep 20 2010"
   prevSep = aqString.ListSeparator
   aqString.ListSeparator = " "
   month = aqString.GetListItem(writtenDate, 1)
   monthLength = aqString.GetLength(month)
   regEx = "([a-zA-Z])"
   
   Matches =  re.findall(regEx,month)
   if Matches:
      ResStr = ""
   for i in Matches:
       ResStr=ResStr + str(i) 
   Log.Message("extracted = "+ResStr)
   month = aqConvert.VarToStr(ResStr)
   month =  MonthToMonth(month)
   return month

def GetYearFromString(writtenDate): # On Calendar after rollforward, get date on Calendar 
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 8/1/2019  ----------'Thu Sep 23 2010'
   # writtenDate = "'Thu Sep 23 2010'"
    prevSep = aqString.ListSeparator
    aqString.ListSeparator = " "
    year = aqString.GetListItem(writtenDate, 3)
    year = aqString.Replace(year, "'", "")
    Log.Message("Year = "+year)   
    year = aqConvert.VarToStr(year)   
    Log.Message(year)
    return year
    

def MonthToMonth(min):
  
  mOUT  = ""
     
  if min == "Jan":  
               mOUT = "January"
           
  if min == "Feb" :
               mOUT = "February"
           
  if min == "Mar":
               mOUT = "March"
           
  if min == "Apr":      
            mOUT = "April"
           
  if min == "May":       
               mOUT = "May"
           
  if min == "Jun":       
               mOUT = "June"
           
  if min == "Jul":       
              mOUT = "July"
           
  if min == "Aug":       
               mOUT = "August"
           
  if  min == "Sep":       
               mOUT = "September"
           
  if min == "Oct":       
               mOUT = "October"
           
  if min == "Nov":       
               mOUT = "November"
           
  if min == "Dec":       
               mOUT = "December"            
  Log.Message(mOUT)       
    
  return mOUT
    
    
def LOCPaymentHoldACH(enableHolds,defaultDays,maxHoldDays):
  cashwise = Sys.Process("Cashwise")
  LOCEditSetup()
  LocTypePaymentTab()
  LocACHHoldPaymentEdit(enableHolds,defaultDays,maxHoldDays)
  cashwise.frmSystemSetup.btnSave.Click()
  


def LOCEditSetup(): 
    #Author Phil Ivey 8/1/2019
    #Last Modified by Phil Ivey 10/29/2019 
    cashwise = Sys.Process("Cashwise")
    CashwisaMainMenu_File("Setup")
    SystemSetupTree("Line of Credit")
    cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.btnAccountTypes.Click()
    cashwise.frmLOCAccountTypeBrowse.btnEdit.Click()
    if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
              cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()

   
def LocTypeStatementTab():
    cashwise = Sys.Process("Cashwise")
    locAcctType = cashwise.frmLOCAccountTypeEdit
    locAcctType.pnlMain.sbData.pgSettings.ClickTab("State&ments")
    
    stateTab = cashwise.frmLOCAccountTypeEdit.pnlMain.sbData.pgSettings.tsStatements.GroupBox1
    stateTab.edBillingStatementReportA.TAdvancedSpeedButton.Click()
    
    cashwise.frmReportList.pnlSearch.isMain.Keys("LOCPERIODICSTATMENTCC")
    cashwise.frmReportList.btnSave.Click()
    if(cashwise.WaitWindow("#32770", "Cashwise", 1,500).Exists):
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
      Log.Error("Report is not Loaded")
    stateTab.dbedEmailProfileName.Keys("smtp.mandrillapp.com")
    stateTab.edSMTPUserName.Keys("mandrillapp@checkcity.com")
    stateTab.edSMTPPassword.Keys("ecXKy6fj1Od6oWEkMFxZaw")
    stateTab.sdSMTPPort.Keys("587")
    stateTab.memEmailBody.Keys("Thanks for Testing")
    stateTab.dbedEmailReplyTo.Keys("info@CheckCity.com")
    stateTab.dbedEmailFromAddress.Keys("info@CheckCity.com")
    stateTab.dbedEmailSubject.Keys("Softwise LOC Test Statement")
    cashwise.frmLOCAccountTypeEdit.btnSave.Click()
    cashwise.frmLOCAccountTypeBrowse.btnSave.Click()
    cashwise.frmSystemSetup.btnSave.Click()
    
def IsAutoPayActive():
  
  cashwise = Sys.Process("Cashwise")  
  page = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "")
  if(page.Find("textContent","ACTIVE",25).Exists):
    autoPayment = "ACTIVE"
  else:
    autoPayment = "INACTIVE"
  return autoPayment  

def LocTypePaymentTab():
    cashwise = Sys.Process("Cashwise")
    locAcctType = cashwise.frmLOCAccountTypeEdit
    locAcctType.pnlMain.sbData.pgSettings.ClickTab("&Payments")
    # todo fill out values for statement
    
 
    
    
    
def LocACHHoldPaymentEdit(enableHolds,defaultDays,maxHoldDays):
    
  cashwise = Sys.Process("Cashwise")
  payHoldAch = cashwise.frmLOCAccountTypeEdit.pnlMain.sbData.pgSettings.tsPayments.gbACHHoldSetup
  payHoldAch.cbEnableACHHold.ClickButton(cbChecked)
  payHoldAch.edDefaultHoldDays.Keys(defaultDays)
  payHoldAch.edMaxACHHoldDays.Keys(maxHoldDays)
  cashwise.frmLOCAccountTypeEdit.btnSave.Click()
  cashwise.frmLOCAccountTypeBrowse.btnSave.Click()
  
       
def AddLocCustomer():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer   
    
 
def ChangeLocLimit(cusID,direction):
#  cusID = "001-0000421"
#  direction = "Up"
  Log.Message("----Changing limit "+direction+"-----")
  locLimit = GetLocLimit(cusID)
  if(direction == "Up"):
    locLimit2 = (locLimit * 1.2)
    locLimit = aqConvert.FloatToStr(locLimit)
    locLimit2 = aqConvert.CurrencyToStr((aqConvert.StrToCurrency((aqConvert.FloatToStr(locLimit2)))))
    Log.Message("---------- Increasing LOC Limit from "+locLimit+" To "+locLimit2+"-------------")
    locLimit2 = aqString.Replace(locLimit2,"$","")
    locLimit2 = aqString.Replace(locLimit2,",","")
  if(direction == "Down"):
    locLimit2 = (locLimit/1.2)
    locLimit = aqConvert.FloatToStr(locLimit)
    locLimit2 = aqConvert.CurrencyToStr((aqConvert.StrToCurrency((aqConvert.FloatToStr(locLimit2)))))
    Log.Message("---------- Decreasing LOC Limit from "+locLimit+" To "+locLimit2+"-------------")
    locLimit2 = aqString.Replace(locLimit2,"$","")
    locLimit2 = aqString.Replace(locLimit2,",","")
  anUpDate = "Update CustomerLimitByLoanTypeCategory set DeferredLimit = "+locLimit2+" where Customer_ID = '"+cusID+"' and Category_ID = 'LOC'"
  RunUpdate(anUpDate)
  afterUpdateLimit = GetLocLimit(cusID)
  afterUpdateLimit = aqConvert.FloatToStr(afterUpdateLimit)
  Log.Message("---------- The LOC Limit is now  "+afterUpdateLimit+"-------------")
  RunLOCPercentOfCreditLimit(cusID) #----THIS EXECUTES LOCPercentOfCreditLimit
  
def OpenLOCPOS():
    cashwise = Sys.Process("Cashwise")
    cashwise.frmMain.TGraphicButton.Click()
    if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,1000).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,2000).Exists):
      cashwise.VCLObject("frmCashOpenConfirm").VCLObject("Button1").Click()
    cashwise.frmSaleEdit.pnlMain.pnlPOSButtons.Window("TAdvancedButton", "* Line of Credit", 23).Click()

def DeleteReportFile(cashFilePath):
  #cashFilePath = "C:\\temp\\OptionalPaymentAuthweb45.txt"
  testPassed = False
  if(aqFileSystem.Exists(cashFilePath)):
    aqFile.Delete(cashFilePath)

 
   
    
def Open_CancelLOCForReport(cusID,testResults):
  cashwise = Sys.Process("Cashwise")
 
  OpenLOCPOS()
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
  ExplicitWait(2)
  while(cashwise.WaitWindow("TfrmWarningDisplay","Warning",1,2000).Exists):  #if any warning appear
    ExplicitWait(3)
    cashwise.VCLObject("frmWarningDisplay").VCLObject("btnSave").Click()
    
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists):
    testResults = CheckForOptionalPayAuth(cusID,testResults)
      
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists == True):
    cashwise.RavePreviewForm.Keys("~fx")
  ExplicitWait(1)
  WaitLOCBrowser()
  
  ExplicitWait(1)
  ClickCancelMainWebPage() 
 
  if(testResults["Results1"] != "ReportTitlePassed" and testResults["Results2"] != "ReportPopulatedPassed"): 
     if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1100).Exists): 
       testResults = CheckForOptionalPayAuth(cusID,testResults) 
 
  ExplicitWait(1)
  return testResults       
  
def ProcessLOCTransTestForReport(cusID,newCus,testResults):
  cashwise = Sys.Process("Cashwise")
  amount = "50.00" # NEED THE CENTS
  drawAmount = "200.00"
  
  OpenLOCPOS()
  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
  ExplicitWait(3)
  while(cashwise.WaitWindow("TfrmWarningDisplay","Warning",1,2000).Exists):
    cashwise.VCLObject("frmWarningDisplay").VCLObject("btnSave")
    
    
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists):
    testResults = CheckForOptionalPayAuth(cusID,testResults)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists):  
    cashwise.RavePreviewForm.Keys("~fx")
  
    
    
    
  if(cashwise.WaitWindow("TfrmWarningDisplay","Warning",1,800).Exists):
    cashwise.frmWarningDisplay.btnSave.Click()
    
  WaitLOCBrowser()
  
  results2 = testResults["Results2"]
  
  res = aqString.Find(results2,"Passed",0,True)
  if(res != -1):
      Log.Message("Report has already appeared and was populated")
      ClickCancelMainWebPage()
      while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 
              Log.Message("------------------------Rave Report Preview Appeared ------------------------------------------")
              cashwise.RavePreviewForm.Keys("~fx")
              ExplicitWait(1)
      cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
      return testResults
  
  if(newCus == 1):#  THIS IS A NEW CUSTOMER
      isDrawButtonVisable = IsButtonThere(cusID,"Draw")
      if isDrawButtonVisable == True:
        ClickInitialDrawButton()
        EnterDrawAmount("200.00")
        ClickDrawButton(drawAmount)
      else: # Click the payment button
        ClickPayButton(amount)# MUST PASS THE AMOUNT BECAUSE THERE ARE TOO MANY OBJECTS THAT START WITH "Pay $"
        EnterPaymentAmount(amount)
        isPaidInFull = HowPaymentApplied()
        if(isPaidInFull == "No"):
          ClickPayButton(amount)
  else: # --------THIS IS NOT A NEW CUSTOMER SO MAKE A SMALL PAYMENT
    ClickPaymentAmount()  
    EnterPaymentAmount(amount)
    isPaidInFull = HowPaymentApplied()
    if(isPaidInFull == "No"):
       ClickPayButton(amount) # MUST PASS THE AMOUNT BECAUSE THERE ARE TOO MANY OBJECTS THAT START WITH "Pay $"
    else:
      ClickPayInFull()
  ClickConfirmButtonOnly()
  ExplicitWait(1)
  ["FullName"]
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists):
    testPassed = CheckForOptionalPayAuth(cusID)
 
    
    
  testPassed = (cusID,"Yes")
  #testPassed = CheckForOptionalPayAuth()
  ProcessTrasaction()
  ExplicitWait(1)
  if(cashwise.WaitWindow("TfrmSaleEdit","Sales Transaction",1,1000).Exists):
      cashwise.frmSaleEdit.btnClose.Click()
  return testPassed
  
  
def ProcessLOCTransTestACHHold(): #cusID,newCus  (
    #Author Phil Ivey  9/12/2019
    #Last Modified by Phil Ivey  9/12/2019
    #LOCPaymentHoldACH(True,2,4) (Example Cashwise date 8/2/2019 Payment Schedule past due, default hold days 8/4/2019, Available hold 3,4,5,6) 
  
  cashwiseDate = GetLocationDate("001")
  locDate = aqConvert.StrToDate(cashwiseDate)
  defaultHoldSetting = aqDateTime.AddDays(locDate,2)
  sDefaultHoldSetting = aqConvert.DateTimeToFormatStr(defaultHoldSetting,"%m/%d/%Y")
                        
#  OpenLOCPOS()
#  CustomerBrowseSearchBrowseScreen(cusID,"ID","Save & Close")
#  WaitLOCBrowser()
  page = cashwise.frmBrowser.WinFormsObject("pnlBrowser")
#  if(newCus == 0): # customer has LOC history
#      ClickHoldButton()
  if((page.Find("ObjectIdentifier","mat_input_0",15).Exists) == True):
        holdBox =  page.Find("ObjectIdentifier","mat_input_0",15)
        defaultHold = holdBox.Text
        defaultUI = aqConvert.StrToDate(defaultHold)
        sDefaultUI = aqConvert.DateTimeToFormatStr(defaultUI,"%m/%d/%Y")
        if(sDefaultHoldSetting == sDefaultUI):
          Log.Checkpoint("Default Hold Days Passed")
  else:
      Log.Message("can't find date")

  
  if((page.Find("nodeName","SVG",15).Exists) == True):
    calendar = page.Find("nodeName","SVG",15)
    calendar.Click()
############################################
  
  if((page.Find("contentText","S",15).Exists) == True):     
    Log.Message("calandar up")
    date1 = page.Find("contentText","18",15)
    date1.DblClick()
  
  if((page.Find("contentText","S",15).Exists) == False):       
    Log.Message("calendar down number was active")
  Log.Message("here")
 
  
  
  
   
def CheckForOptionalPayAuthMovedToHelper():
  testPassed = False  
  Indicator.PushText("In CheckForOptionalPayAuth")
  cashFilePath  = "C:\\temp\\OptionalPaymentAuth.txt"
  cashwise = Sys.Process("Cashwise")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists == False):
    return testPassed  #----testPassed = False returned
  elif(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists):
        cashwise.RavePreviewForm.TImage.Keys("~fs")
  ExplicitWait(1)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys("C:\Temp\OptionalPaymentAuth")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Click()
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[End]")
  cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  if(cashwise.WaitWindow("#32770", "Save File", 1,500).Exists):
        cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  cashwise.RavePreviewForm.TImage.Keys("~fx")
  reportSubString = "Optional      Payment      Authorization"
  testPassed = CheckForFile(cashFilePath,reportSubString)
  if testPassed == True:
          Log.Message("CheckForOptionalPayAuth PASSED")
  return testPassed

  
  
def CheckForLOCAgreementAndPlan():
  testPassed = False  
 
  DeleteReportFile("C:\\temp\\CheckForLOCAgreementAndPlan.txt")
  Indicator.PushText("In CheckForLOCAgreementAndPlan")
  cashFilePath  = "C:\\temp\\CheckForLOCAgreementAndPlan.txt"
  cashwise = Sys.Process("Cashwise")
  cashwise.RavePreviewForm.TImage.Keys("~fs")
  ExplicitWait(1)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys("C:\Temp\CheckForLOCAgreementAndPlan")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Click()
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[End]")
  cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  if(cashwise.WaitWindow("#32770", "Save File", 1,500).Exists):
    cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  cashwise.RavePreviewForm.TImage.Keys("~fx")
  reportSubString = "LINE OF CREDIT AGREEMENT AND PLAN"
  testPassed = CheckForFile(cashFilePath, reportSubString)
  if testPassed == True:
      Log.Message("CheckForLOCAgreementAndPlan PASSED")
  return testPassed
  
 
def CheckOptionalPaymentAuth():
  testPassed = False  
  DeleteReportFile("C:\\temp\\OptionalPaymentAuth.txt")
  
  Indicator.PushText("In OptionalPaymentAuth")
  cashFilePath = "C:\\temp\\OptionalPaymentAuth.txt"   

  cashwise = Sys.Process("Cashwise")
  cashwise.RavePreviewForm.TImage.Keys("~fs")
  ExplicitWait(1)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys("C:\Temp\OptionalPaymentAuth")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Click()
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[End]")
  cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  if(cashwise.WaitWindow("#32770", "Save File", 1,500).Exists):
    cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  cashwise.RavePreviewForm.TImage.Keys("~fx")
  reportSubString = "Optional      Payment      Authorization"
  testPassed = CheckForFile(cashFilePath, reportSubString)
  if testPassed == True:
      Log.Message("CheckForLOCAgreementAndPlan PASSED")
  ExplicitWait(2)
  return testPassed
  
def CheckForFile(cashFilePath, reportSubString):
  testPassed = False
 
  if(aqFileSystem.Exists(cashFilePath)):
   
    cashFile = aqFile.OpenTextFile(cashFilePath,aqFile.faRead,aqFile.ctANSI)  #aqFile.ctUnicode
   
    while not cashFile.IsEndOfFile():
        s = cashFile.ReadLine()
        res = aqString.Find(s,reportSubString,0,True)
        if(res != -1):
          Log.Message("found report string "+reportSubString)
          testPassed = True
  cashFile.Close()    
  if testPassed == True:
    Log.Message("The correct file was presented")
  return testPassed

def CheckLOCAutoPayAccountNumber(cusID,newRouteAcctNum):
      #Author Phil Ivey  11/19/2019
      #Last Modified by Phil Ivey  11/19/2019

  #cusID = "001-0000494"    newRouteAcctNum = "243385252422119729013"
  cashwise = Sys.Process("Cashwise")
  CashwiseMainMenu_Sales("Line of Credit Manager")
  filter = cashwise.frmLOCManager.pnlFilter.gbFilter
  filter.DBCheckBox1.ClickButton(cbChecked)
  cashwise.frmLOCManager.pnlFilter.gbFilter.DBRadioGroup1.Window("TGroupButton", "Open", 5).Click()
  filter.DBLookupEditForm1.Window("TCustomDBEdit", "", 1).Keys(cusID)
  filter.btnApplyFilter.Click()
  cashwise.frmLOCManager.btnAccountView.Click()
  cashwise.frmLOCAccountView.pnlMain.sbData.pnlPaymentInfo.pcPaymentInfo.ClickTab("ACH Auto Pay")
  achRouteAcctNum = cashwise.frmLOCAccountView.pnlMain.sbData.pnlPaymentInfo.pcPaymentInfo.tsACHAutoPay.dbedLOCPaymentPlanAccount.WndCaption
  if(newRouteAcctNum == achRouteAcctNum):
    testResults = "Passed"
  else:
    testResults = "Failed"
  
  cashwise.frmLOCAccountView.btnClose.Click()
  cashwise.frmLOCManager.btnClose.Click()
  
  return testResults
  
  
  


  
def CloseAllLOCAccounts():
      #Author Phil Ivey  11/19/2019
      #Last Modified by Phil Ivey  11/19/2019  
  cashwise = Sys.Process("Cashwise")
  locID = "001"
  numOfOpenLOCAccts = GetNumOfLOCAcctsOpen(locID)
  CashwiseMainMenu_Sales("Line of Credit Manager")
  filter = cashwise.frmLOCManager.pnlFilter.gbFilter
  accountStatus = filter.DBRadioGroup1

  #accountOpenStatus = accountStatus.Find("WndCaption","Open",10)
  accountOpenStatus = accountStatus.Window("TGroupButton", "Open", 5)
  accountOpenStatus.ClickButton()
  filter.DBCheckBox2.ClickButton(cbChecked)
  filter.DBLookupEditForm2.TAdvancedSpeedButton.Click()
  cashwise.frmLocationList.pnlMain.pnlSearch.isMain.Keys(locID)
  cashwise.frmLocationList.btnSave.Click()
  filter.btnApplyFilter.Click()
  
  for x in range(numOfOpenLOCAccts):
    cashwise.frmLOCManager.btnCloseAccount.Click()
    if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    ExplicitWait(1)
    cashwise.frmLOCCloseAccountEdit.pnlMain.sbData.dbleReasonID.TAdvancedSpeedButton.Click()
    cashwise.frmReversalReasonBrowse.btnSave.Click()
    cashwise.frmLOCCloseAccountEdit.btnSave.Click()
    accountOpenStatus = accountStatus.Window("TGroupButton", "Closed", 4)
    accountOpenStatus.ClickButton()
    filter.btnApplyFilter.Click()
    accountOpenStatus = accountStatus.Window("TGroupButton", "Open", 5)
    accountOpenStatus.ClickButton()
    filter.btnApplyFilter.Click()
  Log.Message("LOC Accounts Closed")
  
def StatusAfterFirstPaymentMissedS1(cusID):
      #Author Phil Ivey  11/19/2019
      #Last Modified by Phil Ivey  11/19/2019
  nextBillDate = GetNextBillingDateCus(cusID)
  RollForwardTo(nextBillDate)
  RollForwardLOCNoReports()
  LOCPaymentFromMain(cusID,"60.00")
  CloseCashDrawer()
  RollForwardNoReports(5)
  LOCPaymentFromMain(cusID,"40.00")
  #Status should be open "OPE"
  actualStatus = GetLOCStatusWithCusIDMain(cusID)
  return actualStatus
 
  
def LOCPaymentFromMain(cusID,amount):
      #Author Phil Ivey  11/19/2019
      #Last Modified by Phil Ivey  11/19/2019
    OpenLineOfCreditWithCusID(cusID)
    ClickPaymentAmount()
    EnterPaymentAmount(amount)  # make sure to add the cents 
    ClickPayButton(amount)
    ClickConfirmButtonOnly()
    SignCustomerUpForAutoPay(cusID,"No")
    ProcessTrasaction()
    #Sys.Process("Cashwise").VCLObject("frmSaleEdit").SetFocus()
    #use cashwise window to bring screen to front
    CashwiseMainMenu_Window("Top")
    if(cashwise.WaitWindow("TfrmSaleEdit","Sales Transaction",1.1200).Exists):
      cashwise.frmSaleEdit.btnClose.Click()
    else:
      CashwiseMainMenu_Window("Top")
      Sys.Process("Cashwise").VCLObject("frmSaleEdit").VCLObject("btnClose").Click()
      
  
def GetLOCStatusWithCusIDMain(cusID):
      #Author Phil Ivey  11/19/2019
      #Last Modified by Phil Ivey  11/19/2019
  cashwise = Sys.Process("Cashwise")
  CashwiseMainMenu_Sales("Line of Credit Manager")
  cashwise.frmLOCManager.pnlFilter.gbFilter.DBLookupEditForm1.Keys(cusID)
  cashwise.frmLOCManager.pnlFilter.gbFilter.DBCheckBox1.ClickButton(cbChecked)
  cashwise.frmLOCManager.pnlFilter.gbFilter.DBRadioGroup1.Window("TGroupButton", "Open", 5).Click()
  cashwise.frmLOCManager.pnlFilter.gbFilter.btnApplyFilter.Click()
  cashwise.frmLOCManager.btnAccountView.Click() 
  status = cashwise.frmLOCAccountView.pnlMain.sbData.dbleAccountStatus.Window("TCustomDBEdit", "*", 1).WndCaption
  cashwise.frmLOCAccountView.btnClose.Click()
  cashwise.frmLOCManager.btnClose
  
  return status
  
def CancelCashwiseBrowser():
   #Author Phil Ivey 12/03/2019
   #Last Modified by Phil Ivey 12/03/2019
   cashwise = Sys.Process("Cashwise")
   browser = cashwise.WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser")
   chromeBrowser = browser.WinFormsObject("ChromiumWebBrowser", "")
   cancelButton = chromeBrowser.Find("ObjectLabel","closeCancel",15)
   cancelButton.Click()
   ExplicitWait(2)
   cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
   

  
  