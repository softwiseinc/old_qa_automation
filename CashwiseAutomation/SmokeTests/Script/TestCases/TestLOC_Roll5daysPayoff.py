﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
                                           
def TestLOC_Roll5daysPayoff():
  #Author Pat Holman 7/15/2019
  #Last Modified by Pat Holman 7/15/2019
  Indicator.PushText("RUNNING: TestLOC_Roll5daysPayoff()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  OpenCashDrawer()
  customer = GetTempCustomer()
  AddCustomer(customer)
  AddNewEmployer(customer)
  AddCategoryLocPassLimit(customer,"5000")
  AddBankAccount(customer)
  LineOfCreditMaxLoan(customer)
  CloseSalesTrasaction()
  CloseCashDrawer()
  RollForwardNoReports(5)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestLOC_Roll5daysPayoff()")
  Indicator.PopText()

