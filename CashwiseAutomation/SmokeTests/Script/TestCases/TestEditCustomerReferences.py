﻿TestedApps.Cashwise.Run()
from Customers import *

def Test_EditCustomerReferences():
  #Author Pat Holman 6/3/2019
  #Last Modified by Pat Holman 6/3/2019
  Indicator.PushText("RUNNING: TestEditCustomerReferences()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  editCustomer = GetEditCustomer()
  CreateCustomer(customer)
  EditCustomerReferences(customer, editCustomer)
  DeleteCustomer(customer)
  CloseCashwiseApp()  
  Log.Event("PASSED TEST: AddCustomer() New Customer Added")
  Indicator.PopText()
