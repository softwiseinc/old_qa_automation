﻿TestedApps.Cashwise.Run()
from LineOfCredit import *

def TestLOCPrintStatements8426():
  #Author Phil Ivey  7/3/2019
  #Last Modified by Phil Ivey  8/5/2019
  
  Indicator.PushText("RUNNING: TestLOCStatements()")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempContact()
  fullName = customer["FullName"]
  locationID = GetLocationIDFromfrmMainWndCap()
  locationYear = GetCashwiseLocationYear(locationID)
  AddCustomer(customer)
  cusID = GetCustomerIDFromName(fullName)
  routingAcctNum = AddCusBankAccountBack(cusID,"001")
  customer["BankNumber"] = routingAcctNum
  OpenAndClosePOS()
  AddNewEmployer(customer)
  Log.Message(customer["LastName"])  
  Log.Message(customer["FirstName"])
  locLimit = AddCategoryLocRandomLimit(customer)
  LineOfCreditInitialDrawAmount(customer,locLimit)
  ############GetDrawAmount(amount)
  DeleteRecreateLOCfileShare(locationYear)
  CloseCashDrawer()
  RollForwardLOCNoReports()
  OpenAndClosePOS()
  CashwiseMainMenu_Sales("Line of Credit Manager")
  ViewFirstLOCStatement(fullName)

  Log.Event("PASSED TEST: TestLOCStatements()")
  Indicator.PopText()
  CloseCashwiseApp()
