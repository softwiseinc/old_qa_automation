﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
from Customers import *
                                           
def TestLineOfCreditMinLoan():
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019
  Indicator.PushText("RUNNING: TestLineOfCreditMinLoan")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  #customerId = GetExistingCustomer("PreviousLoanNotPending")
  
  cusID = GetCustomerID_WithLocData()
  customer = LoadCustomerObject2(cusID)
  
  Log.Message("Customer name =  "+customer["FullName"])
  customer = "Empty"
  if customer == "Empty":
      Log.Message("No existing customers creating temp customer")
      customer = AddLocCustomer()
      AddBankAccount(customer)
  
  
  CheckCustomerValuesLog(customer)
  LineOfCreditMinLoan(customer)
#  CashwiseMainMenu_Window("Top")
#  ExplicitWait(1)
#  cashwise.VCLObject("frmSaleEdit").VCLObject("btnClose")
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestLineOfCreditMinLoan() Min LOC")
  Indicator.PopText()
