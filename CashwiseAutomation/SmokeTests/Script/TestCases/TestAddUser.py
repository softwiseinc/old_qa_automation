﻿TestedApps.Cashwise.Run()
from UsersHelper import *

def Test_AddNewUser():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019
  Indicator.PushText("RUNNING: TestAddNewUser()")
  managerUser = GetManager001()
  cashwise = StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])    
  OpenUserMaintenance()
  cashwiseUser = GetUser001()
  AddNewUser(cashwiseUser)      
  ValidateNewUserCreated(cashwiseUser)
  DeleteUserIfRandom(cashwiseUser)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestAddNewUser()")
  Indicator.PopText()

  
      