﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from SalesHelper import OpenPOSForTheDayClosePOS

def TestSetupLineOfCredit():
  #Author Pat Holman 4/24/2019
  #Last Modified by Phil Ivey 8/02/2019
  Indicator.PushText("RUNNING: TestSetupLineOfCredit()")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  AddCustomer(customer)
  OpenPOSForTheDayClosePOS()
  AddNewEmployer(customer)
  AddCategoryLOC(customer)
  DeleteCustomer(customer)
  CloseCashwiseApp()  
  Log.Event("PASSED TEST: TestSetupLineOfCredit()")
  Indicator.PopText()

