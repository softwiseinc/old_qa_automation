﻿TestedApps.Cashwise.Run()
from LineOfCredit import *

def TestLOCViewStatements():
  #Author Phil Ivey  7/3/2019
  #Last Modified by Phil Ivey  8/5/2019

  Indicator.PushText("RUNNING: TestLOCStatements()")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  locationID = GetLocationIDFromfrmMainWndCap()
  locationYear = GetCashwiseLocationYear(locationID)
  #DeleteRecreateLOCfileShare(locationYear)
  customer = GetTempContact()
  AddCustomer(customer)
  routingAcctNum = AddCusBankAccountBack(customer["cusID"],"001")
  customer["BankNumber"] = routingAcctNum
  OpenAndClosePOS()
  AddNewEmployer(customer)
  locLimit = AddCategoryLocRandomLimit(customer)
  #LineOfCreditInitialDraw(customer)
  LineOfCreditInitialDrawAmountAutoPay(customer,locLimit)
  RollForwardLOCNoReports()
  OpenAndClosePOS()
  CashwiseMainMenu_Sales("Line of Credit Manager")
  ViewFirstLOCStatement(customer["FullName"])

  Log.Event("PASSED TEST: TestLOCStatements()")
  Indicator.PopText()
  CloseCashwiseApp()

