﻿TestedApps.Cashwise.Run()
from EmploymentHelper import *
from Customers import *

def Test_AddEmployment():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/17/2019  
  Indicator.PushText("RUNNING: TestAddEmployment()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetTempCustomer()
  CreateCustomer(customer)
  AddNewEmployer(customer)
  DeleteCustomer(customer)
  CloseCashwiseApp()
  Log.Event("PASSED TEST: AddEmployment() New Employment Added")
  Indicator.PopText()
  