﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
                                           
def TestLineOfCreditMaxLoan():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/17/2019
  Indicator.PushText("RUNNING: TestAddEmployment()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  #customerId = GetExistingCustomer("PreviousLoanNotPending")
  customer = GetTempCustomer()
  AddCustomer(customer)
  AddNewEmployer(customer)
  AddCategoryLocPassLimit(customer,"5000")
  AddBankAccount(customer)
  LineOfCreditMaxLoan(customer)
  CloseSalesTrasaction()
  CloseCashwiseApp()
  Log.Event("PASSED TEST: AddEmployment() New Employment Added")
  Indicator.PopText()

