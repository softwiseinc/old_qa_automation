﻿TestedApps.Cashwise.Run()
from Customers import *

def TestRepayStoredCardPOS():
    #Author y 9/03/2019
    #Last Modified by Phil I9/03/2019  
    Indicator.PushText("RUNNING: TestRepayStoredCardPOS()")
    managerUser = GetManager001()
    StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
    StoredBankCardCustomerEdit()
    
    
    
def StoredBankCardCustomerEdit():        
    #  customer edit>Details>Stored card>Add
    cashwise = Sys.Process("Cashwise")
    cashwise.frmMain.MainMenu.Click("Contact|Customer Maintenance...")
    cashwise.frmCustomerBrowse.btnEdit.Click()
    cashwise.frmCustomerEdit.Keys("~[Release]tb")
    
    
    # POS
    