﻿#TestedApps.Cashwise.Run()
from LineOfCredit import *

def TestLOCAchHold():
  #Author Phil Ivey  9/12/2019
  #Last Modified by Phil Ivey  9/12/2019
  #LOCPaymentHoldACH(True,2,4)
  
  Indicator.PushText("RUNNING: TestLOCAchHold")
  managerUser = GetManager002()
#  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])  

  cusID = GetLocCusIdWithAutoPay()
  newCus = 0
  
  if cusID == "Empty":
      Log.Message("No existing customers creating temp customer")
      customer = AddLocCustomer()
      AddBankAccount(customer)
      cusID = customer["cusID"]
      newCus = 1
  
  ProcessLOCTransTestACHHold(cusID,newCus)
  
  Log.Message("here")
  
  
  

#  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestLOCAchHold() ----------- Stuff to do -----------")
  Indicator.PopText()
