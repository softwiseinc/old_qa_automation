﻿#-UserStory Bug 8766: LOC Past Due Status Not Correct


TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
from Customers import *
                                           
def TestLOC_Status_after_1st_payment_missed_9189():
  #Author Phil Ivey 11/18/2019
  #Last Modified by Phil Ivey 11/18/2019
  
  amount = "1150"
  Indicator.PushText("RUNNING: TestLOC_Status_after_1st_payment_missed_9189")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  locationID = GetLocationIDFromfrmMainWndCap()
  locationYear = GetCashwiseLocationYear(locationID)
  DeleteRecreateLOCfileShare(locationYear)
  cusID = GetCustomerID_WithLocData()
  
 
  if cusID == "Empty":
      Log.Message("No existing customers creating temp customer")
      customer = AddLocCustomer()
      AddBankAccount(customer)
  else:
    customer = LoadCustomerObject2(cusID)
  
  Log.Message("Customer name = "+customer["FullName"])
  CheckCustomerValuesLog(customer)
  LineOfCreditInitialDrawAmount(customer,amount)
  cusID = customer["cusID"] 
  #  Have cusID  
  actualStatus = StatusAfterFirstPaymentMissedS1(cusID)
  
  if(actualStatus == "OPE"):
    Log.Message("Status passed Test Case 9189")
    Log.Event("PASSED TEST: TestLOC_Status_after_1st_payment_missed_9189()")
  else:
    Log.Error("$100 worth of payments should have met the minumum required TEST FAILED")
  CloseCashwiseApp()

  Indicator.PopText()
