﻿TestedApps.Cashwise.Run()
from LineOfCredit import *

def TestLOCSetupStatements():
  #Author Phil Ivey  7/3/2019
  #Last Modified by Phil Ivey  8/5/2019
  
  Indicator.PushText("RUNNING: TestLOCSetupStatements")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  LOCEditSetup()
  LocTypeStatementTab()
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestLOCSetupStatements() ----------- Stuff to do -----------")
  Indicator.PopText()
  
