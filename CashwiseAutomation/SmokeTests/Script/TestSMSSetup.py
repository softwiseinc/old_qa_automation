﻿TestedApps.Cashwise.Run()
from SMS import *

def TestSMSSetup():
  #Author Phil Ivey  8/12/2019
  #Last Modified by Phil Ivey  8/12/2019
  
  Indicator.PushText("RUNNING: TestSMSSetup")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  SMSSetup()
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestSMSSetup() ----------- Stuff to do -----------")
  Indicator.PopText()
