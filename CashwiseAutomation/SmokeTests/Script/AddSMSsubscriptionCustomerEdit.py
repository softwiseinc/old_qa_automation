﻿#------------------------------------ This test has manual step
TestedApps.Cashwise.Run()
from SMSHelper import *
from Customers import *

def AddSMSsubscriptionCustomerEdit():
  #Author Phil Ivey 8/19/2019
  #Last Modified by Phil Ivey 8/19/2019
  Indicator.PushText("RUNNING: AddSMSsubscriptionCustomerEdit")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  cusID = GetCustomerID_WithNoSMSData()
  customer = LoadCustomerObject2(cusID)
  
  Log.Message("Customer name = "+customer["FullName"])
  if customer == "Empty":
      Log.Message("No existing customers creating temp customer")
      customer = GetTempCustomer()
      AddCustomer(customer)
      AddNewEmployer(customer)
      AddCategoryLOC(customer)
      AddBankAccount(customer)
      
  customer["PrimaryPhone"] = "8012109029"
  customer["PrimaryPhoneType"] = "CEL"
  EditPrimaryPhone(customer)
  SMSsubscribe(customer)
  Indicator.PopText()