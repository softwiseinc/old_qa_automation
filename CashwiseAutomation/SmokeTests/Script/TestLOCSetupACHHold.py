﻿TestedApps.Cashwise.Run()
from LineOfCredit import *

def TestLOCSetupACHHold():
  #Author Phil Ivey  7/3/2019
  #Last Modified by Phil Ivey  8/5/2019
  
  Indicator.PushText("RUNNING: TestLOCSetupACHHold")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])  

  LOCPaymentHoldACH(True,2,4)
  
  CloseCashwiseApp()
  Log.Event("PASSED TEST: TestLOCSetupACHHold()")
  Indicator.PopText()
