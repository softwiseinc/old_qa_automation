﻿
#TestLOCLimitReportCusWithHistoryIncrease
#TestLOCLimitReportCusWithHistoryDecrease

TestedApps.Cashwise.Run()
from LineOfCredit import *
from Customers import *

def TestLOCLimitReportCusWithHistoryIncrease():
  #Author Phil Ivey 8/21/2019
  #Last Modified by Phil Ivey 8/21/2019
  testResults = GetTestResultContainer()

  Indicator.PushText("RUNNING: TestLOCLimitReportCusWithHistoryIncrease")
  managerUser = GetManager002()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  cusID = GetLOCCusIdWith_LimitChangeLowest()
  newCus = 0
  cashwise = Sys.Process("Cashwise")
  
  if cusID == "Empty":
      Log.Message("No existing customers creating temp customer")
      customer = AddLocCustomer()
      AddBankAccount(customer)
      cusName = customer["FullName"]
      cusID = GetCustomerIDFromName(cusName)
      Log.Message("-----cus name- = "+cusName)
      Log.Message("cus id = "+cusID)
      LineOfCreditRandomWithAutoPay(customer)
      CashwiseMainMenu_Window("Top")
      if(cashwise.WaitWindow("TfrmSaleEdit","Sales Transaction",1,800).Exists): # Make sure Sales Transaction page is closed
        cashwise.frmSaleEdit.btnClose.Click()
        #cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel.Click()
        
      CloseCashDrawer()
      RollForwardNoReports(1)
      newCus = 1
  
  else:
      newCusS = aqConvert.IntToStr(newCus)    
      Log.Message("--------------Is New Customer = "+newCusS+" not new")
      Log.Message("--------------Customer ID is = "+cusID)
      CloseCashDrawer()
      RollForwardNoReports(1)


  ChangeLocLimit(cusID,"Up")
  DeleteReportFile("C:\\temp\\OptionalPaymentAuth.txt")
 
  testResults = Open_CancelLOCForReport(cusID,testResults)
  
  results1 = testResults["Results1"]
  results2 = testResults["Results2"]
  
  if(  results1 != "ReportTitlePassed" and results2 != "ReportPopulatedPassed"):
      if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists): 
         testResults = CheckForOptionalPayAuth(cusID,testResults)
         results1 = testResults["Results1"]
         results2 = testResults["Results2"]

  while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,8000).Exists): 
      cashwise.RavePreviewForm.Keys("~fx")
      ExplicitWait(5)
 

  cashwise.WaitWindow("#32770", "Cashwise", 1,3000)  
  cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
  CashwiseMainMenu_Window("Top")
  ExplicitWait(1)
  cashwise.VCLObject("frmSaleEdit").VCLObject("btnClose")

  Log.Message(results1)
  Log.Message(results2)
  
  
  if(  results1 == "ReportTitlePassed" and results2 == "ReportPopulatedPassed"):
    Log.Checkpoint("SUCCESS: TestLOCLimitReportCusWithHistoryIncrease() is successful.")
  else:
    Log.Error("Wrong report or report not populated")
  CloseCashwiseApp()