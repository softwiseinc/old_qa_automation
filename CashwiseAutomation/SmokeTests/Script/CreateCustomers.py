﻿from Customers import *



def TestCreateCus():
    #Author Phil Ivey 7/24/2019
    #Last Modified by Phil Ivey 7/24/2019
    customer = AddCustomer()
    AddBankAccount(customer)
    AddNewEmployer(customer)
    CheckCustomerValuesLog(customer)

    CloseCashwiseApp()