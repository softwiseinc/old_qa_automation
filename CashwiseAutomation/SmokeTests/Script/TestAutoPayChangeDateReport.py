﻿#-UserStory 7617
#-As a CSR I need the ability to print a new ACH authorization form when the ACH amount or ACH dates change so that I can have the customer sign the new document. 
#-CashwiseAuto>Smoke>LOC>Functionally>TestAutoPayChangeAmount 

#Select * from ReportEvent re
#join ReportEventReport rer on re.ID = rer.ReportEvent_ID
#join Report r on rer.Report_ID = r.ID
#where r.Name = 'LOCAutopayAuth'

TestedApps.Cashwise.Run()
from Employment import *
from Customers import *
from LineOfCredit import *

def TestAutoPayChangeDateReport():
  #Author Phil Ivey 9/03/2019
  #Last Modified by Phil Ivey 9/03/2019 
  testAutoPayChangeDateReportResults = ""
  Indicator.PushText("RUNNING: TestAutoPayChangeDateReport()")
  managerUser = GetManager001()
  testResults = GetTestResultContainer()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  cusID = GetCustomerID_WithLocDataAndAutoPay()
  cashwise = Sys.Process("Cashwise")
  #cusID = "Empty"
  if(cusID == "Empty"):
      Log.Message("No existing customers creating temp customer")
      customer = AddLocCustomer()
      AddBankAccount(customer)
      cusName = customer["FullName"]
      cusID = GetCustomerIDFromName(cusName)
      Log.Message("-----cus name- = "+cusName)
      Log.Message("cus id = "+cusID)
      LineOfCreditRandomWithAutoPay(customer)
      CashwiseMainMenu_Window("Top")
      if(cashwise.WaitWindow("TfrmSaleEdit","Sales Transaction",1,800).Exists): # Make sure Sales Transaction page is closed
        cashwise.frmSaleEdit.btnClose.Click()
        #cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel.Click()
        
      CloseCashDrawer()
      RollForwardNoReports(1)
      
  
  else:
      Log.Message("--------------Customer ID is = "+cusID)
      CloseCashDrawer()
      RollForwardNoReports(1)
 # RemoveReportFromEvent("LCA")# 
  testResults["Results3"] = "useDate"
  nextPaymentDateBefore = GetNextACHPaymentCus(cusID)
  DeleteReportFile("C:\\temp\\OptionalPaymentAuth.txt")  
  
  testResults = EditEmploymentPayDate(cusID,testResults)
  
  nextPaymentDateAfter = GetNextACHPaymentCus(cusID)
  
  while(nextPaymentDateBefore == nextPaymentDateAfter):
    EditEmploymentPayDate(cusID,testResults)
    nextPaymentDateAfter = GetNextACHPaymentCus(cusID)
  
  Log.Message("Scheduled date before employment payday edit = "+nextPaymentDateBefore)
  Log.Message("Scheduled date --AFTER-- employment payday edit = "+nextPaymentDateAfter)
  
  results1 = testResults["Results1"]
  results2 = testResults["Results2"]
  
  if(results1 == "ReportTitlePassed" and results2 == "ReportPopulatedPassed"):
    Log.Message("Report was populated and printed inside the employment edit")
    testAutoPayChangeDateReportResults = "ReportPopulatedPrinted"
  
    

  testResults["Results1"] = "Results1"
  testResults["Results2"] = "Results2"
  testResults["Results3"] = "UseDate"
  DeleteReportFile("C:\\temp\\OptionalPaymentAuth.txt")  
  testResults = Open_CancelLOCForReport(cusID,testResults)
  
  results1 = testResults["Results1"]
  results2 = testResults["Results2"]
  
  if(results1 != "ReportTitlePassed" and results2 != "ReportPopulatedPassed"):
    if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,2000).Exists): 
       testResults = CheckForOptionalPayAuth(cusID,testResults)
       results1 = testResults["Results1"]
       results2 = testResults["Results2"]
    else:
      Log.Message("The Report did not appear When opening LOC changing dates")
         
  while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,8000).Exists): 
      cashwise.RavePreviewForm.Keys("~fx")
      ExplicitWait(3)
   

  cashwise.WaitWindow("#32770", "Cashwise", 1,1000) 
  cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  #LOC item was canceled by the user.
  CashwiseMainMenu_Window("Top")
  cashwise.VCLObject("frmSaleEdit").VCLObject("btnClose")

  if(results1 == "ReportTitlePassed" and results2 == "ReportPopulatedPassed"):
      Log.Message("Report was populated and printed WHEN LOC was open")
      testAutoPayChangeDateReportResults = "ReportPopulatedPrinted"
  
  if(testAutoPayChangeDateReportResults == "ReportPopulatedPrinted"):
    Log.Message("TestAutoPayChangeDateReport --- PASSED---")

  Indicator.PopText()
  CloseCashwiseApp()



      
  
