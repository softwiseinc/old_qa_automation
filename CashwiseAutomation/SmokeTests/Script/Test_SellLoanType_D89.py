﻿TestedApps.Cashwise.Run()
from Customers import *
from SalesHelper import *
from Bank import *

def Test_SellLoanTypeD89():
  #Author Pat Holman 12/22/2020
  #Last Modified by Pat Holman 12/22/2020
  Indicator.PushText("RUNNING: Test_SellLoanTypeD89()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  customer = GetApprovedCustomer()
  AddCustomer(customer)
  AddBankAccount(customer)
  OpenPOS()
  ClickCashAdvanceButton()  
  ExplicitWait(2)
  SearchCustomerFirstName(customer)
  SellLoanTypeD89(customer)
  Log.Event("PASSED TEST: Test_SellLoanTypeD89()")
  Indicator.PopText()
  CloseCashwiseApp()  
 
