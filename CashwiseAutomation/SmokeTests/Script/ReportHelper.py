﻿from BasePage import *





def CheckForOptionalPayAuth(cusID,testResults):
  testPassed = False  
  Indicator.PushText("In CheckForOptionalPayAuth")
  cashFilePath  = "C:\\temp\\OptionalPaymentAuth.txt"
  cashwise = Sys.Process("Cashwise")
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists == False):
    return testPassed  #----testPassed = False returned
  elif(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,1200).Exists):
        cashwise.RavePreviewForm.TImage.Keys("~fs")
  ExplicitWait(1)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys(cashFilePath)
  cashwise.Window("#32770", "Save File", 1).Window("ComboBoxEx32", "", 1).Window("ComboBox", "", 1).Window("Edit", "", 1).Keys("[Tab]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down][Down][Down][Down]")
  cashwise.Window("#32770", "Save File", 1).Window("ComboBox", "", 2).Keys("[Down][Down]")
  cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
  if(cashwise.WaitWindow("#32770", "Save File", 1,500).Exists):
        cashwise.Window("#32770", "Save File", 1).Window("Button", "&Save", 2).Click()
 # if(cashwise.WatWindow("#32770", "Confirm Save As", 1).Exists):
      #  cashwise.Window("#32770", "Confirm Save As", 1).UIAObject("Confirm_Save_As").Window("CtrlNotifySink", "", 7).Window("Button", "&Yes", 1)
  
  cashwise.RavePreviewForm.TImage.Keys("~fx")
  reportSubString = "Optional      Payment      Authorization"
  testResults = CheckForFile(cusID,cashFilePath,reportSubString,testResults)
  
  return testResults
  

def CheckForFile(cusID,cashFilePath, reportSubString,testResults):
    reportPopulated = False
    reportTitle = False
   
    if(aqFileSystem.Exists(cashFilePath)):
     
      cashFile = aqFile.OpenTextFile(cashFilePath,aqFile.faRead,aqFile.ctANSI)  #aqFile.ctUnicode
     
      while not cashFile.IsEndOfFile():
          s = cashFile.ReadLine()
          res = aqString.Find(s,reportSubString,0,True)
          if(res != -1):
            Log.Message("found report Title "+reportSubString)
            reportTitle = True
            testResults["Results1"] = "ReportTitlePassed"
    cashFile.Close()    
    if(reportTitle == False):
           testResults["Results1"] = "ReportTitleFailed"
    if testResults["Results3"] == "Name3 Results":
      lookFor = GetCustomerAccountFromBank(cusID)
      Log.Message("Looking for bank account number " + lookFor)
    else:
      lookFor = GetNextACHPaymentCus_R(cusID)
      lookFor = FixDayInDate(lookFor)
      Log.Message("Looking for Beginning Date "+ lookFor)
      
    
    if(reportTitle == True):
      cashFile = aqFile.OpenTextFile(cashFilePath,aqFile.faRead,aqFile.ctANSI)  #aqFile.ctUnicode
      while not cashFile.IsEndOfFile():
          s = cashFile.ReadLine()
          res = aqString.Find(s,lookFor,0,False)
          if(res != -1):
            Log.Message("found account Number Report is populated "+reportSubString)
            reportPopulated = True
            testResults["Results2"] = "ReportPopulatedPassed"
      cashFile.Close() 
    
    if(reportPopulated == False):  # TRY AGAIN
      cashFile = aqFile.OpenTextFile(cashFilePath,aqFile.faRead,aqFile.ctANSI)  #aqFile.ctUnicode
      while not cashFile.IsEndOfFile():
          s = cashFile.ReadLine()
          aqUtils.Delay(100)
          res = aqString.Find(s,lookFor,0,False)
          
          if(res != -1):
            Log.Message("found account Number Report is populated "+reportSubString)
            reportPopulated = True
            testResults["Results2"] = "ReportPopulatedPassed"
      cashFile.Close() 
    
    if(reportTitle == False):
             testResults["Results2"] = "ReportPopulatedFailed"
    
    return testResults
    
def GetNextACHPaymentCus_R(cusID):
  #cusID  = "001-0000494"
  aQuery = "Select top 1 convert(varchar,PaymentDate,101) PaymentDate from LOCPaymentPlanSchedule " +\
  "where  Account_ID = (select MAX(Account_ID) from LOCPaymentPlanSchedule where Contact_ID  = '"+cusID+"') "+\
  "and PaymentDate >= (select System_Date from Location where ID = '001' )  "+\
  "order by ID"
  columnName = "PaymentDate"
  nextACHDate = RunSingleQuery(aQuery,columnName)
  return nextACHDate 
  
def FixDayInDate(thedate):
  #thedate = "09/08/2010"
  aqString.ListSeparator = "/"
  NumOfWord = aqString.GetListLength(thedate)
  ExplicitWait(1)
   
  month  =  aqString.GetListItem(thedate, 0)
  day  =  aqString.GetListItem(thedate, 1)
  year  =  aqString.GetListItem(thedate, 2)
  monthOfYear = aqConvert.StrToInt(month)
  dayOfMonth = aqConvert.StrToInt(day)
  month = aqConvert.IntToStr(monthOfYear)
  day = aqConvert.IntToStr(dayOfMonth)
  newDate = month+"/"+day+"/"+year
 
  return newDate
   