﻿#-UserStory 7617
#-As a CSR I need the ability to print a new ACH authorization form when the ACH amount or ACH dates change so that I can have the customer sign the new document. 
#-CashwiseAuto>Smoke>LOC>Functionally>TestAutoPayChangeAmountReport 
TestedApps.Cashwise.Run()
from EmploymentHelper import *
from Customers import *
from LineOfCredit import *

def TestAutoPayChangeAmountNoReport():
  #Author Phil Ivey 9/03/2019
  #Last Modified by Pat Holman 10/07/2020
  Indicator.PushText("RUNNING: TestAutoPayChangeAmountNoReport()")
  config = TestConfiguration()
  cashwise = StartCashwise(config["Database"], config["LoginPassword"],config["StoreLocation"])    
  cusID = GetCustomerID_WithLocDataAndAutoPay()
  
  
  if(cusID == "Empty"):
    customer = GetTempContact()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLOC(customer)
    AddBankAccount(customer)
    LineOfCreditRandomWithAutoPay(customer)  
    cusID = customer["cusID"] 

 # LineOfCreditRandomWithAutoPayWithCusID(cusID)
  EditEmploymentPayAmount(cusID,"Increase")
  #testPassed = CheckForLOCAgreementAndPlan()
  ExplicitWait(1)
  if(cashwise.WaitWindow("TfrmContactEmployerList","Contact Employers",1,1000).Exists):
      Sys.Process("Cashwise").VCLObject("frmContactEmployerList").VCLObject("btnSave").Click()
           
  cashwise.frmCustomerEdit.btnSave.Click()
  cashwise.frmCustomerBrowse.btnSave.Click()
         
  Log.Event("PASSED TEST: TestAutoPayChangeAmountReport() Report Printed")
  Indicator.PopText()
  CloseCashwiseApp()
  
  

  
  
  
  
  
  
  
  
  

