﻿TestedApps.Cashwise.Run()
from LineOfCredit import *
from Bank import AddBankAccount
from Customers import *
                                           
def TestLOC_AutoPay_Edit():
  #Author Phil Ivey 12/02/2019
  #Last Modified by Phil Ivey 12/02/2019
  Indicator.PushText("RUNNING: TestLOC_AutoPay_Edit")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])   
  
  cusID = GetCustomerID_With_NOLOC_NoOpenLOC_AndLocData()
  
  if cusID == "Empty":
      Log.Message("No existing customers creating temp customer")
     
      customer = AddLocCustomer()
      
      AddBankAccount(customer)
  else:
      customer = LoadCustomerObject2(cusID)
  
  Log.Message("Customer name = "+customer["FullName"])

  CheckCustomerValuesLog(customer)
  autoPayStatus = LineOfCreditAutoPayEdit(customer)  # returns ACTIVE AND INACTIVE
  CloseCashwiseApp()
  if(autoPayStatus == "ACTIVE"):
    Log.Event("PASSED TEST: AutoPay_Edit() Edited LOC")
  else:
    Log.Error("AutoPay_Edit() FAILED")
  Indicator.PopText()