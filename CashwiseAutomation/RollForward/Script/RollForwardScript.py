﻿#TestedApps.Cashwise.Run()
from RollForwardHelper import*

def RollForward():
  #Author Brian Johnson 12/02/2020
  #Last Modified by Brian Johnson 12/08/2020
  Indicator.PushText("RUNNING: RollForward()")
  managerUser = GetManager001()
  StartCashwise(managerUser["database"], managerUser["Password"],managerUser["location"])
  CloseCashDrawer()
  StartRollForward()
  ContinueRollForward(activeLocations)
  CloseCashwiseApp()  
  Log.Event("RollForward Complete")
  Indicator.PopText()