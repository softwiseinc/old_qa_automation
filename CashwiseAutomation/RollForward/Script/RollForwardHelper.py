﻿from BasePage import *
from CashHelper import*

#activeLocations = ["003","LHI"] #Locations for testing this script
activeLocations = ["ALO","003","LHI","UTO","TXO","KSO","AKO","CAO","IDO","NVO","WYO","MOO","WIO","HIO","COL"]
x = 0



def StartRollForward():
  #Author Brian Johnson 12/02/2020
  #Last Modified by Brian Johnson 12/08/2020
  Indicator.PushText("RUNNING: StartRollForward")
  cashwise = Sys.Process("Cashwise")
  cashwise.VCLObject("frmMain").MainMenu.Click("File|Roll Forward...")
  cashwise.VCLObject("frmClosing").VCLObject("pnlProgress").VCLObject("btnRollForward").Click()
  if(cashwise.WaitWindow("TMessageForm","Confirm", 1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    ExplicitWait(4)
    cashwise.VCLObject("frmCalendarEdit").VCLObject("btnSave").Click()
    ExplicitWait(1)
    cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK").Click()
  #elif(cashwise.VCLObject("frmHistory")):
    #ExplicitWait(4)
  else:
    ExplicitWait(4)
    cashwise.VCLObject("frmCalendarEdit").VCLObject("btnSave").Click()
    ExplicitWait(1)
    cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK").Click()
    
def ChangeLocation(i):
  cashwise = Sys.Process("Cashwise")
  cashwiseApp = cashwise.frmMain
  cashwiseApp.Keys("~uc")
  selectLocationButton = cashwise.VCLObject("frmSelectLocation").VCLObject("pnlMain").VCLObject("sbData").VCLObject("edLocationID").VCLObject("TAdvancedSpeedButton")
  selectLocationButton.Click()
  LocationForm = cashwise.VCLObject("frmLocationList")
  LocationForm.Keys(activeLocations[i])
  ExplicitWait(1) 
  LocationForm.VCLObject("btnSave").Click()
  cashwise.VCLObject("frmSelectLocation").VCLObject("btnSave").Click()
  
def ContinueRollForward(store):
  for x in range(len(activeLocations)):
    ChangeLocation(x)
    StartRollForward()