﻿# this is Helpers from Customers>CustomerHelper>from Helpers import *

from DatabaseQuery import *
import random
import string
import time
from random import randrange
from Bank import *


#CashwiseBrowser = ProjectSuite.Variables.cashwiseBrowser = Sys.Process("Cashwise").WinFormsObject("frmBrowser").WinFormsObject("pnlBrowser").WinFormsObject("ChromiumWebBrowser", "").Window("CefBrowserWindow", "", 1).Window("Chrome_WidgetWin_0", "", 1).Page("https://qa-loc-1.softwise.com/*")
#cashwiseBrowser = ProjectSuite.Variables.AddVariable()
def GenerateRandomString(stringLength):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019
  letters = string.ascii_lowercase
  return ''.join(random.choice(letters) for i in range(stringLength))
  Log.Checkpoint("GenerateRandomUser is successful")  
  

def GenerateRandomNumber(stringLength):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019  
  letters = string.ascii_lowercase
  return ''.join(random.choice(letters) for i in range(stringLength))
  Log.Checkpoint("GenerateRandomUser is successful")  

def GenerateRandomNumberString(stringLength):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019  
  letters = "0987654321"
  return ''.join(random.choice(letters) for i in range(stringLength))

def GenerateRandomPhoneNumber():
    first = str(random.randint(100,999))
    second = str(random.randint(1,888)).zfill(3)

    last = (str(random.randint(1,9998)).zfill(4))
    while last in ['1111','2222','3333','4444','5555','6666','7777','8888']:
        last = (str(random.randint(1,9998)).zfill(4))

    return '{}-{}-{}'.format(first,second, last)

def GenerateRandomSSN():
    first = str(random.randint(528,529))
    second = str(random.randint(11,88)).zfill(2)

    last = (str(random.randint(1,9998)).zfill(4))
    while last in ['1111','2222','3333','4444','5555','6666','7777','8888']:
        last = (str(random.randint(1,9998)).zfill(4))

    randomSSN = '{}-{}-{}'.format(first,second, last)
   
    aQuery = "if not exists (select SSN from Contact where SSN = '"+randomSSN+"') select 'NotInDatabase' Status else select 'InDatabase' Status"
    columnName = "Status" 
    results = RunSingleQuery(aQuery,columnName)
   
    while(results == "InDatabase"):
       first = str(random.randint(528,529))
       second = str(random.randint(11,88)).zfill(2)
       last = (str(random.randint(1,9998)).zfill(4))
       while last in ['1111','2222','3333','4444','5555','6666','7777','8888']: 
        last = (str(random.randint(1,9998)).zfill(4))
       randomSSN = '{}-{}-{}'.format(first,second, last)
       aQuery = "if not exists (select SSN from Contact where SSN = '"+randomSSN+"') select 'NotInDatabase' Status else select 'InDatabase' Status"
       results = RunSingleQuery(aQuery,columnName)
    
    randomSSN = aqString.Replace(randomSSN,"-","",False)
    return randomSSN

def DoesSSNExistInDatabase(thisSSN):  #Exists and NotExists
  #thisSSN = "529-09-0009"
  aQuery = "If exists(Select * from Contact where IsCustomer = 1 and SSN is not NULL and SSN = '"+thisSSN+"') " + \
            "select 'Exists' ExistsAnwser " + \
            "else " + \
	          "select 'NotExists' ExistsAnwser"
  columnName = "ExistsAnwser"
  ssnExist = RunSingleQuery(aQuery,columnName)
  return ssnExist
	          
	          
	          
def ExplicitWait(seconds):
  aqUtils.Delay(seconds * 1000)  
  Log.Checkpoint("ExplicitWait is successful")
     
def GetRandomName():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  aQuery = "SELECT TOP 1 LastName FROM Contact where LastName is Not NULL ORDER BY NEWID()"
  columnName = "LastName" 
  #Project.Variables.AddVariable("lastName","string")
  lastName = RunSingleQuery(aQuery,columnName)
  Log.Message("Last Name = "+lastName)
  aQuery2 = "SELECT TOP 1 FirstName FROM Contact where FirstName is Not NULL ORDER BY NEWID()"
  columnName2 = "FirstName" 
  #Project.Variables.AddVariable("firstName","string")
  firstName = RunSingleQuery(aQuery2,columnName2)
  Log.Message("First Name = "+firstName)
  returnName = lastName+", "+firstName
  #returnName = aqConvert.VarToStr(returnName)
  Log.Message(str(returnName))
  #returnName = Project.Variables.AddVariable()
  return returnName
  
def GetRandomFirstName():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
    aQuery2 = "SELECT TOP 1 FirstName FROM Contact where FirstName is Not NULL ORDER BY NEWID()"
    columnName2 = "FirstName" 
    #Project.Variables.AddVariable("firstName","string")
    
    firstName = RunSingleQuery(aQuery2,columnName2)
    Log.Message("First Name = "+firstName)
    return firstName

def GetRandomLastName():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  aQuery = "SELECT TOP 1 c.LastName FROM Contact c where c.LastName is Not NULL and c.LastName not like '%[0-9]%' ORDER BY NEWID()"
  Log.Message(aQuery)
  columnName = "LastName" 
  #Project.Variables.AddVariable("lastName","string")
  lastName = RunSingleQuery(aQuery,columnName)
  Log.Message("Last Name = "+lastName)
  return lastName
  
def GetLocationDate(locID):
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  #locID = "001"
  aQuery = "select System_Date from Location where ID = '"+locID+"'"
  columnName = "System_Date"
  locDate = RunSingleQuery(aQuery,columnName)
  locDate = aqConvert.DateTimeToStr(locDate)
  Log.Message("Date for "+locID+" is "+locDate)
  return locDate
  
def IntRandomRange(min,max):
  ranNum = randrange(min,max)
  Log.Message(ranNum)
  return ranNum 

def IntRandomRangeMoney(min,max):
  ranNum = randrange(min,max)
#  ranNum = aqConvert.
  Log.Message(ranNum)
  return ranNum 

  
def GetRandomBirthDate():
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  locDate = GetLocationDate("001")
  days = IntRandomRange(6580,18720)   #-27692
  days = (days * -1)
  Log.Message("days = "+str(days))
  newDate = aqDateTime.AddDays(locDate,days)
  Log.Message("newDate = "+str(newDate))
  snewDate = aqConvert.DateTimeToFormatStr(newDate,"%m/%d/%Y")
  Log.Message("this b-date is "+snewDate)
  return snewDate
  
def GetRandomRouteNum():
     #Author Phil 10/29/2019
     #Last Modified by Phil 10/29/2019 
      
    aQuery = "select top 1 RoutingID from Bank where Name is not NULL order by NEWID()"
    columnName = "RoutingID"
    routeID = RunSingleQuery(aQuery,columnName)
    return routeID

def GetRandomAcctNum():
      #Author Phil 10/29/2019
      #Last Modified by Phil 10/29/2019 
       
     aQuery = "select convert(numeric(12,0),rand() * 899999999999) + 100000000000 AcctNum"
     columnName = "AcctNum"
     acctNum = RunSingleQuery(aQuery,columnName)
     
     return aqConvert.FloatToStr(acctNum)
 
  
def GetRandomZip():
   #Author Phil 7/3/2019
   #Last Modified by Phil 8/16/2019 
    
  aQuery = "Select Top 1 Zip From ContactAddress ca where ca.Zip is Not NULL and AddressLine1 like '%[a-z]%' " + \
  "AND ca.Zip Not Like '%[a-z]%' AND " + \
  "(charindex(' ', ca.Zip) = len(ca.Zip) OR charindex(' ', ca.Zip) = 0) order by NEWID()"
  columnName = "Zip"
  zip = RunSingleQuery(aQuery,columnName)
  return zip
  
def GetCityFromZip(zip):
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  columnName = "City"
  aQuery = "Select City from ContactAddress where Zip = '"+zip+"'"
  Log.Message(aQuery)
  city = RunSingleQuery(aQuery,columnName)
  return city
  
def GetStateFromZip(zip):
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019 
  columnName = "State"
  aQuery = "Select State from Zipcode where Zipcode = '"+zip+"'"
  Log.Message(aQuery)
  state = RunSingleQuery(aQuery,columnName)
  return state
  
def GetAddressFromCity(city):
   #Author Phil 7/3/2019
   #Last Modified by Phil 7/3/2019  
  #city = "Salt Lake City"
  aQuery = "select Top 1 AddressLine1 from ContactAddress where City = '"+city+"' and AddressLine1 is not NULL and AddressLine1 like '%[a-z]%' and Contact_ID not in " +\
  "(SELECT Contact_ID FROM ContactAddress ca WHERE City = '"+city+"' and (charindex(' ', AddressLine1) = len(addressLine1) " +\
  " OR charindex(' ', AddressLine1) = 0)) ORDER BY NEWID()"
  Log.Message(aQuery)
  columnName = "AddressLine1"
  address = RunSingleQuery(aQuery,columnName)
  return address

def AddCusBankAccountBack(cusID,locID):
  #Author Phil 7/3/2019
  #Last Modified by Phil 7/3/2019  
  ExplicitWait(1)
  
  try:
    routingAcctNum = AddBankAccountToCustomer(cusID,locID)
    if(routingAcctNum != None and routingAcctNum != ""):
      Log.Checkpoint("SUCCESS: AddBankAccountToCustomer() is successful.")
  except Exception as e:
    Log.Error(str(e))
    Log.Error("ERROR: AddBankAccountToCustomer() is unsuccessful.")
    pass
  finally:
    Indicator.PopText() 
  return routingAcctNum 
  
def GetCustomerIDFromName(cusName):
  #Author Phil Ivey 8/4/2019
  #Last Modified by Phil Ivey 8/4/2019   
  aQuery = "select ID from contact where Name like '" +cusName+"%'";
  columnName = "ID"
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message("cus ID is "+cusID)
  return cusID
  
def GetCustomerIDFromLOCAccount(accountID):
    #Author Phil Ivey 8/4/2019
    #Last Modified by Phil Ivey 8/4/2019   
    #accountID = "001-0000185"
    aQuery = "select Customer_ID from LOCAccount where ID = '" +accountID+"' and Status_ID <> 'CLO'";
    columnName = "Customer_ID"
    cusID = RunSingleQuery(aQuery,columnName)
    Log.Message("cus ID is "+cusID)
    return cusID

def RunLOCPercentOfCreditLimit(cusID):
    #Author Phil Ivey 01/02/2020
    #Last Modified by Phil Ivey 01/02/2020  
    #cusID = "001-0000924"
    accountID = GetLocAccountNumber(cusID)
    aQuery1 = "Select MAX(Number) LastProposalNum from LOCProposal where Account_ID = '" +accountID+ "'";
    columnName = "LastProposalNum"
    lastProposalNum = RunSingleQuery(aQuery1,columnName)
    lastProposalNum = aqConvert.StrToInt(lastProposalNum)
    lastProposalNum = lastProposalNum + 1
    nextProposalNum = aqConvert.IntToStr(lastProposalNum)
    Log.Message("Next Proposal Num is "+nextProposalNum)
    ExecLOCPaymentPlanSchedule(cusID,nextProposalNum)
    
    
    return accountID
    
        
def GetCustomerAccountFromBank(cusID):
    #Author Phil Ivey 8/4/2019
    #Last Modified by Phil Ivey 8/4/2019   
    #cusID = "001-0000999"
    aQuery = "select AccountNumber from ContactAccount where Contact_ID = '" +cusID+ "'";
    columnName = "AccountNumber"
    accountNum = RunSingleQuery(aQuery,columnName)
    Log.Message("Account Number is "+accountNum)
    return accountNum
    
def GetNumberOfCustomerBankAccounts(cusID):
    #Author Phil Ivey 8/4/2019
    #Last Modified by Phil Ivey 12/4/2019   
    #cusID = "001-0300033"
    aQuery = "select count(*) NumOfBankAccts from ContactAccount where Contact_ID = '" +cusID+"'";
    columnName = "NumOfBankAccts"
    numBankAccounts = RunSingleQuery(aQuery,columnName)
    Log.Message("NumBankAccounts is "+aqConvert.IntToStr(numBankAccounts))
  
    return numBankAccounts

  
  
def GetCustomerID_With_NOLOC_NoOpenLOC_AndLocData():  # IF NO RESULTS RETURN = "Empty" 
    #Author Phil Ivey 12/02/2019
    #Last Modified by Phil Ivey 12/02/2019
    aQuery = "select top 1 ID from( select c.ID from  Contact c where c.IsCustomer = 1 " +\
    "and exists (select Customer_ID from CustomerLimitByLoanTypeCategory cc WHERE cc.Customer_ID = c.Id and cc.Category_ID = 'LOC') " +\
    "and exists (select 1 from Contact c1 where c1.ID = c.ID and c1.Birthday is Not NULL) " +\
    "and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID and ce.GrossPay is Not NULL and NextPayDate is not NULL) " +\
    "and exists (select Contact_ID, Count(*) numOfAcct from ContactAccount ca where ca.Contact_ID = c.ID group by Contact_ID having Count(*) = 1) " +\
    "and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID) " +\
    "and not exists (select Status_ID from LOCAccount where Status_ID in ('OPE','PAS','P30','P90','DEF') and Customer_ID = c.ID) " +\
    "and not exists (select 1 from LOCPaymentPlan where AutoPayType = 'ACH' and Account_ID = (select ID from LOCAccount where Customer_ID = c.ID " +\
    "and Status_ID = 'OPE')) " +\
    "union " +\
    "select c.ID from Contact c left join LOCAccount la on c.ID = la.Customer_ID  where la.ID is NULL " +\
    "and exists (select Customer_ID from CustomerLimitByLoanTypeCategory cc WHERE cc.Customer_ID = c.Id and cc.Category_ID = 'LOC') " +\
    "and not exists (select AutoPayType from LOCPaymentPlan where AutoPayType = 'ACH' and Account_ID = " +\
    "(select ID from LOCAccount where Customer_ID = c.ID and Status_ID = 'OPE')) " +\
    "and exists (select 1 from Contact c1 where c1.ID = c.ID and c1.Birthday is Not NULL) "+\
    "and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID and ce.GrossPay is Not NULL and NextPayDate is not NULL) "+\
    "and exists (select Contact_ID, Count(*) numOfAcct from ContactAccount ca where ca.Contact_ID = c.ID group by Contact_ID having Count(*) = 1) "+\
    "and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID) " +\
    ") b order by NEWID()";
  
  
    columnName = "ID"
    Log.Message(aQuery)
    cusID = RunSingleQuery(aQuery,columnName)
    Log.Message("LOC customer ID is "+cusID)
    return cusID

    
def GetOpenLOCWithACHPayments():
   #Author Phil Ivey 12/04/2019
  #Last Modified by Phil Ivey 12/04/2019
  aQuery = " Select  distinct Top 1 la.ID from LOCAccount la join LOCProposal lp on la.ID = lp.Account_ID  " +\
  "join LOCPaymentPlan lpp on lp.Account_ID = lpp.Account_ID where lpp.AutoPayType = 'ACH'  " +\
  "and la.Status_ID not in ('CLO','DEF','DEL','DIS','VOI') and la.Location_ID = '001' and lp.Type_ID = 'ACH' " +\
  " and dbo.LOCPaymentWasReturned(lp.Account_ID,lp.Number) = 0";
  columnName = "ID"
  Log.Message(aQuery)
  accountID = RunSingleQuery(aQuery,columnName)
  Log.Message("LOC account ID is "+accountID)
  return accountID
    
def GetCustomerID_WithLocData():  # IF NO RESULTS RETURN = "Empty" 
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019  
    # Ca = AppIntergration UtahTest
  #Cb = AppIntergration for Utah3
  #Cc = 
  aQuery = "select Top 1 ID from  Contact c where c.IsCustomer = 1 " +\
  " and  exists (select Customer_ID from CustomerLimitByLoanTypeCategory cc WHERE cc.Customer_ID = c.Id and cc.Category_ID = 'LOC' and DeferredLimit > 1160) " +\
  " and exists (select 1 from Contact c1 where c1.ID = c.ID and c1.Birthday is Not NULL)" +\
  " and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID and ce.GrossPay is Not NULL and NextPayDate is not NULL)" +\
  " and exists (select Contact_ID, Count(*) numOfAcct from ContactAccount ca where ca.Contact_ID = c.ID group by Contact_ID having Count(*) = 1) " +\
  " and not exists (select 1 from LOCAccount la where la.Customer_ID = c.ID and Status_ID <> 'CLO')"
  columnName = "ID"
  Log.Message(aQuery)
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message("LOC customer ID is "+cusID)
  return cusID
  
def GetCustomerID_WithLocDataAndAutoPay():  # IF NO RESULTS RETURN = "Empty" 
  #Author Phil Ivey 9/03/2019
  #Last Modified by Phil Ivey 9/03/2019  
   aQuery = "select Top 1 c.ID from  Contact c join LOCAccount la on c.ID = la.Customer_ID and c.IsCustomer = 1 and la.Status_ID not in ('CLO','DEF') "+\
   "join LOCPaymentPlan lpp on la.ID =lpp.Account_ID and lpp.AutoPayType = 'ACH' "+\
	 "and  exists (select Customer_ID from CustomerLimitByLoanTypeCategory cc WHERE cc.Customer_ID = c.Id and cc.Category_ID = 'LOC')  " +\
	 "and exists (select 1 from Contact c1 where c1.ID = c.ID and c1.Birthday is Not NULL) " +\
	 "and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID and ce.GrossPay is Not NULL and NextPayDate is not NULL) " +\
	 "and exists (select Contact_ID, Count(*) numOfAcct from ContactAccount ca where ca.Contact_ID = c.ID group by Contact_ID having Count(*) = 1)"
	
   columnName = "ID"
   Log.Message(aQuery)
   cusID = RunSingleQuery(aQuery,columnName)
   Log.Message("LOC customer ID is "+cusID)
   
   return cusID

  
def IsRaveUp():
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(2)
  if(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,3000).Exists):
    Log.Message("Rave Preview is up")
    isup = True
  else:
    Log.Message("Can't Find Rave")
    isup = False
    
  return isup
    
  
  
  
def GetCustomerID_WithNoSMSData():  # IF NO RESULTS RETURN = "Empty" 
  #Author Phil Ivey 8/19/2019
  #Last Modified by Phil Ivey 8/19/2019  
  aQuery = "select Top 1 ID from  Contact c where c.IsCustomer = 1 " +\
  " and exists (select 1 from Contact c1 where c1.ID = c.ID and c1.Birthday is Not NULL)" +\
  " and exists (select 1 from ContactEmployer ce where ce.Contact_ID = c.ID and ce.GrossPay is Not NULL and NextPayDate is not NULL and IntervalType_ID <> 'S')" +\
  "and not exists (select 1 from SMSContactSubscriber smsub where smsub.Contact_ID = c.ID) and c.Name not like '%[0-9]%' "
  columnName = "ID"
  Log.Message(aQuery)
  cusID = RunSingleQuery(aQuery,columnName)
  Log.Message("LOC customer ID is "+cusID)
  return cusID

  
  
    
def GetRamdomAddressInCity(city):
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019  
    #city = "Salt Lake City"
    aQuery = "select Top 1 ca.AddressLine1 from ContactAddress ca where City = '"+city+"' and ca.AddressLine1 is not NULL " +\
    " and Contact_ID not in ( SELECT Contact_ID FROM ContactAddress ca WHERE City = '"+city+"' and (charindex(' ', AddressLine1) " +\
    " = len(addressLine1) OR charindex(' ', AddressLine1) = 0)) and ca.AddressLine1 like '%[a-z]%' ORDER BY NEWID()"  
    columnName = "AddressLine1"
    address = RunSingleQuery(aQuery,columnName)
    Log.Message("address in "+city+" is "+address)
    return address
    
def GetRandomCustomerEmplorerName():
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019  
    aQuery = "select Top 1 c.Name from ContactEmployer ce join Contact c on ce.Employer_ID = c.ID where c.IsEmployer = 1  ORDER BY NEWID()"
    columnName = "Name"
    emplorerName = RunSingleQuery(aQuery,columnName)
    Log.Message("EmployerName is "+emplorerName)
    return emplorerName

def GetCustomerBankAccount(cusID):
    #Author Phil Ivey 8/4/2019
    #Last Modified by Phil Ivey 12/4/2019   
    #cusID = "001-0300033"
    aQuery = "select RoutingAccountNum from ContactAccount where Contact_ID = '" +cusID+"'";
    columnName = "RoutingAccountNum"
    routingAccountNum = RunSingleQuery(aQuery,columnName)
    
    return routingAccountNum    								  
    
def GetCashwiseLocationYear(locID):
  #Author Phil Ivey 12/05/2019
  #Last Modified by Phil Ivey 12/05/2019  
  #  locID = "001"
    aQuery = " select Year(system_date) LocYear from Location where ID = '"+locID+"'"
    columnName = "LocYear"
    locYear = RunSingleQuery(aQuery,columnName)
    slocYear = aqConvert.IntToStr(locYear)
    return slocYear
    
    
def GetRandomDepartment():
  #Author Phil Ivey 8/1/2019
  #Last Modified by Phil Ivey 8/1/2019  
    aQuery = "select Top 1 Department from QATestData..JobDptPostion where Department like '%[a-z]' order by NEWID()"    
    columnName = "Department"
    department = RunSingleQuery(aQuery,columnName)
    return department
    
def GetRandomPostiion(department):
  #Author Phil Ivey 8/1/2019
  #Last Modified by Phil Ivey 8/1/2019  
    #department = "COLLECTIONS" 
    columnName = "Position"
    aQuery = "select Top 1 Position from QATestData..JobDptPostion where Department = '"+department+"' order by NEWID()"
    postion = RunSingleQuery(aQuery,columnName)
    return postion
    
    
def GetRadomDayOfWeek():
  #Author Phil Ivey 8/1/2019
  #Last Modified by Phil Ivey 8/1/2019  
    dayOfWeek = randrange(1,5)  
    Log.Message(dayOfWeek)
    if dayOfWeek == 1:
      payday = "Monday"
    elif dayOfWeek == 2:
      payday = "Tuesday"
    elif dayOfWeek == 3:
      payday = "Wednesday"
    elif dayOfWeek == 4:
      payday = "Thursday"
    elif dayOfWeek == 5:
      payday = "Friday"
      
    return payday

def CashwisaMainMenu_File(action):
  #Author Phil Ivey 8/6/2019
  #Last Modified by Phil Ivey 8/6/2019  
    cashwise = Sys.Process("Cashwise")
	    
    if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
      Sys.Process("Cashwise").Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
    cashwiseMain =  cashwise.frmMain.MainMenu
    
       
    if(action == "Setup"):
      cashwise.frmMain.Keys("~Fs")
      #cashwiseMain.Click("File|Setup...");
      
    elif(action == "Roll Forward"):
        cashwiseMain.Click("File|Roll Forward...");
         
    elif(action == "About"):
        cashwiseMain.Click("File|About...");
     
    elif(action == "Help"):
      cashwiseMain.Click("File|Help...");
      
    elif(action == "Exit"):
        cashwiseMain.Click("File|Exit");
      

def SystemSetupTree(action):
  #Author Phil Ivey 8/6/2019
  #Last Modified by Phil Ivey 8/6/2019  
   
    cashwise = Sys.Process("Cashwise") 
    tfrmSysSetup = cashwise.frmSystemSetup
    treeView = tfrmSysSetup.pnlMain.sbData.tvMain
    isFocused = treeView.Focused
    if(isFocused == False):
      treeView.SetFocus()
    treeView.ClickItem("|Enterprise")
    
    if(action == "Save & Close"):
        tfrmSysSetup.Keys("~s")
      
    elif(action == "Close"):
        tfrmSysSetup.Keys("~c")
      
    elif(action == "Maximize"):
        tfrmSysSetup.Maximize()

    elif(action == "Enterprise"):
        treeView.SelectItem("|Enterprise")
      
    elif(action == "Locations"):
          treeView.SelectItem("|Enterprise|Locations")
      
    elif(action == "Registration"):
          treeView.SelectItem("|Enterprise|Registration")
      
    elif(action == "Appearance/Behavior"):
       treeView.SelectItem("|Appearance/Behavior")
      
    elif(action == "Table Setup"):
          treeView.SelectItem("|Appearance/Behavior|Table Setup")
           
    elif(action == "Location"):
        treeView.SelectItem("|Location")
      
    elif(action == "Localization"):
          treeView.SelectItem("|Location|Localization")

    elif(action == "Check 21"):
          treeView.SelectItem("|Location|Check 21")
        
    elif(action == "Cashwise Services"):
          treeView.SelectItem("|Location|Cashwise Services")

    elif(action == "Cash Management"):
        treeView.SelectItem("|Cash Management")
      
    elif(action == "Contact"):
        treeView.SelectItem("|Contact")
      
    elif(action == "Bank"):
        treeView.SelectItem("|Bank")
      
    elif(action == "Inventory"):
        treeView.SelectItem("|Inventory")
      
    elif(action == "Sales"):
        treeView.SelectItem("|Sales")
      
    elif(action == "POS"):
          treeView.SelectItem("|Sales|POS")
      
    elif(action == "Check"):
          treeView.SelectItem("|Sales|Check")

    elif(action == "Credit Card"):
          treeView.SelectItem("|Sales|Credit Card")

    elif(action == "Line of Credit"):
          treeView.SelectItem("|Sales|Line of Credit")
    
    elif(action == "Loan Types"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Loan Types")
              
    elif(action == "Fee Types"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Fee Types")
          
    elif(action == "Location Settings"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Location Settings")
          
    elif(action == "Loan Type Selection"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Loan Type Selection")
    
    elif(action == "Commercial Check Setup"):
          treeView.SelectItem("|Sales|Commercial Check Setup")
    
    elif(action == "Gold"):
          treeView.SelectItem("|Sales|Gold")
          
    elif(action == "Modules"):
          treeView.SelectItem("|Sales|Modules")
            
    elif(action == "Money Order"):
          treeView.SelectItem("|Sales|Money Order")
    
    elif(action == "Wire Transfer"):
          treeView.SelectItem("|Sales|Wire Transfer")
        
    elif(action == "NetSpend"):
          Sleep(5000)
          treeView.ClickItem("|Sales|Payouts|Stored Value Card|Netspend")
        
    elif(action == "Wise Card"):
          treeView.SelectItem("|Sales|Payouts|Wise Card")
        
    elif(action == "Wise Card Account Transaction"):
          treeView.SelectItem("|Sales|Payouts|Wise Card|Wise Card Account Transaction")

    elif(action == "Bill Pay"):
          treeView.SelectItem("|Sales|Bill Pay")
        
    elif(action == "Softgate Billpay (IPP)"):
          treeView.SelectItem("|Sales|Bill Pay|Softgate Billpay (IPP)")

    elif(action == "Check Free Pay (CFP)"):
          treeView.SelectItem("|Sales|Bill Pay|Check Free Pay (CFP)")
        
    elif(action == "Cashiers Check"):
          treeView.SelectItem("|Sales|Cashiers Check")
      
    elif(action == "ACH"):
        treeView.SelectItem("|ACH")
      
    elif(action == "Third Party Setup"):
        treeView.SelectItem("|Third Party Setup*")
      
    elif(action == "Tele-Track"):
        treeView.SelectItem("|Tele-Track")
      
    elif(action == "Peripherals"):
        treeView.SelectItem("|Peripherals")
      
    elif(action == "Capture"):
          treeView.SelectItem("|Peripherals|Capture")
      
    elif(action == "Device"):
            treeView.SelectItem("|Peripherals|Capture|Device")
          
    elif(action == "Points"):
        treeView.ClickItem("|Peripherals|Capture|Points")
        
    elif(action == "Labor"):
        treeView.SelectItem("|Labor")
      
    elif(action == "General Ledger Export Setup"):
        treeView.SelectItem("|General Ledger Export Setup")
      
    elif(action == "Collections"):
        treeView.SelectItem("|Collections")

    elif(action == "Configuration"):
          treeView.SelectItem("|Collections|Configuration")
        
    elif(action == "LocationCollectionSetting"):
          treeView.SelectItem("|Collections|LocationCollectionSetting")

    elif(action == "Command"):
        treeView.SelectItem("|Command")
      
    elif(action == "External App Setup"):
        treeView.SelectItem("|External App Setup")
      
    elif(action == "SMS"):
        treeView.SelectItem("|SMS")
      
    
    ExplicitWait(3)