﻿# this is basePage from Customers>CustomerHelper>from BasePage import *

from Helpers import *
from TestUsers import *

   #hey 
def StartCashwise(database, password, location):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019 
  cashwise = Sys.Process("Cashwise")
  CloseStartupError()
  Indicator.PushText("In StartCashwise(database, password, location)")
  ExplicitWait(2)
  SelectDatabase(database)
  EnterUserPassword(password) 
  EnterStoreLocation(location)
  ExplicitWait(2)
  CloseSystemDateWarning()  

  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("No").Click()
    
  Log.Checkpoint("SUCCESS: Cashwise startup is Successful")
  #CashwiseMainMenu_File()
  Indicator.PopText()

def CloseCashwiseApp():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseCashwiseApp()")
  cashwise.VCLObject("frmMain").MainMenu.Click("File|Exit")
  ExplicitWait(1)
  Indicator.PopText()
    
  
def CloseStartupError():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/5/2019 
  ExplicitWait(2)  
  cashwise = Sys.Process("Cashwise")
  Sys.WaitProcess("Cashwise",3000)
#  Sys.WaitProcess(cashwise,3000)
  activeWindow = Sys.Desktop.ActiveWindow()
  text = activeWindow.WndCaption
  if activeWindow.WndCaption == "Error":
    Sys.Process("Cashwise").Window("TMessageForm", "Error").VCLObject("OK").ClickButton()
    Log.Checkpoint("SUCCESS: CloseStartupError is successful.") 

def SelectDatabase(database):
  #Author Pat Holman 4/24/2019
  #Last Modified by Phil 10/03/2019
  #database = "QA01"
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(2)
  Indicator.PushText("In SelectDatabase(database)")
  ExplicitWait(2)
  #--- 9/25/20019 now there is no ObjectIdentifier  -----------------
  listBox = cashwise.Find("ObjectIdentifier", "lbAliases",10)
  
  databaseList = cashwise.Window("TfrmAliasBrowse", "Database Aliases", 1)
  listBox = databaseList.Find("Name", "Window(\"TListBox\", \"\", 1)",10)
  #listBox = databaseList.pnlMain.sbData.lbAliases
  listBox = cashwise.frmAliasBrowse.pnlMain.sbData.lbAliases
  ExplicitWait(1)
  listBox.ClickItem(database)
  listBox.ClickItem(database)
  ExplicitWait(1)
  listBox.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectDatabase is successful, "+database+" opened")
  Indicator.PopText()
  CloseUpgradeDialog()

def EnterStoreLocation(location):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterStoreLocation(location)")
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys(location)
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys("[F9]")

def OpenStoreLocation(storeLocation):
  cashwise = Sys.Process("Cashwise")
  tfrmMain = cashwise.frmMain
  tfrmMain.MainMenu.Click("User|Change Location...")
  tfrmSelectLocation = cashwise.frmSelectLocation
  customDBEdit = tfrmSelectLocation.pnlMain.sbData.edLocationID.TCustomDBEdit
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys(storeLocation)
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys("[F9]")
  #tfrmSelectLocation.MainMenu.Click("[0]|[0]")

def EnterUserPassword(userPassword):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 18/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterUserPassword(userPassword)")
  Sys.Process("Cashwise").frmCPLogin.pnlMain.sbData.edPassword.SetText(userPassword)
  Sys.Process("Cashwise").frmCPLogin.pnlMain.sbData.edPassword.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EnterUserPassword is successful.")
  Indicator.PopText()
 
def EnterUserId(userId):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterUserId(userId)")
  userIdInputField = cashwise.frmCPLogin.pnlMain.sbData.edUserID
  loginControlPanel = cashwise.frmCPLogin
  if(aqObject.CheckProperty(loginControlPanel, "WndCaption", cmpEqual, "Login")):
        Log.Checkpoint("Login Control Panel is visible")
  userIdInputField.Keys(userId)
  userIdInputField.Keys("[Tab]")
  Log.Checkpoint("SUCCESS: EnterUserId is successful.")    
  Indicator.PopText()

def LoginUser(user):
  cashwise = Sys.Process("Cashwise")
  tfrmMain = cashwise.frmMain
  tfrmMain.MainMenu.Click("User|Login...")
  TDBEdit = cashwise.frmCPLogin.pnlMain.sbData.edPassword
  thePW = user["Password"]
  TDBEdit.SetText(thePW)
  TDBEdit.Keys("[F9]")
  ExplicitWait(1)
  

def CloseSystemDateWarning(): 
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 7/16/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseSystemDateWarning()")
  ExplicitWait(2)
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists == False):
      sleep(500)
  
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption == "Warning":
    Sys.Process("Cashwise").Window("TMessageForm", "Warning").VCLObject("OK").ClickButton()
  Log.Checkpoint("SUCCESS: CloseSystemDateWarning is successful.")  
  Indicator.PopText()

def CloseUpgradeDialog():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(1)
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption == "Login":
    return;
  Log.Error("Fix the upgrade error message - Pat")
  Indicator.PopText()

  
def GetCurrentDatabaseOnCashwiseBrowser():

  caption = Sys.Process("Cashwise").WinFormsObject("frmBrowser").WndCaption
  EndPos = aqString.GetLength(caption) 
  StartPos = 30 #start  postion
  ExplicitWait(1)
  Res = aqString.Remove(caption, StartPos, EndPos)
  aqString.ListSeparator = "-"
  NumOfWord = aqString.GetListLength(Res)
  database = ""
  for x in range(0,NumOfWord):
      if(x == 3):
        database = aqString.GetListItem(Res, x)
 
  database = aqConvert.VarToStr(database)
  database = aqString.Trim( database, aqString.stAll )
  Log.Message("database is "+database)
  return database