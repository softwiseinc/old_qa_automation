﻿from BasePage import *
from DatabaseQuery import *
from CashHelper import *


def OpenSystemSetupOption(optin):
  #Author Pat Holman 6/13/2019
  #Last Modified by Pat Holman 6/13/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenSystemSetup()")
  mainMenu = cashwise.frmMain.MainMenu
  setupTreeView = cashwise.frmSystemSetup.pnlMain.sbData.tvMain
  #mainMenu.Click("File|Setup...")
  cashwise.frmMain.Keys("~Fs")
  Log.Checkpoint("SUCCESS: OpenSystemSetup() is successful")
  Indicator.PopText()    
  if optin == "LoanTypes":
    setupTreeView.ClickItem("|Sales|Cash Advance Setup|Loan Types")
  else:
    pass;
    
def EditTypeILC():
  cashwise = Sys.Process("Cashwise")
  tableIndex = {}
  tableIndex["SqlStatement"] = "SELECT * FROM DeferredType"
  tableIndex["Coloumn"] = "ID"
  tableIndex["ColoumnValue"] = "ILC"
  index = GetTableIndex(tableIndex)
  browseGrid = cashwise.frmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.Panel1.BrowseGrid1
  browseGrid.HScroll.Pos = 0
  browseGrid.VScroll.Pos = 0
  for x in range (1, index):
    browseGrid.Keys("[Down]")
  cashwise.frmDeferredTypeSetup.btnEdit.Click(25, 12)
 

def EnableAllowClosedDueDay():
  cashwise = Sys.Process("Cashwise")
  tfrmSystemSetup = cashwise.frmSystemSetup
  pageControl = cashwise.frmDeferredSetup.pnlMain.sbData.pgSettings
  pageControl.ClickTab("&3 Duration")
  TDBCheckBox = pageControl.tsDuration.dbcbAllowClosedDueDay
  TDBCheckBox.ClickButton(cbChecked)
  TDBCheckBox.Keys("[F9]")
  tfrmSystemSetup.pnlMain.sbData.pnlRight.pnlSetup.Panel1.BrowseGrid1.Keys("[F9]")


def RollForward():
  number = 1
  cashwise = Sys.Process("Cashwise")
  for x in range (0, number):
    cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
    cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Confirm"):
      delay(1000)
      #do you want to start performing transaction
    if cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists:
        cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
       
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Daily Analysis Report"):
      delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
    cashwise.frmDynamicEdit.btnClose.Click(34, 9)    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Daily Closeout Report"):
      delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
    cashwise.frmDynamicEdit.btnClose.Click(43, 8)
    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Please confirm the location date*"):
      delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
    cashwise.frmCalendarEdit.btnSave.Click(51, 12)
    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Information"):
      delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
    cashwise.TMessageForm5.OK.ClickButton()

def RollForwardNoReports(number):   #
  #Author Pat Holman 6/13/2019
  #Last Modified by Phil Ivey 10/14/2019  
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(2)
  for x in range (0, number):
    cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
    cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
    delay(1000)
    if(cashwise.WaitWindow("TMessageForm", "Confirm", 1,1000).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    
    WaitOnCalendar()
    
    if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1000).Exists):
      cashwise.frmCalendarEdit.btnSave.Click()
    
    ExplicitWait(2)
    activeWindow = Sys.Desktop.ActiveWindow()
    activeWindow.Keys("[Enter]")
    delay(1000)
    if(cashwise.WaitWindow("TMessageForm", "Information", 1,800).Exists):
      cashwise.Find("ObjectIdentifier","OK",25).Click()
   
 
    

def RollForwardLOCNoReports():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
  cashwise = Sys.Process("Cashwise")
  CloseCashDrawer()
  cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
  cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
  delay(1000)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  WaitOnCalendar()
  cashwise.frmCalendarEdit.btnSave.Click()
  cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK").Click()
  
def RollForwardLOCNoReportsCalendarOpen():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
  cashwise = Sys.Process("Cashwise")
  CloseCashDrawer() 
  delay(1000)
  cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
  delay(1000)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  WaitOnCalendar()
  return 0
 

  
def WaitOnCalendar():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
    cashwise = Sys.Process("Cashwise")
    x = 1
    while cashwise.WaitWindow("TfrmCalendarEdit","Please confirm *",1,500).Exists == False:
      delay(1000) 
      Log.Message("Waiting on Calendar "+ aqConvert.IntToStr(x))
      x = x+1
      if(cashwise.WaitWindow("TMessageForm","Confirm",1,200).Exists):
        Sys.Process("Cashwise").Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
        break
      if(x == 120):
        Log.Error("Something went wrong, look for a cashwise message or error")
    Log.Message("Calendar exists")
    

def WaitOnContactEmployers():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
    cashwise = Sys.Process("Cashwise")
    x = 1
    #if(cashwise.WaitWindow("TfrmContactEmployerList","Contact Employers",1,1200).Exists):
    while cashwise.WaitWindow("TfrmContactEmployerList","Contact Employers",1,1200).Exists == False:
      delay(1000) 
      Log.Message(x)
      Log.Message("Waiting on Contact Employers")
      x = x+1
    Log.Message("Contact Employers Exists")

  
def GetLocationIDFromfrmMainWndCap():

    cashwise = Sys.Process("Cashwise")
    mainPageCaption = cashwise.frmMain.WndCaption
    EndPos = aqString.GetLength(mainPageCaption) 
    StartPos = 31 #start  postion
    
    Res = aqString.Remove(mainPageCaption, StartPos, EndPos )
          # Log["Message"](Res)
    
    aqString.ListSeparator = "-"
    NumOfWord = aqString.GetListLength(Res)
    delay(1)
   
    locationID =  aqString.GetListItem(Res, 2)
    locationID = aqString.Trim( locationID, aqString.stAll)
    Log.Message(locationID + " Location ID from Caption")
    return locationID

#------------------------------------------------------------
def CashwiseMainMenu_File(action):
 # c# script to python regx -- find case '(\S+)':  replace  if\(action == "\1"\): --
 #                             find case '(\S+ \S+)': replace if\(action == "\1"\): -- if there is a space
 #                     Normal  find ["   replace .
 #                     Normal  find "]   replace blank
 
 #action = "Setup"
  
 cashwise = Sys.Process("Cashwise"); 
 cashwiseMain =  cashwise.frmMain.MainMenu;   
   
 if(action == "Setup"):
        cashwise.frmMain.Keys("~Fs")
        #cashwiseMain.Click("File|Setup...");
 
 elif(action == "Roll Forward"):
        cashwiseMain.Click("File|Roll Forward...");
     
 elif(action == "About"):
        cashwiseMain.Click("File|About...");
 elif(action == "Help"):
        cashwiseMain.Click("File|Help...");
     
 elif(action == "Exit"):
        cashwiseMain.Click("File|Exit");
     
  
#-------------------------------------------------------------------------    

#------------------------------------------------------------
def CashwiseMainMenu_Reports(action):
 # c# script to python regx -- find case '(\S+)':  replace  if\(action == "\1"\): --
 #                             find case '(\S+ \S+)': replace if\(action == "\1"\): -- if there is a space
 #                     Normal  find ["   replace .
 #                     Normal  find "]   replace blank
 
 #action = "Report Events"
  
 cashwise = Sys.Process("Cashwise"); 
 cashwiseMain =  cashwise.frmMain.MainMenu;   
   
 if(action == "Setup"):
        cashwise.frmMain.Keys("~Fs")
        #cashwiseMain.Click("File|Setup...");
 
 elif(action == "Roll Forward"):
        cashwiseMain.Click("File|Roll Forward...");
     
 elif(action == "About"):
        cashwiseMain.Click("File|About...");
 elif(action == "Help"):
        cashwiseMain.Click("File|Help...");
     
 elif(action == "Report Events"):
        cashwiseMain.Click("Reports|Report Events...");
     
#------------------------------------------------------------
def CashwiseMainMenu_Window(action):

 #action = "Top" 
 cashwise = Sys.Process("Cashwise"); 
 cashwiseMain =  cashwise.frmMain.MainMenu;   
   
 if(action == "Top"):
        cashwise.frmMain.Keys("~W1")
 Log.Message("Click Top item in Window Meun")  
#-------------------------------------------------------------------------
def SystemSetupTreeView(action):

   tfrmSystemSetup = Sys.Process("Cashwise").frmSystemSetup
   treeView = tfrmSystemSetup.pnlMain.sbData.tvMain
   #  Cashwise crash CSToolsR.bpl  #  Cashwise crash CSToolsR.bpl
   #-----Access violation at address 02F4A555 in module 'CSToolsR.bpl'. Read of address 08BC7450.

   treeView.SetFocus()
 
   treeView.ClickItem("|Enterprise")
    
   if(action == "Save & Close"):
        tfrmSystemSetup.Keys("~s")
      
   elif(action == "Close"):
        tfrmSystemSetup.Keys("~c")
      
   elif(action == "Maximize"):
        tfrmSystemSetup.Maximize()

   elif(action == "Enterprise"):
        treeView.SelectItem("|Enterprise")
      
   elif(action == "Locations"):
          treeView.SelectItem("|Enterprise|Locations")
      
   elif(action == "Registration"):
          treeView.SelectItem("|Enterprise|Registration")
      
   elif(action == "Appearance/Behavior"):
        treeView.SelectItem("|Appearance/Behavior")
       
   elif(action == "Table Setup"):
          treeView.SelectItem("|Appearance/Behavior|Table Setup")
           
   elif(action == "Location"):
        treeView.SelectItem("|Location")
      
   elif(action == "Localization"):
          treeView.SelectItem("|Location|Localization")

   elif(action == "Check 21"):
          treeView.SelectItem("|Location|Check 21")
        
   elif(action == "Cashwise Services"):
          treeView.SelectItem("|Location|Cashwise Services")

   elif(action == "Cash Management"):
        treeView.SelectItem("|Cash Management")
      
   elif(action == "Contact"):
        treeView.SelectItem("|Contact")
      
   elif(action == "Bank"):
        treeView.SelectItem("|Bank")
      
   elif(action == "Inventory"):
        treeView.SelectItem("|Inventory")
      
   elif(action == "Sales"):
        treeView.SelectItem("|Sales")

   elif(action == "POS"):
          treeView.SelectItem("|Sales|POS")
      
   elif(action == "Check"):
          treeView.SelectItem("|Sales|Check")

   elif(action == "Credit Card"):
          treeView.SelectItem("|Sales|Credit Card")
    
   elif(action == "Line of Credit"):
          treeView.SelectItem("|Sales|Line of Credit")

   elif(action == "Loan Types"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Loan Types")
          
   elif(action == "Fee Types"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Fee Types")
      
   elif(action == "Location Settings"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Location Settings")
      
   elif(action == "Loan Type Selection"):
            treeView.SelectItem("|Sales|Cash Advance Setup|Loan Type Selection")

   elif(action == "Commercial Check Setup"):
          treeView.SelectItem("|Sales|Commercial Check Setup")

   elif(action == "Gold"):
          treeView.SelectItem("|Sales|Gold")
      
   elif(action == "Modules"):
          treeView.SelectItem("|Sales|Modules")
        
   elif(action == "Money Order"):
          treeView.SelectItem("|Sales|Money Order")

   elif(action == "Wire Transfer"):
          treeView.SelectItem("|Sales|Wire Transfer")
        
   elif(action == "NetSpend"):
          Sleep(5000)
          treeView.ClickItem("|Sales|Payouts|Stored Value Card|Netspend")

   elif(action == "Wise Card"):
          treeView.SelectItem("|Sales|Payouts|Wise Card")
        
        
   elif(action == "Wise Card Account Transaction"):
          treeView.SelectItem("|Sales|Payouts|Wise Card|Wise Card Account Transaction")
        

   elif(action == "Bill Pay"):
          treeView.SelectItem("|Sales|Bill Pay")

   elif(action == "Softgate Billpay (IPP)"):
          treeView.SelectItem("|Sales|Bill Pay|Softgate Billpay (IPP)")
        

   elif(action == "Check Free Pay (CFP)"):
          treeView.SelectItem("|Sales|Bill Pay|Check Free Pay (CFP)")
        
        
   elif(action == "Cashiers Check"):
          treeView.SelectItem("|Sales|Cashiers Check")
        
      
   elif(action == "ACH"):
        treeView.SelectItem("|ACH")
      
      
   elif(action == "Clarity"):
        treeView.SelectItem("|Clarity*")
      
      
   elif(action == "Tele-Track"):
        treeView.SelectItem("|Tele-Track")
      
      
   elif(action == "Peripherals"):
        treeView.SelectItem("|Peripherals")
      
      
   elif(action == "Capture"):
          treeView.SelectItem("|Peripherals|Capture")
        
      
   elif(action == "Device"):
            treeView.SelectItem("|Peripherals|Capture|Device")
          
      
   elif(action == "Points"):
            Sleep(10000)
            treeView.ClickItem("|Peripherals|Capture|Points")
          
        
   elif(action == "Labor"):
        treeView.SelectItem("|Labor")
      
      
   elif(action == "General Ledger Export Setup"):
        treeView.SelectItem("|General Ledger Export Setup")
      
      
   elif(action == "Collections"):
        treeView.SelectItem("|Collections")
      

   elif(action == "Configuration"):
          treeView.SelectItem("|Collections|Configuration")
        

   elif(action == "LocationCollectionSetting"):
          treeView.SelectItem("|Collections|LocationCollectionSetting")
        

   elif(action == "Command"):
        treeView.SelectItem("|Command")
      
   elif(action == "External App Setup"):
        treeView.SelectItem("|External App Setup")
      
   elif(action == "SMS"):
        treeView.SelectItem("|SMS")
      
  
def RemoveReportFromEvent(event):
  #event = "LCA"
  cashwise = Sys.Process("Cashwise")
  CashwiseMainMenu_Reports("Report Events")
  cashwise.frmReportEventList.pnlSearch.isMain.Keys(event)
  cashwise.frmReportEventList.btnDetailDelete.Click()
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()
  cashwise.frmReportEventList.btnSave.Click() 
  
def ManualLOCReportEventSetup():
  cashwise = Sys.Process("Cashwise")
  delay(1000)
  CashwiseMainMenu_Reports("Report Events")
  cashwise.frmReportEventList.pnlSearch.isMain.Keys("LPP")
  #BuiltIn.ShowMessage("Set the LPP Event up for LOC Autopay Authorzation report preview No other reports \n When LPP shows this one preview report click ok")
  delay(1000)
  cashwise.frmReportEventList.btnDetailDelete.Click()
  cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("OK").Click()
  delay(1000)
  cashwise.frmReportEventList.btnSave.Click()