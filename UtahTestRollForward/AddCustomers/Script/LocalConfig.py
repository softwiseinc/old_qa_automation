﻿# this is LocalConfig from Customers>CustomerHelper>BasePage>from TestUsers import *

# Items in this file are use as local configuration variable.  
# This file is meant to change often 
# Changes based on who or where test are being run.

def DatabaseValue(setting):
  # Ca = AppIntergration UtahTest
  #Cb = AppIntergration for Utah3
     # setting = "Ca"
      connectionValues = {}
      connectionValues["Server"] = ""
      connectionValues["Database"] = ""
      connectionValues["User"] = ""
      connectionValues["Pass"] = ""
      
      if(setting == "Ca"):
        #connectionValues["Server"] = "10.127.192.99"  ccc7sqltest.checkcity.local\ccc7sql2017
        connectionValues["Server"] = "ccc7sqltest.checkcity.local\ccc7sql2017"
        connectionValues["Database"] = "UtahTest"
        connectionValues["User"] = "sa"
        connectionValues["Pass"] = "pa$$0ut"
    
      if(setting == "Cb"):
          connectionValues["Server"] = "10.127.77.100"
          connectionValues["Database"] = "Utah3"
          connectionValues["User"] = "sa"
          connectionValues["Pass"] = "pa$$0ut"

      if(setting == "Cc"):
          connectionValues["Server"] = "10.1.43.86"
          connectionValues["Database"] = "Qa01"
          connectionValues["User"] = "sa"
          connectionValues["Pass"] = "softwise"
          
      return connectionValues 
    
    

def GetLocalConfigSettings():
    #Author Pat Holman 5/7/2019
    #Last Modified by Phil Ivey 8/16/2019
    localConfig = {}
    localConfig["Database"] = "QA01"
    #localConfig["Database"] = "UtahTest"
    #localConfig["Database"] = "SmokeFiveOneCurrent"
    localConfig["Server"] = "SWDPQABuild01"   
    #localConfig["Server"] = "ccc7sqltest.checkcity.local\\ccc7sql2017" 
    theServer = localConfig["Server"] 
    Log.Message("this is the server "+theServer)
    Log.Checkpoint("SUCCESS: GetLocalConfigSettings() is successful.")
    return localConfig
  
def GetLocalConfigSettingsDataBase():
      #Author Pat Holman 5/7/2019
      #Last Modified by Phil Ivey 8/16/2019
      localConfig = {}
      localConfig["Database"] = "QA01"
      dataBase =  localConfig["Database"]
      #localConfig["Database"] = "UtahTest"-------
      #localConfig["Database"] = "SmokeFiveOneCurrent"
      if(dataBase == "QA01"):
        localConfig["Server"] = "SWDPQABuild01" 
    
      elif(dataBase == "UtahTest"):
         localConfig["Server"] = "ccc7sqltest.checkcity.local\\ccc7sql2017" 
      
      elif(dataBase == "Utah3"):
        localConfig["Server"] = "ccqastgsql1.checkcity.local" 
        
      
      theServer = localConfig["Server"] 
      Log.Message("this is the server "+theServer)
      Log.Checkpoint("SUCCESS: GetLocalConfigSettings() is successful.")
      return localConfig
  
def GetTestResultContainer():
    #Author Phil Ivey 01/03/2020
    #Last Modified by Phil Ivey 01/03/2020
    testResults = {}
    testResults["Results1"] = "Name1 Results"
    testResults["Results2"] = "Name2 Results"
    testResults["Results3"] = "Name3 Results"
    return testResults