﻿
from Bank import AddBankAccount
from Customers import *
                                           
def TestLAddCustomer():
  #Author Phil Ivey 7/24/2019
  #Last Modified by Phil Ivey 7/24/2019
  Indicator.PushText("RUNNING: TestLAddCustomers")
  managerUser = GetManager001()
 
 
  customer = GetCustomer()
  
  AddCustomer(customer)
  
#  Customer = {}
#  Customer["FullName"] = "Beltran, Sara W"
#  customer = Customer
  
  AddBankAccount(customer)
  AddNewEmployer(customer)
  
  CheckCustomerValuesLog(customer)

  Log.Event("PASSED TEST: TestLineOfCreditMinLoan() Min LOC")
  Indicator.PopText()