﻿# THIS IS Customers FROM MAIN SCRIPT

from CustomerHelper import *
cashwise = Sys.Process("Cashwise")

def AddCustomer(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019   
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CreateCustomer(customer)")
  if(customer["ReferenceState"] == "EM"):
    customer["ReferenceState"] = "UT"
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  ClickAddCustomerButton()  
  customerFields = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields
  lastName = customerFields.pnlName.edLast
  firstName = customerFields.pnlName.edFirst
  middleInitial = customerFields.pnlName.edMiddle

  customerPhone = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  customerEmail = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.edEmailAddress
  
  notApplicable = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.cbNoEmail
  
  doNotContact = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.dbcDoNotContact
  activeMilitary = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlBasicOptions.pnlIsActiveMilitary.cbIsActiveMilitary
  customerAddress = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlAddress
  personalInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal
  marketingInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing
  additionalInformation = cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional
  customerReference = cashwise.frmCustomerAdd.pnlMain.sbData.pnlAdditional.pnlReference
  
  lastName.SetText(customer["LastName"])
  firstName.SetText(customer["FirstName"])
  middleInitial.Keys(customer["MiddleInitial"])
  
  customerPhone.edPrimaryPhone.Keys(customer["PrimaryPhone"])
  customerPhone.dbedSecondaryPhone.Keys(customer["SecondaryPhone"])
  PrimaryPhoneTypeID = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edPrimaryPhoneTypeID
  PrimaryPhoneTypeID.Keys(customer["PrimaryPhoneType"])
  SecondaryPhoneTypeID = cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone.edSecondaryPhoneTypeID
  SecondaryPhoneTypeID.Keys(customer["SecondaryPhoneType"])
  EnterSocialSecurityNumber(customer["SSN"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlAddress.dblefAddressTypeID.Keys(customer["AddressType"])
  customerAddress.edAddr1.SetText(customer["Address1"])
  customerAddress.edCity.SetText(customer["City"])
  customerAddress.edState.SetText(customer["State"])
  customerAddress.edZip.SetText(customer["ZipCode"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.edBirthdate.Keys(customer["BirthDate"])
  cashwise.frmCustomerAdd.pnlMain.sbData.pnlFields.pnlPersonal.edDescription.Keys(customer["Ethnicity"])
  personalInformation.edPrimaryLanguage.Keys(customer["PrimaryLanguage"])
  personalInformation.edHeight.Keys(customer["Height"])
  personalInformation.edWeight.Keys(customer["Weight"])
  SelectEyeColor(customer)
  SelectHairColor(customer)
  SelectGender(customer)
  SelectMarketingID(customer)
  SetMembershipExpertation(6)  
  customerEmail.SetText(customer["Email"])
  notApplicable.ClickButton(cbchecked)
  #notApplicable.ClickButton(cbUnchecked)
  customerEmail.Keys("[Tab]")
  SelectID1Type(customer)
  SelectID2Type(customer)
  #additionalInformation.pnlID1.edID1State.Keys(customer["ID1_State"])
  additionalInformation.pnlID1.edID1State.Keys("UT")
  additionalInformation.pnlID1.edID1Value.Keys(customer["ID1_Value"])
  additionalInformation.pnlID2.edID2State.Keys(customer["ID2_State"])
  additionalInformation.pnlID2.edID2Value.Keys(customer["ID2_Value"])
  customerReference.edReference1Name.SetText(customer["ReferenceName"])
  SelectReferenceRelation(customer)
  customerReference.edReference1Address.SetText(customer["ReferenceAddress1"])
  customerReference.edReference1PrimaryPhone.SetText(customer["ReferencePrimaryPhone"])
  customerReference.dbedReferenceEmailAddress.SetText(customer["ReferenceEmail"])
  customerReference.edReference1City.SetText(customer["ReferenceCity"])
  customerReference.edReference1Zip.Keys(customer["ReferenceZipCode"])
  customerReference.VCLObject("edReference1State").Keys(customer["ReferenceState"])
  
  SaveNewCustomer()
  CloseCustomerBrowse()
  cusID = GetCustomerIDFromName(customer["FullName"])
  customer["cusID"] = cusID
  Log.Checkpoint(customer["LastName"]+", "+customer["FirstName"])  
  Indicator.PopText()

def AddAdditionalInformation(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerEdit.MainMenu.Click("[2]|[8]")
  Indicator.PushText("In AddAdditionalInformation(customer)")
  formData = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData
  telephoneVerified = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneVerified
  phoneBillCurrent =  cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneCurrent
  phoneInName = cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbTelephoneInName
  telephoneVerified.ClickButton(cbChecked)
  phoneBillCurrent.Click()
  phoneInName.Click()
  formData.dbedTelephoneBalance.SetText("100")
  formData.dbckbBankruptcy.ClickButton(cbChecked)
  formData.dbckbResidenceOwned.ClickButton(cbChecked)
  formData.dbedResidenceMonths.SetText("55")
  formData.dbedResidenceAmount.SetText("1200")
  cashwise.frmCustomerAdditionalEdit.pnlMain.sbData.dbckbResidenceOwned.Keys("[F9]")
  Indicator.PopText()
  
def EditCustomer(customer, editCustomer):
  #Author Pat Holman 5/22/2019
  #Last Modified by Pat Holman 5/22/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditCustomer()")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  customerName = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlName
  customerPhone = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlPhone
  customerEmail = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlEmailAddress.edEmailAddress
  doNotContact = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.dbcDoNotContact
  activeMilitary = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlBasic.pnlBasicOptions.pnlIsActiveMilitary.cbIsActiveMilitary
  customerAddress = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlAddress
  personalInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlPersonal
  marketingInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlFields.pnlMisc.pnlMarketing
  additionalInformation = cashwise.frmCustomerEdit.pnlMain.sbData.pnlAdditional
  customerReference = cashwise.frmCustomerEdit.pnlMain.sbData.pnlAdditional.pnlReference
  customerName.edLast.SetText(editCustomer["LastName"])
  customerName.edFirst.SetText(editCustomer["FirstName"])
  customerName.edMiddle.SetText(editCustomer["MiddleInitial"])
  customerPhone.edPrimaryPhone.Keys(editCustomer["PrimaryPhone"])
  customerPhone.edPrimaryPhoneTypeID.Window("TCustomDBEdit", "*", 1).Keys(editCustomer["PrimaryPhoneType"])
  customerPhone.dbedSecondaryPhone.Keys(editCustomer["SecondaryPhone"])
  EditPrimaryPhoneType(editCustomer)
  EditSecondaryPhoneType(editCustomer)
  EditSocialSecurityNumber(editCustomer)
  customerAddress.edAddr1.SetText(editCustomer["Address1"])
  customerAddress.edCity.SetText(editCustomer["City"])
  customerAddress.edState.SetText(editCustomer["State"])
  customerAddress.edZip.SetText(editCustomer["ZipCode"])
  EditBirthDate(10)
  EditEthnicity(editCustomer)
  EditPrimaryLanguage(editCustomer)
  personalInformation.edHeight.Keys(editCustomer["Height"])
  personalInformation.edWeight.Keys(editCustomer["Weight"])
  EditEyeColor(editCustomer)
  EditHairColor(editCustomer)
  EditGender(editCustomer)
  EditMarketing(editCustomer)
  EditNotes()
  SaveAndCloseCustomerEdit()
  Indicator.PopText()
 
def EditCustomerReferences(customer, editCustomer): 
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 6/7/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EditCustomerReferences(customer, editCustomer)")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  ClickRefrenceButton()
  contactName = cashwise.frmContactReferenceEdit.pnlMain.sbData.dbeName
  contactPanel = cashwise.frmContactReferenceEdit.pnlMain.sbData
  contactReferenceEdit = cashwise.frmContactReferenceEdit
  SelectStatusID()
  contactPanel.dbeName.SetText(editCustomer["FirstName"] + " " +  editCustomer["LastName"])
  contactPanel.pnlAddress.edAddress.SetText(editCustomer["ReferenceAddress1"])
  contactPanel.pnlAddress.edCity.SetText(editCustomer["ReferenceCity"])
  contactPanel.pnlAddress.edState.SetText(editCustomer["ReferenceState"])
  contactPanel.pnlAddress.edZip.SetText(editCustomer["ReferenceState"])
  contactPanel.edPrimaryPhone.Keys(editCustomer["ReferencePrimaryPhone"])
  contactPanel.dbedSecondaryPhone.Keys(editCustomer["ReferencePrimaryPhone"])
  contactPanel.dbeEmailAddress.Keys(editCustomer["ReferenceEmail"])
  SelectPrimaryPhoneTypeID(editCustomer)
  SelectSecondaryPhoneTypeID(editCustomer)
  doNotContact = contactPanel.dbcbDoNotContact
  doNotContact.ClickButton(cbChecked)
  doNotContact.Keys("[F9]")
  cashwise.frmContactReferenceList.btnSave.Click()
  cashwise.frmCustomerEdit.btnSave.Click()
  CloseCustomerBrowse()  
  Indicator.PopText()

    
def CheckCustomerValuesLog(customer):
   varNameArray = ["cusID","FirstName",	"LastName",	"MiddleInitial",	"FullName",	"PrimaryPhone",	"PrimaryPhoneType",	"SecondaryPhone",	"SecondaryPhoneType",	"SSN",	"AddressType",	"Address1",	"Address2",	"City",	"State",	"ZipCode",	"BirthDate",	"Ethnicity",	"PrimaryLanguage",	"Height",	"Weight",	"EyeColor", "HairColor",	"Gender",	"MarketingID",	"Email",	"ID1",	"ID1_State",	"ID1_Value",	"ID2",	"ID2_State",	"ID2_Value",	"ReferenceRelation",	"ReferenceName",	"ReferencePrimaryPhone",	"ReferenceEmail",	"ReferenceAddress1",	"ReferenceCity",	"ReferenceState",	"ReferenceZipCode",	"EmployerName",	"Department",	"Position",	"WorkPhone",	"WorkPhoneExtention",	"Supervisor",	"SupervisorPhone",	"SupervisorPhoneExtention",	"PayPeriod",	"GrossPay", "NetPay",	"Garnishment",	"WorkStartTime",	"WorkStopTime"]
   x = len(varNameArray)
   numOfColumns = (x - 1) 
       
   i = 0
   while  i <= numOfColumns:
      aqConvert.IntToStr(i)
      colName = customer[varNameArray[i]]
      colValue = aqConvert.VarToStr(colName)
      number =  aqConvert.IntToStr(i)
      Log.Message("Checking values for number "+number+" "+varNameArray[i]+" is "+colValue) 
      if(i == 49):
               Log.Message("this is PayPeriod") 
      i = i + 1
  

  
def CreatCustomerData1():
     #Author Phil Ivey 8/20/2019
     #Last Modified by Phil Ivey 12/04/2019 
     firstName = "" 
     locationID = GetLocationIDFromfrmMainWndCap()
     cashwise = Sys.Process("Cashwise")
     firstName = GetRandomFirstName()
     lastName = GetRandomLastName()
     randomText = GenerateRandomString(8)
     locID = GetLocationIDFromfrmMainWndCap()
     randomMiddleInitial = GenerateRandomString(1)
     randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
     randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
     ReferencePrimaryPhone = GenerateRandomPhoneNumber()
     
     randomSSN = GenerateRandomSSN()
#     ssnStatus = DoesSSNExistInDatabase(randomSSN)  #Exists and NotExists
#     while ssnStatus == "Exists":
#        randomSSN = GenerateRandomSSN()
#        ssnStatus = DoesSSNExistInDatabase(randomSSN)
        
     today = aqDateTime.Today()
     birthDay = GetRandomBirthDate()
     zip = GetRandomZip("UT","846")
     city = GetCityFromZip(zip)
     address1 = GetRamdomAddressInCity(city)
     state = GetStateFromZip(zip)
     email = lastName+firstName+"@ivey.pro"
     employerName = GetRandomCustomerEmplorerName()
     department = GetRandomDepartment()
     position = GetRandomPostiion(department)
     payPeriod = RandomPayFreq()
     if payPeriod == "M":
       payPeriod = "Monthly"
     elif payPeriod == "S":
       payPeriod = "Semi-Monthly"
     elif payPeriod == "B":
       payPeriod = "Bi-Weekly"
     elif payPeriod == "W":
       payPeriod = "Weekly"
     payPeriod = "Bi-Weekly"
     newContact = {}
     newContact["FirstName"] = firstName
     newContact["LastName"] = lastName
     newContact["MiddleInitial"] = randomMiddleInitial
     newContact["FullName"] = lastName+", "+firstName
     newContact["PrimaryPhone"] = randomPrimaryPhoneNumber
     newContact["PrimaryPhoneType"] = "HOM"
     newContact["SecondaryPhone"] = randomSecondaryPhoneNumber
     newContact["SecondaryPhoneType"] = "CEL"
     newContact["SSN"] = randomSSN
     newContact["AddressType"] = "HOM"
     newContact["Address1"] = address1
     newContact["Address2"] = "Apt. 312"  
     newContact["City"] = city  
     newContact["State"] = state 
     newContact["ZipCode"] = zip
     newContact["BirthDate"] = birthDay  
     newContact["Ethnicity"] = "CC" 
     newContact["PrimaryLanguage"] = "ENG" 
     newContact["Height"] = "511" 
     newContact["Weight"] = "200" 
     newContact["EyeColor"] = "BRN" 
     newContact["HairColor"] = "BRN" 
     newContact["Gender"] = "Male" 
     newContact["MarketingID"] = "TV" 
     newContact["Email"] = email
     newContact["ID1"] = "ID"
     newContact["ID1_State"] = state
     newContact["ID1_Value"] = randomText
     newContact["ID2"] = "DL"
     newContact["ID2_State"] = "UT"
     newContact["ID2_Value"] = randomText
     newContact["Password"] = randomText
     newContact["ReferenceRelation"] = "OTH"
     newContact["ReferenceName"] = "Reference_Name" + randomText
     newContact["ReferencePrimaryPhone"] = ReferencePrimaryPhone
     newContact["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
     newContact["ReferenceAddress1"] = "512 N 200 E"
     newContact["ReferenceCity"] = "Orem"
     newContact["ReferenceState"] = "UT"
     newContact["EmployerName"] = employerName
     newContact["Department"] = department
     newContact["Position"] = position
     newContact["WorkPhone"] = "8013611123"
     newContact["WorkPhoneExtention"] = "1123"
     newContact["ReferenceZipCode"] = "84057"
     newContact["Supervisor"] = "RaNay Ash"
     newContact["SupervisorPhone"] = "8013611122"
     newContact["SupervisorPhoneExtention"] = "1122"
     newContact["PayPeriod"] = payPeriod
     newContact["GrossPay"] = "4000"
     newContact["NetPay"] = "3000"
     newContact["Garnishment"] = "1000"
     newContact["WorkStartTime"] = "8:00 AM"
     newContact["WorkStopTime"] = "5:00 PM"
     newContact["Overide"] = "~s"
     newContact["isRandomType"] = True  
     newContact["locationID"] = locationID
     newContact["cusID"] = ""
     Log.Message("Customer Name = "+newContact["FullName"]) 
     Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
     return newContact
     
def GetNewCustomerInfo():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer
    

def AddNewEmployer(customer):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  OpenCustomerBrowser()
  SelectCustomerByName(customer)
  OpenCustomerEdit()
  OpenEmployerContact()
  OpenAddEmployerContact()

  department = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedDepartment
  workPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedPhone
  workPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedExtension
  supervisor = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisor
  supervisorPhone = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorPhone
  supervisorPhoneExtention = cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedSupervisorExtension
  grossPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGrossPay  
  netPay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedNetPay
  garnishmet = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedGarnishments
  hasDirectDeposit = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbHasDirectDeposit 
                                        #cashwise.frmContactEmployerEdit.pnlMain.sbData.dbckbHasDirectDeposit
  cbPreferredEmployer = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.cbPreferredEmployer
  workStartTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStartTime
  workStopTime = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbedWorkStopTime
  workDayMonday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayMonday
  workDayTuesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayTuesday
  workDayWednesday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayWednesday
  workDayThursday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayThursday
  workDayFriday = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.dbckbWorkDayFriday
  saveAndCloseButton = cashwise.frmContactEmployerEdit.btnSave
  SelectEmployerName(customer)
  SelectStartDate(3)  
  department.SetText(customer["Department"])
  SelectPosition(customer)
  workPhone.SetText(customer["WorkPhone"])
  workPhoneExtention.SetText(customer["WorkPhoneExtention"])
  supervisor.SetText(customer["Supervisor"])
  supervisorPhone.SetText(customer["SupervisorPhone"])
  supervisorPhoneExtention.SetText(customer["SupervisorPhoneExtention"])
  SelectPayPeriod(customer)
  
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  
  grossPay.SetText(customer["GrossPay"])
  netPay.SetText(customer["NetPay"])
  garnishmet.SetText(customer["Garnishment"])
  ExplicitWait(1)
  hasDirectDeposit.ClickButton(cbChecked)
  cbPreferredEmployer.ClickButton(cbChecked)
  workStartTime.SetText(customer["WorkStartTime"])
  workStopTime.SetText(customer["WorkStopTime"])
  workDayMonday.ClickButton(cbChecked)
  workDayTuesday.ClickButton(cbChecked)
  workDayWednesday.ClickButton(cbChecked)
  workDayThursday.ClickButton(cbChecked)
  workDayFriday.ClickButton(cbChecked)
  saveAndCloseButton.Click()
  
  SaveAndCloseContactEmployers()
  SaveAndCloseCustomerEdit()
  CloseCustomerBrowse()
  
  
def OpenEmployerContact():
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenEmployerContact()")
#  mainMenu = cashwise.frmCustomerEdit.MainMenu
#  mainMenu.Click("[2]|[14]")
  cashwise.frmCustomerEdit.btnEmployers.Click()
  Log.Checkpoint("SUCCESS: OpenEmployerContact() is successful.")
  Indicator.PopText()
   
def OpenAddEmployerContact():
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenAddEmployerContact()")
  tfrmContactEmployerList = cashwise.frmContactEmployerList
  tfrmContactEmployerList.pnlMain.dbgMain.SetFocus()
  tfrmContactEmployerList.pnlMain.dbgMain.Keys("[F12]")
  Log.Checkpoint("SUCCESS: OpenAddEmployerContact() is successful.")
  Indicator.PopText()
  
  
def FindCustomerById(customerId):
  cashwise = Sys.Process("Cashwise")
  tfrmCustomerBrowse = Aliases.Cashwise.frmCustomerBrowse
  #tfrmCustomerBrowse.Click(157, 14)
  incrementalSearch = tfrmCustomerBrowse.pnlSearch.isMain
  #incrementalSearch.Click(76, 8)
  incrementalSearch.Keys(customerId)
  ExplicitWait(3)
  
def UpdateEmploymentBiWeekly():# on customer edit screen known bi-weekly pay period
  cashwise = Sys.Process("Cashwise")
  cashwise.frmCustomerEdit.btnEmployers.Click()
  cashwise.frmContactEmployerList.btnEdit.Click()
  cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.btnBiweeklySelect.Click()
  cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateTwo.ClickButton()
  nextPayDate = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly.dlbledPayDate1.wText
  cashwise.frmContactBiweeklyEdit.btnSave.Click()
  cashwise.frmContactEmployerEdit.btnSave.Click()
  cashwise.frmContactEmployerList.btnSave.Click()
  return nextPayDate
  
  

def SelectEmployerName(customer):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectEmployerName(customer)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.edEmployer.TAdvancedSpeedButton.Click(10, 7)
  cashwise.frmEmployerBrowse.pnlSearch.isMain.Keys(customer["EmployerName"])
  ExplicitWait(3)
  cashwise.frmEmployerBrowse.pnlSearch.isMain.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SelectEmployerName(customer) is successful.")
  Indicator.PopText()

def SelectStartDate(years):
  #Author Pat Holman 5/24/2019
  #Last Modified by Pat Holman 5/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectStartDate(years)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.dbedStartDate.TAdvancedSpeedButton.Click(8, 6)
  advancedButton = cashwise.frmCalendarEdit.Panel1.btnPriorYear
  for x in range(years):
    advancedButton.Click(14, 17)
    ExplicitWait(1)
  advancedButton.Keys("[F9]")
  
    
  
  Log.Checkpoint("SUCCESS: SelectStartDate(years) is successful.")
  Indicator.PopText()

def SelectPosition(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectPosition(customer)")
  cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.TAdvancedSpeedButton.Click(7, 8)
  incrementalSearch = cashwise.frmEmploymentPositionBrowse.pnlSearch.isMain
  incrementalSearch.Click()
  incrementalSearch.DblClick()
  incrementalSearch.Keys(customer["Position"])
  ExplicitWait(3)
  incrementalSearch.Keys("[F9]")

  if(cashwise.WaitWindow("#32770", "Cashwise", 1,5000).Exists):
    cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
    cashwise.frmEmploymentPositionBrowse.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
    
    
    cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.Keys("[Home]![End][Del]")
    cashwise.frmContactEmployerEdit.pnlMain.sbData.dblefEmploymentPosition.Keys(customer["Position"])
    ExplicitWait(3)
   
  Log.Checkpoint("SUCCESS: SelectPosition(customer) is successful.")
  Indicator.PopText()
       
def SelectPayPeriod(customer):
  #Author Pat Holman 5/28/2019
  #Last Modified by Phil 8/2/2019 
  GetRandomBirthDate() 
  # ------------------------biweekly-semi-monthly-monthlyday-monthlythe-weekly
  cusPayPeriod = customer["PayPeriod"]
  cusPayPeriod = aqString.ToLower(cusPayPeriod)
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectPayPeriod(customer)")
  #  cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Bi-Weekly", 4)
  #payPeriod = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod
  payPeriod = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod
  #biweekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly 
  biweekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Bi-Weekly", 4) 
  #semiMonthly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlSemiMonthly
  semiMonthly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Semi-Monthly", 3)
  #monthly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly
  monthly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Monthly", 2)
  #weekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly
  weekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriod.Window("TGroupButton", "Weekly", 1)
  
  if cusPayPeriod == "biweekly":
    #cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod.Window("TGroupButton", "Bi-Weekly", 4).ClickButton()
    biweekly.ClickButton()
   # biweekly = cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlBiWeeklyWeekly  
    biweeklyDay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly
    dayOfWeekPaid = GetRadomDayOfWeek()
    

    
    if dayOfWeekPaid == "Monday":
      biweeklyDay.cbPaydayMonday.ClickButton(cbChecked)
      #biweekly.cbPaydayMonday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Tuesday":
      biweeklyDay.cbPaydayTuesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Wednesday":
      biweeklyDay.cbPaydayWednesday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Thursday":
      biweeklyDay.cbPaydayThursday.ClickButton(cbChecked)
    elif dayOfWeekPaid == "Friday":
      biweeklyDay.cbPaydayFriday.ClickButton(cbChecked)
    
      
    biweeklyDay.btnBiweeklySelect.Click()
    cashwise.frmContactBiweeklyEdit.pnlMain.sbData.pnlDates.rbDateOne.Click()
    cashwise.frmContactBiweeklyEdit.btnSave.Click()
  
  elif cusPayPeriod == "semi-monthly":
    #cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod.Window("TGroupButton", "Semi-Monthly", 3).ClickButton() 
    semiMonthly.ClickButton()   
    ExplicitWait(3)
    semiMonthly.pnlSemiDay1.cbSemiDay1.ClickItem("5")
    semiMonthly.pnlSemiDay2.cbSemiDate2.ClickItem("20")
  
  elif cusPayPeriod == "monthlyday":
    monthly.ClickButton()
    
    monthlyDay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType
    monthlyDay.Window("TGroupButton", "Day", 2).ClickButton()
    
   # cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType.Window("TGroupButton", "The", 1).ClickButton()
    monthly.cbMonthPatternName.ClickItem("Second")
    monthly.cbPatternMonthDay.ClickItem("Wednesday")
    monthly.edPayDayOne.Click(41, 7)
 
  elif cusPayPeriod == "monthlythe":
    monthly.ClickButton()
    
    monthlyDay = cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType
    monthlyDay.Window("TGroupButton", "Day", 2).ClickButton()
    
   # cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriodPattern.pnlMonthly.rgMonthlyPatternType.Window("TGroupButton", "The", 1).ClickButton()
    monthly.cbMonthPatternName.ClickItem("Second")
    monthly.cbPatternMonthDay.ClickItem("Wednesday")
    monthly.edPayDayOne.Click(41, 7)
    
  elif cusPayPeriod == "weekly":
  #cashwise.frmContactEmployerEdit.pnlMain.sbData.gbPayPeriod.Window("TGroupButton", "Weekly", 1).ClickButton()
    weekly.ClickButton()
    #cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly.cbPaydayTuesday
  cashwise.frmContactEmployerEdit.pnlMain.sbData.pnlPayPeriodSettings.gbPayPeriodPattern.pnlBiWeeklyWeekly.cbPaydayTuesday.ClickButton(cbChecked)
   # weekly.dlbledPayDate1.Click(41, 5)
  ExplicitWait(3)
  Log.Checkpoint("SUCCESS: SelectPayPeriod(customer) is successful.")
  Indicator.PopText()

def SaveAndCloseContactEmployers():
  #Author Pat Holman 5/28/2019
  #Last Modified by Pat Holman 5/28/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveAndCloseContactEmployers()")
  cashwise.frmContactEmployerList.btnSave.Click(53, 10)
  Log.Checkpoint("SUCCESS: SaveAndCloseContactEmployers() is successful.")
  Indicator.PopText()
    

def EditSelectedEmployer():
  cashwise = Sys.Process("Cashwise")
  employerEditForm = cashwise.frmContactEmployerEdit
  editButton = cashwise.frmContactEmployerList.btnEdit
  editButton.Click()
  employerEditForm.WaitWindow(5)

#-----------------------------------------
def GetCalendarDateString(dateString):

 #  Monday, September 25, 2017      "Sunday, January 14,2018"
      #dateString = "Wednesday, January 31, 2018"
   
      
      NumOfWord = aqString.GetListLength(dateString)
      aqString.ListSeparator = ","
      wordArray = Sys.OleObject["Scripting.Dictionary"]

      for x in range(NumOfWord):
          y = aqConvert.IntToStr(x)
          wordArray.Add(y, aqString.GetListItem(dateString,x))
          s = wordArray.Item[y];
          Log.Message(s)
      dateInMonth = wordArray.Item["1"] #---January 14
      dateInMonth =  aqString.Trim(dateInMonth, aqString.stAll)
      dateInMonth = aqString.Replace(dateInMonth, " ", ",")  #'January,31'
      NumOfWord2 = aqString.GetListLength(dateInMonth)
      aqString.ListSeparator = ","
      
      wordArray2 = Sys.OleObject["Scripting.Dictionary"]
      x = 0
      y = ""
      for x in range(NumOfWord2):
                y = aqConvert.IntToStr(x)
                wordArray2.Add(y, aqString.GetListItem(dateInMonth,x))
                s = wordArray2.Item[y];
                Log.Message(s)
      dateInMonth2 = wordArray2.Item["1"]
      Log.Message(dateInMonth2)

def StringDateParse(dateString): #--Converts Sunday, January 14, 2018 to 1/14/2018

   
      # dateString = Sys.Process"]("Cashwise").VCLObject"]("frmCalendarEdit").VCLObject"]("txtDate").Caption"]
      #dateString = "Sunday, January 14, 2018"
      dateString = aqString.Replace(dateString, ",", "")
      aqString.ListSeparator = " "
      NumOfWord = aqString.GetListLength(dateString)
     
      wordArray = Sys.OleObject["Scripting.Dictionary"]

      for x in range(NumOfWord):
          y = aqConvert.IntToStr(x)
          wordArray.Add(y, aqString.GetListItem(dateString,x))
          s = wordArray.Item[y];
          Log.Message("number = "+aqConvert.IntToStr(x)+"word = "+s) 
      
      dayOfTheWeek = wordArray.Item["0"]
      monthWord = wordArray.Item["1"]
      dayOfMonth = wordArray.Item["2"]
      year = wordArray.Item["3"]
          
     
      monthDate = switchMonthToNumber(monthWord)
 
      dateStringNum = monthDate+"/"+dayOfMonth+"/"+year
      Log.Message("Converted "+dateString+" to "+dateStringNum)
      
      return dateStringNum

      
      #--------------------------------------
    
def switchMonthToNumber(monthString):
    switcher = {
        "January": "1",
        "February": "2",
        "March": "3",
        "April": "4",
        "May": "5",
        "June": "6",
        "July": "7",
        "August": "8",
        "September": "9",
        "October": "10",
        "November": "11",
        "December": "12"
        
    }
    month = switcher.get(monthString, "Invalid month")
    return month
    
def AddCustomer1():
    customer = CreatCustomerData1()
    AddCustomer(customer)
    AddNewEmployer(customer)
    AddCategoryLocPassLimit(customer,1200)
    fullName = customer["FullName"]
    cusID = GetCustomerIDFromName(fullName)
    customer["cusID"] = cusID
    return customer