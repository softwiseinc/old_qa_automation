﻿from BasePage import *
from Helpers import *

def CloseCashDrawer():
  #Author Pat Holman 7/26/2019
  #Last Modified by Phil Ivey 8/1/2019  
  cashwise = Sys.Process("Cashwise")
  cashwise.Find("ObjectIdentifier", "TGraphicButton_6", 10).Click()
  cashwise.Find("ObjectIdentifier", "btnCloseCash", 10).Click()
  if(cashwise.WaitWindow("#32770","Cashwise",1,500).Exists):
    DeleteUnprocessSales()
    cashwise.Find("ObjectIdentifier", "btnCloseCash", 10).Click()
  if(cashwise.WaitWindow("#32770", "Cashwise", 1,500).Exists):
        Sys.Process("Cashwise").Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
  ExplicitWait(4)
  if(cashwise.WaitWindow("TMessageForm", "Cashwise", 1,800).Exists):
        cashwise.Window("TMessageForm", "Cashwise", 1).VCLObject("OK").Click()
  cashwise.VCLObject("frmDrawerManager").VCLObject("btnClose").Click()
 
  # fix later
#   message = cashwise.Window("TMessageForm", "Cashwise", 1).VCLObject("Message").Caption 
#  if "successfully closed" in message:
#    Log.Message(message)
#    cashwise.Find("ObjectIdentifier", "OK", 10).Click()
#    cashwise.Find("ObjectIdentifier", "frmDrawerManager", 10).Keys("[F9]")
#  else:
#    Log.Error("Drawer NOT closed")
  
def OpenCashDrawer():
  #Author Pat Holman 7/26/2019
  #Last Modified by Pat Holman 7/26/2019  
  cashwise = Sys.Process("Cashwise")
  ExplicitWait(2)
  CloseCashDrawer()
  cashwise.Find("ObjectIdentifier", "TGraphicButton_6", 10).Click()
  cashwise.Find("ObjectIdentifier", "btnOpenCash", 10).Click()
  message = cashwise.Find("ObjectIdentifier", "Message", 10).Caption 
  if "successfully opened" in message:
    Log.Message(message)
    cashwise.Find("ObjectIdentifier", "OK", 10).Click()
    cashwise.Find("ObjectIdentifier", "frmDrawerManager", 10).Keys("[F9]")
    OpenAndClosePOS()
  else:
    Log.Error("Drawer NOT opened")
    
def OpenAndClosePOS():
  #Author Phil 7/3/2019
  #Last Modified by Phil 7/3/2019 
  cashwise = Sys.Process("Cashwise")
  cashwise.Find("ObjectIdentifier", "TGraphicButton", 10).Click()
  activeWindow = Sys.Desktop.ActiveWindow()
  cashwise.Find("","",25)
  message = activeWindow.Caption
  
  
  Res = aqString.Find(message, "Confirm")
  if ( Res != -1 ):
    cashwise.Find("Caption", "*Yes", 10).Click()
  
  if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,1800).Exists):
    cashwise.frmCashOpenConfirm.Button1.Click()
    
  cashwise.Find("ObjectIdentifier", "btnClose", 10).Click()
  
def DeleteUnprocessSales(): # Start on Drawer Manager Screen
  #Author Phil Ivey 8/1/2019
  #Last Modified by Phil Ivey 8/1/2019  
   cashwise = Sys.Process("Cashwise")
   cashwise.Window("#32770", "Cashwise").Window("Button", "OK").ClickButton()
   cashwise.frmDrawerManager.btnUnprocessedSales.Click()
   
   while cashwise.frmSaleUnprocessed.btnEdit.Enabled == True:
     cashwise.frmSaleUnprocessed.btnEdit.Click()
     cashwise.Window("TMessageForm", "Confirm").OK.ClickButton()
     saleCancel =   cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel
     saleCancel.Click()
     cashwise.Window("TMessageForm", "Confirm").OK.ClickButton()
     ExplicitWait(1)
   cashwise.frmSaleUnprocessed.btnClose.Click()