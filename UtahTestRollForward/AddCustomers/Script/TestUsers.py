﻿# this is TestUsers from Customers>CustomerHelper> BasePage>from TestUsers import *


#import BasePage
#import SystemSetupHelper
from BasePage import *
from SystemSetupHelper import *

#cashwise = Sys.Process("Cashwise")
Owner = {}
Owner["userId"] = "OWN"
Owner["Password"] = "own"

Manager001 = {}
Manager001["userId"] = "MAN"
Manager001["Password"] = "man"
Manager001["location"] = "001"


Manager002 = {}
Manager002["userId"] = "MAN"
Manager002["Password"] = "man"
Manager002["location"] = "001"

Manager003 = {}
Manager003["userId"] = "MAN"
Manager003["Password"] = "man22"
Manager003["location"] = "001"

user001 = {}
user001["ID"] = "1"
user001["FirstName"] = "Harry"
user001["LastName"] = "Potter"
user001["Group"] = "MAN"
user001["Password"] = "user1"
user001["Overide"] = "~s"
user001["isRandomType"] = False

user002 = {}
user002["ID"] = "1"
user002["FirstName"] = "Harry"
user002["LastName"] = "Potter"
user002["Group"] = "MAN"
user002["Password"] = "user1"
user002["Overide"] = "~s"
user002["isRandomType"] = False

def GetManager002():
  cashwise = Sys.Process("Cashwise")
  localConfig = GetLocalConfigSettings()
  Log.Message(localConfig["Database"])
  Manager002["database"] = localConfig["Database"]
  if(Manager002["database"] == "UtahTest"):
      Manager002["Password"] = "man2"

  Log.Message(Manager002["database"])
  return Manager002
  
def GetDefaultIndexInfo(loantype): # tableIndex has info for quering, aQuery,Column,ColoumnValue
  cashwise = Sys.Process("Cashwise")
  tableIndex = {}
  tableIndex["SqlStatement"] = "Select ROW_NUMBER() OVER ( ORDER BY ID,LoanModule_ID ) TheIndex, ID from DeferredType"
  tableIndex["Coloumn"] = "ID"
  tableIndex["ColoumnValue"] = loantype
  return tableIndex
  
def GetManager001():
  cashwise = Sys.Process("Cashwise")
  localConfig = GetLocalConfigSettings()
  Manager001["database"] = localConfig["Database"]
  return Manager001
  
def GetManager001Database(dataBase):  # managerUser["database"], managerUser["Password"],managerUser["location"]
   # cashwise = Sys.Process("Cashwise")
    #localConfig = GetLocalConfigSettingsDataBase(dataBase)# localConfig["Database"],["Server"],["userPass"]
    dataBase = "UtahTest"
    Manager003["database"] = dataBase
    return Manager003

def GetTempUser():
  cashwise = Sys.Process("Cashwise")
  randomText = GenerateRandomString(8)
  newUser = {}
  newUser["ID"] = randomText
  newUser["FirstName"] = "First_"+randomText
  newUser["LastName"] = "Last_"+randomText
  newUser["Group"] = "MAN"
  newUser["Password"] = randomText
  newUser["Location"] = "001"
  newUser["Overide"] = "~s"
  newUser["isRandomType"] = True
  Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
  return newUser

def GetTempCustomer():
  # edited Phil Ivey 8/7/2019
  cashwise = Sys.Process("Cashwise")
  randomText = GenerateRandomString(8)
  randomMiddleInitial = GenerateRandomString(1)
  randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
  randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
  ReferencePrimaryPhone = GenerateRandomPhoneNumber()
  randomSSN = GenerateRandomSSN()
  ssnStatus = DoesSSNExistInDatabase(randomSSN)  #Exists and NotExists
  while ssnStatus == "Exists":
        randomSSN = GenerateRandomSSN()
        ssnStatus = DoesSSNExistInDatabase(randomSSN)
  today =  aqDateTime.Today()
  
  newCustomer = {}
  newCustomer["RadomText"] = randomText
  newCustomer["FirstName"] = randomText +  "_FirstName"
  newCustomer["LastName"] = randomText +  "_LastName"
  newCustomer["FullName"] = newCustomer["LastName"]+", "+newCustomer["FirstName"]
  newCustomer["MiddleInitial"] = randomMiddleInitial
  newCustomer["PrimaryPhone"] = randomPrimaryPhoneNumber
  newCustomer["PrimaryPhoneType"] = "HOM"
  newCustomer["SecondaryPhone"] = randomSecondaryPhoneNumber
  newCustomer["SecondaryPhoneType"] = "CEL"
  newCustomer["SSN"] = randomSSN
  newCustomer["AddressType"] = "HOM"
  newCustomer["Address1"] = "410 N Main St"
  newCustomer["Address2"] = "Apt. 312"  
  newCustomer["City"] = "Orem"  
  newCustomer["State"] = "UT"  
  newCustomer["ZipCode"] = "84057"
  newCustomer["BirthDate"] = "02/23/1960"  
  newCustomer["Ethnicity"] = "CC" 
  newCustomer["PrimaryLanguage"] = "ENG" 
  newCustomer["Height"] = "511" 
  newCustomer["Weight"] = "200" 
  newCustomer["EyeColor"] = "BRN" 
  newCustomer["HairColor"] = "BRN" 
  newCustomer["Gender"] = "Male" 
  newCustomer["MarketingID"] = "TV" 
  newCustomer["Email"] = randomText +  "@msgnext.com"
  newCustomer["ID1"] = "ID"
  newCustomer["ID1_State"] = "UT"
  newCustomer["ID1_Value"] = randomText
  newCustomer["ID2"] = "DL"
  newCustomer["ID2_State"] = "UT"
  newCustomer["ID2_Value"] = randomText
  newCustomer["Password"] = randomText
  newCustomer["ReferenceRelation"] = "OTH"
  newCustomer["ReferenceName"] = "Reference_Name" + randomText
  newCustomer["ReferencePrimaryPhone"] = ReferencePrimaryPhone
  newCustomer["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
  newCustomer["ReferenceAddress1"] = "512 N 200 E"
  newCustomer["ReferenceCity"] = "Orem"
  newCustomer["ReferenceState"] = "UT"
  newCustomer["EmployerName"] = "Fidelity Investments"
  newCustomer["Department"] = "Engineering"
  newCustomer["Position"] = "HR"
  newCustomer["WorkPhone"] = "8013611123"
  newCustomer["WorkPhoneExtention"] = "1123"
  newCustomer["ReferenceZipCode"] = "84057"
  newCustomer["Supervisor"] = "RaNay Ash"
  newCustomer["SupervisorPhone"] = "8013611122"
  newCustomer["SupervisorPhoneExtention"] = "1122"
  newCustomer["PayPeriod"] = "Monthly"
  newCustomer["GrossPay"] = "4000"
  newCustomer["NetPay"] = "3000"
  newCustomer["Garnishment"] = "1000"
  newCustomer["WorkStartTime"] = "8:00 AM"
  newCustomer["WorkStopTime"] = "5:00 PM"
  newCustomer["Overide"] = "~s"
  newCustomer["isRandomType"] = True   
  Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
  return newCustomer
  
def GetEditCustomer():
  cashwise = Sys.Process("Cashwise")
  randomText = GenerateRandomString(8)
  randomMiddleInitial = GenerateRandomString(1)
  randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
  randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
  ReferencePrimaryPhone = GenerateRandomPhoneNumber()
  randomSSN = GenerateRandomSSN()
  today = aqDateTime.Today()
  editCustomer = {}
  editCustomer["FirstName"] = randomText +  "_FirstName"
  editCustomer["LastName"] = randomText +  "_LastName"
  editCustomer["MiddleInitial"] = randomMiddleInitial
  editCustomer["PrimaryPhone"] = randomPrimaryPhoneNumber
  editCustomer["PrimaryPhoneType"] = "CEL"
  editCustomer["SecondaryPhone"] = randomSecondaryPhoneNumber
  editCustomer["SecondaryPhoneType"] = "HOM"
  editCustomer["SSN"] = randomSSN
  editCustomer["AddressType"] = "HOM"
  editCustomer["Address1"] = "517 N 200 E"
  editCustomer["Address2"] = "Apt. 517"  
  editCustomer["City"] = "Las Vegas"  
  editCustomer["State"] = "NV"  
  editCustomer["ZipCode"] = "89107"
  editCustomer["BirthDate"] = "03/23/1960"  
  editCustomer["Ethnicity"] = "OTH" 
  editCustomer["PrimaryLanguage"] = "SPA" 
  editCustomer["Height"] = "510" 
  editCustomer["Weight"] = "170" 
  editCustomer["EyeColor"] = "BLA" 
  editCustomer["HairColor"] = "BLA" 
  editCustomer["Gender"] = "Female" 
  editCustomer["MarketingID"] = "KN" 
  editCustomer["Email"] = randomText +  "@msgnext.com"
  editCustomer["ID1"] = "DL"
  editCustomer["ID1_State"] = "CO"
  editCustomer["ID1_Value"] = randomText
  editCustomer["ID2"] = "ID"
  editCustomer["ID2_State"] = "CO"
  editCustomer["ID2_Value"] = randomText
  editCustomer["Password"] = randomText
  editCustomer["ReferenceRelation"] = "COW"
  editCustomer["ReferenceName"] = "Reference_Name" + randomText
  editCustomer["ReferencePrimaryPhone"] = ReferencePrimaryPhone
  editCustomer["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
  editCustomer["ReferenceAddress1"] = "512 N Main St"
  editCustomer["ReferenceCity"] = "Anytown"
  editCustomer["ReferenceState"] = "MA"
  editCustomer["ReferenceZip"] = "82000"
  editCustomer["ReferencePrimaryPhoneTypeID"] = "FAX"
  editCustomer["ReferenceSecondaryPhoneTypeID"] = "BUS"
  editCustomer["EmployerName"] = "pat"
  editCustomer["Department"] = "Sales"
  editCustomer["Position"] = "Sales"
  editCustomer["WorkPhone"] = "8013611133"
  editCustomer["WorkPhoneExtention"] = "1123"
  editCustomer["ReferenceZipCode"] = "84056"
  editCustomer["Supervisor"] = "Howard Ash"
  editCustomer["SupervisorPhone"] = "8013611155"
  editCustomer["SupervisorPhoneExtention"] = "1132"
  editCustomer["PayPeriod"] = "Weekly"
  editCustomer["GrossPay"] = "4001"
  editCustomer["NetPay"] = "3001"
  editCustomer["Garnishment"] = "1001"
  editCustomer["WorkStartTime"] = "8:01 AM"
  editCustomer["WorkStopTime"] = "5:01 PM" 
  editCustomer["Overide"] = "~s"
  editCustomer["isRandomType"] = True   
  Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
  return editCustomer
  

def GetUser001():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019  
  cashwise = Sys.Process("Cashwise")
  if (IsTrueUserExist(user001["FirstName"])): 
   randomText = GenerateRandomString(8)
   newUser = {}
   newUser["ID"] = randomText
   newUser["FirstName"] = "First_"+randomText
   newUser["LastName"] = "Last_"+randomText
   newUser["Group"] = "MAN"
   newUser["Password"] = randomText
   newUser["Overide"] = "~s"
   newUser["isRandomType"] = True
   Log.Checkpoint("SUCCESS:  GetUser001 is successful")
  else :
   newUser = user001      
  return newUser

def GetUser002():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019
  cashwise = Sys.Process("Cashwise")
  user =  user002
  Log.Checkpoint("SUCCESS:  GetUser002 is successful")
  return user

def IsTrueUserExist(name):
  #Author Pat Holman 5/2/2019
  #Last Modified by Pat Holman 4/24/2019  
  cashwise = Sys.Process("Cashwise")
  SelectSearchUsersByType("Name")
  searchInputField = cashwise.frmUserManageCheck.pnlSearch.isMain
  searchInputField.Keys(name)
  editButton = cashwise.frmUserManageCheck.btnEdit
  editButton.Click()
  userEditForm = cashwise.frmUserEdit
  firstNameInput = userEditForm.edFirstName
  firstNameInput.Click()   
  if firstNameInput.wText == name:
    userEditForm.btnClose.Click()
    Log.Checkpoint("SUCCESS: IsTrueUserExist is successful")
    return True
  else:
    userEditForm.btnClose.Click()
    Log.Checkpoint("FAIL: IsTrueUserExist did not find a user")
    return False
  Indicator.PopText()   

def SelectSearchUsersByType(searchType):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 4/24/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SelectSearchUsersByType(searchType)")
  cashwise.frmUserManageCheck.pnlSearch.icMain.TAdvancedSpeedButton.Click()
  typeSelectWindow = cashwise.frmAbstractList  
  searchTypeList = typeSelectWindow.pnlMain.dbgMain
  MousePointerReset()
  if searchType == 'Name':
    searchTypeList.Click(4, 80)
  elif searchType == "Password":
    searchTypeList.Click(4, 60)
  elif searchType == "Group_ID":
    searchTypeList.Click(4, 40)
  else :
    searchTypeList.Click(4, 20)  
  typeSelectWindow.btnSave.Click()  
  Log.Checkpoint("SUCCESS: SelectSearchUsersByType is successful")
  Indicator.PopText()
    
def MousePointerReset():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  tfrmAbstractList = cashwise.frmAbstractList
  browseGrid = tfrmAbstractList.pnlMain.dbgMain
  browseGrid.HScroll.Pos = 1
  browseGrid.VScroll.Pos = 1
  Log.Checkpoint("SUCCESS: MousePointerReset is successful")



def GetTempContact():
  #Author Phil Ivey 8/4/2019
  #Last Modified by Phil Ivey 8/4/2019  
  locationID = GetLocationIDFromfrmMainWndCap()
  cashwise = Sys.Process("Cashwise")
  firstName = GetRandomFirstName()
  lastName = GetRandomLastName()
  randomText = GenerateRandomString(8)
  locID = GetLocationIDFromfrmMainWndCap()
  randomMiddleInitial = GenerateRandomString(1)
  randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
  randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
  ReferencePrimaryPhone = GenerateRandomPhoneNumber()
  randomSSN = GenerateRandomSSN()
  today = aqDateTime.Today()
  birthDay = GetRandomBirthDate()
  zip = GetRandomZip()
  city = GetCityFromZip(zip)
  address1 = GetRamdomAddressInCity(city)
  state = GetStateFromZip(zip)
  email = lastName+firstName+"@ivey.pro"
  employerName = GetRandomCustomerEmplorerName()
  department = GetRandomDepartment()
  position = GetRandomPostiion(department)
  payPeriod = RandomPayFreq()
  if payPeriod == "M":
    payPeriod = "Monthly"
  elif payPeriod == "S":
    payPeriod = "Semi-Monthly"
  elif payPeriod == "B":
    payPeriod = "Bi-Weekly"
  elif payPeriod == "W":
    payPeriod = "Weekly"
  payPeriod = "Bi-Weekly"
  newContact = {}
  newContact["FirstName"] = firstName
  newContact["LastName"] = lastName
  newContact["MiddleInitial"] = randomMiddleInitial
  newContact["FullName"] = lastName+", "+firstName
  newContact["PrimaryPhone"] = randomPrimaryPhoneNumber
  newContact["PrimaryPhoneType"] = "HOM"
  newContact["SecondaryPhone"] = randomSecondaryPhoneNumber
  newContact["SecondaryPhoneType"] = "CEL"
  newContact["SSN"] = randomSSN
  newContact["AddressType"] = "HOM"
  newContact["Address1"] = address1
  newContact["Address2"] = "Apt. 312"  
  newContact["City"] = city  
  newContact["State"] = state 
  newContact["ZipCode"] = zip
  newContact["BirthDate"] = birthDay  
  newContact["Ethnicity"] = "CC" 
  newContact["PrimaryLanguage"] = "ENG" 
  newContact["Height"] = "511" 
  newContact["Weight"] = "200" 
  newContact["EyeColor"] = "BRN" 
  newContact["HairColor"] = "BRN" 
  newContact["Gender"] = "Male" 
  newContact["MarketingID"] = "TV" 
  newContact["Email"] = email
  newContact["ID1"] = "ID"
  newContact["ID1_State"] = state
  newContact["ID1_Value"] = randomText
  newContact["ID2"] = "DL"
  newContact["ID2_State"] = "UT"
  newContact["ID2_Value"] = randomText
  newContact["Password"] = randomText
  newContact["ReferenceRelation"] = "OTH"
  newContact["ReferenceName"] = "Reference_Name" + randomText
  newContact["ReferencePrimaryPhone"] = ReferencePrimaryPhone
  newContact["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
  newContact["ReferenceAddress1"] = "512 N 200 E"
  newContact["ReferenceCity"] = "Orem"
  newContact["ReferenceState"] = "UT"
  newContact["EmployerName"] = employerName
  newContact["Department"] = department
  newContact["Position"] = position
  newContact["WorkPhone"] = "8013611123"
  newContact["WorkPhoneExtention"] = "1123"
  newContact["ReferenceZipCode"] = "84057"
  newContact["Supervisor"] = "RaNay Ash"
  newContact["SupervisorPhone"] = "8013611122"
  newContact["SupervisorPhoneExtention"] = "1122"
  newContact["PayPeriod"] = payPeriod
  newContact["GrossPay"] = "4000"
  newContact["NetPay"] = "3000"
  newContact["Garnishment"] = "1000"
  newContact["WorkStartTime"] = "8:00 AM"
  newContact["WorkStopTime"] = "5:00 PM"
  newContact["Overide"] = "~s"
  newContact["isRandomType"] = True  
  newContact["locationID"] = locationID
  newContact["cusID"] = ""
  Log.Message("Customer Name = "+newContact["FullName"]) 
  Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
  return newContact
  
  
def GetTempContact2():
  cashwise = Sys.Process("Cashwise")
  #firstName = GenerateRandomNumber(5)
  randomText = GenerateRandomString(8)
  randomMiddleInitial = GenerateRandomString(1)
  randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
  randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
  ReferencePrimaryPhone = GenerateRandomPhoneNumber()
  randomSSN = GenerateRandomSSN()
  today = aqDateTime.Today()
  newContact = {}
  #newContact["ID"] = randomText
  newContact["FirstName"] = randomText +  "_FirstName"
  newContact["LastName"] = randomText +  "_LastName"
  newContact["MiddleInitial"] = randomMiddleInitial
  newContact["PrimaryPhone"] = randomPrimaryPhoneNumber
  newContact["PrimaryPhoneType"] = "HOM"
  newContact["SecondaryPhone"] = randomSecondaryPhoneNumber
  newContact["SecondaryPhoneType"] = "CEL"
  newContact["SSN"] = randomSSN
  newContact["AddressType"] = "HOM"
  newContact["Address1"] = "410 N Main St"
  newContact["Address2"] = "Apt. 312"  
  newContact["City"] = "Orem"  
  newContact["State"] = "UT"  
  newContact["ZipCode"] = "84057"
  newContact["BirthDate"] = "02/23/1960"  
  newContact["Ethnicity"] = "CC" 
  newContact["PrimaryLanguage"] = "ENG" 
  newContact["Height"] = "511" 
  newContact["Weight"] = "200" 
  newContact["EyeColor"] = "BRN" 
  newContact["HairColor"] = "BRN" 
  newContact["Gender"] = "Male" 
  newContact["MarketingID"] = "TV" 
  newContact["Email"] = randomText +  "@msgnext.com"
  newContact["ID1"] = "ID"
  newContact["ID1_State"] = "UT"
  newContact["ID1_Value"] = randomText
  newContact["ID2"] = "DL"
  newContact["ID2_State"] = "UT"
  newContact["ID2_Value"] = randomText
  newContact["Password"] = randomText
  newContact["ReferenceRelation"] = "OTH"
  newContact["ReferenceName"] = "Reference_Name" + randomText
  newContact["ReferencePrimaryPhone"] = ReferencePrimaryPhone
  newContact["ReferenceEmail"] = "reference." + randomText + "@msgnext.com"
  newContact["ReferenceAddress1"] = "512 N 200 E"
  newContact["ReferenceCity"] = "Orem"
  newContact["ReferenceState"] = "UT"
  newContact["EmployerName"] = "Fidelity Investments"
  newContact["Department"] = "Engineering"
  newContact["Position"] = "HR"
  newContact["WorkPhone"] = "8013611123"
  newContact["WorkPhoneExtention"] = "1123"
  newContact["ReferenceZipCode"] = "84057"
  newContact["Supervisor"] = "RaNay Ash"
  newContact["SupervisorPhone"] = "8013611122"
  newContact["SupervisorPhoneExtention"] = "1122"
  newContact["PayPeriod"] = "Monthly"
  newContact["GrossPay"] = "4000"
  newContact["NetPay"] = "3000"
  newContact["Garnishment"] = "1000"
  newContact["WorkStartTime"] = "8:00 AM"
  newContact["WorkStopTime"] = "5:00 PM"
  newContact["Overide"] = "~s"
  newContact["isRandomType"] = True   
  Log.Checkpoint("SUCCESS:  GetTempUser() is successful")
  return newContact
    
def GetCustomer():
  cashwise = Sys.Process("Cashwise")
  firstName = GetRandomFirstName()
  lastName = GetRandomLastName()
  name = lastName + ", " + firstName
  randomText = GenerateRandomString(8)
  randomMiddleInitial = GenerateRandomString(1)
  randomPrimaryPhoneNumber = GenerateRandomPhoneNumber()
  randomSecondaryPhoneNumber = GenerateRandomPhoneNumber()
  ReferencePrimaryPhone = GenerateRandomPhoneNumber()
  randomSSN = GenerateRandomSSN()
  today = aqDateTime.Today()
  birthDay = GetRandomBirthDate()
  zip = GetRandomZip()
  city = GetCityFromZip(zip)
  address = GetAddressFromCity(city)
  state = GetStateFromZip(zip)
  payPeriod = RandomPayFreq()
 # email = lastName+firstName+"@ivey.pro"
  email = lastName+firstName+"@msgnext.com"
  locCustomer = {}
  #locCustomer["ID"] = randomText
  locCustomer["FirstName"] = firstName
  locCustomer["LastName"] = lastName
  locCustomer["MiddleInitial"] = randomMiddleInitial
  locCustomer["FullName"] = name
  locCustomer["PrimaryPhone"] = randomPrimaryPhoneNumber
  locCustomer["PrimaryPhoneType"] = "HOM"
  locCustomer["SecondaryPhone"] = randomSecondaryPhoneNumber
  locCustomer["SecondaryPhoneType"] = "CEL"
  locCustomer["SSN"] = randomSSN
  locCustomer["AddressType"] = "HOM"
  locCustomer["Address1"] = address
  locCustomer["Address2"] = ""  
  locCustomer["City"] = city  
  locCustomer["State"] = state  
  locCustomer["ZipCode"] = zip
  locCustomer["BirthDate"] = birthDay  
  locCustomer["Ethnicity"] = "CC" 
  locCustomer["PrimaryLanguage"] = "ENG" 
  locCustomer["Height"] = "511" 
  locCustomer["Weight"] = "200" 
  locCustomer["EyeColor"] = "BRN" 
  locCustomer["HairColor"] = "BRN" 
  locCustomer["Gender"] = "Male" 
  locCustomer["MarketingID"] = "TV" 
  locCustomer["Email"] = email
  locCustomer["ID1"] = "ID"
  locCustomer["ID1_State"] = "UT"
  locCustomer["ID1_Value"] = randomText
  locCustomer["ID2"] = "DL"
  locCustomer["ID2_State"] = "UT"
  locCustomer["ID2_Value"] = randomText
  locCustomer["Password"] = randomText
  locCustomer["ReferenceRelation"] = "OTH"
  locCustomer["ReferenceName"] = "Reference_Name" + randomText
  locCustomer["ReferencePrimaryPhone"] = ReferencePrimaryPhone
  locCustomer["ReferenceEmail"] = "reference." + randomText + "@ivey.pro"
  locCustomer["ReferenceAddress1"] = "512 N 200 E"
  locCustomer["ReferenceCity"] = "Orem"
  locCustomer["ReferenceState"] = "UT"
  locCustomer["EmployerName"] = "Fidelity Investments"
  locCustomer["Department"] = "Engineering"
  locCustomer["Position"] = "HR"
  locCustomer["WorkPhone"] = "8013611123"
  locCustomer["WorkPhoneExtention"] = "1123"
  locCustomer["ReferenceZipCode"] = "84057"
  locCustomer["Supervisor"] = "RaNay Ash"
  locCustomer["SupervisorPhone"] = "8013611122"
  locCustomer["SupervisorPhoneExtention"] = "1122"
  
  locCustomer["PayPeriod"] = "weekly"
  #locCustomer["PayPeriod"] = GetRadomPayPeriod()
  
  locCustomer["GrossPay"] = "4000"
  locCustomer["NetPay"] = "3000"
  locCustomer["Garnishment"] = "1000"
  locCustomer["WorkStartTime"] = "8:00 AM"
  locCustomer["WorkStopTime"] = "5:00 PM"
  locCustomer["Overide"] = "~s"
  locCustomer["isRandomType"] = True   
  Log.Checkpoint("SUCCESS:  GetLOCCustomer() is successful")
  return locCustomer
  
def RandomPayFreq():
  cashwise = Sys.Process("Cashwise")
  num = random.randint(0,3)
  Log.Message(num)
  if(num == 0):  
    Log.Message("PayFreq = M")
    return "M"
  if(num == 1):
    Log.Message("PayFreq = S")
    return "S"
  if(num == 2):
    Log.Message("PayFreq = W")
    return "W"
  if(num == 3):
    Log.Message("PayFreq = B")
    return "B"
    
def GetRadomPayPeriod():
  #Author Phil Ivey 7/31/2020
  #Last Modified by   
    payPeriod = randrange(1,5)  
    Log.Message(payPeriod)
    if payPeriod == 1:
      interval  = "biweekly"
    elif payPeriod == 2:
      interval  = "semi-monthly"
    elif payPeriod == 3:
      interval  = "monthlyday"
    elif payPeriod == 4:
      interval  = "monthlythe"
    elif payPeriod == 5:
      interval  = "weekly"
      
    return interval