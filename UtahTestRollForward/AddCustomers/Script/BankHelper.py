﻿from BasePage import *
from CashHelper import *

def OpenCustomerAccount():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenCustomerAccount()")
  cashwise.frmCustomerEdit.Keys("~[Release]ta")
  #cashwise.frmCustomerEdit.MainMenu.Click("[2]|[1]")
  Log.Checkpoint("SUCCESS: OpenCustomerAccount() is successful.")
  Indicator.PopText()

def BankClickAddButton():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In BankClickAddButton()")
  cashwise.frmContactAccountList.btnAdd.Click()
  Log.Checkpoint("SUCCESS: BankClickAddButton() is successful.")
  Indicator.PopText()

def OpenBankAccountBrowser():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenBankAccountBrowser()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TAdvancedSpeedButton.Click()
  Log.Checkpoint("SUCCESS: OpenBankAccountBrowser() is successful.")
  Indicator.PopText()

def AddNewAccountNumber():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In AddNewAccountNumber()")
  accountNumber = "041212815" + GenerateRandomNumberString(10)
  Delay(500)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,1200).Exists):
      cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
  Delay(500) 
  #cashwise.frmContactAccountEdit.Click()
 # cashwise.frmContactAccountList.btnAdd.Click()
  
 
  customDBEdit = cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TCustomDBEdit
  ExplicitWait(5)
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.Window("TCustomDBEdit", "", 1).Keys(accountNumber)
  #customDBEdit.Keys(accountNumber)
  Log.Checkpoint("SUCCESS: AddNewAccountNumber() is successful.")
  Indicator.PopText()
  


def ClickAddAccountSpeedButton():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In ClickAddAccountSpeedButton()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dblefRoutingAccountNum.TAdvancedSpeedButton.Click(8, 8)
  cashwise.frmBankAccountBrowse.btnAdd.Click(27, 22)
  Log.Checkpoint("SUCCESS: ClickAddAccountSpeedButton() is successful.")
  Indicator.PopText()

def SaveNewAccount():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SaveNewAccount()")
  cashwise.frmBankAccountEdit.pnlMain.sbData.pnlFields.pnlColumn1.pnlRouting.edAcctNum.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SaveNewAccount() is successful.")
  Indicator.PopText()

def SetAccountOpenDate():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In SetAccountOpenDate()")
  cashwise.frmContactAccountEdit.pnlMain.sbData.dbledDateAccountOpened.TAdvancedSpeedButton.Click(10, 4)
  advancedButton = cashwise.frmCalendarEdit.Panel1.btnPriorYear
  advancedButton.Click()
  advancedButton.Keys("[F9]")
  Log.Checkpoint("SUCCESS: SetAccountOpenDate() is successful.")
  Indicator.PopText()

def CloseAddAccountWindows():
  #Author Pat Holman 7/10/2019
  #Last Modified by Pat Holman 7/10/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseAddAccountWindows()")
  cashwise.frmContactAccountEdit.btnSave.Click(68, 11)
  cashwise.frmContactAccountList.btnSave.Click(44, 9)
  cashwise.frmCustomerEdit.btnSave.Click(43, 11)
  cashwise.frmCustomerBrowse.btnSave.Click(34, 14)
  Log.Checkpoint("SUCCESS: CloseAddAccountWindows() is successful.")
  Indicator.PopText()

def Test2():
  cashwise = Sys.Process("Cashwise")
  cashwise.frmContactAccountEdit.btnSave.Click(68, 11)
  cashwise.frmContactAccountList.btnSave.Click(44, 9)
  cashwise.frmCustomerEdit.btnSave.Click(43, 11)
  cashwise.frmCustomerBrowse.btnSave.Click(34, 14)