﻿#from LocalConfig import *
#from Helpers import *					 


#def GetApprovedCusWithNoLoan():
#  tableIndex = {}
#  tableIndex["SqlStatement"] = "Select ROW_NUMBER() OVER ( ORDER BY ID,LoanModule_ID ) TheIndex, ID from DeferredType"
#  tableIndex["Coloumn"] = "ID"
#  tableIndex["ColoumnValue"] = "CDF"
#  index = GetTableIndex(tableIndex)
#  Log.Message(index)


def GetTableIndex(tableIndex):
#def GetTableIndex(datatype, *args):
#      if datatype == 'str':
#          database = ''
#          localConfig = GetLocalConfigSettingsDataBase()
#          computer_name = localConfig["Server"]
#          server_name = localConfig["Server"]
#          database_name = database
#      else:
   localConfig = GetLocalConfigSettingsDataBase()
   computer_name = localConfig["Server"]
   server_name = localConfig["Server"]
   database_name = localConfig["Database"]
   if(database_name == "UtahTest"):
           pw = "pa$$0ut"
   else:
           pw = "softwise"
  
   index = 0    
   theIndex = None
   Log.Checkpoint(tableIndex["SqlStatement"])
   Log.Checkpoint(tableIndex["Coloumn"])
   Log.Checkpoint(tableIndex["ColoumnValue"])
    # Create a Connection object
   AConnection = ADO.CreateADOConnection()
    # Specify the connection string
   AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
    # Suppress the login dialog box
   AConnection.LoginPrompt = False
   AConnection.Open()
    # Execute a simple query
   RecSet = AConnection.Execute_(tableIndex["SqlStatement"])
    # Iterate through query results and insert data into the test log
   RecSet.MoveFirst()
   while not RecSet.EOF and theIndex == None:
       Log.Message(RecSet.Fields.Item[tableIndex["Coloumn"]]);
       coloumnValue = RecSet.Fields.Item[tableIndex["Coloumn"]].Value
       if coloumnValue != tableIndex["ColoumnValue"]:
         index = index +1
         RecSet.MoveNext()
       else:
         theIndex = aqConvert.IntToStr(index)
         Log.Message("the index for "+tableIndex["ColoumnValue"]+" is "+aqConvert.IntToStr(theIndex))
   AConnection.Close()
   return theIndex
      



def RunSingleQuery(aQuery,columnName):
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
 #   dbConnection = DatabaseValue("Ca")  # connectionValues["Server"],    ["Database"] , ["User"], ["Pass"]
#  server_name = dbConnection["Server"]
#    database_name = dbConnection["Database"]
    
 # localConfig = GetLocalConfigSettings()
  server_name ="ccqastgsql1.checkcity.local "
  database_name = "Utah3"
  #computer_name = "ccc7sqltest.checkcity.local\ccc7sql2017"
  pw = "pa$$0ut"

          
  Log.Message("Run Single Query = "+aQuery)
  AConnection = ADO.CreateADOConnection()
    # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+""
  Log.Message("Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+"")
    # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
    # Iterate through query results and insert data into the test log
  
  if(RecSet.EOF != True):
      RecSet.MoveFirst()
  
      while not RecSet.EOF:
        returnName = RecSet.Fields.Item[columnName].Value
        RecSet.MoveNext()
  else:
        returnName = "Empty"
  
  
  AConnection.Close()
  varType = aqObject.GetVarType(returnName)
  if varType == 0:
      returnName = "Empty"
  Log.Message(returnName)
  return returnName

def RunSingleQueryDatabase(aQuery,columnName):
  #Author Phil Ivey 6/25/2019
   #Last Modified by Phil Ivey 6/25/2019
     dbConnection = DatabaseValue("Ca")  # connectionValues["Server"],    ["Database"] , ["User"], ["Pass"]
  # localConfig = GetLocalConfigSettings()
  #   computer_name = dbConnection["Server"]
   #computer_name = "ccc7sqltest.checkcity.local\ccc7sql2017"
     server_name = dbConnection["Server"]
     database_name = dbConnection["Database"]
     if(database_name == "Utah3"):
         pw = "pa$$0ut"
     else:
         pw = "softwise"
 
           
     Log.Message("Run Single Query = "+aQuery)
     AConnection = ADO.CreateADOConnection()
     # Specify the connection string
     AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+""
     Log.Message("Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+"")
     # Suppress the login dialog box
     AConnection.LoginPrompt = False
     AConnection.Open()
     RecSet = AConnection.Execute_(aQuery)
     # Iterate through query results and insert data into the test log
   
     if(RecSet.EOF != True):
       RecSet.MoveFirst()
   
       while not RecSet.EOF:
         returnName = RecSet.Fields.Item[columnName].Value
         RecSet.MoveNext()
     else:
         returnName = "Empty"
   
   
     AConnection.Close()
     varType = aqObject.GetVarType(returnName)
     if varType == 0:
       returnName = "Empty"
     Log.Message(returnName)
     return returnName
    
def GetRandomCompanyAddress(cusState,zipLike):
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
#  cusState = "TX"
#  zipLike = "75%"
  localConfig = GetLocalConfigSettings()
  computer_name = localConfig["Server"]
  #computer_name = "ccc7sqltest.checkcity.local\ccc7sql2017"
  server_name = localConfig["Server"]
  database_name = localConfig["Database"]
  if(database_name == "UtahTest"):
    pw = "pa$$0ut"
  else:
    pw = "softwise"

  companyAddObj = {}
  companyAddObj["CompanyName"] = ""
  companyAddObj["CompanyAddress1"] = ""
  companyAddObj["CompanyPrimaryPhone"] = ""
  companyAddObj["CompanyCity"] = ""
  companyAddObj["CompanyState"] = ""
  companyAddObj["CompanyZip"] = ""
  
  aQuery = "Select Top 1 CompanyName, AddressLine1 CompanyAddress1 ,PrimaryPhone CompanyPrimaryPhone, City CompanyCity,State CompanyState,Zip CompanyZip from Contact where IsEmployer = 1 and CompanyName is not NULL "+\
            " and AddressLine1 is not NULL and PrimaryPhone is not NULL and State = '"+cusState+"' and Zip like '"+zipLike+"' order by NEWID()"
          
  Log.Message(aQuery)
  varReturnArray = ["CompanyName","CompanyAddress1",	"CompanyPrimaryPhone",	"CompanyCity","CompanyState","CompanyZip"]	
  x = len(varReturnArray)
  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
  # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
  # Iterate through query results and insert data into the test log
  try:
         firstMove = RecSet.MoveFirst()
  except Exception as e:
    Log.Message(str(e)+" No Query Results")
    returnField = "Empty"
    return returnField
  i = 0
  while  i < x:
         colName = varReturnArray[i]
         Log.Message(colName)  
         companyAddObj[colName] = RecSet.Fields.Item[colName].Value
         i = i + 1
  AConnection.Close()
  Log.Message( companyAddObj["CompanyName"])
  Log.Message( companyAddObj["CompanyAddress1"])
  Log.Message( companyAddObj["CompanyPrimaryPhone"])
  Log.Message( companyAddObj["CompanyCity"])
  Log.Message( companyAddObj["CompanyState"])
  Log.Message( companyAddObj["CompanyZip"])
 
  return companyAddObj

def GetSinglePayWithACH():
  aQuery = "Select top 1 DT.ID LoanType from DeferredType DT left join LoanType LT on DT.ID = LT.ID and DT.LoanModule_ID = LT.LoanModule_ID " +\
    "where DT.LoanModule_ID = 'DEF' and LT.LoanModule_ID = 'DEF' AND DT.Default_CollateralType_ID = 'ACH' AND (SELECT dbo.LoanTypeDisplayGetCollateralTypes(LT.ID))  = 'ACH' " +\
    "order by DT.ID"
  columnName = "ID"
  singleType = RunSingleQuery(aQuery,columnName)
  Log.Message("single payment loan type is "+singleType)
  return singleType

def GetTotalInterestFromSchedule(loanID):
  #loanID = "001-4882645"
  aQuery = "select ABS(SUM(InterestPayment)) TotalInterest from InstallmentLoanSchedule where Loan_ID = '"+loanID+"'"
  columnName = "TotalInterest"
  totalInterest = RunSingleQuery(aQuery,columnName)
  return totalInterest  
    
def GetAppCusNoLoan(payInterval):
     aQuery = "select Distinct Top 1 C.ID from Contact C " +\
     "join (select * from [benusql01.ccoc7test.com].[CCOAlternate].[dbo].[storelogin] ) ccoAlt on c.ID = ccoAlt.ccoid and C.Birthday = ccoAlt.birthdate and C.Location_ID = 'UTO' " +\
     "left join (select Contact_ID, count(RoutingAccountNum) NumOfAccounts from ContactAccount where RoutingAccountNum is not NULL group by Contact_ID) cAtt on C.ID = cAtt.Contact_ID left join ContactEmployer CE on cAtt.Contact_ID = C.ID and C.CompanyName is not NULL and CE.GrossPay is not NULL and CE.IntervalType_ID is not NULL " +\
     "where C.IsCustomer = 1  and C.FirstName is not NULL and C.LastName is not NULL and SSN is not NULL and C.IsCustomer = 1 and C.PrimaryPhone is not NULL and C.AddressLine1 is not NULL and C.City is not NULL and C.State is not NULL and C.Zip is not Null " +\
			"and C.Birthday is not NULL and  exists (select Type_ID from ContactIdentification ci WHERE ci.Contact_ID = C.ID and Type_ID = 'DL') " +\
			"and ccoAlt.email not like '%checkcity%' and ccoAlt.email not like '%softwise%' " +\
			"and  exists (select Value from ContactIdentification ci WHERE ci.Contact_ID = C.ID and Type_ID = 'DL') and  exists (select State from ContactIdentification ci WHERE ci.Contact_ID = C.ID and Type_ID = 'DL') " +\
			"and  exists (select Phone from ContactEmployer ce WHERE ce.Contact_ID = C.ID) and  exists (select 1 from ContactEmployer ce WHERE ce.Contact_ID = C.ID and IntervalType_ID = '"+payInterval+"') " +\
			"and exists (select Contact_ID from ContactAccount ca where ca.Contact_ID = C.ID group by Contact_ID having Count(*) = 1) and exists (select Contact_ID, Count(*) numOfJobs from ContactEmployer ce where ce.Contact_ID = C.ID group by Contact_ID having Count(*) = 1) " +\
			"and  exists (select 1 from ContactAccount ca WHERE ca.Contact_ID = C.ID) and not exists (Select 1 from Turndown T where T.Customer_ID = C.ID) " +\
			"and not exists (Select 1 from DebtorDebt dd join Debt d on dd.Debt_ID = d.ID where dd.IsActive = 1 and d.Amount > 0 and dd.Debtor_ID = C.ID)  " +\
			"and not exists (Select 1 from Deferred d where d.AmountDue > 0 and d.Customer_ID = C.ID) and C.LastName = 'Approved'"
     columnName = "ID"
     cusID = RunSingleQueryDatabase(aQuery,columnName)
     return cusID
  
def GetRandomFirstName():
  aQuery = "Select top 1 FirstName from QATestData..FirstLastName order by NewID()"
  columnName = "Firstname"
  firstName = QueryTestData(aQuery,columnName)
  return firstName  
   
def GetDeltaFeesRateCurrentProposal():
  aQuery = "Select DeltaFeesRate from DeferredProposal where Deferred_ID in (Select top 1 ID from Deferred where Location_ID = '001' and ID like '%001-%' order by ID desc)"
  columnName = "DeltaFeesRate"
  feeRate = RunSingleQuery(aQuery,columnName)
  return feeRate
  
def GetRandomLastName():
  aQuery = "Select top 1 LastName from QATestData..FirstLastName order by NewID()"
  columnName = "LastName"
  lastName = QueryTestData(aQuery,columnName)
  return lastName
  
def IsNameUnique(firstName,lastName):
#  firstName = "Don"
#  lastName = "Hill"
  
  name = lastName+' ,'+firstName
  aQuery = "Select Name from Contact where Name like '%"+name+"%'"
  columnName = "Name"
  returnName = RunSingleQuery(aQuery,columnName)
  Log.Message(returnName)
  if(returnName == "Empty"):
    return True
  else:
    return False
    
def IsSSNUnique(newSSN):
  #newSSN = "039-70-0725"
  aQuery = "Select SSN from Contact where SSN = '"+newSSN+"'"
  columnName = "SSN"
  returnName = RunSingleQuery(aQuery,columnName)
  Log.Message(returnName)
  if(returnName == "Empty"):
    return True
  else:
    return False    
    
 
def GetRandomBirthDate(): 
  aQuery = "select CONVERT(varchar, DATEADD(day, -(select ROUND(((29254 - 6939 -1) * RAND() + 6939), 0)), GETDATE()),101) BirthDate"
  columnName = "BirthDate"
  birthDate = RunSingleQuery(aQuery,columnName)
  return birthDate

 
def GetRandomZip(cusState,zipLike):
#  cusState = "TX"
#  zipLike = "75%"
  aQuery = "Select Top 1 Zip from Contact where State = '"+cusState+"' and Zip like '"+zipLike+"' order by NewID()"
  columnName = "Zip"
  zip = RunSingleQuery(aQuery,columnName)
  returnZip = aqString.SubString(zip,0,5)
  return returnZip

def GetRandomMonthlyPay():
  aQuery = "select ROUND(((6000 - 2500 -1) * RAND() + 2500), 0) MonthlyPay"
  columnName = "MonthlyPay"
  monthlyPay = RunSingleQuery(aQuery,columnName)
  return monthlyPay

def GetRandomRouteNum():
  aQuery = "select top 1 RoutingID from Bank where LEN(trim(RoutingID)) = 9 and Name is not Null and AddressLine1 is not NULL and City is not NULL " +\
            "and State is not NULL and PrimaryPhone is not NULL   order by NEWID()"
  columnName = "RoutingID"
  routeNum = RunSingleQuery(aQuery,columnName)
  return routeNum
 

   
def GetRandomAddressWithCityState(city,state):
#  city = "BayTown"
#  state = "TX"
  aQuery = "select Top 1 AddressLine1 from Contact where IsCustomer = 1 and City = '"+city+"' and State = '"+state+"' and AddressLine1 is not NULL order by NEWID()"
  columnName = "AddressLine1"
  cusAddress = RunSingleQuery(aQuery,columnName)
  return cusAddress
  
def GetRandomAcctNum():
      #Author Phil 10/29/2019
      #Last Modified by Phil 10/29/2019 
  aQuery = "select convert(numeric(12,0),rand() * 899999999999) + 100000000000 AcctNum"
  columnName = "AcctNum"
  acctNum = RunSingleQuery(aQuery,columnName)
  return aqConvert.FloatToStr(acctNum)
  
def GetFutureDate():
  aQuery = "select convert(varchar,(DATEADD(day, 650, GETDATE())),101) FutureDate"
  columnName = "FutureDate"
  futureDate = RunSingleQuery(aQuery,columnName)
  return futureDate      
 
def GetNewSSN():
  p1 = "237"
  aQuery1 = "select convert(numeric(12,0),rand() * 89) + 10 P2"
  p2 = RunSingleQuery(aQuery1,"P2") 
  aQuery2 = "select convert(numeric(12,0),rand() * 8999) + 1000 P3"
  p3 = RunSingleQuery(aQuery2,"P3") 
  p2 = aqConvert.FloatToStr(p2)
  p3 = aqConvert.FloatToStr(p3)
  newSSN = p1+"-"+p2+"-"+p3
  Log.Message("New SSN is "+newSSN)
  return newSSN
    
   
def QueryTestData(aQuery,columnName):
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
  localConfig = GetLocalConfigSettings()
  computer_name = localConfig["Server"]
  #computer_name = "ccc7sqltest.checkcity.local\ccc7sql2017"
  server_name = localConfig["Server"]
  database_name = "QATestData"
  if(database_name == "QATestData"):
    pw = "pa$$0ut"
  else:
    pw = "softwise"

          
  Log.Message("QueryTestData = "+aQuery)
  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
  # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
  # Iterate through query results and insert data into the test log
  
  if(RecSet.EOF != True):
    RecSet.MoveFirst()
  
    while not RecSet.EOF:
      returnName = RecSet.Fields.Item[columnName].Value
      RecSet.MoveNext()
  else:
      returnName = "Empty"
  
  
  AConnection.Close()
  varType = aqObject.GetVarType(returnName)
  if varType == 0:
    returnName = "Empty"
  Log.Message(returnName)
  return returnName
  
  
def RunUpdate(anUpDate):
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
  localConfig = GetLocalConfigSettings()
  computer_name = localConfig["Server"]
  server_name = localConfig["Server"]
  database_name = localConfig["Database"]
  locID = GetLocationIDFromfrmMainWndCap()
  CreateConnectionSession(locID)
  Log.Message("Run Update = "+anUpDate)
  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
  # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  AConnection.Execute_(anUpDate)
  # Iterate through query results and insert data into the test log
  
  AConnection.Close()
  Log.Message("End of Update")
  
  
def SingleUser():
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
  computer_name =  "ccc7sqltest.checkcity.local\ccc7sql2017"
  server_name =  "ccc7sqltest.checkcity.local\ccc7sql2017"
  database_name ="master"
  killUtatTestCon = "UtatTest"
  script = "ALTER DATABASE [UtahTest] SET SINGLE_USER WITH ROLLBACK IMMEDIATE"
  
  CreateConnectionSession("001")

  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password=pa$$0ut;Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
  # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  AConnection.Execute_(script)
  # Iterate through query results and insert data into the test log
  
  AConnection.Close()
  Log.Message("End of Update")
  
def MULTI_USER():
 #Author Phil Ivey 6/25/2019
  #Last Modified by Phil Ivey 6/25/2019
  computer_name =  "ccc7sqltest.checkcity.local\ccc7sql2017"
  server_name =  "ccc7sqltest.checkcity.local\ccc7sql2017"
  database_name ="master"
  killUtatTestCon = "UtatTest"
  script = "ALTER DATABASE [UtahTest] SET MULTI_USER WITH ROLLBACK IMMEDIATE"
  
  CreateConnectionSession("001")

  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password=pa$$0ut;Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
  # Suppress the login dialog box
  AConnection.LoginPrompt = False
  AConnection.Open()
  AConnection.Execute_(script)
  # Iterate through query results and insert data into the test log
  
  AConnection.Close()
  Log.Message("End of Update")
  
    
def CreateConnectionSession(locationID):
     #Author Phil Ivey 7/03/2019
    #Last Modified by Phil Ivey 7/03/2019
     userName = "OWN"
     #loactionID = "001"
  
     databaseName = "UtahTestRefreshTest2"
     serverName = "ccc7sqltest.checkcity.local\ccc7sql2017"
     SProc = ADO.CreateADOStoredProc()
     SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=pa$$0ut;"
     #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
     Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
     Log.Message(Cstring)
     #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
     SProc.ProcedureName = "dbo.SessionCreateAPI"
     
     # Adding a return parameter 
  #   SProc.Parameters.AddParameter()
  #   SProc.Parameters.Items[0].name = "RETURN_VALUE"
  #   SProc.Parameters.Items[0].DataType = ftInteger 
  #   SProc.Parameters.Items[0].Direction = pdReturnValue
  #   SProc.Parameters.Items[0].Value = None
     
     
     # Adding an in parameter: 
     SProc.Parameters.AddParameter()
     SProc.Parameters.Items[0].name = "@UserName"
     SProc.Parameters.Items[0].DataType = ftFixedChar
     SProc.Parameters.Items[0].Size = 5
     #SProc.Parameters.Items[0].Direction = pdInput
     SProc.Parameters.Items[0].Value = userName
     # Adding an in parameter: 
     SProc.Parameters.AddParameter()
     SProc.Parameters.Items[1].name = "@LocationID"
     SProc.Parameters.Items[1].DataType = ftFixedChar
     SProc.Parameters.Items[1].Size = 4
     #SProc.Parameters.Items[1].Direction = pdInput
     SProc.Parameters.Items[1].Value = locationID
     # Adding an out parameter  
     SProc.Parameters.AddParameter()
     SProc.Parameters.Items[2].name = "@ComputerName"
     SProc.Parameters.Items[2].DataType = ftFixedChar
     SProc.Parameters.Items[2].Size = 32
     #SProc.Parameters.Items[2].Direction = adParamOutput  
     SProc.Parameters.Items[2].Value =  None
      # Running the procedure 
      
     SProc.ExecProc()
  #   Log.Message("Bank Account Num is: " + SProc.Parameters.Items[2].Value)
  #   return SProc.Parameters.Items[2].Value
     SProc.Close()

def AddBankAccountToCustomer(cusID,locationID):
  #Author Phil Ivey 8/4/2019
  #Last Modified by Phil Ivey 10/29/2019 
 

#     cusID = "001-0001170"
#     locationID = "001"
     localInfo = GetLocalConfigSettings()
     databaseName = localInfo["Database"]
     serverName = localInfo["Server"]
     SProc2 = ADO.CreateADOStoredProc()
     #SProc2.ConnectionString = "Provider=SQLOLEDB;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
     SProc2.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
     #SProc2.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
     Cstring = "Provider=SQLOLEDB;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
     Log.Message(Cstring)
     #SProc2.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
     SProc2.ProcedureName = "dbo.AddBankAccountToCustomer"
     
     # Adding a return parameter 
  #   SProc2.Parameters.AddParameter()
  #   SProc2.Parameters.Items[0].name = "RETURN_VALUE"
  #   SProc2.Parameters.Items[0].DataType = ftInteger 
  #   SProc2.Parameters.Items[0].Direction = pdReturnValue
  #   SProc2.Parameters.Items[0].Value = None
     
     
     # Adding an in parameter: 
     SProc2.Parameters.AddParameter()
     SProc2.Parameters.Items[0].name = "@ContactID"
     SProc2.Parameters.Items[0].DataType = ftFixedChar
     SProc2.Parameters.Items[0].Size = 14
     SProc2.Parameters.Items[0].Direction = pdInput
     SProc2.Parameters.Items[0].Value = cusID
     # Adding an in parameter: 
     SProc2.Parameters.AddParameter()
     SProc2.Parameters.Items[1].name = "@Location_ID"
     SProc2.Parameters.Items[1].DataType = ftFixedChar
     SProc2.Parameters.Items[1].Size = 5
     SProc2.Parameters.Items[1].Direction = pdInput
     SProc2.Parameters.Items[1].Value = locationID
     # Adding an out parameter  
     SProc2.Parameters.AddParameter()
     SProc2.Parameters.Items[2].name = "@RoutingAccountNum"
     SProc2.Parameters.Items[2].DataType = ftFixedChar
     SProc2.Parameters.Items[2].Size = 31
     SProc2.Parameters.Items[2].Direction = pdOutput  
     #SProc2.Parameters.Items[2].Direction = adParamOutput  
     SProc2.Parameters.Items[2].Value =  None
      # Running the procedure 
     CreateConnectionSession("001")
     SProc2.ExecProc()
     Log.Message("Bank Account Num is: " + SProc2.Parameters.Items[2].Value)
     return SProc2.Parameters.Items[2].Value
     
     
def ExecLOCPaymentPlanSchedule(cusID,proposalNum):
  
#   cusID = "001-0000999"
#   proposalNum = 26
   #proposalNum = aqConvert.StrToInt(proposalNum)
   accountID = GetLocAccountNumber(cusID)    
   locationID = "001"
   paramValue1 = "25"
   paramValue1 = aqConvert.StrToFloat(paramValue1)
   nextPay = GetCusNextPayDate(cusID)   #11/15/2010
   
   #nextPay = aqConvert.DateTimeToFormatStr(nextPay, "%m/%d/%Y")
   #nextPay = aqConvert.StrToDateTime(nextPay)
  
   localInfo = GetLocalConfigSettings()
   databaseName = localInfo["Database"]
   serverName = localInfo["Server"]
   SProc = ADO.CreateADOStoredProc()
   SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName+";" + "Database="+databaseName +";Uid=sa; Pwd=softwise;"
   #SProc.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+databaseName+";Data Source="+serverName+""
   Cstring = "Provider=SQLNCLI11;Server="+serverName+";Database="+databaseName +";Uid=sa; Pwd=softwise;"
   Log.Message(Cstring)
   #SProc.ConnectionString = "Provider=SQLNCLI11;Server="+serverName;+"\"Database="+databaseName +";Uid=sa; Pwd=softwise;\""
   SProc.ProcedureName = "dbo.LOCPercentOfCreditLimit"
   
   # Adding a return parameter 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[0].name = "RETURN_VALUE"
   SProc.Parameters.Items[0].DataType = ftInteger 
   SProc.Parameters.Items[0].Direction = pdReturnValue
   SProc.Parameters.Items[0].Value = None
   
   
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[1].name = "@AccountID"
   SProc.Parameters.Items[1].DataType = ftFixedChar
   SProc.Parameters.Items[1].Size = 14
   SProc.Parameters.Items[1].Direction = adParamInput
   SProc.Parameters.Items[1].Value = accountID
   # Adding an in parameter: 
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[2].name = "@ProposalNumber"
   #SProc.Parameters.Items[2].DataType = ftInteger
   SProc.Parameters.Items[2].DataType = adInteger
   #SProc.Parameters.Items[2].Size = 2
   SProc.Parameters.Items[2].Direction = adParamInput
   SProc.Parameters.Items[2].Value = proposalNum
   
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[3].name = "@StartDate"
   #SProc.Parameters.Items[3].DataType = adDate
   #SProc.Parameters.Items[3].DataType = ftDateTime  
   SProc.Parameters.Items[3].DataType = ftFixedChar
   #SProc.Parameters.Items[3].Size = 10
   SProc.Parameters.Items[3].Direction = adParamInput
   SProc.Parameters.Items[3].Value =  nextPay
   
   SProc.Parameters.AddParameter()
   SProc.Parameters.Items[4].name = "@ParamValue1"
   SProc.Parameters.Items[4].DataType = ftFloat
   #SProc.Parameters.Items[4].Size = 32
   SProc.Parameters.Items[4].Direction = adParamInput
   SProc.Parameters.Items[4].Value =  paramValue1
   
    # Running the procedure 
   CreateConnectionSession(locationID)
   SProc.ExecProc()
   Log.Message("Check LOCPaymentPlanSchedule for account " + accountID)
   Log.Message(SProc.Parameters.Items[0].Value)

def GetLocAccountNumber(cusID):
       #cusID = "001-0000841"
       aQuery = "select ID from LOCAccount where Customer_ID = '"+cusID+"' and Status_ID <> 'CLO'"
       columnName = "ID"
       accountID = RunSingleQuery(aQuery,columnName)
       return accountID   
     
def LoadCustomerObject2(cusID):
      #Author Phil Ivey 7/15/2019
       #Last Modified by Phil Ivey 7/15/2019
       localConfig = GetLocalConfigSettings()
       computer_name = localConfig["Server"]
       server_name = localConfig["Server"]
       database_name = localConfig["Database"]
       
       if cusID == "Empty":
           return cusID

       
       #cusID = "001-0000421"
       aQuery = "select Top 1 " +\
       " c.ID cusID,c.FirstName,c.LastName,IsNull(c.MiddleName,'') MiddleInitial,c.Name FullName,IsNull(c.PrimaryPhone,'') PrimaryPhone,IsNull(cp1.Type_ID,'') PrimaryPhoneType,IsNull((select Phone from ContactPhone where Contact_ID = c.ID and IsSecondary = 1),'') SecondaryPhone " +\
       " ,IsNull((select Type_ID from ContactPhone where Contact_ID = c.ID and IsSecondary = 1),'') SecondaryPhoneType,IsNull(c.SSN,'') SSN " +\
       " ,IsNull((select Type_ID from ContactAddress where Contact_ID = c.ID and IsPrimary = 1),'') AddressType,IsNull(c.AddressLine1,'') Address1,IsNull(c.AddressLine2,'') Address2,IsNull(c.City,'') City,IsNull(c.State,'') State,IsNull(c.Zip,'') ZipCode,cast(c.Birthday as Date) BirthDate " +\
       " ,IsNull(cpr.Description_ID,'') Ethnicity,IsNull(cpr.Primary_Language_ID,'') PrimaryLanguage,IsNull(cpr.Height,'58') Height,IsNull(cpr.Weight,'120') Weight,IsNull(cpr.EyeColor_ID,'') EyeColor,IsNull(cpr.HairColor_ID,'') HairColor,IsNull(cpr.Gender,'') Gender,IsNull(cr.Marketing_ID,'') MarketingID,IsNull(cea.Email,'') Email,IsNull(cid.Type_ID,'') ID1, IsNull(cid.State,'') ID1_State,IsNull(cid.Value,'') ID1_Value " +\
       " ,cii.Type_ID ID2, cii.State ID2_State,cii.Value ID2_Value, crr.Type_ID ReferenceRelation,crr.Name ReferenceName,crr.PrimaryPhone ReferencePrimaryPhone,crr.EmailAddress ReferenceEmail " +\
       " ,crr.Address ReferenceAddress1,crr.City ReferenceCity,crr.State ReferenceState,crr.Zip ReferenceZipCode,(select Name from Contact where ID = ce.Employer_ID) EmployerName, ce.Department,ce.Position,ce.Phone WorkPhone " +\
       " ,ce.Extension WorkPhoneExtention,ce.Supervisor,ce.SupervisorPhone,ce.SupervisorExtension SupervisorPhoneExtention,ce.IntervalType_ID PayPeriod,ce.GrossPay,ce.NetPay,ce.Garnishments Garnishment,ce.WorkStartTime,ce.WorkStopTime " +\
       " From Contact c " +\
       " left join ContactPhone cp1 on c.ID = cp1.Contact_ID and IsPrimary = 1 " +\
       " left join ContactEmployer ce on c.ID = ce.Contact_ID " +\
       " left join ContactPersonal cpr on c.ID = cpr.Contact_ID " +\
       " left join CustomerReferral cr on c.ID = cr.Customer_ID " +\
       " left join ContactEmailAddress cea on c.ID = cea.Contact_ID " +\
       " left join ContactIdentification cid on c.ID = cid.Contact_ID and cid.Type_ID = 'DL' " +\
       " left join ContactIdentification cii on c.ID = cii.Contact_ID and cii.Type_ID = 'ID' " +\
       " left join ContactReference crr on c.ID =  crr.Contact_ID " +\
       " where c.ID = '"+cusID+"'"
       Log.Message(aQuery)
       
       varNameArray = ["cusID","FirstName",	"LastName",	"MiddleInitial",	"FullName",	"PrimaryPhone",	"PrimaryPhoneType",	"SecondaryPhone",	"SecondaryPhoneType",	"SSN",	"AddressType",	"Address1",	"Address2",	"City",	"State",	"ZipCode",	"BirthDate",	"Ethnicity",	"PrimaryLanguage",	"Height",	"Weight",	"EyeColor","HairColor",	"Gender",	"MarketingID",	"Email",	"ID1",	"ID1_State",	"ID1_Value",	"ID2",	"ID2_State",	"ID2_Value",	"ReferenceRelation",	"ReferenceName",	"ReferencePrimaryPhone",	"ReferenceEmail",	"ReferenceAddress1",	"ReferenceCity",	"ReferenceState",	"ReferenceZipCode",	"EmployerName",	"Department",	"Position",	"WorkPhone",	"WorkPhoneExtention",	"Supervisor",	"SupervisorPhone",	"SupervisorPhoneExtention",	"PayPeriod",	"GrossPay","NetPay",	"Garnishment",	"WorkStartTime",	"WorkStopTime"]	
       x = len(varNameArray)
       nameArrayLen = x-1       
       AConnection = ADO.CreateADOConnection()
       # Specify the connection string
       AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password=softwise;Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+computer_name+""
       # Suppress the login dialog box
       AConnection.LoginPrompt = False
       AConnection.Open()
       RecSet = AConnection.Execute_(aQuery)
       newCustomer = {}
       # Iterate through query results and insert data into the test log
       try:
         firstMove = RecSet.MoveFirst()
       except Exception as e:
         Log.Message(str(e)+" No Query Results")
         returnField = "Empty"
         return returnField
       i = 0
       while  i <= nameArrayLen:
         colName = varNameArray[i]
         number = aqConvert.IntToStr(i)
         Log.Message("Loading Value for customer "+number+" "+colName)  
         newCustomer[colName] = RecSet.Fields.Item[colName].Value
         i = i + 1

       AConnection.Close()

       return newCustomer
       

def GetLocationIDFromfrmMainWndCap():

#  str = "Cashwise - OWN - 001 - Alias(PhilUstorySmokeDatabase) - Clocked Out - Saturday, July 5, 2008"
  cashwise = Sys.Process("Cashwise")
  mainPageCaption = cashwise.frmMain.WndCaption
  # mainPageCaption = Sys["Process"]("Cashwise")["Window"]("TfrmMain", "Cashwise -*", 1)["WndCaption"]
  EndPos = aqString.GetLength(mainPageCaption) 
  StartPos = 25 #start  postion
  #ExplicitWait(1)
  Res = aqString.Remove(mainPageCaption, StartPos, EndPos)
        # Log["Message"](Res)

  aqString.ListSeparator = "-"
  NumOfWord = aqString.GetListLength(Res)
  location = ""
  #ExplicitWait(1)
  for x in range(0,NumOfWord):
      if(x == 2):
        location = aqString.GetListItem(Res, x)
 # Log["Message"]("Loction is "+location)
  #ExplicitWait(1)
  location = aqConvert.VarToStr(location)
  location = aqString.Trim( location, aqString.stAll )
  Log.Message("Location is "+location)
  return location

  
def GetLocationSystemDate(locID):
    #Author Phil 7/3/2019
    #Last Modified by Phil 7/3/2019 
   #locID = "001"
   aQuery = "select System_Date from Location where ID = '"+locID+"'"
   columnName = "System_Date"
   locDate = RunSingleQuery(aQuery,columnName)
   locDate = aqConvert.DateTimeToStr(locDate)
   Log.Message("Date for "+locID+" is "+locDate)
   return locDate
   
def GetCusNextPayDate(cusID):
    #Author Phil 7/3/2019
    #Last Modified by Phil 7/3/2019 
   #cusID = "001-0000924"
   aQuery = "Select convert(varchar,NextPayDate,101)NextPayDate from ContactEmployer where Contact_ID = '"+cusID+"'"
   columnName = "NextPayDate"
   payDate = RunSingleQuery(aQuery,columnName)
   Log.Message("Next pay date is "+aqConvert.DateTimeToStr(payDate))
   return payDate
   
   
def GetWebCusEmail(cusID):
  #cusID = "TXO-124901694"
  server_name = "10.127.74.91"
  database_name = "CCOAlternate"
  if(database_name == "UtahTest"):
    pw = "pa$$0ut"
  else:
    pw = "checkcity"

  aQuery = "select email from storelogin where ccoid = '"+cusID+"'"  
  
  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+""
  # Suppress the login dialog box while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 

  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
  # Iterate through query results and insert data into the test log
  
  if(RecSet.EOF != True):
    RecSet.MoveFirst()
  
    while not RecSet.EOF:
      webLoginName = RecSet.Fields.Item["email"].Value
      RecSet.MoveNext()
  else:
      webLoginName = "Empty"
  
  
  AConnection.Close()
  varType = aqObject.GetVarType(webLoginName)
  if varType == 0:
    webLoginName = "Empty"
  Log.Message(webLoginName)
  return webLoginName
 
  
def GetWebCusPassword(cusID):
  #cusID = "TXO-124901694"
  server_name = "10.127.74.91"
  database_name = "CCOAlternate"
  if(database_name == "UtahTest"):
    pw = "pa$$0ut"
  else:
    pw = "checkcity"

  aQuery = "select pwd from storelogin where ccoid = '"+cusID+"'"  
  
  AConnection = ADO.CreateADOConnection()
  # Specify the connection string
  AConnection.ConnectionString = "Provider=SQLOLEDB.1;Password="+pw+";Persist Security Info=True;User ID=sa;Initial Catalog="+database_name+";Data Source="+server_name+""
  # Suppress the login dialog box while(cashwise.WaitWindow("TRavePreviewForm","Report Preview",1,800).Exists): 

  AConnection.LoginPrompt = False
  AConnection.Open()
  RecSet = AConnection.Execute_(aQuery)
  # Iterate through query results and insert data into the test log
  
  if(RecSet.EOF != True):
    RecSet.MoveFirst()
  
    while not RecSet.EOF:
      webLoginPassWord = RecSet.Fields.Item["pwd"].Value
      RecSet.MoveNext()
  else:
      webLoginPassWord = "Empty"
  
  
  AConnection.Close()
  varType = aqObject.GetVarType(webLoginPassWord)
  if varType == 0:
    webLoginPassWord = "Empty"
  Log.Message(webLoginPassWord)
  return webLoginPassWord