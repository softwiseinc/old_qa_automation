﻿import subprocess
from databaseHelper import *

def UtahTestRollForwardToAlLocations():  #<<<<<<<<<<<<<<<<<< ROLL FORWARD TOTHE CURRENT DATE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    #Author Phil Ivey 07/21/2020
    #Last Modified by Phil Ivey--- D:
#--------------------------------------------TestExecute "D:\Cashwise\TrunkNew\Automation\SmokeTest_2017ForExE\ProjectSuite1\CashwiseSmoketest.pjs" /r /p:Cashwise

       if(Sys.WaitProcess("Cashwise").Exists):
         Sys.Process("Cashwise").Close()
       
       Log.Message("Starting Cashwise")
       Delay(500)
#       SingleUser()
#       Delay(800)
#       MULTI_USER()
       Delay(800)
       
       TestedApps.Cashwise.Run()

       locationArray = aqObject.varArray
  
       locationArray = ReadFile()
       numOfItems =  locationArray[0]
       numOfItems = aqConvert.StrToInt(numOfItems)
       numOfItems = numOfItems +1
       for x in range(numOfItems): 
         Log.Message(locationArray[x])
 #**********************************************************************ENTER UTAH TEST DATABASE BELOW      
       StartCashwise("UtahTest","man22","001")
    
       # BELOW IS FOR WHEN THIS IS USED ON UTATHTEST
       toDateFromData = aqDateTime.Today()
#       toDateFromData = aqConvert.DateTimeToFormatStr(today, "%m/%d/%Y") 
       Delay(500)
 #      toDateFromData = "07/23/2020"
       toDateFromData = aqConvert.StrToDate(toDateFromData)
#       toDateFromData = aqConvert.DateTimeToFormatStr(toDateFromData, "%m/%d/%Y") 
#       toDateFromDatad = aqConvert.StrToDate(toDateFromData)
     # CASHWISE IS ON THE MAIN PAGE
    # THE FOR LOOP BELOW GOES TO THE END OF THE FUNTION
       for x  in range(1,numOfItems): 
          currentLocation = locationArray[x]
          ChangeLocation(currentLocation)
          #toDate = aqDateTime.AddDays(toDateFromData,-1)
          Delay(2500)
          if(Sys.Process("Cashwise").WaitWindow("TMessageForm","Warning",1,3800).Exists):
            Sys.Process("Cashwise").Window("TMessageForm", "Warning", 1).VCLObject("OK").Click()
          Delay(2500)
     
          #get current date of current location 
          locationDate = GetLocationDate(currentLocation)
          #if location date is <= toDateFromData  do the following
          locationDate = aqConvert.StrToDate(locationDate)
        
          if(locationDate <  toDateFromData):
          #else skip the following and log the date
      
          
            cashwise = Sys.Process("Cashwise")
            OpenClosePOS()  
         
      
            CloseCashDrawer() 
            RollForwardLOCNoReportsCalendarOpen()
            #RollForwardCalendarOpen()
            startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
            newDate = ConvertStrToDate(startDateAfterRollForword)
            cashwise.frmCalendarEdit.btnSave.Click()
            if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
            	cashwise.Window("TMessageForm", "Warning", 1).VCLObject("Yes").Click()
            
            cashwise.Window("TMessageForm", "Information", 1).OK.Click()
      
            while newDate < toDateFromData:
      
              #SetFocus(cashwise("frmMain"))
              Sleep(100)
              RollForwardLOCNoReportsCalendarOpen()
              Sleep(100)
              startDateAfterRollForword = cashwise.frmCalendarEdit.txtDate.Caption
              newDate = ConvertStrToDate(startDateAfterRollForword)
              if(cashwise.WaitWindow("TfrmCalendarEdit","Please*",1,1500).Exists):
                cashwise.frmCalendarEdit.btnSave.Click()
              if(cashwise.WaitWindow("TMessageForm","Information",1,1800).Exists):
                cashwise.Window("TMessageForm", "Information", 1).OK.Click()
          else:
               Log.Message("Location does not need to roll forward")

       subprocess.call([r'F:\_code\QA\UtahTestRollForward\KillCashwise.bat'])
       
         
       if(Sys.WaitProcess(" TestExecute").Exists): 
         Sys.Process("TestExecute").Close()
       
#*************************************************************************************************************
#*************************************************************************************************************
#*************************************************************************************************************
#*************************************************************************************************************
#*************************************************************************************************************
#*************************************************************************************************************

def ConvertStrToDate(startDateAfterRollForword): #--Converts Sunday, January 14, 2018 to 1/14/2018
 
       # startDateAfterRollForword = Sys.Process"]("Cashwise").VCLObject"]("frmCalendarEdit").VCLObject"]("txtDate").Caption"]
      # startDateAfterRollForword = "Thu Sep 23 2010"
       startDateAfterRollForword = aqString.Replace(startDateAfterRollForword, ",", "")
       aqString.ListSeparator = " "
       NumOfWord = aqString.GetListLength(startDateAfterRollForword)
      
       wordArray = Sys.OleObject["Scripting.Dictionary"]
 
       for x in range(NumOfWord):
           y = aqConvert.IntToStr(x)
           wordArray.Add(y, aqString.GetListItem(startDateAfterRollForword,x))
           s = wordArray.Item[y];
           Log.Message("number = "+aqConvert.IntToStr(x)+"word = "+s) 
       
       dayOfTheWeek = wordArray.Item["0"]
       monthWord = wordArray.Item["1"]
       dayOfMonth = wordArray.Item["2"]
       year = wordArray.Item["3"]
           
       monthWord = MonthToMonth(monthWord)
       monthDate = switchMonthToNumber(monthWord)
  
       startDateAfterRollForwordNum = monthDate+"/"+dayOfMonth+"/"+year
       Log.Message("Converted "+startDateAfterRollForword+" to "+startDateAfterRollForwordNum)
       
       startDateAfterRollForwordNum = aqConvert.StrToDate(startDateAfterRollForwordNum)
       
       return startDateAfterRollForwordNum



def RollForwardLOCNoReportsCalendarOpen():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
  cashwise = Sys.Process("Cashwise")

  delay(300)
  cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,1500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  delay(1000)  
  if not(cashwise.WaitWindow("TfrmClosing","Close / Roll Forward",1,1800).Exists):
     cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
  cashwise.WaitWindow("TfrmClosing","Close / Roll Forward",1,1800)
  cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
  delay(1000)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,2500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
    delay(5000)
  WaitOnCalendar()
  return 0

def OpenClosePOS():
    #Author Pat Holman 7/26/2019
    #Last Modified by Phil Ivey 8/1/2019
    Delay(1800)
    cashwise = Sys.Process("Cashwise")
    cashwise.Find("ObjectIdentifier", "TGraphicButton", 10).Click()
    Delay(500) 
    if(cashwise.WaitWindow("TMessageForm","Information",1,2800).Exists):  #You are not assigned a drawer
              cashwise.Window("TMessageForm", "Information", 1).VCLObject("OK").Click()
              if(cashwise.WaitWindow("TMessageForm","Confirm",1,2800).Exists):
                cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    else:
   
              if(cashwise.WaitWindow("TMessageForm","Confirm",1,2800).Exists):
               Sys.Process("Cashwise").Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
    	

              if(cashwise.WaitWindow("TfrmCashOpenConfirm","Open?",1,2800).Exists):
              	Sys.Process("Cashwise").VCLObject("frmCashOpenConfirm").VCLObject("Button1").Click()
    	
              if(cashwise.WaitWindow("TMessageForm","Cashwise",1,2800).Exists):
                Sys.Process("Cashwise").Window("TMessageForm", "Cashwise", 1).VCLObject("OK").Click()
    	
   # Sys.Process("Cashwise").VCLObject("frmSaleEdit*").VCLObject("btnClose").Click()
    #Sys.Process("Cashwise").VCLObject("frmSaleEdit*").Keys("~s")
    Sys.Process("Cashwise").VCLObject("frmSaleEdit").Keys("~s")
    Delay(500)
    if(cashwise.WaitWindow("TfrmSaleEdit","Sales Transaction",1,2800).Exists):
      Delay(1000)
      Sys.Process("Cashwise").VCLObject("frmSaleEdit*").VCLObject("btnClose").Click()
      
def CloseCashDrawer():
  #Author Pat Holman 7/26/2019
  #Last Modified by Phil Ivey 8/1/2019
  Delay(800)
  cashwise = Sys.Process("Cashwise")
  probablyClosed = None
  cashwise.Find("ObjectIdentifier", "TGraphicButton_6", 10).Click()
  Delay(1500)
  
  if(cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists) :
     cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  #o drawer assigned to user "MAN"
     probablyClosed = True
  
  else:  #probablyClose is not true line up 5
    if(cashwise.WaitWindow("TMessageForm","Cashwise",1,1800).Exists):
     cashwise.Window("TMessageForm", "Cashwise", 1).VCLObject("OK").Click()
  


    cashwise.Find("ObjectIdentifier", "btnCloseCash", 10).Click()  #-----------------------------------------CLOSE CASH
    Delay(800)
    if(cashwise.WaitWindow("TMessageForm","Cashwise",1,1800).Exists):
       cashwise.Window("TMessageForm", "Cashwise", 1).VCLObject("OK").Click()  #--is not a valid user for location OR COMFIRM CASH IS CLOSED
   #UNPROESSES SALES 
    if(cashwise.WaitWindow("#32770","Cashwise",1,1500).Exists):
       cap2 = cashwise.Window("#32770", "Cashwise", 1).Window("Static", "*", 2).WndCaption
       isThere  = aqString.Find(cap2, "unprocessed sale", 0, True)
       if(isThere != -1): 
          DeleteUnprocessSales()
       else:
          Log.Message("No Unprocessed Sales")
    Delay(800)
    #LABOR CLOCK
    if(cashwise.WaitWindow("#32770","Cashwise",1,1500).Exists):
       cap = cashwise.Window("#32770", "Cashwise", 1).Window("Static", "*", 2).WndCaption
       isThere  = aqString.Find(cap, "perform auto-clock", 0, True)
       if(isThere != -1):
          cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
#          cashwise.Find("ObjectIdentifier", "btnCloseCash", 10).Click()   
    cashwise.VCLObject("frmDrawerManager").VCLObject("btnClose").Click()
    Delay(800)
    if(cashwise.WaitWindow("#32770", "Cashwise", 1,1500).Exists):
          Sys.Process("Cashwise").Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
          delay(1000)
  if(cashwise.WaitWindow("TMessageForm", "Cashwise", 1,1800).Exists):
          cashwise.Window("TMessageForm", "Cashwise", 1).VCLObject("OK").Click()
  if(probablyClosed == True):  # back to row 3
         Log.Message("No Login appeared or probably closes")
  delay(1000)
  if(cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists) :
     cap - Sys.Process("Cashwise").Window("#32770", "Cashwise", 1).Window("Static", "No drawer assigned*", 2)
     isThere  = aqString.Find(cap, "No drawer", 0, True)
     if(isThere != -1):
        cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()  #o drawer assigned to user "MAN"  if(cashwise.WaitWindow("#32770","Cashwise",1,1800).Exists) :
    


def StartCashwise(database, password, location):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019 
  cashwise = Sys.Process("Cashwise")
  CloseStartupError()
  Indicator.PushText("In StartCashwise(database, password, location)")
  Delay(2000)
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
    cashwise.Window("TMessageForm", "Warning", 1).VCLObject("No"). Click()
  
  Delay(2000)
  SelectDatabase(database)
  EnterUserPassword(password) 
  EnterStoreLocation(location)
  Delay(2000)
  CloseSystemDateWarning()  

  if(cashwise.WaitWindow("TMessageForm","Confirm",1,800).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("No").Click()
  
  if(cashwise.WaitWindow("#32770","Cashwise",1,800).Exists):
	   
     cashwise.Window("#32770", "Cashwise", 1).Window("Button", "OK", 1).Click()
	   
  Log.Checkpoint("SUCCESS: Cashwise startup is Successful")
  #CashwiseMainMenu_File()
  Indicator.PopText()
  
def CloseStartupError():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 6/5/2019 

  Delay(2000)
  cashwise = Sys.Process("Cashwise")
  Sys.WaitProcess("Cashwise",3000)
#  Sys.WaitProcess(cashwise,3000)
  activeWindow = Sys.Desktop.ActiveWindow()
  text = activeWindow.WndCaption
  if activeWindow.WndCaption == "Error":
    Sys.Process("Cashwise").Window("TMessageForm", "Error").VCLObject("OK").ClickButton()
    Log.Checkpoint("SUCCESS: CloseStartupError is successful.") 
  
def SelectDatabase(database):
 # database = "UtahTestRefreshTest2"
  #Author Pat Holman 4/24/2019
  #Last Modified by Phil 10/03/2019
  #database = "QA01"
  cashwise = Sys.Process("Cashwise")
  Delay(2000)
  Indicator.PushText("In SelectDatabase(database)")
  Delay(2000)
  #--- 9/25/20019 now there is no ObjectIdentifier  -----------------
#  listBox = cashwise.Find("ObjectIdentifier", "lbAliases",10)
#  listBox = cashwise.frmAliasBrowse.pnlMain.sbData.lbAliases  
#  databaseList = cashwise.Window("TfrmAliasBrowse", "Database Aliases", 1)
 # listBox = databaseList.Find("Name", "Window(\"TListBox\", \"\", 1)",10)
 # listBox = databaseList.pnlMain.sbData.lbAliases
  listBox = Sys.Process("Cashwise").VCLObject("frmAliasBrowse").VCLObject("pnlMain").VCLObject("sbData").VCLObject("lbAliases")
                    
  Delay(2000)
#  listBox.Click()
#  listBox.Keys("[Home]")
#  listBox.Keys("ut")
#  listBox.Keys("[Down]")
  listBox.ClickItem(database)
  listBox.ClickItem(database)
#  listBox.ClickItem(database)
  Delay(800)
  listBox.Keys("[F9]")
  
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists):
    cashwise.Window("TMessageForm", "Warning", 1).VCLObject("No"). Click()
  Log.Checkpoint("SUCCESS: SelectDatabase is successful, "+database+" opened")
  Indicator.PopText()
  CloseUpgradeDialog()
  
  
def CloseUpgradeDialog():
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Delay(2000)
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption == "Login":
    return;
  Log.Error("Fix the upgrade error message - Pat")
  Indicator.PopText()
  
def EnterUserPassword(userPassword):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 18/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterUserPassword(userPassword)")
  Sys.Process("Cashwise").frmCPLogin.pnlMain.sbData.edPassword.SetText(userPassword)
  Sys.Process("Cashwise").frmCPLogin.pnlMain.sbData.edPassword.Keys("[F9]")
  Log.Checkpoint("SUCCESS: EnterUserPassword is successful.")
  Indicator.PopText()
  
def EnterStoreLocation(location):
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 5/2/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In EnterStoreLocation(location)")
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys(location)
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Window("TCustomDBEdit").Keys("[F9]")


def CloseSystemDateWarning(): 
  #Author Pat Holman 4/24/2019
  #Last Modified by Pat Holman 7/16/2019  
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In CloseSystemDateWarning()")
  Delay(2000)
  if(cashwise.WaitWindow("TMessageForm","Warning",1,800).Exists == False):
      sleep(500)
  
  activeWindow = Sys.Desktop.ActiveWindow()
  if activeWindow.WndCaption == "Warning":
    Sys.Process("Cashwise").Window("TMessageForm", "Warning").VCLObject("OK").ClickButton()
  Log.Checkpoint("SUCCESS: CloseSystemDateWarning is successful.")  
  Indicator.PopText()
 
  
  
def WaitOnCalendar():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
    cashwise = Sys.Process("Cashwise")
    x = 1
    while cashwise.WaitWindow("TfrmCalendarEdit","Please confirm *",1,500).Exists == False:
      delay(2500) 
      Log.Message("Waiting on Calendar "+ aqConvert.IntToStr(x))
      x = x+1
      if(cashwise.WaitWindow("TMessageForm","Confirm",1,200).Exists):
        Sys.Process("Cashwise").Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
        break
      if(x == 120):
        Log.Error("Something went wrong, look for a cashwise message or error")
    Log.Message("Calendar exists")
     

def MonthToMonth(mIn):
  
  mOUT  = ""
     
  if mIn == "Jan":  
               mOUT = "January"
           
  if(mIn == "Feb") :
               mOUT = "February"
           
  if(mIn == "Mar"):
               mOUT = "March"
           
  if(mIn == "Apr"):      
            mOUT = "April"
           
  if(mIn == "May"):       
               mOUT = "May"
           
  if(mIn == "Jun"):       
               mOUT = "June"
           
  if(mIn == "Jul"):       
              mOUT = "July"
           
  if(mIn == "Aug"):       
               mOUT = "August"
           
  if(mIn == "Sep"):       
               mOUT = "September"
           
  if(mIn == "Oct"):       
               mOUT = "October"
           
  if(mIn == "Nov"):       
               mOUT = "November"
           
  if(mIn == "Dec"):       
               mOUT = "December"            
  Log.Message(mOUT)       
    
  return mOUT
  
  
    
def switchMonthToNumber(monthString):
    switcher = {
        "January": "1",
        "February": "2",
        "March": "3",
        "April": "4",
        "May": "5",
        "June": "6",
        "July": "7",
        "August": "8",
        "September": "9",
        "October": "10",
        "November": "11",
        "December": "12"
        
    }
    month = switcher.get(monthString, "Invalid month")
    return month

def ChangeLocation(locID): 
  
#  locID = "001"  
  #Author Phil 07/21/2020
  #Last Modified 
  cashwise = Sys.Process("Cashwise")
  Indicator.PushText("In OpenUserMaintenance()")
  userManagerForm = cashwise.frmUserManageCheck
 
  cashwiseApp = cashwise.frmMain

  cashwiseApp.Keys("~uc")
  Delay(1000)
  cashwise.frmSelectLocation.pnlMain.sbData.edLocationID.Keys(locID)
 # cashwise.frmSelectLocation.btnSave.Click()
  cashwise.frmSelectLocation.Keys("~s")
  
  
def ReadFile():
  ForReading = 1
  ForWriting = 2
  ForAppending = 8
  #FileName = "..\\..\\Array.txt"
  locationArray = aqObject.varArray
  locationArray = ["","","","","","","","","","","","","","","","","","","","","","","","","","","",]
  x = 0
  FileName = "F:\\_code\\QA\\UtahTestRollForward\\Array.txt"
  Log.Message(FileName)
  FS = Sys.OleObject["Scripting.FileSystemObject"]
  F = FS.OpenTextFile(FileName, ForReading)
  while not F.AtEndOfStream:
    s = F.ReadLine()
    varType = aqObject.GetVarType(s)
    if(varType == 3):
      s = aqConvert.IntToStr(s)
    locationArray[x] = s
    x = x + 1
  F.Close()
  return locationArray
 
def RollForwardCalendarOpen():
   #Author Phil 7/31/2019
   #Last Modified by Phil 7/31/2019 
  cashwise = Sys.Process("Cashwise")

  delay(1000)
  cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
  delay(1000)
  if(cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists):
    cashwise.Window("TMessageForm", "Confirm", 1).Yes.Click()
  
  activeWindow = Sys.Desktop.ActiveWindow()
  
  while (activeWindow.WndCaption != "Daily Analysis Report"):
      Delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
      cashwise.frmDynamicEdit.btnClose.Click(34, 9)    
  activeWindow = Sys.Desktop.ActiveWindow()
  while (activeWindow.WndCaption != "Daily Closeout Report"):
        Delay(1000)
        activeWindow = Sys.Desktop.ActiveWindow()
        cashwise.frmDynamicEdit.btnClose.Click(43, 8)
    
  WaitOnCalendar()
  return 0
   
def RollForward():
  number = 1
  cashwise = Sys.Process("Cashwise")
  for x in range (0, number):
    cashwise.frmMain.MainMenu.Click("File|Roll Forward...")
    cashwise.frmClosing.pnlProgress.btnRollForward.ClickButton()   
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Confirm"):
      Delay(1000)
      #do you want to start performing transaction
    if cashwise.WaitWindow("TMessageForm","Confirm",1,500).Exists:
        cashwise.Window("TMessageForm", "Confirm", 1).VCLObject("Yes").Click()
       
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Daily Analysis Report"):
      Delay(1000)
      activeWindow = Sys.Desktop.ActiveWindow()
    cashwise.frmDynamicEdit.btnClose.Click(34, 9)    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Daily Closeout Report"):
        Delay(1000)
        activeWindow = Sys.Desktop.ActiveWindow()
        cashwise.frmDynamicEdit.btnClose.Click(43, 8)
    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Please confirm the location date*"):
         Delay(1000)
         activeWindow = Sys.Desktop.ActiveWindow()
         cashwise.frmCalendarEdit.btnSave.Click(51, 12)
    
    activeWindow = Sys.Desktop.ActiveWindow()
    while (activeWindow.WndCaption != "Information"):
      ExplicitWait(1)
      activeWindow = Sys.Desktop.ActiveWindow()
      cashwise.TMessageForm5.OK.ClickButton() 

def willRun():
  myvar = "Will it run?"      
  Log.Message(myvar)
      
def DeleteUnprocessSales(): # Start on Drawer Manager Screen
  #Author Phil Ivey 8/1/2019
  #Last Modified by Phil Ivey 8/1/2019  
   cashwise = Sys.Process("Cashwise")
   cashwise.Window("#32770", "Cashwise").Window("Button", "OK").ClickButton()
   cashwise.frmDrawerManager.btnUnprocessedSales.Click()
   
   while cashwise.frmSaleUnprocessed.btnEdit.Enabled == True:
     cashwise.frmSaleUnprocessed.btnEdit.Click()
     cashwise.Window("TMessageForm", "Confirm").OK.ClickButton()
     #saleCancel =   cashwise.frmSaleEdit.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel
     saleCancel =   cashwise.frmSaleEdit_1.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel
     
   #                            cashwise.frmSaleEdit_1.pnlMain.pnlSaleItems.pnlBottom.pnlProcess.pnlQueueContainer.btnCancel
     saleCancel.Click()
     cashwise.Window("TMessageForm", "Confirm").OK.ClickButton()
     Delay(800)
     cashwise.frmSaleUnprocessed.btnClose.Click()
     
  
def TitleCollateralAdd():
  cashwise = Sys.Process("Cashwise")
  pageControl = cashwise.frmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1
  titleDetails = pageControl.tsTitleDetails

  titleDetails.edMake.Keys("Honda")
  titleDetails.edModel.Keys("Civic")
  titleDetails.edVIN.Keys("1233d321485d221d")
  titleDetails.edHiValue.Keys("17000")
  titleDetails.edLowValue.Keys("11000")
  titleDetails.edTradeValue.Keys("12000")
  titleDetails.edYear.Keys("2018")
  titleDetails.edPolicyNumber.Keys("154887")
  titleDetails.edColor.Keys("Blue")
  titleDetails.edOdometer.Keys("15545")
  titleDetails.edLicenseNumber.Keys("s454s")
  titleDetails.edDescription.Keys("4 door")
  titleDetails.mNotes.Keys("OK")
  titleDetails.edState.Keys("UT")
  titleDetails.edLicenseExpirationDate.TAdvancedSpeedButton.Click()
  cashwise.frmCalendarEdit.Panel1.btnNextYear.Click()
  cashwise.frmCalendarEdit.btnSave.Click()
  titleDetails.edCondition.TAdvancedSpeedButton.Click()
  
  condition = cashwise.frmCollateralConditionBrowse
  condition.pnlSearch.isMain.Keys("Fair")
  condition.pnlSearch.isMain.Keys("[Tab]")
  
  if(condition.btnEdit.Enabled == True):
    condition.btnSave.Click()
  else:
    condition.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
    
  titleDetails.edStyle.TAdvancedSpeedButton.Click()
  
  style = cashwise.frmCollateralStyleBrowse
  style.pnlSearch.isMain.Keys("4 door")
  
  if(style.btnEdit.Enabled == True):
    style.btnSave.Click()
  else:
    style.btnAdd.Click()
    cashwise.frmDynamicEdit.btnSave.Click()
  titleDetails.edEvaluationDate.TAdvancedSpeedButton.Click()
  cashwise.frmCalendarEdit.btnSave.Click()
  
  titleDetails.dbeMonthlyDisposableIncome.Keys("250")
  titleDetails.edAdvance.Keys("5000")
  
  pageControl.ClickTab("Insurance Information")
  
  zip = GetRandomZip()
  state = GetStateFromZip(zip)
  city = GetCityFromZip(zip)
  address = GetAddressFromCity(city)
  
  insureInfo = cashwise.frmDeferredCollateralTitleEdit.pnlMain.sbData.PageControl1.tsInsuranceInfo
  insureInfo.edInsuranceCompany.Keys("AllState")
  insureInfo.edInsAddressLine1.Keys(address)
  insureInfo.edInsCity.Keys(city)
  insureInfo.edInsState.Keys(state)
  insureInfo.edInsZip.Keys(zip)
  
  ExplicitWait(1)
  pageControl.ClickTab("Title Details")
  cashwise.frmDeferredCollateralTitleEdit.btnSave.Click()
  
def TestDatabaseConnection():
  date = GetLocationDate("WYO")
  Log.Message(date)
  
def GetLocationDate(locID):
  #locID = "WYO"
  aQuery = "select System_Date from Location where ID = '"+locID+"'"
  columnName = "System_Date"
  locDate = RunSingleQuery(aQuery,columnName)
  locDate = aqConvert.DateTimeToStr(locDate)
  locDate = aqConvert.DateTimeToFormatStr(locDate, "%m/%d/%Y") 
  Log.Message("Date for "+locID+" is "+locDate)  
  return locDate  
