FROM mcr.microsoft.com/dotnet/framework/sdk:4.8 as build-env
RUN mkdir app
RUN mkdir logs
Copy ./QA/NextGen /app/
Copy ./NUnit.Console-3.10.0/bin/net35 /Users/ContainerAdministrator/AppData/Local/Microsoft/WindowsApps/
WORKDIR /app
RUN Nuget Restore
RUN MSBuild.exe Softwise.sln -t:Rebuild
WORKDIR /app