﻿using NLog;

namespace SoftwiseAPI.Base
{
    public class BaseApplication
    {
        public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    }
}
