﻿using SoftwiseAPI.Model;

namespace SoftwiseAPI.Utilities
{
    class Authentication
    {
        public CleoApiResult<string> Employee(Employee.CurrentEmployee employeeUser)
        {
            //var credentials = @"{'username': 'man@softwise.com', 'password': 'softwise'}";
            var credentials = "{'username': '" + employeeUser.UserName + "','password': '" + employeeUser.Password + "'}";
            return RestHandler.PostData<string, string>("", "employee/auth/login", credentials);
        }
    }
}
