﻿using System;
using NLog;
using NUnit.Framework;
using SoftwiseAPI.Utilities;


namespace SoftwiseAPI.Utilities
{
    class AssertResult
    {
        public bool IsBankAccountPresent(string assertValue, string bankAccount)
        {
            if (string.Equals(assertValue, bankAccount) == true)
                return AssertSet(true);
            return AssertSet(false);

        }
        public static bool IsAddressIdPresent(object resultData, string assertValue, string addressId)
        {
            var keyValue = Libraries.GetKeyValue(resultData.ToString(), assertValue);
            var assertResult = AssertSet(false);
            if (string.Equals(keyValue, addressId) == true)
                assertResult = AssertSet(true);
            Assert.That(assertResult, Is.True, "AddressId is not Present");
            return assertResult;
        }

        public static bool IsValuePresent(object resultData, string assertValue, string value)
        {
            var keyValue = Libraries.GetKeyValue(resultData.ToString(), assertValue);
            var assertResult = AssertSet(false);
            Libraries.LogIt($"Test is checking the value for {assertValue}");
            Libraries.LogIt($"test is looking for {value}");
            Libraries.LogIt($"the key value found is {keyValue}");
            if (string.Equals(keyValue, value) == true)
                assertResult = AssertSet(true);
            Assert.That(assertResult, Is.True, "Value is not Present");
            Libraries.LogIt($"assert result is {assertResult} ");
            return assertResult;
        }
        private static bool AssertSet(bool setValue)
        {
            if (setValue == true)
            {
                GlobalManager.ApiTestStatus = "Pass";
                return true;
            }
            return false;
        }
    }

}
