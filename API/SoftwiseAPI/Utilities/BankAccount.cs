﻿using SoftwiseAPI.Model;

namespace SoftwiseAPI.Utilities
{
    class BankAccount
    {
        public CleoApiResult<string> GetBankAccounts(string authToken, string userId)
        {
            return RestHandler.GetResponse<string>(authToken, $"customers/{userId}/bankAccounts");
        }
    }
}
