﻿using SoftwiseAPI.Model;

namespace SoftwiseAPI.Utilities
{
    class CustomerAccount
    {
        public CleoApiResult<string> GetAddresses(string authToken, string userId)
        {
            return RestHandler.GetResponse<string>(authToken, $"customers/{userId}/addresses");
        }
        public CleoApiResult<string> GetGenericIdResults(string authToken, string userId, string urlSegment)
        {
            return RestHandler.GetResponse<string>(authToken, $"customers/{userId}/{urlSegment}");
        }
    }
}