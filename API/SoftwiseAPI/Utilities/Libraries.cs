﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Serialization.Json;
using Newtonsoft.Json.Linq;
using NLog;
using static SoftwiseAPI.Customers;
using static SoftwiseAPI.Employee;
using System.Data.SqlClient;
using MongoDB.Bson;
using Newtonsoft.Json;
using RazorEngine.Compilation.ImpromptuInterface.InvokeExt;

namespace SoftwiseAPI.Utilities
{

    public static class Libraries
    {

        public static List<string> DataBaseQuery(string dbQuery)
        {
            SqlConnection myConnection = new SqlConnection(GlobalManager.DbConnectString);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand(dbQuery, myConnection);
            var myReader = myCommand.ExecuteReader();
            DataTable tableSchema = myReader.GetSchemaTable();
            List<string> tableData = new List<string>();
            var rowData = new Dictionary<string, string>();

            while (myReader.Read())
            {
                if (tableSchema != null)
                    foreach (DataRow myField in tableSchema.Rows)
                    {
                        var x = 0;
                        foreach (DataColumn myProperty in tableSchema.Columns)
                        {
                            if (myProperty.ColumnName == "ColumnName")
                            {
                                var fieldName = myField[myProperty].ToString();
                                var readValue = myReader[fieldName].ToString();
                                rowData.Add(fieldName, readValue);
                            }
                        }
                    }
                var output = Newtonsoft.Json.JsonConvert.SerializeObject(rowData);
                tableData.Add(output);
                rowData.Clear();
            }
            myReader.Close();
            myConnection.Close();
            return tableData;
        }

        public static string AssertResult(ApiUnderTest.CurrentApi api, object apiResult, string dbQuery)
        {
            SqlConnection myConnection = new SqlConnection(GlobalManager.DbConnectString);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand(dbQuery, myConnection);
            var myReader = myCommand.ExecuteReader();
            DataTable schemaTable = myReader.GetSchemaTable();
            
            JObject results = JObject.Parse(apiResult.ToString());

            while (myReader.Read())
            {
                LogIt("\n______");
                if (schemaTable != null)
                    foreach (DataRow myField in schemaTable.Rows)
                    {
                        foreach (DataColumn myProperty in schemaTable.Columns)
                        {
                            if (myProperty.ColumnName == "ColumnName")
                            {
                                var fieldName = myField[myProperty].ToString();
                                var readValue = myReader[fieldName].ToString();
                                var logItString = $"{fieldName} : {readValue} <--- Database Results";
                                foreach (var properties in api.ResultProperties)
                                {
                                    if (fieldName.ToLower() == properties.ToLower())
                                    {
                                        LogIt(logItString);
                                        LogIt(results.WithArgumentName(properties).Name + " : " + results[properties] + " <-- API Results");
                                        if (readValue != results[properties].ToString())
                                        {
                                            LogIt("Error\n");
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
            myReader.Close();
            myConnection.Close();
            return "this";
        }
        public static string GetKeyValue(string data, string key)
        {
            data = data.Replace("[", "");
            data = data.Replace("]", "");
            data = data.Trim();
            var thisData = JObject.Parse(data);
            return thisData.GetValue(key).ToString();
        }


        public static Dictionary<string, string> DeserializeResponse(this IRestResponse restResponse)
        {
            var jsonObj = new JsonDeserializer().Deserialize<Dictionary<string, string>>(restResponse);
            return jsonObj;
        }

        public static string GetResponseObject(this IRestResponse response, string responseObject)
        {
            JObject obs = JObject.Parse(response.Content);
            return obs[responseObject].ToString();
        }

        public static async Task<IRestResponse<T>> ExecuteAsyncRequest<T>(this RestClient client, IRestRequest request) where T : class, new()
        {
            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            client.ExecuteAsync<T>(request, restResponse =>
            {
                if (restResponse.ErrorException != null)
                {
                    const string message = "Error retrieving response.";
                    throw new ApplicationException(message, restResponse.ErrorException);
                }

                taskCompletionSource.SetResult(restResponse);
            });
            return await taskCompletionSource.Task;
        }

        public static object GetGenericIdResults(ApiUnderTest.CurrentApi api, CurrentEmployee authUser,
            string customerId, string urlSegment)
        {
            var authenticate = new Authentication();
            var customerAccount = new CustomerAccount();
            GlobalManager.RunEnviroment = authUser.RunEnvironment;
            LogIt($"run environment set to {GlobalManager.RunEnviroment} ");
            LogIt($"customerId is {customerId} ");
            var authResult = authenticate.Employee(authUser);
            LogIt($"Auth Token is {authResult.AuthToken} ");
            var apiResult = customerAccount.GetGenericIdResults(authResult.AuthToken, customerId, urlSegment);
            //var resultData = StripBracket(apiResult.Data.ToString());
            //LogIt($"apiResult is\n{resultData} ");
            //return resultData;
            var thisThing = apiResult.Data.ToString();
            LogIt(thisThing);
            return apiResult.Data.ToString();
        }

        public static string StripBracket(string data)
        {
            data = data.Replace("[", "");
            data = data.Replace("]", "");
            data = data.Trim();
            return data;
        }

        public static void LogIt(string logString)
        {
            ILogger log = LogManager.GetLogger(typeof(Logger).ToString());
            log.Info(logString);
        }
    }
}