﻿namespace SoftwiseAPI.Utilities
{
    public static class GlobalManager
    {
        public static string AuthToken { get; set; }
        public static string ApiTestStatus { get; set; }
        public static string RunEnviroment { get; set; }
        public static string DbConnectString { get; set; }
    }
}
