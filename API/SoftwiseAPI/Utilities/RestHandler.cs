﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwiseAPI.Model;

namespace SoftwiseAPI.Utilities
{
  /// <summary>
  ///   Generic class handling the communication with the REST APIs.
  /// </summary>
  public static class RestHandler
  {

    private static string _swapiUrl = "";

    static RestHandler()
    {
        SetRunEnvironment();
    }


    /// <summary>
    ///   Deletes the data.
    /// </summary>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <returns>
    ///   (The response code resulting from the call; The error message, if any.)
    /// </returns>
    public static (string responseCode, string error) DeleteData(string userToken, string restQuery)
    {
      string url = $"{_swapiUrl}{restQuery}";

      (string response, string errHeader) = WebDataWrapper.DeleteWebData(url, userToken);
      var result = new CleoApiResult<dynamic>(response, errHeader);

      return (result.HttpStatusCode, result.Error);
    }

    /// <summary>
    ///   Gets the response.
    /// </summary>
    /// <typeparam name="T">The type of the API object.</typeparam>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <param name="queryFilter">The query filter, if filtering, sorting or paging is desired. Null otherwise.</param>
    /// <returns>
    ///   The response, in API object format.
    /// </returns>
    public static CleoApiResult<T> GetResponse<T>(string userToken, string restQuery)
      where T : class
    {
      // Build the paging / filtering / sorting query
      string url = $"{_swapiUrl}{restQuery}";
      (string content, string errHeader) = WebDataWrapper.GetWebData(url, userToken);
      return new CleoApiResult<T>(content, errHeader);
    }

    /// <summary>
    ///   Gets the response as a return type of boolean.
    /// </summary>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <returns>
    ///   (The response as a boolean; The resulting HTTP status code received from the service; The error message, if any.)
    /// </returns>
    public static (T result, string httpStatusCode, string error) GetStructResponse<T>(string userToken,
      string restQuery) where T : struct
    {
      string url = $"{_swapiUrl}{restQuery}";
      (string content, string errHeader) = WebDataWrapper.GetWebData(url, userToken);
      var response = new CleoApiResult<object>(content, errHeader);

      try
      {
        return (response.HttpStatusCode == "200" ? (T)response.Data : default(T), response.HttpStatusCode,
          response.Error);
      }
      catch (InvalidCastException)
      {
        // sometimes the casting might not work for booleans or integers, so do it manually
        if (typeof(T) == typeof(bool))
        {
          bool.TryParse(response.Data.ToString(), out bool result);
          return ((T) (object) (response.HttpStatusCode == "200" && result), response.HttpStatusCode, response.Error);
        }

        if (typeof(T) == typeof(int))
        {
          int.TryParse(response.Data.ToString(), out int result);
          return ((T) (object) (response.HttpStatusCode == "200" ? result : 0), response.HttpStatusCode,
            response.Error);
        }

        throw;
      }
    }

    /// <summary>
    ///   Posts the data.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <typeparam name="TRequest">Type of the API data.</typeparam>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <param name="data">The data.</param>
    /// <returns>
    ///   If successful (response code of 200), returns the API object just created. Otherwise returns null.
    /// </returns>
    public static CleoApiResult<TResult> PostData<TResult, TRequest>(string userToken, string restQuery, TRequest data)
      where TResult : class
      where TRequest : class
    {
        //string url = $"{_swapiURL}{restQuery}";
        string url = $"{_swapiUrl}{restQuery}";
        //string jsonData = JsonConvert.SerializeObject(data);
        string jsonData = data.ToString();
        (string result, string errorHeader) = WebDataWrapper.PostWebData(url, userToken, jsonData);
        return new CleoApiResult<TResult>(result, errorHeader);
    }

    /// <summary>
    ///   Puts the data.
    /// </summary>
    /// <typeparam name="T">Type of the API data.</typeparam>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <param name="data">The data.</param>
    /// <returns>
    ///   (The response code resulting from the call; The error message, if any.)
    /// </returns>
    public static (string responseCode, string error) PutData<T>(string userToken, string restQuery, T data)
      where T : class
    {
      string url = $"{_swapiUrl}{restQuery}";
      string jsonData = JsonConvert.SerializeObject(data);

      (string response, string errHeader) = WebDataWrapper.PutWebData(url, userToken, jsonData);
      var result = new CleoApiResult<T>(response, errHeader);

      return (result.HttpStatusCode, result.Error);
    }

    public static CleoApiResult<TOutput> PutData<TInput, TOutput>(string userToken, string restQuery, TInput data)
      where TInput : class
      where TOutput : class
    {
      string url = $"{_swapiUrl}{restQuery}";
      string jsonData = JsonConvert.SerializeObject(data);

      (string response, string errHeader) = WebDataWrapper.PutWebData(url, userToken, jsonData);
      return new CleoApiResult<TOutput>(response, errHeader);
    }

    /// <summary>
    ///   Puts the data.
    /// </summary>
    /// <typeparam name="T">Type of the API data.</typeparam>
    /// <param name="userToken">The employeeUser token for authorization.</param>
    /// <param name="restQuery">The rest query.</param>
    /// <param name="data">The data.</param>
    /// <returns>
    ///   (The response code resulting from the call; The error message, if any.)
    /// </returns>
    public static (bool result, string error) PutDataBool<T>(string userToken, string restQuery, T data)
      where T : class
    {
      string url = $"{_swapiUrl}{restQuery}";
      string jsonData = JsonConvert.SerializeObject(data);

      (string response, string errHeader) = WebDataWrapper.PutWebData(url, userToken, jsonData);
      var result = new CleoApiResult<dynamic>(response, errHeader);

      JObject responseJson = JObject.Parse(response);
      return ((bool) responseJson.GetValue("data"), result.Error);
    }

    private static void SetRunEnvironment()
    {
        switch (GlobalManager.RunEnviroment)
        {
           case "QA":
               _swapiUrl = "https://qa-wapi.softwise.com/api/v1/"; 
               break;
           default:
               _swapiUrl = "https://qa-wapi.softwise.com/api/v1/";
               break;
        }
    }
  }
}