﻿using System.IO;
using System.Net;
using System.Text;

namespace SoftwiseAPI.Utilities
{
  public static class WebDataWrapper
  {
    public static (string data, string errorHeader) PutWebData(string url, string userToken, string data)
    {
      return CallWebRequest(url, userToken, "PUT", data);
    }

    public static (string data, string errorHeader) DeleteWebData(string url, string userToken)
    {
      return CallWebRequest(url, userToken, "DELETE", null);
    }

    public static (string data, string errorHeader) PostWebData(string url, string userToken, string data)
    {
      return CallWebRequest(url, userToken, "POST", data);
    }

    public static (string data, string errorHeader) GetWebData(string url, string userToken)
    {
      return CallWebRequest(url, userToken, "GET", null);
    }

    private static (string data, string errorHeader) CallWebRequest(string url, string userToken, string method, string postData)
    {
      string result;
      var client = (HttpWebRequest)WebRequest.Create(url);
      client.MaximumResponseHeadersLength = -1;
      client.Headers.Add("X-Auth-Token", userToken);
      client.Headers.Add("X-User-Type", "Employee");
      client.Headers.Add("Locale", "en-US");
      client.Headers.Add("Sec-Fetch-Mode", "cors");
      client.ContentType = "application/json; charset=utf-8";
      client.Method = method;

      if (method == "POST" || method == "PUT")
      {
        byte[] buffer = Encoding.GetEncoding("utf-8").GetBytes(postData);
        client.ContentLength = buffer.Length;

        using (Stream requestStream = client.GetRequestStream())
        {
          requestStream.Write(buffer, 0, buffer.Length);
        }
      }

      string errorHeader;
      using (var response = (HttpWebResponse)client.GetResponse())
      {
        using (Stream receiveStream = response.GetResponseStream())
        {
          using (var readStream = new StreamReader(receiveStream, Encoding.UTF8))
          {
            result = readStream.ReadToEnd();
          }
        }

        errorHeader = response.Headers["errormessage"];
      }

      return (result, errorHeader);
    }

  }
}