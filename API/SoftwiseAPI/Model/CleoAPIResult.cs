﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwiseAPI.Utilities;

namespace SoftwiseAPI.Model
{
  public class CleoApiResult<T> where T: class
  {
    public string HttpStatusCode { get; set; }

    public string Error { get; set; }

    public T Data { get; set; }

    public string AuthToken { get; set; }

    public CleoApiResult()
    {
      
    }

    public CleoApiResult(string content, string errHeader)
    {
      JObject result = JObject.Parse(content);
      HttpStatusCode = result["code"].ToString();
      if (result.ContainsKey("authToken"))
      {
          AuthToken = result["authToken"].ToString();
          GlobalManager.AuthToken = AuthToken;
      }

      if (HttpStatusCode != "200")
      {
        // There was an error, so figure out what it was
        Error = result["message"].ToString();
        if (!string.IsNullOrEmpty(errHeader))
        {
          Error += $"; {errHeader}";
        }
      } else if (result.ContainsKey("data"))
      {
          try
          {
              Data = JsonConvert.DeserializeObject<T>(result["data"].ToString());
          }
          catch (JsonReaderException)
          {
              // sometimes, in the case of primitive types, DeserializeObject can't properly parse the data (for example, "True" for bool); so just assign the actual value to Data in this case
              Data = (T)(object)result["data"].ToString();
          }
      }
    }
  }
}