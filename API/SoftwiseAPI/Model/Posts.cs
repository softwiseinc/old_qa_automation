﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwiseAPI.Model
{
    public class Posts
    {
        public string Code { get; set; }
        public string Data { get; set; }
        public string Message { get; set; }
        public string AuthToken { get; set; }
        public string CommitDate { get; set; }
    }
    public class VersionData
    {
        public string DatabaseVersion { get; set; }
        public string CommitHash { get; set; }
        public string CommitDate { get; set; }
        public string AssemblyVersion { get; set; }
        public string GitVersion { get; set; }
        public string Message { get; set; }
        public string AuthToken { get; set; }
    }
}
