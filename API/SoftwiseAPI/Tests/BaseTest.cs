﻿using System;
using SoftwiseAPI.Base;
using NLog;
using NUnit.Framework;
using SoftwiseAPI.Utilities;

namespace SoftwiseAPI.Tests
{
    public class BaseTest : BaseApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [OneTimeSetUp]
        public void SetupForEveryTestRun()
        {
            Logger.Debug("________[ NEW TEST RUN STARTED ]________");
            GlobalManager.ApiTestStatus = "Fail";
        }

        [SetUp]
        public void SetupForEverySingleTestMethod()
        {
            var testContext = TestContext.CurrentContext;
            string logText = "**** START ****[ " + testContext.Test.Name + " ]**** START ****";
            Logger.Debug(logText);
        }
        [TearDown]
        public void TearDownForEverySingleTestMethod()
        {
            var testContext = TestContext.CurrentContext;
            Logger.Debug(GetType().FullName + " started a method tear down");
            string logText = "[ " + GlobalManager.ApiTestStatus + " : "  + testContext.Test.Name + " ]";
            Logger.Debug(logText);
        }


    }
}