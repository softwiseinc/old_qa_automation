﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SoftwiseAPI.Utilities;

namespace SoftwiseAPI.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases : BaseTest
        {

            [Test]
            [Category("Regression")]
            [TestCase(TestName = "Test_customers_id_addresses")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Test_customers_id_addresses()
            {
                var api = ApiUnderTest.Get_customers_id_addresses();
                var authUser = Employee.QaEmployee001();
                var customer = Customers.QaCustomer001();
                //0010150
                //var resultData = Libraries.GetGenericIdResults(api, authUser, customer.Id, "addresses");
                //var validationDbQuery = api.ValidationDbQuery + $"'{customer.Id}'";
                var resultData = Libraries.GetGenericIdResults(api, authUser, "'U120-0010150'", "addresses");
                var validationDbQuery = api.ValidationDbQuery + "'U120-0010150'";
                Libraries.LogIt(api.Documentation);
                Libraries.AssertResult(api, resultData, validationDbQuery);
            }
            [Test]
            [Category("Regression")]
            [TestCase(TestName = "Test_customers_id_addresses2")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Test_customers_id_addresses2()
            {
                var api = ApiUnderTest.Get_customers_id_addresses();
                var authUser = Employee.QaEmployee001();
                //var customer = Customers.QaCustomer002();
                //var tableData = Libraries.DataBaseQuery(@"select * from ContactAddress where Contact_ID LIKE 'U120%'");
                //select top 10 * from ContactAddress where Contact_ID LIKE 'U120%'
                var tableData = Libraries.DataBaseQuery(@"select * from ContactAddress where Contact_ID LIKE 'U120%'");
                foreach (var collection in tableData)
                {
                    Dictionary<string, string> contactAddresses = JsonConvert.DeserializeObject<Dictionary<string, string>>(collection);
                    //Libraries.LogIt(JObject.Parse(collection).ToString());
                    Libraries.LogIt(contactAddresses["Contact_ID"]);
                    var resultData = Libraries.GetGenericIdResults(api, authUser, contactAddresses["Contact_ID"], "addresses");
                    var validationDbQuery = api.ValidationDbQuery + $"'{contactAddresses["Contact_ID"]}'";
                    Libraries.LogIt(api.Documentation);
                    Libraries.AssertResult(api, resultData, validationDbQuery);
                }
                

            }
            [Test]
            [Category("Regression")]
            [TestCase(TestName = "TestGetBankAccounts")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void TestGetBankAccounts()
            {
                var api = ApiUnderTest.Get_customers_id_addresses();
                var authUser = Employee.QaEmployee001();
                var customer = Customers.QaCustomer001();
                var resultData = Libraries.GetGenericIdResults(api,authUser, customer.Id, "bankAccounts");
                AssertResult.IsValuePresent(resultData, "bankAccountID", customer.BankAccountId);
            }
            [Test]
            [Category("Regression")]
            [TestCase(TestName = "TestAttachmentsIp_None")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void TestAttachmentsIp_None()
            {
                var api = ApiUnderTest.Get_customers_id_addresses();
                var authUser = Employee.QaEmployee001();
                var customer = Customers.QaCustomer001();
                var resultData = Libraries.GetGenericIdResults(api,authUser, customer.Id, "attachments/ip");
                AssertResult.IsValuePresent(resultData, "id", "0");
            }
        }
    }
}