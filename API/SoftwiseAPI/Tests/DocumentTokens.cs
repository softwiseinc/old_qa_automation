﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using SoftwiseAPI.Utilities;

namespace SoftwiseAPI.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases : BaseTest
        {
            [Test]
            [Category("Test Logging")]
            [TestCase(TestName = "Test Logging")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Test_Logging()
            {
                Logger.Trace("TRACE: This is it");
                Logger.Info("INFO: This is it");
                Logger.Debug("DEBUG: This is it");
                Logger.Warn("WARN: This is it");
            }

            [Test]
            [Category("Regression")]
            [TestCase(TestName = "GetVersionCommitHash")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void GetVersionCommitHash()
            {
                var client = new RestClient("https://dev-wapi.softwise.com/api/v1/");
                var request = new RestRequest("version/", Method.GET);
                var response = client.Execute(request);
                var result = response.GetResponseObject("data");
                var versionData = JObject.Parse(result);
                Logger.Debug(versionData.GetValue("databaseVersion"));
                Logger.Debug(versionData.GetValue("commitHash"));
                Logger.Debug(versionData.GetValue("commitDate"));
                Logger.Debug(versionData.GetValue("assemblyVersion"));
                Logger.Debug(versionData.GetValue("gitVersion"));
                var assertValue = versionData.GetValue("commitHash").ToString();
                Assert.That(assertValue, Is.EqualTo("61570239a2bf56d0f8b82788cc6003f21d70901f"), "Name is not correct");
            }


            [Test]
            [Category("Regression")]
            [TestCase(TestName = "Login")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Login()
            {
                var client = new RestClient("https://qa-wapi.softwise.com/api/v1/");
                var request = new RestRequest("employee/auth/login", Method.POST);
                var data = @"{'username': 'man@softwise.com', 'password': 'softwise'}";
                request.AddJsonBody(data);
                request.AddHeader("Accept", "application/json, text/plain, */*");
                request.AddHeader("X-EmployeeUser-Type", "Employee");
                request.AddHeader("Locale", "en-US");
                request.AddHeader("Content-Type", "application/json");
                var response = client.Execute(request);
                var result = response.GetResponseObject("data");
                Logger.Info(result);
            }

            [Test]
            [Category("Regression")]
            [TestCase(TestName = "Login_Swapi")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Login_Swapi()
            {
                var authenticate = new Authentication();
                var user = Employee.QaEmployee001();
                var result = authenticate.Employee(user);
                Logger.Debug(result.Data);
                Logger.Debug(result.HttpStatusCode);
                Logger.Debug(result.Error);
                Logger.Debug(GlobalManager.AuthToken);
            }

            [Test]
            [Category("Regression")]
            [TestCase(TestName = "Login_Swapi2")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void Login_Swapi2()
            {
                var credentials = @"{'username': 'man@softwise.com', 'password': 'softwise'}";
                var result = WebDataWrapper.PostWebData("https://qa-wapi.softwise.com/api/v1/employee/auth/login", "",
                    credentials);
                Logger.Debug(result);
                Logger.Debug(GlobalManager.AuthToken);
            }

            [Test]
            [Category("Regression")]
            [TestCase(TestName = "GetBankAccount")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void GetBankAccount()
            {
                var authenticate = new Authentication();
                var bankAccounts = new BankAccount();
                var assertResult = new AssertResult();
                var user = Employee.QaEmployee001();
                GlobalManager.RunEnviroment = user.RunEnvironment;
                var customer = Customers.QaCustomer001();
                var customerId = customer.Id;
                var customerBankAccount = customer.BankAccountId;
                var authResult = authenticate.Employee(user);
                var accountResult = bankAccounts.GetBankAccounts(authResult.AuthToken, customerId);
                var assertValue = Libraries.GetKeyValue(accountResult.Data, "bankAccountID");
                var testResult = assertResult.IsBankAccountPresent(assertValue, customerBankAccount);
                Assert.That(testResult, Is.True, "Bank Account is not Present");
            }
        }
    }
}