﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwiseAPI
{
    public class Customers
    {
        internal static CurrentCustomer CustomerUser { get; private set; }

        public static CurrentCustomer QaCustomer001()
        {
            CustomerUser = new CurrentCustomer
            {
                RunEnvironment = "QA",
                Id = "U120-0009968",
                //U120-124501092
                BankAccountId = "U120-124501092|32437751622269872",
                AddressId = "U120-124501092|1"
        };
            return CustomerUser;
        }

        public static CurrentCustomer QaCustomer002()
        {
            CustomerUser = new CurrentCustomer
            {
                RunEnvironment = "QA",
                Id = "U120-0009973",
                BankAccountId = "U120-124501092|32437751622269872",
                AddressId = "U120-0009975|1"

            };
            return CustomerUser;
        }
        public class CurrentCustomer
        {
            public string RunEnvironment { get; set; }
            public string Id { get; set; }
            public string BankAccountId { get; set; }
            public string AddressId { get; set; }
            public string DbQuery { get; set; }
        }
    }
}
