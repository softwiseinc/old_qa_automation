﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace SoftwiseAPI
{
    public  class ApiUnderTest
    {

        internal static CurrentApi api { get; private set; }

        public static CurrentApi Get_customers_id_addresses()
        {

            api = new CurrentApi
            {
                ResultProperties = new List<string>
                {
                    "addressID",
                    "customerID",
                    "statusID",
                    "city",
                    "isPrimary",
                    "addressLine1",
                    "addressLine2",
                    "number",
                    "since",
                    "state",
                    "type",
                    "zip",
                    "addressOwnershipId"
                },
                ValidationDbQuery = @"select(Contact_ID + '|' + (CAST(Number as varchar))) addressID,Contact_ID customerID, Status_ID statusID,City city, CASE WHEN IsPrimary = 1 THEN 'True' ELSE 'False' END as IsPrimary,AddressLine1 addressLine1, AddressLine2 addressLine2, Number number, FORMAT (Since, 'M/d/yyyy h:mm:ss tt') as Since, State state, Type_ID type,Zip zip, AddressOwnership_ID addressOwnershipID from ContactAddress where Contact_ID = ",
                Documentation = @" ---- api documentation ----
                                  Get (api/v1/customers/{id}/addresses/)
                                  Get a list of addresses for the customer.
                                    Method: GET
                                    URI Parameters
                                        id: String - the customer id
                                    Expected Return Type: List<DAddress> - A list of addresses"
            };
            return api;
        }

        public class CurrentApi
        {
            public List<string> ResultProperties { get; set; }
            public string Documentation { get; set; }
            public string ValidationDbQuery { get; set; }
        }
    }
}
