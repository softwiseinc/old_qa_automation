﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using ReSharpDemo.Model;
using ReSharpDemo.Model.Utilities;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Serialization.Json;
//using Assert = NUnit.Framework.Assert;

namespace ReSharpDemo
{
    [TestFixture]
    public class API_Demo
    {
        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_001")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_001()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts/{id}", Method.GET);
            var deserialize = new JsonDeserializer();
            request.AddUrlSegment("id", 2);
            var response = client.Execute(request);
            var output = deserialize.Deserialize<Dictionary<string, string>>(response);
            var result = output["author"];
            Assert.That(result, Is.EqualTo("Jan"), "Author is not correct");
        }

        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_002")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_002()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts/{id}/profile", Method.POST);
            request.AddUrlSegment("id", 2);
            request.AddJsonBody(new {name = "Sam"});
            var response = client.Execute(request);
            var deserialize = new JsonDeserializer();
            var output = deserialize.Deserialize<Dictionary<string, string>>(response);
            var result = output["name"];
            Assert.That(result, Is.EqualTo("Sam"), "Name is not correct");
        }

        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_003")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_003()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts", Method.POST);
            request.AddJsonBody(new Posts(){id = "25",author = "Tonya Kolste", title = "25 Tyler Kolste is the man!"} );
            var response = client.Execute(request);
            var deserialize = new JsonDeserializer();
            var output = deserialize.Deserialize<Dictionary<string, string>>(response);
            var result = output["author"];
            Assert.That(result, Is.EqualTo("Tonya Kolste"), "Name is not correct");
        }
        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_004")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_004()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts", Method.POST);
            request.AddJsonBody(new Posts() { id = "15", author = "Anthony Howard", title = "Cute Baby" });
            var response = client.Execute<Posts>(request);
            Assert.That(response.Data.author, Is.EqualTo("Anthony Howard"), "Name is not correct");
        }
        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_005")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_005()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts", Method.POST);
            request.AddJsonBody(new Posts() { id = "19", author = "Anthony Howard", title = "19 Cute Baby" });
            var response = client.ExecuteAsyncRequest<Posts>(request).GetAwaiter().GetResult();
            Assert.That(response.Data.author, Is.EqualTo("Anthony Howard"), "Name is not correct");
        }

        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_006")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_006()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts/{id}/profile", Method.POST);
            request.AddUrlSegment("id", 2);
            request.AddJsonBody(new { name = "Sam" });
            var response = client.Execute(request);
            var result = response.DeserializeResponse()["name"];
            Assert.That(result, Is.EqualTo("Sam"), "Name is not correct");
        }
        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_007")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_007()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts/{id}", Method.GET);
            request.AddUrlSegment("id", 2);
            var response = client.Execute(request);
            var result = response.GetResponseObject("author");
            Assert.That(result, Is.EqualTo("Jan"), "Author is not correct");
        }

        [Test]
        [Category("Regression")]
        [TestCase(TestName = "Demo_008")]
        [Author("Pat Holman", "pholman@softwise.com")]
        public void Demo_008()
        {
            var client = new RestClient("http://localhost:3000/");
            var request = new RestRequest("posts", Method.POST);
            request.AddJsonBody(new Posts() { id = "26", author = "Anthony Howard", title = "26 Cute Baby" });
            var response = client.ExecutePostTaskAsync<Posts>(request).GetAwaiter().GetResult();
            Assert.That(response.Data.author, Is.EqualTo("Anthony Howard"), "Name is not correct");
        }
    }
}
