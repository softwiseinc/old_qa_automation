﻿using System.ComponentModel.Design.Serialization;

namespace Lendwise
{
    internal class Url
    {
        internal class Test
        {
            // please change this after first production deploy
            public const string CollectionsCommunicationsPage = "https://qa-cleo.softwise.com/cms/admin/communication";
            public const string CollectionsDashboard = "https://qa-cleo.softwise.com/cms";
            public const string CCOWebEmployeeAsCustomerLogin = "https://members.ccoc7.com/LoginLMS.aspx?K8nj=bnVsbA==&Sv3Xb=cWEyQG5vc3Ryb21vcnAuY29t";
            public const string LoanInventory = "https://qa-cleo.softwise.com/lms/loans";
            public const string DeResponseMessage = "https://qa-cleo.softwise.com/lms/admin/deResponseMessage";
            public const string Login = "https://qa-cleo.softwise.com/";
            public const string Admin = "https://qa-cleo.softwise.com/lms/admin";
            public const string About = "https://qa-cleo.softwise.com/lms/admin/version";
            public const string SecuritySettings = "https://qa-cleo.softwise.com/lms/admin/security";
            public const string CustomerDashboard = "https://qa-cleo.softwise.com/lms/customers";
            public const string PendingDashboard = "https://qa-cleo.softwise.com/lms/dashboard";
            public const string PendingDetail = "https://qa-cleo.softwise.com/lms/workflow";
            public const string ApplicationDashboard = "https://qa-cleo.softwise.com/lms/applications";
            public const string ApplicationDetail = "https://qa-cleo.softwise.com/lms/applications/edit";
            public const string TypeManagement = "https://qa-cleo.softwise.com/lms/admin ";
            public const string WarningTypes = "https://qa-cleo.softwise.com/lms/admin/lookup/warningtypes";
            public const string NoteTypes = "https://qa-cleo.softwise.com/lms/admin/lookup/notetypes";
            public const string ApplicationWorkState = "https://qa-cleo.softwise.com/lms/admin/lookup/workstate";
            public const string ApplicationWithdrawReasons = "https://qa-cleo.softwise.com/lms/admin/lookup/applicationwithdrawreason";
            public const string ApprovedDashboard = "https://qa-cleo.softwise.com/lms/approved";
            public const string CustomerDetail = "https://qa-cleo.softwise.com/lms/customers/edit";
            public const string SystemConfiguration = "https://qa-cleo.softwise.com/lms/admin/systemConfig";
            public const string WorkflowManagementPage = "https://qa-cleo.softwise.com/lms/admin/workflow";
            public const string DocumentManagement = "https://qa-cleo.softwise.com/lms/admin/documentManagement";
            public const string GoogleTtsTemplate = "https://qa-cleo.softwise.com/lms/admin/documentManagement/tts";
            public const string AttachmentTypes = "https://qa-cleo.softwise.com/lms/admin/lookup/attachmenttype";
            public const string Tokens = "https://qa-cleo.softwise.com/lms/admin/documentManagement/tokens";
            public const string Templates = "https://qa-cleo.softwise.com/lms/admin/documentManagement/templates";
            public const string UserManagement = "https://qa-cleo.softwise.com/lms/admin/users";
            public const string QueueManagement = "https://qa-cleo.softwise.com/lms/admin/queuemanagement";
            public const string QueueSettings = "https://qa-cleo.softwise.com/lms/admin/queuemanagement/settings";
            public const string UserRolesManagement = "https://qa-cleo.softwise.com/lms/admin/roles";
            public const string QueuePriority = "https://qa-cleo.softwise.com/lms/admin/queuemanagement/priority";
            public const string SystemMaintenance = "https://qa-cleo.softwise.com/lms/admin/systemMaintenance";
            public const string LocationMaintenance = "https://qa-cleo.softwise.com/lms/admin/systemMaintenance/location";
            public const string ApplicationMaintenance = "https://qa-cleo.softwise.com/lms/admin/systemMaintenance/application";
            public const string LoanTypeManagement = "https://qa-cleo.softwise.com/lms/admin/loantype";
            public const string SitesPage = "https://qa-cleo.softwise.com/lms/admin/sites";
            public const string CollectionsAdminPage = "https://qa-cleo.softwise.com/cms/admin";
            public const string CollectionsUserManagementPage = "https://qa-cleo.softwise.com/cms/admin/users";
            public const string CollectionsDebtorDetailPage = "https://qa-cleo.softwise.com/cms/debtor";
            public const string CollectionsDocumentRequestPage =
                "https://qa-cleo.softwise.com/cms/admin/documentRequest";
            public const string DocumentManagementCategoryPage = "https://qa-cleo.softwise.com/lms/admin/documentManagement/categories";
            public const string CategoriesEditPage = "https://qa-cleo.softwise.com/lms/admin/documentManagement/categories/edit";
            public const string Queries = "https://qa-cleo.softwise.com/lms/admin/documentManagement/query";
            public const string AdverseActionSettings = "https://qa-cleo.softwise.com/lms/admin/adverseaction";
            public const string StaticTemplatesPage =
                "https://qa-cleo.softwise.com/lms/admin/documentManagement/static";
            public const string StateTimeZoneMappings = "https://qa-cleo.softwise.com/lms/admin/lookup/statetimezone";
            public const string DisclosureRequestPage = "https://qa-cleo.softwise.com/cms/admin/documentRequest/edit/1";
            public const string ApplicationRequestPage = "https://qa-cleo.softwise.com/cms/admin/documentRequest/edit/2";
            public const string ActionSettingsPage = "https://qa-cleo.softwise.com/lms/admin/actionSettings";
            public const string DecisionDashboard = "https://qa-cleo.softwise.com/lms/decision";
            public const string EmployeeSchedules = "https://qa-cleo.softwise.com/lms/admin/schedules";

        }

        internal class Production
        {
            public const string ActionSettingsPage = "https://lms.checkcity.com/lms/admin/actionSettings";
            public const string DisclosureRequestPage = "https://lms.checkcity.com/cms/admin/documentRequest/edit/1";
            public const string ApplicationRequestPage = "https://lms.checkcity.com/cms/admin/documentRequest/edit/2";
            public const string StateTimeZoneMappings = "https://lms.checkcity.com/lms/admin/lookup/statetimezone";
            public const string AdverseActionSettings = "https://lms.checkcity.com/lms/admin/adverseaction";
            public const string CollectionsDocumentRequestPage =
                "https://lms-cleo.softwise.com/cms/admin/documentRequest";
            public const string CollectionsCommunicationsPage = "https://lms-cleo.softwise.com/cms/admin/communication";
            public const string CollectionsDebtorDetailPage = "https://lms-cleo.softwise.com/cms/debtor";
            public const string CollectionUserManagementPage = "https://lms-cleo.softwise.com/cms/admin/users";
            public const string CollectionsAdminPage = "https://LMs-cleo.softwise.com/cms/admin";
            public const string CollectionsDashboard = "https://LMS-cleo.softwise.com/cms";
            public const string CCOWebEmployeeAsCustomerLogin = "https://members.checkcity.com/LoginLMS.aspx?K8nj=bnVsbA==&Sv3Xb=cWEyQG5vc3Ryb21vcnAuY29t";
            public const string LoanInventory = "https://lms-checkcity.softwise.com/lms/loans";
            public const string DeResponseMessage = "https://lms.checkcity.com/lms/admin/deResponseMessage";
            public const string Login = "https://lms.checkcity.com/";
            public const string Admin = "https://lms.checkcity.com/lms/admin";
            public const string About = "https://lms.checkcity.com/lms/admin/version";
            public const string SecuritySettings = "https://lms.checkcity.com/lms/admin/security";
            public const string CustomerDashboard = "https://lms.checkcity.com/lms/customers";
            public const string PendingDashboard = "https://lms.checkcity.com/lms/dashboard";
            public const string PendingDetail = "https://lms.checkcity.com/lms/workflow";
            public const string DecisionDashboard = "https://lms.checkcity.com/lms/decision";
            public const string ApplicationDashboard = "https://lms.checkcity.com/lms/applications";
            public const string ApprovedDashboard = "https://lms.checkcity.com/lms/approved";
            public const string TypeManagement = "https://lms.checkcity.com/lms/admin ";
            public const string WarningTypes = "https://lms.checkcity.com/lms/admin/lookup/warningtypes";
            public const string NoteTypes = "https://lms.checkcity.com/lms/admin/lookup/notetypes";
            public const string ApplicationWorkState = "https://lms.checkcity.com/lms/admin/lookup/workstate";
            public const string ApplicationWithdrawReasons = "https://lms.checkcity.com/lms/admin/lookup/applicationwithdrawreason";
            public const string CustomerDetail = "https://lms.checkcity.com/lms/customers/edit";
            public const string EmployeeSchedules = "https://lms.checkcity.com/lms/admin/schedules";
            public const string ApplicationDetail = "https://lms-checkcity.com/lms/applications/edit";
            public const string SystemConfiguration = "https://lms.checkcity.com/lms/admin/systemConfig";
            public const string DocumentManagement = "https://lms.checkcity.com/lms/admin/documentManagement";
            public const string Queries = "https://lms-checkcity.softwise.com/lms/admin/documentManagement/query";
            public const string DocumentManagementCategoryPage = "https://lms.checkcity.com/lms/admin/documentManagement/categories";
            public const string CategoriesEditPage = "https://lms.checkcity.com/lms/admin/documentManagement/categories/edit";
            public const string StaticTemplatesPage = "https://lms.checkcity.com/lms/admin/documentManagement/static";
            public const string GoogleTtsTemplate = "https://lms-checkcity.softwise.com/lms/admin/documentManagement/tts";
            public const string WorkflowManagementPage = "https://lms.checkcity.softwise.com/lms/admin/workflow";
            public const string AttachmentTypes = "https://lms-checkcity.softwise.com/lms/admin/lookup/attachmenttype";
            public const string Tokens = "https://lms-checkcity.com/lms/admin/documentManagement/tokens";
            public const string Templates = "https://lms.checkcity.com/lms/admin/documentManagement/templates";
            public const string UserManagement = "https://lms.checkcity.com/lms/admin/users";
            public const string QueueManagement = "https://lms.checkcity.com/lms/admin/queuemanagement";
            public const string UserRolesManagement = "https://lms.checkcity.com/lms/admin/roles";
            public const string QueueSettings = "https://lms.checkcity.com/lms/admin/queuemanagement/settings";
            public const string QueuePriority = "https://lms.checkcity.com/lms/admin/queuemanagement/priority";
            public const string SystemMaintenance = "https://lms.checkcity.com/lms/admin/systemMaintenance";
            public const string LocationMaintenance = "https://lms.checkcity.com/lms/admin/systemMaintenance/location";
            public const string ApplicationMaintenance = "https://lms.checkcity.com/lms/admin/systemMaintenance/application";
            public const string LoanTypeManagement = "https://lms.checkcity.com/lms/admin/loantype";
            public const string SitesPage = "https://lms.checkcity.softwise.com/lms/admin/sites";
            
            



        }

        internal class Staging
        {
            public const string DecisionDashboard = "https://stg-cleo.softwise.com/lms/decision";
            public const string ActionSettingsPage = "https://stg-cleo.softwise.com/lms/admin/actionSettings";
            public const string DisclosureRequestPage = "https://stg-cleo.softwise.com/cms/admin/documentRequest/edit/1";
            public const string ApplicationRequestPage = "https://stg-cleo.softwise.com/cms/admin/documentRequest/edit/2";
            public const string StateTimeZoneMappings = "https://stg-cleo.softwise.com/lms/admin/lookup/statetimezone";
            public const string AdverseActionSettings = "https://stg-cleo.softwise.com/lms/admin/adverseaction";
            public const string Queries = "https://stg-cleo.softwise.com/lms/admin/documentManagement/query";
            public const string StaticTemplatesPage =
                "https://stg-cleo.softwise.com/lms/admin/documentManagement/static";
            public const string EmployeeSchedules = "https://stg-cleo.softwise.com/lms/admin/schedules";
            public const string CategoriesEditPage = "https://stg-cleo.softwise.com/lms/admin/documentManagement/categories/edit";
            public const string DocumentManagementCategoryPage = "https://stg-cleo.softwise.com/lms/admin/documentManagement/categories";
            public const string CollectionsDocumentRequestPage =
                "https://stg-cleo.softwise.com/cms/admin/documentRequest";
            public const string CollectionsCommunicationsPage = "https://stg-cleo.softwise.com/cms/admin/communication";
            public const string CollectionsDebtorDetailPage = "https://stg-cleo.softwise.com/cms/debtor";
            public const string CollectionsUserManagementPage = "https://stg-cleo.softwise.com/cms/admin/users";
            public const string CollectionsAdminPage = "https://stg-cleo.softwise.com/cms/admin";
            public const string CollectionsDashboard = "https://stg-cleo.softwise.com/cms";
            public const string CCOWebEmployeeAsCustomerLogin = "https://staging.checkcity.com/LoginLMS.aspx?K8nj=bnVsbA==&Sv3Xb=cWEyQG5vc3Ryb21vcnAuY29t";
            public const string LoanInventory = "https://stg-cleo.softwise.com/lms/loans";
            public const string DeResponseMessage = "https://stg-cleo.softwise.com/lms/admin/deResponseMessage";
            public const string Login = "https://stg-cleo.softwise.com/";
            public const string Admin = "https://stg-cleo.softwise.com/lms/admin";
            public const string About = "https://stg-cleo.softwise.com/lms/admin/version";
            public const string SecuritySettings = "https://stg-cleo.softwise.com/lms/admin/security";
            public const string CustomerDashboard = "https://stg-cleo.softwise.com/lms/customers";
            public const string PendingDashboard = "https://stg-cleo.softwise.com/lms/dashboard";
            public const string ApprovedDashboard = "https://stg-cleo.softwise.com/lms/approved";
            public const string PendingDetail = "https://stg-cleo.softwise.com/lms/workflow";
            public const string ApplicationDashboard = "https://stg-cleo.softwise.com/lms/applications";
            public const string TypeManagement = "https://stg-cleo.softwise.com/lms/admin ";
            public const string WarningTypes = "https://stg-cleo.softwise.com/lms/admin/lookup/warningtypes";
            public const string NoteTypes = "https://stg-cleo.softwise.com/lms/admin/lookup/notetypes";
            public const string ApplicationWorkState = "https://stg-cleo.softwise.com/lms/admin/lookup/workstate";
            public const string ApplicationWithdrawReasons = "https://stg-cleo.softwise.com/lms/admin/lookup/applicationwithdrawreason";
            public const string WorkflowManagementPage = "https://stg-cleo.softwise.com/lms/admin/workflow";
            public const string CustomerDetail = "https://stg-cleo.softwise.com/lms/customers/edit";
            public const string AttachmentTypes = "https://stg-cleo.softwise.com/lms/admin/lookup/attachmenttype";
            public const string ApplicationDetail = "https://stg-cleo.softwise.com/lms/applications/edit";
            public const string SystemConfiguration = "https://stg-cleo.softwise.com/lms/admin/systemConfig";
            public const string DocumentManagement = "https://stg-cleo.softwise.com/lms/admin/documentManagement";
            public const string GoogleTtsTemplate = "https://stg-cleo.softwise.com/lms/admin/documentManagement/tts";
            public const string Tokens = "https://stg-cleo.softwise.com/lms/admin/documentManagement/tokens";
            public const string Templates = "https://stg-cleo.softwise.com/lms/admin/documentManagement/templates";
            public const string UserManagement = "https://stg-cleo.softwise.com/lms/admin/users";
            public const string QueueManagement = "https://Stg-cleo.softwise.com/lms/admin/queuemanagement";
            public const string UserRolesManagement = "https://stg-cleo.softwise.com/lms/admin/roles";
            public const string QueueSettings = "https://stg-cleo.softwise.com/lms/admin/queuemanagement/settings";
            public const string QueuePriority = "https://stg-cleo.softwise.com/lms/admin/queuemanagement/priority";
            public const string SystemMaintenance = "https://stg-cleo.softwise.com/lms/admin/systemMaintenance";
            public const string LocationMaintenance = "https://stg-cleo.softwise.com/lms/admin/systemMaintenance/location";
            public const string ApplicationMaintenance = "https://stg-cleo.softwise.com/lms/admin/systemMaintenance/application";
            public const string LoanTypeManagement = "https://stg-cleo.softwise.com/lms/admin/loantype";
            public const string SitesPage = "https://stg-cleo.softwise.com/lms/admin/sites";


        }
    }
}