﻿using System;
using AutomationResources;
using AventStack.ExtentReports;
using Lendwise.Pages;
using NUnit.Framework;
using static CheckCityOnline.Users;



namespace Lendwise.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {

            [Test]
            [TestCase(TestName = "RescindLoansUT_ReleaseTest")]
            [Author("Pat Holman", "pholman@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void RescindLoansUT_ReleaseTest()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, ownerEmployee);
                var configuration = TestConfigManager.Config;
                CheckCityOnline.Users.CurrentUser approvedCustomer = CheckCityOnline.Users.GetReleaseTestUser(CustomerTypeReleaseTest.UT_PendingAll, configuration);
                //loginPage 
                if (configuration.SystemUnderTest == SystemUnderTest.PROD)
                {
                    ownerEmployee.UserName = "johnmcclane@nostromorp.com";
                    ownerEmployee.Password = "Diehard";
                }
                loginPage.LogInUser(ownerEmployee);

                //adminPage
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.ClickCustomerDashboard();
                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(approvedCustomer);
                customerDashboardPage.ClickGoToButton();
                customerDashboardPage.ClickTopLoanInList();
                while (Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.Buttons.Rescind))
                {
                    customerDashboardPage.ClickLoanRescind();
                    customerDashboardPage.ClickRescindSave();
                    customerDashboardPage.RescindOverrideHandler(ownerEmployee, configuration);
                    customerDashboardPage.ClickRescindOk();
                    customerDashboardPage.ExplicitWait(2);
                    customerDashboardPage.TryClickTopLoanInList();
                }
                //customerDashboardPage.ClickTopLoanInList();
                //if (Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.Buttons.Rescind))
                //{
                //    customerDashboardPage.ClickLoanRescind();
                //    customerDashboardPage.ClickRescindSave();
                //    customerDashboardPage.ClickRescindOk();
                //    customerDashboardPage.ExplicitWait(12);
                //}
                    


            }

        }
    }
}
