﻿using AutomationResources;
using Lendwise.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using static Lendwise.Pages.CustomerHelper;
using System.Collections.Generic;
using System.Diagnostics;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using MongoDB.Bson.Serialization.Serializers;
using SharpCompress.Compressors.Deflate;
using static CheckCityOnline.Users;
using CustomerType = AutomationResources.CustomerType;

namespace Lendwise.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    internal partial class AllTests
    {
        //All other partial classes are part of this class
        private partial class AllTestsCases : BaseTest
        {
            [Test]
            [TestCase(TestName = "NewLoanSignatureValidation_0000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("SignatureValidation")]
            public void NewLoanSignatureValidation_0000()
            {

                string refVal = null;

                List<string> customerDataList = new List<string>();
                var advanceCustomerService = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advanceCustomerService);
                var pendingDashboardPage = new PendingDashboard(Driver, advanceCustomerService);
                var customerDashboardPage = new CustomerDashboard(Driver, advanceCustomerService);
                var customerDetailPage = new CustomerDetail(Driver, advanceCustomerService);
                var customerHelper = new CustomerHelper(Driver, advanceCustomerService);
                
                customerHelper.BuildCustomerAccounts(Driver, customerDataList);
                Debug.WriteLine(customerDataList.Count.ToString());

                loginPage.LogInUser(advanceCustomerService);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();

                foreach (var customerData in customerDataList)
                {
                    var customer = customerData.Split(',');
                    string customerState = customer[0];
                    string customerFirstName = customer[1];
                    string customerLoanType = customer[2];
                    var nameSplit = customerFirstName.Split(' ');
                    string signatureHash = nameSplit[1].TrimEnd().TrimStart();

                    pendingDashboardPage.ClickHamburgerMenu();
                    pendingDashboardPage.SelectCustomerDashboard();
                    customerDashboardPage.AssertCustomerDashboardLoaded();
                    customerDashboardPage.FindCustomerByName(customerFirstName);
                    customerDashboardPage.ClickGoToButton();
                    customerDetailPage.AssertCustomerDetailVisible();
                    customerDetailPage.ClickThreeDotButtonOnLoansSection();
                    customerDetailPage.SelectSignedDisclosure();
                    switch (customerState)
                    {
                        case "UT":
                            if (customerLoanType == "paydayLoan")
                                customerDetailPage.ValidateUtahPaydayLoanAgreementSignatures(signatureHash);
                            else 
                                customerDetailPage.ValidateUtahPersonalLoanAgreementSignatures(signatureHash);
                            break;
                        case "ID":
                                customerDetailPage.ValidateIdahoPersonalLoanAgreementSignatures(signatureHash);
                                break;
                        case "WY":
                                customerDetailPage.ValidateWyomingPaydayLoanAgreementSignatures(signatureHash);
                                break;
                        case "TX":
                            customerDetailPage.ValidateTexasPersonalLoanAgreementSignatures(signatureHash);
                            break;
                    }
                    customerDetailPage.ExplicitWait(3);
                }
            }

            
            [Test]
            [TestCase(TestName = "LendwiseLogout_7900")]
            //[Ignore("Test Broke", Until = "2020-01-30 12:00:00Z")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void LendWiseLogout_7900()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.ClickLogout();
                adminPage.ClickYes();

            }

            [Test]
            [TestCase(TestName = "Ravi_LendwiseLogout_Fail")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("LoginLogout")]
            public void Ravi_LendwiseLogout_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, user);
                var adminPage = new AdminPage(Driver, user);

                loginPage.LogInUser(user);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.ClickLogout();
                adminPage.ClickYes();
                loginPage.AssertLoginPageNotVisible();
            }



            [Test]
            [TestCase(TestName = "NavigateToApplicationDashboard_7892")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToApplicationDashboard_7892()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);


                //pendingDashboardPage
                pendingDashboardPage.NavigateToApplicationDashboard(csrEmployee);

                //applicationDashboardPage
                applicationDashboardPage.AssertIsApplicationDashboardLoaded();

            }
            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToApplicationDashboard_7892")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToApplicationDashboard_7892()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);


                //pendingDashboardPage
                pendingDashboardPage.NavigateToApplicationDashboard(csrEmployee);

                //applicationDashboardPage
                applicationDashboardPage.AssertIsApplicationDashboardLoaded();

            }

            [Test]
            [TestCase(TestName = "NavigateToCustomerDashboard_7893")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToCustomerDashboard_7893()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
            }
            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToCustomerDashboard_7893")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToCustomerDashboard_7893()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
            }

            [Test]
            [TestCase(TestName = "NavigateToBankCardCustomerDetailPage_10329")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToBankCardCustomerDetailPage_10329()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                
                customerDashboardPage.EnterSearchByEmail(GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance));
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickBankCardsSection();

            }

            [Test]
            [TestCase(TestName = "AddBankCardCustomerDetailPage_10330")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void AddBankCardCustomerDetailPage_10330()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerWithBankCards = "qa1@nostromorp.com";

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmailApprovedCustomer(customerWithBankCards);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickBankCardsSection();
                customerDetailPage.ClickAddButtonBankCardSection();
                customerDetailPage.EnterCardHolderName();
                customerDetailPage.EnterCardNumber();
                customerDetailPage.EnterSecurityCode();
                customerDetailPage.ClickExpirationDateMonth();
                customerDetailPage.SelectMonthExpirationDate();
                customerDetailPage.ClickExpirationDateYear();
                customerDetailPage.SelectYearExpirationDate();
                customerDetailPage.EnterBillingZipcode();
                customerDetailPage.ClickSaveCardButton();
                customerDetailPage.ClickOkStoreCardPopup();
                

            }

            [Test]
            [TestCase(TestName = "BankCardsHistoricalAccounts_10358")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void BankCardsHistoricalAccounts_10358()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerWithBankCards = "qa1@nostromorp.com";

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmailApprovedCustomer(customerWithBankCards);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickBankCardsSection();
                customerDetailPage.ClickHistoricalAccountsBankCardsSection();
                customerDetailPage.AssertHistoricalAccountsDisplayed();


            }

            
            [Test]
            [TestCase(TestName = "NavigateToPendingDetailPage_7902")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToPendingDetailPage_7902()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
            }
            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToPendingDetailPage_7902")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToPendingDetailPage_7902()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
            }

            [Test]
            [TestCase(TestName = "NavigateToLoanInventoryPage_11061")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanInventoryPage_11061()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
            }

            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToLoanInventoryPage_11061")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToLoanInventoryPage_11061()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
            }

            [Test]
            [TestCase(TestName = "NavigateToApprovedDashboardPage_11062")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToApprovedDashboardPage_11062()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
            }

            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToApprovedDashboardPage_11062")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToApprovedDashboardPage_11062()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
            }

            [Test]
            [TestCase(TestName = "NavigateToDecisionDashboardPage_11063")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDecisionDashboardPage_11063()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var decisionDashboardPage = new DecisionDashboardPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectDecisionDashBoard();
                
                decisionDashboardPage.AssertIsDecisionDashboardPageLoad();
            }

            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToDecisionDashboardPage_11063")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToDecisionDashboardPage_11063()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var decisionDashboardPage = new DecisionDashboardPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectDecisionDashBoard();

                decisionDashboardPage.AssertIsDecisionDashboardPageLoad();
            }

            [Test]
            [TestCase(TestName = "NavigateToLoanTermsOnPendingDetailPage_11028")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsOnPendingDetailPage_11028()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                

            }


            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ValidateNoteField_AddNoteNoSave_8277")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Category("RegressionTest")]
            // Last edit by: Pat Holman, December 6, 2019
            // changed name and refactor for framework changes
            public void ValidateNoteField_AddNoteNoSave_8277()
            {
                var employee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, employee);
                var pendingDashboardPage = new PendingDashboard(Driver, employee);
                var pendingDetailPage = new PendingDetail(Driver, employee);

                loginPage.LogInUser(employee);
               
                pendingDashboardPage.NavigateToPendingDashboard();
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDetailPage.ClickApplicationIdByLastName("Pending All");

                pendingDetailPage.AssertIsPendingDetailVisible();
                var appId = pendingDetailPage.GetApplicationId();
                //pendingDetailPage.SelectNoteType();
                pendingDetailPage.SelectNoteTypeOnPendingDetailMain();
                var validationText = pendingDetailPage.EnterNoteOnPendingDetailMainPage();
                //var validationText = pendingDetailPage.EnterNote();
              

                pendingDetailPage.ClickSaveNoteFailCase();
                pendingDetailPage.ClickBacktoDashboard();
                pendingDetailPage.ClickApplicationId(appId);
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.AssertTextNotVisible(validationText);
            }

            [Test]
            [TestCase(TestName = "Ravi_ValidateNoteField_AddNoteNoSave_Fail_8277")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            // [Ignore("Test Broke", Until = "2020-03-09 12:00:00Z")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("LoanAction")]
            public void Ravi_ValidateNoteField_AddNoteNoSave_Fail_8277()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);


                loginPage.LogInUser(csrEmployee);
              
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.AssertClickSaveNoteFail();

            }

            [Test]
            [TestCase(TestName = "ProcessLoanThroughLMS_8288")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("ProcessLoan")]
            public void ProcessLoanThroughLMS_8288()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                //Validate ID
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Income
                pendingDetailPage.ClickValidateIncome();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Address
                pendingDetailPage.ClickValidateAddress();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Phone
                pendingDetailPage.ClickValidatePhone();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Bank
                pendingDetailPage.ClickValidateBank();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();

                //Validate Previous Loan if visible
                pendingDetailPage.ClickValidatePreviousLoan();
                

                //validate SSN
                pendingDetailPage.ClickValidateSSN();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                
                pendingDetailPage.ClickProcessLoan();
                pendingDetailPage.ClickAcknowledge();

            }

            /*[Test]
            [Ignore("Test Broke", Until = "2020-01-30 12:00:00Z")]
            [TestCase(TestName = "Rajesh_ProcessLoanThroughLMS_8288_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void Rajesh_ProcessLoanThroughLMS_8288_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                loginPage.SelectLocation();
                loginPage.ClickAndSelectClose();

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                //Validate ID
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.ClickValidateIdButton();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Income
                pendingDetailPage.ClickValidateIncome();
                //pendingDetailPage.ClickValidateIncomeButton();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Address
                pendingDetailPage.ClickValidateAddress();
                //pendingDetailPage.ClickValidateAddressButton();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Phone
                pendingDetailPage.ClickValidatePhone();
                //pendingDetailPage.ClickValidatePhoneButton();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                //Validate Bank
                pendingDetailPage.ClickValidateBank();
                //pendingDetailPage.ClickValidateBankButton();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();

                pendingDetailPage.ClickProcessLoan();
                pendingDetailPage.ClickAcknowledge();

            }*/


            //[Test]
            //[TestCase(TestName = "ValidateLoanAction_8284_Stg")]
            //[Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            //[Parallelizable(ParallelScope.Self)]
            //[Category("Regression")]
            //[Category("RunEnvStage")]
            //[Category("LoanAction")]
            //public void ValidateLoanAction_8284_Stg()
            //{
            //    var user = Users.GetStagingUser004();
            //    var loginPage = new LoginPage(Driver, user);
            //    var pendingDashboardPage = new PendingDashboard(Driver, user);
            //    var pendingDetailPage = new PendingDetail(Driver, user);

            //    loginPage.LogInUser(user);
            //    loginPage.SelectLocation();
            //    loginPage.ClickAndSelectClose();

            //    pendingDashboardPage.AssertIsPendingDashboardLoaded();
            //    pendingDashboardPage.ClickWorkNext();

            //    pendingDetailPage.AssertIsPendingDetailVisible();
            //    pendingDetailPage.ClickValidateId();
            //    pendingDetailPage.SelectNoteType();
            //    pendingDetailPage.EnterNote();
            //    pendingDetailPage.ClickSaveNote();

            //}


            [Test]
            [TestCase(TestName = "NavigateToTypeManagement_7888")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToTypeManagement_7888()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var typeManagementPage = new TypeManagement(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();
                typeManagementPage.AssertTypeManagementVisible();
            }

            [Test]
            [TestCase(TestName = "NavigateToWarningTypes_8295")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToWarningTypes_8295()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var typeManagementPage = new TypeManagement(Driver, userManagerEmployee);
                var warningTypesPage = new WarningTypes(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
               

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickWarningTypes();

                warningTypesPage.AssertWarningTypesVisible();
            }
            
            [Test]
            [TestCase(TestName = "NavigateToNoteTypes_8297")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToNoteTypes_8297()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var typeManagementPage = new TypeManagement(Driver, managerEmployee);
                var noteTypesPage = new NoteTypes(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
             

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickNoteTypes();

                noteTypesPage.AssertNoteTypesVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToDEResponseMessages_9663")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDEResponseMessages_9663()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var decisionEngineResponsePage = new DecisionEngineResponsePage(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDeResponseMessages();

                decisionEngineResponsePage.AssertDeResponseMessageLoaded();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToApplicationWorkState_8304")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToApplicationWorkState_8304()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var typeManagementPage = new TypeManagement(Driver, ownerEmployee);
                var applicationWorkStatePage = new ApplicationWorkState(Driver, ownerEmployee);

                loginPage.LogInUser(ownerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickApplictionWorkState();

                applicationWorkStatePage.AssertApplicationWorkStateVisible();

            }
            
            [Test]
            [TestCase(TestName = "Ravi_ApprovedDashboardLoanType_9138")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDashboard")]
            public void Ravi_ApprovedDashboardLoanType_9138()
            {
                var advUser = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advUser);
                var pendingDashboard = new PendingDashboard(Driver, advUser);
                var approvedDashboardPage = new ApprovedDashboard(Driver, advUser);

                loginPage.LogInUser(advUser);
                
                pendingDashboard.AssertIsPendingDashboardLoaded();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.AssertCheckLoanType();
            }

            [Test]
            [TestCase(TestName = "NavigateToApplicationWithDrawReasons_8308")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToApplicationWithDrawReasons_8308()
            {

                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var typeManagementPage = new TypeManagement(Driver, ownerEmployee);
                var applicationWithdrawReasonsPage = new ApplicationWithdrawReasons(Driver, ownerEmployee);

                loginPage.LogInUser(ownerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickApplictionWithdrawReasons();

                applicationWithdrawReasonsPage.AssertApplicationWithdrawReasonsVisible();

            }
            
            [Test]
            //[Ignore("Test Broke", Until = "2019-12-26 12:00:00Z")]
            // Please run this test manually and see if the is a bug in our product
            // or the test script needs some maintenance.
            [TestCase(TestName = "CancelLoanApplication_8322")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("LoanApplication")]
            public void CancelLoanApplication_8322()
            {
                //var user = Users.GetQaUser006();
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, userCsrEmployee);

                loginPage.LogInUser(userCsrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectCancelApplication();
                pendingDetailPage.CancelReasonType();
                pendingDetailPage.CancellationNote();
                pendingDetailPage.ClickWithdraw();

            }
            [Test]
            [TestCase(TestName = "CancelLoanLoanApplicationMultipleReasons_10395")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("LoanApplication")]
            public void CancelLoanLoanApplicationMultipleReasons_10395()
            {
                
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, userCsrEmployee);

                loginPage.LogInUser(userCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectCancelApplication();
                pendingDetailPage.SelectMultipleReasonTypes();
                pendingDetailPage.CancellationNote();
                pendingDetailPage.ClickWithdraw();

            }
            
            [Test]
            [TestCase(TestName = "Rajesh_CancelLoanApplication_8322_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_CancelLoanApplication_8322_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectCancelApplication();
                pendingDetailPage.ClickWithdrawFailCase();

            }

            [Test]
            [TestCase(TestName = "Ravi_CancelLoanApplication_8322_Fail")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_CancelLoanApplication_8322_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectCancelApplication();
                pendingDetailPage.AssertCancelLoanFailCondition();
                pendingDetailPage.CancelReasonType();
                pendingDetailPage.CancellationNote();
                pendingDetailPage.AssertClickWithdrawFailCondition();

            }
            
            [Test]
            [TestCase(TestName = "EditLoanApplicationFromPendingDetailPage_8332")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("PendingDetailPage")]
            public void EditLoanApplicationFromPendingDetailPage_8332()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectEditLoan();

                pendingDetailPage.AssertIsPendingDetailVisible();
                //customerDetailPage.AssertCustomerDetailVisible();


            }

            [Test]
            [TestCase(TestName = "Ravi_EditLoanApplicationFromPendingDetailPage_8332_Fail")]
            [Author("Ravi Kazi karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_EditLoanApplicationFromPendingDetailPage_8332_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectEditLoan();

                customerDetailPage.AssertCustomerDetailInvisible();

            }
            
            [Test]
            [TestCase(TestName = "SetWorkState_8337")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void SetWorkState_8337()
            {
                var userCsrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, userCsrEmployee);

                loginPage.LogInUser(userCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectSetWorkState();
                pendingDetailPage.SelectWorkStateType();
                pendingDetailPage.ClickAssign();

            }

            [Test]
            [Category("Ignore")]
            [TestCase(TestName = "Rajesh_SetWorkState_8337_Fail")]
            [Author("Rajesh Kr Das", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_SetWorkState_8337_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
              
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectSetWorkState();
                pendingDetailPage.ClickAssignFailCase();

            }

            [Test]
            [TestCase(TestName = "Ravi_SetWorkState_8337_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_SetWorkState_8337_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectSetWorkState();
                pendingDetailPage.AssertClickAssign();

            }

            
            [Test]
            [TestCase(TestName = "AddWarningFromPendingDetailPage_8339")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("Warning")]
            public void AddWarningFromPendingDetailPage_8339()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.ClickWarningSave();

            }

            [Test]
            [TestCase(TestName = "Rajesh_AddWarningFromPendingDetailPage_8339_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("Warning")]
            public void Rajesh_AddWarningFromPendingDetailPage_8339_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.AssertClickWarningSaveFailCase();

            }


            [Test]
            [TestCase(TestName = "Ravi_AddWarningFromPendingDetailPage_8339_Fail")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_AddWarningFromPendingDetailPage_8339_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.AssertClickWarningSaveFailCondition();

            }
            
            [Test]
            [TestCase(TestName = "Ravi_SetWorkState_assign")]
            [Author("Ravi Karmacharya", "ravik@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_SetWorkState_assign()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectSetWorkState();
                pendingDetailPage.ClickAssign();
            }


            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ReassignApplicationFromPendingDetailPage_8369")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ReassignApplicationFromPendingDetailPage_8369()
            {

                var advancedCsrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advancedCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, advancedCsrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, advancedCsrEmployee);

                loginPage.LogInUser(advancedCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectReassignApplication();
                pendingDetailPage.SelectAssignTo();
                pendingDetailPage.ClickSave();

            }
            [Test]
            [Category("Ignore")]
            [Category("RegressionTest")]
            [TestCase(TestName = "Rajesh_ReassignApplicationFromPendingDetailPage_8369_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            public void Rajesh_ReassignApplicationFromPendingDetailPage_8369_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.AssertClickWarningSaveFailCase();
            }

            [Test]
            [TestCase(TestName = "Ravi_ReassignApplicationFromPendingDetailPage_8369_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("PendingDetailPage")]
            public void Ravi_ReassignApplicationFromPendingDetailPage_8369_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.ClickOnPendingDashboard();
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.AssertClickWarningSaveFailCondition();

            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "DuplicateSSN")]
            [Author("Ravi", "ravi@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            public void DuplicateSsn()
            {
                var user = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, user);
                var adminPage = new AdminPage(Driver, user);
                var approvedDashboardPage = new ApprovedDashboard(Driver, user);

                loginPage.LogInUser(user);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ValidateDuplicateSsn();
            }

            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "RejectWorkItemValidateWorkItemPendingDetailPage_8371")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void RejectWorkItemValidateWorkItemPendingDetailPage_8371()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRejectId();
                pendingDetailPage.SelectRejectNoteType();
                pendingDetailPage.EnterRejectNote();
                pendingDetailPage.ClickSaveRejectNote();
                pendingDetailPage.ClickBankWorkItem();
                pendingDetailPage.Refresh();
                pendingDetailPage.ClickDeniedWorkItem();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.SelectWorkItemNoteType();
                pendingDetailPage.EnterNoteForWorkItem();
                pendingDetailPage.SaveWorkItemPopupNote();
            }


            [Test]
            [TestCase(TestName = "ClickWorkItemYellowNoteToDisplayItems_9758")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ClickWorkItemYellowNoteToDisplayItems_9758()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickIdToDoYellowNote();
            }
            [Test]
            [Category("Ignore")]
            [Category("RegressionTest")]
            [TestCase(TestName = "Rajesh_RejectLoanActionPendingDetailPage_8371_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            public void Rajesh_RejectLoanActionPendingDetailPage_8371_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRejectId();
                pendingDetailPage.ClickSaveRejectNoteFailCase();
            }

            [Test]
            [TestCase(TestName = "Ravi_RejectLoanActionPendingDetailPage_8371_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_RejectLoanActionPendingDetailPage_8371_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRejectId();
                pendingDetailPage.SelectRejectNoteType();
                pendingDetailPage.EnterRejectNote();
                pendingDetailPage.AssertClickSaveRejectNoteFail();

            }


            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "SetWarningEnterBypass_8427")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            //[Category("RegressionTest")]
            //[Category("Warning")]
            public void SetWarningEnterBypass_8427()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, managerEmployee);
                var pendingDetailPage = new PendingDetail(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.ClickWarningSave();
                pendingDetailPage.ClickWarningClose();
                pendingDetailPage.PageRefresh();
                pendingDetailPage.OverrideUserName();
                pendingDetailPage.OverridePassword();
                pendingDetailPage.ClickWarningsOverrideAll();
            }

            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Parallelizable(ParallelScope.Self)]
            //[Category("RegressionTest")]
            [TestCase(TestName = "Rajesh_SetWarningEnterBypass_8427_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            public void Rajesh_SetWarningEnterBypass_8427_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.ClickWarningSave();
                pendingDetailPage.ClickWarningClose();
                pendingDetailPage.PageRefresh();
                pendingDetailPage.AssertWarningsDeleteFailCase();
            }


            [Test]
            [TestCase(TestName = "Ravi_SetWarningEnterBypass_8427_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_SetWarningEnterBypass_8427_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var pendingDetailPage = new PendingDetail(Driver, user);

                loginPage.LogInUser(user);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectWarning();
                pendingDetailPage.ClickAddButton();
                pendingDetailPage.SelectWarningType();
                pendingDetailPage.EnterWarningNotes();
                pendingDetailPage.ClickWarningSave();
                pendingDetailPage.ClickWarningClose();
                pendingDetailPage.PageRefresh();
                pendingDetailPage.AssertWarningsDeleteFail();

            }
            
            [Test]
            [TestCase(TestName = "EmailApprovalEmailFromApprovedDashboard_8431")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]

            [Category("CustomerDashboard")]
            public void EmailApprovalEmailFromApprovedDashboard_8431()
            {
                var advUser = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, advUser);
                var pendingDashboard = new PendingDashboard(Driver, advUser);
                var approvedDashboardPage = new ApprovedDashboard(Driver, advUser);

                loginPage.LogInUser(advUser);
            
                pendingDashboard.AssertIsPendingDashboardLoaded();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickContextmenu();
                approvedDashboardPage.ClickEmailApprovalPageLink();
                approvedDashboardPage.ClickDone();

            }
            [Test]
            [TestCase(TestName = "WithdrawApprovedLoanFromApprovedDashboard_10745")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]

            [Category("CustomerDashboard")]
            public void WithdrawApprovedLoanFromApprovedDashboard_10745()
            {
                var advancedCS = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advancedCS);
                var pendingDashboard = new PendingDashboard(Driver, advancedCS);
                var approvedDashboardPage = new ApprovedDashboard(Driver, advancedCS);

                loginPage.LogInUser(advancedCS);

                pendingDashboard.AssertIsPendingDashboardLoaded();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickContextmenu();
                approvedDashboardPage.SelectWithdrawApprovedLoanOption();
                approvedDashboardPage.ClickOkOnConfirmAppWithdrawalPopup();
                
            }
            [Test]
            [TestCase(TestName = "CancelWithdrawApprovedLoanFromApprovedDashboard_10746")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]

            [Category("CustomerDashboard")]
            public void CancelWithdrawApprovedLoanFromApprovedDashboard_10746()
            {
                var advancedCS = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advancedCS);
                var pendingDashboard = new PendingDashboard(Driver, advancedCS);
                var approvedDashboardPage = new ApprovedDashboard(Driver, advancedCS);

                loginPage.LogInUser(advancedCS);

                pendingDashboard.AssertIsPendingDashboardLoaded();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickContextmenu();
                approvedDashboardPage.SelectWithdrawApprovedLoanOption();
                approvedDashboardPage.ClickCancelOnConfirmAppWithdrawalPopup();

            }

            [Test]
            [TestCase(TestName = "Rajesh_EmailApprovalEmailFromCustomerDashboard_8431_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("RunEnvQA")]
            [Category("CustomerDashboard")]
            public void Rajesh_EmailApprovalEmailFromCustomerDashboard_8431_Fail()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, ownerEmployee);

                loginPage.LogInUser(ownerEmployee);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();
                
                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickContextmenu();
                approvedDashboardPage.ClickEmailApprovalPageLink();
                //approvedDashboardPage.ClickDoneFailCase();

            }
           
            [Test]
            [TestCase(TestName = "EmailApplicationLinkFromApplicationDetailPage_8438")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ApplicationDetailPage")]
            public void EmailApplicationLinkFromApplicationDetailPage_8438()
            {
                var advUser = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, advUser);
                var pendingDashboardPage = new PendingDashboard(Driver, advUser);
                var applicationDashboardPage = new ApplicationDashboard(Driver, advUser);
                var applicationDetailPage = new ApplicationDetail(Driver, advUser);

                loginPage.LogInUser(advUser);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickEmailApplicationLink();
                applicationDetailPage.ClickDone();

            }
            [Test]
            [TestCase(TestName = "Rajesh_EmailApplicationLinkFromApplicationDetailPage_8438_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ApplicationDetailPage")]
            public void Rajesh_EmailApplicationLinkFromApplicationDetailPage_8438_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickEmailApplicationLink();
                applicationDetailPage.ClickDoneFailCase();

            }

            [Test]
            [TestCase(TestName = "Ravi_EmailApplicationLinkFromApplicationDetailPage_8438_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ApplicationDetailPage")]
            public void Ravi_EmailApplicationLinkFromApplicationDetailPage_8438_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var applicationDashboardPage = new ApplicationDashboard(Driver, user);
                var applicationDetailPage = new ApplicationDetail(Driver, user);

                loginPage.LogInUser(user);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();
                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickEmailApplicationLink();
                applicationDetailPage.AssertClickDoneFail();

            }


            [Test]
            [TestCase(TestName = "PrintApplicationFormFromCustomerDetail_8446")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetail")]
            public void PrintApplicationFormFromCustomerDetail_8446()
            {
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, userCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, userCsrEmployee);

                loginPage.LogInUser(userCsrEmployee);
              
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickPrintApplication();
                customerDetailPage.ClickPrintApplicationPopup();

            }

            [Test]
            [TestCase(TestName = "Rajesh_PrintApplicationFormFromCustomerDetail_8446_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void Rajesh_PrintApplicationFormFromCustomerDetail_8446_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickPrintApplication();
                customerDetailPage.ClickPrintApplicationPopupFailCase();

            }

            [Test]
            [TestCase(TestName = "EmailApplicationFormFromCustomerDetailPage_8450")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void EmailApplicationFormFromCustomerDetailPage_8450()
            {

                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, userCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, userCsrEmployee);
                
                loginPage.LogInUser(userCsrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickPrintApplication();
                customerDetailPage.ClickEmailApplicationPopup();
                customerDetailPage.ClickDoneSuccessPopup();

            }

            [Test]
            [TestCase(TestName = "Rajesh_EmailApplicationFormFromCustomerDetailPage_8450_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_EmailApplicationFormFromCustomerDetailPage_8450_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalZeroBalance);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickPrintApplication();
                customerDetailPage.ClickEmailApplicationPopup();
                customerDetailPage.ClickDoneSuccessPopupFailCase();

            }


            [Test]
            [TestCase(TestName = "Ravi_EmailApplicationFormFromCustomerDetailPage_8450_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_EmailApplicationFormFromCustomerDetailPage_8450_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickPrintApplication();
                customerDetailPage.ClickEmailApplicationPopup();
                customerDetailPage.AssertClickDoneSuccessPopupFail();

            }

            [Test]
            [TestCase(TestName = "TextApplicationLinkAppDetailPage _8453")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ApplicationDetailPage")]
            public void TextApplicationLinkAppDetailPage_8453()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickTextApplicationLink();
                applicationDetailPage.ClickClosePopupButton();

            }
            [Test]
            [TestCase(TestName = "Rajesh_TextApplicationLinkAppDetailPage_8453_Fail")]
            [Author("Rajesh KR Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_TextApplicationLinkAppDetailPage_8453_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickTextApplicationLink();
                applicationDetailPage.ClickClosePopupButtonFailCase();

            }

            [Test]
            [TestCase(TestName = "Ravi_TextApplicationLinkAppDetailPage_8453")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwis.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ApplicationDetailPage")]
            public void Ravi_TextApplicationLinkAppDetailPage_8453()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickTextApplicationLink();
                //this will need to be updated once the text feature has been wired
                applicationDetailPage.AssertClickClosePopupButtonFail();

            }

            [Test]
            [Category("Ignore")]
            [Category("BUG 9916")] // THERE IS A DEFECT THAT IS PREVENTING US FROM USING THIS TEST SCRIPT \\
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "AddNoteCustomerDetailPage_8455")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            //[Category("RegressionTest")]
            //[Category("Note")]
            public void AddNoteCustomerDetailPage_8455()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var managerEmployee = Users.GetEmployee(EmployeeType.Manager);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                var searchCustomer = customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickNoteTab();
                customerDetailPage.ClickAddButton();
                customerDetailPage.SelectNoteType();
                customerDetailPage.EnterCustomerDetailNote();
                customerDetailPage.ClickSaveContactSection();
                customerDetailPage.LogOut();
                //cleanup test
                loginPage.LogInUser(managerEmployee);

                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();
                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(searchCustomer);

                customerDashboardPage.ClickGoToButton();
                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickNoteTab();
                customerDetailPage.DeleteNote();
            }
            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ViewCollectorNotesOnCustomerDetailPage_9589")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ViewCollectorNotesOnCustomerDetailPage_9589()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerEmail = "qa1@nostromorp.com";
                var customerId = "0U10-0000244";

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.CustomerDashboardEmail(customerId);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickNoteTab();
                //customerDetailPage.SelectNoteType();
                customerDetailPage.AssertDebtNoteIsVisible();


            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ItemsPerPagePendingDashboard_9594")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ItemsPerPagePendingDashboard_9594()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickItemsPerPageDropDown();
                pendingDashboardPage.SelectMinNumber();

            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ItemsPerPageApprovedDashboard_9599")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ItemsPerPageApprovedDashboard_9599()
            {
                var advancedCsrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advancedCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, advancedCsrEmployee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, advancedCsrEmployee);
                loginPage.LogInUser(advancedCsrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickItemsPerPageDropDown();
                approvedDashboardPage.SelectMinNumber();

            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ItemsPerPageApplicationDashboard_9600")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ItemsPerPageApplicationDashboard_9600()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickItemsPageDropDown();
                applicationDashboardPage.SelectMinNumber();


            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ItemsPerPageCustomerDashboard_9601")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ItemsPerPageCustomerDashboard_9601()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.ClickItemsPageDropDown();
                customerDashboardPage.SelectMidNumber();


            }

            [Test]
            [TestCase(TestName = "ManagerTemporaryPasswords_10596")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ManagerTemporaryPasswords_10596()
            {
                var ownEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownEmployee);
                var adminPage = new AdminPage(Driver, ownEmployee);

                var customerDashboardPage = new CustomerDashboard(Driver, ownEmployee);
                loginPage.LogInUser(ownEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectTemporaryPasswords();
                adminPage.GrabTemporaryPasswordList();
                var firstDisposablePassword = adminPage.StoreFirstTempPassword();
                adminPage.ClickCloseTemporaryPasswordsPopup();
                adminPage.ClickPersonPopUp();
                adminPage.ClickLogout();
                adminPage.LogInAsCustomerServiceAgent();

                var csrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                //var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                //var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var warningNote = "I am a warning Note";
                //pendingDashboardPage.ClickHamburgerMenu();
                //pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickWarningSection();
                customerDetailPage.ClickAddNewWarningButton();
                customerDetailPage.SelectWarningTypeManagerOverride();
                customerDetailPage.AddWarningNote(warningNote);
                customerDetailPage.ClickWarningSaveButton();
                customerDetailPage.PageRefresh();
                customerDetailPage.EnterManagerUserName();
                customerDetailPage.EnterTemporaryPassword(firstDisposablePassword);
                customerDetailPage.ClickOverrideAll();
                

            }
            
            [Test]
            [TestCase(TestName = "CopyPasteCustomerIDPendingDashboard_9591")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            // Last edit by Pat Holman 5/8/2020
            public void CopyPasteCustomerIDPendingDashboard_9591()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                var customerId = pendingDashboardPage.ClickCustomerIdClipBoard();
                pendingDashboardPage.PasteCustomerId(customerId);

            }

            [Test]
            [TestCase(TestName = "Rajesh_AddNoteCustomerDetailPage_8455_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            public void Rajesh_AddNoteCustomerDetailPage_8455_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickNoteTab();
                customerDetailPage.ClickAddButton();
                customerDetailPage.SelectNoteType();
                customerDetailPage.EnterCustomerDetailNote();
                customerDetailPage.AssertClickSaveButtonFail();

            }

            [Test]
            [TestCase(TestName = "Ravi_AddNoteCustomerDetailPage_8455_Fail")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_AddNoteCustomerDetailPage_8455_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickNoteTab();
                customerDetailPage.ClickAddButton();
                customerDetailPage.SelectNoteType();
                customerDetailPage.EnterCustomerDetailNote();
                customerDetailPage.AssertClickSaveButtonFailCondition();

            }
            
            [Test]
            [TestCase(TestName = "ShowClosedLoansCustomerDetailPag_8459")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void ShowClosedLoansCustomerDetailPag_8459()
            {
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, userCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, userCsrEmployee);

                loginPage.LogInUser(userCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickShowClosedLoans();
            }
            [Test]
            [TestCase(TestName = "Rajesh_ShowClosedLoansCustomerDetailPag_8459_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_ShowClosedLoansCustomerDetailPag_8459_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickShowClosedLoansFailCase();
            }

            [Test]
            [TestCase(TestName = "EmailSignedDisclosuresCustomerDetailPage_8515")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void EmailSignedDisclosuresCustomerDetailPage_8515()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerWithActivePaydayLoan =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerWithActivePaydayLoan);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.ClickGoToOptionOnSignedDisclosure();
                customerDetailPage.ClickSignedDisclosureToCustomer();
            }

            [Test]
            [Ignore("Test Broke", Until = "2020-03-09 12:00:00Z")]
            [TestCase(TestName = "Rajesh_EmailSignedDisclosuresCustomerDetailPage_8515_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void Rajesh_EmailSignedDisclosuresCustomerDetailPage_8515_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.ClickGoButton();
                customerDetailPage.ClickSignedDisclosureToCustomerFailCase();
            }

            [Test]
            [TestCase(TestName = "Ravi_EmailSignedDisclosuresCustomerDetailPage_8515_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_EmailSignedDisclosuresCustomerDetailPage_8515_Fail()
            {

                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.AssertClickSignedDisclosureToCustomerFail();
            }

           [Test]
           [TestCase(TestName = "LMSMonthlyPayFrequencySpecificWeekday_10112b")]
           [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
           // [Ignore("Test Broke", Until = "2020-03-09 12:00:00Z")]
           [Parallelizable(ParallelScope.Self)]
           [Category("RegressionTest")]
           [Category("CustomerDetailPage")]
           public void LMSMonthlyPayFrequencySpecificWeekday_10112b()
           {

               var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
               var loginPage = new LoginPage(Driver, user);
               var pendingDashboardPage = new PendingDashboard(Driver, user);
               var customerDashboardPage = new CustomerDashboard(Driver, user);
               var customerDetailPage = new CustomerDetail(Driver, user);
               var customerPaydayWithBalance = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
               
               loginPage.LogInUser(user);
                
               pendingDashboardPage.AssertIsPendingDashboardLoaded();
               pendingDashboardPage.ClickHamburgerMenu();
               pendingDashboardPage.SelectCustomerDashboard();

               customerDashboardPage.AssertCustomerDashboardLoaded();
               customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
               customerDashboardPage.ClickGoToButton();

               customerDetailPage.AssertCustomerDetailVisible();
               customerDetailPage.ClickEmploymentTab();
               customerDetailPage.ClickOnEmploymentName();
               customerDetailPage.ClickHowOftenAreYouPaid();
               customerDetailPage.ClickMonthlyPatternSpecificWeekday();
               customerDetailPage.ClickWeek();
               customerDetailPage.ClickWeekDay();
               customerDetailPage.ClickRecentPayday();
               customerDetailPage.ClickSaveButtonEmploymentSection();
           }

           [Test]
           [TestCase(TestName = "LMSMonthlyPayFrequencySpecificDay_10111a")]
           [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
           //trying to get this code back 05/18/2020
           [Parallelizable(ParallelScope.Self)]
           [Category("RegressionTest")]
           [Category("CustomerDetailPage")]
           public void LMSMonthlyPayFrequencySpecificDay_10111a()
           {

               var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
               var loginPage = new LoginPage(Driver, user);
               var pendingDashboardPage = new PendingDashboard(Driver, user);
               var customerDashboardPage = new CustomerDashboard(Driver, user);
               var customerDetailPage = new CustomerDetail(Driver, user);
               var customerPaydayWithBalance = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
               
               loginPage.LogInUser(user);
                
               pendingDashboardPage.AssertIsPendingDashboardLoaded();
               pendingDashboardPage.ClickHamburgerMenu();
               pendingDashboardPage.SelectCustomerDashboard();

               customerDashboardPage.AssertCustomerDashboardLoaded();
               customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
               customerDashboardPage.ClickGoToButton();

               customerDetailPage.AssertCustomerDetailVisible();
               customerDetailPage.ClickEmploymentTab();
               customerDetailPage.ClickOnEmploymentName();
               customerDetailPage.ClickHowOftenAreYouPaid();
               customerDetailPage.ClickMonthlyPatternSpecificDay();
               customerDetailPage.ClickRecentPaydaySpecificDay();
               customerDetailPage.ClickSaveButtonEmploymentSection();
           }
            

            [Test]
            [TestCase(TestName = "ClickShowProposalLogInTheLoanSectionCustomerDetailPage_8518")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void ClickShowProposalLogInTheLoanSectionCustomerDetailPage_8518()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var paydayLoanWithBalance = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(paydayLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.SelectShowProposalLogItems();
                customerDetailPage.AssertTransactionTextIsVisible();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToLoansTransactionPersonalLoan_9365")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoansTransactionPersonalLoan_9365()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerpersonalWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.AssertTransactionTextIsVisible();


            }
            [Test]
            [TestCase(TestName = "NavigateToAttachmentsCustomerDetailPageAndViewAttachments_9937")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToAttachmentsCustomerDetailPageAndViewAttachments_9937()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerWithAttachment = "qa1@nostromorp.com";

                loginPage.LogInUser(csrEmployee);


                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.CustomerDashboardEmail(customerWithAttachment);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickAttachmentsTab();
                customerDetailPage.ClickViewAttachments();
                

            }

            [Test]
            [TestCase(TestName = "NavigateToCustomerDetailFromLoanInventoryPageByCustomer_ID_9869")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToCustomerDetailFromLoanInventoryPageByCustomer_ID_9869()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);
                var customerDetailPage = new CustomerDetail(Driver, adminEmployee);
                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(adminEmployee);

                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickCustomerId();

                customerDetailPage.AssertCustomerDetailVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToCustomerDetailFromLoanInventoryPageByCustomerName_9870")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToCustomerDetailFromLoanInventoryPageByCustomerName_9870()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);
                var customerDetailPage = new CustomerDetail(Driver, adminEmployee);

                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickCustomerName();

                customerDetailPage.AssertCustomerDetailVisible();

            }
            [Test]
            [TestCase(TestName = "CopyPasteLoanIDOnCustomerIDLoanInventoryPage_10015")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void CopyPasteLoanIDOnCustomerIDLoanInventoryPage_10015()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);


                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickClipBoardOnCustomerID();
                loanInventoryPage.PasteCustomerIdInCustomerIdField();

            }

            [Test]
            [TestCase(TestName = "ClickLoanIDPasteIdInLoanIdInputFieldLoanInventoryPage_10016")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void ClickLoanIDPasteIdInLoanIdInputFieldLoanInventoryPage_10016()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanIdClipBoard();
                loanInventoryPage.PasteLoanIdNumberInLoanIdField();

            }
            [Test]
            [TestCase(TestName = "ClickExportFileLoanInventoryFile_10017")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void ClickExportFileLoanInventoryFile_10017()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);


                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickExportFile();


            }

            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusFundedUtah_9872")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusFundedUtah_9872()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectFunded();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectState();
                
            }
            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusApprovedTexas_9873")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusApprovedTexas_9873()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectApproved();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectTexas();
                
            }

            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusLateWyoming_9874")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusLateWyoming_9874()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectLateAsTheStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectWyoming();


            }
            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusClosedIdaho_9875")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusClosedIdaho_9875()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectClosedAsTheStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectIdaho();

            }
            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusDefaulted_9876")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusDefaulted_9876()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventory = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventory.AssertIsLoanInventory();
                loanInventory.ClickLoanStatus();
                loanInventory.SelectDefaultedAsTheStatus();

            }
            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusRescinded_9877")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusRescinded_9877()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectRescindedAsLoanStatus();


            }
            [Test]
            [TestCase(TestName = "LoanInventoryLoanStatusLateMissouri_9909")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void LoanInventoryLoanStatusLateMissouri_9909()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var loanInventoryPage = new LoanInventory(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectLateAsTheStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectMo();
            }

         
            [Test]
            [TestCase(TestName = "Ravi_ToggleShowProposalLogItems_8518")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_ToggleShowProposalLogItems_8518()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerpersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerpersonalWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.SelectShowProposalLogItems();
                customerDetailPage.UnSelectShowProposalLogItems();
            }

            [Test]
            [TestCase(TestName = "PersonalLoanPaymentACHCustomerDetailPage_8520")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void PersonalLoanPaymentACHCustomerDetailPage_8520()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                
                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible(); // there should be  dummy data on loan section
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectLoanPayment();
                customerDetailPage.ClickPaymentMethodByAch();
                customerDetailPage.EnterPayDownAmount(customerPersonalWithBalance);
                customerDetailPage.ClickPayDown();
                customerDetailPage.ClickOkay();
            }
            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PersonalLoanPaymentDebitCardCustomerDetailPage_9271")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("MakePayment")]
            public void PersonalLoanPaymentDebitCardCustomerDetailPage_9271()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                //var customerWithPersonalLoanBalance = "Stg5@nostromorp.com";

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();
                customerDashboardPage.AssertCustomerDashboardLoaded();

                customerDashboardPage.EnterSearchByEmail(customerPersonalWithBalance);
                customerDashboardPage.ClickGoToButton();
                
                customerDetailPage.AssertCustomerDetailVisible(); // there should be  dummy data on loan section
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.ClickLoanPayment();
                customerDetailPage.PaymentMethodDebitCard(customerPersonalWithBalance);
                customerDetailPage.CardType(customerPersonalWithBalance);
                customerDetailPage.Last4Digits(customerPersonalWithBalance);
                customerDetailPage.PayDownAmount();
                customerDetailPage.ClickPayDownPersonalLoan();
                customerDetailPage.ClickOkay();
            }

            [Test]
            [TestCase(TestName = "Rajesh_LoanIDInLoanSection_9136")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            public void Rajesh_LoanIDInLoanSection_9136()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var approvedCustomerDashboardEmail = "qa1@nostromorp.com";

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmailApprovedCustomer(approvedCustomerDashboardEmail);
                customerDashboardPage.ClickGoToButton();

                //customer detail Ripley Approved
                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.AssertLoanIdInLoanSectionVisible();
            }

            [Test]
            [TestCase(TestName = "NavigateToPersonalPaymentSchedule_9486")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void NavigateToPersonalPaymentSchedule_9486()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible(); // there should be  dummy data on loan section
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.ClickLoanPayment();
                customerDetailPage.SelectPaymentScheduleOnLoanPayDownPopUp();
                customerDetailPage.ClickOk();

            }

            [Test]
            [TestCase(TestName = "NavigateToPersonalLoanLoanSchedule_9925")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void NavigateToPersonalLoanLoanSchedule_9925()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();
                customerDetailPage.AssertCustomerDetailVisible(); // there should be  dummy data on loan section
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectLoanSchedule();
                
            }
            
            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "HoldPaydayLoanCustomerDetailPage_8535")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void HoldPaydayLoanCustomerDetailPage_8535()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(user);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPaydayHold();
                customerDetailPage.EnterNextDayForHold();
                customerDetailPage.ClickSaveButtonPayday();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.AssertCollateralHeldUntilIsVisible();

            }
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "LMSPaydayLoansRefinanceIntoAnEPPAsManager_10163")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void LMSPaydayLoansRefinanceIntoAnEPPAsManager_10163()
            {
                var managerUser = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerUser);
                var adminPage = new AdminPage(Driver, managerUser);
                var customerDashboardPage = new CustomerDashboard(Driver, managerUser);
                var customerDetailPage = new CustomerDetail(Driver, managerUser);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(managerUser);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu(); 
                adminPage.ClickCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectEPPOption();
                customerDetailPage.ClickYesOnEmploymentStatus();
                customerDetailPage.ClickEmailOnEppLoanPopup();
                //customerDetailPage.ClickFirstPaymentDueDateOnEPPLoanPopup();
                //customerDetailPage.SelectFirstPaymentOptionOnEPPLoanPopup();
                //customerDetailPage.ClickSaveOnPaymentDueDateEPPLoanPopup();
                customerDetailPage.ClickOkayOnEPPRequestSubmittedPopup();
                customerDetailPage.AssertPendingEPPRequestIsVisible();
                
            }



            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "LMSPaydayLoansRefinanceIntoAnEPPAsLoanProcessor_10163b")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void LMSPaydayLoansRefinanceIntoAnEPPAsLoanProcessor_10163b()
            {
                var LoanProcessorUser = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, LoanProcessorUser);
                var pendingDashboardPage = new PendingDashboard(Driver, LoanProcessorUser);
                var customerDashboardPage = new CustomerDashboard(Driver, LoanProcessorUser);
                var customerDetailPage = new CustomerDetail(Driver, LoanProcessorUser);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(LoanProcessorUser);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectEPPOption();
                customerDetailPage.ClickYesOnEmploymentStatus();
                customerDetailPage.ClickEmailOnEppLoanPopup();
                //customerDetailPage.ClickFirstPaymentDueDateOnEPPLoanPopup();
                //customerDetailPage.SelectFirstPaymentOptionOnEPPLoanPopup();
                //customerDetailPage.ClickSaveOnPaymentDueDateEPPLoanPopup();
                customerDetailPage.ClickOkayOnEPPRequestSubmittedPopup();
                customerDetailPage.AssertPendingEPPRequestIsVisible();

            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "NavigateToRefinanceDetailsCustomerDetailPage_11030")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToRefinanceDetailsCustomerDetailPage_11030()
            {
                var advanceCustomerUser = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advanceCustomerUser);
                var pendingDashboardPage = new PendingDashboard(Driver, advanceCustomerUser);
                var customerDashboardPage = new CustomerDashboard(Driver, advanceCustomerUser);
                var customerDetailPage = new CustomerDetail(Driver, advanceCustomerUser);
                var customerRefiancedPersonalLoan = "stg4@nostromorp.com";

                loginPage.LogInUser(advanceCustomerUser);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();
                

                customerDashboardPage.EnterSearchByEmailApprovedCustomer(customerRefiancedPersonalLoan);
                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectRefinanceDetails();
                customerDetailPage.AssertRefinanceDetailsIsVisible();

            }
            
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "BlockWebAccess_10166")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void BlockWebAccess_10166()
            {
                var managerUser = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerUser);
                var adminPage = new AdminPage(Driver, managerUser);
                var customerDashboardPage = new CustomerDashboard(Driver, managerUser);
                var customerDetailPage = new CustomerDetail(Driver, managerUser);
                //var customerPaydayWithBalance =
                //  CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                //var customerPaydayWithBalance = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var paydayLoanWithBalance = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(managerUser);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.ClickCustomerDashboard();

                customerDashboardPage.EnterSearchByEmail(paydayLoanWithBalance);
                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickBlockWebAccess();
                customerDetailPage.ClickYesBlockWebAccessPopup();
                //customerDetailPage.ClickEnvelopeByEmailAddress();
                
            }
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "NavigateToDefaultedLoansFromLoanInventoryPagePaydayLoanStateWy_10168")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToDefaultedLoansFromLoanInventoryPagePaydayLoanStateWy_10168()
            {
                var manEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, manEmployee);
                var adminPage = new AdminPage(Driver, manEmployee);
                var loanInventoryPage = new LoanInventory(Driver, manEmployee);
                var customerDetailPage = new CustomerDetail(Driver, manEmployee);
                
                loginPage.LogInUser(manEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectDefaultedAsTheStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectWyoming();
                loanInventoryPage.ClickCustomerName();
                
                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.AssertDebtTypeIsVisible();

            }
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "NavigateToLateLoansFromLoanInventoryPagePaydayLoanStateTX_10170")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToLateLoansFromLoanInventoryPagePaydayLoanStateTX_10170()
            {
                var manEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, manEmployee);
                var adminPage = new AdminPage(Driver, manEmployee);
                var loanInventoryPage = new LoanInventory(Driver, manEmployee);
                var customerDetailPage = new CustomerDetail(Driver, manEmployee);
                
                loginPage.LogInUser(manEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectLateAsTheStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectTexas();
                loanInventoryPage.ClickCustomerName();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.AssertLoanLateStatusIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToCollectionsDashboard_10557")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsDashboard_10557()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                
                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();
                
                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                
            }
            [Test]
            [TestCase(TestName = "NavigateToCommunicationsSectionsInCMS_10673")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCommunicationsSectionsInCMS_10673()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminDashboardPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsCommunicationsPage = new CollectionsCommunications(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminDashboardPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminDashboardPage.ClickCommunicationsButton();
                
                collectionsCommunicationsPage.AssertCollectionsCommunicationsPageLoaded();

            }
            
           
            [Test]
            [TestCase(TestName = "NavigateToDocumentRequestSectionInCollections_10692")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToDocumentRequestSectionInCollections_10692()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                
                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();

            }

            [Test]
            [TestCase(TestName = "NavigateToDisclosureRequestPageCms_10312")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToDisclosureRequestPageCms_10312()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                var disclosureRequestPage = new DisclosureRequestPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();
                collectionsDocumentRequestPage.ClickGoToButtonForDisclosureRequestPage();

                disclosureRequestPage.AssertDisclosureRequestPageLoaded();

            }
            [Test]
            [TestCase(TestName = "NavigateToApplicationRequestPageCms_10524")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToApplicationRequestPageCms_10524()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                var applicatonRequestPage = new ApplicationRequestPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();
                collectionsDocumentRequestPage.ClickGoToButtonForApplicationRequestPage();

                applicatonRequestPage.AssertApplicationRequestPageLoaded();

            }

           
            [Test]
            [TestCase(TestName = "NavigateToCollectionsUserManagementPage_10586")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsUserManagementPage_10586()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsUserManagementPage = new CollectionsUserManagementPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickCollectionsUserManagementButton();

                collectionsUserManagementPage.AssertCollectionsUserManagementPageLoaded();

            }
            [Test]
            [TestCase(TestName = "NavigateToCollectionsDebtorDetailPage_10588")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsDebtorDetailPage_10588()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsDebtorDetailPage = new CollectionsDebtorDetailPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickCustomerId();

                collectionsDebtorDetailPage.AssertCollectionsDebtorDetailPageLoaded();

            }
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "NavigateToRescindLoansFromLoanInventoryPagePaydayLoanStateUT_10172")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToRescindLoansFromLoanInventoryPagePaydayLoanStateUT_10172()
            {
                var manEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, manEmployee);
                var adminPage = new AdminPage(Driver, manEmployee);
                var loanInventoryPage = new LoanInventory(Driver, manEmployee);
                var customerDetailPage = new CustomerDetail(Driver, manEmployee);

                loginPage.LogInUser(manEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectRescindedAsLoanStatus();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectUtah();
                loanInventoryPage.ClickCustomerName();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickShowClosedLoans();
                customerDetailPage.AssertLoanRescindedStatusIsVisible();

            }
            [Test]
            //[Category("Ignore")]
            [TestCase(TestName = "NavigateToApprovedApplicationsOnCustomerDetailPageFromLoanInventoryPagePaydayLoanStateID_10173")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToApprovedApplicationsOnCustomerDetailPageFromLoanInventoryPagePaydayLoanStateID_10173()
            {
                var manEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, manEmployee);
                var adminPage = new AdminPage(Driver, manEmployee);
                var loanInventoryPage = new LoanInventory(Driver, manEmployee);
                var customerDetailPage = new CustomerDetail(Driver, manEmployee);

                loginPage.LogInUser(manEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectLoanInventory();

                loanInventoryPage.AssertIsLoanInventory();
                loanInventoryPage.ClickLoanStatus();
                loanInventoryPage.SelectApproved();
                loanInventoryPage.ClickState();
                loanInventoryPage.SelectIdaho();
                loanInventoryPage.ClickCustomerName();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.UnCheckOpenApplicationsOnly();
                customerDetailPage.AssertApplicationApprovedApplicationIsVisible();

            }
            [Test]
            //[Category("Ignore")]
           // [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "HoldPersonalLoanCustomerDetailPage_9367")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            public void HoldPersonalLoanCustomerDetailPage_9367()
            {
                
                var user = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectHoldAch();
                customerDetailPage.ClickSaveButtonPersonalLoan();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.AssertHoldInstallmentPaymentIsVisible();

            }
            [Test]
            [TestCase(TestName = "VerifyAdverseActionDocument_10498")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            public void VerifyAdverseActionDocument_10498()
            {
              
                var advanceCustomerService = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var customerHelper = new CustomerHelper(Driver, advanceCustomerService);
                //var customerId = customerHelper.BuildCustomerTurndownAccounts(Driver, advanceCustomerService);
                var customer = GenNewRandomMember(CustomerType.UT_Turndown);
                CheckCityOnline.Tests.BaseTest.CreateTurndownCustomer(Driver, customer);
                var loanApprovalPage = new CheckCityOnline.Pages.LoanApprovalPage(Driver, customer);
                //assert turndown
                loanApprovalPage.AssertPersonalLoanTurnDown();
                //navigate to user profile
                NavigateToCustomerProfilePage(customer);
                //grab customerid
                var storeProfilePage = new StoreProfile(Driver, customer);
                var customerId = storeProfilePage.FindCustomerId();
                //LoginToLMS
                LoginToLms(advanceCustomerService);
                //NavigateToCustomerDashboardPage
                NavigateToCustomerDashboardPage(advanceCustomerService);
                NavigateToCustomerDetailPage(advanceCustomerService, customerId);
                //VerifyAdverseActionDocumentIsVisible
                
                AdverseActionDocumentIsVisible(advanceCustomerService);
                
            }

            private void AdverseActionDocumentIsVisible(Users.CurrentUser advanceCustomerService)
            {
                
                var customerDetailPage = new CustomerDetail(Driver, advanceCustomerService);
                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.ClickShowDeniedApplications();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.SelectAdverseAction();
                customerDetailPage.AssertAdverseActionDocumentIsVisible();
            }

           
            private void NavigateToCustomerDetailPage(Users.CurrentUser advanceCustomerService, string customerId)
            {
                var customerDashboardPage = new CustomerDashboard(Driver, advanceCustomerService);
               customerDashboardPage.SearchByCustomerId(customerId);
               customerDashboardPage.ClickGoToButton();

            }

            private void NavigateToCustomerDashboardPage(Users.CurrentUser advanceCustomerService)
            {
               var pendingDashboardPage = new PendingDashboard(Driver, advanceCustomerService);
               var customerDashboardPage = new CustomerDashboard(Driver, advanceCustomerService);
               pendingDashboardPage.ClickHamburgerMenu();
               pendingDashboardPage.SelectCustomerDashboard();
               customerDashboardPage.AssertCustomerDashboardLoaded();
            }

            private void LoginToLms(Users.CurrentUser advanceCustomerService)
            {
                
                var loginPage = new LoginPage(Driver, advanceCustomerService);
                var pendingDashboardPage = new PendingDashboard(Driver, advanceCustomerService);
                loginPage.LogInUser(advanceCustomerService);
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
            }
            
            private void NavigateToCustomerProfilePage(CurrentUser customer)
            {
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var memberPage = new CheckCityOnline.Pages.MemberPage(Driver, customer);
                loanApprovalPage.ClickAccount();

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickProfileUpdated();
                
            }
            
            [Test]
            [TestCase(TestName = "Rajesh_HoldLoanCustomerDetailPage_8535_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_HoldLoanCustomerDetailPage_8535_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
              
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();

                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPaydayHold();
                customerDetailPage.ClickSaveButtonFailCase();
            }

            [Test]
            [TestCase(TestName = "Ravi_HoldLoanCustomerDetailPage_8535_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_HoldLoanCustomerDetailPage_8535_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(user);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPaydayHold();
                customerDetailPage.AssertClickSaveButtonFail();

            }
            [Test]
            [TestCase(TestName = "PayoffInquiryCustomerDetailPagePaydayLoan_8548")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("RunEnvQA")]
            [Category("CustomerDetailPage")]
            public void PayoffInquiryCustomerDetailPagePaydayLoan_8548()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPayoffInquiry();
                customerDetailPage.EnterPayoffInquiryDate();
                customerDetailPage.ClickClose();


            }
            [Test]
            [TestCase(TestName = "PayoffInquiryCustomerDetailPagePersonalLoan_9364")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("RunEnvQA")]
            [Category("CustomerDetailPage")]
            public void PayoffInquiryCustomerDetailPagePersonalLoan_9364()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPadayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPadayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPayoffInquiry();
                customerDetailPage.EnterPayoffInquiryDate();

            }
            [Test]
            [Ignore("Test Broke", Until = "2020-03-09 12:00:00Z")]
            [TestCase(TestName = "Rajesh_PayoffInquiryCustomerDetailPagePaydayLoan_8548_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void Rajesh_PayoffInquiryCustomerDetailPagePaydayLoan_8548_Fail()
            {

                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPayoffInquiry();
                customerDetailPage.EnterPayoffInquiryDate();
                customerDetailPage.ClickCloseFailCase();

            }

            [Test]
            [TestCase(TestName = "PersonalLoanPayoffLoanByACHCustomerDetailPage_8571")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void PersonalLoanPayoffLoanByACHCustomerDetailPage_8571()
            {
                var csrCustomer = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrCustomer);
                var pendingDashboardPage = new PendingDashboard(Driver, csrCustomer);
                var customerDashboardPage = new CustomerDashboard(Driver, csrCustomer);
                var customerDetailPage = new CustomerDetail(Driver, csrCustomer);
                var customerPersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrCustomer);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectLoanPayoff();
                customerDetailPage.ClickPaymentMethodByAch();
                customerDetailPage.ClickVerifyOnEmail();
                customerDetailPage.ClickPayOff();
                customerDetailPage.ClickConfirm();
                customerDetailPage.ClickOkay();

            }

            [Test]
            [TestCase(TestName = "Ravi_MakePaymentPayoffACHCustomerDetailPage_8571_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_MakePaymentPayoffACHCustomerDetailPage_8571_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayZeroBalance);

                loginPage.LogInUser(user);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickLoansTab();
                customerDetailPage.AssertNoLoansForThisCustomer("Validate that Loadn Transaction popup is not displayed.");
                
            }

            [Test]
            [TestCase(TestName = "PaydayLoanPayoffLoanByACHCustomerDetailPage_9269")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("MakePayment")]
            public void PaydayLoanPayoffLoanByACHCustomerDetailPage_9269()
            {
                var csrCustomer = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrCustomer);
                var pendingDashboardPage = new PendingDashboard(Driver, csrCustomer);
                var customerDashboardPage = new CustomerDashboard(Driver, csrCustomer);
                var customerDetailPage = new CustomerDetail(Driver, csrCustomer);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                
                loginPage.LogInUser(csrCustomer);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPickup();
                customerDetailPage.ClickPaymentMethodByAch();
                customerDetailPage.ClickVerifyOnEmail();
                customerDetailPage.ClickPickUp();
                customerDetailPage.ClickConfirm();
                customerDetailPage.ClickOk();
            }

            [Test]
            [TestCase(TestName = "NavigateToSystemConfiguration_8575")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void NavigateToSystemConfiguration_8575()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var enterpriseSettingsPage = new SystemConfiguration(Driver, ownerEmployee);

                loginPage.LogInUser(ownerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSystemConfiguration();

                enterpriseSettingsPage.AssertIsSystemConfigurationVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToDocumentManagementPage_8578")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDocumentManagementPage_8578()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var documentManagementPage = new DocumentManagement(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
              

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();
                documentManagementPage.AssertIsDocumentManagementVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToDocumentManagementCategoryPage_10628")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDocumentManagementCategoryPage_10628()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var documentManagementPage = new DocumentManagement(Driver, adminEmployee);
                var documentMangementCategoriesPage = new DocumentManagementCategoryPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickCategoriesButton();
                
                documentMangementCategoriesPage.AssertIsDocumentManagementCategoryPageLoaded();
               
            }
            [Test]
            [TestCase(TestName = "NavigateToStaticTemplatesPage_10776")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToStaticTemplatesPage_10776()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var documentManagementPage = new DocumentManagement(Driver, adminEmployee);
                var staticTemplatesPage = new StaticTemplatesPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickStaticTemplatesButton();

                staticTemplatesPage.AssertIsStaticTemplatesPageLoaded();
                
            }
            [Test]
            [TestCase(TestName = "AddCategoryThenDeleteCatagoryTest_10709")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void AddCategoryThenDeleteCatagoryTest_10709()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var documentManagementPage = new DocumentManagement(Driver, adminEmployee);
                var documentMangementCategoriesPage = new DocumentManagementCategoryPage(Driver, adminEmployee);
                var categoriesEditPage = new CategoriesEditPage(Driver, adminEmployee);
                var testName = "Test10709a";
                var descriptionOfTest = "AddDelete";
                loginPage.LogInUser(adminEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickCategoriesButton();

                documentMangementCategoriesPage.AssertIsDocumentManagementCategoryPageLoaded();
                documentMangementCategoriesPage.ClickPlusButton();

                categoriesEditPage.AssertIsCategoriesEditPageLoaded();
                categoriesEditPage.ClickNameField();
                categoriesEditPage.EnterTestName(testName);
                categoriesEditPage.ClickDescriptionField();
                categoriesEditPage.EnterDescription(descriptionOfTest);
                categoriesEditPage.ClickSaveButton();

                documentMangementCategoriesPage.AssertIsDocumentManagementCategoryPageLoaded();
                //clicksearchfield
                //entertestname
                //clickcontextmenu
                //clickDelete
                //assertTestnameisGone
                
            }
            [Test]
            [TestCase(TestName = "NavigateToUserManagementPage_8580")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToUserManagementPage_8580()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var userManagementPage = new UserManagement(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickUserManagement();
                userManagementPage.AssertIsUserManagementVisible();
            }
            
            [Test]
            [TestCase(TestName = "NavigateToQueueManagement_8582")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToQueueManagement_8582()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var queueManagementPage = new QueueManagement(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickQueueManagement();
                queueManagementPage.AssertIsQueueManagementVisible();
            }
            
            [Test]
            [TestCase(TestName = "NavigateToUserRolesManagement_8584")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToUserRolesManagement_8584()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var userRolesManagementPage = new UserRolesManagement(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickUserRolesManagement();
                userRolesManagementPage.AssertIsUserRolesManagementVisible();

            }
            
            [Test]
            [TestCase(TestName = "ResetPasswordFromCustomerDetailPage_8666")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ResetPassword")]
            public void ResetPasswordFromCustomerDetailPage_8666()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                loginPage.SelectLocation();
                loginPage.ClickAndSelectClose();

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickResetPassword();
                customerDetailPage.ClickConfirm();
                customerDetailPage.ClickAcknowledgedButton();

            }
            [Test]
            [TestCase(TestName = "Rajesh_ResetPasswordFromCustomerDetailPage_8666_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_ResetPasswordFromCustomerDetailPage_8666_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickRightHamburgerMenu();
                customerDetailPage.ClickResetPassword();
                customerDetailPage.ClickConfirm();
                customerDetailPage.ClickAcknowledgedButtonFailCase();

            }

           
            
            [Test]
            [TestCase(TestName = "PasswordResetPendingDetailPage_8668")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("ResetPassword")]
            public void PasswordResetPendingDetailPage_8668()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectResetPassword();
                pendingDetailPage.ClickConfirmButton();
                pendingDetailPage.ClickAcknowledged();

            }
            [Test]
            [TestCase(TestName = "SelectPaydayAsLoanTypeOnPendingDashboard_9605")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan Type Payday")]
            public void SelectPaydayAsLoanTypeOnPendingDashboard_9605()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickLoanTypeDropDown();
                pendingDashboardPage.SelectPaydayAsLoanType();

            }
            [Test]
            [TestCase(TestName = "ClockInandOutViewLog_11027")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan Type Payday")]
            public void ClockInandOutViewLog_11027()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickClockInButton();
                pendingDashboardPage.EnterEmployeePassword();
                pendingDashboardPage.ClickOkButton();
                pendingDashboardPage.ClickClockOutButton();
                pendingDashboardPage.ClickAgentToolsIcon();
                pendingDashboardPage.SelectTimeCardButton();
                pendingDashboardPage.AssertTimecardPopupIsVisible();


            }

            [Test]
            [TestCase(TestName = "AddTimeAndDateRecord_11031")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan Type Payday")]
            public void AddTimeAndDateRecord_11031()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                

                loginPage.LogInUser(adminEmployee);

               adminPage.AssertIsAdminPageVisible();
               adminPage.ClickUserManagement();
               adminPage.ClickSearchByField();
               adminPage.EnterEmailAddress();
               adminPage.ClickContextMenu();
               adminPage.SelectTimeCard();
               adminPage.ClickAddNewButton();
               adminPage.ClickClockInField();
               adminPage.ClickHours();
               adminPage.ClickSetOnTimeIn();
               adminPage.ClickClockOutField();
               adminPage.clickSetOnTimeOut();
               adminPage.ClickSave();
                
            }


            [Test]
            [TestCase(TestName = "DeleteTimeAndDateRecord_11032")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan Type Payday")]
            public void DeleteTimeAndDateRecord_11032()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickUserManagement();
                adminPage.ClickSearchByField();
                adminPage.EnterEmailAddress();
                adminPage.ClickContextMenu();
                adminPage.SelectTimeCard();
                adminPage.ClickAddNewButton();
                adminPage.ClickClockInField();
                adminPage.ClickHours();
                adminPage.ClickSetOnTimeIn();
                adminPage.ClickClockOutField();
                adminPage.clickSetOnTimeOut();
                adminPage.ClickSave();
                adminPage.ClickContextMenuOnTimeCardPopup();
                adminPage.SelectDelete();
                adminPage.ClickDeleteConfirm();

            }

            [Test]
            [TestCase(TestName = "SelectPersonalAsLoanTypeOnPendingDashboard_9607")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan Type Personal")]
            public void SelectPersonalAsLoanTypeOnPendingDashboard_9607()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickLoanTypeDropDown();
                pendingDashboardPage.SelectPersonalLoanAsLoanType();

            }
            [Test]
            [TestCase(TestName = "FilterLoanApplicationsByStatePendingDashboard_9608")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan StateColumn")]
            public void FilterLoanApplicationsByStatePendingDashboard_9608()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickState();
                pendingDashboardPage.SelectStateWY();

            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "FilterLoanPaydayStateUT_9618")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan StateColumn")]
            public void FilterLoanPaydayStateUT_9618()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickLoanTypeDropDown();
                pendingDashboardPage.SelectPaydayAsLoanType();
                
                pendingDashboardPage.ClickState();
                pendingDashboardPage.SelectStateUT();

            }
            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "FilterLoanPersonalLoanStateUT_9619")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan StateColumn")]
            public void FilterLoanPersonalLoanStateUT_9619()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickLoanTypeDropDown();
                pendingDashboardPage.SelectPersonalLoanAsLoanType();
                pendingDashboardPage.ClickState();
                pendingDashboardPage.SelectStateUT();

            }
            [Test]
            [TestCase(TestName = "FilterLoanByStateTexasPendingDashboard_9620")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("Loan StateColumn")]
            public void FilterLoanByStateTexasPendingDashboard_9620()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickState();
                pendingDashboardPage.SelectStateTx();
            }

            [Test]
            [TestCase(TestName = "Rajesh_PasswordResetPendingDetailPage_8668_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("ResetPassword")]
            public void Rajesh_PasswordResetPendingDetailPage_8668_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                //var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectResetPassword();
                pendingDetailPage.ClickConfirmButton();
                pendingDetailPage.ClickAcknowledgedFailCase();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToQueueManagementSettingsPage_8670")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToQueueManagementSettingsPage_8670()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var queueManagementPage = new QueueManagement(Driver, userManagerEmployee);
                var queueSettingsPage = new QueueSettings(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickQueueManagement();

                queueManagementPage.AssertIsQueueManagementVisible();
                queueManagementPage.ClickSettings();

                queueSettingsPage.AssertIsQueueSettingsVisible();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToQueueManagementPriorityPage_8671")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToQueueManagementPriorityPage_8671()
            {

                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var queueManagementPage = new QueueManagement(Driver, userManagerEmployee);
                var queuePriorityPage = new QueuePriority(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
              

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickQueueManagement();

                queueManagementPage.AssertIsQueueManagementVisible();
                queueManagementPage.ClickPriority();

                queuePriorityPage.AssertIsQueuePriorityVisible();

            }

            [Test]
            [TestCase(TestName = "AchReplacementAmendmentWithPaydayLoan_8675")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void AchReplacementAmendmentWithPaydayLoan_8675()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayLoan = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayLoan);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnBankAccountName();
                //customerDetailPage.ClickOnBankSection();
                customerDetailPage.ClickOnCheckingAccount();
                customerDetailPage.EnterAccountNumber();
                var confirmingAccountNumber = customerDetailPage.EnterAccountNumber();
                customerDetailPage.EnterAccountNumber2ndTime(confirmingAccountNumber);
                customerDetailPage.SaveButton();
                customerDetailPage.ValidateCheckBox();
                customerDetailPage.SendAmendment();
                customerDetailPage.ClickOk();

            }
            [Test]
            [TestCase(TestName = "UnmaskCustomerSocialSecurityNumberPendingDetailPage_9672")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void UnmaskCustomerSocialSecurityNumberPendingDetailPage_9672()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);
                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickSsn();
                pendingDetailPage.EnterManagerUserName();
                pendingDetailPage.EnterMangerPassword();
                pendingDetailPage.ClickOkayOnUnmaskSsnPopup();
                pendingDetailPage.AssertIsSsnVisible();
            }
            [Test]
            [TestCase(TestName = "UnmaskCustomerSocialSecurityNumberCustomerDashboard_9678")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void UnmaskCustomerSocialSecurityNumberCustomerDashboard_9678()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalZeroBalance);


                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                //customerDashboardPage.EnterSearchByEmail(LoanType.Payday);
                customerDashboardPage.ClickSsn();
                customerDashboardPage.EnterManagerUserName();
                customerDashboardPage.EnterMangerPassword();
                customerDashboardPage.ClickOkayOnUnmaskSsnPopup();
                customerDashboardPage.AssertIsSsnVisible();


            }
            [Test]
            [TestCase(TestName = "UnmaskCustomerSocialSecurityNumberPendingDashboard_9680")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void UnmaskCustomerSocialSecurityNumberPendingDashboard_9680()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickSsn();
                pendingDashboardPage.EnterManagerUserName();
                pendingDashboardPage.EnterMangerPassword();
                pendingDashboardPage.ClickOkayOnUnmaskSsnPopup();
                pendingDashboardPage.AssertIsSsnVisible();

            }
            [Test]
            [TestCase(TestName = "UnmaskCustomerSocialSecurityNumberCustomerDetailPage_9681")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void UnmaskCustomerSocialSecurityNumberCustomerDetailPage_9681()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalZeroBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                //customerDashboardPage.EnterSearchByEmail(LoanType.Payday);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickSsn();
                customerDetailPage.EnterManagerUserNameOnCustomerDetailPage();
                customerDetailPage.EnterManagerPasswordOnCustomerDetailPage();
                customerDetailPage.ClickOkayOnUnmaskSsnPopup();
                customerDetailPage.AssertIsSsnVisible();
            }

            [Test]
            [TestCase(TestName = "AchReplacementAmendmentNoLoan_9290")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void AchReplacementAmendmentNoLoan_9290()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                //var customerApprovedPaydayZeroBalance = GetCustomer(CustomerType.UT_ApprovedPaydayZeroBalance);
                var customerApprovedPaydayZeroBalance = "CAO-124938486";
                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.SearchByCustomerId(customerApprovedPaydayZeroBalance);
               //customerDashboardPage.EnterSearchByEmail(customerApprovedPaydayZeroBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.DuplicateDataHelper();
                customerDetailPage.ClickOnBankAccountName();
                customerDetailPage.ClickOnCheckingAccount();
                var confirmingAccountNumber = customerDetailPage.EnterAccountNumber();
                customerDetailPage.EnterAccountNumber2ndTime(confirmingAccountNumber);
                customerDetailPage.SaveButton();
            }

            [Test]
            [TestCase(TestName = "Rajesh_AchReplacementAmendment_8675_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Rajesh_AchReplacementAmendment_8675_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(LoanType.Payday);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnBankSection();
                customerDetailPage.ClickOnCheckingAccount();
                customerDetailPage.SaveButton();
                customerDetailPage.ValidateCheckBox();
                customerDetailPage.SendAmendment();
                customerDetailPage.ClickOkFailCase();
            }
            
           
            [Test]
            [TestCase(TestName = "ShowDeniedApplicationCustomerDetailPage_8696")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            [Category("CustomerDetailPage")]
            public void ShowDeniedApplicationCustomerDetailPage_8696()
            {
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, userCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, userCsrEmployee);
                var turndownCustomer = "skayjbmqg@nostromorp.com";


                loginPage.LogInUser(userCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                
                customerDashboardPage.EnterSearchByEmailApprovedCustomer(turndownCustomer);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.UnCheckOpenApplicationsOnly();
                customerDetailPage.AssertApplicationStatusDeniedIsVisible();

            }
         
          
            
            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "EmailApprovalPageLinkFromCustomerDetailPage_8698")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            //[Category("Smoke")]
            //[Category("CustomerDetailPage")]
            public void EmailApprovalPageLinkFromCustomerDetailPage_8698()
            {
                var cusUser = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, cusUser);
                var pendingDashboardPage = new PendingDashboard(Driver, cusUser);
                var customerDashboardPage = new CustomerDashboard(Driver, cusUser);
                var customerDetailPage = new CustomerDetail(Driver, cusUser);

                loginPage.LogInUser(cusUser);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.ClickthreeDotButtonOnApplicationSection();

                customerDetailPage.SelectEmailApprovalPageLink();
                customerDetailPage.ClickDoneEmailHasBeenSentPopup();

            }


            [Test]
            [TestCase(TestName = "Ravi_EmailApprovalPageLinkFromCustomerDetailPage_8698_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            public void Ravi_EmailApprovalPageLinkFromCustomerDetailPage_8698_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationTab();

                customerDetailPage.ClickthreeDotButtonOnApplicationSection();
                customerDetailPage.SelectEmailApprovalPageLink();
                customerDetailPage.AssertClickDoneEmailHasBeenSentPopupFail();

            }
            
            [Test]
            [TestCase(TestName = "RefreshApplicationFromApplicationDetailPage_8716")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void RefreshApplicationFromApplicationDetailPage_8716()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickRefreshApplication();
            }
            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "Rajesh_RefreshApplicationFromApplicationDetailPage_8716_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            public void Rajesh_RefreshApplicationFromApplicationDetailPage_8716_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickRefreshApplicationFailCase();
            }

            [Test]
            [TestCase(TestName = "Ravi_RefreshApplicationFromApplicationDetailPage_8716_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("ApplicationDetailPage")]
            public void Ravi_RefreshApplicationFromApplicationDetailPage_8716_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var applicationDashboardPage = new ApplicationDashboard(Driver, user);
                var applicationDetailPage = new ApplicationDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.AssertClickRefreshApplicationFail();

            }

            [Test]
            [TestCase(TestName = "EmailSignedApplicationFromCustomerDetailPagePaydayLoan_8738")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("ApplicationDetailPage")]
            public void EmailSignedApplicationFromCustomerDetailPagePaydayLoan_8738()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydaywithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydaywithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.ClickGoButton();
                customerDetailPage.ClickEmailSignedDisclosure();
            }
            [Test]
            [TestCase(TestName = "EmailSignedApplicationFromCustomerDetailPagePersonalLoan_9359")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            [Category("CustomerDetailPage")]

            public void EmailSignedApplicationFromCustomerDetailPagePersonalLoan_9359()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalLoanwithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalLoanwithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.ClickGoButton();
                customerDetailPage.ClickEmailSignedDisclosure();
            }
            
          
            

            [Test]
            [TestCase(TestName = "Ravi_PrintSignedApplicationFromCustomerDetailPage_8738_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            [Category("CustomerDetailPage")]
            public void Ravi_PrintSignedApplicationFromCustomerDetailPage_8738_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.AdvanceCustomerService); // in this testcase - Signed Disclosure button not found. 
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);

                loginPage.LogInUser(user);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectSignedDisclosure();
                customerDetailPage.ClickGoToOptionOnSignedDisclosure();
                customerDetailPage.AssertClickPrint();
            }
            [Test]
            [TestCase(TestName = "PaydayLoanPayDownDebitCardCustomerDetailPage_8742")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2023-04-02 12:00:00Z")]
            //[Category("BUG")] // https://cashwise.visualstudio.com/Softwise/_workitems/edit/9437
            // Last edit: Pat Holman 12/09/2019
            public void PaydayLoanPayDownDebitCardCustomerDetailPage_8742()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPayDown();
                customerDetailPage.PaymentMethodDebitCard(customerPaydayWithBalance);
                customerDetailPage.CardType(customerPaydayWithBalance);
                customerDetailPage.Last4Digits(customerPaydayWithBalance);
                customerDetailPage.EnterPayDownAmount(customerPaydayWithBalance);
                customerDetailPage.ClickPayDown();
                customerDetailPage.ClickOkay();
            }

            [Test]
            [TestCase(TestName = "Ravi_MakeAPaymentPayDownDebitCardCustomerDetailPage_8742_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            [Category("CustomerDetailPage")]
            public void Ravi_MakeAPaymentPayDownDebitCardCustomerDetailPage_8742_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayZeroBalance);

                loginPage.LogInUser(user);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.AssertThreeDotButtonOnLoansSectionFail();


            }
            
            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PersonalLoanPayoffDebitCardCustomerDetailPage_8744")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("MakePayment")]
            public void PersonalLoanPayoffDebitCardCustomerDetailPage_8744()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);
            

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalWithBalance);
                customerDashboardPage.ClickGoToButton();


                customerDetailPage.AssertCustomerDetailVisible(); // there should be  dummy data on loan section
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectLoanPayoff();
                customerDetailPage.PaymentMethodDebitCard(customerPersonalWithBalance);
                customerDetailPage.CardType(customerPersonalWithBalance);
                customerDetailPage.Last4Digits(customerPersonalWithBalance);
                customerDetailPage.ClickPayOff();
                customerDetailPage.ClickOkay();

            }
            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PaydayPayoffDebitCardCustomerDetailPage_9291")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void PaydayPayoffDebitCardCustomerDetailPage_9291()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickThreeDotButtonOnLoansSection();
                customerDetailPage.SelectPickup();
                customerDetailPage.PaymentMethodDebitCard(customerPaydayWithBalance);
                customerDetailPage.CardType(customerPaydayWithBalance);
                customerDetailPage.Last4Digits(customerPaydayWithBalance);
                customerDetailPage.ClickPickUp();
                customerDetailPage.ClickOkay();
            }

            [Test]
            [TestCase(TestName = "Ravi_MakeAPaymentPayoffDebitCardCustomerDetailPage_8744_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            public void Ravi_MakeAPaymentPayoffDebitCardCustomerDetailPage_8744_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayZeroBalance);

                loginPage.LogInUser(user);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.AssertThreeDotButtonOnLoansSectionFail();
            }

            [Test]
            [TestCase(TestName = "ValidateAndInvalidateWorkItem_8321")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void ValidateAndInvalidateWorkItem_8321()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                pendingDetailPage.ClickWorkItemBank();
                pendingDetailPage.Refresh();
                pendingDetailPage.ClickWorkItemId();
                pendingDetailPage.InvalidateWorkItemId();
                pendingDetailPage.SelectNoteTypeOnInvalidateAction();
                //pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterInvalidateNote();
                pendingDetailPage.ClickSaveNote();
            }

            [Test]
            [TestCase(TestName = "Ravi_InvalidateLoanAction_8321_Fail")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void Ravi_InvalidateLoanAction_8321_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();
                pendingDashboardPage.CloseDeferredTurndownIfExist();
                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickValidateId();
                pendingDetailPage.SelectNoteType();
                pendingDetailPage.EnterNote();
                pendingDetailPage.ClickSaveNote();
                pendingDetailPage.ClickWorkItemId();
                pendingDetailPage.InvalidateWorkItemId();
                pendingDetailPage.AssertClickSaveNoteFail();
            }

            [Test]
            [TestCase(TestName = "NavigateToTokensPage_8800")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToTokensPage_8800()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var documentManagementPage = new DocumentManagement(Driver, userManagerEmployee);
                var tokensPage = new TokensPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
               

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickTokens();

                tokensPage.AssertIsTokensPageVisible();

            }

            
            [Test]
            [TestCase(TestName = "NavigateToTemplatesPage_8803")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToTemplatesPage_8803()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var documentManagementPage = new DocumentManagement(Driver, userManagerEmployee);
                var templatesPage = new TemplatesPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickTemplates();

                templatesPage.AssertIsTemplatesPageVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToDocumentManagementQueries_10961")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDocumentManagementQueries_10961()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var documentManagementPage = new DocumentManagement(Driver, userManagerEmployee);
                var queriesPage = new Queries(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickQueries();
                
                queriesPage.AssertIsQueriesPageVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToAdverseActionSettingsPage_10962")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToAdverseActionSettingsPage_10962()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var adverseActionSettingsPage = new AdverseActionSettings(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickAdverseActionSettings();
               
                adverseActionSettingsPage.AssertIsAdverseActionSettingsPageVisible();
                
            }

            [Test]
            [TestCase(TestName = "NavigateToEmployeeSchedulesInLMS_11064")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToEmployeeSchedulesInLMS_11064()
            {
                var userOwnerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userOwnerEmployee);
                var adminPage = new AdminPage(Driver, userOwnerEmployee);
                var employeeSchedulesPage = new EmployeeSchedulesPage(Driver, userOwnerEmployee);

                loginPage.LogInUser(userOwnerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickEmployeeSchedules();

                employeeSchedulesPage.AssertIsEmployeeSchedulesVisible();

                
            }
            [Test]
            [TestCase(TestName = "NavigateToActionSettingsPage_10756")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToActionSettingsPage_10756()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var actionSettingsPage = new ActionSettingsPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickActionSettings();

                actionSettingsPage.AssertActionSettingsPageLoaded();




            }
            [Test]
            [TestCase(TestName = "NavigateToSitesPage_10963")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToSitesPage_10963()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var sitesPage = new SitesPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSitesButton();

                sitesPage.AssertIsSitesPageVisible();
                
            }
            [Test]
            [TestCase(TestName = "NavigateToStateTimeZoneMappingsFromTypeManagementPage_10964")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToStateTimeZoneMappingsFromTypeManagementPage_10964()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var typeManagementPage = new TypeManagement(Driver, userManagerEmployee);
                var stateTimeZoneMappingsPage = new StateTimeZoneMappingsPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickStateTimeZoneMappings();

                stateTimeZoneMappingsPage.AssertIsStateTimeZoneMappingsPageLoaded();
                

            }
            [Test]
            [TestCase(TestName = "NavigateToSecuritySettings_8799")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToSecuritySettings_8799()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var securitySettingsPage = new SecuritySettings(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSecuritySettings();

                securitySettingsPage.AssertIsSecuritySettingsPageVisible();

            }

            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ResetPasswordFromApplicationDetailPage_8816")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ResetPasswordFromApplicationDetailPage_8816()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickResetPassword();
            }
            [Test]
            [TestCase(TestName = "Rajesh_ResetPasswordFromApplicationDetailPage_8816_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void Rajesh_ResetPasswordFromApplicationDetailPage_8816_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var applicationDashboardPage = new ApplicationDashboard(Driver, csrEmployee);
                var applicationDetailPage = new ApplicationDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectApplicationDashboard();

                applicationDashboardPage.AssertIsApplicationDashboardLoaded();
                applicationDashboardPage.ClickGoToButton();

                applicationDetailPage.AssertIsApplicationDetailVisible();
                applicationDetailPage.ClickResetPasswordFailCase();
            }
            
            [Test]
            [TestCase(TestName = "NavigateToWorkFlowManagementPage_8915")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToWorkFlowManagementPage_8915()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var workflowManagementPage = new WorkflowManagementPage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
              

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickWorkflowManagement();

                workflowManagementPage.AssertIsWorkflowManagementPageVisible();

            }
            
            [Test]
            [TestCase(TestName = "BackToDashboardFromPendingDetailPage_8918")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("PendingDetailPage")]
            public void BackToDashboardFromPendingDetailPage_8918()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                                       

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickBacktoDashboard();

                pendingDashboardPage.AssertIsPendingDashboardLoaded();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToSystemMaintenancePage_8943")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToSystemMaintenancePage_8943()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var systemMaintenancePage = new SystemMaintenancePage(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSystemMaintenanceTile();

                systemMaintenancePage.AssertIsSystemMaintenancePageVisible();

            }



            [Test]
            [TestCase(TestName = "NavigateToLoanTypeManagementPage_8946")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTypeManagementPage_8946()
            {
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var loanTypeManagementPage = new LoanTypeManagement(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickLoanTypeManagementTile();

                loanTypeManagementPage.AssertIsLoanTypeManagementVisible();
            }


            [Test]
            [TestCase(TestName = "CheckActiveMilitaryCustomerDetailPage_8954")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            // Last edit by: Pat Holman
            // changed category name
            public void CheckActiveMilitaryCustomerDetailPage_8954()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);


                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContact();
                customerDetailPage.ClickActiveMilitary();
                customerDetailPage.ClickSaveContactSection();

            }

            [Test]
            [TestCase(TestName = "Rajesh_CheckActiveMilitaryCustomerDetailPage_8954_Fail")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void Rajesh_CheckActiveMilitaryCustomerDetailPage_8954_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                //var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPayDay = CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalZeroBalance);

                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPayDay);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContact();
                customerDetailPage.ClickActiveMilitary();
                customerDetailPage.AssertClickSaveFail();

            }
            
            [Test]
            [TestCase(TestName = "UnassignAnAssignedLoan_9039")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            // PatEdit
            public void UnassignAnAssignedLoan_9039()
            {
                var advancedCsrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);
                var loginPage = new LoginPage(Driver, advancedCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, advancedCsrEmployee);
                var pendingDetailPage = new PendingDetail(Driver, advancedCsrEmployee);

                loginPage.LogInUser(advancedCsrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickWorkNext();

                pendingDetailPage.AssertIsPendingDetailVisible();
                pendingDetailPage.ClickRightHamburgerMenu();
                pendingDetailPage.SelectReassignApplication();
                pendingDetailPage.ClickClearAll();
                pendingDetailPage.ClickSave();

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
            }
            
            [Test]
            //[Category("Ignore")]
          //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "DuplicateEmailWarning_10146")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void DuplicateEmailWarning_10146()
            {
                
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, managerEmployee);
                var customerDetailPage = new CustomerDetail(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickApplicationIda();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickEnvelopeByEmailAddress();
                customerDetailPage.ClickHamburgerMenu();
                customerDetailPage.SelectApprovedDashboard();
              //approvedDashboardPage.CopyContactEmail();
                
                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickSecondApplicationIdb();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContact();
                customerDetailPage.ClickEmailAddressInputFieldEraseOldEmail();
                customerDetailPage.AssertDuplicateDataWarningVisible();
                customerDetailPage.ClickCloseOnDuplicateWarningPopup();
                customerDetailPage.PageRefresh();
                
            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "DuplicateEmailVerification_10544")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void DuplicateEmailVerification_10544()
            {
                
                var employee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, employee);
                var adminPage = new AdminPage(Driver, employee);
                var approvedDashboardPage = new ApprovedDashboard(Driver, employee);
                var customerDashboardPage = new CustomerDashboard(Driver, employee);
                var customerDetailPage = new CustomerDetail(Driver, employee);

                loginPage.LogInUser(employee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.SelectApprovedDashboard();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickFirstRowAppID();
               // approvedDashboardPage.ClickApplicationIda();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickEnvelopeByEmailAddress();
                customerDetailPage.ClickHamburgerMenu();
                customerDetailPage.SelectApprovedDashboard();
                //approvedDashboardPage.CopyContactEmail();

                approvedDashboardPage.AssertIsApprovedDashboardVisible();
                approvedDashboardPage.ClickSecondApplicationIdb();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContact();
                customerDetailPage.ClickEmailAddressInputFieldEraseOldEmail();
                customerDetailPage.AssertDuplicateDataWarningVisible();
                customerDetailPage.ClickOkayOnDuplicateWarningPopup();
                customerDetailPage.ClickSaveContactSection();
                customerDetailPage.PageRefresh();
                var email = customerDetailPage.GrabEmailAddress();
                customerDetailPage.ClickHamburgerMenu();
                customerDetailPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.ClickSearchByInputField();
                customerDashboardPage.EnterDuplicateEmailAddress(email);
                customerDashboardPage.AssertDuplicateEmailsAreVisible(email);


            }
            [Test]
            [TestCase(TestName = "NavigateToDefaultedLoansCustomerDetailPage_8879")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToDefaultedLoansCustomerDetailPage_8879()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickDefaultedSection();

            }

            [Test]
            [TestCase(TestName = "AddPinnedNoteCustomerDetailPage_11029")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void AddPinnedNoteCustomerDetailPage_11029()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                //customerDetailPage.ClickOkayOnDuplicateWarningPopup();
                customerDetailPage.ClickNoteTab();
                customerDetailPage.ClickAddButton();
                customerDetailPage.SelectNoteType();
                customerDetailPage.EnterCustomerDetailNote();
                customerDetailPage.ClickSaveContactSection();
                customerDetailPage.ClickContextMenuNotesSection();
                customerDetailPage.SelectPinnedNote();
                customerDetailPage.AssertPinnedNoteIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsUtahPayday_11014")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsUtahPayday_11014()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsKansasPayday_11020")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsKansasPayday_11020()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerKansasPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.KS_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerKansasPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToLoanTermsAlaskaPayday_11021")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsAlaskaPayday_11021()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerAlaskaPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.AK_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerAlaskaPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsAlabamaPayday_11022")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsAlabamaPayday_11022()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerAlabamaPaydayWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.AK_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerAlabamaPaydayWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToLoanTermsUtahPersonalLoan_11015")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsUtahPersonalLoan_11015()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerPersonalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerPersonalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsIdahoInstallmentLoan_11016")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsIdahoInstallmentLoan_11016()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerIdahoInstallmentWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerIdahoInstallmentWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsTexasInstallmentLoan_11017")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsTexasInstallmentLoan_11017()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerTexasInstallmentWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
                

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerTexasInstallmentWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsWisconsinInstallmentLoan_11018")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsWisconsinInstallmentLoan_11018()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerWisconsinInstallmentWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.WI_ApprovedPersonalWithBalance);


                loginPage.LogInUser(csrEmployee);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerWisconsinInstallmentWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToLoanTermsMissouriInstallmentLoan_11019")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLoanTermsMissouriInstallmentLoan_11019()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var customerMissouriInstallmentWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.KS_ApprovedPaydayWithBalance);


                loginPage.LogInUser(csrEmployee);
              

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerMissouriInstallmentWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickContextMenu();
                customerDetailPage.ClickLoanTerms();
                customerDetailPage.AssertLoanTermsIsVisible();

            }

            [Test]
            [TestCase(TestName = "ShowHistoricalPendingApplications_9061")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Smoke")]
            [Category("ApplicationDetailPage")]
            public void ShowHistoricalPendingApplications_9061()
            {
                var userCsrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, userCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, userCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, userCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, userCsrEmployee);
                var customerApprovedLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);


               loginPage.LogInUser(userCsrEmployee);
               
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customerApprovedLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.UnCheckOpenApplicationsOnly();
                customerDetailPage.ClickShowHistoricalPendingApplications();

            }
            [Test]
            [TestCase(TestName = "Rajesh_ShowHistoricalPendingApplications_9061_Fail")]
            [Author(name: "Rajesh Kr Das", email: "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void Rajesh_ShowHistoricalPendingApplications_9061_Fail()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickApplicationSection();
                customerDetailPage.UnCheckOpenApplicationsOnly();
                customerDetailPage.ClickShowHistoricalPendingApplicationsFailCase();

            }


            [Test]
            [TestCase(TestName = "NavigateToApplicationMaintenancePage_9063")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToApplicationMaintenancePage_9063()
            {
                var operationsEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, operationsEmployee);
                var adminPage = new AdminPage(Driver, operationsEmployee);
                var systemMaintenancePage = new SystemMaintenancePage(Driver, operationsEmployee);
                var applicationMaintenancePage = new ApplicationMaintenance(Driver, operationsEmployee);

                loginPage.LogInUser(operationsEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSystemMaintenanceTile();

                systemMaintenancePage.AssertIsSystemMaintenancePageVisible();
                systemMaintenancePage.ClickApplicationMaintenanceTile();

                applicationMaintenancePage.AssertIsApplicationMaintenancePageVisible();

            }
            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToApplicationMaintenancePage_9063")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToApplicationMaintenancePage_9063()
            {
                var operationsEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, operationsEmployee);
                var adminPage = new AdminPage(Driver, operationsEmployee);
                var systemMaintenancePage = new SystemMaintenancePage(Driver, operationsEmployee);
                var applicationMaintenancePage = new ApplicationMaintenance(Driver, operationsEmployee);

                loginPage.LogInUser(operationsEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSystemMaintenanceTile();

                systemMaintenancePage.AssertIsSystemMaintenancePageVisible();
                systemMaintenancePage.ClickApplicationMaintenanceTile();

                applicationMaintenancePage.AssertIsApplicationMaintenancePageVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToLocationMaintenancePage_9065")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToLocationMaintenancePage_9065()
            {
                var userManagerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, userManagerEmployee);
                var adminPage = new AdminPage(Driver, userManagerEmployee);
                var systemMaintenancePage = new SystemMaintenancePage(Driver, userManagerEmployee);
                var locationMaintenancePage = new LocationMaintenance(Driver, userManagerEmployee);

                loginPage.LogInUser(userManagerEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSystemMaintenanceTile();

                systemMaintenancePage.AssertIsSystemMaintenancePageVisible();
                systemMaintenancePage.ClickLocationMaintenanceTile();

                locationMaintenancePage.AssertIsLocationMaintenancePageVisible();

            }

            [Test]
            [TestCase(TestName = "ViewOnlineCustomerDashBoardFromCustomerDetailPage_9292")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ViewOnlineCustomerDashBoardFromCustomerDetailPage_9292()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboard = new PendingDashboard(Driver, csrEmployee);
                var customerDashboard = new CustomerDashboard(Driver, csrEmployee);
                var customerDetail = new CustomerDetail(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
                
                pendingDashboard.AssertIsPendingDashboardLoaded();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectCustomerDashboard();

                customerDashboard.AssertCustomerDashboardLoaded();
                customerDashboard.EnterSearchByEmail();
                customerDashboard.ClickGoToButton();

                customerDetail.AssertCustomerDetailVisible();
                customerDetail.ClickRightHamburgerMenu();
                customerDetail.ClickViewOnlineCustomerDashboard();
            }

            [Test]
            [TestCase(TestName = "GetPendingApplicationCustomer_000")]
            [Author(name: "Pat Holman", email: "pholman@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Maintenance")]
            public void GetPendingApplicationCustomer_000()
            {
                var advanceCsrEmployee = Users.GetEmployee(EmployeeType.AdvanceCustomerService);

                var loginPage = new LoginPage(Driver, advanceCsrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, advanceCsrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, advanceCsrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, advanceCsrEmployee);
                var customer = CheckCityOnline.Users.GetCustomer(CustomerType.UT_PendingAll);
                
                loginPage.LogInUser(advanceCsrEmployee);
                
                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(customer);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();

            }

            [Test]
            [TestCase(TestName = "RetireCustomer_000")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Maintenance")]
            public void RetireCustomer_000()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.Manager);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboard = new PendingDashboard(Driver, csrEmployee);
                var customerDashboard = new CustomerDashboard(Driver, csrEmployee);
                var customerDetail = new CustomerDetail(Driver, csrEmployee);
                var adminDashboard = new ApplicationDashboard(Driver, csrEmployee);

                var retirePattern = @"pivey.pro";
                loginPage.LogInUser(csrEmployee);
                loginPage.SelectLocation();
                loginPage.ClickAndSelectClose();
                pendingDashboard.ClickHamburgerMenu();
                pendingDashboard.SelectCustomerDashboard();
                customerDashboard.AssertCustomerDashboardLoaded();
                while (true)
                {
                    var searchInputElement = "/html/body/cleo-root/cleo-lms-layout/div/div[2]/div/cleo-lms-customer-view/div/sw-table/div/div[1]/sw-text/sw-input-wrapper/div/div/div/mat-form-field/div/div[1]/div/input";
                    Driver.FindElement(By.XPath(searchInputElement)).SendKeys(retirePattern);
                    customerDetail.ExplicitWait(5);
                    try
                    {
                        customerDashboard.ClickGoToButton();
                    }
                    catch (Exception)
                    {
                        break;
                    }
                    var lastNameInputElement = "/html/body/cleo-root/cleo-lms-layout/div/div[2]/div/cleo-lms-customer-edit/div/mat-accordion[2]/mat-expansion-panel/div/div/div/cleo-lms-customer-personal/form/div[1]/sw-text[3]/sw-input-wrapper/div/div/div/mat-form-field/div/div[1]/div/input";
                    var emailAddressElement = "/html/body/cleo-root/cleo-lms-layout/div/div[2]/div/cleo-lms-customer-edit/div/mat-accordion[2]/mat-expansion-panel/div/div/div/cleo-lms-customer-personal/form/div[3]/sw-text/sw-input-wrapper/div/div/div/mat-form-field/div/div[1]/div/input";
                    var saveButtonElement = "/html/body/cleo-root/cleo-lms-layout/div/div[2]/div/cleo-lms-customer-edit/div/mat-accordion[2]/mat-expansion-panel/div/div/div/cleo-lms-customer-personal/div/dis-button/div/button/span/span/span";
                    var hamburgerElement = "/html/body/cleo-root/cleo-lms-layout/div/div[2]/header/div[1]/dis-button/div/button/span/span/mat-icon";
                    var customerDashElement = "/html/body/cleo-root/cleo-lms-layout/div/div[1]/div/div/mat-card/div[3]/dis-button/div/button/span/span/span";
                    customerDetail.AssertCustomerDetailVisible();
                    Driver.FindElement(By.XPath(lastNameInputElement)).Clear();
                    Driver.FindElement(By.XPath(lastNameInputElement)).SendKeys("Retire");
                    Driver.FindElement(By.XPath(lastNameInputElement)).SendKeys(Keys.Tab);
                    var oldEmailAddress = Driver.FindElement(By.XPath(emailAddressElement)).GetAttribute("value");
                    var newEmailAddress = oldEmailAddress.Replace("@", "").Replace(".", "").Replace("-", "") + "@example.com";
                    Driver.FindElement(By.XPath(emailAddressElement)).Clear();
                    Driver.FindElement(By.XPath(emailAddressElement)).SendKeys(newEmailAddress);
                    Driver.FindElement(By.XPath(emailAddressElement)).SendKeys(Keys.Tab);
                    Driver.FindElement(By.XPath(saveButtonElement)).Click();
                    customerDetail.ExplicitWait(10);
                    Driver.FindElement(By.XPath(hamburgerElement)).Click();
                    customerDetail.ExplicitWait(3);
                    Driver.FindElement(By.XPath(customerDashElement)).Click();
                    customerDetail.ExplicitWait(5);
                }
            }
            
            [Test]
            [Ignore("Test Broke", Until = "2020-01-30 12:00:00Z")]
            [TestCase(TestName = "NavigateToGoogleTTSPage_9294")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToGoogleTTSPage_9294()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var adminPage = new AdminPage(Driver, csrEmployee);
                var documentManagementPage = new DocumentManagement(Driver, csrEmployee);
                var googleTtsTemplatePage = new GoogleTtsTemplate(Driver, csrEmployee);


                loginPage.LogInUser(csrEmployee);
                

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickDocumentManagement();

                documentManagementPage.AssertIsDocumentManagementVisible();
                documentManagementPage.ClickGoogleTtsTemplate();

                googleTtsTemplatePage.AssertIsGoogleTtsTemplateVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToAttachmentTypesPage_9295")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToAttachmentTypesPage_9295()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var adminPage = new AdminPage(Driver, csrEmployee);
                var typeManagementPage = new TypeManagement(Driver, csrEmployee);
                var attachmentTypesPage = new AttachmentTypes(Driver, csrEmployee);

                loginPage.LogInUser(csrEmployee);
               
                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickTypeManagement();

                typeManagementPage.AssertTypeManagementVisible();
                typeManagementPage.ClickAttachmentTypes();

                attachmentTypesPage.AssertIsAttachmentTypesVisible();

            }


            [Test]
            [TestCase(TestName = "ShowProposalLogItemsPaydayLoanCustomerDetailPage_9580")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowProposalLogItemsPaydayLoanCustomerDetailPage_9580()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var paydayLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(paydayLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowProposalLogItems();
                customerDetailPage.AssertTransactionTextIsVisible();
                
            }
            [Test]
            [TestCase(TestName = "ShowOverridePaydayLoanCustomerDetailPage_9759")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowOverridePaydayLoanCustomerDetailPage_9759()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var paydayLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(paydayLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowOverride();
                customerDetailPage.AssertOverrideIsVisible();



            }
            [Test]
            [TestCase(TestName = "ShowSaleIDPaydayCustomerDetailPage_9760")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowSaleIDPaydayCustomerDetailPage_9760()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var paydayLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(paydayLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowSaleId();
                customerDetailPage.AssertSaleIdIsVisible();

            }
            [Test]
            [TestCase(TestName = "ShowSaleIDPersonalLoanCustomerDetailPage_10020")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowSaleIDPersonalLoanCustomerDetailPage_10020()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var personalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(personalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowSaleId();
                customerDetailPage.AssertSaleIdIsVisible();

            }
            [Test]
            [TestCase(TestName = "ShowProposalLogPersonalLoanCustomerDetailPage_10021")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowProposalLogPersonalLoanCustomerDetailPage_10021()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var personalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(personalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowProposalLogItems();

            }
            [Test]
            [TestCase(TestName = "ShowOverrideLogPersonalLoanCustomerDetailPage_10022")]
            [Author(name: "Ryan Palmer", email: "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void ShowOverrideLogPersonalLoanCustomerDetailPage_10022()
            {
                var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, csrEmployee);
                var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                var personalLoanWithBalance =
                    CheckCityOnline.Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);

                loginPage.LogInUser(csrEmployee);

                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ClickHamburgerMenu();
                pendingDashboardPage.SelectCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail(personalLoanWithBalance);
                customerDashboardPage.ClickGoToButton();

                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickOnFundedForLoanTransactions();
                customerDetailPage.ClickShowOverride();
                customerDetailPage.AssertOverrideIsVisible();


            }
          

            [Test]
            [TestCase(TestName = "AllWarningTypes")]
            [Author(name: "Rajesh Kr Das", email: "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void AllWarningTypes()
            {
                // ************ Pat ******************
                List<string> warningTypes = new List<string>();
                var managerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, managerEmployee);
                var adminPage = new AdminPage(Driver, managerEmployee);
                var customerDetailPage = new CustomerDetail(Driver, managerEmployee);
                var customerDashboardPage = new CustomerDashboard(Driver, managerEmployee);

                loginPage.LogInUser(managerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickHamburgerMenu();
                adminPage.ClickCustomerDashboard();

                customerDashboardPage.AssertCustomerDashboardLoaded();
                customerDashboardPage.EnterSearchByEmail();
                customerDashboardPage.ClickGoToButton();
                customerDetailPage.AssertCustomerDetailVisible();
                customerDetailPage.ClickWarningSection();
                customerDetailPage.ClickAddNewWarningButton();
                customerDashboardPage.GetAllWarningTypes(warningTypes);

                foreach (string warning in warningTypes)
                {
                    customerDetailPage.AddWarningType(warning);
                    customerDetailPage.ClickGoButton();
                    customerDetailPage.DeleteWarningTypes();
                    customerDetailPage.ExplicitWait(3);
                    customerDetailPage.ClickAddNewWarningButton();
                }
            }

            [Test]
            [TestCase(TestName = "NavigateToSiteOnAdminPage_9570")]
            [Category("SmokeTest")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NavigateToSiteOnAdminPage_9570()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var sitesPage = new SitesPage(Driver, ownerEmployee);

                loginPage.LogInUser(ownerEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickSitesButton();

                sitesPage.AssertIsSitesPageVisible();

            }

            [Test]
            [TestCase(TestName = "FindLoadIDs")]
            [Author("Pat Holman", "pholman@softwise.com")]
            [Ignore("Test Broke", Until = "2020-03-09 12:00:00Z")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void FindLoadIDs()
            {

                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var loginPage = new LoginPage(Driver, user);
                var pendingDashboardPage = new PendingDashboard(Driver, user);
                var customerDashboardPage = new CustomerDashboard(Driver, user);
                var customerDetailPage = new CustomerDetail(Driver, user);
                var customerPaydayWithBalance = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                loginPage.LogInUser(user);


                pendingDashboardPage.AssertIsPendingDashboardLoaded();
                pendingDashboardPage.ListLoanIDs();

            }


            [Test]
            [Category("Ignore")]
            [Category("BUG 9910")] // THERE IS A DEFECT THAT IS PREVENTING US FROM USING THIS TEST SCRIPT \\
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "AddID_CustomerDetail")]
            [Author("Ravi", "ravi@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            //[Category("RegressionTest")]
            //[Category("AddingCustomerID")]
            public void AddID_CustomerDetail()
            {
                //Array of list
                string[] ds = new string[]
                {
                    "Commercial Drivers License",
                    "drivers license",
                    "drivers license-2",
                    "Driving Privilege Card",
                    "Employer  ID",
                    "Foreign Drivers License",
                    "Foreign ID",
                    "Government ID",
                    "StateColumn Identification",
                    "other id",
                    "Institituto Federal Electorial",
                    "MATRICULA CONSULAR",
                    "Mexico ID",
                    "Military ID",
                    "Has NO ID",
                    "OTHER ID",
                    "Passport",
                    "Permanent Resident Card",
                    "Resident Alien",
                    "school Id",
                    "Social Security Card",
                    "StateColumn Issued ID",
                    "temporary operator license",
                    "Tribal Indian ID",
                    "Utah Driver's License",
                    "Voter Registration Card",
                    "visa"
                };


                for (int i = 0; i < ds.Length; i++)
                {
                    string idLabel = ds[i];


                    var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                    var loginPage = new LoginPage(Driver, csrEmployee);
                    var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
                    var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
                    var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
                    var drivingId = "CUS123456";
                    var customerPaydayLoan = GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                    var managerEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                    var adminPage = new AdminPage(Driver, managerEmployee);

                    //Login
                    loginPage.LogInUser(csrEmployee);


                    //Goto Customer Dashboard
                    pendingDashboardPage.AssertIsPendingDashboardLoaded();
                    pendingDashboardPage.ClickHamburgerMenu();
                    pendingDashboardPage.SelectCustomerDashboard();
                    customerDashboardPage.AssertCustomerDashboardLoaded();
                    customerDashboardPage.CustomerDashboardEmail(customerPaydayLoan.Email);
                    customerDashboardPage.ClickGoToButton();
                    customerDetailPage.AssertCustomerDetailVisible();

                    //Click AddID
                    customerDashboardPage.ClickAddId();
                    customerDashboardPage.CmbAddId_CustomerDetail(drivingId, idLabel);

                    //Logout
                    adminPage.ClickPersonPopUp();
                    adminPage.ClickLogout();
                    adminPage.ClickYes();

                    //Again Login
                    loginPage.LogInUser(csrEmployee);
                    loginPage.SelectLocation();
                    loginPage.ClickAndSelectClose();

                    //Again goto Customer Dashboard
                    pendingDashboardPage.AssertIsPendingDashboardLoaded();
                    pendingDashboardPage.ClickHamburgerMenu();
                    pendingDashboardPage.SelectCustomerDashboard();
                    customerDashboardPage.AssertCustomerDashboardLoaded();
                    customerDashboardPage.CustomerDashboardEmail(customerPaydayLoan.Email);
                    customerDashboardPage.ClickGoToButton();
                    customerDetailPage.AssertCustomerDetailVisible();

                    customerDashboardPage.ClickAddId();
                    // customerDashboardPage.mbAddId_CustomerDetail(string id_number, string id_label);


                    //Again Logout
                    adminPage.ClickPersonPopUp();
                    adminPage.ClickLogout();
                    adminPage.ClickYes();
                }
            }
        }
    }
}