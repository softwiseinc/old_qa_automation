﻿using AutomationResources;
using Lendwise.Pages;
using NUnit.Framework;

namespace Lendwise.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [TestCase(TestName = "LoginInvalidUser_8757")]
            [Author("Pat Holman", "pholman@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            public void LoginInvalidUser_8757()
            {
                var user = Users.InvalidUsernamePassword();
                var loginPage = new LoginPage(Driver, user);
                loginPage.LogInUserFailure(user);
            }
            [Test]
            [TestCase(TestName = "AdminLogin_7881")]
            [Author("Ryan Palmer,", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            // Last Edit by Pat Holman
            // minor edits for variable name change
            public void AdminLogin_7881()
            {
                var employee = Users.GetEmployee(EmployeeType.Supervisor);
                var loginPage = new LoginPage(Driver, employee);
                loginPage.LogInUser(employee);
            }
            [Test]
            [TestCase(TestName = "MonitorUserLogin_9948")]
            [Author("Ryan Palmer,", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            public void MonitorUserLogin_9948()
            {
                
                var monEmployee = Users.GetEmployeeMonitor01();
                var loginPage = new LoginPage(Driver, monEmployee);
                var pendingDashboard = new PendingDashboard(Driver, monEmployee);

                loginPage.LoginMonitor(monEmployee);
                pendingDashboard.AssertIsPendingDashboardLoaded();
                
            }

            [Test]
            [TestCase(TestName = "Rajesh_AdminLogin_7881_Fail_QA")]
            [Author("Rajesh Kr Das,", "rdas@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("Regression")]
            [Category("LoginLogout")]
            public void Rajesh_AdminLogin_7881_Fail_QA()
            {
                var user = Users.GetQaInvalid_Login_user();
                var loginPage = new LoginPage(Driver, user);
                loginPage.LogInUserForcedError(user);
            }

            [Test]
            [TestCase(TestName = "Ravi_AdminLogin_Fail")]
            [Author("Ravi,", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("LoginLogout")]
            public void Ravi_AdminLogin_Fail()
            {
                var user = Users.GetQaUser_invalid_login();
                var loginPage = new LoginPage(Driver, user);
                loginPage.LogInUserForceError(user);
            }

           

        }
    }
}
