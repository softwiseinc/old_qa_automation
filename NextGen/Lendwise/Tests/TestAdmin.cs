﻿using System;
using AutomationResources;
using AventStack.ExtentReports;
using Lendwise.Pages;
using NUnit.Framework;

namespace Lendwise.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {

            [Test]
            [TestCase(TestName = "NavigateToAdminPage_8212")]
            [Author("Ryan Palmer,", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToAdminPage_8212()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                
                //loginPage 
                loginPage.LogInUser(ownerEmployee); 
                
                //adminPage
                adminPage.AssertIsAdminPageVisible();
                
            }

            [Test]
            [TestCase(TestName = "FinalCountDown_NavigateToAdminPage_8212")]
            [Author("Ryan Palmer,", "rpalmer@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void FinalCountDown_NavigateToAdminPage_8212()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, ownerEmployee);
                var adminPage = new AdminPage(Driver, ownerEmployee);

                //loginPage 
                loginPage.LogInUser(ownerEmployee);

                //adminPage
                adminPage.AssertIsAdminPageVisible();

            }


            [Test]
            [TestCase(TestName = "Ravi_NavigateToAdminPage_8212")]
            [Author("Ravi Kazi Karmacharya,", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            // Last edit by Pat Holman (1/13/2020) - change EmployeeType
            public void Ravi_NavigateToAdminPage_8212()
            {
                var user = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, user);
                var adminPage = new AdminPage(Driver, user);

                //loginPage 
                loginPage.LogInUser(user);
                

                //adminPage
                adminPage.AssertIsAdminPageInvisible();

            }


            [Test]
            [TestCase(TestName = "NavigateToAboutPage_7887")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            public void NavigateToAboutPage_7887()
            {
                var ownerEmployee = Users.GetEmployee(EmployeeType.Owner);
                var adminPage = new AdminPage(Driver, ownerEmployee);
                var aboutPage = new AboutPage(Driver, ownerEmployee);

               // adminPage.LoginUser(user);
               adminPage.NavigateToAboutPage(ownerEmployee);
               aboutPage.AssertIsAboutPageVisible();
            }


            [Test]
            [TestCase(TestName = "Ravi_NavigateToAboutPage_Fail")]
            [Author("Ravi Karmacharya", "ravik@softwise.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("SmokeTest")]
            [Category("Navigate")]
            // Last edit by Pat Holman (1/13/2020) - Change failure logic
            public void Ravi_NavigateToAboutPage_Fail()
            {
                var user = Users.GetEmployee(EmployeeType.CustomerServiceRep);
                var adminPage = new AdminPage(Driver, user);
                var aboutPage = new AboutPage(Driver, user);
                try
                {
                    adminPage.NavigateToAboutPage(user);
                    Reporter.LogTestStepForBugLogger(Status.Fail, "Forced navigation - Navigation must not work");
                }
                catch (Exception e)
                {
                    Reporter.LogPassingTestStepToBugLogger(Status.Pass, "Forced navigation Exception Failure:" + e);
                    aboutPage.AssertIsAboutPageNotVisible();
                }
                
            }


        }
    }
}
