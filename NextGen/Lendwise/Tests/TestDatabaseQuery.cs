﻿using Lendwise.Pages;
using NUnit.Framework;

namespace Lendwise.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            //[Category("Regression")]
            [TestCase(TestName = "TestDatabaseQuery_0000")]
            [Author("Pat Holman,", "pholman@softwise.com")]
            public void TestDatabaseQuery_0000()
            {
                var baseApplicationPage = new BaseApplicationPage(Driver);
                baseApplicationPage.GetSomeData();
            }
        }
    }
}