﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class QueuePriority : BaseApplicationPage
    {
        public QueuePriority(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.QueuePriority;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.QueuePriority;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.QueuePriority;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsQueuePriorityVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueuePriority))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Priority screen text is visible");
                Logger.Trace("Queue priority is Visible. ");
                return isVisible;
            }
        }

        public bool IsQueuePriorityInvisible
        {
            get
            {
                var queuePriorityText = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueuePriority))
                    .Text;
                var flag = (queuePriorityText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Priority screen text is not visible");
                Logger.Trace("Queue priority is not Visible. ");
                return flag;
            }
        }

        public void AssertIsQueuePriorityVisible()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueuePriority);
            Assert.IsTrue(IsQueuePriorityVisible, ErrorStrings.QueuePriorityNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is visible. ");
        }
        public bool IsQueuePriorityNotVisible
        {
            get
            {
                var isVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Priority screen text is not visible");
                Logger.Trace("Queue priority is Visible. ");
                return isVisible;
            }
        }

        public void AssertIsQueuePriorityNotVisibleCase()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueuePriority);
            Assert.IsTrue(IsQueuePriorityNotVisible?false:true, ErrorStrings.QueuePriorityVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }


        public void AssertIsQueuePriorityInvisible()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueuePriority);
            Assert.IsFalse(IsQueuePriorityInvisible, ErrorStrings.QueuePriorityVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

    }
}
