﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class TypeManagement : BaseApplicationPage
    {
        public TypeManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.TypeManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.TypeManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.TypeManagement;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsTypeManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ScreenText.TypeManagement))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Type Management is visible");
                Logger.Trace("Type Management is Visible. ");
                return isVisible;
            }
        }


        public bool IsTypeManagementInVisible
        {
            get
            {
                var flag = true;
                var typeManagementText = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ScreenText.TypeManagement))
                    .Text;

                if (typeManagementText!=Dummy.Text)
                {
                    flag = false;
                }
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Type Management is not visible");
                Logger.Trace("Type Management is not Visible. ");
                return flag;
            }
        }

        public void AssertTypeManagementVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ScreenText.TypeManagement);
            Assert.IsTrue(IsTypeManagementVisible, ErrorStrings.TypeManagementNotVisible);
        }

        public void AssertTypeManagementNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ScreenText.TypeManagement);
            Assert.IsTrue(IsTypeManagemenNotVisible?false:true, ErrorStrings.TypeManagementVisible);
        }

        public bool IsTypeManagementNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(InvalidUrl.Url); ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Type Management is not visible");
                Logger.Trace("Type Management is not Visible. ");
                return isNotVisible;
            }
        }

        public bool IsTypeManagemenNotVisible {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text) ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Type Management is not visible");
                Logger.Trace("Type Management is not Visible. ");
                return isNotVisible;
            }
        }

        public void AssertTypeManagementNotVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ScreenText.TypeManagement);
            Assert.IsTrue(IsTypeManagementNotVisible ? false:true, ErrorStrings.TypeManagementNotVisible);
        }


        //public void AssertTypeManagementNotVisible()
        //{
        //    WaitForElement(10, Elements.AdminPage.TypeManagement.ScreenText.TypeManagement);
        //    Assert.IsFalse(IsTypeManagementInVisible, ErrorStrings.TypeManagementVisible);
        //}


        public void ClickWarningTypes()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.Buttons.WarningTypes);
            var warningTypesButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.WarningTypes));
            warningTypesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Warning Types");
        }

        public void ClickNoteTypes()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.Buttons.NoteTypes);
            var noteTypesButton = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.NoteTypes));
            noteTypesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Note Types");
        }

        public void ClickApplictionWorkState()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.Buttons.ApplicationWorkState);
            var applicationWorkState =
                Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.ApplicationWorkState));
            applicationWorkState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Application Work StateColumn button");
        }

        public void ClickApplictionWithdrawReasons()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.Buttons.ApplicationWithdrawReasons);
            var appWithdrawReasonsButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.ApplicationWithdrawReasons));
            appWithdrawReasonsButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Application Withdraw Reason");
        }

        public void ClickAttachmentTypes()
        {
            WaitForElement(5, Elements.AdminPage.TypeManagement.Buttons.AttachmentTypes);
            var attachmentTypesButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.AttachmentTypes));
            attachmentTypesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Attachment Types button");
        }

        public void ClickStateTimeZoneMappings()
        {
            WaitForElement(5, Elements.AdminPage.TypeManagement.Buttons.StateTimeZoneMappings);
            var stateTimeZoneMappings =
                Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.StateTimeZoneMappings));
            stateTimeZoneMappings.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click State Time Zone Mappings");
        }
    }
}
