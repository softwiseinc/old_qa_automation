﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class DecisionEngineResponsePage : BaseApplicationPage
    {

        public DecisionEngineResponsePage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PendingDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PendingDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PendingDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsDeResponseMessageLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard loaded");
                Logger.Trace($"Pending Dashboard page loaded. ");
                return isVisible;
            }
        }

        public void AssertDeResponseMessageLoaded()
        {

        }


    }
}
