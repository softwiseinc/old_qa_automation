﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class AdverseActionSettings : BaseApplicationPage
    {
        public AdverseActionSettings(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.AdverseActionSettings;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.AdverseActionSettings;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.AdverseActionSettings;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsAdverseActionSettingsPageVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Adverse Action Settings page visible");
                Logger.Trace($"Adverse Action Settings page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsAdverseActionSettingsPageVisible()
        {
            Assert.IsTrue(IsAdverseActionSettingsPageVisible, ErrorStrings.AdverseActionSettingsPageNotVisible);

        }


    }
}
