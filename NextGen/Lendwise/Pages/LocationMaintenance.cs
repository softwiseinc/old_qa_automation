﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
namespace Lendwise.Pages
{
    class LocationMaintenance : BaseApplicationPage
    {
        public LocationMaintenance(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LocationMaintenance;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LocationMaintenance;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LocationMaintenance;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsLocationMaintenancePageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.LocationMaintenancePage.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Location Maintenance screen text is visible");
                Logger.Trace($"Location Maintenance page loaded. ");
                return isVisible;
            }
        }

        public bool IsLocationMaintenancePageNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Location Maintenance screen text is not visible");
                Logger.Trace($"Location Maintenance page loaded. ");
                return isNotVisible;
            }
        }
        public void AssertIsLocationMaintenancePageVisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.LocationMaintenancePage.ScreenText);
            Assert.IsTrue(IsLocationMaintenancePageVisible, ErrorStrings.SystemMaintenanceNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Location Maintenance page loaded after login in as Administrative agent. ");
        }
        public void AssertIsLocationMaintenancePageNotVisibleCase()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.LocationMaintenancePage.ScreenText);
            Assert.IsTrue(IsLocationMaintenancePageNotVisible?false:true, ErrorStrings.SystemMaintenanceVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Location Maintenance page is notloaded after login in as Administrative agent. ");
        }
    }
}
