﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class UserManagement : BaseApplicationPage
    {
        public UserManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UserManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UserManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UserManagement;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsUserManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page visible");
                Logger.Trace($"User Management page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsUserManagementVisible()
        {
            WaitForElement(10, Elements.AdminPage.UserManagement.ScreenText);
            Assert.IsTrue(IsUserManagementVisible, ErrorStrings.UserManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is visible. ");
        }

        public void AssertIsUserManagementNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.UserManagement.ScreenText);
            Assert.IsTrue(IsUserManagementNotVisible?false:true, ErrorStrings.UserManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

        public bool IsUserManagementInVisible
        {
            get
            {
                var userManagementText = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.ScreenText)).Text;
                var flag = false;

                if (userManagementText==Dummy.Text)
                {
                    flag = true;
                }

                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page is not visible");
                Logger.Trace($"User Management page loaded. ");
                return flag;
            }
        }

        public bool IsUserManagementNotVisible {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page is not visible");
                Logger.Trace($"User Management page is not loaded. ");
                return isNotVisible;
            }
        }

        public void AssertUserManagementNotVisible()
        {
            WaitForElement(10, Elements.AdminPage.UserManagement.ScreenText);
            Assert.IsFalse(IsUserManagementInVisible, ErrorStrings.UserManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }
    }
}
