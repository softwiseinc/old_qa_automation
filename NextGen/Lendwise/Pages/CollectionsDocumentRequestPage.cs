﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace Lendwise.Pages
{
    internal class CollectionsDocumentRequestPage : BaseApplicationPage
    {
        public CollectionsDocumentRequestPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsDocumentRequestPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsDocumentRequestPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsDocumentRequestPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsDocumentRequestPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections document request page loaded successfully");
                Logger.Trace($"collections document request page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertCollectionsDocumentRequestPageLoaded()
        {

            Assert.IsTrue(IsCollectionsDocumentRequestPageLoaded, ErrorStrings.CollectionsDocumentRequestPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the document request page did not load. ");
        }

        public void ClickGoToButtonForDisclosureRequestPage()
        {
           WaitForElement(5, Elements.DocumentRequestPage.Buttons.DisclosureRequestGoTo);
           var disclosureButton =
               Driver.FindElement(By.XPath(Elements.DocumentRequestPage.Buttons.DisclosureRequestGoTo));
           disclosureButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Disclosure Go To button");
        }

        public void ClickGoToButtonForApplicationRequestPage()
        {
            WaitForElement(5, Elements.DocumentRequestPage.Buttons.ApplicationRequestGoTo);
            var applicationButton =
                Driver.FindElement(By.XPath(Elements.DocumentRequestPage.Buttons.ApplicationRequestGoTo));
            applicationButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Disclosure Go To button");
        }


    }
}