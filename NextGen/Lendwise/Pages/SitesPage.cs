﻿
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;


namespace Lendwise.Pages
{
    internal class SitesPage : BaseApplicationPage
    {
        
            public SitesPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
            {
                if (user.RunEnvironment == "PROD")
                    TestEnvironment.Url = Url.Production.SitesPage;
                else if (user.RunEnvironment == "STAGE")
                    TestEnvironment.Url = Url.Staging.SitesPage;
                else if (user.RunEnvironment == "QA")
                    TestEnvironment.Url = Url.Test.SitesPage;
            }
            private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

            public bool IsSitesPageVisible
            {
                get
                {
                    var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.Sites.ScreenText)).Displayed;
                    Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                    Logger.Trace($"Sites page is visible. ");
                    return isVisible;
                }
            }

            public void AssertIsSitesPageVisible()
            {
                WaitForElement(10, Elements.AdminPage.Sites.ScreenText);
                Assert.IsTrue(IsSitesPageVisible ? true : false, ErrorStrings.SitesPageNotVisible);
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                    "Validate that the sites page loaded after login in as Administrative agent. ");
            }


    }
}
