﻿using System;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class AdminPage : BaseApplicationPage
    {
        public AdminPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Admin;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Admin;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Admin;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsAdminPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsAdminPageInvisible()
        {
            ExplicitWait(10);
            var flag = false;
            if (IsElementDisplayed(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.AdminPageIsVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the AdminPage page is not loaded after login in as Administrative agent.");
            Logger.Trace($"AdminPage page is not loaded. ");

        }

        public void AssertIsAdminPageVisible()
        {
            WaitForElement(5, Elements.AdminPage.ScreenText.AdminPageDashboard);
            Assert.IsTrue(IsAdminPageVisible ? true : false, ErrorStrings.AdminPageIsNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }

        public bool IsAdminPageNotVisible
        {
            get
            {
                var isVisible = Driver.Url.Contains(InvalidUrl.Url);
                // var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }



        public void AssertIsAdminPageNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.ScreenText.AdminPageDashboard);
            Assert.IsTrue(IsAdminPageNotVisible ? false : true, ErrorStrings.AdminPageIsVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }


        public void NavigateToAboutPage(Users.CurrentUser user)
       {
            var loginPage = new LoginPage(Driver, user);
            loginPage.LogInUser(user);
            loginPage.SelectLocation();
            loginPage.ClickAndSelectClose();
            var adminPage = new AdminPage(Driver, user);
            adminPage.ClickAboutButton();
           

       }
        
        private void ClickAboutButton()
       {
           WaitForElement(10, Elements.AdminPage.Buttons.About);
           var aboutButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.About));
           aboutButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click about button");
       }

        public void ClickPersonPopUp()
        {
           WaitForElement(10, Elements.AdminPage.Buttons.PersonPopUp);
           var personPopUpButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.PersonPopUp));
           personPopUpButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Person PrintApplicationPopup button");
        }


        public void ClickLogout()
        {
            WaitForElement(10, Elements.AdminPage.Buttons.Logout);
            ExplicitWait(2);
            var logoutButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.Logout));
            logoutButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click logout button");
        }

        public void ClickYes()
        {
           WaitForElement(10, Elements.AdminPage.Buttons.Yes);
           var yesButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.Yes));
           yesButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to logout. ");
        }

        public void ClickYesFail()
        {
            WaitForElement(10, Elements.AdminPage.Buttons.Yes);
            var yesButton = Driver.Url.Contains(InvalidUrl.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click yes to logout. ");
        }

        public void ClickTypeManagement()
        {
           WaitForElement(10, Elements.AdminPage.Buttons.TypeManagement);
           var typeManagementButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.TypeManagement));
           typeManagementButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Type Management");
        }

        public void ClickSystemConfiguration()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.SystemConfiguration);
           var enterpriseSettings = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.SystemConfiguration));
           enterpriseSettings.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Enterprise Settings");

        }

        public void ClickDocumentManagement()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.DocumentManagement);
            var documentManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.DocumentManagement));
            documentManagement.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Document Management button");
        }

        public void ClickUserManagement()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.UserManagement);
            var userManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.UserManagement));
            userManagement.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on User Management");
        }

        public void ClickQueueManagement()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.QueueManagement);
           var queueManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.QueueManagement));
           queueManagement.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Queue Management button");
        }

        public void ClickUserRolesManagement()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.UserRolesManagement);
            var userRolesManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.UserRolesManagement));
            userRolesManagement.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click User Roles Management");
        }


        public void ClickSecuritySettings()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.SecuritySettings);
            var securitySettings = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.SecuritySettings));
            securitySettings.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Security Settings button");
        }

        public void ClickWorkflowManagement()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.WorkflowManagement);
            var workFlowManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.WorkflowManagement));
            workFlowManagement.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Workflow Management");
        }

        public void ClickSystemMaintenanceTile()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.SystemMaintenance);
            var systemMaintenance = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.SystemMaintenance));
            systemMaintenance.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click System Maintenance tile");

        }

        public void ClickLoanTypeManagementTile()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.LoanTypeManagement);
           var loanTypeManagement = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.LoanTypeManagement));
           loanTypeManagement.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on Loan Type Management Tile");
        }

        public void ClickHamburgerMenu()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu);
            var hamburgerMenu = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu));
            hamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click menu button");
        }


        public void SelectApprovedDashboard()
        {
            WaitForElement(10, Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard);
            var approvedDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select approved Dashboard");
        }

        public void ClickCustomerDashboard()
        {
            WaitForElement(10, Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard);
            var customerDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard));
            customerDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Customer Dashboard");
        }

        public void ClickSitesButton()
        {
            WaitForElement(3, Elements.AdminPage.Buttons.Sites);
            var sites = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.Sites));
            sites.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click Sites");
        }

        public void ClickDeResponseMessages()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.DeResponseMessages);
           var deResponseButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.DeResponseMessages));
           deResponseButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click DeResponse Button");
        }

        public void ClickLoanInventory()
        {
           WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.LoanInventory);
           var loanInventoryButton = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.LoanInventory));
           loanInventoryButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select loan inventory page");
        }

        public void SelectLoanInventory()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.LoanInventory);
            var loanInventory = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.LoanInventory));
            loanInventory.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Loan Inventory");
        }


        public void SelectPendingDashboard()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.PendingDashboard);
            var pendingDashBoardOption =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.PendingDashboard));
            pendingDashBoardOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the pending dashboard");
        }

        public void SelectCmsPage()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.CollectionsManagement);
            var cmsPage = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.CollectionsManagement));
            cmsPage.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select CMS option");

        }

        public void SelectTemporaryPasswords()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.TemporaryPasswords);
           var temporaryPasswordsOptions = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.TemporaryPasswords));
           temporaryPasswordsOptions.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Temp Passwords");

        }

        public void GrabTemporaryPasswordList()
        {
            ExplicitWait(1);
            AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.TemporaryPasswordsPopup.PasswordList.ListOfPasswords);
        }

        public string StoreFirstTempPassword() 
        {
            WaitForElement(5, Elements.TemporaryPasswordsPopup.PasswordList.firstTempPassword);
            string firstTempPassword = Driver
                .FindElement(By.XPath(Elements.TemporaryPasswordsPopup.PasswordList.firstTempPassword)).Text;
            firstTempPassword = firstTempPassword.Substring(0);

            return firstTempPassword;
        }

        public void LogInAsCustomerServiceAgent()
        {
            var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
            var loginPage = new LoginPage(Driver, csrEmployee);
            //var pendingDashboardPage = new PendingDashboard(Driver, csrEmployee);
            var customerDashboardPage = new CustomerDashboard(Driver, csrEmployee);
            loginPage.LogInUser(csrEmployee);
            customerDashboardPage.AssertCustomerDashboardLoaded();

        }

        internal void ClickCloseTemporaryPasswordsPopup()
        {
            WaitForElement(5, Elements.TemporaryPasswordsPopup.Buttons.close);
            var close = Driver.FindElement(By.XPath(Elements.TemporaryPasswordsPopup.Buttons.close));
            close.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click close on the temp passwords popup");
        }


        public void ClickAdverseActionSettings()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.AdverseActionSettings);
            var adverseActionSettings = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.AdverseActionSettings));
            adverseActionSettings.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Adverse Action Settings");
        }

        public void ClickActionSettings()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.ActionSettings);
           var actionSettings = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.ActionSettings));
           actionSettings.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Action Settings button ");
        }

        public void ClickSearchByField()
        {
           WaitForElement(5, Elements.AdminPage.UserManagement.InputField.SearchByField);
           var searchField = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.InputField.SearchByField));
           searchField.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click search by field");
        }

        public void EnterEmailAddress()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.InputField.SearchByField);
            var searchBy = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.InputField.SearchByField));
            searchBy.SendKeys("@nostromorp.com");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter email address");
        }

        public void ClickAddNewButton()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.Buttons.AddNewButton);
            var addButton = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.Buttons.AddNewButton));
            addButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click add button");
        }

        public void ClickContextMenu()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.Buttons.ContextMenu);
            var menuButton = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.Buttons.ContextMenu));
            menuButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on menu button");
        }

        public void SelectTimeCard()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.ContextMenuOptions.TimeCard);
            var timecard = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.ContextMenuOptions.TimeCard));
            timecard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click add ");
        }

        public void ClickClockInField()
        {
           WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.InputFields.ClockIn);
           var clockInField =
               Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.InputFields.ClockIn));
           clockInField.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click clock in field");
        }

        public void ClickHours()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.HourAdjustmentArrowDown);
            var downArrow =
                Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons
                    .HourAdjustmentArrowDown));
            downArrow.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click arrow down on hours ");
        }

        public void ClickSetOnTimeIn()
        {
           WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Set);
           var set = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Set));
           set.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click set");
        }

        public void ClickClockOutField()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.InputFields.ClockOut);
            var clockInField =
                Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.InputFields.ClockOut));
            clockInField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click clock in field");
        }

        public void clickSetOnTimeOut()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Set);
            var set = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Set));
            set.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click set");
        }

        public void ClickSave()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Save);
            var save = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Save));
            save.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save");
        }

        public void ClickContextMenuOnTimeCardPopup()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.ContextMenu);
            var contextmenu =
                Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.ContextMenu));
            contextmenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click context menu");
        }

        public void SelectDelete()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Delete);
            var delete = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Delete));
            delete.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click delete");
        }

        public void ClickDeleteConfirm()
        {
            WaitForElement(5, Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Delete);
            var delete = Driver.FindElement(By.XPath(Elements.AdminPage.UserManagement.TimeCardPopup.Buttons.Delete));
            delete.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click delete");
        }

        public void SelectDecisionDashBoard()
        {
           WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.DecisionDashboardPage);
           var selectDecisionDashboard =
               Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.DecisionDashboardPage));
           selectDecisionDashboard.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Decision Dashboard ");
        }

        public void ClickEmployeeSchedules()
        {
           WaitForElement(5, Elements.AdminPage.Buttons.EmployeeSchedules);
           var employeeSchedules = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.EmployeeSchedules));
           employeeSchedules.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Employee Schedules");
        }
    }
}
