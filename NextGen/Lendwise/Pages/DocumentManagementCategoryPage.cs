﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class DocumentManagementCategoryPage : BaseApplicationPage
    {
        public DocumentManagementCategoryPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DocumentManagementCategoryPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DocumentManagementCategoryPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DocumentManagementCategoryPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsDocumentManagementCategoryPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Category page Loaded");
                Logger.Trace($"Category page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsDocumentManagementCategoryPageLoaded()
        {
            
            Assert.IsTrue(IsDocumentManagementCategoryPageLoaded, ErrorStrings.DocumentManagementCategoryDidNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }


        public void ClickPlusButton()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.Buttons.PlusButton);
            var plusButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.Buttons.PlusButton));
            plusButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Plus button ");
        }
    }
}
