﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class SystemConfiguration : BaseApplicationPage
    {
        public SystemConfiguration(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.SystemConfiguration;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.SystemConfiguration;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.SystemConfiguration;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsSystemConfigurationVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.SystemConfiguration.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public bool IsSystemConfigurationNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isNotVisible;
            }
        }

        public bool IsSystemConfigurationInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.SystemConfiguration.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page not  visible");
                Logger.Trace($"AdminPage page loaded. ");
                return flag;
            }
        }



        public void AssertIsSystemConfigurationVisible()
        {
            WaitForElement(10, Elements.AdminPage.SystemConfiguration.ScreenText);
            Assert.IsTrue(IsSystemConfigurationVisible, ErrorStrings.EnterpriseSettingsIsNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }

        public void AssertIsSystemConfigurationNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.SystemConfiguration.ScreenText);
            Assert.IsTrue(IsSystemConfigurationNotVisible?false:true, ErrorStrings.EnterpriseSettingsIsVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }

        public void AssertIsSystemConfigurationInvisible()
        {
            WaitForElement(10, Elements.AdminPage.SystemConfiguration.ScreenText);
            Assert.IsFalse(IsSystemConfigurationInvisible, ErrorStrings.EnterpriseSettingsIsVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }


    }


    
}