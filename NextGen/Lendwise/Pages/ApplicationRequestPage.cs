﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace Lendwise.Pages
{
    internal class ApplicationRequestPage: BaseApplicationPage
    {

        public ApplicationRequestPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationRequestPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationRequestPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationRequestPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        public bool IsApplicationRequestPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections document request page loaded successfully");
                Logger.Trace($"collections document request page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertApplicationRequestPageLoaded()
        {

            Assert.IsTrue(IsApplicationRequestPageLoaded, ErrorStrings.ApplicationRequestPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the document request page did not load. ");
        }

    }
}
