﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{
    internal class PendingDetail : BaseApplicationPage
    {
        public PendingDetail(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PendingDetail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PendingDetail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PendingDetail;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsPendingDetailVisible
        {
            get
            {

                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard loaded");
                Logger.Trace($"Pending Dashboard page loaded. ");

                return isVisible;
            }
        }


        public bool IsPendingDetailInvisible
        {
            get
            {
                var flag = (TestEnvironment.Url == Dummy.Url) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard is not loaded");
                Logger.Trace($"Pending Dashboard page mis not loaded. ");

                return flag;
            }
        }
        private bool IsWarningPopUpVisible()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            ExplicitWait(2);
            //elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));
            elementList.AddRange(Driver.FindElements(By.XPath(Elements.PendingDetailPage.WarningPopUp.Warning)));
            if (elementList.Count > 0)
                return true;
            return false;
        }

        private bool IsOverrideAllButtonVisible()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            //buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)));
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll)));
            if (buttonList.Count > 0)
                return true;
            return false;
        }
        private bool IsOkVisibleButton()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            //buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Okay)));
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.Okay)));
            if (buttonList.Count > 0)
                return true;
            return false;

        }
        private bool OverrideHandlerManagerPassword()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                //elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.PendingDetailPage.WarningPopUp.Warning)));
                if (elementList.Count > 0)
                {

                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.UserNameOverride))
                    //    .SendKeys(Users.User.OverrideUsername);
                    Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.InputFields.UserNameOverride)).SendKeys(Users.User.OverrideUsername);
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.PasswordOverride))
                    //    .SendKeys(Users.User.OverridePassword);
                    Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.InputFields.PasswordOverride)).SendKeys(Users.User.OverridePassword);
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)).Click();
                    Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll)).Click();
                    ExplicitWait(5);
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }

        public void AssertIsPendingDetailVisible()
        {
            
            WaitForElement(10, Elements.PendingDetailPage.ScreenText.PendingDetail);
            Assert.IsTrue(IsPendingDetailVisible, ErrorStrings.PendingDetailNotVisible);
            while (IsWarningPopUpVisible())
            {

                if (IsOverrideAllButtonVisible())
                {

                    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Warning Override all button is visible");
                    Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll)).Click();
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)).Click();
                    ExplicitWait(3);
                    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Override all button if it is visible");

                    if (OverrideHandlerManagerPassword())
                    {
                        Reporter.LogPassingTestStepToBugLogger(Status.Info, "Warning username and password was used");

                    }
                }
                if (IsOkVisibleButton())
                {
                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info, "Warning Okay button is visible");
                   // var okay = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Okay));
                    var okay = Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.Okay));
                    okay.Click();
                    Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp okay");
                }
            }
            //while (PressButtonText("Close"))
            //{
            //    Reporter.LogTestStepForBugLogger(Status.Info, "Warning is Visible");
            //    var close = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Close));
            //    close.Click();
            //    ExplicitWait(5);
            //    Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp Closed");
            //}
           
            //Assert.IsTrue(IsDuplicateDataVisible, "Duplicate data is not visible");
            var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
            var pendingDetailPage = new PendingDetail(Driver, csrEmployee);
            while (IsDuplicateDataVisible())
            {
                pendingDetailPage.DuplicateDataHelper();
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the pending detail page is visible. ");
        }

        public void DuplicateDataHelper()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            if (Helper.Page.ElementVisibilityState(Driver, "//*[@role='dialog']//*[text()='OK']"))
            {
                elementList.AddRange(Driver.FindElements(By.XPath("//*[@role='dialog']//*[text()='OK']")));
                foreach (var element in elementList)
                {
                    try
                    {
                        if (element.Displayed)
                            element.Click();
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine("Element Not visible ");
                    }
                }
            }
        }

        private bool IsDuplicateDataVisible()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            //buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Okay)));
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.PendingDetailPage.DuplicateWarningPopup.ScreenText)));
            if (buttonList.Count > 0)
                return true;
            return false;

        }

        public void AssertTextNotVisible(string validationText)
        {
            IWebElement body = Driver.FindElement(By.TagName("body"));
            String bodyText = body.Text;
            Assert.IsFalse(bodyText.Contains(validationText), ErrorStrings.PendingDetailTextNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validation Text is visible :" + validationText);
        }
        public bool IsSsnVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.PendingDetailPage.ScreenText.SocialSecurityNumber)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        public void AssertIsSsnVisible()
        {

            WaitForElement(5, Elements.PendingDetailPage.ScreenText.SocialSecurityNumber);
            Assert.IsTrue(IsSsnVisible, ErrorStrings.SsnIsNotVisible);
        }

        public bool PressButtonText(string text)
        {
            var checkXpath = $"//*[text()='{text}']";
            try
            {
                Driver.FindElement(By.XPath(checkXpath));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void AssertPendingDetailNotVisible()
        {
            ExplicitWait(10);
            Assert.IsFalse(IsPendingDetailInvisible, ErrorStrings.PendingDetailVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the pending detail page is not visible. ");
        }





        public void ClickValidateId()
        {
           WaitForElement(10, Elements.PendingDetailPage.Buttons.ValidateId);
           var validateId = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidateId));
           validateId.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate the loan action");
        }
        
        public void CancelLoanValidationPopup()
        {
            ExplicitWait(5);
            ClickIfPresent(Elements.PendingDetailPage.LoanValidationPopUp.CancelBtn, "Click on Popup cancel button", null);
        }
        public void SelectNoteType()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInput);
            Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInput)).Click();
            var noteType = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInput));
            noteType.SendKeys("Called");
            noteType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click note type enter happy test");
        }
        
        public string EnterNote()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.AddNote);
            var addNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.AddNote));
            var randomText = GetRandomText(15);
            addNote.SendKeys(Users.User.Note + "\n" +randomText);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a note");
            return randomText;
        }

        public string GetRandomText(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(DateTime.Now.Millisecond);
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString().ToUpper();
        }

        public void ClickSaveNote()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.SaveNote);
            var saveNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.SaveNote));
            saveNote.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save Note");
        }
        public void ClickSaveNoteFailCase()
        {
            WaitForElement(10, Elements.PendingDetailPage.Notes.Buttons.Save);
            var saveNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Notes.Buttons.Save));

            var flag = true;
            if(saveNote.Text == DummyText.Text)
            {
                flag = false;
                saveNote.Click();
            }
            Assert.IsTrue(flag, ErrorStrings.SaveNoteNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Save Note");
        }

        public void AssertClickSaveNoteFail()
        {
            ExplicitWait(3);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.PendingDetailPage.PleaseAddNotePopUp.InvalidateAddNote);
            Assert.IsTrue(elementState, ErrorStrings.SaveNoteNotVisibleClicked);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the save note is clicked.");
            Logger.Trace($"Save note is clicked. ");
            
        }

        public void ClickValidateIncome()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.PendingDetailPage.Buttons.ValidateIncome);
           var validateIncome = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidateIncome));
           validateIncome.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click Validate Income");
        }
        public void ClickValidateAddress()
        {
            
            WaitForElement(10, Elements.PendingDetailPage.Buttons.ValidateAddress);
            var validateAddress = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidateAddress));
            validateAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate Address");
        }

        public void ClickValidatePhone()
        {
            WaitForElement(10, Elements.PendingDetailPage.Buttons.ValidatePhone);
            var validateAddress = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidatePhone));
            validateAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate Address");
        }

        public void ClickValidateBank()
        {
            WaitForElement(10, Elements.PendingDetailPage.Buttons.ValidateBank);
            var validateBank = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidateBank));
            validateBank.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate Income");
        }


        public void ClickProcessLoan()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.PendingDetailPage.Buttons.ProcessLoan);
           var processLoanButton = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ProcessLoan));
           processLoanButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click process loan");
        }

        public void ClickAcknowledged()
        {
           ExplicitWait(2);
           WaitForElement(5,Elements.PendingDetailPage.PasswordResetRequestPopup.Acknowledged);
           var acknowledgeButton =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.PasswordResetRequestPopup.Acknowledged));
           acknowledgeButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Acknowledge button");
        }

        public void ClickAcknowledgedFailCase()
        {
           
            ExplicitWait(2);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.PasswordResetRequestPopup.Acknowledged)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.AcknowledgedButtonNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Acknowledge button");
        }

        public void AssertClickAcknowledgedFail()
        {
            ExplicitWait(3);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.PendingDetailPage.PasswordResetRequestPopup.AcknowledgedFailCase);
            Assert.IsTrue(elementState, ErrorStrings.ClickedOnAcknowledgeBtn);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Acknowledge' button is not clicked.");
            Logger.Trace("'Acknowledge' button is not found. ");
            

        }

        public void ClickRightHamburgerMenu()
        {
            
            WaitForElement(7, Elements.PendingDetailPage.RightMenu.HamburgerMenu.ClickHamburgerMenu);
            var clickRightHamburgerMenu =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenu.ClickHamburgerMenu));
            clickRightHamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Right hamburger menu");
        }


        public void SelectCancelApplication()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.CancelApplication);
           var cancelApplication =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.CancelApplication));
           cancelApplication.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Cancel Application ");
        }
       
        public void CancelReasonType()
        {

            WaitForElement(5, Elements.PendingDetailPage.RightMenu.CancelApplication.ReasonType);
            var reasonType =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.CancelApplication.ReasonType));
            reasonType.SendKeys(Users.User.ReasonType);
            reasonType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select reason for cancellation ");
        }

        public void CancellationNote()
        {
            
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.CancelApplication.CancelNotes);
            var notes = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.CancelApplication.CancelNotes));
            notes.SendKeys(Users.User.NoteReason);
            ExplicitWait(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Note for cancelling ");
        }


        public void ClickWithdraw()
        {
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.Buttons.Withdraw);
            var withdraw = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.Buttons.Withdraw));
            withdraw.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Withdraw ");
        }
        public void ClickWithdrawFailCase()
        {
            
            ExplicitWait(7);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.RightMenu.Buttons.Withdraw)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.ClickWithdrawNotFound);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Withdraw button ");


        }


        public void AssertClickWithdrawFailCondition()
        {
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.Buttons.Withdraw);
            var flag = false;
            if (IsElementDisplayed(By.XPath(Elements.PendingDetailPage.RightMenu.Buttons.Withdraw)))
            {
                flag = true;
            }
               
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Withdraw' button is not found.");
            Logger.Trace("'Withdraw' button is not found. ");
            
            Assert.IsTrue(flag, ErrorStrings.WithdrawButtonFound);
        }


        public void AssertCancelLoanFailCondition()
        {
            ExplicitWait(5);
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.PendingDetailPage.RightMenu.CancelApplication.CancelNotes)))
            {
                flag = false;
            }
          
            

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Withdraw' button is not found.");
            Logger.Trace("'Withdraw' button is not found. ");


            Assert.IsTrue(flag, ErrorStrings.WithdrawButtonFound);
        }

    


        public void SelectEditLoan()
        {
            ExplicitWait(3);
           WaitForElement(5, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.Edit);
           var selectEdit =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.Edit));
           selectEdit.Click();
            
        }

        public string EnterNoteOnPendingDetailMainPage()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.AddNotePendingDetailMain);
            var addNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.AddNotePendingDetailMain));
            var randomText = GetRandomText(15);
            addNote.SendKeys(Users.User.Note + "\n" + randomText);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a note");
            return randomText;
        }

        public void SelectSetWorkState()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.SetWorkState);
            var selectSetWorkState =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.SetWorkState));
            selectSetWorkState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select work state reason");
        }

        public void SelectWorkStateType()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp.WorkStateReason.UnassignLoan);
            var workStateReason = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp
                .WorkStateReason.UnassignLoan));
            workStateReason.SendKeys("Follow-up 30 Minutes");
            ExplicitWait(3);
            workStateReason.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter unassigned loan and click enter");
        }


        public void ClickAssign()
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp.Buttons.Assign);
           var reassign =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp.Buttons.Assign));
           reassign.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Assign. ");
        }
        public void ClickAssignFailCase()
        {
            ExplicitWait(5);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp.Buttons.Assign)))
            {
                flag = true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Assign. ");
            Assert.IsTrue(flag, ErrorStrings.AssignBtnNotClicked);

        }

        public void AssertClickAssign()
        {
            ExplicitWait(5);
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.PendingDetailPage.RightMenu.SetWorkStatePopUp.Buttons.Assign)))
            {
                flag = false;
            }

       

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Assign' button is not clicked.");
            Logger.Trace("'Assign' button is not found. ");
            Assert.IsTrue(flag, ErrorStrings.AssignBtnClicked);

           
        }

        public void SelectWarning()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.Warnings);
            var warnings =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.Warnings));
            warnings.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warnings ");
        }


        public void ClickAddButton()
        {
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.AddWarning);
            var addWarning =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.AddWarning));
            addWarning.Click();
        }

        public void SelectWarningType()
        {
            ExplicitWait(7);
           var selectWarningType =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningType));
            selectWarningType.SendKeys("Manager Override"); //Users.user.WarningType
            selectWarningType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter type of warning and click enter");

        }

        
        public void EnterWarningNotes()
        {
            ExplicitWait(5);
            WaitForElement(10, Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningNote);
            var enterWarningNotes =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningNote));
            enterWarningNotes.SendKeys("Attention");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter warning note");
        }

        public void ClickWarningSave()
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.Save);
            var saveButton =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save ");
        }
        public void AssertClickWarningSaveFailCase()
        {
          
            ExplicitWait(7);
            var flag = true;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.ReassignApplication)))
            {
                flag = false;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click save ");
            Logger.Trace("'Save' button is not found. ");
            Assert.IsTrue(flag, ErrorStrings.CustomerDetailNoteSaveFound);


        }
        public void AssertClickWarningSaveFailCondition()
        {
            ExplicitWait(5);
            var flag = false;
            if (IsElementDisplayed(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions .ReassignApplication))) //Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.SaveFail
            {
                flag = true;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate Save' button is not clicked.");
            Logger.Trace("'Save' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.CustomerDetailNoteSaveFound);

        }

        public void SelectReassignApplication()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.ReassignApplication);
            var reassignApp =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions
                    .ReassignApplication));
            reassignApp.Click();

        }

        public void SelectAssignTo()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.AssignTo);
           var assignToUser =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.AssignTo));
            //assignToUser.Click();
           assignToUser.SendKeys("Goldsmith, Jerry");
           assignToUser.SendKeys(Keys.Enter);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new user that will be assigned current loan");
        }


        public void ClickRejectId()
        {
           WaitForElement(5, Elements.PendingDetailPage.RejectLoanActions.RejectId);
           var rejectId = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RejectLoanActions.RejectId));
           rejectId.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click reject loan ID");
        }

        public void ClickOnFirstNav()
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.PendingDetailPage.RejectLoanActions.WorkFlowFirstNav);
            var firstNav = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RejectLoanActions.WorkFlowFirstNav));
            firstNav.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Workflow first Nav");
        }

        public void SelectRejectNoteType()
        {
            WaitForElement(10, Elements.PendingDetailPage.PleaseAddNotePopUp.RejectNoteType);
            var rejectNoteType =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectNoteType));
            
            rejectNoteType.SendKeys("Called"); //Users.user.RejectNoteType
            ExplicitWait(2);
            rejectNoteType.SendKeys(Keys.Enter);
        }

        public void EnterRejectNote()
        {
           WaitForElement(5, Elements.PendingDetailPage.PleaseAddNotePopUp.RejectAddNote);
           var addNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectAddNote));
           addNote.SendKeys("Called customer");
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter reason loan action is being rejected");
        }

        public void ClickSaveRejectNote()
        {
           WaitForElement(5, Elements.PendingDetailPage.PleaseAddNotePopUp.RejectSaveNote);
           var saveNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectSaveNote));
           saveNote.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save Note");
        }
        public void ClickSaveRejectNoteFailCase()
        {
            ExplicitWait(5);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectSaveNote)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.SaveNoteNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Save Note");

        }


        public void AssertClickSaveRejectNoteFail()
        {


            ExplicitWait(5);

           // var saveNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectSaveNote));
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.RejectNoteType)))
            {
                flag = false;
            }
            Assert.IsTrue(flag, ErrorStrings.SaveNoteVisibleClicked);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the 'Save Note' is visible and clicked. ");
            Logger.Trace($"'Save Note' is visible and note.");
        }

        public void ClickWarningClose()
        {
            ExplicitWait(7);
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons.Close);
            var close = Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.AddWarningPopUp.WarningButtons
                .Close));
            close.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Close ");
        }

        public void PageRefresh()
        {
            Driver.Navigate().Refresh();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Page must reload");
        }

        public void OverrideUserName()
        {

            WaitForElement(10, Elements.PendingDetailPage.WarningPopUp.InputFields.UserName);
            var overrideUsername =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.InputFields.UserName));
            overrideUsername.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Enter Override ManagerUsername");

        }

        public void OverridePassword()
        {
            
            WaitForElement(10, Elements.PendingDetailPage.WarningPopUp.InputFields.Password);
            var overridePassword =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.InputFields.Password));
            overridePassword.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Override ManagerPassword ");
        }

        public void WarningsDelete()
        {
            WaitForElement(10, Elements.PendingDetailPage.WarningPopUp.Buttons.Delete);
            var delete = Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.Delete));
            delete.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click delete warning ");
        }
        public void AssertWarningsDeleteFailCase()
        {
            WaitForElement(5, Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll)))
            {
                flag = true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click delete warning ");
            Logger.Trace("'Delete' button is not found. ");
            Assert.IsTrue(flag, ErrorStrings.WarningDeleteBtnNotClicked);

        }

        public void AssertWarningsDeleteFail()
        {
    
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.Delete)))
            {
                flag = false;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Delete' button is not clicked.");
            Logger.Trace("'Delete' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.WarningDeleteBtnClicked);

        }

        public void SelectResetPassword()
        {
            ExplicitWait(3);
           WaitForElement(10, Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.ResetPassword);
           var resetPassword =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.HamburgerMenuOptions.ResetPassword));
           resetPassword.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Reset ManagerPassword");
        }

        public void ClickAcknowledge()
        {
            ExplicitWait(4);
            WaitForElement(5, Elements.PendingDetailPage.ApplicationStatusPopUp.Acknowledge);
            var acknowledge =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.ApplicationStatusPopUp.Acknowledge));
            acknowledge.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click acknowledge ");
        }

        public void ClickWorkItemId()
        {
            ExplicitWait(3);

            WaitForElement(5, Elements.PendingDetailPage.LoanWorkItems.Id);
           var id = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanWorkItems.Id));
           id.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click ID so the next method will invalidate this loan action");
        }

       
        public void InvalidateWorkItemId()
        {
            if (IsElementPresent(By.XPath(Elements.PendingDetailPage.InvalidateLoanActions.InvalidateId)))
            {
                WaitForElement(5, Elements.PendingDetailPage.InvalidateLoanActions.InvalidateId);
                var invalidateId =
                    Driver.FindElement(By.XPath(Elements.PendingDetailPage.InvalidateLoanActions.InvalidateId));
                invalidateId.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click invalidate loan action");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "invalidate loan action not found on page.");
            }
            
        }

        public void EnterInvalidateNote()
        {
            WaitForElement(5, Elements.PendingDetailPage.PleaseAddNotePopUp.InvalidateAddNote);
            var invalidateNote =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.PleaseAddNotePopUp.InvalidateAddNote));
            invalidateNote.SendKeys(Users.User.InvalidateLoanActionNote);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Enter note regarding the invalidating the loan");
        }

        public void ClickBacktoDashboard()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDetailPage.Buttons.BackToDashboard);
            var backToDashboard = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.BackToDashboard));
            backToDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Back to Dashboard");
        }

        public void ClickClearAll()
        {
            ExplicitWait(3);
           WaitForElement(5, Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.Buttons.ClearAll);
           var clearAll =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.Buttons.ClearAll));
           clearAll.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click clear all");
        }

        public void ClickSave()
        {
            WaitForElement(5, Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.Buttons.Save);
            var save = Driver.FindElement(
                By.XPath(Elements.PendingDetailPage.RightMenu.ReassignToUserPopUp.Buttons.Save));
            save.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save button");
        }

        public string GetApplicationId()
        {
            return Driver.FindElement(By.XPath(Elements.PendingDetailPage.ScreenText.ApplicationId)).Text;
        }

        public bool ClickApplicationId(string appId)
        {
            ExplicitWait(5);
            bool isIdFound = false;
            var applicationTable = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.ApplicationId));


            foreach (var applicationId in applicationTable)
            {
                try
                {

                    applicationTable = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.ApplicationId));
                    applicationId.Click();
                       
                        
                    
                }
                catch (Exception )
                {
                    applicationTable = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.ApplicationId));
                    applicationId.Click();
                }

                //if (applicationId.Text == appId)
                //    break;
            }

            //foreach (var applicationId in applicationTable)
            //{
            //    if (applicationId.Text == appId)
            //    {
            //        applicationId.Click();
            //        isIdFound = true;
            //        break;
            //    }
            //}
            return isIdFound;
        }

        public bool ClickApplicationIdByLastName(string lastName)
        {
            ExplicitWait(5);
            bool isIdFound = false;
            var applicationTable = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.ApplicationRow));
            
            for (int i = 1; i < applicationTable.Count; i++)
            {
                string applicationRow = Elements.PendingDashboardTable.ApplicationRowPartial + i + "]";
                var rowText = Driver.FindElement(By.XPath(applicationRow)).Text;
                if (rowText.Contains(lastName))
                {
                    string applicationId = applicationRow + Elements.PendingDashboardTable.ApplicationIdPartial;
                    Driver.FindElement(By.XPath(applicationId)).Click();
                    isIdFound = true;
                    break;
                }
            }
            return isIdFound;
        }

        public void ClickBankWorkItem()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanWorkItems.Bank);
            var bankWorkItem = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanWorkItems.Bank));
            bankWorkItem.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Bank work item");
        }

        public void ClickHistoricalAccount()
        {
            WaitForElement(5, Elements.PendingDetailPage.OptionsOnWorkItems.HistoricalAccounts);
            var historicalAccountsCheckBox =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.OptionsOnWorkItems.HistoricalAccounts));
            historicalAccountsCheckBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Historical Accounts");
        }

        public void ClickDeniedWorkItem()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanWorkItems.Id);
            var loanWorkItem = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanWorkItems.Id));
            loanWorkItem.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click ID");
        }

        public void ClickWorkItemBank()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanWorkItems.Bank);
            var bank = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanWorkItems.Bank));
            bank.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click new work item");
        }

        public void Refresh()
        {
            Driver.Navigate().Refresh();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Page must reload");
        }

        public void SelectWorkItemNoteType()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.NoteType);
            var typeOfNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteType));
            typeOfNote.SendKeys("Other");
            typeOfNote.SendKeys(OpenQA.Selenium.Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter note type");
        }

        public void EnterNoteForWorkItem()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.AddNote);
            var note = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.AddNote));
            note.SendKeys(Users.User.Note);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a note");
        }

        public void SaveWorkItemPopupNote()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.SaveNote);
            var noteSave = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.SaveNote));
            noteSave.Click();
        }

        public void ClickValidatePreviousLoan()
        {
           WaitForElement(5, Elements.PendingDetailPage.Buttons.ValidatePreviousLoan);
           var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
            var pendingDetailPage = new PendingDetail(Driver, csrEmployee);
           var validatePreviousLoan =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidatePreviousLoan));
            if (IsElementVisible(By.XPath(Elements.PendingDetailPage.Buttons.ValidatePreviousLoan)))
           {
               validatePreviousLoan.Click();
               pendingDetailPage.SelectNoteType();
               pendingDetailPage.EnterNote();
               pendingDetailPage.ClickSaveNote();
            }

           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate previous loans ");
        }
        public void ClickSsn()
        {
            WaitForElement(5, Elements.PendingDetailPage.ScreenText.SocialSecurityNumber);
            var viewSsn = Driver.FindElement(By.XPath(Elements.PendingDetailPage.ScreenText.SocialSecurityNumber));
            viewSsn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click SSN and override ");
        }

        public void EnterManagerUserName()
        {
            
            WaitForElement(5, Elements.PendingDetailPage.UnmaskSsnPopup.InputFields.ManagerUsername);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.UnmaskSsnPopup.InputFields.ManagerUsername));
            usernameFieldInput.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager username");
        }

        public void EnterMangerPassword()
        {
            WaitForElement(5, Elements.PendingDetailPage.UnmaskSsnPopup.InputFields.ManagerPassword);
            var passwordFieldInput =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.UnmaskSsnPopup.InputFields.ManagerPassword));
            passwordFieldInput.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter password");
        }

        public void ClickOkayOnUnmaskSsnPopup()
        {
            WaitForElement(5, Elements.PendingDetailPage.UnmaskSsnPopup.Buttons.Ok);
            var okbutton = Driver.FindElement(By.XPath(Elements.PendingDetailPage.UnmaskSsnPopup.Buttons.Ok));
            okbutton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");

        }


        public void ClickIdToDoYellowNote()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.PendingDetailPage.WorkItemsToDo.IdToDo);
            var clickIdToDoYellowNote = Driver.FindElement(By.XPath(Elements.PendingDetailPage.WorkItemsToDo.IdToDo));
            clickIdToDoYellowNote.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yellow note it must expand.");
        }

        public void SwitchTab()
        {

           Actions action = new Actions(new ChromeDriver());
           action.SendKeys(Keys.Control).SendKeys(Keys.Tab).Build().Perform();
            
        }

        public void NewTab()
        {
            Actions action = new Actions(new ChromeDriver());
            action.SendKeys(Keys.Control + "t");
        }

        public void SelectMultipleReasonTypes()
        {
           WaitForElement(5, Elements.PendingDetailPage.RightMenu.CancelApplication.ReasonType);
           var reasonType =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.CancelApplication.ReasonType));
           reasonType.SendKeys(Users.User.ReasonType);
           reasonType.SendKeys(Keys.Enter);
           ExplicitWait(1);
           var noteTypeClick =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.RightMenu.CancelApplication.CancelNotes));
           noteTypeClick.Click();
           ExplicitWait(1);
           reasonType.Click();
           reasonType.SendKeys(Users.User.ReasonTypeC);
           reasonType.SendKeys(Keys.Enter);
           
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select multiple reasons for the cancellation reason type");
           
        }

        public void ClickValidateSSN()
        {
            WaitForElement(5, Elements.PendingDetailPage.Buttons.ValidateSSN);
            var validateSSN = Driver.FindElement(By.XPath(Elements.PendingDetailPage.Buttons.ValidateSSN));
            validateSSN.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Validate ssn ");
        }

        public void ClickWarningsOverrideAll()
        {
           
            WaitForElement(5, Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll);
            var overrideAllButton =
                Driver.FindElement(By.XPath(Elements.PendingDetailPage.WarningPopUp.Buttons.OverrideAll));
            overrideAllButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click override all button ");
        }

        public void SelectNoteTypeOnInvalidateAction()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInputOnInvalidate);
            Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInputOnInvalidate)).Click();
            var noteType = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeInputOnInvalidate));
            noteType.SendKeys("Called Customer");
            noteType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click note type enter happy test");
        }

        public void SelectNoteTypeOnPendingDetailMain()
        {
            WaitForElement(5, Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeMainPendingDetailPage);
            Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeMainPendingDetailPage)).Click();
            var noteType = Driver.FindElement(By.XPath(Elements.PendingDetailPage.LoanValidationPopUp.NoteTypeMainPendingDetailPage));
            noteType.SendKeys("Called Customer");
            noteType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click note type enter called customer");
        }

        public void ClickConfirmButton()
        {
           WaitForElement(5, Elements.PendingDetailPage.ConfirmPasswordResetRequestPopup.Buttons.Confirm);
           var confirm =
               Driver.FindElement(By.XPath(Elements.PendingDetailPage.ConfirmPasswordResetRequestPopup.Buttons
                   .Confirm));
           confirm.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click confirm");
        }
    }
}
