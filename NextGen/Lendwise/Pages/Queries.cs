﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class Queries : BaseApplicationPage
    {
        public Queries(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Queries;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Queries;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Queries;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsQueriesPageVisible
        {
            get
            {
                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queries page visible");
                Logger.Trace($"Queries page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsQueriesPageVisible()
        {
            Assert.IsTrue(IsQueriesPageVisible, ErrorStrings.QueriesPageNotVisible);
            
        }

    }
}
