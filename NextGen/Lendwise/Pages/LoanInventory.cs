﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI.WebControls;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class LoanInventory : BaseApplicationPage
    {


        public LoanInventory(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanInventory;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanInventory;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanInventory;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsLoanInventoryVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Customer Detail screen text is visible");
                Logger.Trace("Customer Detail is Visible. ");
                return isLoaded;
            }
        }

        public void AssertIsLoanInventory()
        {
            
            Assert.IsTrue(IsLoanInventoryVisible, ErrorStrings.LoanInventoryIsNotVisible);
           
        }

        public void ClickLoanStatus()
        {
            WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanDropDownMenu);
            var loanStatus = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanDropDownMenu));
            loanStatus.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on loan status field");

        }

        public void ClickCustomerId()
        {
            WaitForElement(5, Elements.LoanInventoryPage.Links.CustomerId);
            var customerIdLink = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.Links.CustomerId));
            customerIdLink.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Customer ID");
        }

        public void ClickCustomerName()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.LoanInventoryPage.Links.CustomerName);
           var customerNameLink = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.Links.CustomerName));
           customerNameLink.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click customer name ");
        }

        public void SelectFunded()
        {
            WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Funded);
            var funded =
                Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Funded));
            funded.Click();
            ExplicitWait(2);
            //funded.SendKeys(Keys.Enter);
            
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Funded");
        }

        public void ClickState()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateDropDownMenu);
            var stateButton = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateDropDownMenu));
            
            stateButton.SendKeys(Keys.Return);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Sate column");
        }

        public void SelectState()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Utah);
            var utah = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Utah));
            //utah.SendKeys("UT");
            utah.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Utah as the state");
        }

        
        public void SelectApproved()
        {
            WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Approved);
            var approvedOption =
                Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Approved));
            approvedOption.Click();
            ExplicitWait(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Approved Option");
        }

        public void SelectTexas()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Texas);
            var txOption = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Texas));
            txOption.Click();
            ExplicitWait(8);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Texas as the state");
        }

        public void SelectLateAsTheStatus()
        {
           WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Late);
           var lateOption =
               Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Late));
           lateOption.Click();
           ExplicitWait(2);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Late as the option");
        }

        public void SelectWyoming()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Wyoming);
            var wyOption = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Wyoming));
            wyOption.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Select Wyoming as the state");
        }

        public void SelectClosedAsTheStatus()
        {
           WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Closed);
           var closedOption =
               Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Closed));
           closedOption.Click();
           ExplicitWait(2);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Closed as the options");
        }

        public void SelectIdaho()
        {
           WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Idaho);
           var idOption = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Idaho));
           idOption.Click();
           ExplicitWait(9);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Idaho as the state");

        }

        public void SelectDefaultedAsTheStatus()
        {
            WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Defaulted);
            var defaultedOption =
                Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Defaulted));
            defaultedOption.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the default option");
        }

        public void SelectRescindedAsLoanStatus()
        {
           WaitForElement(5, Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Rescinded);
           var rescindedOption =
               Driver.FindElement(By.XPath(Elements.LoanInventoryPage.LoanStatus.LoanStatusMenuOptions.Rescinded));
           rescindedOption.Click();
           ExplicitWait(5);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the Rescinded option");

        }

        public void SelectMo()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Missouri);
            var missouriOption = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Missouri));
            missouriOption.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the Missouri as the state");
        }

        public void ClickClipBoardOnCustomerID()
        {
            ExplicitWait(15);
            WaitForElement(5, Elements.LoanInventoryPage.Clipboards.CustomerIdClipBoard);
            var customerId = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.Clipboards.CustomerIdClipBoard));
            customerId.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the clipboard for the customer ID number to be copied");
        }

        public void PasteCustomerIdInCustomerIdField()
        {
            WaitForElement(5, Elements.LoanInventoryPage.InputFields.CustomerIdField);
            var customerIdInputField =
                Driver.FindElement(By.XPath(Elements.LoanInventoryPage.InputFields.CustomerIdField));
            customerIdInputField.SendKeys(Keys.Control + "v");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Paste customer ID in the Customer ID field");
        }

        public void ClickLoanIdClipBoard()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.LoanInventoryPage.Clipboards.LoanIdClipBoard);
            var loanId = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.Clipboards.LoanIdClipBoard));
            loanId.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the loan ID clipboard ");
        }

        public void PasteLoanIdNumberInLoanIdField()
        {
            ExplicitWait(3);
           WaitForElement(5, Elements.LoanInventoryPage.InputFields.LoanIdField);
           var loanIdInputField = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.InputFields.LoanIdField));
           loanIdInputField.SendKeys(Keys.Control + "v");
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Paste loan id in the loan id input field");
        }

        public void ClickExportFile()
        {
            ExplicitWait(20);
            WaitForElement(5, Elements.LoanInventoryPage.ExportFile.ExportFileLink);
            var exportFile =
                Driver.FindElement(By.XPath(Elements.LoanInventoryPage.ExportFile.ExportFileLink));
            exportFile.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the export file");
        }

        public void SelectUtah()
        {
            WaitForElement(5, Elements.LoanInventoryPage.State.StateOptions.Utah);
            var utahStateOption = Driver.FindElement(By.XPath(Elements.LoanInventoryPage.State.StateOptions.Utah));
            utahStateOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Utah as the state");
        }
    }
}
