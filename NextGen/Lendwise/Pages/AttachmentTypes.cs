﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
namespace Lendwise.Pages
{
    internal class AttachmentTypes : BaseApplicationPage
    {

        public AttachmentTypes(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.AttachmentTypes;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.AttachmentTypes;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.AttachmentTypes;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsAttachmentTypesVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.Buttons.AttachmentTypes)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsAttachmentTypesVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.Buttons.AttachmentTypes);
            Assert.IsTrue(IsAttachmentTypesVisible, ErrorStrings.AttachmentTypesTextNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }







    }
}
