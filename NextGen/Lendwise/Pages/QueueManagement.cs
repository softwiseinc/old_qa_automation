﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class QueueManagement : BaseApplicationPage
    {
        public QueueManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.QueueManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.QueueManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.QueueManagement;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsQueueManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueueManagement)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page visible");
                Logger.Trace($"User Management page loaded. ");
                return isVisible;
            }
        }
        public bool IsQueueManagementNotVisibleCase
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page is not visible");
                Logger.Trace($"User Management page loaded. ");
                return isNotVisible;
            }
        }

        public void AssertIsQueueManagementVisible()
        {
            WaitForElement(10, Elements.AdminPage.QueueManagementPage.ScreenText.QueueManagement);
            Assert.IsTrue(IsQueueManagementVisible, ErrorStrings.QueueManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is visible. ");
        }

        public void AssertIsQueueManagementNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.QueueManagementPage.ScreenText.QueueManagement);
            Assert.IsTrue(IsQueueManagementNotVisibleCase?false:true, ErrorStrings.QueueManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text isnot visible. ");
        }

        public bool IsQueueManagementNotVisible
        {
            get
            {
                var queueManagementText = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueueManagement)).Text;
                var flag = false;
                if (queueManagementText == Dummy.Text)
                {
                    flag = true;
                }

                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page visible");
                Logger.Trace($"User Management page loaded. ");
                return flag;
            }
        }


        public void AssertIsQueueManagementNotVisible()
        {
            WaitForElement(10, Elements.AdminPage.QueueManagementPage.ScreenText.QueueManagement);

            Assert.IsFalse(IsQueueManagementNotVisible, ErrorStrings.QueueManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

        public void ClickSettings()
        {
           WaitForElement(5, Elements.AdminPage.QueueManagementPage.Buttons.Settings);
           var settings = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.Buttons.Settings));
           settings.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click settings button");
        }

        public void ClickPriority()
        {
           WaitForElement(5, Elements.AdminPage.QueueManagementPage.Buttons.Priority);
           var priority = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.Buttons.Priority));
           priority.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Priority button");
        }
    }
}
