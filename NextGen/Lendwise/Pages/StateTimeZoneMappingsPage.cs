﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class StateTimeZoneMappingsPage : BaseApplicationPage
    {
        public StateTimeZoneMappingsPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StateTimeZoneMappings;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StateTimeZoneMappings;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StateTimeZoneMappings;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsStateTimeZoneMappingsPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the State Time Zone Mappings page loaded");
                Logger.Trace($"State Time Zone Mappings page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsStateTimeZoneMappingsPageLoaded()
        {
            Assert.IsTrue(IsStateTimeZoneMappingsPageLoaded, ErrorStrings.StateTimeZoneMappingsPageDidNotLoad);

        }



    }
}
