﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class WarningTypes : BaseApplicationPage
    {

        public WarningTypes(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.WarningTypes;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.WarningTypes;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.WarningTypes;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsWarningTypesVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.WarningTypesPage.ScreenText.WarningTypes))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the bread crumb Warning types is visible");
                Logger.Trace("Warning types is Visible. ");
                return isVisible;
            }
        }

        public bool IsWarningTypesInvisible
        {
            get
            {
                var flag = false;
                var warningTypesText = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.WarningTypesPage.ScreenText.WarningTypes))
                    .Text;
                if (warningTypesText == Dummy.Text)
                {
                    flag = true;
                }
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the bread crumb Warning types is not visible");
                Logger.Trace("Warning types is not Visible. ");
                return flag;
            }
        }

        public bool IsWarningTypesNotVisible {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the bread crumb Warning types is not visible");
                Logger.Trace("Warning types is not Visible. ");
                return isNotVisible;
            }
        }

        public void AssertWarningTypesVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.WarningTypesPage.ScreenText.WarningTypes);
            Assert.IsTrue(IsWarningTypesVisible, ErrorStrings.WarningTypesNotVisible);
        }

        public void AssertWarningTypesNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.WarningTypesPage.ScreenText.WarningTypes);
            Assert.IsTrue(IsWarningTypesNotVisible?false:true, ErrorStrings.WarningTypesVisible);
        }

        public void AssertWarningTypesInvisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.WarningTypesPage.ScreenText.WarningTypes);
            Assert.IsFalse(IsWarningTypesInvisible, ErrorStrings.WarningTypesVisible);
        }

    }
}
