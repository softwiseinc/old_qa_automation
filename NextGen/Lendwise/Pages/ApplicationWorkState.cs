﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class ApplicationWorkState : BaseApplicationPage
    {

        public ApplicationWorkState(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationWorkState;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationWorkState;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationWorkState;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApplicationWorkStateVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ApplicationWorkStatePage.ScreenText.ApplicationWorkState))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Work States screen text is visible");
                Logger.Trace("Application work states is Visible. ");
                return isVisible;
            }
        }
        public bool IsApplicationWorkStateNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Work States screen text is not visible");
                Logger.Trace("Application work states is not Visible. ");
                return isNotVisible;
            }
        }

        public bool IsApplicationWorkStateInvisible
        {
            get
            {
                var applicationWorkStateText = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ApplicationWorkStatePage.ScreenText.ApplicationWorkState))
                    .Text;
                var flag = (applicationWorkStateText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Work States screen text is not visible");
                Logger.Trace("Application work states is not Visible. ");
                return flag;
            }
        }

        public void AssertApplicationWorkStateVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWorkStatePage.ScreenText.ApplicationWorkState);
            Assert.IsTrue(IsApplicationWorkStateVisible, ErrorStrings.ApplicationWorkStatesNotVisible);
        }
        public void AssertApplicationWorkStateNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWorkStatePage.ScreenText.ApplicationWorkState);
            Assert.IsTrue(IsApplicationWorkStateNotVisible?false:true, ErrorStrings.ApplicationWorkStatesVisible);
        }

        public void AssertApplicationWorkStateInvisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWorkStatePage.ScreenText.ApplicationWorkState);
            Assert.IsFalse(IsApplicationWorkStateInvisible, ErrorStrings.ApplicationWorkStatesVisible);
        }

    }
}
