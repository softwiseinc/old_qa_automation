﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class UserRolesManagement : BaseApplicationPage
    {
        public UserRolesManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UserRolesManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UserRolesManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UserRolesManagement;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsUserRolesManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.UserRolesManagement.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page visible");
                Logger.Trace($"User Management page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsUserRolesManagementVisible()
        {
            WaitForElement(10, Elements.AdminPage.UserRolesManagement.ScreenText);
            Assert.IsTrue(IsUserRolesManagementVisible, ErrorStrings.UserRolesManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is visible. ");
        }

        public void AssertIsUserRolesManagementNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.UserRolesManagement.ScreenText);
            Assert.IsTrue(IsUserRolesManagementNotVisible?false:true, ErrorStrings.UserRolesManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

        public bool IsUserRolesManagementInvisible
        {
            get
            {
 
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.UserRolesManagement.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page not visible");
                Logger.Trace($"User Management page loaded. ");
                return flag;
            }
        }

        public bool IsUserRolesManagementNotVisible {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text); ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the User Management page is not visible");
                Logger.Trace($"User Management page is not loaded. ");
                return isNotVisible;
            }
        }

        public void AssertIsUserRolesManagementInvisible()
        {
            WaitForElement(10, Elements.AdminPage.UserRolesManagement.ScreenText);
            Assert.IsFalse(IsUserRolesManagementInvisible, ErrorStrings.UserRolesManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }
    }
}
