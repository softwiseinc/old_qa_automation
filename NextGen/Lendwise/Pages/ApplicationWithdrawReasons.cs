﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class ApplicationWithdrawReasons : BaseApplicationPage
    {
        public ApplicationWithdrawReasons(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationWithdrawReasons;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationWithdrawReasons;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationWithdrawReasons;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApplicationWithdrawReasonsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ApplicationWithdrawReasonsPage.ScreenText.ApplicationWithdrawReason))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate Application Withdraw Reasons title text is visible");
                Logger.Trace("Warning types is Visible. ");
                return isVisible;
            }
        }
        public bool IsApplicationWithdrawReasonsNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate Application Withdraw Reasons title text is not visible");
                Logger.Trace("Warning types is not Visible. ");
                return isNotVisible;
            }
        }


        public bool IsApplicationWithdrawReasonsInvisible
        {
            get
            {
                var applicationWithdrawReasonText = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.ApplicationWithdrawReasonsPage.ScreenText.ApplicationWithdrawReason))
                    .Text;
                var flag = (applicationWithdrawReasonText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate Application Withdraw Reasons title text is not visible");
                Logger.Trace("Warning types is not Visible. ");
                return flag;
            }
        }

        public void AssertApplicationWithdrawReasonsVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWithdrawReasonsPage.ScreenText.ApplicationWithdrawReason);
            Assert.IsTrue(IsApplicationWithdrawReasonsVisible, ErrorStrings.ApplicationWithdrawReasonsNotVisible);
        }
        public void AssertApplicationWithdrawReasonsNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWithdrawReasonsPage.ScreenText.ApplicationWithdrawReason);
            Assert.IsTrue(IsApplicationWithdrawReasonsNotVisible ? false:true, ErrorStrings.ApplicationWithdrawReasonsVisible) ;
        }

        public void AssertApplicationWithdrawReasonsInVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.ApplicationWithdrawReasonsPage.ScreenText.ApplicationWithdrawReason);
            Assert.IsFalse(IsApplicationWithdrawReasonsInvisible, ErrorStrings.ApplicationWithdrawReasonsVisible);
        }

    }
}
