﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{
    internal class SystemMaintenancePage : BaseApplicationPage
    {
        public SystemMaintenancePage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.SystemMaintenance;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.SystemMaintenance;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.SystemMaintenance;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsSystemMaintenancePageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the System Maintenance screen text is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }


        public void AssertIsSystemMaintenancePageVisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.ScreenText);
            Assert.IsTrue(IsSystemMaintenancePageVisible, ErrorStrings.SystemMaintenanceNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the System Maintenance page loaded after login in as Administrative agent. ");
        }


        public void AssertIsSystemMaintenancePageInvisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.ScreenText);
            var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.ScreenText));
            var flag = false;
            if (screenText.Text==Dummy.Text)
            {
                flag = true;
            }

            Assert.IsFalse(flag, ErrorStrings.SystemMaintenanceVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate the System Maintenance screen text is Invisible");
            Logger.Trace($"System Maintenance screen text is Invisible. ");
        }

        public void ClickApplicationMaintenanceTile()
        {
            WaitForElement(5, Elements.AdminPage.SystemMaintenancePage.Buttons.ApplicationMaintenance);
            var applicationMaintenanceTile =
                Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.Buttons.ApplicationMaintenance));
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click on location maintenance tile");
        }

        public void ClickLocationMaintenanceTile()
        {
            WaitForElement(5, Elements.AdminPage.SystemMaintenancePage.Buttons.LocationMaintenance);
            var locationMaintenanceTile =
                Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.Buttons.LocationMaintenance));
            locationMaintenanceTile.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click location maintenance tile");
        }
    }
}
