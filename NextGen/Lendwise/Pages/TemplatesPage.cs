﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class TemplatesPage : BaseApplicationPage
    {
        public TemplatesPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Templates;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Templates;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Templates;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsTemplatesPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Templates.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public bool IsTemplatesPageInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Templates.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return flag;
            }
        }

        public void AssertIsTemplatesPageVisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.DocumentManagement.Templates.ScreenText);
            Assert.IsTrue(IsTemplatesPageVisible, ErrorStrings.TemplatesPageNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }

        public void AssertIsTemplatesPageInvisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.DocumentManagement.Templates.ScreenText);
            Assert.IsFalse(IsTemplatesPageInvisible, ErrorStrings.TemplatesPageInvisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }

    }
}
