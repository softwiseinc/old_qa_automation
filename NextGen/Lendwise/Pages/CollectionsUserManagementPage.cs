﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace Lendwise.Pages
{
    internal class CollectionsUserManagementPage : BaseApplicationPage
    {
        public CollectionsUserManagementPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionUserManagementPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsUserManagementPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsUserManagementPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsUserManagementPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertCollectionsUserManagementPageLoaded()
        {

            Assert.IsTrue(IsCollectionsUserManagementPageLoaded, ErrorStrings.CollectionsUserManagementPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the collections admin page did not load. ");
        }


    }
}
