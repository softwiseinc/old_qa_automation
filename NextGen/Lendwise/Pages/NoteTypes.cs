﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class NoteTypes : BaseApplicationPage
    {
        public NoteTypes(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.NoteTypes;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.NoteTypes;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.NoteTypes;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsNoteTypesVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.NoteTypesPage.ScreenText.NoteTypes))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Warning types title text is visible");
                Logger.Trace("Warning types is Visible. ");
                return isVisible;
            }
        }

        public bool IsNoteTypesNotVisibleCase
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Warning types title text is not visible");
                Logger.Trace("Warning types is Visible. ");
                return isNotVisible;
            }
        }

        public void AssertNoteTypesVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.NoteTypesPage.ScreenText.NoteTypes);
            Assert.IsTrue(IsNoteTypesVisible, ErrorStrings.NoteTypesNotVisible);
        }

        public void AssertNoteTypesNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.NoteTypesPage.ScreenText.NoteTypes);
            Assert.IsTrue(IsNoteTypesNotVisible?false:true, ErrorStrings.NoteTypesVisible);

        }


        public bool IsNoteTypesNotVisible
        {
            get
            {
                var getNoteText = Driver.FindElement(By.XPath(Elements.AdminPage.TypeManagement.NoteTypesPage.ScreenText.NoteTypes))
                    .Text;
                var flag = false;
                if (getNoteText==Dummy.Text)
                {
                    flag = true;
                }

                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Warning types title text is not visible");
                Logger.Trace("Warning types is not Visible. ");
                return flag;
            }
        }

        public void AssertNoteTypesNotVisible()
        {
            WaitForElement(10, Elements.AdminPage.TypeManagement.NoteTypesPage.ScreenText.NoteTypes);
            Assert.IsFalse(IsNoteTypesNotVisible, ErrorStrings.NoteTypesVisible);
        }
    }
}
