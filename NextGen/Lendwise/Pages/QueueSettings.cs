﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{
    internal class QueueSettings : BaseApplicationPage
    {
        public QueueSettings(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.QueueSettings;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.QueueSettings;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.QueueSettings;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsQueueSettingsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueueSettings))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Settings screen text is visible");
                Logger.Trace("Queue Settings is Visible. ");
                return isVisible;
            }
        }

        public bool IsQueueSettingsInvisible
        {
            get
            {
                var queueSettingsText = Driver.FindElement(By.XPath(Elements.AdminPage.QueueManagementPage.ScreenText.QueueSettings))
                    .Text;
                var flag = (queueSettingsText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Settings screen text is not visible");
                Logger.Trace("Queue Settings is Visible. ");
                return flag;
            }
        }

        public bool IsQueueSettingsNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Queue Settings screen text is not visible");
                Logger.Trace("Queue Settings is not Visible. ");
                return isNotVisible;
            }
        }

        public void AssertIsQueueSettingsVisible()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueueSettings);
            Assert.IsTrue(IsQueueSettingsVisible, ErrorStrings.QueueManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is visible. ");
        }

        public void AssertIsQueueSettingsNotVisibleCase()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueueSettings);
            Assert.IsTrue(IsQueueSettingsNotVisible?false:true, ErrorStrings.QueueManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

        public void AssertIsQueueSettingsInvisible()
        {
            WaitForElement(5, Elements.AdminPage.QueueManagementPage.ScreenText.QueueSettings);
            Assert.IsFalse(IsQueueSettingsInvisible, ErrorStrings.QueueManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the User management page screen text is not visible. ");
        }

    }
}
