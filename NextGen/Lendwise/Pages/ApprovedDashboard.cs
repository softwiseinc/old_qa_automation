﻿using System;
using System.Diagnostics;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class ApprovedDashboard : BaseApplicationPage
    {
        public ApprovedDashboard(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApprovedDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApprovedDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApprovedDashboard;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApprovedDashboardVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Approved Dashboard is visible");
                Logger.Trace($"ApprovedDashboard page loaded. ");
                return isVisible;
            }
        }

        public bool IsDuplicateDataWarningNotVisible {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.DuplicateDataWarning)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Duplicate data warning is visible");
                Logger.Trace($"Duplicate data warning modal appears. ");
                return isVisible;
            }
        }
        public void ValidateDuplicateSsn()
        {

            WaitForElement(10, Elements.ApprovedDashboard.Buttons.SrcAppId);
            var srcId = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.SrcAppId));
            srcId.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Application ID ");

            WaitForElement(10, Elements.ApprovedDashboard.Buttons.Ssn);
            IWebElement ssnSrc = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Ssn));
            Actions copyActions = new Actions(Driver);
            copyActions.KeyDown(ssnSrc, Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform();
            copyActions.KeyDown(Keys.Control).SendKeys("c").KeyUp(ssnSrc, Keys.Control).Perform();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "SSN is copied: "+ ssnSrc);
            
            //Back to previous page
            Driver.Navigate().Back();

            WaitForElement(10, Elements.ApprovedDashboard.Buttons.DestAppId);
            IWebElement destSrc = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.DestAppId));
            destSrc.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Destinated Application Id is clicked.");

            WaitForElement(10, Elements.ApprovedDashboard.Buttons.Ssn);
            IWebElement ssnDest = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Ssn));

            Actions pasteActions = new Actions(Driver);
            pasteActions.KeyDown(ssnDest, Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform();
            pasteActions.KeyDown(Keys.Control).SendKeys("v").KeyUp(ssnDest, Keys.Control).Perform();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Duplicate SSN is entered.");

            ssnDest.SendKeys(Keys.Tab);

            //Waiting for Duplicate pop
            WaitForElement(10, Elements.ApprovedDashboard.DuplicateSsnPopUp);
            if (IsElementPresent(By.XPath(Elements.ApprovedDashboard.DuplicateSsnPopUp)))
            {
                Assert.IsTrue(true, ErrorStrings.DuplicateSsnMsgDisplayed);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Duplicate SSN message displayed.");
            }
            else
            {
                Assert.IsTrue(false, ErrorStrings.DuplicateSsnMsgDisplayed);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Duplicate SSN message not  displayed.");
            }


        }

        public void AssertCheckLoanType()
        {
            WaitForElement(10, Elements.ApprovedDashboard.CustomerListRow);
            IWebElement ntTable = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.CustomerListTable));
            System.Collections.Generic.IReadOnlyCollection<IWebElement> tableRows = ntTable.FindElements(By.TagName("tr"));
            bool flag = false;
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Checking Loan type in each row.");
            if (tableRows.Count>=1)
            {
                foreach (var customer in tableRows)
                {
                    var tdCollections = customer.FindElements(By.XPath("td[5]"));
                    foreach (var tdCollection in tdCollections)
                    {
                        string column = tdCollection.Text;

                        if (column == "Payday" || column == "Personal")
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }

                    }
                }
                Assert.IsTrue(flag, ErrorStrings.LoanTypeManagementNotVisible);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Loan type  personal / payday found.");
            }
            else
            {
                Assert.IsTrue(true, ErrorStrings.LoanTypeManagementNotVisible);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "No table rows found.");
            }
           

           

        }

        public void AssertIsApprovedDashboardVisible()
        {
            WaitForElement(10, Elements.ApprovedDashboard.ScreenText);
            Assert.IsTrue(IsApprovedDashboardVisible ? true : false, ErrorStrings.ApprovedDashboardNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Approved Dashboard loaded after login in as Administrative agent. ");
        }
        public void AssertDuplicateDataWarningVisible()
        {
            WaitForElement(10, Elements.ApprovedDashboard.DuplicateDataWarning);
            Assert.IsTrue(IsDuplicateDataWarningNotVisible ? true : false, ErrorStrings.DuplicateDataWarningVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Duplicate Data Warning is visible. ");
        }
        
        public void ClickApplicationIda()
        {
            WaitForElement(10, Elements.ApprovedDashboard.ApplicationId.Ida);
            var applicationId = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ApplicationId.Ida));


            applicationId.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Application ID ");
        }

        public void ClickSecondApplicationIdb()
        {
            WaitForElement(10, Elements.ApprovedDashboard.ApplicationId.Ida2);
            var applicationId = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ApplicationId.Ida2));
            applicationId.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on other Application ID ");
        }
        public void CopyContactEmail()
        {
            WaitForElement(10, Elements.ApprovedDashboard.ContactEmail.CopyEmail);
            IWebElement copyEmail = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ContactEmail.CopyEmail));
            Actions actions = new Actions(Driver);
            actions.KeyDown(copyEmail, Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform();
            actions.KeyDown(Keys.Control).SendKeys("c").KeyUp(copyEmail, Keys.Control).Perform();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Email & copy ");
        }
        public void PasteContactEmail()
        {
            WaitForElement(10, Elements.ApprovedDashboard.ContactEmail.PasteEmail);
            IWebElement pasteEmail = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ContactEmail.PasteEmail));
            Actions actions = new Actions(Driver);
            actions.KeyDown(pasteEmail, Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform();
            actions.KeyDown(Keys.Control).SendKeys("v").KeyUp(pasteEmail, Keys.Control).Perform();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Email & paste ");
        }

        public void ClickContextmenu()
        {
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.ContextMenu);
            var contextMenu = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.ContextMenu));
            contextMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click context menu ");
        }
        public void ClickMiddleName()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.ApprovedDashboard.Input.MiddleName);
            var middleName = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Input.MiddleName));
            middleName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on middle name");
        }
        public void ClickOk()
        {
            ExplicitWait(5);
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.Okay);
            var clickOkay = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Okay));
            clickOkay.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");
        }

        public void ClickEmailApprovalPageLink()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.ApprovalPageLink);
            var emailLink = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.EmailLink));
            emailLink.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Email Approval Link");
        }

        public void ClickDone()
        {
            WaitForElement(5, Elements.ApprovedDashboard.Buttons.Done);
            var done = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Done));
            done.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click Done ");
        }
        //public void ClickDoneFailCase()
        //{
        //    ExplicitWait(2);
        //    var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.PrintApplicationPopup.Buttons.Done);
        //    Assert.IsTrue(elementState, ErrorStrings.SaveNoteNotVisibleClicked);
        //    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Done ");
        //}

        public void AssertClickDoneFail()
        {
            WaitForElement(5, Elements.ApprovedDashboard.Buttons.Done);
            var done = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Done));
            var flag = false;
            if (done.Text==Dummy.Text)
            {
                flag = true;
                done.Click();
            }
            Assert.IsFalse(flag, ErrorStrings.DoneButtonVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the 'Done' button is invisible and not clicked.");
            Logger.Trace($"Done button is invisible and not clicked ");

           
        }

        public void ClickPersonPopUp()
        {
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.PersonPopUp);
            var personPopUpButton = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.PersonPopUp));
            personPopUpButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Person PrintApplicationPopup button");
        }


        public void ClickLogout()
        {
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.Logout);
            ExplicitWait(2);
            var logoutButton = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Logout));
            logoutButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click logout button");
        }

        public void ClickYes()
        {
            WaitForElement(10, Elements.ApprovedDashboard.Buttons.Yes);
            var yesButton = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Yes));
            yesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to logout. ");
        }

        public void ClickItemsPerPageDropDown()
        {
            WaitForElement(5, Elements.ApprovedDashboard.ItemsPerPageDropDown.ItemsPerPage);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ItemsPerPageDropDown.ItemsPerPage));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void SelectMinNumber()
        {
            WaitForElement(5, Elements.ApprovedDashboard.ItemsPerPageDropDown.ItemsMin);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.ItemsPerPageDropDown.ItemsMin));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void ClickHamburgerMenu()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu);
            var hamburgerMenu = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu));
            hamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click menu button");
        }

        public void ClickFirstRowAppID()
        {
            WaitForElement(10, Elements.ApprovedDashboard.CustomerListRow);
            IWebElement ntTable = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.CustomerListTable));
            System.Collections.Generic.IReadOnlyCollection<IWebElement> tableRows = ntTable.FindElements(By.TagName("tr"));
            bool flag = false;
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Checking Loan type in each row.");
            string tempString = "null";
            if (tableRows.Count >= 1)
            {
                

                foreach (var row in tableRows)
                {
                    var id = row.FindElements(By.XPath("//tbody/tr[1]/td[4]/div/span/span"));
                    tempString = row.Text;
                    Debug.WriteLine(tempString);
                    
                }
                Assert.IsTrue(flag, ErrorStrings.LoanTypeManagementNotVisible);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Loan type  personal / payday found.");
            }
            else
            {
                Assert.IsTrue(true, ErrorStrings.LoanTypeManagementNotVisible);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "No table rows found.");
            }

        }

        public void SelectWithdrawApprovedLoanOption()
        {
           WaitForElement(5, Elements.ApprovedDashboard.Buttons.Withdraw);
           var withdrawOption = Driver.FindElement(By.XPath(Elements.ApprovedDashboard.Buttons.Withdraw));
           withdrawOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Withdraw option");
        }

        public void ClickOkOnConfirmAppWithdrawalPopup()
        {
           WaitForElement(5, Elements.ApprovedDashboard.ConfirmApplicationWithdrawalPopup.buttons.OK);
           var ok = Driver.FindElement(
               By.XPath(Elements.ApprovedDashboard.ConfirmApplicationWithdrawalPopup.buttons.OK));
           ok.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on ok to confirm loan approval withdrawal");
        }

        public void ClickCancelOnConfirmAppWithdrawalPopup()
        {
            WaitForElement(5, Elements.ApprovedDashboard.ConfirmApplicationWithdrawalPopup.buttons.Cancel);
            var cancel = Driver.FindElement(
                By.XPath(Elements.ApprovedDashboard.ConfirmApplicationWithdrawalPopup.buttons.Cancel));
            cancel.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on ok to confirm loan approval withdrawal");
        }
    }
    
    
}
