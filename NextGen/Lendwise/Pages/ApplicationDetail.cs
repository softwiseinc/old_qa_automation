﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages 
{
    class ApplicationDetail : BaseApplicationPage
    {
        public ApplicationDetail(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationDetail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationDetail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationDetail;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApplicationDetailVisible
        {
            get
            {
                //var isVisible = Driver.FindElement(By.XPath(Elements.ApplicationDetail.ScreenText.ApplicationDetailPage)).Displayed;
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Detail text visible");
                Logger.Trace($"Pending Detail page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsApplicationDetailVisible()
        {
            //WaitForElement(10, Elements.ApplicationDetail.ScreenText.ApplicationDetailPage);
            Assert.IsTrue(IsApplicationDetailVisible, ErrorStrings.ApplicationDetailNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Detail text is visible. ");
        }

        public void ClickEmailApplicationLink()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.EmailApplicationLink);
            var emailApplicationLinkButton =
                Driver.FindElement(By.XPath(Elements.ApplicationDetail.Buttons.EmailApplicationLink));
            emailApplicationLinkButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Email Application Link. ");
        }

        public void ClickDone()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ApplicationDetail.Popup.Done);
            var button = Driver.FindElement(By.XPath(Elements.ApplicationDetail.Popup.Done));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Done after pop displays email sent");
        }
        public void ClickDoneFailCase()
        {
            ExplicitWait(2);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.ApplicationDetail.Popup.Done);
            Assert.IsTrue(elementState, ErrorStrings.SaveNoteNotVisibleClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Done after pop displays email sent");
        }
        public void AssertClickDoneFail()
        {
            ExplicitWait(5);
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.ApplicationDetail.Popup.Done)))
            {
                flag = false;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Done' button is not clicked after email is sent.");
            Logger.Trace("'Done' button is not clicked after email is sent.");
            Assert.IsTrue(flag, ErrorStrings.DoneBtnIsNotClicked);

        }

        public void ClickTextApplicationLink()
        {
           WaitForElement(5, Elements.ApplicationDetail.Buttons.TextApplicationLink);
           var textButton = Driver.FindElement(By.XPath(Elements.ApplicationDetail.Buttons.TextApplicationLink));
           textButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Text Application Link");
        }

        public void ClickClosePopupButton()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplicationDetail.Popup.Close);
            var closeButton = Driver.FindElement(By.XPath(Elements.ApplicationDetail.Popup.Close));
            closeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Close on popup. ");
        }

        public void ClickClosePopupButtonFailCase()
        {
         
            ExplicitWait(2);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.ApplicationDetail.Popup.Close)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.OpoupCloseBtnNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Close on popup. ");

    }

        public void AssertClickClosePopupButtonFail()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplicationDetail.Popup.Close);
            var closeButton = Driver.FindElement(By.XPath(Elements.ApplicationDetail.Popup.Close));
            var flag = false;
            if (closeButton.Text==Dummy.Text)
            {
                flag = true;
                closeButton.Click();
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate close' button is not clicked.");
            Logger.Trace("'Close' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.OpoupCloseBtnClicked);
        }

        public void ClickClosePopupButtonFail()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplicationDetail.Popup.Close);
            var closeButton = Driver.Url.Contains(InvalidUrl.Url);
            //closeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Close on popup. ");
        }

        public void ClickRefreshApplication()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.RefreshApplication);
            var refreshApplication =
                Driver.FindElement(By.XPath(Elements.ApplicationDetail.Buttons.RefreshApplication));
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Refresh Application Button");
        }
        public void ClickRefreshApplicationFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.ApplicationDetail.Buttons.RefreshApplication)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.RefreshApplicationBtnNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Refresh Application Button");

        }


        public void AssertClickRefreshApplicationFail()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.RefreshApplication);
            var refreshApplication =
                Driver.FindElement(By.XPath(Elements.ApplicationDetail.Buttons.RefreshApplication));
            var flag= false;
            if (refreshApplication.Text==Dummy.Text)
            {
                flag = true;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Refresh Application' button is not clicked.");
            Logger.Trace("'Refresh Application' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.RefreshApplicationBtn);
        }

        public void ClickResetPassword()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.ResetPassword);
            var resetPassword = Driver.FindElement(By.XPath(Elements.ApplicationDetail.Buttons.ResetPassword));
            resetPassword.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Reset ManagerPassword button");
        }

        public void ClickResetPasswordFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.ApplicationDetail.Buttons.ResetPasswordFail)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.ResetBtnNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Reset ManagerPassword button");

        }

        public void AssertClickResetPasswordFail()
        {
            ExplicitWait(5);
            var flag = false;
            if (IsElementDisplayed(By.XPath(Elements.ApplicationDetail.Buttons.ResetPassword)))
            {
                flag = true;
            }
           

            Assert.IsFalse(flag, ErrorStrings.ResetBtnClicked);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the  Reset ManagerPassword button is clicked.");
            Logger.Trace($"Clicked on Reset ManagerPassword button. ");
        }

        public void ClickResetPasswordFail()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.ResetPassword);
            var resetPassword = Driver.Url.Contains(InvalidUrl.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Reset ManagerPassword button");
        }

        public void ClickRefreshApplicationFail()
        {
            WaitForElement(5, Elements.ApplicationDetail.Buttons.RefreshApplication);
            var refreshApplication =
                Driver.Url.Contains(InvalidUrl.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Refresh Application Button");
        }
    }
}
