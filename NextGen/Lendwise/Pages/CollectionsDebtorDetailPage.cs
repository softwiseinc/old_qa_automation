﻿using System;
using System.Collections.Generic;
using AutomationResources;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;


namespace Lendwise.Pages
{
    internal class CollectionsDebtorDetailPage : BaseApplicationPage
    {

        public CollectionsDebtorDetailPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsDebtorDetailPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsDebtorDetailPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsDebtorDetailPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsDebtorDetailPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertCollectionsDebtorDetailPageLoaded()
        {

            //if (DebtorValidationsIsVisible())
            //{
            //    ExplicitWait(3);
            //    Reporter.LogTestStepForBugLogger(Status.Info, "Debtor validation Banner is visible click Okay button");
            //    var okay = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorValidationPopup.Buttons.Ok));
            //    okay.Click();
            //    Reporter.LogTestStepForBugLogger(Status.Info, "Okay button clicked");
            //}
            ExplicitWait(3);
            var debtorValidationBanner = false;
            if (IsElementVisible(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorValidationPopup
                .DebtorValidationBanner)))
            {
                debtorValidationBanner = true;
                var okay = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorValidationPopup.Buttons.Ok));
                
                okay.Click();
            }
            ExplicitWait(3);
            Assert.IsTrue(debtorValidationBanner, ErrorStrings.PrintApplicationNotVisible);
            Assert.IsTrue(IsCollectionsDebtorDetailPageLoaded, ErrorStrings.CollectionsDashboardLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage loaded after about tile was clicked on. ");


            var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
            var collectionsDebtorDetailPage = new CollectionsDebtorDetailPage(Driver, adminEmployee);
            
            while (IsDupDataOkVisibleButton())
            {
                collectionsDebtorDetailPage.DuplicateDataHelper();

            }
        }

        
        public void DuplicateDataHelper()
        {
            ExplicitWait(2);
            if (IsDupDataOkVisibleButton())
            {
                ExplicitWait(3);
                Reporter.LogTestStepForBugLogger(Status.Info, "Warning Okay button is visible");
                var okay = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DuplicateWarningPopup
                    .Buttons.Okay));
                okay.Click();
                Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp okay");
            }

        }

        private bool IsDupDataOkVisibleButton()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
           
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CollectionsDebtorDetailPage.DuplicateWarningPopup
                .Buttons.Okay)));
            if (buttonList.Count > 0)
                return true;
            return false;

        }





    //public bool DebtorValidationsIsVisible()
    //{
    //    WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorValidationPopup.DebtorValidationBanner);

    //}


    public void ClickPaymentButton()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.Buttons.Payment);
            var paymentButton =
                Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.Buttons.Payment));
            paymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Payment button");
            
        }

        public void ClickPaymentMethod()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.DropDown.PaymentMethod);
            var paymentMethod = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
                .DebtorPaymentPopup.DropDown.PaymentMethod));
            paymentMethod.Click();
          Reporter.LogPassingTestStepToBugLogger(Status.Info, "click payment method type");

        }

        public void SelectAchAsPaymentMethod()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.DropDown.AchPaymentMethod);
            var achMethod =
                Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.DropDown.AchPaymentMethod));
            achMethod.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Ach payment method");
        }

        public void ClickAmountInputField()
        {
           WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.InputFields.PaymentAmountInputField);
           var paymentInputField = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
               .DebtorPaymentPopup.InputFields.PaymentAmountInputField));
           paymentInputField.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click payment input field");
        }

        public void EnterMinimumPaymentAmount()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.InputFields.PaymentAmountInputField);
            var paymentInputField = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
                .DebtorPaymentPopup.InputFields.PaymentAmountInputField));
            paymentInputField.SendKeys(PrincipalPayment.Minimum);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter minimum amount");
        }

        internal void ClickPersonalLoanCheckBox()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.CheckBoxes.PersonalLoanCheckBox);
            var pCheckBox = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
                .DebtorPaymentPopup.CheckBoxes.PersonalLoanCheckBox));
            pCheckBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Personal loan Check Box");
        }

        public void ClickSaveButton()
        {
           WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.Buttons.Save);
           var saveButton = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
               .DebtorPaymentPopup.Buttons.Save));
           saveButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save Button");
        }

        public void ClickOkOnPaymentSuccessfulPopup()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.Buttons.Ok);
            var okButton =
                Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup
                    .Buttons.Ok));
            okButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click ok on payment successful popup");
        }

        public void ClickNotesSection()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.Tabs.Notes);
            var notesSection = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.Tabs.Notes));
            notesSection.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click notes sections");
        }
        public bool IsPaymentNoteIsVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.Notes.NotesField.TopLoanInList))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the payment date is visible");
                Logger.Trace($"Payment date is visible=>{isVisible}");
                return isVisible;
            }
        }
        internal void AssertPaymentNoteIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsPaymentNoteIsVisible, ErrorStrings.DebtPaymentNoteIsNotVisible);
        }

        public void SelectAchArrangedPaymentAsCategory()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.DropDown.AchArrangedPayment);
            var achArrangedPayment = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
                .DebtorPaymentPopup.DropDown.AchArrangedPayment));
            achArrangedPayment.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select ACH Arranged Payment");
        }

        public void ClickCategoryInputField()
        {
            WaitForElement(5, Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.InputFields.Category);
            var category = Driver.FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans
                .DebtorPaymentPopup.InputFields.Category));
            category.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Category field");
        }
        public bool DebtorPaymentPopUpIsVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.CollectionsDebtorDetailPage.DefaultedLoans.DebtorPaymentPopup.ScreenText.DebtorPaymentBanner))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the payment date is visible");
                Logger.Trace($"Payment date is visible=>{isVisible}");
                return isVisible;
            }
        }
        public void AssertDebtorPaymentPopUpIsVisible()
        {
            Assert.IsTrue(DebtorPaymentPopUpIsVisible, "Debt Payment pop is not visible");
        }
    }
}
