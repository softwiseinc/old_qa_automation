﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using AutomationResources;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Configuration;
using Newtonsoft.Json;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
using OpenQA.Selenium.Support.UI;
using Newtonsoft.Json;

namespace Lendwise.Pages
{
    internal class CustomerDashboard : BaseApplicationPage
    {
        public CustomerDashboard(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CustomerDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CustomerDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CustomerDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCustomerDashboardLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsCustomerDashboardNotLoadedCase
        {
            get
            {
                var isNotLoaded = Driver.Url.Contains(InvalidUrl.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page is not loaded successfully");
                Logger.Trace($"Home page is loaded=>{isNotLoaded}");
                return isNotLoaded;
            }
        }

        public bool IsCustomerDashboardNotLoaded
        {
            get
            {
                var flag = (TestEnvironment.Url == Dummy.Url) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{flag}");
                return flag;
            }
        }

        public void AssertCustomerDashboardLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsCustomerDashboardLoaded, ErrorStrings.CustomerDashboardNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage loaded after about tile was clicked on. ");
        }
        public void AssertCustomerDashboardNotLoadedFailCase()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsCustomerDashboardNotLoadedCase?false:true, ErrorStrings.CustomerDashboardLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage loaded after about tile was clicked on. ");
        }

        public void AssertCustomerDashboardIsNotLoaded()
        {
            ExplicitWait(3);
            Assert.IsFalse(IsCustomerDashboardNotLoaded, ErrorStrings.CustomerDashboardNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Customer Dashboard page is not loaded.");
        }
        public bool IsSsnVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDashboard.ScreenText.SocialSecurityNumber)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        public void AssertIsSsnVisible()
        {

            WaitForElement(5, Elements.CustomerDashboard.ScreenText.SocialSecurityNumber);
            Assert.IsTrue(IsSsnVisible, ErrorStrings.SsnIsNotVisible);
        }


        public void ClickThreeDotButton()
        {
          
           WaitForElement(10, Elements.CustomerDashboard.Buttons.ThreeDot);
           var threeDotExpandableMenuButton = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.ThreeDot));
           threeDotExpandableMenuButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click thee dot expandable menu");
        }

        public void ClickEmailApprovalLink()
        {
            ExplicitWait(3);
           WaitForElement(10, Elements.CustomerDashboard.Buttons.EmailLink);
           var emailLink = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.EmailLink));
           emailLink.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Email Approval Link");
        }

        public void ClickDone()
        {
           
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.Done);
            var done = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.Done));
            done.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click Done after email has sent");
        }


        public void AssertClickDoneFail()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.Done);
            var done = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.Done));
            var flag = false;
            if (done.Text == Dummy.Text)
            {
                flag = true;
                done.Click();
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Done' button is not clicked after email is sent.");
            Logger.Trace("'Done' button is not clicked after email is sent.");
            Assert.IsFalse(flag, ErrorStrings.DoneBtnIsNotClicked);

        }

        public string CustomerDashboardEmail(string email)
        {
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
            return email;
        }

        public string EnterSearchByEmailApprovedCustomer(string email)
        {
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
            return email;
        }

        public CheckCityOnline.Users.CurrentUser EnterSearchByEmail()
        {
            CheckCityOnline.Users.CurrentUser searchCustomer =
                (CheckCityOnline.Users.CurrentUser)GetCustomer("Approved");
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(searchCustomer.Email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
            return searchCustomer;
        }
        public CheckCityOnline.Users.CurrentUser EnterSearchByEmail(LoanType loanType)
        {
            CheckCityOnline.Users.CurrentUser searchCustomer =
                (CheckCityOnline.Users.CurrentUser)GetCustomerByLoanType(loanType);
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(searchCustomer.Email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
            return searchCustomer;
        }

        
        public CheckCityOnline.Users.CurrentUser EnterSearchByEmail(CheckCityOnline.Users.CurrentUser searchCustomer)
        {
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(searchCustomer.Email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
            return searchCustomer;
        }

        public static CheckCityOnline.Users.CurrentUser GetCustomerWithAgedLoan(LoanType loanType)
        {
            List<object> customerList = CustomerManager.UT_ApprovedPaydayWithBalance;
            

            var customerCount = customerList?.Count - 1;
            Random random = new Random();
            int rInt = random.Next(0, (int)customerCount);
            // ReSharper disable once PossibleInvalidOperationException
            var customerIndex = random.Next(0, (int)(customerCount - 1));
            var customer = (CheckCityOnline.Users.CurrentUser)customerList[customerIndex];
            return customer;
        }
        private static CheckCityOnline.Users.CurrentUser GetCustomerByLoanType(LoanType loanType)
        {
            List<object> customerList = null;
            switch (loanType)
            {
                case LoanType.Payday:
                    customerList = CustomerManager.UT_ApprovedPaydayWithBalance;
                    break;
                case LoanType.Personal:
                    customerList = CustomerManager.UT_ApprovedPersonalWithBalance;
                    break;
            }
            var customerCount = customerList?.Count - 1;
            Random random = new Random();
            int rInt = random.Next(0, (int)customerCount);
            // ReSharper disable once PossibleInvalidOperationException
            var customerIndex = random.Next(0, (int)(customerCount - 1));
            var customer = (CheckCityOnline.Users.CurrentUser)JsonConvert.DeserializeObject<CheckCityOnline.Users.CurrentUser>(
                customerList[customerIndex].ToString());
            return customer;
        }

        private static object GetCustomer(string customerType)
        {
            List<object> customerList = null;
            switch (customerType)
            {

                case "Approved":
                    customerList = CustomerManager.UT_ApprovedPaydayWithBalance;
                    break;
                case "Pending All":
                    customerList = CustomerManager.UT_PendingAll;
                    break;
                case "Turndown":
                    customerList = CustomerManager.UT_Turndown;
                    break;
                case "Turndown Lead":
                    customerList = CustomerManager.UT_TurndownLead;
                    break;
            }
            var customerCount = customerList?.Count - 1;
            Random random = new Random();
            // ReSharper disable once PossibleInvalidOperationException
            var customerIndex = random.Next(0, (int)(customerCount));
            CheckCityOnline.Users.CurrentUser customer =
                (CheckCityOnline.Users.CurrentUser)JsonConvert.DeserializeObject<CheckCityOnline.Users.CurrentUser>(
                    customerList[customerIndex].ToString());
            return customer;




        }

        public void ClickGoToButton()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.GoToButton);
            var button = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.GoToButton));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click go to button ");
            //CheckForDuplicatePhoneData();
        }

        //private void CheckForDuplicatePhoneData()
        //{
        //    ExplicitWait(5);
        //    if (Driver.FindElement(By.XPath("//*[text()='Duplicate Data Found - Phone Number']")).Displayed)
        //        Driver.FindElement(By.XPath("//*[text()='OK']")).Click();
        //}

        public void ClickAddId()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Addbtn);
            var addIdButton = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Addbtn));
            addIdButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click add button");
        }


        public void CmbAddId_CustomerDetail(string idNumber, string idLabel)
        {
            WaitForElement(5, Elements.CustomerDashboard.CmbAddId);
            ClickIfPresent(Elements.CustomerDashboard.Buttons.Close, "Attention popup is displayed", "Pop up is closed.");
            var addIdSaveBtn = Driver.FindElement((By.XPath(Elements.CustomerDashboard.Buttons.Save)));
            var addIdValue = Driver.FindElement((By.XPath(Elements.CustomerDashboard.CmbAddId)));
            addIdValue.SendKeys(idLabel);
            addIdValue.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, idLabel+ " is selected");
            var addIdIdNumber = Driver.FindElement((By.XPath(Elements.CustomerDashboard.AddIdIdNumber)));
            addIdIdNumber.SendKeys(idNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, " ID Number for "+ idLabel + " is added");


            if (idLabel == "drivers license" || idLabel == "StateColumn Identification")
            {
                var stateIssueBy = Driver.FindElement((By.XPath(Elements.CustomerDashboard.StateIssueBy)));
                stateIssueBy.SendKeys("nepal");

                var expiration = Driver.FindElement((By.XPath(Elements.CustomerDashboard.Expiration)));
                expiration.SendKeys("1/10/2021");
            }
            

            addIdSaveBtn.Click();
            addIdValue.Clear(); 
            
            
        }

        public void ClickLastNameOfCustomer()
        {
           WaitForElement(5, Elements.CustomerDashboard.NameLink.Lastname);
           var lastName = Driver.FindElement(By.XPath(Elements.CustomerDashboard.NameLink.Lastname));
           lastName.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click last name of the customer that was found");
        }

        public void ClickItemsPageDropDown()
        {
            WaitForElement(5, Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsPerPage);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsPerPage));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void SelectMinNumber()
        {
            WaitForElement(5, Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsMin);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsMin));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void ClickSsn()
        {
            WaitForElement(5, Elements.CustomerDashboard.UnmaskSsn);
            var unmaskSsn = Driver.FindElement(By.XPath(Elements.CustomerDashboard.UnmaskSsn));
            unmaskSsn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click SSN an override will display ");
        }

        public void EnterManagerUserName()
        {

            WaitForElement(5, Elements.CustomerDashboard.UnmaskSsnPopup.InputFields.ManagerUsername);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDashboard.UnmaskSsnPopup.InputFields.ManagerUsername));
            usernameFieldInput.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager username");
        }

        public void EnterMangerPassword()
        {
            WaitForElement(5, Elements.CustomerDashboard.UnmaskSsnPopup.InputFields.MangerPassword);
            var passwordFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDashboard.UnmaskSsnPopup.InputFields.MangerPassword));
            passwordFieldInput.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter password");
        }

        public void ClickOkayOnUnmaskSsnPopup()
        {
            WaitForElement(5, Elements.CustomerDashboard.UnmaskSsnPopup.Buttons.Ok);
            var okbutton = Driver.FindElement(By.XPath(Elements.PendingDetailPage.UnmaskSsnPopup.Buttons.Ok));
            okbutton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");

        }

        public void ClickOnCheckingAccount()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Bank.CheckingAccount);
            var checkingAccount = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Bank.CheckingAccount));
            checkingAccount.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void GetAllWarningTypes(List<string> types)
        {
            Driver.FindElement(By.XPath("//*[@id='mat-dialog-0']/cleo-warning-edit/div/div/div/form/div[1]/sw-select/sw-input-wrapper/div/div[1]/div/ng-select/div/span")).Click();
            ReadOnlyCollection<IWebElement> warningTypes = Driver.FindElements(By.XPath("//*[contains(@class,'ng-dropdown-panel')]//*[contains(@class,'ng-option')]//*[contains(@class,'ng-star-inserted')]"));
            foreach (IWebElement warning in warningTypes)
            {
                types.Add(warning.Text);
            }
        }

        public void SelectMidNumber()
        {
            WaitForElement(5, Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsMid);
            var itemsMidOption = Driver.FindElement(By.XPath(Elements.CustomerDashboard.ItemsPerPageDropDown.ItemsMid));
            itemsMidOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select 50 for amount of customer to be displayed ");
        }

       public void EnterSearchByName(string customerName)
        {
            CheckCityOnline.Users.CurrentUser searchCustomer =
                (CheckCityOnline.Users.CurrentUser)GetCustomer("Approved");
            WaitForElement(5, Elements.CustomerDashboard.SearchField);
            var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
            searchField.SendKeys(customerName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
        }

       public void FindCustomerByName(string customerName)
       {
           ExplicitWait(10);
           //WaitForElement(5, Elements.CustomerDashboard.SearchField);
           var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
           searchField.SendKeys(customerName);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's email you are searching for ");
       }

        public void ClickTopLoanInList()
       {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.TopLoanInList);
           var itemsMidOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.TopLoanInList));
           itemsMidOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the Top loan in the active loan list ");
       }

       public void ClickLoanRescind()
       {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.Rescind);
           var itemsMidOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.Rescind));
           itemsMidOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the the Rescind Button ");
        }

       public void ClickRescindSave()
       {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.RescindSave);
           var itemsMidOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.RescindSave));
           itemsMidOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click 'Save' button on the 'Rescind Loan' pop-up ");
       }

       public void ClickRescindOk()
       {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.RescindOK);
           var itemsMidOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.RescindOK));
           itemsMidOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click 'OK' on 'Loan Rescind Loan' pop-up ");
        }

       public void RescindOverrideHandler(Users.CurrentUser ownerEmployee, TestConfiguration configuration)
       {
           ExplicitWait(4);
           if (Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Popup.DeferedValidation.CredentialsPrompt))
           {
                if (configuration.SystemUnderTest.ToString() == SystemUnderTest.QA.ToString())
                {
                    Driver.FindElement(By.XPath("//*[@id='mat-input-9']")).Click();
                    Driver.FindElement(By.XPath("//*[@id='mat-input-9']")).SendKeys("qa00@nostromorp.com");
                    Driver.FindElement(By.XPath("//*[@id='mat-input-10']")).SendKeys("alien");
                }

                if (configuration.SystemUnderTest == SystemUnderTest.STAGE)
                {
                    Driver.FindElement(By.XPath("//*[@id='mat-input-9']")).Click();
                    Driver.FindElement(By.XPath("//*[@id='mat-input-9']")).SendKeys("qa000@nostromorp.com");
                    Driver.FindElement(By.XPath("//*[@id='mat-input-10']")).SendKeys("alien");
                }


           }
           ExplicitWait(3);
           if (Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.Buttons.Override))
           {
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.Override)).Click();
           }
           ExplicitWait(3);
        }

       public void TryClickTopLoanInList()
       {
           try
           {
               ClickTopLoanInList();
           }
           catch (Exception)
           {
               Reporter.LogTestStepForBugLogger(Status.Info, "no more loans");
           }
        }

       public void SearchByCustomerId(string customerId)
       {
          WaitForElement(5, Elements.CustomerDashboard.SearchField);
          var searchField = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
          searchField.SendKeys(customerId);

       }

       public void ClickSearchByInputField()
       {
           WaitForElement(5, Elements.CustomerDashboard.SearchField);
           var searchFieldInput = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
           searchFieldInput.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the search field");
        }

      
       public void EnterDuplicateEmailAddress(string email)
       {
           WaitForElement(5, Elements.CustomerDashboard.SearchField);
           var emailAddress = Driver.FindElement(By.XPath(Elements.CustomerDashboard.SearchField));
           emailAddress.SendKeys(email);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters the duplicate email address.");
        }

       public bool GetEmailCountOnPage(string emailAddress)
       {
           bool isVisible = false;
           List<IWebElement> elementList = new List<IWebElement>();
           elementList.AddRange(Driver.FindElements(By.XPath("//*[contains(@class,'cdk')]/td[position()=12]")));
           if (elementList.Count > 1)
           {
               isVisible = true;
           }
           return isVisible;
       }

       public void AssertDuplicateEmailsAreVisible(string email)
       {
           Assert.IsTrue(GetEmailCountOnPage(email), ErrorStrings.GetEmailCountOnPageNotMoreThanOne);
       }
    }
}
