﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class PendingDashboard : BaseApplicationPage
     {
        public PendingDashboard(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PendingDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PendingDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PendingDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsPendingDashboardLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard loaded");
                Logger.Trace($"Pending Dashboard page loaded. ");
                return isVisible;
            }
        }


    
        public bool PendingDashboardNotLoaded
        {
            get
            {
                var flag = false;
                if (TestEnvironment.Url == Dummy.Url)
                {
                    flag = true;
                }

                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard is not loaded");
                Logger.Trace($"Pending Dashboard page is not loaded. ");
                return flag;
            }
        }

        public void AssertIsPendingDashboardLoaded()
        {
            ExplicitWait(2);
            Assert.IsTrue(IsPendingDashboardLoaded, ErrorStrings.PendingDashboardNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Dashboard loaded. ");
        }


        public bool IsSsnVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.SocialSecurity.SocialSecNumber)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        // Last edit by Pat Holman 5/14/20
        public void AssertIsSsnVisible()
        {
            WaitForElement(5, Elements.PendingDashboardPage.CustomerRows.Ssn);
            var customerRowsSsn = Driver.FindElements(By.XPath(Elements.PendingDashboardPage.CustomerRows.Ssn));
            string ssnValue = customerRowsSsn[1].Text;
            bool maskString = ssnValue.Contains("xxx-xx");
            Assert.IsFalse(maskString, ErrorStrings.SsnIsNotVisible);
        }

        public void NavigateToApplicationDashboard(Users.CurrentUser user)
        {
            var loginPage = new LoginPage(Driver, user);
            var pendingDashboardPage = new PendingDashboard(Driver, user);
            loginPage.LogInUser(user);
            loginPage.SelectLocation();
            loginPage.ClickAndSelectClose();
            AssertIsPendingDashboardLoaded();
            pendingDashboardPage.ClickHamburgerMenu();
            pendingDashboardPage.SelectApplicationDashboard();
         }

        public void ClickHamburgerMenu()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu);
            var hamburgerMenu = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu));
            hamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click menu button");
        }

        public void SelectApplicationDashboard()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.ApplicationDashboard);
            var applicationDashboardButton = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.ApplicationDashboard));
            applicationDashboardButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Application Dashboard");
        }

        public void NavigateToCustomerDashboard(Users.CurrentUser user)
        {
           // var loginPage = new LoginPage(Driver, user);
            var pendingDashboardPage = new PendingDashboard(Driver, user);
           /*loginPage.LogInUser(user);
            loginPage.SelectLocation();
            loginPage.ClickAndSelectClose();*/
            AssertIsPendingDashboardLoaded();
            pendingDashboardPage.ClickHamburgerMenu();
            pendingDashboardPage.SelectCustomerDashboard();

        }

        public void SelectCustomerDashboard()
        {
            WaitForElement(10, Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard);
            var applicationCustomerDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard));
            applicationCustomerDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Customer Dashboard");
        }

       

        public void CloseDeferredTurndownIfExist()
        {
            ClickIfPresent(Elements.PendingDashboardPage.Buttons.Close, "Deferred Turndown is Visible", "Deferred Turndown Closed");
        }

        public void ClickWorkNext()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.PendingDashboardPage.Buttons.WorkNext);
            var workNextButton = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.WorkNext));
            workNextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click work next button");
            // Ravi - changed to 7 seconds
        }

        public void ClickAgentToolsIcon()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PendingDashboardPage.Buttons.AgentToolsIcon);
            var agentToolsIcon = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.AgentToolsIcon));
            agentToolsIcon.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Agent Tools Icon");
        }

        public void ClickChangePassword()
        {
            ExplicitWait(3);
            //WaitForElement(5, Elements.PendingDashboardPage.WarningButtons.ChangePassword);
            var selectChangePassword =
                Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.ChangePassword));
            selectChangePassword.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click change password button");
        }

        public void EnterCurrentPassword()
        {
            ExplicitWait(4);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.CurrentPassword);
            var password = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.CurrentPassword));
            password.SendKeys(Users.User.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter current password");
        }

        public void EnterNewPassword()
        { 
            ExplicitWait(3);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.NewPassword);
            var newPassword = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.NewPassword));
            newPassword.SendKeys(Users.User.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new password. ");
        }

        public void EnterConfirmPassword()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.ConfirmPassword);
            var confirmPassword = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.ConfirmPassword));
            confirmPassword.SendKeys(Users.User.ConfirmPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new password for a second time. ");

        }

        public void ClickChangeButton()
        {
            
            WaitForElement(10, Elements.ChangeYourPasswordPopUp.ChangeButton);
            var change = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.ChangeButton));
          
            ExplicitWait(3);
            change.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click Change button ");
        }

        private void EnterPreviousPassword(Users.CurrentUser user)
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.CurrentPassword);
            var prevPassword = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.CurrentPassword));
            prevPassword.SendKeys(Users.User.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the current password");

        }
        private void EnterOriginalPassword(Users.CurrentUser user)
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.NewPassword);
            var password = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.NewPassword));
            password.SendKeys(Users.User.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter original password");
        }

        private void EnterOriginalConfirmedPassword(Users.CurrentUser user)
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ChangeYourPasswordPopUp.ConfirmPassword);
            var ogConfirmedPassword = Driver.FindElement(By.XPath(Elements.ChangeYourPasswordPopUp.ConfirmPassword));
            ogConfirmedPassword.SendKeys(Users.User.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter original password to confirm your password ");
        }
        public void ClickLogout()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.PendingDashboardPage.Buttons.Logout);
            var logout = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.Logout));
            logout.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Logout ");
        }

        public void ClickYesToLogout()
        {
           
           WaitForElement(5, Elements.PendingDashboardPage.Buttons.YesToLogout);
           var yesToLogout = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.YesToLogout));
           yesToLogout.Click();
        }

        public void ResetToPreviousPassword(Users.CurrentUser user)
        {
            ClickAgentToolsIcon();
            ClickChangePassword();
            EnterPreviousPassword(user);
            EnterOriginalPassword(user);
            EnterOriginalConfirmedPassword(user);
            ClickChangeButton();
        }

        public void ClickOnPendingDashboard()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.PendingDashboard);
            var pendingnDashboardButton = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.PendingDashboard));
            pendingnDashboardButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pending Dashboard");
        }

        public void NavigateToPendingDashboard()
        {
            ClickHamburgerMenu();
            ClickOnPendingDashboard();
        }

        public void SelectApprovedDashboard()
        {
            WaitForElement(10, Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard);
            var approvedDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select approved Dashboard");
        }

        // Last edit by Pat Holman 5/8/2020
        public string ClickCustomerIdClipBoard()
        {
            WaitForElement(5,Elements.PendingDashboardTable.IdClipboard);
            ReadOnlyCollection<IWebElement> customerIdsClipBoard;
            try
            {
                customerIdsClipBoard = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.IdClipboard));
                customerIdsClipBoard[0].Click();
            }
            catch (Exception)
            {
                customerIdsClipBoard = Driver.FindElements(By.XPath(Elements.PendingDashboardTable.IdClipboard));
                customerIdsClipBoard[0].Click();
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on clipboard " );
            return customerIdsClipBoard[0].Text;
        }

        // Last edit by Pat Holman 5/8/2020
        public void PasteCustomerId(string customerId)
        {
            string validateId;
            WaitForElement(5, Elements.PendingDashboardPage.SearchField.Search);
            var searchInput = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.SearchField.Search));
            searchInput.SendKeys(Keys.Control + "v");
            WaitForElement(5, Elements.PendingDashboardTable.IdClipboard);
            ReadOnlyCollection<IWebElement> customerIdsClipBoard;

            try
            {
                customerIdsClipBoard = Driver.FindElements(By.XPath($"//*[contains(@class,'ng-star-inserted')]//*[contains(text(),{customerId})]"));
                validateId = customerIdsClipBoard[0].Text;
            }
            catch (Exception)
            {
                customerIdsClipBoard = Driver.FindElements(By.XPath($"//*[contains(@class,'ng-star-inserted')]//*[contains(text(),{customerId})]"));
                validateId = customerIdsClipBoard[0].Text;
            }
            Assert.IsTrue(validateId.Contains(customerId), "Customer ID is NOT visible");
            Reporter.LogPassingTestStepToBugLogger(Status.Pass, $"{customerId} is on the first row ");
        }

        public void ClickItemsPerPageDropDown()
        {
            WaitForElement(5, Elements.PendingDashboardPage.DropDown.ItemsPerPage);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.DropDown.ItemsPerPage));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click items per page option");
        }

        public void SelectMinNumber()
        {
            WaitForElement(5, Elements.PendingDashboardPage.DropDown.ItemsMin);
            var itemsMin = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.DropDown.ItemsMin));
            itemsMin.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select 25 ");
        }

        public void ClickLoanTypeDropDown()
        {
           WaitForElement(5, Elements.PendingDashboardPage.LoanTypeDropDown.LoanType);
           var loanTypeDropDown = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.LoanTypeDropDown.LoanType));
           loanTypeDropDown.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Loan Type");
        }

        public void SelectPaydayAsLoanType()
        {
           WaitForElement(5, Elements.PendingDashboardPage.LoanTypeDropDown.DeferredLoanType);
           var paydayTypeOfLoan =
               Driver.FindElement(By.XPath(Elements.PendingDashboardPage.LoanTypeDropDown.DeferredLoanType));
           paydayTypeOfLoan.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select payday loan");
        }

        public void SelectPersonalLoanAsLoanType()
        {
            WaitForElement(5, Elements.PendingDashboardPage.LoanTypeDropDown.InstallLoanType);
            var paydayTypeOfLoan =
                Driver.FindElement(By.XPath(Elements.PendingDashboardPage.LoanTypeDropDown.InstallLoanType));
            paydayTypeOfLoan.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select personal loan");
        }

        public void ClickState()
        {
            WaitForElement(5, Elements.PendingDashboardPage.State.StateDropdownField);
            var stateDp = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.State.StateDropdownField));
            //stateDp.Click();
            stateDp.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click state drop down");
        }

        public void SelectStateUT()
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.PendingDashboardPage.State.SelectUT);
            var selectState = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.State.SelectUT));
            selectState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select state");
        }

        // Last edit by Pat Holman 5/14/20
        public void ClickSsn()
        {
            WaitForElement(5, Elements.PendingDashboardPage.CustomerRows.Ssn);
            ReadOnlyCollection<IWebElement> customerRowsSsn;
            try
            {
                customerRowsSsn = Driver.FindElements(By.XPath(Elements.PendingDashboardPage.CustomerRows.Ssn));
                customerRowsSsn[1].Click();
            }
            catch (Exception)
            {
                customerRowsSsn = Driver.FindElements(By.XPath(Elements.PendingDashboardPage.CustomerRows.Ssn));
                customerRowsSsn[1].Click();
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click SSN an override will display ");
        }

        public void EnterManagerUserName()
        {

            WaitForElement(5, Elements.PendingDashboardPage.UnmaskSsnPopup.InputFields.ManagerUsername);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.PendingDashboardPage.UnmaskSsnPopup.InputFields.ManagerUsername));
            usernameFieldInput.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager username");
        }

        public void EnterMangerPassword()
        {
            WaitForElement(5, Elements.PendingDashboardPage.UnmaskSsnPopup.InputFields.ManagerPassword);
            var passwordFieldInput =
                Driver.FindElement(By.XPath(Elements.PendingDashboardPage.UnmaskSsnPopup.InputFields.ManagerPassword));
            passwordFieldInput.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter password");
        }

        public void ClickOkayOnUnmaskSsnPopup()
        {
            WaitForElement(5, Elements.PendingDashboardPage.UnmaskSsnPopup.Buttons.Ok);
            var okbutton = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.UnmaskSsnPopup.Buttons.Ok));
            okbutton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");

        }


        public void SelectStateTx()
        {
            WaitForElement(5, Elements.PendingDashboardPage.State.SelectTx);
            var selectState = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.State.SelectTx));
            selectState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select state");
        }

        public void SelectStateWY()
        {
            WaitForElement(5, Elements.PendingDashboardPage.State.SelectWY);
            var selectState = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.State.SelectWY));
            selectState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select state");
        }

        public void SelectLoanInventoryPage()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.LoanInventory);
            var loanInventory = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.LoanInventory));
            loanInventory.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select loan inventory page");
        }

        public void ListLoanIDs()
        {
            IReadOnlyCollection<IWebElement> tableRows =
                Driver.FindElements(By.XPath("//tbody/tr"));
            string LoanId = null;
            
            
            for (int i = 0; i < tableRows.Count; i++)
            {
                foreach (var row in tableRows)
                {
                    LoanId = row.Text;
                    var result = row.Text.Split(new String[] { "\r\n" }, StringSplitOptions.None);
                    Debug.WriteLine(LoanId);
                    Debug.WriteLine(result[0]);
                    Debug.WriteLine(result[1]);
                    Debug.WriteLine(result[2]);
                    Debug.WriteLine(result[3]);
                    Debug.WriteLine(result[4]);

                }
            }
        }

        public void ClickClockInButton()
        {
           WaitForElement(5, Elements.PendingDashboardPage.Buttons.ClockIn);
           var clockInButton = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.ClockIn));
           clockInButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click clock in");
        }

        public void EnterEmployeePassword()
        {
            WaitForElement(5, Elements.PendingDashboardPage.Timecard.PasswordInputField);
            var inputField = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Timecard.PasswordInputField));
            //inputField.SendKeys("goodfellas");
            inputField.SendKeys(Users.User.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "enter password");
        }

        public void ClickOkButton()
        {
            WaitForElement(5, Elements.PendingDashboardPage.Timecard.PasswordInputOkayButton);
            var okayButton =
                Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Timecard.PasswordInputOkayButton));
            okayButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on okay button");
            ExplicitWait(15);
        }

        public void ClickClockOutButton()
        {
            WaitForElement(5, Elements.PendingDashboardPage.Buttons.ClockOut);
            var clockOut = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.ClockOut));
            clockOut.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click clock out");
        }

        public void SelectTimeCardButton()
        {
            WaitForElement(5, Elements.PendingDashboardPage.Buttons.Timecard);
            var timeCardButton = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Buttons.Timecard));
            timeCardButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click time card");
        }

        public bool IsTimecardVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.Timecard.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Timecard popup is visible");
                Logger.Trace($"Timecard visible. ");
                return isVisible;
            }
        }

        public void AssertTimecardPopupIsVisible()
        {
            Assert.IsTrue(IsTimecardVisible, ErrorStrings.TimecardPopupIsNotVisible);
        }
     }
}
