﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class TokensPage : BaseApplicationPage

    {

        public TokensPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Tokens;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Tokens;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Tokens;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsTokensPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Tokens.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public bool IsTokensPageInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Tokens.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return flag;
            }
        }

        public bool IsTokensPageNotVisibleCase {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text) ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page  is not loaded. ");
                return isNotVisible;
            }
        }

        public void AssertIsTokensPageVisible()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.Tokens.ScreenText);
            Assert.IsTrue(IsTokensPageVisible, ErrorStrings.TokensPageNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }
        public void AssertIsTokensPageNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.Tokens.ScreenText);
            Assert.IsTrue(IsTokensPageNotVisibleCase?false:true, ErrorStrings.TokensPageVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }


        

    }
}
