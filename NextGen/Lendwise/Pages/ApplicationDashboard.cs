﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class ApplicationDashboard : BaseApplicationPage
    {
        public ApplicationDashboard(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApplicationDashboardLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Dashboard loaded");
                Logger.Trace($"Application Dashboard page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsApplicationDashboardLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsApplicationDashboardLoaded, ErrorStrings.ApplicationDashboardNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Dashboard is loaded. ");
        }
        public void AssertIsApplicationDashboardNotLoadedCase()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsApplicationDashboardNotLoaded ? false : true, ErrorStrings.ApplicationDashboardLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Dashboard is not loaded. ");
        }
        public bool IsApplicationDashboardNotLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains(InvalidUrl.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Dashboard is not loaded");
                Logger.Trace($"Application Dashboard page is not loaded. ");
                return isVisible;
            }
        }

        public void AssertIsApplicationDashboardNotLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsApplicationDashboardNotLoaded ? false : true, ErrorStrings.ApplicationDashboardNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Dashboard is not loaded. ");
        }


        public void ClickGoToButton()
        {
            WaitForElement(5, Elements.ApplicationDashboard.GoToButton);
            var button = Driver.FindElement(By.XPath(Elements.ApplicationDashboard.GoToButton));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Go To Button ");
        }

        public void ClickItemsPageDropDown()
        {
            WaitForElement(5, Elements.ApplicationDashboard.ItemsPerPageDropDown.ItemsPerPage);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.ApplicationDashboard.ItemsPerPageDropDown.ItemsPerPage));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }

        public void SelectMinNumber()
        {
            WaitForElement(5, Elements.ApplicationDashboard.ItemsPerPageDropDown.ItemsMin);
            var itemsPerPageOption = Driver.FindElement(By.XPath(Elements.ApplicationDashboard.ItemsPerPageDropDown.ItemsMin));
            itemsPerPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click items per page option");
        }
    }
}
