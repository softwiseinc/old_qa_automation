﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class StaticTemplatesPage : BaseApplicationPage
    {

        public StaticTemplatesPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StaticTemplatesPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StaticTemplatesPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StaticTemplatesPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsStaticTemplatesPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the static templates page Loaded");
                Logger.Trace($"static template page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsStaticTemplatesPageLoaded()
        {

            Assert.IsTrue(IsStaticTemplatesPageLoaded, ErrorStrings.StaticTemplatesPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Categories edit page loaded after agent clicked plus button. ");
        }

    }
}
