﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AutomationResources;
using AventStack.ExtentReports;
using MongoDB.Bson;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;


namespace Lendwise.Pages
{
    internal class CustomerDetail : BaseApplicationPage
    {
        public CustomerDetail(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CustomerDetail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CustomerDetail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CustomerDetail;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCustomerDetailVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.CustomerDetailFor))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Customer Detail screen text is visible");
                Logger.Trace("Customer Detail is Visible. ");
                return isVisible;
            }
        }


        public bool IsCustomerDetailInvisible
        {
            get
            {

                var flag = false;
                if (IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.ScreenText.CustomerDetailFor)))
                {
                    flag = true;
                }

                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Customer Detail screen text is Invisible");
                Logger.Trace("Customer Detail is InVisible. ");
                return flag;
            }
        }

        public void AssertCustomerDetailIdIsVisible(string id)
        {
            ExplicitWait(5);
            var result = Driver.PageSource.Contains(id);
            Assert.IsTrue(result, ErrorStrings.CustomerDetailIdNotVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, id + " found.");
        }

        public bool IsSsnVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.SocialSecurity.SocialSecNumber))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        public void AssertIsSsnVisible()
        {

            WaitForElement(5, Elements.CustomerDetailPage.SocialSecurity.SocialSecNumber);
            Assert.IsTrue(IsSsnVisible, ErrorStrings.SsnIsNotVisible);
        }

        public void ClickThreeDot_CustomerID()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.ThreeDotCustomerId);
            var threeButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.ThreeDotCustomerId));
            threeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on three dot menu. ");


        }

        public void ClickThreeDot_Delete_CustomerID()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.ThreeDotDeleteCustomerId);
            var threeButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.ThreeDotDeleteCustomerId));
            threeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Delete menu. ");
        }

        public void AssertCustomerDetailVisible()
        {

            //test for the warning in a while loop
            //if true
            //test for ok
            //if ok click that button
            //else enter manager override
            ExplicitWait(5);
            WaitForElement(5, Elements.CustomerDetailPage.ScreenText.CustomerDetailFor);
            while (IsWarningPopUpVisible())
            {

                if (IsOverrideAllButtonVisible())
                {
                   
                    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Warning Override all button is visible");
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)).Click();
                    ExplicitWait(3);
                    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Override all button if it is visible");

                    if (OverrideHandlerManagerPassword())
                    {
                        Reporter.LogPassingTestStepToBugLogger(Status.Info,"Warning username and password was used");

                    }
                }
                if (IsOkVisibleButton())
                {
                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info, "Warning Okay button is visible");
                    var okay = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Okay));
                    okay.Click();
                    Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp okay");
                }
            }

            Assert.IsTrue(IsCustomerDetailVisible, ErrorStrings.CustomerDetailNotVisible);
            //Assert.IsTrue(IsDuplicateDataVisible, "Duplicate data is not visible");
            var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
            var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
            while (IsDupDataFoundVisible())
            {
                customerDetailPage.DuplicateDataHelper();

            }
            
        }

        //public void AssertCustomerDetailVisible()
        //{

        //    //test for the warning in a while loop
        //    //if true
        //    //test for ok
        //    //if ok click that button
        //    //else enter manager override
        //    ExplicitWait(5);
        //    WaitForElement(5, Elements.CustomerDetailPage.ScreenText.CustomerDetailFor);
        //    while (IsWarningPopUpVisible())
        //    {

        //        if (IsOkVisibleButton())
        //        {
        //            Reporter.LogTestStepForBugLogger(Status.Info, "Warning Okay button is visible");
        //            var okay = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Okay));
        //            okay.Click();
        //            ExplicitWait(3);
        //            Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp okay");
        //        }
        //        if (IsOverrideAllButtonVisible())
        //        {
        //            ExplicitWait(3);
        //            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Warning Override all button is visible");

        //            Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)).Click();
        //            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Override all button if it is visible");

        //        }
        //        if (OverrideHandlerManagerPassword())
        //            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Warning username and password was used");
        //    }

        //    Assert.IsTrue(IsCustomerDetailVisible, ErrorStrings.CustomerDetailNotVisible);
        //}



        public void ClickEmploymentTab()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Employment);
            var employmentTab =
                Driver.FindElement(
                    By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Employment));
            employmentTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the Employment Tab");
        }

        public void ClickOnEmploymentName()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Employment.EmploymentName);
            var clickEmploymentName =
                Driver.FindElement(
                    By.XPath(Elements.CustomerDetailPage.Employment.EmploymentName));
            clickEmploymentName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on employment name");
        }

        public void ClickHowOftenAreYouPaid()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.HowOftenAreYouPaid);
            var howOftenAreYouPaidInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.HowOftenAreYouPaid));
            howOftenAreYouPaidInputField.SendKeys("Monthly");
            howOftenAreYouPaidInputField.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter monthly as How often you are paid");
        }

        public void ClickMonthlyPatternSpecificWeekday()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.MonthlyPattern);
            var monthlyPatternInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.MonthlyPattern));
            monthlyPatternInputField.SendKeys("Specific Weekday");
            monthlyPatternInputField.SendKeys(Keys.Enter);
        }

        public void ClickWeekDay()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.Weekday);
            var weekDayInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.Weekday));
            weekDayInputField.SendKeys("Wednesday");
            weekDayInputField.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a day of the week");

        }

        public void ClickRecentPayday()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.RecentPayday);
            var enterTomorrowsDate =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.RecentPayday));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            enterTomorrowsDate.SendKeys(strDateOnly);
            enterTomorrowsDate.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the following day for when the customer is paid");


        }

        public void ClickSaveButtonEmploymentSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.Buttons.Save);
            var saveButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.Buttons.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the save button on the employment section");
        }

        public void ClickWeek()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.Week);
            var weekInputField = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.Week));
            weekInputField.SendKeys("Fourth");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the forth as the week");

        }

        public void ClickMonthlyPatternSpecificDay()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.MonthlyPattern);
            var monthlyPatternInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.MonthlyPattern));
            monthlyPatternInputField.SendKeys("Specific Day");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Specific Day for the monthly pattern");
        }

        public void ClickRecentPaydaySpecificDay()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Employment.InputFields.RecentPaydaySpecificDay);
            var enterTomorrowsDate =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Employment.InputFields.RecentPaydaySpecificDay));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            enterTomorrowsDate.SendKeys(strDateOnly);
            enterTomorrowsDate.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the following day for when the customer is paid");
        }

        private bool IsWarningPopUpVisible()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            ExplicitWait(2);
            elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));
            if (elementList.Count > 0)
                return true;
            return false;
        }

        private bool IsOverrideAllButtonVisible()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)));
            if (buttonList.Count > 0)
                return true;
            return false;
        }
        private bool IsOkVisibleButton()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Okay)));
            if (buttonList.Count > 0)
                return true;
            return false;

        }
        public bool IsLoanIdInLoansectionVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Table.TableRow))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan ID is visible");
                Logger.Trace("Loan ID is Visible. ");
                return isVisible;
            }
        }

        //AssertLoanIDInLoanSectionVisible
        public void AssertLoanIdInLoanSectionVisible()
        {
            IReadOnlyCollection<IWebElement> tableRows =
                Driver.FindElements(By.XPath(Elements.CustomerDetailPage.Table.TableRow));
            for (int i = 0; i < tableRows.Count; i++)
            {
                WaitForElement(5, Elements.CustomerDetailPage.ScreenText.LoanId);
                Reporter.LogTestStepForBugLogger(Status.Info, "LoanID in Loan section is visible");
                Assert.IsTrue(IsLoanIdInLoansectionVisible, ErrorStrings.LoanIdInLoansectionNotVisible);

            }
        }

        public void ClickCloseButton()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.CustomerDetailPage.WarningPopup.Buttons.Close);
            var workNextButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Close));
            workNextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click close button");
            // Ravi - changed to 7 seconds
        }

        public void AssertCustomerDetailInvisible()
        {
            ExplicitWait(5);
            while (PressButtonText("Close"))
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "Warning is Visible");
                var close = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Close));
                close.Click();
            }


            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Warning popup is not closed'");
            Logger.Trace("Warning popup is not closed. ");
            Assert.IsFalse(IsCustomerDetailInvisible, ErrorStrings.WarningPopupIsClosed);
        }

        public void ClickPrintApplication()
        {
            WaitForElement(5, Elements.CustomerDetailPage.RightHamburgerMenuOptions.PrintApplication);
            var printButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.RightHamburgerMenuOptions.PrintApplication));
            printButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Print Application ");
        }

        public void ClickPrintApplicationPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Popup.Buttons.PrintApplicationPopup);
            var printApplicationButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.PrintApplicationPopup));
            printApplicationButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Print Application. ");
        }

        public void ClickPrintApplicationPopupFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.PrintApplicationPopup)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.PrintApplicationNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Print Application. ");

        }

        public void AsertClickPrintApplicationPopup()
        {
            // assocated test need work and are broken
            //WaitForElement(5, Elements.CustomerDetailPage.Popup.Buttons.PrintApplicationPopup);
            //var printApplicationButton =
            //    Driver.Url.Contains(DummyText.text);
            //Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Print Application. ");


            //var flag = false;
            //if (printApplicationButton.Text == Dummy.text)
            //{
            //    flag = true;
            //    printApplicationButton.Click();
            //}

            //Assert.IsFalse(flag, ErrorStrings.PrintApplicationVisible);
            //Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the Print Application is clicked.");
            //Logger.Trace($"Print Application is cliked.");

        }

        public void ClickEmailApplicationPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Popup.Buttons.EmailApplication);
            var emailButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.EmailApplication));
            emailButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Email Application");
        }

        public void ClickDoneSuccessPopup()
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.CustomerDetailPage.Popup.Buttons.Done);
            var doneButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.Done));
            doneButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Done after Success pops up. ");
        }

        public void ClickDoneSuccessPopupFailCase()
        {

            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.Done)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.DoneBtnIsNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Done after Success pops up. ");
        }

        public void AssertClickDoneSuccessPopupFail()
        {
            ExplicitWait(5);
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.Popup.Buttons.Done)))
            {
                flag = false;
            }


            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Done' button is not clicked after popup.");
            Logger.Trace("'Done' button is not found. ");
            Assert.IsTrue(flag, ErrorStrings.DoneBtnCLickedAfterPopup);


        }

        public void ClickNoteTab()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Notes);
            var notesTab = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Notes));
            notesTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click notes tab to open notes field");

        }

        public void ClickAddButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.AddButton);
            var addNoteButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.AddButton));
            addNoteButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click add note");
        }

        public void SelectNoteType()
        {

            WaitForElement(5, Elements.CustomerDetailPage.Notes.NoteType);
            var typeOfNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.NoteType));
            typeOfNote.SendKeys("Other");
            typeOfNote.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the type of note.");
        }

        public void EnterCustomerDetailNote()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.NoteField);
            var enterNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.NoteField));
            enterNote.SendKeys(Users.User.CustomerDetailNote);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter note ");
        }

        public void ClickThreeDotButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.ThreeDot);
            var threeDot = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.ThreeDot));
            threeDot.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click three dot button");
        }

        public void ClickSaveButtonPayday()
        {
           
            var saveButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save button ");

            ExplicitWait(4);
            OverrideHandlerManagerPasswordPayday();
        }
        private bool OverrideHandlerManagerPasswordPayday()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));

                if (elementList.Count > 0)
                {
                    ExplicitWait(2);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.UserName))
                        .SendKeys(Users.User.OverrideUsername);
                    ExplicitWait(2);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.Password))
                        .SendKeys(Users.User.OverridePassword);
                    ExplicitWait(2);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Override)).Click();
                    ExplicitWait(2);
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }
        
        public void ClickSaveButtonPersonalLoan()
        {
            ExplicitWait(3);
            var saveButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save button ");
            ExplicitWait(2);
            OverrideHandlerManagerPasswordPersonalLoan();
            ExplicitWait(3);
            
        }
        private bool OverrideHandlerManagerPasswordPersonalLoan()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));
                ExplicitWait(3);
                if (elementList.Count > 0)
                {
                    ExplicitWait(3);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.UserNamePersonalLoan))
                        .SendKeys(Users.User.OverrideUsername);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.Password))
                        .SendKeys(Users.User.OverridePassword);
                    ExplicitWait(5);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Override)).Click();
                    ExplicitWait(5);
                    
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }

       
        private bool OverrideHandlerManagerPassword()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.WarningPopup.Warning)));

                if (elementList.Count > 0)
                {
                    
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.UserNameOverride))
                        .SendKeys(Users.User.OverrideUsername);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.PasswordOverride))
                        .SendKeys(Users.User.OverridePassword);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll)).Click();
                    ExplicitWait(5);
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }
        private bool OverrideDeferredValidationPassword()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.DeferredValidationPopup.ScreenText)));

                if (elementList.Count > 0)
                {
                    ExplicitWait(3);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DeferredValidationPopup.Buttons.Override)).Click();
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.UserNameOverride))
                    //    .SendKeys(Users.user.OverrideUsername);
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.PasswordOverride))
                    //    .SendKeys(Users.user.OverridePassword);
                    //Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.Override)).Click();
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }

        public void ClickSaveButtonFailCase()
        {

            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Notes.Save)))
            {
                flag = true;
            }

            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click save button ");
            Assert.IsTrue(flag, ErrorStrings.ClickSaveButtonNotFound);

        }

        public void AssertClickSaveButtonFail()
        {

            ExplicitWait(3);
            var flag = true;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Notes.Save)))
            {
                flag = false;
            }
            Assert.IsTrue(flag, ErrorStrings.CustomerDetailNoteSaveFound);
        }

        public void AssertClickSaveButtonFailCondition()
        {
            ExplicitWait(5);
            var flag = true;
            if (IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.Notes.SaveFail)))
            {
                flag = false;
            }


            Reporter.LogTestStepForBugLogger(Status.Info, "Validate Save' button is not clicked.");
            Logger.Trace("'Save' button is not found. ");

            Assert.IsTrue(flag, ErrorStrings.CustomerDetailNoteSaveFound);
        }

        public void ClickLoansTab()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Loans);
            var loansTab = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Loans));
            loansTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Loans");
        }

        public void ClickShowClosedLoans()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.CheckBox);
            var checkBox = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.CheckBox));
            checkBox.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Show Closed Loans");
        }

        public void ClickShowClosedLoansFailCase()
        {

            ExplicitWait(2);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.CheckBox)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.ClickShowNotClosed);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Show Closed Loans");

        }

        public void AssertClickShowClosedLoansFail()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.CheckBox);
            var checkBox = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.CheckBoxText));

            var flag = false;
            if (checkBox.Text == Dummy.Text)
            {
                flag = true;
            }


            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Show Closed Loans didn't click");
            Logger.Trace("Didn't Click Show Closed Loans");

            Assert.IsFalse(flag, ErrorStrings.ClickShowClosed);
        }

        public void ClickThreeDotButtonOnLoansSection()
        {
            ExplicitWait(3);
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.ThreeDot)))
            {
                WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.ThreeDot);
                var threeDot = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.ThreeDot));
                threeDot.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "click thee dot button");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Error,
                    "Three dot button not present or loan list not found.");
                Assert.IsTrue(false, "Three dot button not present or loan list not found. ");
            }

        }

        public void SelectEditPaybackMethod()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.EditPaybackMethod);
            var editPayback = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.EditPaybackMethod));
            editPayback.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click edit payback method");
        }

        public void SelectPaybackType()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EditPaybackMethodPopup.PaybackType);
            var paybackType =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EditPaybackMethodPopup.PaybackType));
            paybackType.SendKeys(Users.User.PaybackMethod);
            paybackType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Payback Type and click enter ");

        }


        public void SelectSignedDisclosure()
        {
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.SignedDisclosure)))
            {
                ExplicitWait(3);
                WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.SignedDisclosure);
                var signedDisclosure =
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.SignedDisclosure));
                signedDisclosure.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Signed Disclosure ");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Error, "Signed Disclosure button not found. ");
                Assert.IsTrue(false, "Signed Disclosure button not found. ");
            }

        }

        public void ClickSignedDisclosureToCustomer()
        {
            WaitForElement(5,
                Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons.EmailSignedDisclosureToCustomer);
            var email = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons
                .EmailSignedDisclosureToCustomer));
            email.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Email Signed Disclosure ");

        }

        
        public void ClickSignedDisclosureToCustomerFailCase()
        {

            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons
                .EmailSignedDisclosureToCustomer)))
            {
                flag = true;
            }

            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Email Signed Disclosure ");
            Assert.IsTrue(flag, ErrorStrings.EmailSignedDisclosureNotClicked);

        }

        public void AssertClickSignedDisclosureToCustomerFail()
        {
            ExplicitWait(5);
            var flag = true;
            if (!IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons
                .EmailSignedDisclosureToCustomer)))
            {
                flag = false;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Email Signed Disclosure' button is not clicked.");
            Logger.Trace("'Email Signed Disclosure' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.EmailSignedDisclosureClicked);


        }


        public void AssertThreeDotButtonOnLoansSectionFail()
        {
            ExplicitWait(3);
           
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.Buttons.ThreeDot);
            Assert.IsTrue(elementState, "Three dot button not present or loan list not found.");
            Reporter.LogPassingTestStepToBugLogger(Status.Error,
                "Three dot button not present or loan list not found.");
            
        }

        public void SelectLoanTransactions()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanTransactions);
            var loanTransactions =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanTransactions));
            loanTransactions.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Loan transactions ");
        }




        public void AssertNoLoansForThisCustomer(string msgSuccessful)
        {

            ExplicitWait(3);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.NoActiveLoans);
            Assert.IsTrue(elementState, msgSuccessful);
            

        }

       
        public void AssertTransactionForLoanFailCase()
        {
            ExplicitWait(2);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.Funded);
            Assert.IsTrue(elementState, ErrorStrings.TransactionForScreenTextNotVisible);
          
        }


        public void SelectLoanPayoff()
        {
            ExplicitWait(3);
            var makeAPayment = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanPayoff));
            makeAPayment.Click();
            OverrideDeferredValidationPassword();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Make a Payment");
        }

        public void SelectPickup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.Pickup);
            var pickupButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.Pickup));
            pickupButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Pickup button");
            OverrideDeferredValidationPassword();
        }

        public void EnterPayDownPaymentType()
        {
            ExplicitWait(5);



            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType);
            var paymentType =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType));
            paymentType.SendKeys(Users.User.PaymentTypePayDown);
            paymentType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter payment type Pay down and press enter");

        }

        public void EnterPayDownAmount(CheckCityOnline.Users.CurrentUser user)
        {

            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayDownAmount);
            var payDownAmount =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayDownAmount));
            payDownAmount.Clear();
            payDownAmount.SendKeys(user.PaymentSmall);
            payDownAmount.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Pay Down amount and press Enter ");
        }

        public void ClickPayDown()
        {

            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PayDown);
            var payDown =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PayDown));
            payDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Pay Down");
        }

        public void ClickPayDownPersonalLoan()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PayDownPersonalLoan);
            var payDown =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PayDownPersonalLoan));
            payDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Pay Down");
        }

        public void AssertClickPayDownFail()
        {
            ExplicitWait(3);
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PayDown);
            Assert.IsTrue(elementState, ErrorStrings.PayDownButtonVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the 'Pay Down' visible and clicked");
            Logger.Trace($"'Pay Down' is visible and clicked. ");
        }

        public void SelectPaydayHold()
        {
            ExplicitWait(3);
            var hold = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.Hold));
            hold.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Hold feature ");
        }


        public void EnterNextDayForHold()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.HoldPopup.Buttons.NewDepositDate);
            var holdDay =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.HoldPopup.Buttons.NewDepositDate));
            holdDay.SendKeys(Keys.Control + "a");
            //holdDay.SendKeys(Keys.Backspace);
            var date = DateTime.Today.AddDays(5);
            string strDateOnly = date.ToString("d");
            holdDay.SendKeys(strDateOnly);
            holdDay.SendKeys(Keys.Tab);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Choose the next day for hold");
        }

        public void SelectPayoffInquiry()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.PayoffInquiry);
            var payoffInquiry = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.PayoffInquiry));
            payoffInquiry.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pickup Inquiry");
        }

        public void SelectLoanPaymentOnLoanPayDownPopUp()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanPayment);
            var loanPaymentButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanPayment));
            loanPaymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select loan payment");
        }

        public void EnterPayoffInquiryDate()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.PayoffDate);
            var payoffDate =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.PayoffDate));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            ExplicitWait(3);
            payoffDate.Clear();
            payoffDate.Click();
            payoffDate.SendKeys(Keys.Control + 'a');
            payoffDate.SendKeys(Keys.Delete);
            payoffDate.SendKeys(strDateOnly);
            payoffDate.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Choose the next day for hold");
        }


        public void EnterPayoffInquiryDateFail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.PayoffDate);
            var payoffDate = Driver.Url.Contains(InvalidUrl.Url);
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Choose the next day for hold");
        }

        public void EnterExtendPaymentType()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType);
            var paymentType =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType));
            paymentType.SendKeys(Users.User.PaymentTypeExtend);
            paymentType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Extend for Payment Type");
        }

        public void EnterPayoffDate()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayoffDate);
            var payoffDate =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayoffDate));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            payoffDate.SendKeys(strDateOnly);
            payoffDate.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Choose the next day for the Pickup date");
        }

        public void EnterTotalPaymentAmount()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.TotalPayment);
            var totalPayment =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.TotalPayment));
            totalPayment.SendKeys(Users.User.TotalPayment);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter amount of payment ");
        }

        public void ClickExtend()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Extend);
            var extend =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Extend));
            extend.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Extend button");
        }

        public void EnterPayoffPaymentType(CheckCityOnline.Users.CurrentUser user)
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType);
            var paymentType =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentType));
            paymentType.SendKeys(user.PaymentMethodDB);
            paymentType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Pickup type");
        }

        public void ClickPickUp()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Pickup);
            var payoff =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Pickup));
            payoff.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Pickup button ");
        }

        public void AssertClickPickupFail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Pickup);
            var payoff =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Pickup));
            var flag = false;
            if (payoff.Text == Dummy.Text)
            {
                flag = true;
                payoff.Click();
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Pickup' button is not clicked.");
            Logger.Trace("'Pickup' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.PayoffBtnFoundAndClicked);


        }

        public void ClickResetPassword()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.ResetPassword);
            var resetPassword = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.ResetPassword));
            resetPassword.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Reset ManagerPassword");
        }

        public void ClickAcknowledgedButton()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.PasswordResetRequestPopup.Acknowledged);
            var acknowledged =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.PasswordResetRequestPopup.Acknowledged));
            acknowledged.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Acknowledge");

        }

        public void ClickAcknowledgedButtonFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.PasswordResetRequestPopup.Acknowledged)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.AdminPageIsNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Acknowledge");

        }

        public void AssertClickAcknowledgedButtonFail()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.PasswordResetRequestPopup.Acknowledged);
            var flag = false;

            if (!IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.PasswordResetRequestPopup.Acknowledged)))
            {
                flag = true;
            }

            Assert.IsFalse(flag, ErrorStrings.AdminPageIsNotVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the Acknowledge is clicked.");
            Logger.Trace($"AdminPage page is not loaded. ");
        }

        public void ClickOnBankSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.BankInformation);
            var bankInformation =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.BankInformation));
            bankInformation.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click on bank field to expose the current bank account");
        }

        public void ClickOnCheckingAccount()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.CheckingAccount);
            var bankInformation =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.CheckingAccount));
            bankInformation.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click on checking account table data to show banking infomrations");
        }


        public void ClickOnBankAccountName()
        {
            if (IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.BankInformation.Buttons.BankName)))
            {
                WaitForElement(5, Elements.CustomerDetailPage.BankInformation.Buttons.BankName);
                var bankOfCascadesText =
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.Buttons.BankName));
                bankOfCascadesText.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                    "Click the bank name so the account number drops down");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Error, "Unable to click bank name.");
                Assert.IsTrue(false, "Unable to click bank name.");
            }

        }


        public void EnterRoutingNumber()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.RoutingNumber);
            var routingNumber =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.RoutingNumber));
            routingNumber.SendKeys(Users.User.MountainAmerica);
            routingNumber.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Routing number ");
        }

        public int GetRandomNumber(int accountNumber)
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());

            int newAccountNumber = random.Next(9300000, 9400000);

            return newAccountNumber;

        }

        public string EnterAccountNumber()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountNumber);
            var bankAccountNumber =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountNumber));
            bankAccountNumber.Click();
            ExplicitWait(1);
            bankAccountNumber.Clear();
            var csrEmployee = Users.GetEmployee(EmployeeType.CustomerServiceRep);
            var customerDetailPage = new CustomerDetail(Driver, csrEmployee);
            customerDetailPage.DuplicateBankHelper();
            var accountNumber = DateTime.Now.Ticks.ToString();
            accountNumber = accountNumber.Remove(0, 2);
            bankAccountNumber.SendKeys(accountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter account number and store it so the number can be enter again on the next method");
            return accountNumber;

        }

        public void DuplicateBankHelper()
        {
           
            bool DuplicateBankPopupIsVisible = false;

            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.DuplicateDataWarning)))
            {
                DuplicateBankPopupIsVisible = true;
                var okay = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Okay));
                okay.Click();

            }
            Assert.IsFalse(DuplicateBankPopupIsVisible, "ERROR: Duplicate warning popup is visible");
            
        }


        public void BankAccountType()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountType);
            var bankType =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountType));
            bankType.SendKeys(Users.User.BankType);
            bankType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select bank account type");
        }

        public void BankAccountOpenDate()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountOpenDate);
            var openDate =
                Driver.FindElement(
                    By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountOpenDate));
            ExplicitWait(10);
            openDate.Click();
            openDate.Clear();
            //openDate.SendKeys(Keys.Control + 'A');
            //openDate.SendKeys(text:Keys.Delete);
            openDate.SendKeys(Users.User.BankAccountOpenDate);
            openDate.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter date bank was opened.");
        }

        public void SaveButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.Buttons.Save);
            var save = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.Buttons.Save));
            save.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save");
        }

        public void ValidateCheckBox()
        {
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.BankInformation.ReplaceCollateralPopup.Checkbox)))
            {
                WaitForElement(7, Elements.CustomerDetailPage.BankInformation.ReplaceCollateralPopup.Checkbox);
                var checkbox =
                    Driver.FindElement(
                        By.XPath(Elements.CustomerDetailPage.BankInformation.ReplaceCollateralPopup.Checkbox));
                checkbox.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click bank Validation ");
            }

        }

        public void SendAmendment()
        {
            ExplicitWait(1);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.ReplaceCollateralPopup.SendAmendment);
            var sendAmendment =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.ReplaceCollateralPopup
                    .SendAmendment));
            sendAmendment.Click();

        }

        public void ClickOk()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.PendingReplaceCollateralPopup.Ok);
            var ok = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation
                .PendingReplaceCollateralPopup.Ok));
            ok.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");

        }

        public void ClickOkFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.BankInformation.PendingReplaceCollateralPopup.Ok))
            )
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.ClickOkButtonNotFound);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Okay");

        }

        public void PageRefresh()
        {
            Driver.Navigate().Refresh();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Page must reload");
        }

        public void ClickOldBank()
        {

            ExplicitWait(3);
            WaitForElement(10, Elements.CustomerDetailPage.BankInformation.ScreenText.BankName);
            var bankName =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.ScreenText.BankName));
            ExplicitWait(3);
            bankName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click on the bank name to display pending bank message");
        }

        public void ClickOnBankAccountDetail()
        {

            ExplicitWait(3);
            WaitForElement(10, Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountDetail);
            var bankName =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.BankAccountDetail));
            bankName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click on the bank name ");
        }

        public void AssertClickOldBankFailCondition()
        {

            ExplicitWait(5);
            var flag = false;
            if (IsElementDisplayed(By.XPath(Elements.CustomerDetailPage.BankInformation.Buttons.BankNameFail)))
            {
                flag = true;
            }

            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that after clicking on bank name, the pending bank message is not displayed.");

            Assert.IsFalse(flag, ErrorStrings.BankPendingMessage);
        }

        public void ClickShowDeniedApplications()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ShowDeniedApplications);
            var checkBox =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ShowDeniedApplications));
            checkBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Show Denied Applications");
        }

        public void ClickShowDeniedApplicationsFailCase()
        {
            ExplicitWait(2);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ShowDeniedApplications)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.NotClickedOnShowDeniedBtn);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Show Denied Applications");
        }

        public void AssertClickShowDeniedApplicationsFail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ShowDeniedApplications);
            var checkBox =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ShowDeniedApplications));
            var flag = false;
            if (checkBox.Text == Dummy.Text)
            {
                flag = true;
                checkBox.Click();
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Show Denied Applications is not clicked.");
            Logger.Trace("'Show Denied Applications' button is not found. ");
            Assert.IsFalse(flag, ErrorStrings.ClickedOnShowDeniedBtn);
        }

        public void ClickApplicationTab()
        {

            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ApplicationTab);
            var applicationTab =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ApplicationTab));
            applicationTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Application tab ");
        }


        public void ClickLoanTab()
        {

            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ApplicationTab);
            var applicationTab =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ApplicationTab));
            applicationTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Application tab ");
        }

        public void ClickthreeDotButtonOnApplicationSection()
        {
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ThreeDot)))
            {
                ExplicitWait(3);
                WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ThreeDot);
                var threeDot = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ThreeDot));
                threeDot.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Three Dot button ");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Three Dot button not available.");
            }

        }

        public void SelectEmailApprovalPageLink()
        {
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.EmailApprovalPageLink)))
            {
                ExplicitWait(3);
                WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.EmailApprovalPageLink);
                var emailPageLink =
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons
                        .EmailApprovalPageLink));
                emailPageLink.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Email Approval Page Link");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Not able to click Email Approval Page Link");
            }

        }

        public void ClickDoneEmailHasBeenSentPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons.Done);
            var done = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons.Done));
            done.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click done");
        }

        public void ClickDoneEmailHasBeenSentPopupFailCase()
        {

            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons.Done)))
            {
                flag = true;
            }

            Assert.IsFalse(flag, ErrorStrings.DoneButtonNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to click done");
        }

        public void AssertClickDoneEmailHasBeenSentPopupFail()
        {
            if (IsElementPresent(By.XPath(Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons.Done)))
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Email Approval Page Link");
                WaitForElement(5, Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons.Done);
                var done = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.SuccessPopup.Buttons
                    .Done));
                var flag = false;
                if (done.Text == Dummy.Text)
                {
                    flag = false;
                }

                Assert.IsFalse(flag, ErrorStrings.DoneButtonVisible);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the 'Done' is visible and clicked.");
                Logger.Trace($"'Done' is clicked and visible. ");
            }
            else
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "'Done' button is invisible.");

            }

        }

        public void ClickPrint()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons.Print);
            var print = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons
                .Print));
            print.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the print button ");
        }

        public void ClickEmailFailCase()
        {

            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons.EmailSignedDisclosureToCustomer)))
            {
                flag = true;
            }

            Assert.IsTrue(flag, ErrorStrings.EmailSignedDisclosureNotClicked);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click the print button ");

        }

        public void AssertClickPrint()
        {
            ExplicitWait(3);
           
            var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Loans.SignedDisclosurePopup.Buttons.Print);
            Assert.IsTrue(elementState, ErrorStrings.PrintBtnClicked);
            ExplicitWait(3);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the print button is not clicked.");
            Logger.Trace($"The print button is not clicked. ");

        }

        public void PaymentMethodDebitCard(CheckCityOnline.Users.CurrentUser user)
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentMethod);
            var paymentMethod =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentMethod));
            paymentMethod.SendKeys("Bank Card");
            paymentMethod.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter payment method debit card then press enter on the key board");
        }

        public void PayDownAmount()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayDownAmount);
            var payDownAmount =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PayDownAmount));
            payDownAmount.SendKeys(Keys.Control + "a");
            payDownAmount.SendKeys(Keys.Delete);
            payDownAmount.SendKeys(Users.User.PayDownAmount);
            payDownAmount.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Highlight the listed amount delete it then enter pay down amount");

        }

        public bool PressButtonText(string text)
        {
            var checkXpath = $"//*[text()='{text}']";
            try
            {
                Driver.FindElement(By.XPath(checkXpath));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void ClickContact()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Contact);
            var activeMilitary =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Contact));
            activeMilitary.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Contact");
        }

        public void ClickActiveMilitary()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Buttons.ActiveMilitary);
            var activeMilitary =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Buttons.ActiveMilitary));
            activeMilitary.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Active Military");
        }

        public void ClickSaveContactSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Buttons.Save);
            var save = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Buttons.Save));
            save.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save");

        }


        public bool PressButtonTextDisplayed(string text)
        {
            var checkXpath = $"//*[text()='{text}']";
            return Driver.FindElement(By.XPath(checkXpath)).Displayed;
        }


        public void AssertClickSaveFail()
        {
            /*WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Buttons.Save);
            var save = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Buttons.Save));
            var flag = false;
            if (save.Text==Dummy.text)
            {
                flag = true;
                save.Click();
            }

            Assert.IsFalse(flag, ErrorStrings.SaveNoteVisibleClicked);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that the 'Save' is  invisible and not clicked.");
            Logger.Trace($"'Save' button is invisible.");*/
            ExplicitWait(2);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.ContactInformation.Buttons.Save)))
            {
                flag = true;
                Reporter.LogTestStepForBugLogger(Status.Info, " 'Save' button is visible");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                    "Validate that the 'Save' is  invisible and not clicked. ");
            }

            Assert.IsTrue(flag, ErrorStrings.SaveNoteNotVisibleClicked);

        }

        public void ClickApplicationSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Applications);
            var applications =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Applications));
            applications.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click applications section");
        }

        public void ClickDefaultedSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Defaulted);
            var defaulted =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Defaulted));
            defaulted.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Defaulted Section");

        }

        public void ClickShowHistoricalPendingApplications()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ShowHistoricalPendingApplications);
            var showHistoricalPendingApplications = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications
                .Buttons.ShowHistoricalPendingApplications));
            showHistoricalPendingApplications.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click on Show Historical Pending Applications checkbox");
        }

        public void ClickShowHistoricalPendingApplicationsFailCase()
        {
            /*WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ShowHistoricalPendingApplications);
            var showHistoricalPendingApplications = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications
                .Buttons.ShowHistoricalPendingApplications));
            var flag = true;
            if(showHistoricalPendingApplications.Text==DummyText.text)
            {
                flag = false;
                showHistoricalPendingApplications.Click();

            }
            Assert.IsTrue(flag, ErrorStrings.showHistorialPendingApplicationsFail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click on Show Historical Pending Applications checkbox");
            */
            ExplicitWait(2);
           var elementState = AutomationResources.Helper.Page.ElementVisibilityState(Driver, Elements.CustomerDetailPage.Applications.Buttons
                .ShowHistoricalPendingApplications);
            Assert.IsTrue(elementState, ErrorStrings.ShowHistorialPendingApplicationsFail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Unable to Click on Show Historical Pending Applications checkbox");

        }

        public void DeleteNote()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.NoteThreeDot);
            Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.NoteThreeDot)).Click();
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Notes.DeleteNoteButton);
            Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.DeleteNoteButton)).Click();
        }

        public void SelectPayDown()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.Paydown);
            
            var payDownButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.Paydown));
            payDownButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pay down option");
            ExplicitWait(3);
            OverrideDeferredValidationPassword();
        }

        public void ClickViewOnlineCustomerDashboard()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.ViewOnlineCustomerDashboard);
            var viewOnlineCustomerDashboardButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.ViewOnlineCustomerDashboard));
            viewOnlineCustomerDashboardButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click View Online Dashboard");
            
        }

        public void ClickShowHistoricalAccounts()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.Buttons.ShowDeniedApplications);
            var showDeniedAccountsCheckBox =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.Buttons.ShowDeniedApplications));
            showDeniedAccountsCheckBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click denied applications");
        }

        public void ClickWarningSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.Warnings);
            var warningsSection =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.Warnings));
            warningsSection.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Warnings section");
            ExplicitWait(4);
        }

        public void ClickAddNewWarningButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.AddNewWarning);
            var warningsSection =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.AddNewWarning));
            warningsSection.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click add new warning button");
        }

        public void AddWarningNoteNoEmail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Customer Does Not Have Email Address Warning Test Note");

        }

        public void ClickWarningSaveButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningButtons.Save);
            var warningSaveButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningButtons.Save));
            warningSaveButton.Click();
        }

        public void SelectWarningTypeNoEmail()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Customer Does Not Have Email Address");
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Select Warning Type => Customer Does Not Have Email Address.");

        }

        public bool IsWarningTypeNoEmailLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeNoEmail))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate warning message Customer Does Not Have Email Address is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeNoEMailLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeNoEmailLoaded, ErrorStrings.WarningTypeNoEmailNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Customer Does Not Have Email Address is loaded. ");
        }


        public void AddWarningTypesNoEmail()
        {
            //add warning type => Customer Does Not Have Email Address
            SelectWarningTypeNoEmail();
            AddWarningNoteNoEmail();
            ClickWarningSaveButton();
            AssertWarningTypeNoEMailLoaded();

        }

        public bool IsWarningTypeFraudAlertLoaded
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeFraudAlert)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate warning message Potential Fraud Alert is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeFraudAlertLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeFraudAlertLoaded, ErrorStrings.WarningTypeFraudAlertNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Potential Fraud Alert is visible");
        }

        public void AddWarningNoteFraudAlert()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Potential Fraud Alert Warning Test Note");

        }


        public void SelectWarningTypeFraudAlert()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Potential Fraud Alert");
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => Potential Fraud Alert.");

        }

        public void AddWarningTypesFraudAlert()
        {
            //add warning type => Potential Fraud Alert
            SelectWarningTypeFraudAlert();
            AddWarningNoteFraudAlert();
            ClickWarningSaveButton();
            AssertWarningTypeFraudAlertLoaded();

        }

        public bool IsWarningTypeSacLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeSac))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate warning message Suspicious Activity Customer is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }


        public bool IsWarningTypeLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.CustomerWarningTestNote))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Customer Warning Test Note ==> is visible");
                Logger.Trace($"Customer Warning Test Note ==> is visible");
                return isVisible;
            }
        }
        public void AssertWarningTypeSacLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeSacLoaded, ErrorStrings.WarningTypeSACNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Suspicious Activity Customer is visible");
        }

        public void AddWarningNoteSac()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Suspicious Activity Customer Warning Test Note");

        }

        public void SelectWarningTypeSac()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Suspicious Activity Customer");
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => Suspicious Activity Customer");

        }

        public void AddWarningTypesSac()
        {
            //add warning type => Suspicious Activity Customer
            SelectWarningTypeSac();
            AddWarningNoteSac();
            ClickWarningSaveButton();
            AssertWarningTypeSacLoaded();

        }


        public bool IsWarningTypeAtoLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeAto))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate warning message Advanced Teller Override is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeAtoLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeAtoLoaded, ErrorStrings.WarningTypeATONotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Advanced Teller Override is visible");
        }

        public void AddWarningNoteAto()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Advanced Teller Override Warning Test Note");

        }

        public void SelectWarningTypeAto()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Advanced Teller Override");
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => Advanced Teller Override");

        }

        public void AddWarningTypesAto()
        {
            //add warning type => Advanced Teller Override
            SelectWarningTypeAto();
            AddWarningNoteAto();
            ClickWarningSaveButton();
            AssertWarningTypeAtoLoaded();

        }

        /**/

        public bool IsWarningTypeDmoLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeDmo))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate warning message District Manager Override is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeDmoLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeDmoLoaded, ErrorStrings.WarningTypeDMONotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => District Manager Override is visible");
        }

        public void AddWarningNoteDmo()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("District Manager Override Warning Test Note");

        }

        public void SelectWarningTypeDmo()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("District Manager Override");
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => District Manager Override");

        }

        public void AddWarningTypesDmo()
        {
            //add warning type => District Manager Override
            SelectWarningTypeDmo();
            AddWarningNoteDmo();
            ClickWarningSaveButton();
            AssertWarningTypeDmoLoaded();

        }

        /**/

        public bool IsWarningTypeManagerOverrideLoaded
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeManagerOverride))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate warning message Manager Override is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeManagerOverrideLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeManagerOverrideLoaded, ErrorStrings.WarningTypeManagerOverrideNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Manager Override is visible");
        }

        public void AddWarningNoteManagerOverride()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Manager Override Warning Test Note");

        }

        public void SelectWarningTypeManagerOverride()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Manager Override");
            warningTypeAttention.SendKeys(Keys.ArrowDown);
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => Manager Override");

        }

        public void AddWarningTypesManagerOverride()
        {
            //add warning type => Manager Override
            SelectWarningTypeManagerOverride();
            AddWarningNoteManagerOverride();
            ClickWarningSaveButton();
            AssertWarningTypeManagerOverrideLoaded();

        }

        /**/

        public bool IsWarningTypeTellerOverrideLoaded
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.CustomerDetailPage.ScreenText.WarningTypeTellerOverride)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate warning message Teller Override is visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertWarningTypeTellerOverrideLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeTellerOverrideLoaded, ErrorStrings.WarningTypeTellerOverrideNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Teller Override is visible");
        }

        public void AddWarningNoteTellerOverride()
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Teller Override Warning Test Note");

        }

        public void SelectWarningTypeTellerOverride()
        {

            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys("Teller Override");
            warningTypeAttention.SendKeys(Keys.ArrowDown);
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => Teller Override");

        }

        public void AddWarningTypesTellerOverride()
        {
            //add warning type => Teller Override
            SelectWarningTypeTellerOverride();
            AddWarningNoteTellerOverride();
            ClickWarningSaveButton();
            AssertWarningTypeTellerOverrideLoaded();

        }

        public void ClickGoButton()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.GoButton);
            var button = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.GoButton));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click go to button ");

        }

        public void ConfirmDeleteWarning()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.ConfirmDeleteWarning);
            var button = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.ConfirmDeleteWarning));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Delete to confirm Deletion ");

        }

        public void ClickDeleteWarning()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDashboard.Buttons.DeleteWarning);
            var button = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.DeleteWarning));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Delete Warning ");

        }

        public void DeleteWarningTypes()
        {
            ClickDeleteWarning();
            ConfirmDeleteWarning();
        }


        public void SelectHoldAch()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.HoldAch);
            var achHold = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.HoldAch));
            achHold.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "select hold ach feature");
        }

        public void EnterNextDayForHoldOnPersonalLoan()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.HoldPopup.Buttons.NewDepositDatePersonalLoan);
            var holdDay =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.HoldPopup.Buttons
                    .NewDepositDatePersonalLoan));
            holdDay.SendKeys(Keys.Control + "a");
            holdDay.SendKeys(Keys.Backspace);
            var date = DateTime.Today.AddDays(3);
            string strDateOnly = date.ToString("d");
            holdDay.SendKeys(strDateOnly);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a new date");

        }

        public void ClickOkay()
        {
            WaitForElement(5, Elements.CustomerDetailPage.PaymentSuccessfulPopup.Okay);
            var ok = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.PaymentSuccessfulPopup.Okay));
            ok.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click okay");
        }

        public void CardType(CheckCityOnline.Users.CurrentUser user)
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.CardType);
            var cardType = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.CardType));
            cardType.SendKeys(user.CardType);
            cardType.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter last four of card number");
        }

        public void Last4Digits(CheckCityOnline.Users.CurrentUser user)
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Last4Digits);
            var last4Digits =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Last4Digits));
            last4Digits.Click();
            last4Digits.SendKeys(CheckCityOnline.Users.Input.AccountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "enter last four digits of the card");
        }

        public void ClickPayOff()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Payoff);
            var payOff =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.Payoff));
            payOff.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click payoff");
        }

        public void ClickPaymentSchedule()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PaymentSchedule);
            var paymentSchedule =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons
                    .PaymentSchedule));
            paymentSchedule.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Payment Schedule ");
        }

        //public void ClickOnLoanTransaction()
        //{
        //    WaitForElement(5, Elements.CustomerDetailPage.Loans.LoanTransactionsPopup.LoanTransactions.TimeStamp);
        //    var timeStamp =
        //        Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanTransactionsPopup.LoanTransactions
        //            .TimeStamp));
        //    timeStamp.Click();
        //    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click line item that has pm in it");
        //}

        public bool IsDebtNoteVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.DebtNote)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate debt message is visible");
                Logger.Trace($"debt note is visible. ");
                return isVisible;
            }
        }

        public void AssertDebtNoteIsVisible()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.DebtNote);
            Assert.IsTrue(IsDebtNoteVisible, ErrorStrings.NoteTypesNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Debt note is visible ");
        }


        public void ClickSsn()
        {
            WaitForElement(5, Elements.CustomerDetailPage.SocialSecurity.SocialSecNumber);
            var ssnAction = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.SocialSecurity.SocialSecNumber));
            ssnAction.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click SSN ");
        }

        public void EnterManagerUserName()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerUsernameCustomerDetailPage);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerUsernameCustomerDetailPage));
            usernameFieldInput.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager username");
        }

        public void EnterMangerPassword()
        {
            
            WaitForElement(5, Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerPassword);
            var passwordFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerPassword));
            passwordFieldInput.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter password");
        }

        public void ClickOkayOnUnmaskSsnPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.UnmaskSsnPopup.Buttons.Ok);
            var okButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.UnmaskSsnPopup.Buttons.Ok));
            okButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay");

        }

        public void ClickShowProposalLogItems()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Checkboxes.ShowProposalLogItems);
            var clickCheckBoxShowProposal = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans
                .Checkboxes.ShowProposalLogItems));
            clickCheckBoxShowProposal.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Show Prop Log Item");

        }

        public void ClickShowOverride()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Checkboxes.ShowOverride);
            var clickShowOverrideCheckBox =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes
                    .ShowOverride));
            clickShowOverrideCheckBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Show Override");
        }

        public bool IsOverrideColumnVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanTransactionColumns.Override))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate Override column is visible on the transaction popup ");
                Logger.Trace("override column is Visible. ");
                return isVisible;
            }
        }


        public void AssertOverrideIsVisible()
        {

            Assert.IsTrue(IsOverrideColumnVisible, ErrorStrings.ShowOverrideIsNotVisible);

        }

        public void ClickShowSaleId()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Checkboxes.ShowSaleId);
            var clickShowSaleIdCheckBox =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes
                    .ShowSaleId));
            clickShowSaleIdCheckBox.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Show Sale Id");
        }




        public bool IsSaleIdColumnVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanTransactionColumns.SaleItemId))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate Override column is visible on the transaction popup ");
                Logger.Trace("override column is Visible. ");
                return isVisible;
            }
        }

        public void AssertSaleIdIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsSaleIdColumnVisible, ErrorStrings.ShowSaleItemIdIsNotVisible);
        }

        public void ClickClose()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.Close);
            var closeButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.Close));
            closeButton.Click();
        }

        public void LogOut()
        {
            WaitForElement(5, "//*[text()='person']");
            Driver.FindElement(By.XPath("//*[text()='person']")).Click();
            WaitForElement(5, "//*[text()='logout']");
            Driver.FindElement(By.XPath("//*[text()='logout']")).Click();
            ExplicitWait(4);
            WaitForElement(5, "//*[text()='Yes']");
            Driver.FindElement(By.XPath("//*[text()='Yes']")).Click();
            ExplicitWait(4);
        }

        public void AddWarningType(string warning)
        {
            SelectWarningType(warning);
            AddWarningNote(warning);
            ClickWarningSaveButton();
            ExplicitWait(4);
            AssertWarningTypeLoaded(warning);
        }

        private void SelectWarningType(string warning)
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningType);
            var warningTypeAttention =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningType));
            warningTypeAttention.SendKeys(warning);
            warningTypeAttention.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Warning Type => " + warning);
        }

        public void AddWarningNote(string warning)
        {
            WaitForElement(5, Elements.CustomerDetailPage.AddWarningPopUp.WarningNote);
            var warningNote = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.AddWarningPopUp.WarningNote));
            warningNote.SendKeys("Customer Warning Test Note ==> " + warning);
        }

        public void AssertWarningTypeLoaded(string warning)
        {
            ExplicitWait(3);
            Assert.IsTrue(IsWarningTypeLoaded, ErrorStrings.WarningTypeNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that Warning Type => Suspicious Activity Customer is visible");
        }

        public void ClickCloseFailCase()
        {
            ExplicitWait(3);
            var flag = false;
            if (IsElementVisible(By.XPath(Elements.CustomerDetailPage.Loans.PayoffInquiryPopup.Close)))
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.CloseButtonNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Unable to Click Close. ");

        }

        public void ClickAttachmentsTab()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Attachments.ScreenText);
           var attachmentsTab = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Attachments.ScreenText));
           attachmentsTab.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the attachment tab . ");
        }

        public void ClickViewAttachments()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Attachments.Buttons.ViewAttachment);
            var viewAttachmentButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Attachments.Buttons.ViewAttachment));
            viewAttachmentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click view attachment. ");
        }

        public void ClickDoneButton()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Attachments.Buttons.Done);
            var doneButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Attachments.Buttons.Done));
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the done button after image loads. ");
        }

        public void ClickOnFundedForLoanTransactions()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Funded);
            var fundedText = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Funded));
            fundedText.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on funded text so transaction are displayed");
        }
        public bool IsTransactionTextVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.TransactionScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate transaction text is visible ");
                Logger.Trace("transaction text is Visible. ");
                return isVisible;
            }
        }

        public void AssertTransactionTextIsVisible()
        {
            
            Assert.IsTrue(IsTransactionTextVisible, ErrorStrings.TransactionForScreenTextNotVisible);

        }

        public void ClickLoanPayment()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanPayment);
            var loanPaymentOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanPayment));
            loanPaymentOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Loan Payment as the option for this test");
        }

        public void SelectPaymentScheduleOnLoanPayDownPopUp()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons.PaymentSchedule);
           var paymentScheduleOption =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.Buttons
                   .PaymentSchedule));
           paymentScheduleOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Payment Schedule");
        }

        public void SelectLoanSchedule()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanSchedule);
            var loanScheduleOptions =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanSchedule));
            loanScheduleOptions.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Loan Schedule ");
        }

        public void ClickGoToOptionOnSignedDisclosure()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.GoToOption);
            var goToButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.GoToOption));
            goToButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click go to option on signed disclosure ");
        }

        public void ClickVerifyOnEmail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.VerifiedEmailButton);
            var verifiedButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.VerifiedEmailButton));
            verifiedButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the Verify button for email address");
        }

        public void ClickConfirm()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.ConfirmButton);
           var confirm = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.ConfirmButton));
           confirm.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the confirm button to finalize the payment");
        }


        public void ClickPaymentMethodByAch()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentMethod);
            var paymentField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.MakeAPaymentPopup.PaymentMethod));
            paymentField.Click();
            paymentField.SendKeys("ACH");
            paymentField.SendKeys(Keys.Enter);
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the input field for Payment method type in ACH ");
        }

        public void SelectShowProposalLogItems()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Checkboxes.ShowApprovedLoans);
            var listCheck = Driver.FindElements(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes.CheckBoxElementType));
            if(!listCheck[2].Selected)
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes.ShowApprovedLoans)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the show proposal log items check box");
        }

        public void UnSelectShowProposalLogItems()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Checkboxes.ShowApprovedLoans);
            var listCheck = Driver.FindElements(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes.CheckBoxElementType));
            if (listCheck[2].Selected)
            {
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Checkboxes.ShowApprovedLoans)).Click();
                Assert.IsFalse(listCheck[2].Selected, "Test FAIL, CheckBox:Show Proposal Log Items was NOT toggled");
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Test PASS, CheckBox:Show Proposal Log Items was toggled");
        }

        public void SelectLoanPayment()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanPayment);
            var loanPayment = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanPayment));
            loanPayment.Click();
            ExplicitWait(2);
            OverrideDeferredValidationPassword();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click loan payment then deferred validation password maybe called");
        }

        public void ClickEnvelopeByEmailAddress()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Buttons.EmailAddressEnvelope);
           var emailAddressEnvelopeIcon =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.EmailAddressEnvelope));
           emailAddressEnvelopeIcon.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,  "Click the envelope it will be copied to the clip board");
        }

        public void ClickHamburgerMenu()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu);
            var hamburgerMenu = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu));
            hamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click menu button");
        }

        public void SelectApprovedDashboard()
        {
            WaitForElement(10, Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard);
            var approvedDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.ApprovedDashboard));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select approved Dashboard");
        }

        public void ClickEmailAddressInputFieldEraseOldEmail()
        {
            WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Inputfields.Email);
            var emailInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Inputfields.Email));
            emailInputField.SendKeys(Keys.Control + "a");
            emailInputField.SendKeys(Keys.Delete);
            ExplicitWait(2);
            emailInputField.SendKeys(Keys.Control + "v");
            ExplicitWait(1);
            emailInputField.SendKeys(Keys.Tab);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click email address input field clear old email address");
        }

        public void PasteNewEmailAddress()
        {
            //WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Inputfields.Email);
            var emailInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Inputfields.Email));
            emailInputField.SendKeys(Keys.Control + "v");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Paste email address");
        }

        public bool IsDuplicateDataWarningNotVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.DuplicateDataWarning)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Duplicate data warning is visible");
                Logger.Trace($"Duplicate data warning modal appears. ");
                return isVisible;
            }
        }

        public void AssertDuplicateDataWarningVisible()
        {
            WaitForElement(10, Elements.CustomerDetailPage.DuplicateWarningPopup.DuplicateDataWarning);
            Assert.IsTrue(IsDuplicateDataWarningNotVisible ? true : false, ErrorStrings.DuplicateDataWarningVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Duplicate Data Warning is visible. ");
        }

        public void TabOffEmailAddress()
        {
            WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Inputfields.Email);
            var emailInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Inputfields.Email));
            emailInputField.SendKeys(Keys.Tab);
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click tab so the duplicate warning will appear");
        }

        public void ClickCloseOnDuplicateWarningPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Close);
            var closeButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Close));
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click close on the duplicate warning popup");
        }

        public bool IsHoldInstallmentPaymentIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.HoldInstallmentPayment)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the hold installment text is visible");
                Logger.Trace($"Hold installment text is visible. ");
                return isVisible;
            }
        }
        
        public void AssertHoldInstallmentPaymentIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsHoldInstallmentPaymentIsVisible, ErrorStrings.HoldInstallmentPaymentTextNotVisible);
        }

        public bool IsCollateralHeldUntilIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.CollateralHeldUntil)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collateral held until text is visible");
                Logger.Trace($"collateral held until text is visible. ");
                return isVisible;
            }
        }

        public void AssertCollateralHeldUntilIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsCollateralHeldUntilIsVisible, ErrorStrings.CollateralHeldUntilTextNotVisible);
        }

        public void ViewPersonalLoanAgreement()
        {
            WaitForElement(5, Elements.CustomerDetailPage.ContactInformation.Inputfields.Email);
            var emailInputField =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.ContactInformation.Inputfields.Email));
            emailInputField.SendKeys(Keys.Tab);
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click tab so the duplicate warning will appear");
        }
        
        public void ValidateUtahPaydayLoanAgreementSignatures(string signatureHash)
        {
            int expectedCount = 3;
            SelectAgreementDocument("Loan Agreement");
            ExplicitWait(3);
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            CloseLoanAgreementPopUp();
        }

        public void ValidateUtahPersonalLoanAgreementSignatures(string signatureHash)
        {
            int expectedCount = 2;
            SelectAgreementDocument("Loan Agreement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            SelectAgreementDocument("ACH Authorization");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            CloseLoanAgreementPopUp();
        }

        public void ValidateIdahoPersonalLoanAgreementSignatures(string signatureHash)
        {
            int expectedCount = 2;
            SelectAgreementDocument("Loan Agreement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            SelectAgreementDocument("ACH Authorization");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            CloseLoanAgreementPopUp();
        }

        public void ValidateWyomingPaydayLoanAgreementSignatures(string signatureHash)
        {
            int expectedCount = 3;
            SelectAgreementDocument("WyomingLoanAgreement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            CloseLoanAgreementPopUp();
        }

        public void ValidateTexasPersonalLoanAgreementSignatures(string signatureHash)
        {
            int expectedCount = 2;
            SelectAgreementDocument("Loan Agreement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            
            expectedCount = 1;
            SelectAgreementDocument("ACH Authorization");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            
            expectedCount = 3;
            SelectAgreementDocument("Texas CreditService Agreement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            
            SelectAgreementDocument("Texas OCCC Disclosure");
            ExplicitWait(4);
            ClosePrintPopUp();
            
            expectedCount = 3;
            SelectAgreementDocument("Texas Credit Services Disclosure Statement");
            ValidateFirstNameElementCount(signatureHash, expectedCount);
            ClosePrintPopUp();
            
            CloseLoanAgreementPopUp();
        }
        private bool ValidateFirstNameElementCount(string signatureHash, int count)
        {
            ExplicitWait(7);
            bool isSignatureFound = false;
            isSignatureFound = Helper.Page.AskYesNoQuestion($"Did you see the customer name in {count} time in the document?");
            Assert.IsTrue(isSignatureFound, "Signature was not found in agreement");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Validate signature on payday loan agreement");
            return true;
        }
        public void SelectAgreementDocument(string fileName)
        {
            WaitForElement(5, $"//app-document-list-dialog//tr[contains(., '{fileName}')]/td[position()=5]");
            var approvedDashboard =
                Driver.FindElement(By.XPath($"//app-document-list-dialog//tr[contains(., '{fileName}')]/td[position()=5]"));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Agreement document with the following in the file name: " + fileName);
        }

        public void ClosePrintPopUp()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Popup.Print.Button.Close);
            var approvedDashboard =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Popup.Print.Button.Close));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Close print document popup");
        }

        public void CloseLoanAgreementPopUp()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Popup.LoanAgreement.Button.Close);
            var approvedDashboard =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Popup.LoanAgreement.Button.Close));
            approvedDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Close print loan agreement popup");
        }


        public void SelectEPPOption()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.ExtendedPaymentPlan);
           var extendedPaymentPlanOption =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.ExtendedPaymentPlan));
           extendedPaymentPlanOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select EPP option");
        }

        public void ClickFirstPaymentDueDateOnEPPLoanPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EppLoanPopup.InputFields.FirstPaymentDueDateInputField);
            var firstPaymentInputField = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppLoanPopup
                .InputFields.FirstPaymentDueDateInputField));
            firstPaymentInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the First payment due date input field ");
        }

        public void SelectFirstPaymentOptionOnEPPLoanPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EppLoanPopup.InputFields.FirstPaymentDueDateOption);
            var firstPaymentOption =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppLoanPopup.InputFields.FirstPaymentDueDateOption)); 
            firstPaymentOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the date for the first payment of the extended payment plan");
        }
        public void ClickSaveOnPaymentDueDateEPPLoanPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EppLoanPopup.Buttons.Save);
            var saveButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppLoanPopup.Buttons.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save on EPP loan pop up");

        }

        public void ClickOkayOnEPPRequestSubmittedPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EppRequestSubmittedPopup.Okay);
            var okayButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppRequestSubmittedPopup.Okay));
            okayButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click okay on epp request submitted popup");
        }

        public bool IsPendingEPPRequestVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.PendingEPPRequest)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending EPP Request until text is visible");
                Logger.Trace($"Pending EPP Request text is visible. ");
                return isVisible;
            }
        }
        
        public void AssertPendingEPPRequestIsVisible()
        {
            Assert.IsTrue(IsPendingEPPRequestVisible, ErrorStrings.PendingEPPRequestIsNotVisibile);
        }


        public void ClickBlockWebAccess()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.BlockWebAccess);
            var blockWebAccessButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.BlockWebAccess));
            blockWebAccessButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the Block Web Access button");
        }

        public void ClickYesBlockWebAccessPopup()
        {
           WaitForElement(5, Elements.CustomerDetailPage.BlockWebAccessPopup.Buttons.Yes);
           var yesButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BlockWebAccessPopup.Buttons.Yes));
           yesButton.Click();
        }

        public bool IsDebtTypeVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Defaulted.DebtTypWy)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Debt Type text is visible");
                Logger.Trace($"Debt Type text is visible. ");
                return isVisible;
            }
        }
        
        public void AssertDebtTypeIsVisible()
        {
            Assert.IsTrue(IsDebtTypeVisible, ErrorStrings.DebtTypeIsNotVisible);
        }

        public bool IsStatusTypeLateVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanStatusLate)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the loan status LATE text is visible");
                Logger.Trace($"loan status LATE text is visible. ");
                return isVisible;
            }
        }
        
        public void AssertLoanLateStatusIsVisible()
        {
            
            Assert.IsTrue(IsStatusTypeLateVisible, ErrorStrings.LoansStatusLateNotVisible);
        }

        public bool IsStatusStatusRescindedVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanStatusRescinded)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the loan status Rescinded text is visible");
                Logger.Trace($"loan status RESCINDED text is visible. ");
                return isVisible;
            }
        }
        
        public void AssertLoanRescindedStatusIsVisible()
        {
            
            Assert.IsTrue(IsStatusStatusRescindedVisible, ErrorStrings.LoansStatusRescindedNotVisible);
        }

        public bool IsApplicationApprovedApplicationsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.ApplicationStatus)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Approved Application is visible");
                Logger.Trace($"Application Approved Application text is visible. ");
                return isVisible;
            }
        }

        public void AssertApplicationApprovedApplicationIsVisible()
        {
            Assert.IsTrue(IsApplicationApprovedApplicationsVisible, ErrorStrings.ApplicationApprovedApplicationIsNotVisible);
        }

        public void ClickBankCardsSection()
        {
           WaitForElement(5, Elements.CustomerDetailPage.CustomerDetailPageOptions.BankCards);
           var bankCardsSection =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.CustomerDetailPageOptions.BankCards));
           bankCardsSection.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on bank card section ");
        }

        public void ClickAddButtonBankCardSection()
        {
            Driver.SwitchTo().Frame(1);

            WaitForElement(5, Elements.CustomerDetailPage.BankCards.Buttons.AddButton);
            var addButton = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.Buttons.AddButton));
            addButton.Click();
            ExplicitWait(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click add button to add card");

        }

        public void EnterCardHolderName()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields.CardholderName);
            var cardholderName =
                Driver.FindElement(By.CssSelector(Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields
                    .CardholderName));
            cardholderName.SendKeys("Ripley Approved");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Name on card");
            
        }

        public void EnterCardNumber()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields.CardNumber);
            var cardNumber =
                Driver.FindElement(
                    By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields.CardNumber));
            cardNumber.SendKeys("4012888888881881");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter card number");
        }

        public void EnterSecurityCode()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields.SecurityCode);
            var securityCode =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields
                    .SecurityCode));
            securityCode.SendKeys("999");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Security code ");
        }

        public void ClickExpirationDateMonth()
        {
           WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.ExpirationDate.Month);
           var monthSection =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.ExpirationDate
                   .Month));
           monthSection.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click month on expiration date ");
        }
        public void SelectMonthExpirationDate()
        {
           WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.ExpirationDate.ThirdMonth);
           var march = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons
               .ExpirationDate.ThirdMonth));
           march.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "select the month you want ");
        }

        public void ClickExpirationDateYear()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.ExpirationDate.Year);
            var year = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons
                .ExpirationDate.Year));
            year.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click the year field ");
        }
        
        public void SelectYearExpirationDate()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.ExpirationDate.TwentyThreeYear);
            var twentyTwentyThree = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups
                .Buttons.ExpirationDate.TwentyThreeYear));
            twentyTwentyThree.SendKeys("enter");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "select the yeah the card expires ");
        }

        public void EnterBillingZipcode()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields.BillingZipcode);
            var billingZipcode =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.InputFields
                    .BillingZipcode));
            billingZipcode.SendKeys("84601");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Billing zip code");
        }

        public void ClickSaveCardButton()
        {

            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.SaveCard);
            var saveCard =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.SaveCard));
            saveCard.Click();
            ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save Card Button ");
        }

        public void ClickOkStoreCardPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.Okay);
            var okayButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.StoreCardPopups.Buttons.Okay));
            okayButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Okay Button ");
        }


        public void ClickHistoricalAccountsBankCardsSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.BankCards.Buttons.HistoricalAccounts);
            var historicalAccountsOption =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.Buttons.HistoricalAccounts));
            historicalAccountsOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on Historical loans option");
        }

        public bool IsHistoricalAccountsDisplayed
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankCards.VaultId)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the deleted bank cards displayed");
                Logger.Trace($"Deleted Bank cards are visible. ");
                return isVisible;
            }
        }

        
        public void AssertHistoricalAccountsDisplayed()
        {

            Assert.IsTrue(IsHistoricalAccountsDisplayed, "Historical bank cards are not displayed");

        }

        public void SelectAdverseAction()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ContextMenuOptions.AdverseAction);
           var adverseActionOption =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ContextMenuOptions
                   .AdverseAction));
           adverseActionOption.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Adverse Action Option");
        }

        public void ClickContextMenu()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ContextMenu);
            var contextMenu =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ContextMenu));
            contextMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on context menu");
        }

        public bool IsAdverseActionDocumentVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ContextMenuOptions.AdverseActionPopup.AdverseActionPdf)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the deleted bank cards displayed");
                Logger.Trace($"Deleted Bank cards are visible. ");
                return isVisible;
            }
        }
        

        public void AssertAdverseActionDocumentIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsAdverseActionDocumentVisible, ErrorStrings.AdverseActionPdfIsNotVisible);
        }

        public void ClickOkayOnDuplicateWarningPopup()
        {
            WaitForElement(5, Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Okay);
            var closeButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Okay));
            closeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click close on the duplicate warning popup");
        }

        public void SelectCustomerDashboard()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard);
            var customerDashboard =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.CustomerDashboard));
            customerDashboard.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select CustomerDashboard ");
        }


        public string GrabEmailAddress()
        {
            WaitForElement(5, Elements.CustomerDashboard.Buttons.EmailHref);
            string email = Driver.FindElement(By.XPath(Elements.CustomerDashboard.Buttons.EmailHref)).Text;
            return email;
        }


        public void EnterTemporaryPassword(string firstDisposablePassword)
        {
           WaitForElement(5, Elements.CustomerDetailPage.WarningPopup.InputFields.PasswordOverride);
            Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.InputFields.PasswordOverride))
               .SendKeys(firstDisposablePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter temp password");
        }

        public void ClickOverrideAll()
        {
            WaitForElement(5, Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll);
            var overrideAllButton =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.WarningPopup.Buttons.OverrideAll));
            overrideAllButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click override all button");
        }

        public void EnterManagerUserNameOnCustomerDetailPage()
        {
            WaitForElement(5, Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerUsernameCustomerDetailPage);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerUsernameCustomerDetailPage));
            usernameFieldInput.SendKeys(Users.User.OverrideUsername);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager username on customer detail page");
        }

        public void EnterManagerPasswordOnCustomerDetailPage()
        {
            WaitForElement(5, Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerPasswordCustomerDetailPage);
            var usernameFieldInput =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.UnmaskSsnPopup.InputFields.ManagerPasswordCustomerDetailPage));
            usernameFieldInput.SendKeys(Users.User.OverridePassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter manager password on customer detail page");
        }

        public void ClickYesOnEmploymentStatus()
        {
           WaitForElement(5, Elements.CustomerDetailPage.Loans.EppLoanPopup.EmploymentStatusPopup.Buttons.Yes);
           var yes = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppLoanPopup.EmploymentStatusPopup
               .Buttons.Yes));
           yes.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes ");
           OverrideRequiredHandler();
        }

        public void ClickEmailOnEppLoanPopup()
        {

            OverrideRequiredHandler();
            ExplicitWait(3);
            WaitForElement(5, Elements.CustomerDetailPage.Loans.EppLoanPopup.Buttons.Email);
           var email = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.EppLoanPopup.Buttons.Email));
           email.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on Email Button");
           
        }
        private bool OverrideRequiredHandler()
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = true;

            while (isVisible)
            {
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.OverrideRequiredPopup.OverrideRequired)));

                if (elementList.Count > 0)
                {

                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.OverrideRequiredPopup.InputFields.UserName))
                        .SendKeys(Users.User.OverrideUsername);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.OverrideRequiredPopup.InputFields.Password))
                        .SendKeys(Users.User.OverridePassword);
                    Driver.FindElement(By.XPath(Elements.CustomerDetailPage.OverrideRequiredPopup.Buttons.Okay)).Click();
                    ExplicitWait(5);
                }
                else
                {
                    isVisible = false;
                }
                elementList.Clear();
            }
            return isVisible;
        }

        public void ReEnterAccountNumber()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.ReEnterBankAccountNumber);
            var bankAccountNumber =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.ReEnterBankAccountNumber));
            bankAccountNumber.Clear();
            var accountNumber = DateTime.Now.Ticks.ToString();
            accountNumber = accountNumber.Remove(0, 2);
            bankAccountNumber.SendKeys(accountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter account number");
        }

        public void EnterAccountNumber2ndTime(string confirmingAccountNumber)
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.CustomerDetailPage.BankInformation.InputFields.ReEnterBankAccountNumber);
            var bankAccountNumber =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.BankInformation.InputFields.ReEnterBankAccountNumber));
            bankAccountNumber.Clear();
            bankAccountNumber.SendKeys(confirmingAccountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the same account number ");
        }

        public void ClickLoanTerms()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.LoanTerms);
            var loanTerms = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.LoanTerms));
            loanTerms.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Loan Terms");

        }
        public bool IsLoanTermsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.LoanTermsPopup.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Terms screen text is visible");
                Logger.Trace("Loan Terms is Visible. ");
                return isVisible;
            }
        }
        

        public void AssertLoanTermsIsVisible()
        {
            Assert.IsTrue(IsLoanTermsVisible, ErrorStrings.LoanTermsTextNotVisible);

        }


        public void SelectPinnedNote()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.PinnedNote);
            var pinNoteOption = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.PinnedNote));
            pinNoteOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pinned note option ");
        }

        public bool IsPinnedNoteIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.PinnedYellowNote))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate The note is highlighted ");
                Logger.Trace("note field is highlighted Visible. ");
                return isVisible;
            }
        }


        public void AssertPinnedNoteIsVisible()
        {
            ExplicitWait(5);
            Assert.IsTrue(IsPinnedNoteIsVisible, ErrorStrings.PinnedYellowNoteIsNotVisible);
        }

        public void ClickContextMenuNotesSection()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Notes.ContextMenuNotesSections);
            var contextMenu =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Notes.ContextMenuNotesSections));
            contextMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on context menu");
        }

        public void SelectRefinanceDetails()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Loans.Buttons.RefinanceDetails);
            var refinanceDetails =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.Buttons.RefinanceDetails));
            refinanceDetails.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select refinance details ");
        }


        public bool IsRefinanceDetailsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Loans.RefinanceDetailsPopup.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate The note is highlighted ");
                Logger.Trace("note field is highlighted Visible. ");
                return isVisible;
            }
        }




        public void AssertRefinanceDetailsIsVisible()
        {
            
            Assert.IsTrue(IsRefinanceDetailsVisible, ErrorStrings.LoanTermsTextNotVisible);


        }

        public bool IsDuplicateDataVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        public void DuplicateDataHelper()
        {
            ExplicitWait(2);
            if (IsDupDataFoundVisible())
            {
                ExplicitWait(3);
                Reporter.LogTestStepForBugLogger(Status.Info, "Warning Okay button is visible");
                var okay = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.Buttons.Okay));
                okay.Click();
                Reporter.LogTestStepForBugLogger(Status.Info, "Warning PopUp okay");
            }

        }

        private bool IsDupDataFoundVisible()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.CustomerDetailPage.DuplicateWarningPopup.ScreenText)));
            if (buttonList.Count > 0)
                return true;
            return false;

        }

        public void ClickRightHamburgerMenu()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Buttons.RightHamburgerMenu);
            var button = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Buttons.RightHamburgerMenu));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"click right hamburger menu");
        }

        public void UnCheckOpenApplicationsOnly()
        {
            WaitForElement(5, Elements.CustomerDetailPage.Applications.Buttons.ShowOpenApplicationsOnly);
            var openApplicationsOnly =
                Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.Buttons.ShowOpenApplicationsOnly));
            openApplicationsOnly.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "uncheck Open applications only ");

        }

        public void ClickEmailSignedDisclosure()
        {
           WaitForElement(5, Elements.CustomerDetailPage.SignedDisclosurePopup.Buttons.EmailSignedDisclosure);
           var emailSignedDisclosure =
               Driver.FindElement(By.XPath(Elements.CustomerDetailPage.SignedDisclosurePopup.Buttons
                   .EmailSignedDisclosure));
           emailSignedDisclosure.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click email signed disclosure ");

        }


        public bool IsDeniedApplicationVisible
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.CustomerDetailPage.Applications.ApplicationDeniedStatus))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate SSN is visible");
                Logger.Trace($"SSN is visible. ");

                return isVisible;
            }
        }

        public void AssertApplicationStatusDeniedIsVisible()
        {
            Assert.IsTrue(IsDeniedApplicationVisible,"Denied status is not visible");
        }
    }
}

  

