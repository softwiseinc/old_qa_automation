﻿using System;
using System.Data;
using System.Diagnostics;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Lendwise.Pages
{
    internal class LoginPage : BaseApplicationPage
    {
        public LoginPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Login;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Login;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Login;
        }

        public bool IsUrlLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home/Login page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsLoginUnsuccessful
        {
            get
            {
                WaitForElement(15, Elements.LoginPage.ErrorMessage.FailToLogin);
                var isLoaded = Driver.FindElement(By.XPath(Elements.LoginPage.ErrorMessage.FailToLogin)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate user is unable to login");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsLoginSuccessful
        {
            
            get
            {
                bool isPageLoaded = false;
                // check if admin page or pending dashboard

                try
                {
                    isPageLoaded = Driver.FindElement(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)).Displayed;
                }
                catch (Exception e )
                {
                    //Debug.WriteLine(e);
                }

                if (!isPageLoaded)
                {
                    try
                    {
                        isPageLoaded = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.ScreenText.Text)).Displayed;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        
                    }
                }
                return isPageLoaded;

            }
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        internal void LogInUserFailure(Users.CurrentUser user)
        {
            NavigateToLoginPage();
            EnterUserName(user);
            EnterPassword(user);
            ClickLogInButtonInvalidUser();
        }




        public bool IsLoginPageVisible
        {
            get
            {
                var flag = false;
                if (Driver.Url != Dummy.Url)
                {
                    flag = true;
                }
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the login page visible");
                return flag;
            }
        }

        public void AssertLoginPageNotVisible()
        {
            WaitForElement(5, Elements.LoginPage.Button.Login);
            Assert.IsTrue(IsLoginPageVisible, ErrorStrings.HomePageLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Login Page not loaded after about Logout was clicked on. ");
        }


        public void LogInUser(Users.CurrentUser user)
        {
            
            NavigateToLoginPage();
            EnterUserName(user);
            EnterPassword(user);
            ClickLogInButton();
        }


        public void LogInUserForceError(Users.CurrentUser user)
        {
            NavigateToLoginPage();
            EnterUserName(user);
            EnterPassword(user);
            ClickLogInButtonForFailure();

        }
        public void LogInUserForcedError(Users.CurrentUser user)
        {
            NavigateToLoginPage();
            EnterUserName(user);
            EnterPassword(user);
            ClickLogInButtonForFailure();

        }

        private void ClickLogInButtonForFailure()
        {
            WaitForElement(5, Elements.LoginPage.Button.Login);
            Driver.FindElement(By.XPath(Elements.LoginPage.Button.Login)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Login' button.");
            ExplicitWait(3);
            Assert.IsTrue(IsLoginUnsuccessful, ErrorStrings.UserNotLoggedIn);
        }
        private void ClickLogInButton()
        {
            WaitForElement(5, Elements.LoginPage.Button.Login);
            Driver.FindElement(By.XPath(Elements.LoginPage.Button.Login)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Login' button.");
            ExplicitWait(7);
            Assert.IsTrue(IsLoginSuccessful, ErrorStrings.UserNotLoggedIn);
        }
        private void ClickLogInButtonInvalidUser()
        {
            WaitForElement(5, Elements.LoginPage.Button.Login);
            Driver.FindElement(By.XPath(Elements.LoginPage.Button.Login)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Login' button.");
            ExplicitWait(7);
            Assert.IsFalse(IsLoginSuccessful, ErrorStrings.UserNotLoggedIn);
        }

        private void EnterPassword(Users.CurrentUser user)
        {
            WaitForElement(15, Elements.LoginPage.Input.Password);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.Password)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ManagerUsername/Email value in the user name input field.");
        }

        private void EnterUserName(Users.CurrentUser user)
        {
            WaitForElement(15, Elements.LoginPage.Input.UserName);
            ExplicitWait(2);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.UserName)).SendKeys(user.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ManagerUsername/Email value in the user name input field.");
        }

        private void NavigateToLoginPage()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            Assert.IsTrue(IsUrlLoaded, ErrorStrings.HomePageNotLoaded);
        }

        public void SelectLocation()
        {
            return;
            WaitForElement(10, Elements.LoginPage.LocationPopup.CheckCityOnlineId);
            var selectLocation = Driver.FindElement(By.XPath(Elements.LoginPage.LocationPopup.CheckCityOnlineId));
            selectLocation.Click();
            ExplicitWait(3);
        }

        public void ClickAndSelectClose()
        {
            return;
           WaitForElement(10, Elements.LoginPage.LocationPopup.CloseButton);
           var button = Driver.FindElement(By.XPath(Elements.LoginPage.LocationPopup.CloseButton));
           button.Click();
           ExplicitWait(3);
        }

        public void LoginWithChangedPassword(Users.CurrentUser user)
        {

            NavigateToLoginPage();
            EnterUserName(user);
            EnterChangedPassword(user);
            ClickLogInButton();
        }

        private void EnterChangedPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.LoginPage.Input.Password);
            var updatedPassword = Driver.FindElement(By.XPath(Elements.LoginPage.Input.Password));
            updatedPassword.SendKeys(Users.User.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new password");
        }

        public void PageRefresh()
        {
            Driver.Navigate().Refresh();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Page must reload");
        }

        public void LoginMonitor(Users.CurrentUser monEmployee)
        {
            NavigateToLoginPage();
            ClickMonitorIcon();
            PageRefresh();
            EnterUserName(monEmployee);
            EnterPassword(monEmployee);
            ClickLogInButton();

        }

        public void ClickMonitorIcon()
        {
            WaitForElement(5, Elements.LoginPage.Button.MonitorIcon);
            var monitorIconButton = Driver.FindElement(By.XPath(Elements.LoginPage.Button.MonitorIcon));
            monitorIconButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Monitor option");

        }
    }
}