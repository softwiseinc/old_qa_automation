﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class GoogleTtsTemplate : BaseApplicationPage
    {
        public GoogleTtsTemplate(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.GoogleTtsTemplate;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.GoogleTtsTemplate;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.GoogleTtsTemplate;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsGoogleTtsTemplateVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.GoogleTtsTemplate.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsGoogleTtsTemplateVisible()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.GoogleTtsTemplate.ScreenText);
            Assert.IsTrue(IsGoogleTtsTemplateVisible, ErrorStrings.GoogleTtsTemplateNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }




    }
}
