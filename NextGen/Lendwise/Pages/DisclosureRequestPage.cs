﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace Lendwise.Pages
{
    internal class DisclosureRequestPage : BaseApplicationPage
    {

        public DisclosureRequestPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DisclosureRequestPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DisclosureRequestPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DisclosureRequestPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsDisclosureRequestPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections document request page loaded successfully");
                Logger.Trace($"collections document request page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertDisclosureRequestPageLoaded()
        {

            Assert.IsTrue(IsDisclosureRequestPageLoaded, ErrorStrings.DisclosureRequestPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the document request page did not load. ");
        }


    }
}
