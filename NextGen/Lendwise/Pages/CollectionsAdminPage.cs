﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
namespace Lendwise.Pages
{
    internal class CollectionsAdminPage : BaseApplicationPage
    {


        public CollectionsAdminPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsAdminPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsAdminPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsAdminPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsAdminPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections communications page loaded successfully");
                Logger.Trace($"collections communications page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertCollectionsAdminPageLoaded()
        {

            Assert.IsTrue(IsCollectionsAdminPageLoaded, ErrorStrings.CollectionsAdminPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the collections admin page did not load. ");
        }

        internal void ClickCollectionsUserManagementButton()
        {
           WaitForElement(5, Elements.CollectionsAdminPage.Buttons.UserManagement);
            var collectionsUserManagementButton =
                Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.Buttons.UserManagement));
            collectionsUserManagementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Collections User management button");
        }

        public void ClickCommunicationsButton()
        {
            WaitForElement(5, Elements.CollectionsAdminPage.Buttons.Communications);
            var communications = Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.Buttons.Communications));
            communications.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Communications button");
        }

        public void ClickDocumentRequestButton()
        {
            WaitForElement(5, Elements.CollectionsAdminPage.Buttons.DocumentRequest);
            var collectionsDocReq = Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.Buttons.DocumentRequest));
            collectionsDocReq.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,  "Click on doc request button");
        }
    }
}
