﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{
    internal class LoanTypeManagement : BaseApplicationPage
    {
        public LoanTypeManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanTypeManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanTypeManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanTypeManagement;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsLoanTypeManagementPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.LoanTypeManagement.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Type Management screen text is visible");
                Logger.Trace($"Loan Type Management is visible. ");
                return isVisible;
            }
        }
        public bool IsLoanTypeManagementPageNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Type Management screen text is not visible");
                Logger.Trace($"Loan Type Management is visible. ");
                return isNotVisible;
            }
        }


        public void AssertIsLoanTypeManagementVisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.LoanTypeManagement.ScreenText);
            Assert.IsTrue(IsLoanTypeManagementPageVisible, ErrorStrings.LoanTypeManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Loan Type Management page loaded after login in as Administrative agent. ");
        }
        public void AssertIsLoanTypeManagementNotVisibleCase()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.LoanTypeManagement.ScreenText);
            Assert.IsTrue(IsLoanTypeManagementPageNotVisible?false:true, ErrorStrings.LoanTypeManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Loan Type Management page is not loaded after login in as Administrative agent. ");
        }

        public void AssertIsLoanTypeManagementInvisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.LoanTypeManagement.ScreenText);

            var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.LoanTypeManagement.ScreenText));
            var flag = false;
            if (screenText.Text==Dummy.Text)
            {
                flag = true;
            }
            
            Assert.IsFalse(flag, ErrorStrings.LoanTypeManagementVisible);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Type Management screen text is invisible");
            Logger.Trace($"Loan Type Management is invisible. ");
        }


    }
}
