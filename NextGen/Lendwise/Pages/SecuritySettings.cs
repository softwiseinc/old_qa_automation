﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class SecuritySettings : BaseApplicationPage
    {
        public SecuritySettings(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.SecuritySettings;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.SecuritySettings;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.SecuritySettings;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsSecuritySettingsPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.SecuritySettings.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }

        public bool IsSecuritySettingsPageInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.SecuritySettings.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return flag;
            }
        }

        public bool IsSecuritySettingsPageNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isNotVisible;
            }
        }

        public void AssertIsSecuritySettingsPageVisible()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.AdminPage.SecuritySettings.ScreenText);
            Assert.IsTrue(IsSecuritySettingsPageVisible, ErrorStrings.SecuritySettingsNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }

        public void AssertIsSecuritySettingsPageNotVisibleCase()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.AdminPage.SecuritySettings.ScreenText);
            Assert.IsTrue(IsSecuritySettingsPageNotVisible?false:true, ErrorStrings.SecuritySettingsVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }

        public void AssertIsSecuritySettingsPageInvisible()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.AdminPage.SecuritySettings.ScreenText);
            Assert.IsFalse(IsSecuritySettingsPageInvisible, ErrorStrings.SecuritySettingsVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }

    }


}

