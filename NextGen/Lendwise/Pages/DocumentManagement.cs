﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class DocumentManagement : BaseApplicationPage
    {
        public DocumentManagement(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DocumentManagement;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DocumentManagement;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DocumentManagement;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsDocumentManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }
        public bool IsDocumentManagementNotVisible
        {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isNotVisible;
            }
        }

        public bool IsDocumentManagementInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.ScreenText)).Text;
                var flag = (screenText == Dummy.Text) ? true : false;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page is not visible");
                Logger.Trace($"AdminPage page loaded. ");
                return flag;
            }
        }

        public void AssertIsDocumentManagementVisible()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.ScreenText);
            Assert.IsTrue(IsDocumentManagementVisible, ErrorStrings.DocumentManagementNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }

        public void AssertIsDocumentManagementNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.ScreenText);
            Assert.IsTrue(IsDocumentManagementNotVisible?false:true, ErrorStrings.DocumentManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }

        public void AssertIsDocumentManagementInvisible()
        {
            WaitForElement(10, Elements.AdminPage.DocumentManagement.ScreenText);
            Assert.IsFalse(IsDocumentManagementInvisible, ErrorStrings.DocumentManagementVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page is not loaded after login in as Administrative agent. ");
        }
        public void ClickTokens()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.Tokens);
            var tokens = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.Tokens));
            tokens.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Tokens button");
        }
        public void ClickTemplates()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.Templates);
            var templates = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.Templates));
            templates.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Templates button");
        }
        public void ClickGoogleTtsTemplate()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.GoogleTtsTemplate);
            var googleTtsTemplateButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.GoogleTtsTemplate));
            googleTtsTemplateButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Google TTS Template button");

        }
        public void ClickCategoriesButton()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.Categories);
            var categoriesButton = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.Categories));
            categoriesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Categories button");
        }

        public void ClickStaticTemplatesButton()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.StaticTemplates);
            var staticTemplatesButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.StaticTemplates));
            staticTemplatesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Static Templates Button");
        }

        public void ClickQueries()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Buttons.Queries);
            var queries = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Buttons.Queries));
            queries.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on queries button ");
        }
    }
}
