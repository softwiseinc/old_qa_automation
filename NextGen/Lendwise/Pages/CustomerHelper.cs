﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using static CheckCityOnline.Users;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{

    internal class CustomerHelper : BaseApplicationPage
    {
        public CustomerHelper(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CustomerDetail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CustomerDetail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CustomerDetail;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        public void BuildCustomerAccounts(IWebDriver driver, List<string> customerNameList)
        {
            GeneratePayDayLoanCustomer(driver, CustomerType.UT_ApprovedPaydayWithBalance, customerNameList);
            GeneratePersonalLoanCustomer(driver, CustomerType.UT_ApprovedPersonalWithBalance, customerNameList);
            GeneratePersonalLoanCustomer(driver, CustomerType.ID_ApprovedPersonalWithBalance, customerNameList);
            GeneratePayDayLoanCustomer(driver, CustomerType.WY_ApprovedPaydayWithBalance, customerNameList);
            GeneratePersonalLoanCustomer(driver, CustomerType.TX_ApprovedPersonalWithBalance, customerNameList);
        }

        private static void GeneratePersonalLoanCustomer(IWebDriver driver, CustomerType customerType,
            List<string> customerNameList)
        {
            var loanAmount = 118;
            string firstName = null;
            bool isLoanCreated = false;
            while (isLoanCreated == false)
            {
                var customer = GenNewRandomMember(customerType);
                try
                {
                    isLoanCreated = CheckCityOnline.Tests.BaseTest.CreatePersonalLoanCustomer(driver, customer, loanAmount);
                    if (isLoanCreated)
                        firstName = customer.FirstName;
                }
                catch (Exception)
                {
                    Console.WriteLine("Try Create Again :)");
                }
            }
            var state = customerType.ToString().Split('_');
            string customerData = state[0] + ',' + firstName + ',' + "personalLoan";
            customerNameList.Add(customerData);
            Reporter.LogTestStepForBugLogger(Status.Info, "customer created and added to list: " + firstName);
        }

        private static void GeneratePayDayLoanCustomer(IWebDriver driver, CustomerType customerType,
            List<string> customerNameList)
        {
            var loanAmount = 119;
            string firstName = null;
            bool isLoanCreated = false;
            while (isLoanCreated == false)
            {
                var customer = GenNewRandomMember(customerType);
                try
                {
                    isLoanCreated = CheckCityOnline.Tests.BaseTest.CreatePaydayLoanCustomer(driver, customer, loanAmount);
                    if (isLoanCreated)
                        firstName = customer.FirstName;
                }
                catch (Exception)
                {
                    Console.WriteLine("Try Create Again :)");
                }
            }
            var state = customerType.ToString().Split('_');
            string customerData = state[0] + ',' + firstName + ',' + "paydayLoan";
            customerNameList.Add(customerData);
            Reporter.LogTestStepForBugLogger(Status.Info, "customer created and added to list: " + firstName);
        }

        public object BuildCustomerTurndownAccounts(IWebDriver driver)
        {
            // applyturndown(driver, type)
            ApplyTurndown(driver, CustomerType.UT_Turndown);


            // get the UID from the profile page

            // return the uid
            return "UID";
        }

        private void ApplyTurndown(IWebDriver driver, CustomerType utTurndown)
        {

            var turnDownCustomer = GenNewRandomMember(CustomerType.UT_Turndown);
            var applyLoanPage = new CheckCityOnline.Tests.ApplyLoan(Driver, turnDownCustomer);

            //Personal Information
            applyLoanPage.FillPersonalInformationFirstName_Valid(turnDownCustomer);
            applyLoanPage.FillPersonalInformationLastName_Valid(turnDownCustomer);
            applyLoanPage.FillPersonalInformationEmail_Valid();
            applyLoanPage.FillPersonalInformationDob_Valid(turnDownCustomer);
            applyLoanPage.FillPersonalInformationZipCode_Valid(turnDownCustomer);
            applyLoanPage.ClickPersonalInformationContinueBtn();
            applyLoanPage.ClickPersonalInformationPaydayRadio();
            applyLoanPage.ClickPersonalInformationContinueBtn();

            //income information
            applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
            applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
            applyLoanPage.SelectIncomeInformationBiWeekly();
            applyLoanPage.SelectIncomeInformationPayDay();
            applyLoanPage.SelectIncomeInformationIncomeType();
            applyLoanPage.FillIncomeInformationCompanyName();
            applyLoanPage.FillIncomeInformationPhoneNumber();
            applyLoanPage.FillIncomeInformationCity();
            applyLoanPage.SelectIncomeInformationState();
            applyLoanPage.FillIncomeInformationZipCode();
            applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
            applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();

            //Bank Information
            applyLoanPage.FillBankInformationRouting();
            applyLoanPage.FillBankInformationBankAccount();
            applyLoanPage.FillBankInformationBankAccountCheck();
            applyLoanPage.SelectBankInformationTimeWithEmp_Year();
            applyLoanPage.SelectBankInformationTimeWithEmp_Month();

            //Identification Number
            applyLoanPage.SelectDirectDeposit();
            applyLoanPage.SelectIdentificationInformationState();
            applyLoanPage.FillIdentificationNumber();
            applyLoanPage.FillIdentificationExpirationDate();
            applyLoanPage.FillIdentificationSocialSecurity();

            //Contact Information
            applyLoanPage.FillContactInformationStreetAddress();
            applyLoanPage.SelectResidenceLengthYear();
            applyLoanPage.SelectResidenceLengthMonth();
            applyLoanPage.FillContactInformationPhoneNumber(turnDownCustomer);
            applyLoanPage.ClickContactInformationSendPhoneVerification();

            applyLoanPage.ClickContactInformationOptInSms();
            applyLoanPage.ClickContactInformationOptInSmsMarketing();

            //Security Information
            applyLoanPage.FillSecurityInformationPassword(turnDownCustomer);

            //Submit Information
            applyLoanPage.ClickSubmitInformationAgreeConsent();
            applyLoanPage.ClickSubmitInformationAgreeTerms();
            applyLoanPage.ClickSubmitInformationSubmitBtn();
        }
    }
}
