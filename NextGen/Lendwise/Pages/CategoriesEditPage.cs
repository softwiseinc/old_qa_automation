﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class CategoriesEditPage : BaseApplicationPage
    {

        public CategoriesEditPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CategoriesEditPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CategoriesEditPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CategoriesEditPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCategoriesEditPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Categories edit page Loaded");
                Logger.Trace($"Categories edit page loaded. ");
                return isLoaded;
            }
        }

        public void AssertIsCategoriesEditPageLoaded()
        {

            Assert.IsTrue(IsCategoriesEditPageLoaded, ErrorStrings.CategoriesEditPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Categories edit page loaded after agent clicked plus button. ");
        }


        public void ClickNameField()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields.Name);
            var nameField =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields
                    .Name));
            nameField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click name input field");
        }

        public void EnterTestName(string testName)
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields.Name);
            var nameField =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields
                    .Name));
            nameField.SendKeys(testName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the test name");
        }

        public void ClickDescriptionField()
        {
           WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields.Description);
           var descriptionField =
               Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields
                   .Description));
           descriptionField.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the description field");
        }

        public void EnterDescription(string descriptionOfTest)
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields.Description);
            var descriptionField = Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields
                .Description));
            descriptionField.SendKeys(descriptionOfTest);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the description of your test");
        }

        public void ClickSaveButton()
        {
            WaitForElement(5, Elements.AdminPage.DocumentManagement.Categories.EditPage.InputFields.Description);
            var saveButton =
                Driver.FindElement(By.XPath(Elements.AdminPage.DocumentManagement.Categories.EditPage.Buttons.Save));
            saveButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click save button");
        }
    }
}
