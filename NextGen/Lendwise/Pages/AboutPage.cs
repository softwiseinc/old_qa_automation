﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class AboutPage : BaseApplicationPage
    {
        public AboutPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.About;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.About;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.About;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsAboutPageLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the about page visible");
                Logger.Trace($"aboutPage page loaded. ");
                return isVisible;
            }
        }

        public void AssertIsAboutPageVisible()
        {
           Assert.IsTrue(IsAboutPageLoaded, ErrorStrings.AboutPageNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage loaded after about tile was clicked on. ");
        }


        public bool IsAboutPageLoadFail
        {
            get
            {
                var flag = false;
                if (TestEnvironment.Url == Dummy.Url)
                {
                    flag = true;
                }
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the about page visible");
                Logger.Trace($"aboutPage page loaded. ");
                return flag;
            }
        }

        public void AssertIsAboutPageNotVisible()
        {
            Assert.IsFalse(IsAboutPageLoadFail, ErrorStrings.AboutPageLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage not loaded after about tile was clicked on. ");
        }



    }
}
