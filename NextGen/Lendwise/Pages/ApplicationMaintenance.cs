﻿using System;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace Lendwise.Pages
{
    internal class ApplicationMaintenance : BaseApplicationPage
    {
        public ApplicationMaintenance(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ApplicationMaintenance;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ApplicationMaintenance;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ApplicationMaintenance;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsApplicationMaintenancePageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.SystemMaintenancePage.ApplicationMaintenancePage.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Maintenance screen text is visible");
                Logger.Trace($"Application Maintenance page loaded. ");
                return isVisible;
            }
        }
        public bool IsApplicationMaintenancePageNotVisible
        {
            get
            {
                var isVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Application Maintenance screen text is not visible");
                Logger.Trace($"Application Maintenance page is not loaded. ");
                return isVisible;
            }
        }

        public void AssertIsApplicationMaintenancePageVisible()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.ApplicationMaintenancePage.ScreenText);
            Assert.IsTrue(IsApplicationMaintenancePageVisible, ErrorStrings.ApplicationMaintenanceNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Maintenance page loaded after login in as Administrative agent. ");
        }

        public void AssertIsApplicationMaintenancePageNotVisibleCase()
        {
            ExplicitWait(2);
            WaitForElement(10, Elements.AdminPage.SystemMaintenancePage.ApplicationMaintenancePage.ScreenText);
            Assert.IsTrue(IsApplicationMaintenancePageNotVisible?false:true, ErrorStrings.ApplicationMaintenanceVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Application Maintenance page is not loaded after login in as Administrative agent. ");
        }
    }
}
