﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Assert = NUnit.Framework.Assert;
using Keys = OpenQA.Selenium.Keys;

namespace Lendwise.Pages
{
    internal class DecisionDashboardPage : BaseApplicationPage
    {
        public DecisionDashboardPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DecisionDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DecisionDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DecisionDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsDecisionDashboardVisible
        {
            get
            {

                var isVisible = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard loaded");
                Logger.Trace($"Pending Dashboard page loaded. ");

                return isVisible;
            }
        }

        public void AssertIsDecisionDashboardPageLoad()
        {
            
            Assert.IsTrue(IsDecisionDashboardVisible, ErrorStrings.DecisionDashBoardDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The decision dashboard loaded. ");
        }


    }
}
