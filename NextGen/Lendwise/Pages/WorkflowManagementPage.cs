﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;


namespace Lendwise.Pages
{
    internal class WorkflowManagementPage : BaseApplicationPage
    {


        public WorkflowManagementPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.WorkflowManagementPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.WorkflowManagementPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.WorkflowManagementPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsWorkflowManagementPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.WorkflowManagement.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Workflow Management page visible");
                Logger.Trace($"Workflow Management Page page loaded. ");
                return isVisible;
            }
        }

        public bool IsWorkflowManagementPageInvisible
        {
            get
            {
                var screenText = Driver.FindElement(By.XPath(Elements.AdminPage.WorkflowManagement.ScreenText));
                    
                var flag = false;
                if (screenText.Text==Dummy.Text)
                {
                    flag = true;
                }
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Workflow Management page is Invisible");
                Logger.Trace($"Workflow Management Page is not loaded. ");
                return flag;
            }
        }

        public bool IsWorkflowManagementPageNotVisible {
            get
            {
                var isNotVisible = Driver.Url.Contains(DummyText.Text);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Workflow Management page is not visible");
                Logger.Trace($"Workflow Management Page page loaded. ");
                return isNotVisible;
            }
        }

        public void AssertIsWorkflowManagementPageVisible()
        {
            WaitForElement(10, Elements.AdminPage.WorkflowManagement.ScreenText);
            Assert.IsTrue(IsWorkflowManagementPageVisible, ErrorStrings.WorkflowManagementPageNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Workflow Management page loaded after login in as Administrative agent. ");
        }
        public void AssertIsWorkflowManagementPageNotVisibleCase()
        {
            WaitForElement(10, Elements.AdminPage.WorkflowManagement.ScreenText);
            Assert.IsTrue(IsWorkflowManagementPageNotVisible?false:true, ErrorStrings.WorkflowManagementPageVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Workflow Management page is not loaded after login in as Administrative agent. ");
        }

        public void AssertIsWorkflowManagementPageInvisible()
        {
            WaitForElement(10, Elements.AdminPage.WorkflowManagement.ScreenText);
            Assert.IsFalse(IsWorkflowManagementPageInvisible, ErrorStrings.WorkflowManagementPageVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the Workflow Management page is not loaded after login in as Administrative agent. ");
        }
    }
}
