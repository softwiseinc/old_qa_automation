﻿using NLog;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using NUnit.Framework;


namespace Lendwise.Pages
{
   internal class EmployeeSchedulesPage : BaseApplicationPage
    {
        public EmployeeSchedulesPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EmployeeSchedules;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EmployeeSchedules;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.EmployeeSchedules;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsEmployeeSchedulesVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Customer Detail screen text is visible");
                Logger.Trace("Customer Detail is Visible. ");
                return isLoaded;
            }
        }

        public void AssertIsEmployeeSchedulesVisible()
        {
            Assert.IsTrue(IsEmployeeSchedulesVisible, ErrorStrings.AdverseActionSettingsPageNotVisible);

        }
    }
}