﻿using System.CodeDom;
using System.ComponentModel.Design.Serialization;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace CollectionManagement
{
    internal class ErrorStrings
    {
        public const string ShowOverrideIsNotVisible = "The 'Override' column is not visible on the transaction popup";
        public const string LoanInventoryIsNotVisible = "The breadcrumb 'Loan Inventory' is not visible";
        public const string ShowSaleItemIdIsNotVisible =
            "The 'SaleItemId' column is not visible on the transaction popup";

        public const string CCOWebEmployeeAsCustomerLoginNotLoaded =
            "The CCOWebEmployeeAsCustomerLogin page did not load";

        public const string HoldInstallmentPaymentTextNotVisible = "The 'Hold installment Payment' is not visible";
        public const string CollateralHeldUntilTextNotVisible = "The 'Collateral Held Until' is not visible";
        public const string TransactionForScreenTextNotVisible = "The 'Transactions for' screen text is not visible";
        public const string DeResponseMessage = "The 'DeResponse Message' Page did not load";
        public const string SitesPageNotVisible = "Sites 'Text'is not visible";
        public const string HomePageNotLoaded = "Home page did not open successfully";
        public const string PendingEPPRequestIsNotVisibile = "The Text 'Pendnig EPP Request' Is not visible";
        public const string DebtTypeIsNotVisible = "The 'Debt Type' is not Visible";
        public const string LoansStatusLateNotVisible = "The 'Status Late'is not visible";
        public const string LoansStatusRescindedNotVisible = "The 'Status Rescinded'is not visible";
        public const string ApplicationApprovedApplicationIsNotVisible = "The 'Application Approved' Text is not visible ";
        public const string HomePageLoaded = "The 'Home' page is loaded successfully.";
        public const string UserNotLoggedIn = "The user login was not successful";
        public const string AdminPageIsNotVisible = "The 'AdminPage Dashboard' title section is not visible";
        public const string AdverseActionPdfIsNotVisible = "The 'Adverse Action Pdf' is not visible";
        public const string AdminPageIsVisible = "The 'AdminPage Dashboard' title section is  visible";
        public const string AboutPageNotLoaded = "The 'AdminPage Dashboard' title section is not visible";
        public const string AboutPageLoaded = "The 'About' page is loaded successfully.";
        public const string CollectionsDashboardLoaded = "The 'Collections Dasboard' Did not load";
        public const string CollectionsDocumentRequestPageDidNotLoad = "The 'Document request' did not load";
        public const string CollectionsAdminPageDidNotLoad = "The 'Collection Admin Page'did no load";
        public const string CollectionsCommunicationsPageDidNotLoad = "The 'Collectins Communications' Page did not load";

        public const string CollectionsUserRolesManagementTextNotVisible =
            "The 'User Roles Management' Text is not visible";

        public const string CollectionsNoteTypeTextNotVisible = "The 'Note Type' Text is not visible";
        public const string CollectionsTypeManagementTextIsNotVisible = "The 'Type Management' text is not visible";
        public const string DebtPaymentNoteIsNotVisible = "The 'Payment Note' is not visibile";
        public const string CollectionsUserManagementPageDidNotLoad =
            "The 'Collections User Management' Page did not load";

        public const string DisclosureRequestPageDidNotLoad = "The 'Document Request' Page did not load";
        public const string ApplicationRequestPageDidNotLoad = "The 'Application Request' Page did not load";
        public const string CustomerDashboardNotLoaded = "The 'Customer Dashboard' page did not load ";
        public const string ApplicationDashboardNotLoaded = "The 'Application Dashboard' page did not load";
        public const string ApplicationDashboardLoaded = "The 'Application Dashboard' page loaded";
        public const string ApplicationDetailNotVisible = "The 'Application Detail' text did not visible";
        public const string PendingDashboardNotLoaded = "The 'Pending Dashboard' page did not load";
        public const string PendingDashboardLoaded = "The 'Pending Dashboard' page loaded";
        public const string PendingDetailNotVisible = "The 'Pending Detail' page is not visible";
        public const string PendingDetailTextNotVisible = "The validation text is not visible";
        public const string PendingDetailVisible = "The 'Pending Detail' page is  visible";
        public const string TypeManagementNotVisible = "The 'Type Management' text is not visible";
        public const string TypeManagementVisible = "The 'Type Management' text is visible";
        public const string WarningTypesNotVisible = "The 'Warning Types' text is not visible";
        public const string WarningTypesVisible = "The 'Warning Types' text is visible";
        public const string NoteTypesNotVisible = "The 'Note Types' text is not Visible";
        public const string SsnIsNotVisible = "SSN is not visible after clicked";
        public const string NoteTypesVisible = "The 'Note Types' text is  Visible";
        public const string ApplicationWorkStatesNotVisible = "The 'Application Work States' text is not visible";
        public const string ApplicationWorkStatesVisible = "The 'Application Work States' text is visible";
        public const string CustomerDetailNotVisible = "The 'Customer Detail for' screen text is not visible";
        public const string CustomerDetailIdNotVisible = "ID not found in page source.";
        public const string EnterpriseSettingsIsNotVisible = "The 'Enterprise Settings' screen text is not visible";
        public const string EnterpriseSettingsIsVisible = "The 'Enterprise Settings' screen text is visible";
        public const string ApplicationWithdrawReasonsNotVisible = "The 'Application Withdraw Reasons' text is not visible";
        public const string ApplicationWithdrawReasonsVisible = "The 'Application Withdraw Reasons' text is visible";
        public const string SystemMaintenanceNotVisible = "The 'System Maintenace' Screen text is not visible";
        public const string SystemMaintenanceVisible = "The 'System Maintenace' Screen text is visible";
        public const string LocationMaintenanceNotVisible = "The 'Location Maintenance' screen text is not visible";
        public const string ApplicationMaintenanceNotVisible = "The 'Application Maintenance' screen text is not visible";
        public const string LoanTypeManagementNotVisible = "The 'Loan Type Management' Screen text is not visible";
        public const string LoanTypeManagementVisible = "The 'Loan Type Management' Screen text is visible";
        public const string ClickShowClosed = "Click Show Closed Loans";
        public const string WithdrawButtonFound = "The 'Withdraw' button clicked.";
        public const string CustomerDetailNoteSaveFound = "The 'Save' button found and clicked.";
        public const string EmailSignedDisclosureClicked = "The 'Email Signed Disclosure' button found and clicked.";
        public const string PrintBtnClicked = "The print button found and clciked.";
        public const string ResetBtnClicked = "The Reset button found and clciked.";
        public const string PrintApplicationVisible = "The 'Print Application' is visible and clicked";
        public const string SaveNoteVisibleClicked = "The 'Save Note' is visible and clicked";
        public const string DoneButtonVisible = "The 'Done' is visible and clicked";
        public const string PayDownButtonVisible = "The 'Pay Down' is visible and clicked";

        public const string RefreshApplicationBtn = "The 'Refresh Application' button found and clicked.";
        public const string EmailSignedDisclosureNotClicked = "The 'Email Signed Disclosure' button is not found and clicked.";

        public const string DoneBtnIsNotClicked = "The 'Done' button found and clicked.";
        public const string WarningPopupIsClosed = "The warning popup is closed";
        public const string OpoupCloseBtnClicked = "The 'Close' button found and clicked.";
        public const string WarningDeleteBtnClicked = "The 'Delete' button found and clicked.";
        public const string ClickedOnShowDeniedBtn = "The 'Show Denied Applicaiton' button found and clicked.";
        public const string ClickedOnAcknowledgeBtn = "The 'Acknowledge' button found and clicked.";
        public const string ClickedOnAcknowledgeBtnFailed = "The 'Acknowledge' button not found to click";
        public const string DoneBtnCLickedAfterPopup = "The 'Done' button found and clicked.";
        public const string PayoffBtnFoundAndClicked = "The 'Pickup' button found and clicked.";
        public const string AssignBtnClicked = "The 'assign' button found and clicked.";
        public const string ApprovedDashboardNotVisible = "The 'Approved Dashboard' Text is not visibile";
        public const string DuplicateSsnMsgDisplayed = "Duplicate SSN message displayed ";
        public const string DuplicateSsnMsgNotDisplayed = "Duplicate SSN message not displayed ";
        public const string CategoriesEditPageDidNotLoad = "The 'Categories Edit' page did not load";
        public const string StaticTemplatesPageDidNotLoad = "The 'Static Template' page did not load";
        public const string WorkflowManagementPageNotVisible = "The 'Workflow Management'screen text is not visible";
        public const string WorkflowManagementPageVisible = "The 'Workflow Management'screen text is visible";
        public const string AttachmentTypesTextNotVisible = "The 'Attachment Types' screen text is not visible";
        public const string DocumentManagementNotVisible = "The 'Document Management' screen text is not visible";
        public const string QueriesPageNotVisible = "The 'Queries Page'is not visible";
        public const string AdverseActionSettingsPageNotVisible = "The 'Adverse Action Settings' Page did not load";

        public const string CollectionsEmployeeSchedulesPageNotLoaded =
            "The 'Collections Employee Schedules' not loaded";
        public const string StateTimeZoneMappingsPageDidNotLoad = "The 'State Time Zone Mappings' Page did not load";
        public const string DocumentManagementCategoryDidNotLoaded = "The 'Categories Page' did not load.";
        public const string DocumentManagementVisible = "The 'Document Management' screen text is visible";
        public const string GoogleTtsTemplateNotVisible = "The 'Google TTS Template' screen text is not visible";
        public const string TokensPageNotVisible = "The 'Tokens Page' screen text is not visible";
        public const string TokensPageVisible = "The 'Tokens Page' screen text is  visible";
        public const string TemplatesPageNotVisible = "The 'Templates Page' screen text is not visible";
        public const string TemplatesPageInvisible = "The 'Templates Page' screen text is visible";
        public const string UserManagementNotVisible = "The 'User Management' screen text is not visible";
        public const string UserManagementVisible = "The 'User Management' screen text is visible";
        public const string QueueManagementNotVisible = "The 'Queue Management' screen text is not visible";
        public const string QueueManagementVisible = "The 'Queue Management' screen text is visible";
        public const string UserRolesManagementNotVisible = "The 'User Role Management' screen text is not visible";
        public const string UserRolesManagementVisible = "The 'User Role Management' screen text is  visible";
        public const string QueueSettingsNotVisible = "The 'Queue Settings' screen text is not visible";
        public const string SecuritySettingsNotVisible = "The 'Security Settings' screen text is not visible";
        public const string SecuritySettingsVisible = "The 'Security Settings' screen text is visible";
        public const string QueuePriorityNotVisible = "The 'Queue Priority' screen test is not visible";
        public const string QueuePriorityVisible = "The 'Queue Priority' screen test is visible";

        public const string LoanTransactionNotSelected = "The 'Loan Transaction' is not selected";
        public const string GetEmailCountOnPageNotMoreThanOne = "The 'Email Count'is not more than one";
        public const string BankPendingMessage = "The pending message of bank  is displayed.";

        public const string ClickWithdrawFoundAndClicked = "The withdrawl button is found and clicked";
        public const string DoneButtonNotVisible = "The 'Done' is not visible and clicked";

        public const string ClickSaveButtonNotFound = "Save button not found";
        public const string ClickOkButtonNotFound = "The okay button was not found";
        public const string SaveNoteNotVisible = "The 'Save Note' is not visible";

        public const string PrintBtnNotClicked = "The Print button is not clicked";

        public const string DoneButtonNotClicked = "The Done button is not clicked";
        public const string ClickWithdrawNotFound = "The withdrawl button is not found and clicked";
        public const string WarningSaveButtonNotClicked = "The warning save button is not clicked";

        public const string PrintApplicationNotVisible = "Print button is not visible";

        public const string AcknowledgedButtonNotClicked = "The acknowledged button is not clicked";

        public const string ClickCloseOnLoanTransactionPopupFail = "Close on transaction PrintApplicationPopup is not clicked";

        public const string DefaultedSectionNotClicked = "Defaulted section not clicked";

        public const string CustomerDashboardLoaded = "Customer dashboard loaded successfully";

        public const string ApplicationMaintenanceVisible = "Application maintenance page is visible";

        public const string OpoupCloseBtnNotClicked = "Open PoupClose button not clicked";
        public const string ShowHistorialPendingApplicationsFail = "Unable to show historial pendig application ";

        public const string NotClickedOnShowDeniedBtn = "Not clicked on show denied button";

        public const string ClickShowNotClosed = "Unable to click show closed";

        public const string WarningDeleteBtnNotClicked = "Unable to click warning delete button";
        public const string WarningOverrideAllBtnNotClicked = "Unable to click warning Override All button";
        public const string ResetBtnNotClicked = "Reset button not clicked";

        public const string RefreshApplicationBtnNotClicked = "Refresh application button not found";
        
        public const string AssignBtnNotClicked = "Assign button is not clicked";

        public const string DuplicateDataWarningVisible = "Duplicate data warning is visible";
        public const string DuplicateDataWarningNotVisible = "Duplicate Data warning Not visibile";
        public const string WarningTypeNoEmailNotLoaded = "Warning Type => Customer Does Not Have Email Address not loaded.";

        public static string WarningTypeFraudAlertNotLoaded = "Warning Type => Potential Fraud Alert not loaded";

        public static string WarningTypeSACNotLoaded = "Warning Type => Suspicious Activity Customer not loaded";
        public static string WarningTypeNotLoaded = "Warning Type =>  not loaded";

        public static string WarningTypeATONotLoaded = "Warning Type => Advanced Teller Override not loaded";

        public static string WarningTypeDMONotLoaded = "Warning Type => District Manager Override not loaded";

        public static string WarningTypeManagerOverrideNotLoaded = "Warning Type => Manager Override not loaded";

        public static string WarningTypeTellerOverrideNotLoaded = "Warning Type => Teller Override not loaded";
        public static string showHistorialPendingApplications = "Historical Pending application checkbox is not visible";
        public const string LoanIdInLoansectionNotVisible = "LoanID in Loan section not visible";

        public const string SaveNoteNotVisibleClicked = "Save Note button not visible";
        public const string RejectNoteSaveButtonNotVisible = "Save button not visible";
        public const string PrintApplicationApplicationFailCase = "Print button fail case";
        public const string CloseButtonNotVisible = "Close button not visible";
    }

    internal class Dummy
    {
        public const string Url = "http://fake-url.com";
        public const string Text = "LoremIpsum";
    }

    internal class InvalidUrl
    {
        public const string Url = "http://invalidurl.com";

    }
    internal class DummyText
    {
        public const string Text = "DummyText";
    }
    internal class LoanAmount
    {
        public const string InvalidSmall = "99";
        public const string Minimum = "100";
        public const string Medium = "500";
        public const string Maximum = "1000";
        public const string InvalidLarge = "1001";
    }

    internal class PersonalLoanAmount
    {
        public const string InvalidSmall = "99";
        public const string Minimum = "100";
        public const string Medium = "500";
        public const string Maximum = "1000";
        public const string InvalidLarge = "1001";
    }

    internal class PrincipalPayment
    {
        public const string Small = "15";
        public const string Minimum = "25";
        public const string Medium = "50";
        public const string Large = "75";
        public const string Maximum = "100";
    }

    internal class PayDownAmount
    {
        public const string Small = "15";
        public const string Minimum = "25";
        public const string Medium = "50";
        public const string Large = "75";
        public const string Maximum = "100";
    }

    internal class InvalidData
    {
        public const string BirthDate = "11/11/1111";
    }

    internal class ElementBuild
    {
        public const string Part1Extend = "//*[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl";
        public const string Part2Extend = "_hlLoanWidgetOutstandingExtendUrl']";
        public const string Part1Payable = "//*[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl";
        public const string Part2Payable = "_hlLoanWidgetOutstandingPaymentUrl']";
        public const string Part1Refinance = "//*[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl";
        public const string Part2Refinance = "00_hlLoanWidgetOutstandingStoreLoanRefinanceUrl']";
    }
}