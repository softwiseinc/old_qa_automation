﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CollectionManagement.Pages 
{
    internal class UserRolesManagementPage : BaseApplicationPage
    {

    public UserRolesManagementPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
    {
        if (user.RunEnvironment == "PROD")
            TestEnvironment.Url = Url.Production.CollectionsUserRolesManagementPage;
        else if (user.RunEnvironment == "STAGE")
            TestEnvironment.Url = Url.Staging.CollectionsRulesRolesManagement;
        else if (user.RunEnvironment == "QA")
            TestEnvironment.Url = Url.Test.CollectionsUserRolesManagementPage;
    }
    private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

   
    public bool IsUserRolesManagementVisible
        {
        get
        {
            var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
            
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate the users roles management page loaded successfully");
            Logger.Trace($"users roles management page is loaded=>{isLoaded}");
            return isLoaded;
        }
    }


    public void AssertUserRolesManagementPageVisible()
    {

        Assert.IsTrue(IsUserRolesManagementVisible, ErrorStrings.CollectionsUserRolesManagementTextNotVisible);
        Reporter.LogPassingTestStepToBugLogger(Status.Info,
            "Validate the users roles management page did not load. ");
    }

}
}
