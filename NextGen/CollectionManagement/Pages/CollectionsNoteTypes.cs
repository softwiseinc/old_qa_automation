﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace CollectionManagement.Pages
{
    internal class CollectionsNoteTypes : BaseApplicationPage
    {
        public CollectionsNoteTypes(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsNoteTypesPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsNoteTypesPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsNoteTypesPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();



        public bool IsCollectionsNoteTypesVisible
        {
            get
            {
                var isVisible =
                    Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.TypeManagementPage.NoteTypesPage.ScreenText))
                        .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections Type management page is visible");
                Logger.Trace("Collections type management is Visible");
                return isVisible;
            }
        }

        public void AssertCollectionsNoteTypesVisible()
        {

            Assert.IsTrue(IsCollectionsNoteTypesVisible, ErrorStrings.CollectionsNoteTypeTextNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the type management page is not visible. ");
        }


    }
}
