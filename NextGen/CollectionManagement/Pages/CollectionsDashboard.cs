﻿using System;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CollectionManagement.Pages
{
    internal class CollectionsDashboard : BaseApplicationPage
    {

        public CollectionsDashboard(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsDashboard;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsDashboard;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsDashboard;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsDashboardLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        

        public void AssertCollectionsDashboardLoaded()
        {
            
            Assert.IsTrue(IsCollectionsDashboardLoaded, ErrorStrings.CollectionsDashboardLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AboutPage loaded after about tile was clicked on. ");
           
        }


        public void ClickHamburgerMenu()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu);
            var hamburgerMenu = Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenu.ClickHamburgerMenu));
            hamburgerMenu.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click menu button");
        }

        public void SelectAdminPage()
        {
            WaitForElement(5, Elements.LeftMenu.HamburgerMenuOptions.CollectionsAdminPage);
            var collectionsAdminPageOption =
                Driver.FindElement(By.XPath(Elements.LeftMenu.HamburgerMenuOptions.CollectionsAdminPage));
            collectionsAdminPageOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Collections Admin Page");
        }

        public void ClickCustomerId()
        {
           WaitForElement(5, Elements.CollectionsDashboard.ClickAbleObjects.CustomerId);
           var customerId = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.ClickAbleObjects.CustomerId));
           customerId.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Customer id ");

        }

        public void EnterCustomerId(string customerId)
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.CollectionsDashboard.InputFields.SearchBy);
           var customerSearchField = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.InputFields.SearchBy));
           customerSearchField.SendKeys(customerId);
           ExplicitWait(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Customer ID");
        }


        public void ClickGoToButton()
        {
            ExplicitWait(15);
            WaitForElement(5, Elements.CollectionsDashboard.Buttons.GoToButton);
            //var goToButton = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.Buttons.GoToButton));
            //goToButton.Click();

            _ = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.Buttons.GoToButton));
            IWebElement goToButton;
            try
            {
                goToButton = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.Buttons.GoToButton));
                goToButton.Click();
            }
            catch (Exception)
            {
                goToButton = Driver.FindElement(By.XPath(Elements.CollectionsDashboard.Buttons.GoToButton));
                goToButton.Click();
            }

            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click go to button on customer selected");
        }

        public void ClickCustomersName()
        {ExplicitWait(8);
            WaitForElement(5, "//*[contains(@class,'at-cell cdk-cell cdk-column-firstName mat-column-firstName clickable ng-star-inserted')]");
            var firstName = Driver.FindElement(By.XPath(
                "//*[contains(@class,'at-cell cdk-cell cdk-column-firstName mat-column-firstName clickable ng-star-inserted')]"));
            firstName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "work around while go to button is not working ");
        }
    }
}
