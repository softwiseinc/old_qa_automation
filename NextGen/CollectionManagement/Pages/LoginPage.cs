﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CollectionManagement.Pages
{
    internal class LoginPage : BaseApplicationPage
    {
        public LoginPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Login;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Login;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Login;
        }

        public bool IsUrlLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home/Login page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsLoginSuccessful
        {
            
            get
            {
                bool isPageLoaded = false;
                // check if admin page or pending dashboard

                try
                {
                    isPageLoaded = Driver.FindElement(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)).Displayed;
                }
                catch (Exception e )
                {
                    //Debug.WriteLine(e);
                }

                if (!isPageLoaded)
                {
                    try
                    {
                        isPageLoaded = Driver.FindElement(By.XPath(Elements.PendingDashboardPage.ScreenText.Text)).Displayed;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        
                    }
                }
                return isPageLoaded;

            }
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void LogInUser(Users.CurrentUser user)
        {
            
            NavigateToLoginPage();
            EnterUserName(user);
            EnterPassword(user);
            ClickLogInButton();
        }
        private void ClickLogInButton()
        {
            WaitForElement(5, Elements.LoginPage.Button.Login);
            Driver.FindElement(By.XPath(Elements.LoginPage.Button.Login)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Login' button.");
            ExplicitWait(7);
            Assert.IsTrue(IsLoginSuccessful, ErrorStrings.UserNotLoggedIn);
        }
        private void EnterPassword(Users.CurrentUser user)
        {
            WaitForElement(15, Elements.LoginPage.Input.Password);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.Password)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ManagerUsername/Email value in the user name input field.");
        }
        private void EnterUserName(Users.CurrentUser user)
        {
            WaitForElement(15, Elements.LoginPage.Input.UserName);
            ExplicitWait(2);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.UserName)).SendKeys(user.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ManagerUsername/Email value in the user name input field.");
        }
        private void NavigateToLoginPage()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            Assert.IsTrue(IsUrlLoaded, ErrorStrings.HomePageNotLoaded);
        }
    }
}