﻿using System;
using NLog;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;
using System.Data.SqlClient;
using AventStack.ExtentReports;

namespace CollectionManagement.Pages
{
    public class BaseApplicationPage
    {
        protected IWebDriver Driver { get; set; }
        public TestEnvironment TestEnvironment { get; } = new TestEnvironment();

        public BaseApplicationPage(IWebDriver driver)
        {
            Driver = driver;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void ExplicitWait(int seconds)
        {
            int waitTime = seconds * 1000;
            Thread.Sleep(waitTime);
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                Driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public bool IsElementVisible(By by)
        {
            try
            {
                return Driver.FindElement(by).Displayed;

            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void ClickIfPresent(string xpath, string msgBeforeExecution, string msgAfterExecution)
        {
   
            ExplicitWait(7);
            while (IsElementPresent(By.XPath(xpath)))
            {
                Reporter.LogTestStepForBugLogger(Status.Info, msgBeforeExecution);
                var close = Driver.FindElement(By.XPath(xpath));
                close.Click();
                ExplicitWait(5);
                Reporter.LogTestStepForBugLogger(Status.Info, msgAfterExecution);
            }
        }

        public void PopUp(string xpathToCheck, string xpathToClose,  string msgBeforeExecution, string msgAfterExecution)
        {

            ExplicitWait(7);
            while (IsElementPresent(By.XPath(xpathToCheck)))
            {
                Reporter.LogTestStepForBugLogger(Status.Info, msgBeforeExecution);
                var close = Driver.FindElement(By.XPath(xpathToClose));
                close.Click();
                ExplicitWait(5);
                Reporter.LogTestStepForBugLogger(Status.Info, msgAfterExecution);
            }
        }

        public bool IsElementDisplayed(By by)
        {
            try
            {
                return Driver.FindElement(by).Displayed;
 
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void WaitForElement(int seconds, string elementString)
        {
            ExplicitWait(7);
            Logger.Trace($"WAITING FOR ELEMENT: {elementString}");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(seconds));
            var element = By.XPath(elementString);
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(element));
            }
            catch (Exception e)
            {
                Logger.Error($"ERROR: Element was not found {elementString}");
                Logger.Error(e);
                throw;
            }

            Logger.Trace($"ELEMENT FOUND: '{elementString}' <grin>");
        }

        public void GetSomeData()
        {
            var queryString = "select * from contact";
            var connectionString = @"Server=SWDPQABuild01;Database=QA01;User Id=sa;ManagerPassword=softwise;";
    
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                //command.Parameters.AddWithValue("Name", "name");
                connection.Open();
                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Logger.Debug(String.Format("{0}, {1}",
                            reader["Name"], reader["Name"]));// etc
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
        }
    }

    public class TestEnvironment
    {
        public string Url { get; set; }
    }
}