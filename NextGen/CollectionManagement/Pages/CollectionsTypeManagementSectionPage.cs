﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;



namespace CollectionManagement.Pages
{
    internal class CollectionsTypeManagementSectionPage : BaseApplicationPage
    {

        public CollectionsTypeManagementSectionPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsTypeManagePage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsTypeManagePage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsTypeManagePage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
  

        public bool IsTypeManagementVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.ScreenText.TypeManagement)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections Type management page is visible");
                Logger.Trace("Collections type management is Visible");
                return isVisible;
            }
        }

        public void AssertTypeManagementVisible()
        {

            Assert.IsTrue(IsTypeManagementVisible, ErrorStrings.CollectionsUserRolesManagementTextNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the type management page is not visible. ");
        }


        public void ClickNoteTypesButton()
        {
            WaitForElement(5, Elements.CollectionsAdminPage.TypeManagementPage.Buttons.NoteTypes);
            var noteType =
                Driver.FindElement(By.XPath(Elements.CollectionsAdminPage.TypeManagementPage.Buttons.NoteTypes));
            noteType.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Note type button");
        }
    }
}
