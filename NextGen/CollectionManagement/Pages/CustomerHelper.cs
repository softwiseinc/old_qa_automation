﻿using NLog;
using OpenQA.Selenium;

namespace CollectionManagement.Pages
{

    internal class CustomerHelper : BaseApplicationPage
    {
        public CustomerHelper(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CustomerDetail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CustomerDetail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CustomerDetail;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    }
}
