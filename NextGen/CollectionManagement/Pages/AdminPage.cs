﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace CollectionManagement.Pages
{
    internal class AdminPage : BaseApplicationPage
    {
        public AdminPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Admin;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Admin;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Admin;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsAdminPageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.AdminPage.ScreenText.AdminPageDashboard)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the admin page visible");
                Logger.Trace($"AdminPage page loaded. ");
                return isVisible;
            }
        }
        public void AssertIsAdminPageVisible()
        {
            WaitForElement(10, Elements.AdminPage.ScreenText.AdminPageDashboard);
            Assert.IsTrue(IsAdminPageVisible ? true : false, ErrorStrings.AdminPageIsNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the AdminPage page loaded after login in as Administrative agent. ");
        }
        public void ClickPersonPopUp()
        {
           WaitForElement(10, Elements.AdminPage.Buttons.PersonPopUp);
           var personPopUpButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.PersonPopUp));
           personPopUpButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Person PrintApplicationPopup button");
        }
        public void ClickYes()
        {
           WaitForElement(10, Elements.AdminPage.Buttons.Yes);
           var yesButton = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.Yes));
           yesButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to logout. ");
        }
        public void SelectCmsPage()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.CollectionsManagement);
            var cmsPage = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.CollectionsManagement));
            cmsPage.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select CMS option");
        }

        public void ClickEmployeeSchedules()
        {
            WaitForElement(5, Elements.AdminPage.Buttons.EmployeeSchedules);
            var employeeSchedules = Driver.FindElement(By.XPath(Elements.AdminPage.Buttons.EmployeeSchedules));
            employeeSchedules.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Employee Schedules button");
        }
    }
}
