﻿
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CollectionManagement.Pages
{
    internal class CollectionsCommunications : BaseApplicationPage
    {

        public CollectionsCommunications(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsCommunicationsPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsCommunicationsPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsCommunicationsPage;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsCommunicationsPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the collections communications page loaded successfully");
                Logger.Trace($"collections communications page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertCollectionsCommunicationsPageLoaded()
        {

            Assert.IsTrue(IsCollectionsCommunicationsPageLoaded, ErrorStrings.CollectionsCommunicationsPageDidNotLoad);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate the collections communications page did not load. ");
        }



    }
}
