﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using Assert = NUnit.Framework.Assert;

namespace CollectionManagement.Pages
{
    internal class ActionSettingsPage : BaseApplicationPage
    {
       
            public ActionSettingsPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
            {
                if (user.RunEnvironment == "PROD")
                    TestEnvironment.Url = Url.Production.ActionSettingsPage;
                else if (user.RunEnvironment == "STAGE")
                    TestEnvironment.Url = Url.Staging.ActionSettingsPage;
                else if (user.RunEnvironment == "QA")
                    TestEnvironment.Url = Url.Test.ActionSettingsPage;
            }
            private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

            public bool IsActionSettingsPageLoaded
            {
                get
                {

                    var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                    Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Pending Dashboard loaded");
                    Logger.Trace($"Pending Dashboard page loaded. ");

                    return isLoaded;
                }
            }

            public void AssertActionSettingsPageLoaded()
            {
                Assert.IsTrue(IsActionSettingsPageLoaded, "Action Settings Page did not load");
            }


        }
}
