﻿using NLog;
using OpenQA.Selenium;
using AventStack.ExtentReports;
using NUnit.Framework;



namespace CollectionManagement.Pages
{
    internal class CollectionsEmployeeSchedules : BaseApplicationPage
    {
        public CollectionsEmployeeSchedules(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.CollectionsEmployeeSchedulesPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.CollectionsEmployeeSchedulesPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.CollectionsEmployeeSchedulesPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsCollectionsEmployeeSchedulesVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Customer Detail screen text is visible");
                Logger.Trace("Customer Detail is Visible. ");
                return isLoaded;
            }
        }

        public void AssertIsCollectionsEmployeeSchedulesVisible()
        {
            Assert.IsTrue(IsCollectionsEmployeeSchedulesVisible, ErrorStrings.CollectionsEmployeeSchedulesPageNotLoaded);

        }


    }
}