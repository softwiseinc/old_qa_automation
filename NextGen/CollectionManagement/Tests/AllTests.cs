﻿using AutomationResources;
using CollectionManagement.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using static CollectionManagement.Pages.CustomerHelper;
using System.Collections.Generic;
using System.Diagnostics;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using MongoDB.Bson.Serialization.Serializers;
using SharpCompress.Compressors.Deflate;
using static CheckCityOnline.Users;
using CustomerType = AutomationResources.CustomerType;

namespace CollectionManagement.Tests
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    internal partial class AllTests
    {
        //All other partial classes are part of this class
        private partial class AllTestsCases : BaseTest
        {

            [Test]
            [TestCase(TestName = "NavigateToCollectionsDashboard_10557")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsDashboard_10557()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                
                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();
                
                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                
            }
            [Test]
            [TestCase(TestName = "NavigateToCommunicationsSectionsInCMS_10673")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCommunicationsSectionsInCMS_10673()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminDashboardPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsCommunicationsPage = new CollectionsCommunications(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminDashboardPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminDashboardPage.ClickCommunicationsButton();
                
                collectionsCommunicationsPage.AssertCollectionsCommunicationsPageLoaded();

            }
            
            [Test]
            [TestCase(TestName = "NavigateToCollectionsAdminPage_10584")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsAdminPage_10584()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var  collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();
                
                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                
            }
            [Test]
            [TestCase(TestName = "NavigateToEmployeeSchedulesInCMS_11066")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToEmployeeSchedulesInCMS_11066()
            {
                var adminEmployee = Users.GetCollectionsStageEmployeeOwner03();
                var loginPage = new LoginPage(Driver, adminEmployee);
                var collectionsDashboard = new CollectionsDashboard(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                

                loginPage.LogInUser(adminEmployee);
                
                collectionsDashboard.AssertCollectionsDashboardLoaded();
                collectionsDashboard.ClickHamburgerMenu();
                collectionsDashboard.SelectAdminPage();

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickEmployeeSchedules();


            }
            
            [Test]
            [TestCase(TestName = "NavigateToDocumentRequestSectionInCollections_10692")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToDocumentRequestSectionInCollections_10692()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                
                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();

            }
            [Test]
            [TestCase(TestName = "NavigateToUserRolesManagementSectionInCollections_11024")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToUserRolesManagementSectionInCollections_11024()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsUserRolesManagementPage = new UserRolesManagementPage(Driver, adminEmployee);
                

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickUserRolesManagementButton();
               
                
                collectionsUserRolesManagementPage.AssertUserRolesManagementPageVisible();

            }

            [Test]
            [TestCase(TestName = "NavigateToTypeManagementSectionInCollections_11025")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToTypeManagementSectionInCollections_11025()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsTypeManagementPage = new CollectionsTypeManagementSectionPage(Driver, adminEmployee);
                var collectionsUserRolesManagementPage = new UserRolesManagementPage(Driver, adminEmployee);
                
                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickTypeManagementButton();
                
                collectionsTypeManagementPage.AssertTypeManagementVisible();

            }
            [Test]
            [TestCase(TestName = "NavigateToNoteTypesInTypeManagementSectionCollections_11026")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToNoteTypesInTypeManagementSectionCollections_11026()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsTypeManagementPage = new CollectionsTypeManagementSectionPage(Driver, adminEmployee);
                var collectionsNoteTypePage = new CollectionsNoteTypes(Driver, adminEmployee);


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickTypeManagementButton();


                collectionsTypeManagementPage.AssertTypeManagementVisible();
                collectionsTypeManagementPage.ClickNoteTypesButton();

                collectionsNoteTypePage.AssertCollectionsNoteTypesVisible();


            }

            [Test]
            [TestCase(TestName = "NavigateToDisclosureRequestPageCms_10312")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToDisclosureRequestPageCms_10312()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                var disclosureRequestPage = new DisclosureRequestPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();
                collectionsDocumentRequestPage.ClickGoToButtonForDisclosureRequestPage();

                disclosureRequestPage.AssertDisclosureRequestPageLoaded();

            }
            [Test]
            [TestCase(TestName = "NavigateToApplicationRequestPageCms_10524")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToApplicationRequestPageCms_10524()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsDocumentRequestPage = new CollectionsDocumentRequestPage(Driver, adminEmployee);
                var applicatonRequestPage = new ApplicationRequestPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickDocumentRequestButton();

                collectionsDocumentRequestPage.AssertCollectionsDocumentRequestPageLoaded();
                collectionsDocumentRequestPage.ClickGoToButtonForApplicationRequestPage();

                applicatonRequestPage.AssertApplicationRequestPageLoaded();

            }

            [Test]
            [TestCase(TestName = "MakePaymentOnDebtCms_10699")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CMS")]

            public void MakePaymentOnDebtCms_10699()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsDebtorDetailPage = new CollectionsDebtorDetailPage(Driver, adminEmployee);
                var customerId = "U70-124052503";


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.EnterCustomerId(customerId);
                //collectionsDashboardPage.ClickCustomersName();
                collectionsDashboardPage.ClickGoToButton();

                collectionsDebtorDetailPage.AssertCollectionsDebtorDetailPageLoaded();
                collectionsDebtorDetailPage.ClickPaymentButton();
                collectionsDebtorDetailPage.ClickPaymentMethod();
                collectionsDebtorDetailPage.SelectAchAsPaymentMethod();
                collectionsDebtorDetailPage.ClickCategoryInputField();
                collectionsDebtorDetailPage.SelectAchArrangedPaymentAsCategory();
                collectionsDebtorDetailPage.ClickPersonalLoanCheckBox();
                collectionsDebtorDetailPage.ClickAmountInputField();
                collectionsDebtorDetailPage.EnterMinimumPaymentAmount();
                collectionsDebtorDetailPage.ClickSaveButton();
                collectionsDebtorDetailPage.ClickOkOnPaymentSuccessfulPopup();
                collectionsDebtorDetailPage.ClickNotesSection();
                collectionsDebtorDetailPage.AssertPaymentNoteIsVisible();
                
            }
            [Test]
            [TestCase(TestName = "NavigateToMakePaymentOptionInCMS_10982")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CMS")]

            public void NavigateToMakePaymentOptionInCMS_10982()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsDebtorDetailPage = new CollectionsDebtorDetailPage(Driver, adminEmployee);
                var customerId = "001-124816376";


                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.EnterCustomerId(customerId);
                //collectionsDashboardPage.ClickCustomersName();
                collectionsDashboardPage.ClickGoToButton();

                collectionsDebtorDetailPage.AssertCollectionsDebtorDetailPageLoaded();
                collectionsDebtorDetailPage.ClickPaymentButton();
                collectionsDebtorDetailPage.AssertDebtorPaymentPopUpIsVisible();
               

            }

            [Test]
            [TestCase(TestName = "NavigateToCollectionsUserManagementPage_10586")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsUserManagementPage_10586()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsAdminPage = new CollectionsAdminPage(Driver, adminEmployee);
                var collectionsUserManagementPage = new CollectionsUserManagementPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickHamburgerMenu();
                collectionsDashboardPage.SelectAdminPage();

                collectionsAdminPage.AssertCollectionsAdminPageLoaded();
                collectionsAdminPage.ClickCollectionsUserManagementButton();

                collectionsUserManagementPage.AssertCollectionsUserManagementPageLoaded();

            }
            [Test]
            [TestCase(TestName = "NavigateToCollectionsDebtorDetailPage_10588")]
            [Author("Ryan Palmer", "rpalmer@softwiseonline.com")]
            [Parallelizable(ParallelScope.Self)]
            [Category("RegressionTest")]
            [Category("CustomerDetailPage")]
            public void NavigateToCollectionsDebtorDetailPage_10588()
            {
                var adminEmployee = Users.GetEmployee(EmployeeType.Owner);
                var loginPage = new LoginPage(Driver, adminEmployee);
                var adminPage = new AdminPage(Driver, adminEmployee);
                var collectionsDashboardPage = new CollectionsDashboard(Driver, adminEmployee);
                var collectionsDebtorDetailPage = new CollectionsDebtorDetailPage(Driver, adminEmployee);

                loginPage.LogInUser(adminEmployee);

                adminPage.AssertIsAdminPageVisible();
                adminPage.ClickPersonPopUp();
                adminPage.SelectCmsPage();

                collectionsDashboardPage.AssertCollectionsDashboardLoaded();
                collectionsDashboardPage.ClickCustomerId();

                collectionsDebtorDetailPage.AssertCollectionsDebtorDetailPageLoaded();

            }

//*************************
        }
    }
}