﻿using System;
using System.Collections.Generic;
using AutomationResources;

namespace CollectionManagement
{
    
    public class Users
    {
        internal static CurrentUser User { get; private set; }


        //Payday loan customer
        public static CurrentUser GetQaUser001()
        {
            User = new CurrentUser
            {
                Close = "Close",
                RunEnvironment = "QA",
                UserName = "goodfellas5@nostromorp.com",
                Signature = "Frankie Carbone",
                Address = "123 mob Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84103 ",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Attention",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                WarningNote = "I am a note and I like automation",
                PhoneNumber = "8012543973",
                PaymentTypePayoff = "Pickup",
                PaymentTypePayDown = "Paydown",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                Password = @"goodfellas",
                Note = "I am validating this work item",
                NoteTypeDebt = "Debtor Notes",
                BirthDate = @"01 / 03 / 1967",
                CascadesBank = "Checking Account# 654987 (Bank Of The Cascades)",
                SearchByEmail = "qa3@nostromorp.com",
                SecurityQuestion1 = CheckCityOnline.Users.SecurityQuestion.One,
                SecurityQuestion2 = CheckCityOnline.Users.SecurityQuestion.Two,
                SecurityQuestion3 = CheckCityOnline.Users.SecurityQuestion.Three,
                SecurityQuestion4 = CheckCityOnline.Users.SecurityQuestion.Four,
                NewBankAccountNumber = "93701",
                NewPassword = "goodfellas1"
               
            };
            return User;
        }

        public static CurrentUser GetQaUser002()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "goodfellas3@nostromorp.com",
                Signature = "Billy Batts",
                Password = @"goodfellas",
                NewPassword = "goodfellas1",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                OverrideUsername = "qa00@nostromorp.com",
                OverridePassword = "alien",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Teller Override",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Emailed",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "Application Expired",
                CustomerDetailNoteType = "Emailed",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                InvalidateLoanActionNote = "Invalidating this note",
                TransactionDate = "PM",
                SearchByEmail = "qa1@nostromorp.com",
                NewBankAccountNumber = "199701",
                AdverseActionCustomerId = "U10-0006460",
                AdverseActionCustomerEmail  = "Wefawosal@nostromorp.com"


            };
            return User;

        }

      
        public static CurrentUser GetQaUser_invalid_login()
        {
            User = new CurrentUser
            {
                Close = "Close",
                RunEnvironment = "QA",
                UserName = "invalid@invalid.com",
                Signature = "Ravi",
                Password = "invalid",
                LocationId = "CCO"

            };
            return User;

        }
        public static CurrentUser GetQaInvalid_Login_user()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "invalid@lorem.com",
                Signature = "Rajesh",
                Password = "invalid",
                LocationId = "CCO"

            };
            return User;

        }
        
        public static CurrentUser GetQaUser004()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "goodfellas4@nostromorp.com",
                Signature = "Henry Hill",
                Password = "goodfellas",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.CustomerServiceRep,
                WarningType = "Attention",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "expired loan",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                InvalidateLoanActionNote = "Invalidating this note",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                AdverseActionCustomerId = "U10-0006460",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com"

            };
            return User;

        }
        
        public static CurrentUser GetQaUser005()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "QA101@nostromorp.com",
                Signature = "Joe Pending All",
                Password = "Checkcity1!",
                Close = "Close",
                LocationId = "CCO"  //Check City Online

            };
            return User;

        }
        public static CurrentUser GetQaUser006()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "Goodfellas7@nostromorp.com",
                Close = "Close",
                Signature = "Karen Hill",
                NoteType = "Emailed",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Note = "Happy test validate",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Attention",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "expired loan",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "Application Expired",
                PaymentTypePayDown = "Pay Down",
                InvalidateLoanActionNote = "Invalidating this note",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                Password = "goodfellas",
                NewPassword = "goodfellas2",
                ConfirmPassword = "goodfellas2",
                OverrideUsername = "qa00@nostromorp.com",
                OverridePassword = "alien",
                SearchByEmail = "em:qa225@nostromorp.com",
                LocationId = "CCO"  ,
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93701",
                BankType = "Checking",
                BankAccountOpenDate = "06/22/1979",
                AdverseActionCustomerId = "U10-0006460",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com"

            };
            return User;
        }
        public static CurrentUser GetQaUser007()
        {
            User = new CurrentUser
            {
                //personal loan customer
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "qa2227@nostromorp.com",
                Signature = "Kreischer Approved",
                NoteType = "Emailed",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Note = "Happy test validate",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Attention",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "expired loan",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                TotalPayment = "50",
                Password = "Diehard1!",
                NewPassword = "Diehard2!",
                ConfirmPassword = "Diehard2!",
                OverrideUsername = "qa00@nostromorp.com",
                OverridePassword = "alien",
                SearchByEmail = "em:qa2227@nostromorp.com",
                AdverseActionCustomerId = "U10-0006460",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com",
                LocationId = "CCO"  //Check City Online

            };
            return User;

        }
        //for test case 8698
        public static CurrentUser GetQaUser008()
        {
            User = new CurrentUser
            {
                //personal loan customer
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "goodfellas8@nostromorp.com",
                Signature = "Kreischer Approved",
                NoteType = "Emailed",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Note = "Happy test validate",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Teller Override",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "Application Expired",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                TotalPayment = "50",
                Password = "goodfellas",
                NewPassword = "Diehard2!",
                ConfirmPassword = "Diehard2!",
                OverrideUsername = "qa00@nostromorp.com",
                OverridePassword = "alien",
                SearchByEmail = "em:Aurora-WIBTHXXZOZApproved@msgnext.com",
                AdverseActionCustomerId = "U10-0006460",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com",
                LocationId = "CCO"  //Check City Online

            };
            return User;

        }
        public static CurrentUser GetStagingUser001()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "qa000@nostromorp.com",
                Signature = "Ellen Ripley",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "alien",
                LocationId = "UTO",  //Check City Online
                EmployeeRole = EmployeeType.Owner,
                AdverseActionCustomerId = "U10-0006460",
            };
            return User;

        }
        public static CurrentUser GetStagingUser002()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien01@nostromorp.com",
                Signature = "Dallas Skerritt",
                Close = "Close",
                EmployeeRole = EmployeeType.Manager,
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "alien",
                AdverseActionCustomerId = "U10-0006460",
                LocationId = "UTO"  //Check City Online advanced cs rep


            };
            return User;
        }

        public static CurrentUser GetStagingUser003()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                Close = "Close",
                UserName = "alien02@nostromorp.com",
                Signature = "Kane Hurt",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "alien",
                LocationId = "UTO"  ,
                SearchByEmail = "stg7549c@nostromorp.com",
                AdverseActionCustomerId = "U10-0006460"
            };
            return User;

        }
        public static CurrentUser GetStagingUser004()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien03@nostromorp.com",
                Signature = "Ash Holm",
                Close = "Close",
                NoteType = "Emailed",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                NoteReason = "expired loan",
                CancellationNote = "Application Expired",
                Note = "Happy test validate",
                ReassignToUser = "Skerrit, Dallas",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                WarningType = "Update Email Address",
                WarningNote = "Do not loan",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                Password = "alien",
                NewPassword = "aliens",
                ConfirmPassword = "aliens",
                SearchByEmail = "em:qa446@nostromorp.com",
                OverrideUsername = "qa000@nostromorp.com",
                OverridePassword = "alien",
                LocationId = "CCO"  ,
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93700",
                BankType = "Checking",
                BankAccountOpenDate = "06/06/1996",
                AdverseActionCustomerId = "U10-0006460"

            };
            return User;

        }
        public static CurrentUser GetStagingUser005()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien04@nostromorp.com",
                Signature = "Kane Hurt",
                Password = "alien",
                NoteType = "Emailed",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                NoteReason = "expired loan",
                CancellationNote = "Application Expired",
                Note = "Happy test validate",
                ReassignToUser = "Baker, Alex",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                WarningType = "Update Email Address",
                WarningNote = "Do not loan",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                NewPassword = "aliens",
                ConfirmPassword = "aliens",
                SearchByEmail = "em:qa456@nostromorp.com",
                OverrideUsername = "qa000@nostromorp.com",
                OverridePassword = "alien",
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93700",
                BankType = "Checking",
                BankAccountOpenDate = "06/06/1996",
                AdverseActionCustomerId = "U10-0006460",
                LocationId = "UTO"  //Check City Online

            };
            return User;

        }
        //Payday loan customer
        public static CurrentUser GetStagingUser006()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien04@nostromorp.com",
                Signature = "Kane Hurt",
                Password = "alien",
                NoteType = "Emailed",
                Close = "Close",
                NoteReason = "Application Expired",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                CancellationNote = "Application Expired",
                Note = "Happy test validate",
                ReassignToUser = "Baker, Alex",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                WarningType = "Update Email Address",
                WarningNote = "Do not loan",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                NewPassword = "aliens",
                ConfirmPassword = "aliens",
                SearchByEmail = "em:qa4@nostromorp.com",
                OverrideUsername = "qa000@nostromorp.com",
                OverridePassword = "alien",
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93700",
                BankType = "Checking",
                BankAccountOpenDate = "06/06/1996",
                AdverseActionCustomerId = "U10-0006460",
                LocationId = "UTO"  //Check City Online

            };
            return User;

        }

        public static CurrentUser GetProdUser001()
        {
            User = new CurrentUser
            {
                RunEnvironment = "PROD",
                UserName = "Raystantz@checkcity.com",
                Signature = "Ray Stantz",
                Password = "Ghostbusters1!",
                Close = "Close",
                SecurityQuestion1 = CheckCityOnline.Users.SecurityQuestion.One,
                SecurityQuestion2 = CheckCityOnline.Users.SecurityQuestion.Two,
                SecurityQuestion3 = CheckCityOnline.Users.SecurityQuestion.Three,
                SecurityQuestion4 = CheckCityOnline.Users.SecurityQuestion.Four,
                NewPassword = "Ghostbusters1!",

            };
            return User;
        }

        public static CurrentUser InvalidUsernamePassword()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "InvalidUsername@example.com",
                Close = "Close",
                Password = "InvalidPassword!"
            };
            return User;
        }

        public static CurrentUser GetQaEmployeeOwner01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "QA00@nostromorp.com",
                Signature = "Dallas Skeritt",
                Password = "alien",
                LocationId = "CCO",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                EmployeeRole = EmployeeType.Owner

            };
            return User;

        }
        public static CurrentUser GetQaEmployeeOwner02()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "GoodFellas@nostromorp.com",
                Signature = "Pauly Ciscero",
                Password = "goodfellas",
                LocationId = "CCO",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                EmployeeRole = EmployeeType.Manager

            };
            return User;

        }
        public static CurrentUser GetQaEmployeeManager01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "goodfellas1@nostromorp.com",
                Signature = "James Conway",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "goodfellas",
                LocationId = "CCO",  //Check City Online
                NoteType = "Emailed",
                EmployeeRole = EmployeeType.Manager,
                WarningTypeAttention = "Attention"

            };
            return User;

        }

        public static CurrentUser GetQaEmployeeManager02()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Close = "Close",
                UserName = "Alien@nostromorp.com",
                Signature = "Elien Ripley",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "alien",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.Manager
            };
            return User;

        }

        public static CurrentUser GetQaEmployeeSupervisor01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "goodfellas2@nostromorp.com",
                Signature = "Tommy Devito",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "goodfellas",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.Supervisor
            };
            return User;

        }

        public static CurrentUser GetQaEmployeeAdvanceCustomerService01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "goodfellas3@nostromorp.com",
                Signature = "Billy Batts",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Password = "goodfellas",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.AdvanceCustomerService
            };
            return User;

        }
        public static CurrentUser GetEmployeeMonitor01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "monitor",
                Signature = "Billy Batts",
                Close = "Close",
                Password = "ccomonitor",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.Monitor
            };
            return User;

        }

        public static CurrentUser GetQaEmployeeCustomerService01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "goodfellas4@nostromorp.com",
                Signature = "Henry Hill",
                Password = "goodfellas",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                LocationId = "CCO",  //Check City Online
                EmployeeRole = EmployeeType.CustomerServiceRep,
                WarningType = "Attention",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "Application Expired",
                NoteTypeDebt = "Debtor Notes",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                InvalidateLoanActionNote = "Invalidating this note",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                AdverseActionCustomerId = "U10-0006460"
            };
            return User;
        }

        public static CurrentUser GetQaEmployeeCustomerService02()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "Goodfellas5@nostromorp.com",
                Signature = "Frankie Carbone",
                NoteType = "Emailed",
                Note = "Happy test validate",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                ReassignToUser = "Ripley, Ellen",
                WarningType = "Attention",
                WarningNote = "I am a note and I like automation",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                NoteReason = "Application Expired",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                CancellationNote = "expired loan",
                PaymentTypePayDown = "Pay Down",
                InvalidateLoanActionNote = "Invalidating this note",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                Password = "goodfellas",
                NewPassword = "goodfellas2",
                ConfirmPassword = "goodfellas2",
                OverrideUsername = "qa00@nostromorp.com",
                OverridePassword = "alien",
                SearchByEmail = "qa1@nostromorp.com",
                LocationId = "CCO",
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93701",
                BankType = "Checking",
                TransactionDate = "PM",
                NoteTypeDebt = "Debtor Notes",
                BankAccountOpenDate = "06/22/1979",
                EmployeeRole = EmployeeType.CustomerServiceRep,
                AdverseActionCustomerId = "U10-0006460",

            };
            return User;
        }

        public static CurrentUser GetStageEmployeeOwner01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                UserName = "webadmin@checkcity.com",
                Signature = "Web Admin",
                Close = "Close",
                Password = "softwise",
                LocationId = "UTO",  //Check City Online advanced cs rep
                EmployeeRole = EmployeeType.Owner
            };
            return User;
        }

        public static CurrentUser GetCollectionsStageEmployeeOwner03()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                UserName = "Hobbit@nostromorp.com",
                Signature = "Thorin Armitage",
                Close = "Close",
                Password = "hobbit",
                LocationId = "CMS",  //Check City Online advanced cs rep
                EmployeeRole = EmployeeType.Owner
            };
            return User;
        }

        public static CurrentUser GetStageEmployeeManager01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien05@nostromorp.com",
                Signature = "Eddie  Powell",
                Password = "alien",
                ReasonType = "Application Expired",
                Close = "Close",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                LocationId = "UTO",  //Check City Online advanced cs rep
                EmployeeRole = EmployeeType.AdvanceCustomerService
            };
            return User;
        }

        public static CurrentUser GetStageEmployeeSupervisor01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                Close = "Close",
                UserName = "alien06@nostromorp.com",
                Signature = "Jerry  Goldsmith",
                Password = "alien",
                LocationId = "UTO",  //Check City Online advanced cs rep
                EmployeeRole = EmployeeType.Supervisor
            };
            return User;
        }

        public static CurrentUser GetStageEmployeeCustomerService01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien03@nostromorp.com",
                Signature = "Ash Holm",
                NoteType = "Emailed",
                Close = "Close",
                NoteReason = "Application Expired",
                CancellationNote = "expired loan",
                Note = "Happy test validate",
                ReassignToUser = "Skerrit, Dallas",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                WarningType = "Update Email Address",
                WarningNote = "Do not loan",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                Password = "alien",
                NewPassword = "aliens",
                ConfirmPassword = "aliens",
                SearchByEmail = "em:qa446@nostromorp.com",
                OverrideUsername = "qa000@nostromorp.com",
                OverridePassword = "alien",
                LocationId = "CCO",
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93700",
                BankType = "Checking",
                BankAccountOpenDate = "06/06/1996",
                TransactionDate = "PM",
                NoteTypeDebt = "Debtor Notes",
                EmployeeRole = EmployeeType.CustomerServiceRep,
                AdverseActionCustomerId = "U10-0006460"
            };
            return User;
        }
        public static CurrentUser GetStageEmployeeAdvanceCustomerService01()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                UserName = "alien01@nostromorp.com",
                Signature = "Dallas Skerritt",
                Password = "alien",
                Close = "Close",
                LocationId = "UTO",  //Check City Online advanced cs rep
                EmployeeRole = EmployeeType.AdvanceCustomerService,
                AdverseActionCustomerId = ""
            };
            return User;
        }
        public static CurrentUser GetStageEmployeeAdvanceCustomerService02()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "alien04@nostromorp.com",
                Signature = "Kane Hurt",
                Password = "alien",
                NoteType = "Emailed",
                Close = "Close",
                ReasonType = "Application Expired",
                ReasonTypeB = "Unable to Verify Address",
                ReasonTypeC = "Withdrawn by User",
                NoteReason = "Application Expired",
                CancellationNote = "expired loan",
                Note = "Happy test validate",
                ReassignToUser = "Baker, Alex",
                WorkState = "Unassign loan",
                PaybackMethod = "Card",
                WarningType = "Update Email Address",
                WarningNote = "Do not loan",
                RejectNoteType = "Called",
                RejectAddNote = "Information isn't sufficient. ",
                InvalidateLoanActionNote = "Invalidating this note",
                CustomerDetailNoteType = "Called",
                CustomerDetailNote = "This is for Automation",
                PaymentTypePayDown = "Pay Down",
                PayDownAmount = "25",
                PaymentTypePayoff = "Pickup",
                PaymentTypeExtend = "Extend",
                PaymentMethodDebit = "Bank Card",
                PaymentMethodAch = "ACH",
                TotalPayment = "50",
                NewPassword = "aliens",
                ConfirmPassword = "aliens",
                SearchByEmail = "em:qa456@nostromorp.com",
                OverrideUsername = "qa000@nostromorp.com",
                OverridePassword = "alien",
                MountainAmerica = "324079555",
                NewBankAccountNumber = "93700",
                BankType = "Checking",
                BankAccountOpenDate = "06/06/1996",
                LocationId = "UTO",  //Check City Online
                EmployeeRole = EmployeeType.AdvanceCustomerService,
                AdverseActionCustomerId = ""
            };
            return User;
        }
      
        public static void GetAvailableEmployees()
        {
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeOwner01());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeOwner02());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeManager01());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeManager02());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeSupervisor01());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeAdvanceCustomerService01());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeCustomerService01());
                    EmployeeManager.CheckCityEmployees.Add(GetQaEmployeeCustomerService02());
                    break;
                case SystemUnderTest.STAGE:
                    EmployeeManager.CheckCityEmployees.Add(GetStageEmployeeOwner01());
                    EmployeeManager.CheckCityEmployees.Add(GetStageEmployeeManager01());
                    EmployeeManager.CheckCityEmployees.Add(GetStageEmployeeSupervisor01());
                    EmployeeManager.CheckCityEmployees.Add(GetStageEmployeeAdvanceCustomerService02());
                    EmployeeManager.CheckCityEmployees.Add(GetStageEmployeeCustomerService01());
                    break;
            }   
        }

        internal static Users.CurrentUser GetEmployee(EmployeeType employeeRole)
        {
            List<Users.CurrentUser> availableEmployee = new List<CurrentUser>();
            foreach (Users.CurrentUser employee in EmployeeManager.CheckCityEmployees)
            {
                if (employee.EmployeeRole == employeeRole && employee.RunEnvironment == TestConfigManager.Config.SystemUnderTest.ToString())
                {
                    availableEmployee.Add(employee);
                }
            }
            var random = new Random(Guid.NewGuid().GetHashCode());
            if(availableEmployee.Count > 0)
                return availableEmployee[random.Next(0, availableEmployee.Count )];
            else
                return availableEmployee[0];
        }

        public class CurrentUser
        {
            public string AdverseActionCustomerEmail { get; set; }
            public string AdverseActionCustomerId { get; set; }
            public string TransactionDate { get; set; }
            public string NewBankAccountNumber { get; set; }
            public string InvalidateLoanActionNote { get; set; }
            public string PaymentMethodDebit { get; set; }
            public string PaymentMethodAch { get; set; }
            public string BankAccountOpenDate { get; set; }
            public string BankType { get; set; }
            public string MountainAmerica { get; set; }
            public string PaybackMethod { get; set; }
            public string CustomerDetailNoteType { get; set; }
            public string CustomerDetailNote { get; set; }
            public string SearchByEmail { get; set; }
            public string OverrideUsername { get; set; }
            public string NoteTypeDebt { get; set; }
            public string OverridePassword { get; set; }
            public string ConfirmPassword { get; set; }
            public string RejectNoteType { get; set; }
            public string RejectAddNote { get; set; }
            public string ReassignToUser { get; set; }
            public string WarningNote { get; set; }
            public string WarningType { get; set; }
            public string WorkState { get; set; }
            public string NoteReason { get; set; }
            public string ReasonType { get; set; }
            public string ReasonTypeB { get; set; }
            public string ReasonTypeC { get; set; }
            public string CancellationNote { get; set; }
            public string Note { get; set; }
            public string RunEnvironment { get; set; }
            public string NoteType { get; set; }
            public string UserName { get; set; }
            public string LocationId { get; set; }
            public string PhoneNumber { get; set; }
            public string Address { get; set; }
            public string State { get; set; }
            public string City { get; set; }
            public string ZipCode { get; set; }
            public string ReferralEmail { get; set; }
            public string UpdatedEmail { get; set; }
            public string PaymentTypePayDown { get; set; }
            public string PayDownAmount { get; set; }
            public string PaymentTypePayoff { get; set; }
            public string PaymentTypeExtend { get; set; }
            public string TotalPayment { get; set; }
            public string PayoffDate { get; set; }
            public string Signature { get; set; }
            public string CascadesBank { get; set; }
            public string Close { get; set; }

            public EmployeeType EmployeeRole { get; set; }
            public string Password { get; set; }
            public string BirthDate { get; set; }
            public string NewPassword { get; set; }
            public string SecurityQuestion1 { get; set; }
            public string SecurityQuestion2 { get; set; }
            public string SecurityQuestion3 { get; set; }
            public string SecurityQuestion4 { get; set; }
            public string WarningTypeAttention { get; set; }
        }
    }
}
