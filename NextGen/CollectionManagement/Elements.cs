﻿using System;
using System.CodeDom;
using System.ComponentModel.Design.Serialization;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Web.UI.WebControls;
using NUnit.Framework;
using OpenQA.Selenium;
using RazorEngine.Compilation.ImpromptuInterface;

namespace CollectionManagement
{
    internal class Elements
    {
        internal class LeftMenu
        {
            internal class HamburgerMenu
            {
                public const string ClickHamburgerMenu = "//*[text()='menu']";

            }

            internal class HamburgerMenuOptions
            {
                public const string CustomerDashboard = "//*[text()= 'Customer Dashboard']";
                public const string ApprovedDashboard = "//*[text()= 'Approved Dashboard']";
                public const string PendingDashboard = "//*[text()= 'Pending Dashboard']";
                public const string ApplicationDashboard = "//*[text()= 'Application Dashboard']";
                public const string LoanInventory = "//*[text()=  'Loan Inventory']";
                public const string Admin = "//*[text()= 'Admin']";
                public const string CollectionsAdminPage = "//*[text()='Admin']";

            }
        }

        internal class TemporaryPasswordsPopup
        {
            internal class ScreenText
            {
                public const string TemporaryPasswords = "//*[text()='Temporary Passwords']";
            }
            internal class PasswordList
            {
                public const string ListOfPasswords = "//*[contains(@class,'matcell-description ng-star-inserted')]";
                public const string firstTempPassword = "//*[contains(@class,'matcell-description ng-star-inserted')]//following::tr[1]";
            }

            internal class Buttons
            {
                public const string close = "//*[text()='Close']";
            }
        }
        internal class LoginPage
        {
            internal class Input
            {
                public const string UserName = "//*[@id='mat-input-0']";
                public const string Password = "//*[contains(@id,'mat-input')][@type='password']";
            }

            internal class Button
            {
                public const string Login = "//span[text()='Login']";
                public const string LoginFail = "//span[text()='Logins']";
                public const string MonitorIcon = "//mat-icon[contains(text(),'tv')]";
            }

            internal class LocationPopup
            {
                public const string Title = "//div[text()='Select Location']";
                public const string CheckCityOnlineId = "//*[text()='CCO']";
                public const string CloseButton = "//span[contains(text(),'Close')]";
            }

            internal class Message
            {

            }

            internal class ErrorMessage
            {
                public const string FailToLogin = "//div[@class='error-message ng-star-inserted']";
            }
        }

        
        internal class AdminPage
        {

            internal class ScreenText
            {
                public const string AdminPageDashboard = "//div[contains(text(), 'Dashboard')]";
            }

            internal class Buttons
            {
                public const string ActionSettings = "//*[text()='Action Settings']";
                public const string About = "//span[contains(text(),'About')]";
                public const string UserManagement = "//span[contains(text(), 'User Management')]";
                public const string QueueManagement = "//span[contains(text(),'Queue Management')]";
                public const string UserRolesManagement = "//span[contains(text(), 'User Roles Management')]";
                public const string EnterpriseSettings = "//*[text() = 'Enterprise Settings']";
                public const string DocumentManagement = "//*[text() = 'Document Management']";
                public const string SystemMaintenance = "//span[contains(text(), 'System Maintenance')]";
                public const string TypeManagement = "//*[text()= 'Type Management']";
                public const string SecuritySettings = "//*[text()='Security Settings']";
                public const string PersonPopUp = "//*[text()= 'person']";
                public const string Logout = "//*[text()= 'logout']";
                public const string Yes = "//*[text()= 'Yes']";
                public const string AdverseActionSettings = "//*[text()='Adverse Action Settings']";
                public const string WorkflowManagement = "//*[text()= 'Workflow Management']";
                public const string LoanTypeManagement = "//*[text()= 'Loan Type Management']";
                public const string Sites = "//*[text()='Sites']";
                public const string DeResponseMessages = "//*[text()= 'DE Response Messages']";
                public const string SystemConfiguration = "//*[text()= 'System Configuration']";
                public const string CollectionsManagement = "//*[text()= 'CMS']";
                public const string CollectionsUserManagement = "//*[text()= 'User Management']";
                public const string TemporaryPasswords = "//*[text()= 'Temporary Passwords']";
                public const string Communications = "//*[text()='Communications']";
                public const string CollectionsDocumentRequest = "//*[text()= 'Document Request']";
                public const string EmployeeSchedules = "//*[text()= 'Employee Schedules']";

            }
            

            internal class SecuritySettings
            {
                public const string ScreenText = "//*[text()='Security Settings']";
            }
            internal class EnterpriseSettings
            {
                public const string ScreenText = "//*[contains(text(), 'Enterprise Settings')]";

                internal class Buttons
                {
                    public const string Save = "//*[text()= 'Save']";
                    public const string Cancel = "//*[text()= 'Cancel']";
                    public const string ExpirationDate = "//*[contains(@id, 'mat-input')]";
                }
            }

            internal class Sites
            {
                public const string ScreenText = "//span[contains(text(),'Sites')]";

            }

            internal class DocumentManagement
            {
                public const string ScreenText = "//*[contains(text(), 'Document Management')]";

                internal class Buttons
                {
                    public const string Tokens = "//*[text()='Tokens']";
                    public const string Templates = "//*[text()='Templates']";
                    public const string GoogleTtsTemplate = "//*[text()= 'Google TTS Template']";
                    public const string Categories = "//*[text()='Categories']";
                    public const string StaticTemplates = "//*[text()= 'Static Templates']";
                    public const string Queries = "//*[text()='Queries']";
                }

                internal class Tokens
                {
                    public const string ScreenText = "//*[text()=' Tokens']";
                }

                internal class Templates
                {
                    public const string ScreenText = "//*[text()=' Templates']";
                }

                internal class GoogleTtsTemplate
                {
                    public const string ScreenText = "//*[text()= 'Google TTS Template']";
                }

                internal class Categories
                {
                    internal class Buttons
                    {
                        public const string PlusButton = "//mat-icon[contains(text(),'add')]";
                        public const string ContextMenu = "//tr[1]//td[4]//span[1]//dis-tablemenu[1]//div[1]//button[1]//span[1]//mat-icon[1]";
                    }

                    internal class InputFields
                    {
                        public const string SearchByField = "";
                    }

                    internal class EditPage
                    {
                        internal class InputFields
                        {
                            public const string Name = "//input[contains(@id,'mat-input-')]";
                            public const string Description = "//input[contains(@id,'mat-input-')]//following::input";
                        }

                        internal class Buttons
                        {
                            public const string Save = "//*[text()='Save']";
                            public const string Cancel = "//*[text()= 'Cancel']";
                        }
                    }

                }
            }
            internal class UserManagement
            {
                public const string ScreenText = "//*[contains(text(), 'User Management')]";
            }

            internal class TypeManagement
            {
                internal class ScreenText
                {
                    public const string TypeManagement = "//*[text()='Type Management']";

                }

                internal class Buttons
                {
                    public const string WarningTypes = "//*[text()='Warning Types']";
                    public const string NoteTypes = "//*[text()='Note Types']";
                    public const string ApplicationWithdrawReasons = "//*[text()='Application Withdraw Reasons']";
                    public const string ApplicationWorkState = "//*[text()='Application Work State']";
                    public const string AttachmentTypes = "//*[text()='Attachment Types']";
                    public const string StateTimeZoneMappings = "//*[text()= 'State Time Zone Mappings']";
                }

                internal class AttachmentTypes
                {
                    public const string ScreenText = "//*[text()='Attachement Types']";
                    
                }
                internal class WarningTypesPage
                {
                    internal class ScreenText
                    {
                        public const string WarningTypes = "//*[text()='Warning Types']";
                    }
                }

                internal class NoteTypesPage
                {
                    internal class ScreenText
                    {
                        public const string NoteTypes = "//*[text()='Note Types']";
                    }
                }

                internal class ApplicationWorkStatePage
                {
                    internal class ScreenText
                    {
                        public const string ApplicationWorkState = "//*[text()='Application Work States']";
                    }
                }

                internal class ApplicationWithdrawReasonsPage
                {
                    internal class ScreenText
                    {
                        //this will break once they fix the bread crumb error on this change to //*[text()='Application Withdraw Reason']
                        public const string ApplicationWithdrawReason =
                            "//div[@class='title-section'][contains(.,'Application Withdraw Reasons')]";
                    }
                }


            }

            internal class SystemConfiguration
            {
                public const string ScreenText = "//*[text()= 'System Configuration']";
            }
            internal class SystemMaintenancePage
            {
                public const string ScreenText = "//*[text()= ' System Maintenance']";

                internal class Buttons
                {
                    public const string LocationMaintenance = "//*[text()='Location Maintenance']";
                    public const string ApplicationMaintenance = "//*[text()='Application Maintenance']";
                }

                internal class ApplicationMaintenancePage
                {
                    public const string ScreenText = "//*[text()='Application Maintenance']";
                }

                internal class LocationMaintenancePage
                {
                    public const string ScreenText = "//*[text()='Location Maintenance']";

                }
            }
            internal class QueueManagementPage
            {
                internal class ScreenText
                {
                    public const string QueueManagement = "//*[text()=' Queue Management']";
                    public const string QueueSettings = "//*[text()='Queue Settings']";
                    public const string QueuePriority = "//*[text()=' Priority']";
                }


                internal class Buttons
                {
                    public const string Settings = "//*[text()= 'Settings']";
                    public const string Priority = "//*[text()= 'Priority']";
                }
            }

            internal class UserRolesManagement
            {
                public const string ScreenText = "//*[text()='User Roles Management']";
            }

            internal class LoanTypeManagement
            {
                public const string ScreenText = "//*[text()=' Loan Type Management']";

            }

            internal class WorkflowManagement
            {
                public const string ScreenText = "//*[text()='Workflow Management']";
            }

        }



        internal class PendingDashboardPage
        {
            internal class Buttons
            {
                public const string WorkNext = "//*[text()='Work Next']";
                public const string AgentToolsIcon = "//*[text()= 'person']";
                public const string ChangePassword = "//*[contains(@id,'cdk-overlay')]  //*[text()=' Change ManagerPassword']";
                public const string Logout = "//*[text()= ' Logout']";
                public const string YesToLogout = "//*[text()='Yes']";
                public const string Close = "//*[text()='Close']";
                
            }

            internal class SocialSecurity
            {
                public const string SocialSecNumber = "//td[@class='mat-cell cdk-column-ssn mat-column-ssn ng-star-inserted']";
            }

            internal class CustomerRows
            {
                public const string Ssn = "//*[contains(@class,'ng-star-inserted')]//*[contains(@class,'cdk-column-ssn')]";

            }
            internal class State
            {
                public const string StateDropDown = "//*[contains(@class,'select-block ng-select ng-select-single ng-select-searchable ng-select-clearable ng-untouched ng-pristine ng-valid')]//following::input[2]";
                public const string StateDropdownPh = "";
                public const string StateDropdownField = "//*[@class='ng-value-container']//following::input[2]";
                public const string SelectWY = "//span[@class='ng-option-label ng-star-inserted'][contains(text(),'WY')]";
                public const string SelectUT = "//span[@class='ng-option-label ng-star-inserted'][contains(text(),'UT')]";
                public const string SelectTx = "//span[@class='ng-option-label ng-star-inserted'][contains(text(),'TX')]";
            }
            internal class LoanTypeDropDown
            {
                public const string LoanType = "//span[@class='ng-arrow-wrapper']";
                public const string DeferredLoanType = "//*[text()='DEF']";
                public const string InstallLoanType = "//*[text()='INS']";
            }
            internal class DropDown
            {
                public const string ItemsPerPage = "//*[contains(@class,'mat-select-arrow-wrapper ng-tns-c108-')]";
                public const string ItemsMin = "//span[@class='mat-option-text'][contains(text(),'25')]";
            }
            internal class ScreenText
            {
                public const string Text = "//div[@class='title-section'][contains(.,'Pending Dashboard')]";
            }
            internal class CustomerId
            {
                public const string CustomerIdClipBoard = "//mat-icon[contains(text(),'assignment')][1]";
            }

            internal class SearchField
            {
                public const string Search = "//*[contains(@id,'mat-input')]";
            }
            internal class UnmaskSsnPopup
            {
                public const string ScreenText = "//*[text()='Unmask SSN']";

                internal class Buttons
                {
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string Ok = "//*[text()='OK']";
                }

                internal class InputFields
                {
                    public const string ManagerUsername = "//*[@type= 'text']//following::input[3]";
                    public const string ManagerPassword = "//*[contains(@class,'dialog-block')]//input[@type='password']"; // PatEdit 5/14/2020
                }
            }
        }

        internal class LoanInventoryPage
        {
            public const string BreadCrumb = "//div[text()= 'Loan Inventory']";

            internal class State
            {
                
                public const string StateDropDownMenu = "//*[contains(@class,'select-block ng-select ng-select-single ng-select-searchable ng-select-clearable ng-untouched ng-pristine ng-valid')]//following::input[1]";

                internal class StateOptions
                {
                    public const string Utah = "//*[text()='UT'][@class='ng-option-label ng-star-inserted']";
                    public const string Alabama = "//*[text()= 'AL']";
                    public const string California = "//*[text()= 'CA']";
                    public const string Idaho= "//*[text()='ID'][@class='ng-option-label ng-star-inserted']";
                    public const string Illinois = "//*[text()= 'IL']";
                    public const string WashingtonDc = "//*[text()= 'DC']";
                    public const string Arizona = "//*[text()= 'AZ']";
                    public const string Arkansas = "//*[text()= 'AR']";
                    public const string Colorado = "//*[text()= 'CO']";
                    public const string Florida = "//*[text()= 'FL']";
                    public const string Wyoming = "//*[text()='WY'][@class='ng-option-label ng-star-inserted']";
                    public const string Missouri = "//*[text()='MO'][@class='ng-option-label ng-star-inserted']";
                    public const string Texas = "//*[text()='TX'][@class='ng-option-label ng-star-inserted']"; // Pat edit



                }
                
                
            }
                internal class LoanStatus
                {

                    public const string LoanDropDownMenu = "//*[@class='ng-arrow-wrapper']";
                    internal class LoanStatusMenuOptions
                    {
                        public const string Funded = "//*[text()='Funded'][@class='ng-option-label ng-star-inserted']";
                        public const string Approved = "//*[text()='Approved'][@class='ng-option-label ng-star-inserted']";
                        public const string Defaulted = "//*[text()='Defaulted']";
                        public const string Closed = "//*[text()='Closed'][@class='ng-option-label ng-star-inserted']";
                        public const string Late = "//*[text()='Late'][@class='ng-option-label ng-star-inserted']";
                        public const string Rescinded = "//*[text()='Rescinded'][@class='ng-option-label ng-star-inserted']";


                    }

                    internal class InputFields
                    {
                        public const string LoanStatus = "//*[@class='ng-input']";
                        public const string State = "";
                    }
                   
                }

                internal class Links
                {
                    public const string CustomerName = "//*[@class='matcell-description ng-star-inserted']//*[@class='ng-star-inserted']//following::span[8]";

                    public const string CustomerId =
                        "//*[@class='matcell-description ng-star-inserted']//*[@class='ng-star-inserted']//following::span[6]";
                }

                internal class Clipboards
                {
                    public const string CustomerIdClipBoard = "//*[@class='matcell-description ng-star-inserted']//*[@class='ng-star-inserted']//following::span[4]";
                    public const string LoanIdClipBoard = "//*[@class='matcell-description ng-star-inserted']//*[@class='ng-star-inserted']//following::span[1]";
                }

                internal class InputFields
                {
                    public const string LoanIdField = "//*[@class='ng-untouched ng-pristine ng-valid ng-star-inserted']";
                    public const string CustomerIdField = "//*[@class='ng-untouched ng-pristine ng-valid ng-star-inserted']//following::input[1]";

                }

                internal class ExportFile
                {
                    public const string ExportFileLink = "//*[text()='Export file']";
                }
        }

        internal class PendingDashboardTable
        {
            public const string ApplicationId = "//*//tr/td[6]/div/span";
            public const string ApplicationRow = "//cleo-dashboard/div/sw-table/div/div[2]/table/tbody/tr";
            public const string ApplicationRowPartial = "//cleo-dashboard/div/sw-table/div/div[2]/table/tbody/tr[";
            public const string ApplicationIdPartial = "/td[6]/div/span";
            public const string IdClipboard = "//*[contains(@class,'ng-star-inserted')]//*[contains(text(),'assignment')]";
        }

        internal class ChangeYourPasswordPopUp
        {

            public const string CurrentPassword =
                "//*[contains(@label, 'Current ManagerPassword')]//*[contains(@class, 'mousetrap')]";

            public const string NewPassword = "//*[@label= 'ManagerPassword']//*[contains(@class, 'mousetrap')]";

            public const string ConfirmPassword =
                "//*[contains(@label, 'Confirm ManagerPassword')]//*[contains(@class, 'mousetrap')]";

            public const string ChangeButton = "//*[text()='Change']";
        }

        internal class PendingDetailPage
        {
            internal class ScreenText
            {
                public const string PendingDetail = "//*[text()= 'Pending Detail']";
                //public const string ApplicationId = "//*[contains(@class,'ng-star-inserted')]//*[contains(@class,'cdk-column-id')]";
                public const string SocialSecurityNumber = "//*[@class= 'clickable']";
                // //div[contains(text(),'Application')]//../following-sibling::div
                public const string ApplicationId = "//div[@class='customer-detail-block']";
            }

            internal class Buttons
            {
                public const string ValidateId ="//*[@class= 'title-line'] [text()='Validate ID']";//"(//div[contains(@ng-reflect-klass,'nav-block pending')])[1]"; // "//*[@class= 'title-line'] [text()='Validate ID']";
                public const string ValidateIncome = "//span[contains(text(),'Validate Income')]"/*"//span[contains(.,'Validate Income')]"*/ /*"//*[@class= 'title-line'] [text()='Validate Income']"*/;
                public const string ValidateAddress = "//span[contains(text(),'Validate Address')]";/*"//span[contains(.,'Validate Address')]";*/ /*"//*[@class= 'title-line'] [text()='Validate Address']*/
                public const string ValidatePhone = "//span[contains(text(),'Validate Phone')]";/*"//span[contains(.,'Validate Phone')]";*/ /*//*[@class= 'title-line'] [text()='Validate Phone']"*/
                public const string ValidateBank = "//span[contains(text(),'Validate Bank')]"; /*"//span[contains(text(),'Validate Bank')]";*/ /*"//*[@class= 'title-line'] [text()='Validate Bank Account']";*/
                public const string ProcessLoan = "//*[text()='Process Loan']";
                public const string ValidateSSN = "//*[text()='Validate SSN/ITIN']";
                public const string BackToDashboard = "//*[text()= 'Back to Dashboard']";
                public const string ValidatePreviousLoan = "//*[text()='Validate Previous Loan']";
}

            internal class LoanWorkItems
            {
                public const string Id = "//*[@class= 'title-text'] [text()='ID']";
                //public const string ID = "//div[@class='nav-block completed']//div[@class='title-block']";
                public const string Income = "//*[@class= 'title-text'] [text()='Income']";
                public const string Address = "//*[@class= 'title-text'] [text()='Address']";
                public const string Phone = "//*[@class= 'title-text'] [text()='Phone']";
                public const string Bank = "//span[contains(text(),'Bank')]";

            }

            internal class WorkItemsToDo
            {
                public const string IdToDo = "//mat-icon[contains(text(),'call_received')]";
            }
            
            internal class Notes
            {
                internal class Inputs
                {
                    
                    public const string NoteType = "//*[@role='combobox'][@type='text']";
                    //public const string Note = "//textarea[@class='tts-body ng-pristine ng-invalid ng-touched'][@ng-reflect-name='note']";
                    public const string Note =
                        "//*[@formcontrolname='note'][@ng-reflect-name='note']";
                }

                internal class Buttons
                {
                    public const string Save = "//*[text()='Save']";
                    public const string Cancel = "//*[text()='Cancel']";
                }
            }
            internal class OptionsOnWorkItems
            {
                public const string HistoricalAccounts = "//*[text()='Historical Accounts']";
            }
            internal class RejectLoanActions
            {
                public const string RejectId = "//*[text()='Reject ID']";
                public const string RejectIncome = "//*[text()='Reject Income']";
                public const string RejectAddress = "//*[text()='Reject Address']";
                public const string RejectPhone = "//*[text()='Reject Phone']";
                public const string RejectBank = "//*[text()='Reject Bank Account']";
                public const string WorkFlowFirstNav = "//cleo-workflow-nav[1]";
            }

            internal class InvalidateLoanActions
            {
                public const string InvalidateId = "//*[text()='Invalidate ID']";
                public const string InvalidateIncome = "//*[text()='Invalidate Income']";
                public const string InvalidateAddress = "//*[text()='Invalidate Address']";
                public const string InvalidatePhone = "//*[text()='Invalidate Phone']";
                public const string InvalidateBank = "//*[text()='Invalidate Bank']";
                public const string InvalidatePreviousLoan = "//*text()= 'Invalidate Previous Loan' ";
            }

            internal class PleaseAddNotePopUp
            {
                public const string RejectNoteType = "//div[@class='ng-select-container ng-has-value']//input";
                public const string RejectAddNote = "//textarea[@placeholder='Add note..']";
                public const string InvalidateAddNote = "//textarea[contains(@formcontrolname,'note')][@rows='4']";
                public const string RejectSaveNote = "//span[@class='title-line'][contains(.,'Save Note')]";
            }

            internal class UnmaskSsnPopup
            {
                public const string ScreenText = "//*[text()='Unmask SSN']";
                
                internal class Buttons
                {
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string Ok = "//*[text()='OK']";
                }

                internal class InputFields
                {
                    public const string ManagerUsername = "//*[contains(@class,'mat-form-field-infix')]//*[@type= 'text']";
                    public const string ManagerPassword = "//*[contains(@class,'mat-form-field-infix')]//*[@type= 'password']";
                }
            }
            internal class LoanValidationPopUp
            {
                public const string NoteTypeMainPendingDetailPage = "//*[@class='ng-input']//input";
                public const string NoteTypeInput = "//div[@class='ng-input']//following::input";
                public const string NoteTypeInputOnInvalidate = "//div[@class='ng-input']//following::input[2]";
                public const string NoteType = "//div[@class='ng-select-container ng-has-value']//input";
                //     //*[contains(@role,'option')][2]
                public const string SaveNote = "//span[@class='title-line'][contains(.,'Save Note')]";
                public const string Cancel = "//*[text()= 'Cancel']";
                //public const string AddNote = "//textarea[@class='tts-body ng-pristine ng-invalid ng-touched'][@ng-reflect-name='note']";
                public const string AddNote = "//*[@formcontrolname='notes']";
                public const string AddNotePendingDetailMain = "//*[@formcontrolname='note']";
                public const string CancelBtn = "//*[@id='mat-dialog-0']//dis-button[2]/div/button";

                
            }

            internal class ApplicationStatusPopUp
            {
                public const string Acknowledge = "//*[text()='Acknowledge']";
            }

            internal class PasswordResetRequestPopup
            {
                public const string Acknowledged = "//*[text()='Acknowledged']";
                public const string AcknowledgedFailCase = "//*[text()='Acknowledge']";
            }

            internal class RightMenu
            {
                internal class HamburgerMenu
                {
                    public const string ClickHamburgerMenu = "//cleo-workflow-title//button//span/mat-icon";
                }

                internal class HamburgerMenuOptions
                {
                    public const string Edit = "//button[contains(text(),'Edit')]";
                    public const string ReassignApplication = "//button[contains(text(),'Reassign Application')]";
                    public const string Warnings = "//button[contains(text(),'Warnings')]";
                    public const string CancelApplication = "//button[contains(text(),'Cancel Application')]";
                    public const string SetWorkState = "//button[contains(text(),'Set Work State')]";
                    public const string ResetPassword = "//button[contains(text(),'Reset Password')]";
                }

                internal class CancelApplication
                {
                    public const string ReasonType = "/html/body/div[2]/div[2]/div/mat-dialog-container/cleo-workflow-withdraw-dialog/div/div/div/div[1]/sw-select/sw-input-wrapper/div/div/div/ng-select/div/div/div[2]/input";
                    public const string CancelNotes = "//*[@formcontrolname='notes']";

                }

                internal class Buttons
                {
                    public const string Withdraw = "//*[text()= 'Withdraw']";
                    public const string Cancel = "//*[text()= 'Cancel']";
                }

                internal class SetWorkStatePopUp
                {
                    internal class Buttons
                    {
                        
                        public const string Assign = "//*[text()='Assign']";
                        public const string Cancel = "//*[text()='Cancel']";
                    }

                    internal class WorkStateReason
                    {
                        public const string UnassignLoan = "//*[contains(@id,'mat-dialog')]  //*[@role='combobox']";
                    }
                }

                internal class AddWarningPopUp
                {

                    public const string WarningType = "//div[@class='ng-input']//following::input[2]";

                    public const string WarningNote = "//*[@formcontrolname='notes']";

                    internal class WarningButtons
                    {

                        public const string AddWarning = "//*[contains(@id,'mat-dialog')]  //*[text()= 'add']";
                       // public const string Save = "//form[@class='ng-dirty ng-valid ng-touched']//span[@class='title-line'][contains(text(),'Save')]";
                        public const string Save = "//mat-dialog-container[1]//dis-button[2]";
                        public const string SaveFail = "//mat-dialog-containers[1]//dis-button[2]"; //forcefully the given xpath is wrong to pass script
                        public const string Cancel = "//*[text()='Cancel']";
                        public const string CancelSecondary = "(//span[@class='title-line'][contains(.,'Cancel')])[2]";
                        public const string Close = "//*[text()='Close']";
                    }
                }

                internal class ReassignToUserPopUp
                {

                    public const string AssignTo = "(//input[@role='combobox'])[2]";

                    internal class Buttons
                    {
                        public const string Cancel = "//*[text()='Cancel']";
                        //public const string Save = "//*[@id='mat-dialog-1']//dis-button[2]";
                        public const string Save = "//*[contains(@class,'dialog-block')]//*[contains(@class,'mat-button-wrapper')]//*[text()='Save']";  // PatEdit 5/14/20
                        public const string ClearAll = "//*[@title='Clear all']";
                    }

                }

            }

            internal class WarningPopUp
            {
                internal class OverrideInput
                {
                    public const string UserName =
                        "//div[contains(@class,'mat-form-field-infix ng-tns-c8')]//following::input[3]";

                    public const string Password =
                        "//div[contains(@class,'mat-form-field-infix ng-tns-c8')]//following::input[4]";
                }

                internal class OverrideButton
                {
                    public const string Delete = "//*[text()= 'Delete']";
                    public const string Close = "//*[text()= 'Close']";
                    public const string OverrideAll = "//*[text()='Override All']";
                }
            }

        }

        internal class CustomerDashboard
        {
            //public const string CustomerDashBoardPage = "//div[@class='content-container']";
            //public const string CustomerDashBoardPage = "//*[contains(text(),'Zayne-ghtggsfd@nostromorp.com')]";

           
            internal class Buttons
            {
                public const string ThreeDot = "//*/tr[1]/*/span/*//button/span/mat-icon";
                public const string EmailLink = "//*[text()='Email Approval Page Link']";
                public const string EmailHref = "//a[contains(@href, '')]";
                public const string Done = "//*[text()='Done']";
                public const string Close = "//*[text()='Close']";
                public const string Save = "(//span[@class='title-line'][contains(.,'Save')])[2]";
                public const string GoToButton = "//*/div[2]//tr[1]//button//mat-icon";
                public const string GoButton = "(//mat-icon[contains(.,'input')])[1]";
                public const string DeleteWarning = "//span[@class='title-line'][contains(.,'Delete Warning')]";
                public const string ConfirmDeleteWarning = "(//span[@class='title-line'][contains(.,'Delete')])[2]";
            }
            internal class ItemsPerPageDropDown
            {
                public const string ItemsPerPage = "//*[contains(@class,'mat-select-arrow-wrapper ng-tns-c108-')]";
                public const string ItemsMid = "//span[@class='mat-option-text'][contains(text(),'50')]";
                public const string ItemsMin = "//span[@class='mat-option-text'][contains(text(),'25')]";
            }
            internal class NameLink
            {
                public const string Firstname = "";
                public const string Lastname = "//*[text()='Approved']";
            }
            public const string SearchField = "//*[contains(@id,'mat-input')]";
            public const string Addbtn = "//span[@class='title-line'][contains(.,'Add ID')]";
            public const string CmbAddId = "//input[contains(@role,'combobox')]";
            public const string StateIssueBy = "(//input[contains(@role,'combobox')])[2]";
            public const string Expiration = "(//input[contains(@type,'text')])[10]";
            public const string AddIdIdNumber = "(//*[@id='cdk-accordion-child-5']//div[@class='mat-form-field-flex']//following-sibling::input)[6]";
            public const string AddIdState = "(//input[contains(@role,'combobox')])[2]";
            public const string AddIdExpiration = "//input[@id='mat-input-33']";
            public const string UnmaskSsn = "//*[@class= 'ng-star-inserted']//*[@role='gridcell'][11]";
            internal class ScreenText
            {
                public const string SocialSecurityNumber = "//*[text()='SSN']";
                
            }

            internal class UnmaskSsnPopup
            {
                public const string ScreenText = "//*[text()='Unmask SSN']";

                internal class Buttons
                {
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string Ok = "//*[text()='OK']";
                }

                internal class InputFields
                {
                    public const string ManagerUsernameCustomerDetailPage = "//*[contains(@class,'dialog-block')]//input[@type='text']";
                    public const string ManagerPasswordCustomerDetailPage = "//*[contains(@class,'dialog-block')]//input[@type='password']";
                    public const string ManagerUsername = "//*[@type='text']//following::input[10]";
                    public const string MangerPassword = "//*[contains(@class,'dialog-block')]//input[@type='password']"; //PatEdit 5/14/20
                }
            }
        }

        internal class ApprovedDashboard
        {
            public const string ScreenText = "//*[@class='title-section'][text()= 'Approved Dashboard']";
            public const string CustomerListRow = "//table/tbody/tr"; //table[@class='mat-table']/tbody/tr";
            public const string CustomerListTable = "//table";  //table[@class='mat-table']
            internal class ApplicationId
            {
                public const string Ida= "(//span[@class='ng-star-inserted'])[7]";
               // public const string Idb = "//span[contains(text(),'44195')]";
               public const string Ida2 = "(//span[@class='ng-star-inserted'])[60]";
            }

            internal class ConfirmApplicationWithdrawalPopup
            {
                internal class buttons
                {
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string OK = "//*[text()= 'OK']";
                }
            }
            internal class ContactEmail
            {
                public const string CopyEmail= "//input[@id='mat-input-9']";
                public const string PasteEmail = "//*[@id='mat-input-16']";
            }
            internal class ItemsPerPageDropDown
            {
                public const string ItemsPerPage = "//*[contains(@class,'mat-select-arrow-wrapper ng-tns-c108-')]";
                public const string ItemsMin = "//span[@class='mat-option-text'][contains(text(),'25')]";
            }

            internal class Input
            {
                public const string MiddleName = "//*[@id='mat-input-12']";
            }
            public const string DuplicateDataWarning = "//div[@class='title'][contains(.,'Duplicate Data Found - Email')]";
            public const string DuplicateSsnPopUp = "//div[@class='title'][contains(.,'Duplicate Data Found - SSN')]";
            internal class Buttons
            {
                public const string Withdraw = "//*[text()='Withdraw']";
                public const string ContextMenu = "//*/tr[1]/*/span/*//button/span/mat-icon";
                public const string ApprovalPageLink = "//*[text()='Email Approval Page Link']";
                public const string Done = "//*[text()='Done']";
                public const string Okay = "//span[@class='title-line'][contains(.,'OK')]";

                public const string PersonPopUp = "//*[text()= 'person']";
                public const string Logout = "//*[text()= 'logout']";
                public const string Yes = "//*[text()= 'Yes']";
                public const string SrcAppId = "(//span[@class='ng-star-inserted'])[199]";
                public const string DestAppId = "(//span[@class='ng-star-inserted'])[23]";
                public const string Ssn = "(//input[@type='text'])[4]";
                public const string Save = "//button[contains(.,'Save')]";
                public const string Ok = "//button[contains(.,'OK')]";


            }
            
        }

        internal class CollectionsAdminPage
        {
            internal class ScreenText
            {
                public const string AdminDashboard = "//*[text()='Admin Dashboard']";
                public const string TypeManagement = "//*[text()='Type Management']";
            }

            internal class Buttons
            {
                public const string UserManagement = "//*[text()='User Management']";
                public const string UserRolesManagement = "//*[text()='User Roles Management']";
                public const string Communications = "//*[text()='Communications']";
                public const string DocumentRequest = "//*[text()= 'Document Request']";
                public const string DebtPaymentTypes = "//*[text()= 'Debt Payment Types']";
                public const string DebtorPaymentPlans = "//*[text()= 'Debtor Payment Plan']";
                public const string TypeManagement = "//*[text()='Type Management']";
            }
            internal class TypeManagementPage
            {
                
                internal class Buttons
                {
                    public const string NoteTypes = "//*[text()='Note Types']";
                }

                internal class NoteTypesPage
                {
                    public const string ScreenText = "//*[text()='Note Types']";
                }
            }


        }

        

        internal class DocumentRequestPage
        {
            internal class Buttons
            {
                public const string DisclosureRequestGoTo = "//tbody/tr[1]/td[3]/span[1]/button[1]/span[1]/mat-icon[1]";
                public const string ApplicationRequestGoTo = "//tbody/tr[2]/td[3]/span[1]/button[1]/span[1]/mat-icon[1]";

            }
        }
        internal class CollectionsDashboard
        {
            internal class ScreenText
            {
                public const string CollectionsDashboard = "//*[text()='Collections Dashboard']";
            }

           
            internal class InputFields
            {
                public const string SearchBy = "//*[contains(@id,'mat-input')]";
                
            }

            internal class ClickAbleObjects
            {
                 
                public const string CustomerIdClipBoard = "";
                public const string PhoneNumberClipBoard = "";
                public const string EmailAddressClipBoard = "";
                public const string CustomerName = "";
                public const string SSN = "";
                public const string CustomerId = "//tr[1]//td[2]//div[1]";
            }

            internal class Buttons
            {
                public const string GoToButton = "//mat-icon[contains(text(),'input')]";

            }
            internal class ItemsPerPageDropDown
            {
                public const string ItemsPerPage = "//*[contains(@class,'mat-select-arrow-wrapper ng-tns-c103-')]";
                public const string ItemsMid = "//span[@class='mat-option-text'][contains(text(),'50')]";
                public const string ItemsMin = "//span[@class='mat-option-text'][contains(text(),'25')]";
            }
        }

        internal class CollectionsDebtorDetailPage
        {
            internal class Tabs
            {
                public const string Notes = "//*[text()='notes']";
                public const string Employment = "//*[text()='Employment']";
                public const string Loans = "//*[text()='Loans']";
                public const string DefaultedLoans = "//*[text()='Defaulted Loans']";
                public const string Attachments = "//*[text()='Attachments']";
                public const string Warnings = "//*[text()='Warnings']";
                public const string Bank = "//*[text()='Bank']";

            }

            internal class Notes
            {
                internal class DropDown
                {

                }

                internal class Buttons
                {

                }

                internal class NotesField
                {
                    public const string TopLoanInList = "//*[@class='table-container']//*[contains(@class,'mat-row cdk-row ng-star-inserted')]//following::tr[9]//td[position()=2]";
                }
            }
            internal class DefaultedLoans
            {
                internal class Buttons
                {
                    public const string Payment = "//*[text()= 'Payment']";
                }

                internal class DebtorValidationPopup
                {
                    public const string DebtorValidationBanner = "//*[text()='Debtor Validations']";
                    internal class Buttons
                    {
                        public const string Ok = "//*[text()='OK']";
                    }
                }
                internal class DebtorPaymentPopup
                {
                    internal class ScreenText
                    {
                        public const string DebtorPaymentBanner = "//*[text()='Debtor Payment']";
                    }
                    internal class DropDown
                    {
                        public const string AchPaymentMethod =
                            "//*[contains(text(), 'ACH')]";

                        public const string AchArrangedPayment = "//*[contains(text(), 'ACH Arranged Payment')]";
                        public const string PaymentMethod = "//div[@class='spacing-container select-block-spacing-container no-explanation no-icon with-label no-hint is-required']//span[@class='ng-arrow-wrapper']";
                    }

                    internal class InputFields
                    {
                        public const string PaymentAmountInputField = "//*[contains(@class,'mat-form-field-infix ng-tns-c8')]//*[contains(@id,'mat-input-')]//following::input[15]";
                        public const string Category = "//*[contains(@class,'mat-form-field-infix ng-tns-c8')]//*[contains(@id,'mat-input-')]//following::input[14]";
                    }

                    internal class CheckBoxes
                    {
                        public const string PersonalLoanCheckBox = "//mat-checkbox[contains(@id,'mat-checkbox-')]//following::mat-checkbox[7]";
                    }

                    internal class Buttons
                    {
                        public const string Cancel = "//*[text()='Cancel']";
                        public const string Save = "//button[@class='mat-focus-indicator mat-raised-button mat-button-base mat-primary ng-star-inserted']//span[@class='title-line'][contains(text(),'Save')]";
                        public const string CheckAll = "//*[text()='Check All']";
                        public const string UncheckAll = "//*[text()='Uncheck All']";
                        public const string Ok = "//*[text()='OK']";

                    }
                }
            }
        }
        internal class CustomerDetailPage
        {
            internal class ScreenText
            {
                public const string CustomerDetailFor = "//*[contains(text(), 'Customer Detail for')]";
                public const string WarningTypeNoEmail = "(//span[@class='ng-star-inserted'][contains(.,'Customer Does Not Have Email Address')])[4]";
                public const string WarningTypeFraudAlert = "(//span[@class='ng-star-inserted'][contains(.,'Potential Fraud Alert')])[2]";
                public const string WarningTypeSac = "(//span[contains(.,'Suspicious Activity Customer')])[2]";
                public const string WarningTypeAto = "(//span[contains(.,'Advanced Teller Override')])[2]";
                public const string WarningTypeDmo = "(//span[contains(.,'District Manager Override')])[2]";
                public const string WarningTypeManagerOverride = "(//span[contains(.,'Manager Override')])[6]";
                public const string WarningTypeTellerOverride = "(//span[contains(.,'Teller Override')])[6]";
                public const string LoanId = "//td[3]//span[@class='ng-star-inserted']/span[@class='ng-star-inserted']";
                public const string CustomerWarningTestNote = "//*[contains(text(), 'Customer Warning Test Note ==>')]";
            }

            internal class BlockWebAccessPopup
            {
                internal class Buttons
                {
                    public const string Yes = "//*[text()='Yes']";
                    public const string Cancel = "//*[text()= 'Cancel']";
                }
            }

            internal class DuplicateWarningPopup
            {
                public const string DuplicateDataWarning = "//div[@class='title'][contains(.,'Duplicate Data Found - Email')]";
                public const string DuplicateSsnPopUp = "//div[@class='title'][contains(.,'Duplicate Data Found - SSN')]";

                internal class Buttons
                {
                    public const string Okay = "//*[text()= 'OK']";
                    public const string Close = "//*[text()= 'Close']";
                }
            }
            internal class PrintApplicationPopup
            {
                internal class Buttons
                {
                    public const string Close = "//*[text()='Close']";
                    public const string EmailApplication = "//*[text()='Email Application']";
                    public const string PrintApplication = "//button[@class='mat-focus-indicator mat-raised-button mat-button-base mat-primary ng-star-inserted']//span[@class='title-line'][contains(text(),'Print Application')]";
                }
            }

            internal class Table
            {
                public const string TableRow = "//*[@id='cdk-accordion-child-0']//table/tbody/tr";
            }
            internal class SocialSecurity
            {
                public const string SocialSecNumber = "//span[@class='clickable']";
            }
            internal class AddWarningPopUp
            {

                public const string WarningType = "//*[contains(@id,'mat-dialog')]  //*[@role='combobox']";

                public const string WarningNote = "//*[@formcontrolname='notes']";

                internal class WarningButtons
                {
                    public const string Save = "//mat-dialog-container[1]//dis-button[2]";
                    public const string Close = "//*[text()='Close']";
                }
               
            }
            internal class UnmaskSsnPopup
            {
                public const string ScreenText = "//*[text()='Unmask SSN']";

                internal class Buttons
                {
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string Ok = "//*[text()='OK']";
                }

                internal class InputFields
                {
                    public const string ManagerUsernameCustomerDetailPage = "//*[contains(@class,'dialog-block')]//input[@type='text']";

                    public const string ManagerPasswordCustomerDetailPage =
                        "//*[contains(@class,'dialog-block')]//input[@type='password']";
                    public const string ManagerUsername = "//*[@type='text']//following::input[12]";
                    public const string ManagerPassword = "//*[@type= 'password']//input";
                }
            }
            internal class Buttons
            {
                public const string ThreeDotCustomerId = "(//*[@id='cdk-accordion-child-5']//button[contains(@class, 'mat-menu-trigger mat-icon-button mat-button-base mat-primary')])[2]";
                public const string ThreeDotDeleteCustomerId = "//*[@id='cdk-overlay-1']/div/div/span/button/span[contains(text(), 'Delete')]";
                public const string PrintApplication = "(//button[contains(.,'Print Application')])";
                public const string ResetPassword = "//*[text()='Reset Password']";
                public const string EmailAddressEnvelope = "//mat-icon[contains(text(),'email')]";
                public const String ViewOnlineCustomerDashboard = "//*[text()= 'View Online Customer Dashboard']";
                public const string Close = "";
                public const string BlockWebAccess = "//*[text()='Block Web Access']";
            }

            internal class CustomerDetailPageOptions
            {
                public const string Applications = "//*[text()='Applications']";
                public const string Loans = "//*[text()='Loans']";
                //public const string Notes = "//*[text()='Note']";
                public const string Notes = "//mat-icon[contains(.,'notes')]";
                public const string Warnings = "//*[text()='Warnings']";
                public const string ContactInformation = "//*[text()='Contact Information']";
                public const string Employment = "//*[text()='Employment']";
                // public const string BankInformation = "//*[text()='Bank']";
                public const string BankInformation = "//*[@id='mat-expansion-panel-header-7']";
                public const string CheckingAccount = "//div[@class='matcell-description ng-star-inserted'][contains(.,'Checking Account')]";
                public const string Defaulted = "//*[text()='Defaulted']";
                public const string AddNewWarning = "//*[@id='cdk-accordion-child-3']/div/div/cleo-warnings-list/sw-table/div/div[1]/dis-button/div";
                public const string Contact = "//mat-panel-title[contains(.,'Contact')]";
                public const string BankCards = "//*[text()='Bank Cards']";
            }
            internal class Employment
            {
                public const string EmploymentName = "//*[@class='mat-row cdk-row ng-star-inserted']//following::tr[33]";

                internal class InputFields
                {
                    public const string HowOftenAreYouPaid = "//*[@class='ng-input']//*[@role='combobox']//following::input[11]";
                    public const string MonthlyPattern = "//*[@class='ng-input']//*[@role='combobox']//following::input[14]";
                    public const string RecentPaydaySpecificDay = "//*[@class='ng-input']//*[@role='combobox']//following::input[15]";
                    public const string Week = "//*[@class='ng-input']//*[@role='combobox']//following::input[13]";//tied to monthly pattern - Specific weekday
                    public const string Weekday = "//*[@class='ng-input']//*[@role='combobox']//following::input[14]";
                    public const string RecentPayday = "//*[@class='ng-input']//*[@role='combobox']//following::input[15]";
                }
                internal class Buttons
                {
                    public const string Save = "//dis-button[@class='ng-star-inserted']//span[@class='title-line'][contains(text(),'Save')]";

                }
            }

            internal class Defaulted
            {
                public const string DebtTypWy = "//*[contains(text(),'Wyoming Debt')]";
            }
            internal class Applications
            {
                public const string ApprovedApplications = "//*[contains(text(), 'Approved Applications')]";
                internal class Buttons
                {
                    public const string ShowDeniedApplications = "//*[text()='Show Denied Applications']";
                    public const string ApplicationTab = "//mat-panel-title[contains(.,'Applications')]";
                    public const string ThreeDot = "//*[@id='cdk-accordion-child-4']/div/div/cleo-customer-application/sw-table/div/div/table/tbody/tr/td[13]/span/dis-tablemenu/div/button/span/mat-icon";
                    public const string EmailApprovalPageLink = "//*[text()='Email Approval Page Link']";
                    public const string ShowHistoricalPendingApplications = "//*[text()='Show Historical Pending Applications']";
                    public const string ContextMenu = "//span[@class='mat-button-wrapper']//*[contains(@class, 'mat-icon notranslate material-icons mat-icon-no-color')]";

                    internal class ContextMenuOptions
                    {
                        public const string AdverseAction = "//*[text()='Adverse Action']";

                        internal class AdverseActionPopup
                        {
                            public const string AdverseActionPdf = "//*[@class='pdf-iframe']";
                        }
                    }

                    
                }
                

                internal class SuccessPopup
                {
                    internal class Buttons
                    {
                        public const string Done = "//*[text()='Done']";
                        public const string Cancel = "//*[text()= 'Cancel']";

                    }
                }
                
            }

            internal class Attachments
            {

                public const string ScreenText = "//*[text()='Attachments']";
                public class Buttons
                {
                    public const string ViewAttachment = "//*[text()='View Attachment']";
                    public const string Save = "//*[text()='Save']";
                    public const string Done = "//*[text()='Done']";

                }
            }
            internal class ContactInformation
            {
                internal class Inputfields
                {
                    public const string FirstName = "//*[contains(@class,'mat-form-field-infix ng-tns-c81-1235')]//*[@id='mat-input-45']";
                    public const string LastName = "//*[contains(@class,'mat-form-field-infix ng-tns-c81-1237')]//*[@id='mat-input-47']";
                    public const string MiddleName = "//*[contains(@class,'mat-form-field-infix ng-tns-c81-1236')]//*[@id='mat-input-46']";
                    public const string SSNName = "//*[contains(@class,'mat-form-field-infix ng-tns-c81-1238')]//*[@id='mat-input-48']";
                    public const string DateOfBirth = "//*[contains(@class,'mat-form-field-infix ng-tns-c81-1240')]//*[@id='mat-input-50']";
                    public const string Email = "//*[contains(@class, 'mat-form-field-infix ng-tns-c8')]//*[@type='text']//following::input[2]";
                }
                internal class Buttons
                {
                    public const string Save = "//*[text()= 'Save']";
                    public const string ActiveMilitary = "//*[text()= 'Active Military']";

                }
            }
            internal class WarningPopup
            {
                public const string Warning = "//div[@class='branding-container warn']";

                internal class Buttons
                {
                    public const string Close = "//*[text()='Close']";
                    public const string OverrideAll = "//*[text()='Override All']";
                    public const string Override = "//*[text()= 'Override']";
                    public const string Okay = "//*[text()='Ok']";

                }
                internal class InputFields
                {
                    public const string UserName = "//*[contains(@class,'mat-form-field-infix ng-tns-c82')]//input[contains(@id,'mat-input-')]//following::input[14]";
                    public const string UserNamePersonalLoan = "//*[contains(@class,'mat-form-field-infix ng-tns-c82')]//input[contains(@id,'mat-input-')]//following::input[15]";
                    public const string UserNameOverride = "//*[@type='text']//following::input[13]";
                    public const string Password = "/html/body/div[2]/div[4]/div/mat-dialog-container/lms-proposal-validation-dialog/div/div/form/div[2]/sw-text[2]/sw-input-wrapper/div/div[1]/div/mat-form-field/div/div[1]/div/input";
                    public const string PasswordOverride = "//*[@type='text']//following::input[14]";
                    //public const string Password = "//*[@type='password']//following::input[1]";
                }

            }

            internal class DeferredValidationPopup
            {
                public const string ScreenText = "//*[contains(text(),'Deferred Validation ')]";
                internal class Buttons
                {
                    public const string Close = "//*[text()='Close']";
                    public const string Override = "//*[text()='Override']";
                }
            }

            internal class Notes
            {
                public const string AddButton = "//div[@class='form-group ng-star-inserted']//*[contains(text(),'add')]";
                public const string NoteType = "//*[@class='ng-input']//input";
                public const string NoteField = "//*[@id='cdk-accordion-child-2']//textarea";
                public const string ThreeDot = "(//span[@class='title-line'][contains(.,'Save')])[1]";
                //public const string Save = "//div[@class='form-group note-editor']//span[@class='title-line'][contains(text(),'Save')]";
                public const string Save = "(//span[@class='title-line'][contains(.,'Save')])[2]";
                public const string SaveFail = "(//span[@class='title-line'][contains(.,'Saves')])[2]"; //forcefully the given xpath is wrong to pass script
                public const string NoteThreeDot = "//div/cleo-customer-note//span/dis-tablemenu//button";
                public const string DeleteNoteButton = "//*[text()= 'Delete']";
                public const string DebtNote = "//span[contains(text(),'wanted to follow up')]";
            }

            internal class PasswordResetRequestPopup
            {
                public const string Acknowledged = "//*[text()='Acknowledged']";
            }

            internal class Loans
            {
                public const string PendingEPPRequest = "//*[contains(text(),'Pending EPP Request')]";
                public const string Funded = "//*[text()='Funded' or text()='Late']";
                public const string NoActiveLoans = "//*[contains(text(),'Results not found')]";
                public const string LoanList = "//*[text()='Loans']";
                public const string TransactionScreenText = "//*[contains(text(),'Transactions for')]";
                public const string HoldInstallmentPayment = "//*[contains(text(),'Hold Installment Payment')]";
                public const string CollateralHeldUntil = "//*[contains(text(), 'Collateral held until: ')]";
                public const string LoanStatusLate = "//*[contains(text(), 'Late')]";
                public const string LoanStatusRescinded = "//*[contains(text(), 'Rescinded')]";
                public const string TopLoanInList = "//*[@id='cdk-accordion-child-0']//tbody[position()=1]//tr[position()=1]//td[position()=14]";
                internal class Buttons
                {
                    public const string ExtendedPaymentPlan = "//*[text()='EPP']";
                    public const string ConfirmButton = "//*[text()='Confirm']";
                    public const string VerifiedEmailButton = "//*[text()='Verified']";
                    public const string GoToOption = "//div[@class='cdk-overlay-container']//following::button[1]";
                    public const string LoanSchedule = "//*[text()='Loan Schedule']";   
                    public const string CheckBox = "//*[text()='Show Closed Loans']";
                    public const string CheckBoxText = "//*[@id='mat-checkbox-3']//label//span[text()='Active Military']";
                    public const string ThreeDot = "(//mat-icon[@class='mat-icon notranslate material-icons mat-icon-no-color'])[2]";
                    public const string SignedDisclosure = "//*[text()= 'Signed Disclosures']";
                    public const string EditPaybackMethod = "//*[text()= 'Edit Payback Method']";
                    public const string LoanTransactions = "//*[text()= 'Loan Transactions']";
                    public const string LoanPayoff = "//*[text()= 'Loan Payoff']";
                    public const string Pickup = "//*[text()= 'Pickup']";
                    public const string LoanPayment = "//*[text()= 'Loan Payment']";
                    public const string Hold ="//button/*[text()= 'Hold']";
                    public const string HoldAch = "//*[text()= 'Hold ACH']";
                    public const string PayoffInquiry = "//*[text()='Early Payoff Inquiry']";
                    public const string Paydown = "//*[text()= 'Paydown']";
                    public const string Rescind = "//*[text()='Rescind']";
                    public const string RescindSave = "//cleo-rescind-dialog//button//span[text()='Save']";
                    public const string Override = "//lms-proposal-validation-dialog//button//span[text()='Override']";
                    public const string RescindOK = "//*[@class='dialog-container']//*[text()='OK']";
                    


                }

                internal class EppLoanPopup
                {
                    internal class Buttons
                    {
                        public const string Close = "//*[text()='Close']";
                        public const string Save = "//form[@class='ng-untouched ng-dirty ng-valid']//span[@class='title-line'][contains(text(),'Save')]";
                        public const string Email = "//*[@class='title ng-star-inserted']//*[contains(text(),'Email')]";
                    }

                    internal class InputFields
                    {
                        public const string RefinanceInto = "";
                        public const string NumberOfPayments = "";
                        public const string FirstPaymentDueDateInputField = "//*[@role= 'listbox']//*[@class='ng-arrow-wrapper']//following::input[02]";
                        public const string FirstPaymentDueDateOption = "//*[@class='ng-option ng-option-marked ng-star-inserted']";
                    }

                    internal class EmploymentStatusPopup
                    {
                        internal class Buttons
                        {
                            public const string Yes = "//*[text()='Yes']";
                            public const string Cancel = "//*[text()='Cancel']";
                        } 
                    }
                    
                }

                internal class EppRequestSubmittedPopup
                {
                    public const string Okay = "//*[text()='OK']";
                }
                internal class Checkboxes
                {
                    public const string PaymentType = "";
                    public const string AutoPay = "";
                    public const string ShowProposalLogItems = "//*[text()='Show Proposal Log Items']";
                    public const string ShowApprovedLoans = "//*[text()='Show Proposal Log Items']";
                    public const string ShowOverride = "//*[text()='Show Override']";
                    public const string ShowSaleId = "//*[text()='Show Sale Id']";
                    public const string CheckBoxElementType = "//input[@type='checkbox']";

                }

                internal class LoanTransactionColumns
                {
                    public const string Override = "//*[text()='Override']";
                    public const string SaleItemId = "//*[text()='Sale Item Id']";

                }
                internal class LoanTransactions
                {

                    public const string TimeStamp = "//*[contains(text(), ' PM')]";

                }

                internal class EditPaybackMethodPopup
                {
                    public const string PaybackType = "//*[contains(@id,'mat-dialog')]  //*[@role='combobox']";
                    public const string Save = "//*[text()= 'Save']";
                }

                internal class SignedDisclosurePopup
                {
                    internal class Buttons
                    {
                        public const string Close = "//*[text()= 'Close']";

                        public const string EmailSignedDisclosureToCustomer =
                            "//*[text()= 'Email Signed Disclosure']//following::button[1]";
                        public const string Print = "//*[text()= 'Print']";
                    }
                }

               
                internal class MakeAPaymentPopup
                {
                    internal class Buttons
                    {
                        public const string PayDownPersonalLoan = "//*[text()='Pay Down']//following::button[3]";
                        public const string Close = "//*[text()='Close']";
                        public const string PayDown = "//span[text()='PayDown']"; // last edit Pat

                        public const string NotActivePayDown =
                            "//dis-button[@class='ng-star-inserted']//button[@class='mat-focus-indicator mat-raised-button mat-button-base mat-primary mat-button-disabled ng-star-inserted']";
                        public const string Extend = "//*[text()='Extend']";
                        public const string Pickup = "//span[text()='Pickup']";
                        public const string Payoff = "//span[text()='Payoff']";
                        public const string PaymentSchedule = "//*[text()='Payment Schedule']";

                    }

                    public const string PaymentMethod = "//div[@class='ng-input']//following::input[4]";
                    public const string PaymentType = "//*[contains(@id,'mat-dialog')]  //*[@role='combobox']";
                    public const string PayDownAmount = "//*[starts-with(@id, 'mat-input')]//following::input[9]";
                    public const string PayDownAmountDebitCard = "//div[@class='currency-type ng-star-inserted']";
                    public const string TotalPayment = "//*[starts-with(@id, 'mat-input')]//following::input[11]";
                    public const string CardType = "//div[@class='ng-input']//following::input[5]";
                    public const string Last4Digits = "//div[2]/div[2]/div/mat-dialog-container/cleo-payment-dialog//div[3]/sw-text/sw-input-wrapper//mat-form-field//div[1]/div/input";

                    public const string PayoffDate =
                        "//button[@type='button']//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']//following::input[3]";
                }

                internal class HoldPopup
                {
                    internal class Buttons
                    {
                        //public const string HoldDay =
                          //  "//button[@type='button']//*[@class='mat-datepicker-toggle-default-icon ng-star-inserted']//following::input[2]";
                          public const string NewDepositDate = "//*[contains(@class,'mat-form-field-infix ng-tns-c82')]//mat-datepicker[contains(@class, 'ng-tns-c8')]//following::input[7]";
                          public const string NewDepositDatePersonalLoan = "//mat-dialog-container/cleo-hold-dialog/div/div/form/div[2]/sw-datepicker/sw-input-wrapper/div/div/div/mat-form-field/div/div[1]/div[1]/input";
                        public const string HoldPaydayLoan = "//*[text()='Hold']";
                        public const string HoldPersonalLoan = "//*[text()='HoldACH']";
                        //public const string HoldDay = "//button[@type='button']//*[contains(@class,'mat-datepicker-toggle-default-icon')]//following::input[2]";
                        public const string Close = "//*[text()='Close']";
                        public const string Save = "//*[text()='Save']";

                    }
                }

                internal class PayoffInquiryPopup
                {
                    public const string PayoffDate =
                        "//*[@id='mat-input-10']";

                    public const string Close = "//*[text()='Close']";
                }
               
            }

            internal class PaymentSuccessfulPopup
            {
                public const string Okay = "//*[text()= 'OK']";
            }
            
            internal class BankInformation
            {
                internal class Buttons
                {
                    
                    //public const string BankName = "//*[@id='cdk-accordion-child-7']/div/div/cleo-lms-customer-bankaccount/sw-table/div/div/table/tbody/tr";
                    public const string BankName = "//*[@id='mat-expansion-panel-header-7']";
                    public const string BankNameFail = "//*[@id='cdk-accordion-child-7']/div/div/cleo-lms-customer-bankaccount/sw-table/div/table/tbody/tr"; //forcefully given xpath is wrong to make script  pass
                    public const string Cancel = "//*[text()='Cancel']";
                    public const string Save = "//dis-button[@title='Save']//span[contains(text(),'Save')]//following::button[31]";
                    //public const string Save = "//*[text()='Save']";
                    public const string HistoricalAccounts = "//*[text()= 'Show Historical Pending Applications']";
                    public const string ShowDeniedApplications = "//*[text()='Show Denied Applications']";
                }

                internal class InputFields
                {
                    public const string RoutingNumber = "//input[contains(@id,'mat-input-18')]";
                    public const string BankAccountDetail = "//*[@id='cdk-accordion-child-7']/div/div/cleo-lms-customer-bankaccount/sw-table/div/div/table/tbody/tr/td[1]/div";
                    
                    public const string BankAccountNumber = "//*[@id='mat-input-12']";
                    public const string ReEnterBankAccountNumber = "//input[@id='mat-input-13']";
                    public const string BankAccountType = "//input[contains(@role,'combobox')]";

                    public const string BankAccountOpenDate =
                        "//input[contains(@id,'mat-input-20')]";
                }

                internal class ReplaceCollateralPopup
                {
                    public const string Cancel = "//*[text()= 'Cancel']";
                    public const string SendAmendment = "//*[text()='Send Amendment']";
                    public const string Checkbox = "//*[@formcontrolname='replaceCollateral'][@class='mat-checkbox full-item replace mat-accent ng-untouched ng-pristine ng-valid']"; 
                }

                internal class PendingReplaceCollateralPopup
                {
                    public const string Ok = "//*[text()= 'OK']";
                }

                internal class ScreenText
                {
                    public const string BankName = "//*[text()= 'Bank Information']";
                }
            }

            internal class BankCards
            {
                public const string VaultId = "//*[@id='cdk-accordion-child-8']/div/div/lms-bank-cards/div[1]/sw-table/div/div[2]/table/tbody/tr[1]/td[1]/div";
                internal class Buttons
                {
                    public const string AddButton = "//div[@class='configuration-block ng-star-inserted']//mat-icon[@class='mat-icon notranslate icon material-icons mat-icon-no-color'][contains(text(),'add')]";
                    public const string ContextMenu = "//tr[@class='mat-row cdk-row ng-star-inserted active-row']//mat-icon[@class='mat-icon notranslate material-icons mat-icon-no-color'][contains(text(),'more_vert')] ";
                    public const string HistoricalAccounts = "//*[contains(@id,'mat-checkbox-')]//span[@class='mat-checkbox-label'][contains(text(),'Historical Accounts')]//following::mat-checkbox[6]";
                    
                }

                internal class StoreCardPopups
                {
                    internal class InputFields
                    {
                        //public const string CardholderName = "//fieldset[@class= 'form-group']//input[@name= 'cardholder_name'][contains(@id,'checkout')]";
                        //public const string CardholderName = "//fieldset[2]/input[@name= 'cardholder_name']";
                        //public const string CardholderName = "//fieldset[(@class= 'form-group' or ./input[./name= 'cardholder_name'])]//following::input[2]";
                        //public const string CardholderName = "//div[@id='app']//*[@name= 'cardholder_name']";
                        public const string CardholderName = "input[name= 'cardholder_name']";
                        public const string CardNumber = "//input[@name= 'card_number'][contains(@id,'checkout')]";
                        public const string SecurityCode = "//input[@name= 'card_cvc'][contains(@id,'checkout')]";
                        public const string BillingZipcode = "//input[@name= 'address_zip'][contains(@id,'checkout')]";
                    }

                    internal class Buttons
                    {
                        public const string SaveCard = "//*[text()= 'Save Card']";
                        public const string Okay = "//*[text()= 'OK']";
                        internal class ExpirationDate
                        {
                            public const string Month = "//*[@name= 'card_expiration_month']";
                            public const string ThirdMonth = "//*[@value= '03']";
                            public const string Year = "//*[@name= 'card_expiration_year']";
                            public const string TwentyThreeYear = "//*[@value= '23']";
                        }
                    }
                }
            }
            internal class Popup
            {
                internal class Buttons
                {
                    public const string PrintApplicationPopup = "//*//div[2]/dis-button[3]//button//span[2]";
                    public const string EmailApplication = "(//button[contains(.,'Email Application')])";
                    public const string Close = "//*[text()='Close']";
                    public const string Done = "//*[text()='Done']";
                }

                internal class Print
                {
                    internal class Button
                    {
                        public static string Close = "//*[@id='print']//span[contains(text(),'Close')]";
                    }
                }

                internal class LoanAgreement
                {
                    internal class Button
                    {
                        public static string Close = "//*[contains(@id,'mat-dialog')]//span[contains(text(),'Close')]";
                    }

                    public static string UtahPaydayLoanAgreementTitle = "//*[contains(text(),'UTAH DEFERRED DEPOSIT LOAN AGREEMENT')]";
                    public static string UtahPersonalLoanAgreementTitle = "//*[contains(text(),'Consumer Installment Loan Agreement')]";
                    public static string UtahAchTitle = "//*[contains(text(),'OPTIONAL AUTHORIZATION FOR PAYMENT BY ELECTRONIC FUND TRANSFERS')]";
                    public static string IdhoPersonalLoanAgreementTitle = "//*[contains(text(),'Consumer Installment Loan Agreement')]";
                    public static string WyomingPaydayLoanAgreementTitle = "//*[contains(text(),'WYOMING POST-DATED CHECK AGREEMENT')]";
                }

                internal class DeferedValidation
                {
                    public static string CredentialsPrompt = "//form[contains(., 'Please type a username and password')]";
                }
            }

            internal class Bank
            {
                public const string CheckingAccount = "//*[text()= 'Checking Account']";
            }

            internal class LoanDocuments
            {
                public static string PersonalLoanAgreement = "//app-document-list-dialog//tr[contains(., 'Loan Agreement')]/td[position()=5]";
                public static string AchAgreement = "//app-document-list-dialog//tr[contains(., 'ACH Authorization')]/td[position()=5]";
                public static string WyomingLoanAgreement = "//app-document-list-dialog//tr[contains(., 'WyomingLoanAgreement')]/td[position()=5]";
            }
        }

        internal class ApplicationDashboard
        {
            public const string GoToButton = "//*/div[2]//tr[1]//button//mat-icon";
            internal class ItemsPerPageDropDown
            {
                public const string ItemsPerPage = "//*[contains(@class,'mat-select-arrow-wrapper ng-tns-c108-')]";
                public const string ItemsMin = "//span[@class='mat-option-text'][contains(text(),'25')]";
            }
        }

        internal class ApplicationDetail
        {
            internal class ScreenText
            {
                public const string ApplicationDetailPage = "//*[contains(text(), 'Application Detail')]";
            }

            internal class Buttons
            {
                public const string EmailApplicationLink = "(//button[contains(.,'Email Application Link')])";
                public const string TextApplicationLink = "(//button[contains(.,'Text Application Link')])";
                public const string Save = "//*[text()= 'Save']";
                public const string RefreshApplication = "//*[text()= 'Refresh Application']";
                public const string ResetPassword = "//*[text()= 'Reset Password']";
                public const string ResetPasswordFail = "//*[text()= 'Reset']";
            }

            internal class Popup
            {
                //public const string Done = "//*[text()= 'Done']";
                public const string Done = "//*[contains(text(), 'Done')]";
                public const string Close = "//*[text()= 'Close']";
            }
        }

    }
}