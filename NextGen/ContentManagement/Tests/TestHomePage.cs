﻿using ContentManagement.Pages;
using NUnit.Framework;

namespace ContentManagement.Tests
{
    [TestFixture]
    public class HomePageFunctionality : BaseTest
    {
        [Test]
        [TestCase(TestName = "HomePage Load")]
        public void HomePage_000_PROD()
        {
            var homePage = new HomePage(Driver);
            homePage.GoTo();
            Assert.IsTrue(homePage.IsLoaded, ErrorStrings.HomePageDidNotOpen);
        }
        [Test]
        [TestCase(TestName = "ForcedError - HomePage Load")]
        public void ForcedError_HomePage_000_PROD()
        {
            var homePage = new HomePage(Driver);
            homePage.ForcedErrorGoTo();
            Assert.IsTrue(homePage.IsLoaded, ErrorStrings.HomePageDidNotOpen);
        }
    }
}