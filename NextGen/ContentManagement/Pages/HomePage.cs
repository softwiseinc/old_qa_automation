﻿using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;

namespace ContentManagement.Pages
{
    internal class HomePage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public HomePage(IWebDriver driver) : base(driver)
        {
        }
        public bool IsLoaded
        {

            get
            {
                var isLoaded = Driver.Url.Contains(UrlStrings.CheckCityHomePage);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate whether the Home Page loaded successfully.");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        internal void ForcedErrorGoTo()
        {
            var url = UrlStrings.DuckDuckGo;
            Driver.Navigate().GoToUrl(url);
            Reporter.LogPassingTestStepToBugLogger($"In a browser, go to url=>{url}");
        }

        internal void GoTo()
        {
            var url = UrlStrings.CheckCityHomePage;
            Driver.Navigate().GoToUrl(url);
            Reporter.LogPassingTestStepToBugLogger($"In a browser, go to url=>{url}");
        }
    }
}