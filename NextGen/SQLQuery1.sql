﻿;WITH OpenAccounts AS (SELECT lcv.AccountId FROM LineOfCreditView lcv WHERE lcv.IsOpen = 1 AND lcv.AccountId like '001%' 
),AllDates (RollForwardDate,ID,CharFrom) 
AS ( 
SELECT la.NextPaymentDueDate,la.ID,(select 'PC') CharFrom FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId 
UNION 
SELECT la.NextBillingCycleDate,la.ID,(select 'BC') CharFrom FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId  
UNION 
SELECT lpps.PaymentDate, lpps.Account_ID ID,(select 'AP') CharFrom FROM LOCPaymentPlanSchedule lpps INNER JOIN OpenAccounts oa ON lpps.Account_ID = oa.AccountId
) 

SELECT *, (select CustomerName from LOCAccount where ID = ad.ID) CusName 
FROM AllDates ad where ad.RollForwardDate > (select System_Date from Location where ID = '001')
order by ad.RollForwardDate


;WITH OpenAccounts AS (SELECT lcv.AccountId FROM LineOfCreditView lcv WHERE lcv.IsOpen = 1 AND lcv.AccountId like '001%'),AllDates (RollForwardDate,ID,CharFrom) AS (SELECT la.NextPaymentDueDate,la.ID,(select 'PC') CharFrom FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId UNION SELECT la.NextBillingCycleDate,la.ID,(select 'BC') CharFrom FROM LOCAccount la INNER JOIN OpenAccounts oa ON la.ID = oa.AccountId UNION SELECT lpps.PaymentDate, lpps.Account_ID ID,(select 'AP') CharFrom FROM LOCPaymentPlanSchedule lpps INNER JOIN OpenAccounts oa ON lpps.Account_ID = oa.AccountId) SELECT *, (select CustomerName from LOCAccount where ID = ad.ID) CusName FROM AllDates ad where ad.RollForwardDate > (select System_Date from Location where ID = '001') order by ad.RollForwardDate
