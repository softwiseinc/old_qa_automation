﻿using System.Data.SqlClient;

namespace AutomationResources
{
    public class Database
    {
        public string ExecuteSingleQuery(string aQuery)
        {
            
            string returnData = null;
            //string aQuery = "select top 1 FirstName from Contact order by NEWID()";
            //string columnName;
            string connect = "";
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    connect = "Data Source=ccc7sqltest.checkcity.local\\ccc7sql2017;Initial Catalog=UtahTest;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
                case SystemUnderTest.STAGE:
                    connect = "Data Source=ccqastgsql1.checkcity.local.;Initial Catalog=Utah3;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
                case SystemUnderTest.PROD:
                    connect = "Data Source=ccqastgsql1.checkcity.local.;Initial Catalog=Utah3;Persist Security Info=True;User ID=sa;Password=pa$$0ut;"; break;
                default:
                    connect = "Data Source=ccqastgsql1.checkcity.local.;Initial Catalog=Utah3;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
            }

            SqlConnection databaseConnection = new System.Data.SqlClient.SqlConnection(connect);
            databaseConnection.Open();
            SqlCommand cmd = new SqlCommand(aQuery, databaseConnection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                returnData = reader[0].ToString();
            }
            databaseConnection.Close();
            return returnData;
        }

        public string BuildConnectionString()
        {
            string connect;
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    connect = "Data Source=ccc7sqltest.checkcity.local\\ccc7sql2017;Initial Catalog=UtahTest;User Id=webuser;Password=Cr@zyP@$$word;";
                    break;
                case SystemUnderTest.STAGE:
                    connect = "Data Source=ccqastgsql1.checkcity.local.;Initial Catalog=Utah3;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
                case SystemUnderTest.PROD:
                    connect = "Data Source=ccc7sqltest.checkcity.local\\ccc7sql2017;Initial Catalog=UtahTest;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
                default:
                    connect = "Data Source=ccc7sqltest.checkcity.local\\ccc7sql2017;Initial Catalog=UtahTest;Persist Security Info=True;User ID=sa;Password=pa$$0ut;";
                    break;
            }
            return connect;
        }
    }
}
