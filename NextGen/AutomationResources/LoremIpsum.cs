﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace AutomationResources
{
    public static class LoremIpsum
    {
        public static string Words(int number)
        {

            var words = new[]{"lorem", "ipsum", "dolor", "sit", "amet", "consectetuer",
                "adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod",
                "tincidunt", "nullam", "laoreet", "dolore", "magna", "aliquam", "erat", "lacus",
                "tempus","pulvinar ","fusce ","dui","justo","non","Cras","quam","blandit"};
            
            var rand = new Random(Guid.NewGuid().GetHashCode());

            StringBuilder result = new StringBuilder();

            for (int w = 0; w < number; w++)
            {
                if (w > 0) { result.Append(" "); }
                result.Append(words[rand.Next(words.Length)]);
            }
            
            return result.ToString();
        }

        public static string FiveParagraphs()
        {
            string html = null;
            WebRequest request = WebRequest.Create("http://lipsum.com/feed/html");
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            html = reader.ReadToEnd(); //se citeste codul HTMl

            //searching for Lorem Ipsum
            html = html.Remove(0, html.IndexOf("<div id=\"lipsum\">"));
            html = html.Remove(html.IndexOf("</div>"));
            html = html
                .Replace("<div id=\"lipsum\">", "")
                .Replace("</div>", "")
                .Replace("<p>", "")
                .Replace("</p>", "");

            reader.Close();
            dataStream.Close();
            response.Close();
            return html;
        }

    }
}
