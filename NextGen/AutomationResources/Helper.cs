﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenQA.Selenium;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection.Emit;
using OpenQA.Selenium.Support.Extensions;
using SeleniumExtras.WaitHelpers;
using Label = System.Windows.Forms.Label;

namespace AutomationResources
{
    public static class Helper
    {
        public static class Page
        {
            // This method will test to see if an web element is found on the page
            // If the element is found on the active page you will get a return value of bool true
            // If the element is not found on the active page you will get a return value of bool false
            // usage AutomationResources.Helper.Page.ElementVisibilityState(driver, "xpath")
            // Last edit by Pat Holman 4/24/2020
            public static bool ElementVisibilityState(IWebDriver driver, string xpathString)
            {
                List<IWebElement> elementList = new List<IWebElement>();
                bool isVisible = false;

                elementList.AddRange(driver.FindElements(By.XPath(xpathString)));
                
                if (elementList.Count > 0 && elementList[0].Displayed)
                {
                    isVisible = true;
                }

                return isVisible;
            }


            public static bool AskYesNoQuestion(string question)
            {
                bool yes = false;
                DialogResult res = MessageBox.Show(question, "Confirmation", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information);
                Debug.WriteLine(res.ToString());
                if ((res = DialogResult.Yes) != DialogResult.None)
                    yes = true;
                return yes;
            }

        }

    }
}
