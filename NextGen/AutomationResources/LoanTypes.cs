﻿namespace AutomationResources
{
    public enum LoanType
    {
        Personal,
        Payday
    }

    public class LoanProperties
    {   
        public int Count { get; set; }
        public string Type { get; set; }
        public bool PastDue { get; set; }
        public int MinimumBalance { get; set; }
        public int MinimumDaysOld { get; set; }
        public string CustomerState { get; set; }
        public string CustomerType { get; set; }
        public string CustomerLastName { get; set; }
        public bool WithBalance { get; set; }
        public string LoanType { get; set; }
        public string Location_ID { get; set; }
    }
}