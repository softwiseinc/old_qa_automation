﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver.Core.Operations;


namespace AutomationResources
{
    public static class HttpWeb
    {
        public static string GetFromWebApi(string uri)
        {
            int count = 0;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            while (true)
            {
                count += 1;
                if (count > 5)
                    break;
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream ?? throw new InvalidOperationException()))
                    {
                        return reader.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                    Thread.Sleep(4000);
                }
            }
            return null;
        }
    }
}

