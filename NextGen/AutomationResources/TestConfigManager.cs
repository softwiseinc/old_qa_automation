﻿using System.Collections.Generic;
using System.Configuration;
using NUnit.Framework.Constraints;

namespace AutomationResources
{
    public static class TestConfigManager {
        private static Dictionary<string, TestConfiguration> _configs = new Dictionary<string, TestConfiguration>
        {
            {"Release", new TestConfiguration
                {
                    // ******** GENERAL CONFIGURATION ********
                    RunHeadless = "true",
                    ReleaseTest = false,
                    RunBrowserStack = false,
                    GenerateCustomers = false,
                    GenerateCustomersCount = 2000,

                    // ******** THIS SECTION IS OPTIONAL - USED ONLY FOR BROWSERSTACK TESTING ********
                    BrowserStackPlatform = "ChromeWin10",
                    //BrowserStackPlatform = "ChromeWin8.1",
                    //BrowserStackPlatform = "FirefoxWin10",
                    //BrowserStackPlatform = "FirefoxWin8.1",
                    //BrowserStackPlatform = "EdgeWin10",
                    //BrowserStackPlatform = "EdgeWin8.1",
                    //BrowserStackPlatform = "IEWin10",
                    //BrowserStackPlatform = "IEWin8.1",
                    //BrowserStackPlatform = "SafariCatalina",
                    //BrowserStackPlatform = "SafariMojave",
                    //BrowserStackPlatform = "ChromeCatalina",
                    //BrowserStackPlatform = "ChromeMojave",

                    // ******** THIS SECTION IS REQUIRED ********
                    SystemUnderTest = SystemUnderTest.QA,
                    //SystemUnderTest = SystemUnderTest.STAGE,
                    //SystemUnderTest = SystemUnderTest.PROD,

                    // ******** THIS SECTION IS REQUIRED ********
                    //CatchAllDomain = "msgnext.com",
                    //CatchAllDomain = "holman.work",
                    CatchAllDomain = "nostromorp.com",
                    //CatchAllDomain = "ivey.pro",
                    //CatchAllDomain = "brianjohnson.work",
                    //CatchAllDomain = "bendarling.work",
                    //CatchAllDomain = "rajeshdas.work",
                    //CatchAllDomain = "ravikarmacharya.work",
                    
                    // ******** THIS SECTION IS REQUIRED ********
                    RemoteUrl = "http://qaremote1.softwise.co:4444",
                    //RemoteUrl = "http://swdpqa02.softwise.co:4444"
                }
            },
            {"Phil", new TestConfiguration
                {
                    // ******** GENERAL CONFIGURATION ********
                    RunHeadless = "false",
                    ReleaseTest = false,
                    RunBrowserStack = false,
                    GenerateCustomers = false,
                    GenerateCustomersCount = 2000,

                    // ******** THIS SECTION IS OPTIONAL - USED ONLY FOR BROWSERSTACK TESTING ********
                    BrowserStackPlatform = "ChromeWin10",
                    //BrowserStackPlatform = "ChromeWin8.1",
                    //BrowserStackPlatform = "FirefoxWin10",
                    //BrowserStackPlatform = "FirefoxWin8.1",
                    //BrowserStackPlatform = "EdgeWin10",
                    //BrowserStackPlatform = "EdgeWin8.1",
                    //BrowserStackPlatform = "IEWin10",
                    //BrowserStackPlatform = "IEWin8.1",
                    //BrowserStackPlatform = "SafariCatalina",
                    //BrowserStackPlatform = "SafariMojave",
                    //BrowserStackPlatform = "ChromeCatalina",
                    //BrowserStackPlatform = "ChromeMojave",

                    // ******** THIS SECTION IS REQUIRED ********
                    SystemUnderTest = SystemUnderTest.QA,
                    //SystemUnderTest = SystemUnderTest.STAGE,
                    //SystemUnderTest = SystemUnderTest.PROD,

                    // ******** THIS SECTION IS REQUIRED ********
                    CatchAllDomain = "ivey.pro",
                    //CatchAllDomain = "nostromorp.com",
                    //CatchAllDomain = "msgnext.com",
                    //CatchAllDomain = "holman.work",
                    //CatchAllDomain = "nostromorp.com",
                    //CatchAllDomain = "brianjohnson.work",
                    //CatchAllDomain = "bendarling.work",
                    //CatchAllDomain = "rajeshdas.work",
                    //CatchAllDomain = "ravikarmacharya.work",
                    
                    // ******** THIS SECTION IS REQUIRED ********
                    RemoteUrl = "http://swdpqa01.softwise.co:4444"
                    //RemoteUrl = "http://pholman-laptop.softwise.co:4444"



                }
            },
            {"Ryan", new TestConfiguration
                {
                    // ******** GENERAL CONFIGURATION ********
                    RunHeadless = "false",
                    ReleaseTest = false,
                    RunBrowserStack = false,

                    // ******** CUSTOMER CACHE ********
                    BuildCustomerCache = false,
                    Developer = "Ryan",
                   // RootPath = @"C://Users//rpalmer//_Code//QA//NextGen//CheckCityOnline//cache//",
                   RootPath = @"C://Users//rpalmer//source//repos//QA//NextGen//CheckCityOnline//cache//",

                    
                    // ******** THIS SECTION IS REQUIRED ********
                    //SystemUnderTest = SystemUnderTest.QA,
                    SystemUnderTest = SystemUnderTest.STAGE,
                    //SystemUnderTest = SystemUnderTest.PROD,

                    // ******** CUSTOMER GENERATION COUNTS ********
                    GenerateCustomers = false,
                    GenerateCustomersCount = 500,
                    PendingAllGenerator = 5,
                    ApprovedPersonalWithBalanceGenerator = 5,
                    ApprovedPersonalZeroBalanceGenerator = 5,
                    ApprovedPaydayWithBalanceGenerator = 5,
                    ApprovedPaydayZeroBalanceGenerator = 5,
                    TurndownLeadGenerator = 5,
                    TurndownGenerator = 5,

                    // ******** THIS SECTION IS REQUIRED ********
                    //CatchAllDomain = "msgnext.com",
                    CatchAllDomain = "nostromorp.com",
                    //CatchAllDomain = "brianjohnson.work",

                    // ******** THIS SECTION IS OPTIONAL - USED ONLY FOR BROWSERSTACK TESTING ********
                    //BrowserStackPlatform = "ChromeWin10",
                    //BrowserStackPlatform = "ChromeWin8.1",
                    //BrowserStackPlatform = "FirefoxWin10",
                    //BrowserStackPlatform = "FirefoxWin8.1",
                    //BrowserStackPlatform = "EdgeWin10",
                    //BrowserStackPlatform = "EdgeWin8.1",
                    //BrowserStackPlatform = "IEWin10",
                    //BrowserStackPlatform = "IEWin8.1",
                    //BrowserStackPlatform = "SafariCatalina",
                    //BrowserStackPlatform = "SafariMojave",
                    BrowserStackPlatform = "ChromeCatalina",
        
                    //BrowserStackPlatform = "ChromeMojave",
                    
                    // ******** THIS SECTION IS REQUIRED ********
                    RemoteUrl = "http://LAPTOP-MFJAVKVP.softwise.co:4444"
                    //RemoteUrl = "http://RyanP-laptop.softwise.co:4444"
                    //RemoteUrl = "http://pholman-laptop.softwise.co:4444"
                }
            },
            {"Pat", new TestConfiguration
                {
                    
                    // ******** GENERAL CONFIGURATION ********
                    RunHeadless = "false",
                    ReleaseTest = false,
                    RunBrowserStack = false,

                    // ******** CUSTOMER CACHE ********
                    BuildCustomerCache = false,
                    Developer = "Pat",
                    // C:\Users\pholman\source\repos\QA\NextGen\CheckCityOnline\cache\QA\Pat
                    RootPath = @"C://Users//pholman//source//repos//QA//NextGen//CheckCityOnline//cache//",
                    //RootPath = @"D://_code//QA//NextGen//CheckCityOnline//cache//",

                    // ******** THIS SECTION IS REQUIRED ********
                    //SystemUnderTest = SystemUnderTest.QA,
                    SystemUnderTest = SystemUnderTest.STAGE,
                    //SystemUnderTest = SystemUnderTest.PROD,

                    // ******** CUSTOMER GENERATION COUNTS ********
                    GenerateCustomers = false,
                    GenerateCustomersCount = 500,
                    PendingAllGenerator = 1,
                    ApprovedPaydayWithBalanceGenerator = 3,
                    ApprovedPersonalZeroBalanceGenerator = 1,
                    ApprovedPersonalWithBalanceGenerator = 3,
                    ApprovedPaydayZeroBalanceGenerator = 1,
                    TurndownLeadGenerator = 1,
                    TurndownGenerator = 1,
        

                    // ******** THIS SECTION IS REQUIRED ********
                    //CatchAllDomain = "msgnext.com",
                    CatchAllDomain = "nostromorp.com",
                    

                    // ******** THIS SECTION IS OPTIONAL - USED ONLY FOR BROWSERSTACK TESTING ********
                    //BrowserStackPlatform = "ChromeWin10",
                    //BrowserStackPlatform = "ChromeWin8.1",
                    //BrowserStackPlatform = "FirefoxWin10",
                    //BrowserStackPlatform = "FirefoxWin8.1",
                    //BrowserStackPlatform = "EdgeWin10",
                    //BrowserStackPlatform = "EdgeWin8.1",
                    //BrowserStackPlatform = "IEWin10",
                    //BrowserStackPlatform = "IEWin8.1",
                    //BrowserStackPlatform = "SafariCatalina",
                    //BrowserStackPlatform = "SafariMojave",
                    BrowserStackPlatform = "ChromeCatalina",
        
                    //BrowserStackPlatform = "ChromeMojave",

                    // ******** THIS SECTION IS REQUIRED ********
                    RemoteUrl = "http://pholman-laptop.softwise.co:4444"
                    //RemoteUrl = "http://qavm.softwise.co:4444"
                    //RemoteUrl = "http://qaremote1.softwise.co:4444"
                    //RemoteUrl = "http://swdpqa02.softwise.co:4444"
                }
            }
        };

        public static TestConfiguration Config => _configs.TryGetValue(ConfigurationManager.AppSettings["env"], out TestConfiguration output) ? output : new TestConfiguration
        {
            RemoteUrl = "http://qaremote1.softwise.co:4444",
            SystemUnderTest = SystemUnderTest.QA,
            CatchAllDomain = "msgnext.com",
            GenerateCustomers = true,
            GenerateCustomersCount = 2,
            RunHeadless = "false"
        };
    }
}
