﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationResources
{
    public enum EmployeeType
    {
        Owner,
        Supervisor,
        Manager,
        CustomerServiceRep,
        AdvanceCustomerService,
        Operations,
        Monitor
    }
}
