﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using BrowserStack;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;
using NLog;


namespace AutomationResources
{
    public class WebDriverFactory
    {
        public TestConfiguration Config;
        protected string profile;
        protected string environment;
        private Local browserStackLocal;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public WebDriverFactory(string profile, string environment)
        {
            Config = TestConfigManager.Config;
            Config.RemoteUrl = TestContext.Parameters["RemoteUrl"]?.ToLower() ?? Config.RemoteUrl;
            Config.RunHeadless = TestContext.Parameters["RunHeadless"]?.ToLower() ?? Config.RunHeadless;
            Config.CatchAllDomain = TestContext.Parameters["CatchAllDomain"]?.ToLower() ?? Config.CatchAllDomain;
            GenerateCustomersHandler();
            SystemUnderTestHandler();
            this.profile = profile;
            this.environment = environment;
        }

        public WebDriverFactory()
        {
            Config = TestConfigManager.Config;
        }

        public void SystemUnderTestHandler()
        {
            if (!string.IsNullOrEmpty(TestContext.Parameters["SUT"]))
            {
                if (Enum.TryParse(TestContext.Parameters["SUT"].ToUpper(), out SystemUnderTest target))
                {
                    Config.SystemUnderTest = target;
                }
            }
        }

        public static void GenerateCustomersHandler()
        {
            
            if (!string.IsNullOrEmpty(TestContext.Parameters["GenerateCustomers"]))
            {
                TestConfigManager.Config.GenerateCustomers = bool.Parse(TestContext.Parameters["GenerateCustomers"]);

                if (!string.IsNullOrEmpty((TestContext.Parameters["SUT"])))
                {
                    TestConfigManager.Config.SystemUnderTest = TestContext.Parameters["SUT"].ToUpper().Contains("QA")
                        ? SystemUnderTest.QA
                        : SystemUnderTest.STAGE;
                }
                if (!string.IsNullOrEmpty((TestContext.Parameters["ApprovedWithBalance"])))
                    TestConfigManager.Config.ApprovedPersonalWithBalanceGenerator = int.Parse(TestContext.Parameters["ApprovedPersonalWithBalanceGenerator"]);
                
                if (!string.IsNullOrEmpty((TestContext.Parameters["ApprovedZeroBalance"])))
                    TestConfigManager.Config.ApprovedPersonalZeroBalanceGenerator = int.Parse(TestContext.Parameters["ApprovedPersonalZeroBalanceGenerator"]);

                if (!string.IsNullOrEmpty((TestContext.Parameters["ApprovedWithBalance"])))
                    TestConfigManager.Config.ApprovedPaydayWithBalanceGenerator = int.Parse(TestContext.Parameters["ApprovedPaydayWithBalanceGenerator"]);

                if (!string.IsNullOrEmpty((TestContext.Parameters["ApprovedZeroBalance"])))
                    TestConfigManager.Config.ApprovedPaydayZeroBalanceGenerator = int.Parse(TestContext.Parameters["ApprovedPaydayZeroBalanceGenerator"]);

                if (!string.IsNullOrEmpty((TestContext.Parameters["Turndown"])))
                    TestConfigManager.Config.TurndownGenerator = int.Parse(TestContext.Parameters["TurndownGenerator"]);
                
                if (!string.IsNullOrEmpty((TestContext.Parameters["TurndownLead"])))
                    TestConfigManager.Config.TurndownLeadGenerator = int.Parse(TestContext.Parameters["TurndownLeadGenerator"]);

                if (!string.IsNullOrEmpty((TestContext.Parameters["PendingAll"])))
                    TestConfigManager.Config.PendingAllGenerator = int.Parse(TestContext.Parameters["PendingAllGenerator"]);

                Debug.WriteLine("Config.GenerateCustomers: " + TestConfigManager.Config.GenerateCustomers);
                Debug.WriteLine("Config.ApprovedPersonalWithBalanceGenerator: " + TestConfigManager.Config.ApprovedPersonalWithBalanceGenerator);
                Debug.WriteLine("Config.ApprovedPersonalZeroBalanceGenerator: " + TestConfigManager.Config.ApprovedPersonalZeroBalanceGenerator);
                Debug.WriteLine("Config.ApprovedPaydayWithBalanceGenerator: " + TestConfigManager.Config.ApprovedPaydayWithBalanceGenerator);
                Debug.WriteLine("Config.ApprovedPaydayZeroBalanceGenerator: " + TestConfigManager.Config.ApprovedPaydayZeroBalanceGenerator);
                Debug.WriteLine("Config.TurndownGenerator: " + TestConfigManager.Config.TurndownGenerator);
                Debug.WriteLine("Config.TurndownLeadGenerator: " + TestConfigManager.Config.TurndownLeadGenerator);
                Debug.WriteLine("Config.PendingAllGenerator: " + TestConfigManager.Config.PendingAllGenerator);
                Logger.Debug("Config.GenerateCustomers: " + TestConfigManager.Config.GenerateCustomers);
                Logger.Debug("Config.ApprovedPersonalWithBalanceGenerator: " + TestConfigManager.Config.ApprovedPersonalWithBalanceGenerator);
                Logger.Debug("Config.ApprovedPersonalZeroBalanceGenerator: " + TestConfigManager.Config.ApprovedPersonalZeroBalanceGenerator);
                Logger.Debug("Config.ApprovedPaydayWithBalanceGenerator: " + TestConfigManager.Config.ApprovedPaydayWithBalanceGenerator);
                Logger.Debug("Config.ApprovedPaydayZeroBalanceGenerator: " + TestConfigManager.Config.ApprovedPaydayZeroBalanceGenerator);
                Logger.Debug("Config.Turndown: " + TestConfigManager.Config.TurndownGenerator);
                Logger.Debug("Config.TurndownLead: " + TestConfigManager.Config.TurndownLeadGenerator);
                Logger.Debug("Config.PendingAll: " + TestConfigManager.Config.PendingAllGenerator);
            }
                
        }

        public IWebDriver Create(BrowserType browserType)
        {
            Config = TestConfigManager.Config;
            if (Config.RunBrowserStack)
                browserType = BrowserType.BrowserStack;

            switch (browserType)
            {
                case BrowserType.Chrome:
                    return GetChromeDriver();
                case BrowserType.BrowserStack:
                    return GetBrowserStackDriver();
                default:
                    throw new ArgumentOutOfRangeException($"No such browser exists");
            }
        }

        private IWebDriver GetBrowserStackDriver()
        {
            NameValueCollection caps = ConfigurationManager.GetSection("capabilities/" + profile) as NameValueCollection;
            NameValueCollection settings = ConfigurationManager.GetSection("environments/" + environment) as NameValueCollection;
#pragma warning disable 618
            DesiredCapabilities capability = new DesiredCapabilities();
#pragma warning restore 618

            if (caps != null)
                foreach (string key in caps.AllKeys)
                {
#pragma warning disable 618
                    capability.SetCapability(key, caps[key]);
#pragma warning restore 618
                }

            if (settings != null)
                foreach (string key in settings.AllKeys)
                {
#pragma warning disable 618
                    capability.SetCapability(key, settings[key]);
#pragma warning disable 618
                }
            String username = Environment.GetEnvironmentVariable("BROWSERSTACK_USERNAME");
            if (username == null)
            {
                username = ConfigurationManager.AppSettings.Get("user");
            }

            String accesskey = Environment.GetEnvironmentVariable("BROWSERSTACK_ACCESS_KEY");
            if (accesskey == null)
            {
                accesskey = ConfigurationManager.AppSettings.Get("key");
            }


            Dictionary<string, object> chromeProfile = new Dictionary<string, object>();
            chromeProfile.Add("profile.default_content_setting_values.notifications", 2); // 0 - Default, 1 - Allow, 2 - Block
            Dictionary<string, object> chromeOptions = new Dictionary<string, object>();
            chromeOptions.Add("prefs", chromeProfile);
            Config = TestConfigManager.Config;

            capability.SetCapability("browserstack.user", username);
            capability.SetCapability("browserstack.key", accesskey);
            capability.SetCapability("browserstack.localIdentifier", "Test123");
            capability.SetCapability("resolution", "1024x768");

            if (Config.BrowserStackPlatform.Contains("Win"))
            {
                capability.SetCapability("os", "Windows");
                capability.SetCapability("os_version", Config.BrowserStackPlatform.Contains("10") ? "10" : "8.1");
            }
            else
            {
                capability.SetCapability("os", "OS X");
                capability.SetCapability("os_version", Config.BrowserStackPlatform.Contains("Catalina") ? "Catalina" : "Mojave");
            }

            var TestName = TestContext.CurrentContext.Test.Name;
            
            switch (Config.BrowserStackPlatform)
            {
                case "ChromeWin10":
                    capability.SetCapability("name", "[ChromeWin10] [" + Config.SystemUnderTest + "] - " + TestName );
                    capability.SetCapability("browser", "chrome");
                    capability.SetCapability("chromeOptions", chromeOptions);
                    break;
                case "ChromeWin8.1":
                    capability.SetCapability("name", "[ChromeWin8.1] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "chrome");
                    capability.SetCapability("chromeOptions", chromeOptions);
                    break;
                case "FirefoxWin10":
                    capability.SetCapability("name", "[FirefoxWin10] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Firefox");
                    break;
                case "FirefoxWin8.1":
                    capability.SetCapability("name", "[FirefoxWin8.1] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Firefox");
                    break;
                case "EdgeWin10":
                    capability.SetCapability("name", "[EdgeWin10] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Edge");
                    break;
                case "EdgeWin8.1":
                    capability.SetCapability("name", "[EdgeWin8.1] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Edge");
                    break;
                case "IEWin10":
                    capability.SetCapability("name", "[IEWin10] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "IE");
                    break;
                case "IEWin8.1":
                    capability.SetCapability("name", "[IEWin8.1] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "IE");
                    break;
                case "SafariCatalina":
                    capability.SetCapability("name", "[SafariCatalina] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Safari");
                    break;
                case "SafariMojave":
                    capability.SetCapability("name", "[SafariMojave] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "Safari");
                    break;
                case "ChromeCatalina":
                    capability.SetCapability("name", "[ChromeCatalina] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "chrome");
                    capability.SetCapability("chromeOptions", chromeOptions);
                    break;
                case "ChromeMojave":
                    capability.SetCapability("name", "[ChromeMojave] [" + Config.SystemUnderTest + "] - " + TestName);
                    capability.SetCapability("browser", "chrome");
                    capability.SetCapability("chromeOptions", chromeOptions);
                    break;
            }

            if (capability.GetCapability("browserstack.local") != null && capability.GetCapability("browserstack.local").ToString() == "true")
            {
                browserStackLocal = new Local();
                List<KeyValuePair<string, string>> bsLocalArgs = new List<KeyValuePair<string, string>>() {
                    new KeyValuePair<string, string>("key", accesskey)
                };
                browserStackLocal.start(bsLocalArgs);
            }
            IWebDriver remoteDriver = new RemoteWebDriver(new Uri("http://" + ConfigurationManager.AppSettings.Get("server") + "/wd/hub/"), capability);
            return remoteDriver;
        }

        private IWebDriver GetChromeDriver()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            if (Config.RunHeadless == "true")
                chromeOptions.AddArgument("--headless");

            chromeOptions.AddArgument("--disable-notifications");
            chromeOptions.AddArgument("--ignore-certificate-errors");

            IWebDriver remoteDriver = new RemoteWebDriver(new Uri(Config.RemoteUrl), chromeOptions);
            return remoteDriver;
        }
    }
}
