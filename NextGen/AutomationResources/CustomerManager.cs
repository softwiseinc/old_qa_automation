﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace AutomationResources
{
    public static class CustomerManager
    {
        public static List<object> UT_65PlusPersonal = new List<object>();
        public static List<object> ID_65PlusPersonal = new List<object>();
        public static List<object> UT_70PlusPayday = new List<object>();
        public static List<object> WI_65PlusPersonal = new List<object>();
        public static List<object> TX_65PlusPersonal = new List<object>();
        public static List<object> UT_70PlusPersonal = new List<object>();
        public static List<object> ID_70PlusPersonal = new List<object>();
        public static List<object> WI_70PlusPersonal = new List<object>();
        public static List<object> TX_70PlusPersonal = new List<object>();
        public static List<object> UT_PendingAll = new List<object>();
        public static List<object> UT_TurndownLead = new List<object>();
        public static List<object> UT_Turndown = new List<object>();
        public static List<object> UT_ApprovedPaydayPastDue = new List<object>();
        public static List<object> UT_ApprovedPersonalPastDue = new List<object>();
        public static List<object> UT_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> UT_ApprovedPaydayZeroBalance = new List<object>();
        public static List<object> UT_ApprovedPersonalWithBalance = new List<object>();
        public static List<object> UT_ApprovedPersonalZeroBalance = new List<object>();
        public static List<object> WY_PendingAll = new List<object>();
        public static List<object> WY_TurndownLead = new List<object>();
        public static List<object> WY_Turndown = new List<object>();
        public static List<object> WY_ApprovedPaydayPastDue = new List<object>();
        public static List<object> WY_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> WY_ApprovedPaydayZeroBalance = new List<object>();
        public static List<object> TX_PendingAll = new List<object>();
        public static List<object> TX_TurndownLead = new List<object>();
        public static List<object> TX_Turndown = new List<object>();
        public static List<object> TX_ApprovedPersonalPastDue = new List<object>();
        public static List<object> TX_ApprovedPersonalWithBalance = new List<object>();
        public static List<object> TX_ApprovedPersonalZeroBalance = new List<object>();
        public static List<object> ID_PendingAll = new List<object>();
        public static List<object> ID_TurndownLead = new List<object>();
        public static List<object> ID_Turndown = new List<object>();
        public static List<object> ID_ApprovedPersonalPastDue = new List<object>();
        public static List<object> ID_ApprovedPersonalWithBalance = new List<object>();
        public static List<object> ID_ApprovedPersonalZeroBalance = new List<object>();
        public static List<object> AK_PendingAll = new List<object>();
        public static List<object> AK_TurndownLead = new List<object>();
        public static List<object> AK_Turndown = new List<object>();
        public static List<object> AK_ApprovedPaydayPastDue = new List<object>();
        public static List<object> AK_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> AK_ApprovedPaydayZeroBalance = new List<object>();
        public static List<object> KS_PendingAll = new List<object>();
        public static List<object> KS_TurndownLead = new List<object>();
        public static List<object> KS_Turndown = new List<object>();
        public static List<object> KS_ApprovedPaydayPastDue = new List<object>();
        public static List<object> KS_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> KS_ApprovedPaydayZeroBalance = new List<object>();
        public static List<object> WI_PendingAll = new List<object>();
        public static List<object> WI_TurndownLead = new List<object>();
        public static List<object> WI_Turndown = new List<object>();
        public static List<object> WI_ApprovedPersonalPastDue = new List<object>();
        public static List<object> WI_ApprovedPersonalWithBalance = new List<object>();
        public static List<object> WI_ApprovedPersonalZeroBalance = new List<object>();
        public static List<object> CA_PendingAll = new List<object>();
        public static List<object> CA_TurndownLead = new List<object>();
        public static List<object> CA_Turndown = new List<object>();
        public static List<object> CA_ApprovedPaydayPastDue = new List<object>();
        public static List<object> CA_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> CA_ApprovedPaydayZeroBalance = new List<object>();

        public static List<object> MO_PendingAll = new List<object>();
        public static List<object> MO_TurndownLead = new List<object>();
        public static List<object> MO_Turndown = new List<object>();
        public static List<object> MO_ApprovedPersonalPastDue = new List<object>();
        public static List<object> MO_ApprovedPersonalWithBalance = new List<object>();
        public static List<object> MO_ApprovedPersonalZeroBalance = new List<object>();

        public static List<object> AL_PendingAll = new List<object>();
        public static List<object> AL_TurndownLead = new List<object>();
        public static List<object> AL_Turndown = new List<object>();
        public static List<object> AL_ApprovedPaydayPastDue = new List<object>();
        public static List<object> AL_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> AL_ApprovedPaydayZeroBalance = new List<object>();

        public static List<object> HI_PendingAll = new List<object>();
        public static List<object> HI_TurndownLead = new List<object>();
        public static List<object> HI_Turndown = new List<object>();
        public static List<object> HI_ApprovedPaydayPastDue = new List<object>();
        public static List<object> HI_ApprovedPaydayWithBalance = new List<object>();
        public static List<object> HI_ApprovedPaydayZeroBalance = new List<object>();
        
        //public static List<object> NV_PendingAll = new List<object>();
        //public static List<object> NV_TurndownLead = new List<object>();
        //public static List<object> NV_Turndown = new List<object>();
        //public static List<object> NV_ApprovedPaydayPastDue = new List<object>();
        public static List<object> NV_ApprovedPaydayWithBalance = new List<object>();
        //public static List<object> NV_ApprovedPaydayZeroBalance = new List<object>();



        public static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
                File.WriteAllText(filePath, contentsToWriteToFile);
            }

            catch (Exception)
            {
                Debug.WriteLine("File did now write");
                throw;
            }
        }

        public static string ReadFromJsonFile(string pathFileName) 
        {
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.PROD)
            {
                return null;
            }
            var fileContents = File.ReadAllText(pathFileName);
            var jasonString= JsonConvert.DeserializeObject(fileContents);
            return jasonString.ToString();
        }
    }
}
