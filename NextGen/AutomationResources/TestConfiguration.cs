﻿using System.Collections.Generic;

namespace AutomationResources
{
    public class TestConfiguration
    {
        public SystemUnderTest SystemUnderTest { get; set; }
        public string RemoteUrl { get; set; }
        public string CatchAllDomain { get; set; }
        public string RunHeadless { get; set; } = "false";
        public bool GenerateCustomers { get; set; } = false;
        public bool BuildCustomerCache { get; set; } = false;
        public bool ReleaseTest { get; set; } = false;
        public int PendingAllGenerator { get; set; } = 5;
        public int ApprovedPersonalWithBalanceGenerator { get; set; } = 5;
        public int ApprovedPersonalZeroBalanceGenerator { get; set; } = 5;
        public int ApprovedPaydayWithBalanceGenerator { get; set; } = 5;
        public int ApprovedPaydayZeroBalanceGenerator { get; set; } = 5;
        public int TurndownLeadGenerator { get; set; } = 5;
        public int TurndownGenerator { get; set; } = 5;
        public int GenerateCustomersCount { get; set; } = 50;
        public bool RunBrowserStack { get; internal set; }
        public string BrowserStackPlatform { get; set; }
        public string RootPath { get; set; }
        public string Developer { get; set; }
    }

}