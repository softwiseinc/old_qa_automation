﻿using System.CodeDom;
using System.Collections.Concurrent;
using System.ComponentModel.Design.Serialization;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Threading;
using Microsoft.SqlServer.Server;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace CheckCityOnline
{
    internal class Elements
    {

        internal class EfpPageLoginPage
        {

            internal class InputFields
            {
                public const string UserID = "//*[@class='body']//input[@id='ctl00_CustomContent_FUserID'][@name='ctl00$CustomContent$FUserID']";
                public const string Password = "//*[@class='body']//input[@id='ctl00_CustomContent_FPassword']";
            }

            internal class Buttons
            {
                public const string Login = "//*[@id='ctl00_CustomContent_FSubmit']";
            }

            internal class DropDown
            {
                public const string State = "//*[@name= 'ctl00$CustomContent$FState'][@id= 'ctl00_CustomContent_FState']";

                internal class DropdownOptions
                {
                    public const string Colorado = "//*[@value='CO']";
                    public const string Nevada = "//*[@value='NV']";
                    public const string Utah = "//*[@value='UT']";
                    public const string Virginia = "//*[@value='VA']";
                   
                }
            }
            internal class EfpPage
            {
                public const string Breadcrumb = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']";

                internal class EfpTabs
                {
                    public const string LoanApproval = "//a[contains(text(),'Loan Approval')]";
                    public const string LoanDocuments = "//a[contains(text(),'Loan Documents')]";
                    public const string AdverseActions = "//a[contains(text(),'Adverse Actions')]";
                    public const string PaymentHoliday = "//a[contains(text(),'Payment Holiday')]";
                }

                internal class LoanDocumentPage
                {
                    internal class Dropdown
                    {
                        public const string CustomerState = "//select[@id='ctl00_CustomContent_selState']";

                        internal class DropdownStateOptions
                        {
                            public const string StateUtah = "//*[@value='UT']";
                            public const string StateColorado = "//*[@value='CO']";
                            public const string StateMissouri = "//*[@value='MO']";
                            public const string StateNV = "//*[@value='NV']";
                            public const string StateNewMexico = "//*[@value='NM']";
                            public const string StateVirginia = "//*[@value='VI']";
                            public const string StateWyoming = "//*[@value='WY']";

                        }
                    }
                    internal class InputFields
                    {
                        public const string CustomerID = "//input[@id='ctl00_CustomContent_txtCustomerID']";
                    }
                    internal class Buttons
                    {
                        public const string LookupCustomer = "//input[@id='ctl00_CustomContent_btnLookupCustomer']";
                    }

                    internal class AssertItems
                    {
                        public const string DateSigned =
                            "//th[contains(text(), 'Date Signed')]//following::td[position()=18]";

                        public const string LoanDocuments = "//h5[contains(text(),'Loan Documents')]";
                    }

                    internal class LoanDocumentListPopup
                    {
                        internal class Buttons
                        {
                            public const string DocumentId = "//th[contains(text(), 'Document ID')]//following::td[position()=1]";
                            public const string Close = "//*[text()='Close']";
                        }
                    }

                }

                internal class EfpAdverseActionPage
                {
                    internal class InputFields
                    {
                        public const string CustomerID = "//input[@id='ctl00_CustomContent_txtCustomerID']";
                    }

                    internal class StateDropDownList
                    {
                        public const string StateUtah = "//*[@value='UT']";
                        public const string StateColorado = "//*[@value='CO']";
                        public const string StateMissouri = "//*[@value='MO']";
                        public const string StateNV = "//*[@value='NV']";
                        public const string StateNewMexico = "//*[@value='NM']";
                        public const string StateVirginia = "//*[@value='VI']";
                        public const string StateWyoming = "//*[@value='WY']";
                    }

                    internal class Buttons
                    {
                        public const string LookupCustomer = "//input[@id='ctl00_CustomContent_btnLookupCustomer']";
                    }

                    internal class AdverseActionTable
                    {
                        internal class LoanId
                        {

                        }

                        internal class AdverseActionPDF
                        {
                            public const string AdverseActionPdf = "//*[@target='_blank']";
                        }
                    }

                }

            }

            internal class EfpLoanApprovalPage
            {
                internal class EfpLoanApprovalTabs
                {
                    public const string LoanApproval = "//*[text()= 'Loan Approval']";
                    public const string LoanDocuments = "//*[text()= 'Loan Documents']";
                    public const string AdverseActions = "//*[text()= 'Adverse Action']";
                    public const string paymentHoliday = "//*[text()='Payment Holiday']";

                    internal class LoanApprovalOptions
                    {
                        internal class DropDowns
                        {
                            public const string CustomerState = "//select[@id='ctl00_CustomContent_selState']";
                            public const string UseACHPayment = "//select[@id='ctl00_CustomContent_selACH']";
                        }

                        internal class InputFields
                        {
                            public const string CustomerID = "//input[@id='ctl00_CustomContent_txtCustomerID']";
                        }

                        internal class Buttons
                        {
                            public const string LookupCustomer = "//input[@id='ctl00_CustomContent_btnLookupCustomer']";
                        }
                    }

                    internal class LoanDocumentOptions
                    {
                        internal class Dropdown
                        {
                            public const string CustomerState = "//select[@id='ctl00_CustomContent_selState']";

                            internal class DropdownStateOptions
                            {
                                public const string StateUtah = "//*[@value='UT']";
                                public const string StateColorado = "//*[@value='CO']";
                                public const string StateMissouri = "//*[@value='MO']";
                                public const string StateNV = "//*[@value='NV']";
                                public const string StateNewMexico = "//*[@value='NM']";
                                public const string StateVirginia = "//*[@value='VI']";
                                public const string StateWyoming = "//*[@value='WY']";

                            }
                        }

                        internal class InputFields
                        {
                            public const string CustomerID = "//input[@id='ctl00_CustomContent_txtCustomerID']";
                        }

                        internal class Buttons
                        {
                            public const string LookupCustomer = "//input[@id='ctl00_CustomContent_btnLookupCustomer']";
                            public const string LoanIdNumber = "//th[contains(text(), 'Loan ID')]//following::td[position()=1]";
                        }

                        internal class AssertItems
                        {
                            public const string DateSigned =
                                "//th[contains(text(), 'Date Signed')]//following::td[position()=2]";

                            public const string LoanDocuments = "//h5[contains(text(),'Loan Documents')]";
                        }

                        internal class LoanDocumentListPopup
                        {
                            internal class Buttons
                            {
                                public const string DocumentId = "//th[contains(text(), 'Document ID')]//following::td[position()=1]";
                                public const string Close = "//*[text()='Close']";
                            }
                        }
                        
                    }

                    internal class AdverseActionsOptions
                    {
                        internal class Dropdown
                        {
                            public const string CustomerState = "//select[@id='ctl00_CustomContent_selState']";
                                                                    
                        }

                        internal class InputFields
                        {
                            public const string CustomerID = "//input[@id='ctl00_CustomContent_txtCustomerID']";

                        }

                        internal class Buttons
                        {
                            public const string LookupCustomer = "//input[@id='ctl00_CustomContent_btnLookupCustomer']";
                        }
                    }

                    internal class PaymentHolidayOptions
                    {
                        internal class InputField
                        {
                            public const string LoanID = "//input[@id='ctl00_CustomContent_txtLoanID']";
                        }

                        internal class Buttons
                        {
                            public const string LookupLoan = "//input[@id='ctl00_CustomContent_btnLookupLoan']";
                        }
                    }
                }
            }
        }
        internal class MarketingPopup
        {
            public const string GiveAway = "//*[@id='om - cl7hv3e1j0dld7g3x3np - optin']/div[1]/button";
        }
        internal class ApplyLoan
        {
            internal class ErrorPath
            {
                public const string Input = "//input[contains(@class,'error')]";
                public const string InputSelect = "//input[contains(@class,'error')] | //select[contains(@class,'error')]";
                public const string RoutingNumberInput = "//input[@id='routingNumber'][contains(@class,'error')]";
                public const string BankAccountInput = "//input[@id='bankAccount'][contains(@class,'error')]";
                public const string SocialSecurityInput = "//input[@id='socialSecurity'][contains(@class,'error')]";
                public const string CreatePasswordInput = "//input[@id='createPassword'][contains(@class,'error')]";
                // createPassword
            }

            internal class PersonalInformation
            {
                public const string FirstName = "//*[@id='firstName']";
                public const string LastName = "//*[@id='lastName']";
                public const string LoanType = "//label[contains(text(),'Loan type')]";
                public const string PersonalLoanType = "//input[@id='rdoLoanChoice1']";
                public const string EmailAddress = "//*[@id='emailAddress']";
                //public const string DOB = "/html/body/div[1]/div[1]/div[1]/form[2]/div[3]/div/div[4]/div[2]/input";
                public const string DOB = "//input[@id= 'birthDate'][@name= 'birthDate']";
                public const string ZipCode = "//*[@id='zipCode']";
                public const string ContinueBtn = "//*[@id='continueButton']";
                public const string PersonalLoan = "//*[@id='rdoLoanChoice1']";
                public const string PaydayLoan = "//*[@id='rdoLoanChoice2']";
                public const string LoanTypeOption = "//label[contains(text(),'Loan type')]";
                public const string ZipCodeError =
                    "//div[@class='tooltip-inner'][contains(text(), 'difficulty looking up your zip')]";
                public const string DirectDepositYes = "//input[@id='switch_firstIncome_left']";
                public const string AccountTypeChecking = "//*[@id='switch_bankType_right']";
                public const string HomeRent = "//input[@id='switch_rent_right']";
                public static string EmailFuturePaymentsYes = "//*[@id='optIn']";
            }

            internal class IncomeInformation
            {
                public const string MonthlyIncome = "//*[contains(@id,'monthlyIncome')]"; 
                public const string Payperiod = "//*[@id='payPeriod']";
                public const string day_one = "//*[@id='semiMonthlyDay1']";
                public const string day_two = "//*[@id='semiMonthlyDay2']";
                public const string SelBiWeekly = "//*[@id='selBiWeekly']";
                public const string SelBiWeeklyDate = "//*[@id='selBiWeeklyDate']";
                public const string BiWeeklyDate = "//*[@id='selBiWeeklyDate']";
                public const string IncomeType = "//*[@id='incomeType']";
                public const string NameOfCompany = "//*[@id='employerName']";
                public const string SelectNameOfCompany = "//span[contains(text(), 'wise, Inc']";
                public const string EmpPhoneNumber = "//*[@id='employerPhone']";
                public const string EmpCity = "//*[@id='employerCity']";
                public const string EmpState = "//*[@id='employerState']";
                public const string EmpZipCode = "//*[@id='employerZip']";
                public const string EmpTimeYear = "//*[@id='employerTimeYear']";
                public const string EmpTimeMonth = "//*[@id='employerTimeMonth']";
                public const string lastPayDay  = "//*[@id='lblCorrectPayday']";
                public const string DirectDeposit = "//input[@id='switch_firstIncome_left']";
            }


            internal class BankInformation
            {
                public const string RoutingNumber = "//*[@id='routingNumber']";
                public const string BankAccount = "//*[@id='bankAccount']";
                public const string ReBankAccount = "//*[@id='bankAccountCheck']";
                public const string BankAccountOpenYear = "//*[@id='bankAccountLengthYear']";
                public const string BankAccountOpenMonth = "//*[@id='bankAccountLengthMonth']";
                public const string UseBankLoanPayment = "//*[@id='ctl00_CustomContent_pnlBankAccount']/div/div[2]/div/label[1]";
                public const string ConfirmBankAccount = "   //*[@id='confirmBank']";
                public const string CheckingAccount = "//input[@id='switch_bankType_right']";
                public const string BankSectionHeader = "//*[@id='form']/fieldset[3]/h4";
            }

            internal class IdentifyInformation
            {
                public const string DirectDepositYes = "//*[@id='switch_firstIncome_left']";
                public const string IdentificationType = "//*[@id='ddlIdentityType']";
                public const string IdentificationState = "//*[@id='identityState']";
                public const string IdentificationNumber = "//*[@id='identityNumber']";
                public const string ExpirationDate = "//*[@id='idExpirationDate']";
                public const string SocialSecurityNumber = "//*[@id='socialSecurity']";
                public const string BankTypeChecking = "//*[@id='switch_bankType_right']";
                
            }


            internal class ContactInformation
            {
                public const string cityStateZipDisplay = "//*[@id='cityStateZipDisplay']";
                public const string streetAddress = "//*[@id='streetAddress']";
                public const string residenceLengthYear = "//*[@id='residenceLengthYear']";
                public const string residenceLengthMonth = "//*[@id='residenceLengthMonth']";
                public const string phoneNumber = "//*[@id='phoneNumber']";
                //public const string phoneNumber = "/html/body/div[1]/div[1]/div[1]/form[2]/div[7]/div/div[3]/div[2]/input";
                public const string phoneNumberVerifyCode = "//*[@id='verifyPhoneCode']";
                public const string validatePhoneBtn = "//*[@id='validatePhoneBtn']";
                public const string optInSMS = "//input[@id='optInSMS']/following-sibling::span";
                public const string optInSMSMarketing = "//input[@id='optInSMSMarketing']/following-sibling::span";
                public const string Rent = "//input[@id='switch_rent_left']";
                public const string NoticeAboutFuturePayment = "//*[@id='emailOptInNotice']//label[text()='Yes']";
                
            }

            internal class SecurityInformation
            {
                public const string createPassword = "//*[@id='createPassword']";
            }

            internal class SubmitInformation
            {
                public const string agreeConsent = "//label[contains(@class,'checkbox-container')]//input[@id='agreeTerms']/following-sibling::span";
                public const string agreeTerms = "//label[contains(@class,'checkbox-container')]//input[@id='agreeConsent']/following-sibling::span";
                public const string SubmitBtn = "//*[@id='applySubmit']";
                public const string Withdraw = "//*[@id='resetButton']";
                public const string ConfirmYes = "//*[text()='Yes']";
            }

            internal class SussessfulSubmitApplyLoan
            {
                public const string congratulation = "//h2[@class='congratulations-title']";
            }

            public class Button
            {
                public static string ApplySubmit = "//*[@id='applySubmit']";
            }
        }

        internal class Header
        {
            public const string BrandLogo = "//*[@id='logo']";
        }
        internal class InStoreReferral
        {
            internal class Link
            {
                public const string ClickHereStoreReferral = "//*[@id='ctl00_CustomContent_HyperLink2']";
            }
        }
        internal class ReferralPage
        {
            internal class Link
            {
                public const string ClickHereStoreReferral = "//*[@id='ctl00_CustomContent_HyperLink2']";
                public const string TermsAndConditions = "//*[@id='ctl00_CustomContent_HyperLink1']";

            }
            internal class ScreenText
            {
                public const string ReferAFriend = "//*[@id='aspnetForm']/div[3]/div/div[2]/fieldset/h4";
            }
            internal class Button
            {
                public const string CopyReferralLink = "//*[@id='clickCopy']";
                public const string ReferralViaEmail = "//*[@id='ctl00_CustomContent_loginContainer']/center/button[3]";
                public const string SendEmailReferrals = "//*[@id='ctl00_CustomContent_FSubmit']";
            }

            internal class Assert
            {
                public const string CopyLinkSuccess = "//*[@id='clickCopySuccess']";
            }

            internal class Input
            {
                public const string EmailAddress = "//*[@id='ctl00_CustomContent_FEmailAddress']";

            }
        }

        internal class Loan
        {
            internal class Personal
            {
                public const string PersonalLoan = "//a[contains(.,'Request Personal LoanRepay Over Time')]";
            }

            internal class PayDay
            {
                public const string PersonalLoan = "//a[contains(.,'Request Payday LoanRepay On Your Next Payday')]";
            }

            internal class LoanHistory
            {
                internal class Buttons
                {
                    public const string Terms = "//th[contains(text(), 'Terms')]//following::td[position()=4]";
                }
                internal class LeftMenu
                {
                    public const string Profile = "//*[@id='mobileLinkView']/*[text()='Profile']";
                }

                internal class LoanDocumentListDropDown
                {
                    public const string ScreenText = "//h5[contains(text(),'Loan Document List')]";

                    internal class Buttons
                    {
                        public const string Close = "//*[text()='Close']";
                        public const string DocumentID = "";
                    }
                }
            }


        }

        internal class MakeMyPaymentsPage
        {
            internal class Input
            {
                public const string LastName = "//*[@id='ctl00_CustomContent_lastName']";
                public const string Birthdate = "//*[@id='ctl00_CustomContent_birthDate']";
                public const string LastFourSsn = "//*[@id='ctl00_CustomContent_SSN']";
            }

            internal class Button
            {
                public const string Login = "//*[@id='ctl00_CustomContent_loginButton']";
            }
        }

        internal class MemberLoginPage
        {
            internal class Input
            {
                public const string UserName = "//*[@id='ctl00_CustomContent_textUsername']";
                public const string Password = "//*[@id='ctl00_CustomContent_textPassword']";
                public const string CustomerId = "//*[@id='ctl00_CustomContent_textCustomerID']";
                public const string CustomerDob = "//*[@id='ctl00_CustomContent_SSN']";
            }

            internal class Button
            {
                public const string Next = "//*[@id='ctl00_CustomContent_btnNext']";
                public const string Login = "//*[@id='ctl00_CustomContent_logMeBtn']";
                public const string Submit = "//*[@id='ctl00_CustomContent_pnlNewCustomer']/div[5]/input";
            }

            internal class TextLink
            {
                public static string CustomerIDRecipt = "//*[@id='ctl00_CustomContent_pnlNewCustomer']";
                public const string ForgotYourPassword = "//*[@id='ctl00_CustomContent_lbLostPassword']";
            }

            internal class NavLink
            {
                public const string ApplyNow = "//*[@id='menu-item-310'][contains(.,'Apply Now')]";

            }
            internal class Message
            {
                public const string PasswordSuccessfullyResetMessage =
                    "//*[@id='ctl00_CustomContent_pnlPasswordUpdated']";
            }

            internal class ErrorMessage
            {
                public const string EmailRequired = "//*[@id='ctl00_CustomContent_rfvUsername']";
                public const string InvalidEmail = "//*[@id='ctl00_CustomContent_ctl02']";
            }

            internal class Dropdown
            {
                public const string SelectState = "//*[@id='ctl00_CustomContent_ddlState']";
            }

            internal class TopMenu
            {
                public static string UserTab = "//*[@id='loggedInMenuButton']";
            }
        }

        internal class ApplyPage
        {
            internal class ErrorMessage
            {
                public const string InputRequired = "//input[contains(@class,'ClickTaleSensitive ccoapp error')]";
                public const string LoanTypeRequired = "//label[contains(@class,'ltError')]";
                public const string SelectRequired = "//select[contains(@class,'addField ClickTaleSensitive error')]";
                public const string SelectYearRequired = "//select[@name='bankAccountLengthYear'][@class='addField ClickTaleSensitive yearMonth error']";
                public const string SelectMonthRequired = "//select[@name='bankAccountLengthMonth'][contains(@class,'addField ClickTaleSensitive yearMonth error')]";
                public const string SelectIdTypeRequired = "//select[@name='ddlIdentityType'][@class='ClickTaleSensitive error']";
                public const string SelectIdStateRequired = "//select[@name='identityState'][@class='ClickTaleSensitive error']";
                public const string ClickAgreeTextMessageRequired = "//div[@id='agreeSMSError'][@class='agreementerror']";
                public const string ClickAgreeTermRequired = "//div[@id='agreeTermsError'][@class='agreementerror']";
                public const string ClickAgreeConsentRequired = "//div[@id='agreeConsentError'][@class='agreementerror']";
                public const string SelectLivingYearRequired = "//select[@name='residenceLengthYear'][@class='addField ClickTaleSensitive yearMonth error']";
                public const string SelectLivingMonthRequired = "//select[@name='residenceLengthMonth'][@class='addField ClickTaleSensitive yearMonth error']";
            }
            internal class Input
            {
                public const string FirstName = "//input[@id='firstName']";
                public const string LastName = "//input[@id='lastName']";
                public const string Email = "//input[@id='emailAddress']";
                public const string DateOfBirth = "//input[@id='birthDate']";
                public const string ZipCode = "//input[@id='zipCode']";
                public const string PersonalLoan = "//label[contains(.,'Personal Loan (Repay Over Time)')]";
                public const string PaydayLoan = "//label[contains(.,'(Repay On Your Next Payday)')]";
                public const string MonthlyIncome = "//input[@id='monthlyIncome']";
                public const string NameOfCompany = "//input[@id='employerName']";
                public const string EmployerPhoneNumber = "//input[@id='employerPhone']";
                public const string EmployersCity = "//input[@id='employerCity']";
                public const string EmployersZipCode = "//input[@id='employerZip']";
                public const string ChooseDirectDeposit = "//label[@for='switch_firstIncome_left']";
                public const string ChooseChecking = "//label[@for='switch_bankType_right']";
                public const string AddAdditionalIncome = "//label[contains(.,'Add Additional Income')]";
                public const string FirstIncomeDepositYes = "//label[@for='switch_firstIncome_left']";
                public const string SecondIncomeDepositYes = "//label[@for='switch_secondIncome_left']";

                public const string AdditionalMonthlyIncome = "//input[@id='monthlyIncomeAdd']";
                public const string AdditionalNameOfCompany = "//input[@id='employerNameAdd']";
                public const string AdditionalPhoneNumber = "//input[@id='employerPhoneAdd']";
                public const string AdditionalEmployersCity = "//input[@id='employerCityAdd']";
                public const string AdditionalEmployersZipCode = "//input[@id='employerZipAdd']";
                public const string AdditionalChooseDirectDeposit = "//label[@for='switch_firstIncome_left']";
   
                public const string RoutingNumber = "//input[@id='routingNumber']";
                public const string BankAccountNumber = "//input[@id='bankAccount']";
                public const string ReEnterBankAccountNumber = "//input[@id='bankAccountCheck']";
                public const string IdentificationNumber = "//input[@id='identityNumber']";
                public const string ExpirationDate = "//input[@id='idExpirationDate']";
                public const string SocialSecurityNumber = "//input[@id='socialSecurity']";
                public const string StreetAddress = "//input[@id='streetAddress']";
                public const string Password = "//input[@id='createPassword']";
                public const string ChooseBankAccountForPayment = "//label[@for='switch_bankPayment_left']";
                public const string ChooseRentOption = "//label[contains(.,'Rent')]";
                public const string ContactPhoneNumber = "//input[@id='phoneNumber']";
                public const string VerificationCode = "//input[@id='verifyPhoneCode']";

                public const string AgreeTextMessages = "//input[@id='optInSMS']/following-sibling::span";
                public const string AgreePhoneCalls = "//input[@id='optInSMSMarketing']/following-sibling::span";
                public const string ContactPassword = "//input[@id='createPassword']";
                public const string AgreeTerms = "//input[@id='agreeTerms']/following-sibling::span";
                public const string AgreeConsent = "//input[@id='agreeConsent']/following-sibling::span";
                //public const string CaAgreeConsent = "//*[@id='CAAgree']";
                public const string CaAgreeConsent = "//*[contains(@id,'CAAgreement')]//span";
                public const string SubmitApplication = "//input[@id='applySubmit']";

                //*[contains(@id,'CAAgreement')]//span

            }

            internal class Text
            {
                public const string DetailedWirelessPolicy = "//*[contains(text(),'automated telephone calls, including text messages')]";
            }

            internal class Button
            {
                public const string SendVerificationCode = "//button[@id='validatePhoneBtn']";
            }
            internal class Link
            {
                public const string ClickContinue = "//a[contains(@id,'continueButton')]";
                
            }
            internal class Select
            {
                public const string SelectPayPeriod = "//select[@id='payPeriod']";
                public const string SelectDayPaid = "//select[@id='selBiWeekly']";
                public const string SelectPayDay = "//select[@id='selBiWeeklyDate']";
                public const string SelectIncomeType = "//select[@id='incomeType']";
                public const string SelectEmployersState = "//*[@id='employerState']";
                public const string SelectYear = "//select[@id='employerTimeYear']";
                public const string SelectMonth = "//select[@id='employerTimeMonth']";
                public const string SelectCheckingForBankType = "//*[@id='switch_bankType_right']";
                public const string SelectCity = "//select[@id='selectedCity']";

                public const string AdditionalSelectPayPeriod = "//select[@id='payPeriodAdd']";
                public const string AdditionalSelectDayPaid = "//select[@id='selBiWeeklyAdd']";
                public const string AdditionalSelectPayDay = "//select[@id='selBiWeeklyDateAdd']";
                public const string AdditionalSelectIncomeType = "//select[@id='incomeTypeAdd']";
                public const string AdditionalSelectEmployersState = "//select[@id='employerStateAdd']";
                public const string AdditionalSelectYear = "//select[@id='employerTimeYearAdd']";
                public const string AdditionalSelectMonth = "//select[@id='employerTimeMonthAdd']";

                public const string SelectOpenYear = "//select[@id='bankAccountLengthYear']";
                public const string SelectOpenMonth = "//select[@id='bankAccountLengthMonth']";

                public const string SelectIdentificationType = "//select[@id='ddlIdentityType']";
                public const string SelectIdentificationState = "//select[@id='identityState']";

                public const string SelectLivingYear = "//select[@id='residenceLengthYear']";
                public const string SelectLivingMonth = "//select[@id='residenceLengthMonth']";

                public const string SelectMonthlyType = "//select[@id='selMonthlyType']";
                public const string SelectDayOfMonthPaid = "//select[@id='selMonthlyByDay']";

                public const string SelectMultipleCities = "//select[@id='selectedCity']";

            }
        }
        internal class MemberPage
        {
            internal class AccountPages
            {
                public const string ClickProfile = "//*[@id='ctl00_CustomContent_hlUpdateMember'] ";
                public const string ClickReferAFriend = "//h2[@class='congratulations-title']";
            }
            internal class ScreenText
            {
                public static string BringTheFollowingToStore = "//*[@id='ctl00_CustomContent_pnlPendingStoreWidget']";
                public const string PaymentApproved = "//*[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl00_pnlLoanWidgetOutstandingApprovedLoan']/div ";
                public const string CheckPendingApplication = "//a[@id='ctl00_CustomContent_hlPendingApproval']";
                public const string MakePaymentTile = "//a[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl00_hlLoanWidgetOutstandingStoreLoanPaymentUrl']";
                public const string PendingPaydayLoanTile = "//*[@id='ctl00_CustomContent_lblPendingStoreLoanType']";
            }

            internal class TypesOfLoans
            {
                public const string PersonalLoan = "//*[@id= 'ctl00_CustomContent_repLoanWidgetOutstanding_ctl02_lblLoanWidgetOutstandingType'][contains(text(),'Personal')]";

                public const string PaydayLoan =
                    "//*[@id= 'ctl00_CustomContent_repLoanWidgetOutstanding_ctl00_lblLoanWidgetOutstandingType'][contains(text(),'Payday')]";
            }

            internal class Button
            {
                public const string ClickCheckPendingApplication = "//*[text()='Check Pending Application']";
                public const string ClickRequestPaydayLoan = "//*[@id='ctl00_CustomContent_hlNewLoanButtonsPaydayUrl']";
                public const string ClickRequestPersonalLoan = "//*[@id='ctl00_CustomContent_hlNewLoanButtonsPersonalUrl']";
                public const string ClickMakePaymentPersonalLoan = "//*[text()='Make Payment']";
                public const string ClickPaydayExtend = "//*[text()='Extend']";
                public const string ClickPaydayMakePayment = "//*[text()='Make Payment']";
                //  ctl00_CustomContent_repLoanWidgetOutstanding_ctl00_lblLoanWidgetOutstandingDeferredID
                public const string Extend = "//*[@class='d-button'][contains(.,'Extend')]";
                public const string UploadDocs = "//*[@class='for-loggedin'][@href='upload-documents.aspx']";
                public const string Refinance = "//*[contains(@id,'ctl00_CustomContent_repLoanWidgetOutstanding_ctl0')]//*[text()='Refinance']";
                //public const string Refinance =
                 //   "//*[@id='ctl00_CustomContent_repLoanWidgetOutstanding_ctl00_hlLoanWidgetOutstandingStoreLoanRefinanceUrl']";
                public const string Logout = "//a[text()='logout']";
                public const string RefinanceExtend = "//*[@id='linkExtend'] ";
                public const string RefinancePayDown = "//*[@id='linkPayDown']";
                public const string RefinanceBorrow = "//*[@id='linkBorrow']";
                public const string Dashboard = "//*[@href='Member.aspx']";
                public const string ReferFriend = "//*[@href='refer-a-friend.aspx']";


                //public const string Dashboard = "//*[@class='for-loggedin'][@href='Member.aspx']";
                //public const string ReferFriend = "//*[@class='for-loggedin'][@href='refer-a-friend.aspx']";
            }

            internal class PaymentPopupOption
            {
                public const string MakePayment = "//*[@id='paymentButton']";
                public const string PayOff = "//*[@id='payoffButton']";
            }

            internal class SideBar
            {
                public const string UserName = "//*[@id='aspnetForm']/*/div[1]/div[2]";
                public const string LoanHistory = "//*[@id='ctl00_CustomContent_hlLoanHistory']";
            }

            internal class NewLoanConfirmation
            {
                public const string MemberDashboard = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']/span[3] ";
                public const string StoreOption = "//*[contains(text(), 'Finish Your Application')]";
            }

            public class TopMenu
            {
                public static string MenuButton = "//*[contains(@id,'loggedInMenuButton')]";

                internal class MenuButtonOptions
                {
                    public const string MenuButton2 = "//*[@id='loggedInMenuButton'][@class='username']";
                    public const string Profile = "//*[@class='for-loggedin break' and 'profile.aspx']";
                    public const string ProfileNV = "//a[@class='for-loggedin break' or @href= 'profile.aspx']";
                    public const string Dashboard = "//a[@class='for-loggedin'][@href='Member.aspx']";
                    public const string LoanHistory = "//*[@href='store-loan-history.aspx']";
                    public const string UploadDocs = "//*[@class='for-loggedin'][@href='upload-documents.aspx']";
                    public const string Locations = "//*[@class='for-loggedin'][@href='locations.aspx']";
                    public static string ReferFriend = "//*[@href='refer-a-friend.aspx']";
                    public const string Logout = "//a[@href='MemberLogin.aspx']";
                    internal class LocationOptions
                    {
                        public const string Utah = "//*[text()= 'Utah']";
                        public const string Nevada = "//*[text()= 'Nevada']";
                        public const string Colorado = "//*[text()= 'Colorado']";
                        public const string Virginia = "//*[text()= 'Virginia']";
                    }
                }
                
            }

            internal class ReferAFriend
            {
                public static string TermsAndConditions = "//*[@href='online-referral-terms-and-conditions.aspx']";
            }
        }

        internal class DocumentUploadServices
        {
            internal class ScreenText
            {
                public const string DocumentUploadServices = "//*[text()='Upload Docs for Online Loans']";
            }
        }

        internal class ThankYouPaymentPage
        {
            public const string ScreenText = "//*[text()='Thank you for your payment!']";
            public const string BackToDashboard = "//*[text()='Back to Dashboard']";
        }
        internal class StoreLoanHistory
        {
            public const string BreadCrumb = "//span[contains(text(),'Store Loan History')]";
            public const string Terms = "//tr[1]//td[4]//a[1]";

            internal class LoanDocumentListPopup
            {
                public const string Close = "//*[text()= 'Close']";
                public const string TableRow = "//*[@class= 'table-row']";

                internal class LoanDocumentsPopup
                {
                    public const string Close = "//button[@class='btn btn-primary left']";
                    public const string Print = "//*[text()= 'Print']";
                }
            }
        }
        internal class StoreProfile
        {
            
            internal class Buttons
            {
                //public const string UpdateProfile = "//*[text()= 'UPDATE PROFILE']";
                public const string ChangePassword = "//*[text()='Change Password']";
                public const string SaveChanges = "//*[@id='ctl00_CustomContent_btnSubmit']";
                public const string ReceiveNoticeAboutFuturePaymentsByEmail = "//*[@name='emailOptSwitchOn']";
                public const string NoNoticeAboutFuturePaymentsByEmail =
                    "//*[@name='emailOptSwitchOn']//following::input";
            }
            internal class AreYouSure
            {
                internal class Buttons
                {
                    public const string NoSendNoticesByMail = "//*[contains(text(),'No, send me notices')]";
                    public const string EnrollMeInEmailNotices = "//*[contains(text(),'Enroll me in email notices!')]";
                }
            }

            internal class InputFields
            {
                public const string EmailAddress = "//*[@id='ctl00_CustomContent_FOldEmailAddress_DataDisplay']";
            }

            internal class ScreenText
            {
                public const string ProfileUpdateSuccessful = "//*[@id='ctl00_CustomContent_ErrorMessage']";
                public const string AccountPages = "//*[contains(text(), 'Member Information')]";
                public const string CustomerId = "//div[@id='profileDisplay']//div[@class='data']";
                public const string WouldYouLikeToReceiveNoticeAboutFuturePaymentsByEmail = "//*[@id='optIn']";
            }

            public static string ProfilePanel = "//h2[text()='Profile']";
        }

        internal class StoreProfileEdit
        {
            internal class Buttons
            {
                
                
                public const string SaveChanges = "//*[@id='ctl00_CustomContent_btnSubmit']";
                public const string Cancel = "//*[text()='Cancel']";
                public const string YesForFuturePayments = "//*[@id='optIn']";
            }

            internal class InputFields
            {
                public const string EmailAddress = "//*[@name='ctl00$CustomContent$newEmail'][@id='ctl00_CustomContent_newEmail']";
                public const string PhoneNumber = "//*[@id='ctl00_CustomContent_newPhone']";
            }

            internal class ScreenText
            {
                public const string AccountPages = "//*[contains(text(), 'Member Information')]";
            }
        }

        internal class StoreChangePassword
        {
            internal class InputFields
            {
                public const string NewPassword = "//*[contains(@name,'ctl00$CustomContent$NewPassword')]"; // ctl00$CustomContent$NewPassword
                public const string ConfirmPassword = "//*[contains(@name,'ctl00$CustomContent$PasswordConfirm')]"; // ctl00$CustomContent$PasswordConfirm

            }

            internal class Buttons
            {
                public const string Change = "//*[@value='Change']";
                public const string SaveChanges = "//input[@id='ctl00_CustomContent_submitBtn']";
            }
        }

        internal class ExtensionType
        {
            public const string ScreenText = "//*[contains(text(),  'Extend Loan')]";
            public const string DateField = "//input[@id='ctl00_CustomContent_txtDueDate']";
            public const string ExtensionDueDate = "//*[@class='calweek']//following::a[19]";
            public const string NextMonth = "//a[@id='calnext']"; 
            public const string PaymentMethodACH = "//label[contains(text(),'ACH')]//span[@class='radiomark']";
            public const string PaymentMethodDebitCard = "//label[contains(text(),'Debit')]//span[@class='radiomark']";
            public const string Continue = "//input[@id='ctl00_CustomContent_ImageButton1']";
            public const string CallNext = "//*[@id='calnext']";
            public const string PayFrequency = "//*[@id='ctl00_CustomContent_selPayPeriod']";
            public const string PayPeriod = "//*[text()='Bi-Weekly']";
            public const string ClickNext = "//*[text()='Next']";
            public const string PaymentPlanScreenText = "//h2[@class='active-blk extendAttribute fppTrueAttribute']";
            public const string SelectDateButton = "//*[@name='ctl00$CustomContent$txtDueDate'][@id='ctl00_CustomContent_txtDueDate']";

        }
        internal class StoreExtension
        {
            public const string BreadCrumb = "//*[text()= 'Store Extension']";
            public const string DateField = "//input[@id='ctl00_CustomContent_txtDate']";
            public const string ExtensionDueDate = "//*[@id='calweeks']/div[2]/a[6]";
            public const string AchPaymentMethod = "//*[@type= 'radio'][@value='ACH']";
            public const string DebitPaymentMethod = "//*[@type= 'radio'][@value='CRE']";
            public const string Continue = "//input[@id='ctl00_CustomContent_ImageButton1']";
            public const string CallNext = "//*[@id='calnext']";
            public const string PayFrequency = "//*[@id='ctl00_CustomContent_selPayPeriod']";
            public const string PayPeriod = "//*[text()='Bi-Weekly']";
        }

        internal class ExtensionAccept
        {
            public const string BreadCrumb = "//*[text()= 'Store Extension Accept']";
            public const string MakePayment = "//*[@name= 'ctl00$CustomContent$ImageButton2']";
            public const string SignatureField = "//*[@id='ctl00_CustomContent_txtSignACHProvisions']";
            public const string FirstPaymentPlanSignatureField = "//*[@name='ctl00$CustomContent$pp_inpBorrowerSignature']";
            public const string ClickAcceptButton = "//*[text()='Accept']";
            public const string ClickIAgreeButton = "//*[text()='I Agree']";
            public const string SecondPaymentPlanSignatureField =
                "//input[@name='ctl00$CustomContent$txtSignCheckProvisions']";
            public const string ClickPaymentPlanButton = "//*[@name='ctl00$CustomContent$btnPaymentPlan']";
            

            public const string FirstExtendedPaymentPlanSignature =
                "//input[@id='ctl00_CustomContent_PayPlanBorrowerSignature']";

            public const string SecondExtendedPaymentPlanSignature =
                "//input[@id='ctl00_CustomContent_txtSignExtensionAgreement']";
        }

        internal class PaymentThankYou
        {
            internal class Buttons
            {
                public const string GotItButton = "//*[text()='Got it!']";
            }
        }
        internal class ExtensionTransact
        {
            internal class Buttons
            {
                public const string IAgree = "//*[text()='I Agree']";
                public const string IDoNotAgree = "//*[text()='I Do Not Agree']";
            }
        }
        internal class StoreExtensionMessage
        {
            internal class MakePaymentDueDateConfirmation
            {
                public const string YesWithoutMovingTheDueDate = "//*[text()='Yes, continue without extending the due date. ']";
                public const string NoIWantToMoveTheDueDate = "//label[contains(text(),'No, I want to extend the due date.')]";
            }

            public const string ScreenText = "//*[text()= 'Store Customer']";
            public const string SaveAndContinue = "//input[@id='ctl00_CustomContent_btnSubmitNoExtension']";
            public const string DueDateConfirmation = "//*[text()= 'Make Payment: Due Date Confirmation']";
            public const string FinalizeYourInstallmentPayoffPayment = "//*[text()='Finalize Your Installment Payoff Payment']";
            public const string MakePayment = "//*[@id='ctl00_CustomContent_ImageButton2']";
        }
        internal class UpdateMember
        {
            internal class ScreenText
            {
                public const string MemberProfile = "//*[@id='userMenu']/div/a[3][contains(.,'Profile')]";
            }
            public const string UpdateButton = "//*[@id='profileDisplay']//*[contains(@onclick,'toggleEditMode(1)')]";
            public const string MemberInformation = "//h4[contains(.,'Member Information')]";
            internal class LinkButton
            {
                public const string ChangePassword = "//*[text()='Change Password']";
                public const string ChangeEmailCA = "//*[text()='Change Email']";
                public const string ChangeEmail = "//*[@id='ctl00_CustomContent_newEmail']";
                public const string ChangeAddress = "//*[@id='ctl00_CustomContent_FEditAddress']";
            }

            internal class Link
            {
                public const string EditIncome = "//*[contains(text(), 'Edit Income')]";
                public const string EditPhone = "//*[@id='ctl00_CustomContent_FEditPhone']"; 
            }
        }

        internal class UpdateEmail
        {
            internal class ScreenText
            {
                public const string ChangeEmail = "//*[@id='ctl00_CustomContent_newEmail']";

                public const string EmailUpdateConfirmation =
                    "//*[@id='ctl00_CustomContent_FNewEmailAddress_ErrorDisplay']/b";

                public const string InvalidEmailAddressError = "//*[@id='ctl00_CustomContent_FNewEmailAddress_ErrorDisplay']";
            }

            internal class Input
            {
                public const string CurrentEmailAddress = "//*[@id='ctl00_CustomContent_FOldEmailAddress_DataDisplay']";
                public const string NewEmailAddress = "//*[@id='ctl00_CustomContent_FNewEmailAddress_DataDisplay'] ";
                public const string ConfirmEmailAddress = "//*[@id='ctl00_CustomContent_FNewEmailAddressAgain_DataDisplay'] ";

                //public const string UpdatedEmailAddress = "//input[contains(@id,'newEmail')]";
            }

            internal class Button
            {
                public const string UpdateEmailAddress = "//*[@id='ctl00_CustomContent_FSubmitEmailAddress'] ";
            }
        }

        internal class UpdateMemberInfo
        {
            internal class ScreenText
            {
                public const string UpdateInfoBreadCrumb = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']/span[5]";
                public const string UpdateInfoText = "//*[@id='profileEdit']//*[contains(@onclick,'toggleEditMode(0)')]";
                public static string PhoneNumber = "//*[contains(@class,'message-sent')]//*[contains(text(),'(801) 893-2295')]";
            }

            internal class Input
            {
                public const string Phone = "//*[@id='ctl00_CustomContent_FPhone_FHomePhone_DataDisplay'] ";
                public const string Address = "//*[@id='ctl00_CustomContent_FAddress_FAddress_DataDisplay']";
                public const string State = "//*[@id='ctl00_CustomContent_FAddress_FAddressStateID_DataDisplay']";
                public const string City = "//*[@id='ctl00_CustomContent_FAddress_FCity_DataDisplay']";
                public const string ZipCode = "//*[@id='ctl00_CustomContent_FAddress_FZip_DataDisplay']";

                public const string NewPhone = "//*[contains(@name,'ctl00$CustomContent$newPhone')]";
                public const string MembersAddress = "//input[contains(@id,'newStreet1')]";
                public const string MembersCity = "//input[contains(@id,'newCity')]";
                public const string MembersState = "//*[contains(@name,'ctl00$CustomContent$newState')]";
                public const string MembersZipCode = "//input[contains(@id,'newZip')]";
            }
            internal class Button
            {
                public const string Submit = "//*[@id='ctl00_CustomContent_FSubmit']";
                public const string MembersSubmit = "//input[contains(@id,'btnSubmit')]";
                public static string SendCode = "//*[contains(@name,'validatePhoneBtn')]";
                public static string SaveChanges = "//*[contains(@name,'ctl00$CustomContent$btnSubmit')]";
            }
        }

        internal class UpdatePassword
        {
            internal class ScreenText
            {
                public const string ChangePassword = "//*[@id='aspnetForm']/div[3]/div/div[2]/fieldset/h4";
                public const string ValidatedPassword = "//*[@id='ctl00_CustomContent_FNewPassword_ErrorDisplay']/b ";
            }

            internal class Input 
            {
                public const string CurrentPassword = "//*[@id='ctl00_CustomContent_FOldPassword_DataDisplay']";
                public const string NewPassword = "//*[@id='ctl00_CustomContent_FNewPassword_DataDisplay']";
                public const string ConfirmPassword = "//*[@id='ctl00_CustomContent_FNewPasswordAgain_DataDisplay']";
            }

            internal class Button
            {
                //public const string UpdatePassword = "//*[@id='ctl00_CustomContent_FSubmit'] ";
                public const string UpdatePassword = "//*[@value='Update Password']";
                public const string Logout = "//*[text()='logout']";
            }
        }
        internal class UpdatePasswordNV
        {
            internal class ScreenText
            {
                public const string ChangePassword = "//*[@id='aspnetForm']/div[3]/div/div[2]/fieldset/h4";
                public const string ValidatedPassword = "//*[@id='ctl00_CustomContent_FNewPassword_ErrorDisplay']/b ";
            }

            internal class Input
            {
                public const string CurrentPassword = "//input[@id='ctl00_CustomContent_FOldPassword_DataDisplay']";
                public const string OldPassword = "//input[@id='ctl00_CustomContent_txtOldPassword']";
                public const string NewPassword = "//input[@id='ctl00_CustomContent_FNewPassword_DataDisplay'] ";

                public const string ConfirmPassword = "//input[@id='ctl00_CustomContent_FNewPasswordAgain_DataDisplay'] ";

            }

            internal class Button
            {
                public const string UpdatePassword = "//*[@id='ctl00_CustomContent_FSubmit'] ";
                public const string Logout = "//*[text()='logout']";
            }
        }
       
        internal class NewLoan
        {
            internal class EnterLoanAmount
            {
                public const string LoanAmount = "//*[@id='ctl00_CustomContent_FLoanAmount_DataDisplay'] ";
            }

            internal class MakePaymentFrom
            {
                public const string CheckingAccount = " //*[@id='ctl00_CustomContent_AOffsetAccount_DataDisplay']";
            }

            internal class Input
            {
                public const string SignHere = "//*[@id='ctl00_CustomContent_FSignature_DataDisplay'] ";
            }

            internal class Button
            {
                public const string ClickIAgreeContinueButton = "//*[@id='ctl00_CustomContent_FSubmit']";
            }
        }

        internal class LoanDisclosure
        {
            internal class Input
            {
                public const string NameContractSignature = "//input[@name='AgreementSignature1']";
                public const string NameTermsAndDisclosures = "//input[@id='loandoc1']";

            }

            internal class Button
            {
                public const string ClickUtahDeferredLoanAgreement = "//a[@id='loanbutton1']";
            }
            internal class Message
            {
                public const string InlineAudioMessage = "//*[@id='ctl00_CustomContent_ReviewTerm']/div[1]";
            }
        }

        internal class ThankYou
        {
            internal class ScreenText
            {
                public const string CurrentLoanStatus = "//*[@id='content']/div/p[1]";
                public const string CurrentLoanStatusPendingApproval = "//p[contains(text(),'Current Loan Status:')]";
            }

            internal class Button
            {
                public const string ClickOpinionCounts = " //*[@id='sa_close']";
                public const string LoanAggreement = "//*[@id='mainBody']/div[1]/div/div/div/ul/li[1]";
                //public const string ClickOpinionCounts = "//a[@class='dsb'][contains(.,'Cancel')]";
                //public const string EndMySession = "//a[text()='End My Session']";
                public const string EndMySession = "//a[contains(@class,'btn btn-success')]";
                public const string EndMySessionNow = "//a[text()='End My Session']";
                public const string MyAccount = "//*[@id='loginTab']/span[3]/a[1]";

            }

        }

        internal class LostPassword
        {
            internal class Input
            {
                public const string EmailInput = "//*[@id='ctl00_CustomContent_FEmailAddress']";
                public const string DateOfBirthInput = "//*[@id='ctl00_CustomContent_Birthdate']";

            }
            internal class Button
            {
                public const string Submit = "//*[@id='ctl00_CustomContent_Button1']";
                public const string ForgotPassword = "//*[@id='ctl00_CustomContent_lbLostPassword']";
                public const string SubmitAfterSsnChallenge = "//*[@id='ctl00_CustomContent_btnResetPassword']";

            }

            internal class Error
            {
                public const string DateOfBirthRequired = "//*[@id='ctl00_CustomContent_ctl04']";
                public const string DateOfBirthRequiredText = "* Date of Birth is required";
                public const string BirthDateNotFound = "//*[@id='ctl00_CustomContent_FPanel']/span";
            }


        }
        internal class MemberSecurity
        {
            internal class Input
            {
                public const string Question1 = "//*[@id='ctl00_CustomContent_secA1']";
                public const string Question2 = "//*[@id='ctl00_CustomContent_secA2']";
                public const string SocialSecurity = "//*[@id='ctl00_CustomContent_textConfirmSSN']";
            }

            internal class Button
            {
                public const string Submit1 = "//*[@id='ctl00_CustomContent_Button1']";
                public const string Submit2 = "//*[@id='ctl00_CustomContent_Button2']";
            }

            internal class ScreenMessage
            {
                public const string QuestionOne = "//*[@id='ctl00_CustomContent_FLoginInformation']";
            }

            internal class PageText
            {
                public const string Question1 = "//*[@id='ctl00_CustomContent_secQ1']/b";
                public const string Question2 = "//*[@id='ctl00_CustomContent_secQ2']/b";
            }
            internal class Attribute
            {
                internal class SecurityQuestion
                {
                    public const string Option1 = "What is the name of your first pet?";
                    public const string Option2 = "What is your favorite book?";
                    public const string Option3 = "What was the make of your first car?";
                    public const string Option4 = "What is your favorite color?";
                    

                }
            }
        }

        internal class ResetMyPassword
        {
            internal class Input
            {

                public const string NewPassword = "//input[@id='ctl00_CustomContent_NewPassword']";
                public const string ConfirmPassword = "//input[@id='ctl00_CustomContent_PasswordConfirm']";
            }

            internal class Button
            {
                public const string ChangePassword = "//*[contains(@name,'ctl00$CustomContent$submitBtn')]";
                public const string SaveChanges = "//input[@id='ctl00_CustomContent_submitBtn']";
                public const string TogglePassword = "//*[@class='icon inactive']";
            }
        }

        internal class SideBarMenu
        {
            //public const string ReferAFriend = "//*[text()='Refer A Friend']";
            public const string ReferAFriend = "//a[@href='Referral.aspx'][contains(.,'Refer A Friend»')]";
        }

        internal class StoreBankEdit
        {
            internal class ScreenText
            {
                public const string ConfirmBankAccount = "//*[@id='aspnetForm']/h2";
                public const string WouldYouLikeToReceiveNoticeAboutFuturePaymentsByEmail = "//*[@id='optIn']";
            }

            internal class AreYouSure
            {
                internal class Buttons
                {
                    public const string NoSendNoticesByMail = "//*[contains(text(),'No, send me notices')]";
                    public const string EnrollMeInEmailNotices = "//*[contains(text(),'Enroll me in email notices!')]";
                }
            }

            internal class Button
            {
                public const string LoanPaymentsYes = "//*[@id='switch_bankPayment_left']//following::label[1]";//yes
                public const string LoanPaymentsNo = "//*[@id='switch_bankPayment_right']//following::label[1]";
                public const string ConfirmBankAccount = "//*[@id='confirmBank']";
                public const string ACHLoanPaymentYes = "//*[@id='ctl00_CustomContent_pnlBankAccount']/div/div[2]/div/label[1]";
                public const string ReceiveNoticeAboutFuturePaymentsByEmail = "//*[@name='emailOptSwitchOn']";
                public const string Cancel = "//*[@class='btn'][text()='Cancel']";

                public const string NoNoticeAboutFuturePaymentsByEmail =
                    "//*[@name='emailOptSwitchOn']//following::input";

            }

            internal class DropDown
            {

                internal class Button
                {
                    public const string ClickEnrollMeInAutoPay = "//*[@id='bankPaymentConfirm']/div[3]/a[1]";
                    public const string ClickContinueWithOutEnrollingInAutoPay = "//*[@id='optOut']";
                    public const string Years = "//*[@id='bankAccountLengthYear']";
                    public const string Months = "//*[@id='bankAccountLengthMonth']";
                    public const string SelectWeeklyDate = "//*[@id='selWeeklyDate']";
                    public const string SelectFirstOption = "//*[@id='selWeeklyDate']/option[2]";
                }
            }

            internal class Input
            {
                public const string RoutingNumber = "//*[@id='routingNumber']";
                public const string BankAccountNumber = "//*[@id='bankAccount']";
                public const string ReEnterBankAccountNumber = "//*[@id='bankAccountCheck']";

            }

            internal class Radio
            {
                public const string OptOut = "//*[@id='optOut']";
                public const string OptIn = "//*[@id='optIn']";
                
            }

            internal class Popup
            {
                internal class Button
                {
                    public const string Close = "//*[@class='btn close']";
                    public const string LeftSideOption = "//*[@class='left']";
                }
            }
        }

        internal class LoanApproval
        {
            
            
            public const string FinishYourApp = "//*[text()= 'Finish Your Application']";
            public const string ScreenText = "//*[@class= 'congratulations-approval-text']";
            public const string Congratulations = "//*[contains(@class,'congratulations-container')]";


            internal class ScreenTextOnPage
            {
                public const string DeclinedAtThisTime = "we have declined your credit application at this time";
                public const string TurnedDown = "We cannot give you a loan at this time";
                public const string YourAlmostDone = "You're Almost Done!";
                public const string Congratulations = "Congratulations!";
                public const string FinishYourApplication = "Finish Your Application";
            }
            public const string YourAlmostDone = "//h2[contains(text(),'Almost Done!')]";
            public const string RefinanceLoanDetailsPageScreenText = "//h2[contains(text(),'Refinance Loan Details')]";

            internal class Buttons
            {
                public const string InStore = "//*[text()= 'In Store']";
                public const string Online = "//*[text()= 'Online']";
            }

            internal class Link
            {
                public const string Logout = "//*[text()='logout']";
                public const string Account = "//*[text()='account']";
            }
        }
        internal class LoanAtStore
        {
            internal class ScreenText
            {

                public const string FinishMyApp = "//*[contains(text(), 'Finish my Application at the ')]";
                public const string SeeYouIn = "//*[contains(text(), 'See You In')]";
            }

            internal class Buttons
            {
                public const string Cancel = "//*[text()= 'Cancel']";
                public const string SendMyApp = "//*[contains(text(), 'Send My Application to')]";
                public const string Location = "//select[@id='selStore']";
                public const string GotIt = "//*[text()= 'Got It!']";
                
            }
           
            
        }
        internal class PayDayLoan
        {

            internal class PaydayExtension
            {

                internal class Input
                {
                    public const string PrincipalPayment = "//*[@id='ctl00_CustomContent_FPaymentAmount_DataDisplay'] ";
                    public const string DesiredDueDate = "//input[@name= 'ctl00$CustomContent$txtDate'][@class='date']";
                }

                internal class ScreenText
                {
                    public const string StoreCustomerExtension = "//*[text()= 'Store Customer']";

                }

                internal class PaymentInformation
                {
                    public const string PaymentFromChecking = "//*[text()=' ACH (Free)']";
                }

                internal class Button
                {
                    public const string Continue = "//input[@id='ctl00_CustomContent_ImageButton1']";
                }
            }
            internal class PayDayPayOff
            {
                internal class ScreenText
                {
                    public const string PayOff = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']/span[5]";

                }

                internal class PaymentInformation
                {
                    public const string PaymentFromChecking = "//*[@id='ctl00_CustomContent_FPaymentLocation']";
                }

                internal class WithdrawalDate
                {
                    public const string EnterDate = "//*[@id='ctl00_CustomContent_FPaymentDate_DataDisplay']";
                }

                internal class Button
                {
                    public const string Next = "//*[@id='ctl00_CustomContent_FSubmit']";
                }

                internal class PaymentAccept
                {
                    public const string BreadCrumb = "//*[text()= 'Payment Accept']";

                    internal class Buttons
                    {
                        public const string IAgree = "//*[@id='ctl00_CustomContent_bankPayment']//a[contains(text(),'I Agree')]";
                        public const string Next = "//*[@id='formBtns']/a[text()='Next']";
                        public const string Accept = "//*[@id='aspnetForm']/div[4]/a[2]";
                        public const string MakeAPayment = "//input[@id='ctl00_CustomContent_ImageButton2']";
                        public const string Decline = "//a[@id='ctl00_CustomContent_imgBtnDecline']//img";
                    }
                }

            }

            internal class StorePayments
            {
                public const string Next = "//*[@id='formBtns']/a[text()='Next']";
                public const string PaymentAmount = "//input[@id='ctl00_CustomContent_txtPayment']";
                public const string BreadCrumb = "//h2[contains(text(),'Store Customer')]";
                public const string MakePaymentHeadline = "//h2[contains(text(),'Make Payment')]";
            }
            internal class PaymentTransact
            {
                internal class ScreenText
                {
                    public const string TransactionInfo = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']/span[5] ";
                    public const string TotalPayment = "//*[@id='ctl00_CustomContent_FTotalPayment']/table/tbody/tr/td[2]/span ";
                }

                internal class Input
                {
                    public const string EnterSignature = "//*[@id='ctl00_CustomContent_FSignature_DataDisplay']";
                }

                internal class Button
                {
                    public const string ClickAgreeToTransaction = "//*[text()= 'I Agree'] ";
                    public const string ClickNext = "//*[@id='ctl00_CustomContent_FSubmit'] ";
                }

            }

            
            internal class PaydayPayment
            {
                internal class ScreenText
                {
                    
                    public const string Payment = "//*[@id='ctl00_CustomBreadcrumbs_SiteMap1']/span[5] ";
                }

                internal class PaymentType
                {
                    public const string ACH = "//label[contains(text(),'ACH')]";
                }

                internal class Input
                {
                    public const string PaymentDate = "//*[@id='ctl00_CustomContent_FPaymentDate_DataDisplay']";
                    public const string PaymentAmount = "//input[@id='ctl00_CustomContent_txtPayment'] ";
                }

                internal class Button
                {
                    public const string Next = "//*[text()='Next']";
                }
            }
        }

        internal class Loans
        {
            internal class LoanApproval
            {
                internal class ScreenText
                {
                    public const string RefinanceLoanDetails = "//h2[contains(text(),'Refinance Loan Details')]";
                    public const string Congratulations = "//*[text()= 'Congratulations!']";
                    public const string TurnDownLead = "//*[contains(text(), 'we have declined')]";
                    public const string PastDue = "//*[text()='Past Due']";
                    public const string TurnDown = "//p[contains(text(),'We cannot give you a loan at this time. Please app')]";
                    public const string LoanApprovalDate = "//*[@id='ctl00_CustomContent_repLoanWidgetFunding_ctl00_lblLoanWidgetFundingDate']";
                }
                internal class Input
                {
                    public const string EnterAmount = "//*[contains(@id, 'txtLoanAmount')]";
                    public const string EnterPayDownAmount = "//*[@id='txtPaymentAmount']";
                    public const string NewLoanAmount = "//*[contains(@id, 'newLoanAmount')]";
                    public const string NevadaLoanAmount = "//*[@id='ctl00_CustomContent_FLoanAmount_DataDisplay']";
                }

                internal class Button
                {
                    public const string GetAmount = "//*[@id='btnCreateLoan']";
                    public const string Extend = "//*[@id='btnExtendLoan']";
                    public const string Update = "//*[text()='Update']";
                    public const string Continue = "//*[@class='dsb']";
                }
            }
            internal class ThankYouPage
            {
                public const string ScreenText = "//*[@id='tertiary']//div[contains(text(), 'Thank you!')]";
                public const string ThankYouForYourPayment = "//*[text()='Thank you for your payment!']";
                //Thank you!
                public const string Button = "//*[text()='Back to Dashboard']";
                public const string GotItButton = "//*[text()='Got it!']";
            }
            internal class PersonalLoanDetails
            {
                internal class ScreenText
                {
                    public const string LoanDetails = "//*[text()='Loan Details']";
                }

                internal class Button
                {
                    public const string GetNow = "//*[@id='aspnetForm']/div[3]/div/div[4]/a[1]";
                    public const string PayAmountNow = "//*[@class='cash-variable-block']";
                    public const string Continue = "//*[@class='dsb']";
                    public const string EditLoanAmount = "//*[@class= 'fas fa-pencil-alt']";
                    public const string ConfirmNewLoanAmount = "//*[@class= 'fas fa-check']";
                    public const string ExtendNow = "//*[text()= 'Extend Now']";
                }

                internal class InputField
                {
                    public const string AmountField = "//input[@id='newLoanAmount']";
                }
            }

            internal class LoanDisclosure
            {
                internal class ScreenText
                {
                    public const string ReviewAndSign = "//h2[@class='rs-title']";

                }

                internal class Input
                {
                    public const string SignatureAgreementOne = "//*[@id='loandoc1']";
                    public const string SignatureAgreementTwo = "//*[@id='loandoc2']";
                    public const string SignatureAgreementThree = "//*[@id='loandoc3']";
                    public const string SignatureAgreementFour = "//*[@id='loandoc4']";
                    public const string SignatureAgreementFive = "//*[@id='loandoc5']";
                    public const string SignYourName = "//*[contains(@class,'rs-sign-here')]";
                    public const string NevadaSignature = "//*[@id='ctl00_CustomContent_FSignature_DataDisplay']";
                    public const string SignNevadaLoanDisclosure = "//*[contains(@name,'AgreementSignature1')]";
                }

                public class Button
                {
                    public const string SignatureAgreementOne = "//*[@id='loanbutton1'] ";
                    public const string SignatureAgreementTwo = "//*[@id='loanbutton2']";
                    public const string SignatureAgreementThree = "//*[@id='loanbutton3']";
                    public const string SignatureAgreementFour = "//*[@id='loanbutton4']";
                    public const string SignatureAgreementFive = "//*[@id='loanbutton5']";
                    public const string IAgree = "//*[text()='I Agree']";

                    public const string SignUtahDeferredLoanAgreement =
                        "//*[text()='Sign Utah Deferred Loan Agreement']";

                    public const string SignKansasConsumerLoanAgreement =
                        "//*[text()='Sign Kansas Consumer Loan Agreement']";

                    public const string SignKansasNoticeToConsumer = "//*[text()='Sign Kansas Notice To Consumer']";
                    public const string SignPersonalLoanAgreement = "//*[text()='Sign Personal Loan Agreement']";
                    public const string SignAchAuthorization = "//*[contains(text(),'Sign ACH Authorization')]";
                    public const string SignWisconsinAchAuthorization =
                        "//*[contains(text(),'Sign Wisconsin ACH Authorization')]";
                    public const string SignWisconsinLoanAgreement = "//*[text()='Sign Wisconsin Loan Agreement']";
                    public const string ButtonOne = "//*[contains(@id,'loanbutton1')]";
                    public const string ButtonTwo = "//*[contains(@id,'loanbutton2')]";
                    public const string ButtonThree = "//*[contains(@id,'loanbutton3')]";
                    public const string ButtonFour = "//*[contains(@id,'loanbutton4')]";
                    public const string ButtonFive = "//*[contains(@id,'loanbutton5')]";
                    public const string NevadaIAgreeButton = "//*[@id='ctl00_CustomContent_FSubmit']";

                    public const string NevadaDisclosureIAgreeButton =
                        "//*[@id='ctl00_CustomizableContentPlaceHolder1_FSubmit']";
                }
            }

            internal class DocumentUpload
            {
                internal class ScreenText
                {
                    public const string AlmostDone = "//*[@id='ctl00_CustomContent_uploadFilesPage']";
                }

                internal class Button
                {
                    public const string Browse = "//*[@id='fileIDList']";
                    public const string Submit = "//*[@id='ctl00_CustomContent_btnUpload']";
                    public const string AccountLink = "//*[text()='account']";
                }
            }
            internal class PersonalLoanThankYou
            {
                internal class ScreenText
                {
                    public const string CurrentLoanStatus = "//*[text()='Current Loan Status:']";
                }

                internal class Button
                {
                    public const string ClickXOnSurvey = "//*[@id='sa_close']";
                    public const string ClickEndMySession = "//*[text()='End My Session']";
                }

            }


            

            internal class StoreInstallmentPayment
            {
                internal class ScreentText
                {
                    public const string StoreCustomerInstallmentPayment = "//*[text()= 'Store Customer'] ";
                    public const string PayOffAmount = "//*[@id='ctl00_CustomContent_txtPayoff'] ";
                }

                internal class SelectYourPaymentMethod
                {
                    public const string Ach = "//*[@id='ctl00_CustomContent_ACH']";
                    public const string Debit = "//*[@id='ctl00_CustomContent_CRE']";
                }

                internal class PaymentAmount
                {
                    public const string EnterAmount = "//*[@id='ctl00_CustomContent_txtPayment']";
                }

                internal class Button
                {
                    public static string AchRadio = "//*[@id='aspnetForm']//label[contains(text(), 'ACH')]";
                    public const string Continue = "//*[@id='ctl00_CustomContent_ImageButton1']";
                    public const string PaymentMethodAch = "//*[@id='ctl00_CustomContent_ACH'] ";
                    public const string PaymentMethodDebit = "//*[@id='ctl00_CustomContent_CRE']";
                }

                internal class Input
                {
                    public const string PaymentAmount = "//*[@id='ctl00_CustomContent_txtPayment'] ";
                }

            }

            internal class StoreInstallmentPaymentAccept
            {
                internal class ScreenText
                {
                    public const string StoreInstallmentPaymentAccept = "//*[contains(text(),'Your Installment Payoff Payment')] ";
                    public const string StorePaymentAccept = "//span[contains(text(),'Store Payment Accept')]";
                    public const string OneTimePaymentAccept = "//*[contains(text(),'ONE-TIME ONLINE PAYMENT AUTHORIZATION')]";
                    //ONE-TIME ONLINE PAYMENT AUTHORIZATION
                }

                internal class Button
                {
                    public const string MakePayment = "//*[@id='ctl00_CustomContent_ImageButton2']";
                    public const string Accept = "//*[@id='aspnetForm']/div[4]/a[2]";
                    public const string Iagree = "//a[contains(text(),'I Agree')]"; 
                }

                internal class RepayDebitCardWidget
                {
                    internal class InputFields
                    {
                        public const string CardholderName = "//*[@name='cardholder_name']";
                        public const string CardNumber = "//*[@name='card_number']";
                        public const string SecurityCode = "//*[@name='card_cvc']";
                        public const string ExpirationMonth = "//*[@name='card_expiration_month']";
                        public const string ExpirationYear = "//*[@name='card_expiration_year']";
                        public const string ZipCode = "//*[@name='address_zip']";
                        public const string PayButton = "//*[@class='btn btn-lg btn-success btn-block']";


                    }

                    internal class Buttons
                    {
                        public const string Pay = "//*[contains(text(),'Pay']";
                    }
                }
            }
            

        }
        internal class IncomeError
        {
            internal class ScreenText
            {
                public const string UpdateIncome = "//*[contains(text(), 'Update Income')]"; 
            }
        }

        internal class Security
        {
            internal class Question
            {
                public const string One = "//*[@id='ctl00_CustomContent_A1']";
                public const string Two = "//*[@id='ctl00_CustomContent_A2']";
                public const string Three = "//*[@id='ctl00_CustomContent_A3']";
                public const string Four = "//*[@id='ctl00_CustomContent_A4']";
                public const string Submit = "//*[@id='ctl00_CustomContent_secCreateSubmit']";
            }
        }

        internal class StoreRegistration
        {
            internal class ScreenText
            {
                public const string ClickToLogin = "//*[text()= 'Click here to login']";
            }

            internal class Input
            {
                public const string Password = "//*[@id='ctl00_CustomContent_txtPassword']";
            }

            internal class Button
            {
                public const string Register = "//*[@id='ctl00_CustomContent_FSubmitRegistration']";
            }
        }
        public const string DefaultPassword = "Diehard1!";
    }
}