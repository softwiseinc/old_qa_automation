﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using AutomationResources;

namespace CheckCityOnline
{
    public partial class Users
    {
        private static string BuildCustomerIdQuery(string lastName)
        {
            return "select top 1 c.id CusID from Contact c where LastName = '" + lastName +
                   "' and isNull (MiddleName, '') != 'Utilized' AND NOT EXISTS (SELECT 1 FROM Deferred d WHERE d.Customer_ID = c.ID)";
        }


        private static string BuildLoanPendingCustomerQuery()
        {
            string benuSqlLink = null;
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    benuSqlLink = "benusql01.ccoc7test.com";
                    break;
                case SystemUnderTest.STAGE:
                    benuSqlLink = "ccoc7sql02-test.ccoc7.com";
                    break;
            }

            return
                "Select Distinct ccoAlt.email Email,ccoAlt.pwd Password from CustomerApplication ca " +
                "JOIN CustomerApplicationWorkflowProcess cawp ON ca.ID = cawp.CustomerApplication_ID " +
                "JOIN WorkflowProcess wp ON cawp.Process_ID = wp.ID JOIN Contact c on ca.Contact_ID = c.ID " +
                "JOIN (select * from[" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] ) ccoAlt " +
                "ON c.ID = ccoAlt.ccoid and c.Birthday = ccoAlt.birthdate " +
                "JOIN (SELECT ID, DATEDIFF(Hour,CreatedDate, (GETDATE())) " +
                "AS hourpart, DATEDIFF(minute,CreatedDate, (GETDATE()))%60 " +
                "AS minpart from CustomerApplication) ts on ca.ID = ts.ID " +
                "WHERE wp.ProcessType_ID IN ('GRD', 'TTS') " +
                "AND cawp.ApplicationStatus_ID = 0 " +
                "AND ca.ApplicationWithdrawReason_ID IS NULL " +
                "AND ca.TurnDown_ID IS NULL " +
                "AND c.PrimaryEmail LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%'";
        }

        private static string BuildOpenLoanCustomerQuery(string StateCode)
        {
            string benuSqlLink = null;
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    benuSqlLink = "benusql01.ccoc7test.com";
                    break;
                case SystemUnderTest.STAGE:
                    benuSqlLink = "ccoc7sql02-test.ccoc7.com";
                    break;
            }

            return
                "SELECT D.LoanModule_ID LoanMod,D.ID LoanID,D.Advanced,D.Type_ID LoanType, " +
                "(SELECT StartDays FROM DeferredEarlyPayoffDiscount WHERE DeferredType_ID = D.Type_ID) DiscountStartDay, D.FundingDate, " +
                "(SELECT DATEDIFF(day,(D.FundingDate), " +
                "(SELECT ils.PaymentDueDate FROM InstallmentLoanSchedule ils " +
                "JOIN (SELECT Loan_ID,MAX(PaymentNumber) Number FROM InstallmentLoanSchedule group by Loan_ID ) sIS " +
                "ON ils.Loan_ID = sIS.Loan_ID AND ils.PaymentNumber = sIS.Number WHERE ils.Loan_ID = D.ID))) LoanDur, " +
                "(SELECT DATEDIFF(day,D.FundingDate,(SELECT System_Date FROM Location WHERE ID = 'UTO'))) " +
                "LoanAge , D.Customer_ID, D.Customer_Name," +
                "ccoAlt.Email WebLoginName,ccoAlt.pwd,ccoAlt.birthdate," +
                "C.SSN,DP.Date LastProposalDate,D.DueDate,C.PrimaryEmail,DP.Number ProposalNumber,DP.ClearingStatus_ID " +
                "FROM Deferred D JOIN DeferredProposal DP ON D.ID = DP.Deferred_ID " +
                "inner JOIN (SELECT Deferred_ID,MAX(Number) Number FROM DeferredProposal group by Deferred_ID ) sDP ON  DP.Deferred_ID = sDP.Deferred_ID " +
                "AND DP.Number = sDP.Number JOIN Contact C ON D.Customer_ID = C.ID " +
                "JOIN (SELECT * FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] ) ccoAlt " +
                "ON D.Customer_ID = ccoAlt.ccoid AND C.Birthday = ccoAlt.birthdate " +
                "WHERE D.AmountDue > 0 " +
                "AND C.PrimaryEmail LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%'";
        }

        private static string BuildAgedLoanCustomerQuery(LoanType loanType, string agingTime)
        {
            string customerLoanType;
            switch (loanType)
            {
                case LoanType.Payday:
                    customerLoanType = "DEF";
                    break;
                default:
                    customerLoanType = "INS";
                    break;
            }

            string benuSqlLink = null;
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    benuSqlLink = "benusql01.ccoc7test.com";
                    break;
                case SystemUnderTest.STAGE:
                    benuSqlLink = "ccoc7sql02-test.ccoc7.com";
                    break;
            }

            return
                "SELECT Distinct C.ID Id,C.Birthday DateOfBirth, C.State State,C.PrimaryEmail Email,C.SSN SocialSecurityNumber, " +
                "C.LastName LastName,C.FirstName FirstName,ccoAlt.Email UserName, ccoAlt.pwd Password " +
                "from Deferred D " +
                "JOIN DeferredProposal DP ON D.ID = DP.Deferred_ID INNER " +
                "JOIN (SELECT Deferred_ID,MAX(Number) Number " +
                "from DeferredProposal group by Deferred_ID ) sDP ON  DP.Deferred_ID = sDP.Deferred_ID " +
                "AND DP.Number = sDP.Number JOIN Contact C ON D.Customer_ID = C.ID " +
                "JOIN (SELECT * FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] ) ccoAlt " +
                "ON D.Customer_ID = ccoAlt.ccoid AND C.Birthday = ccoAlt.birthdate where D.AmountDue > 0 " +
                "AND C.LastName = 'Approved' AND (SELECT DATEDIFF(day,D.FundingDate,(SELECT GETDATE()))) >= " +
                agingTime + " " +
                "AND D.LoanModule_ID = '" + customerLoanType + "' " +
                "AND C.PrimaryEmail LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%'";

        }

        private static string BuildCustomerQuery(LoanProperties loan)
        {
            string benuSqlLink = null;
            string customerQuery = null;
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    benuSqlLink = "benusql01.ccoc7test.com";
                    break;
                case SystemUnderTest.STAGE:
                    benuSqlLink = "ccoc7sql02-test.ccoc7.com";
                    break;
            }

            switch (loan.CustomerLastName)
            { 
                    case "Approved":
                        customerQuery = ApprovedCustomerQuery(loan, benuSqlLink);
                    break;
                    case "Turndown":
                        customerQuery = TurndownCustomerQuery(loan, benuSqlLink);
                    break;
                    case "TurndownLead":
                        customerQuery = TurndownLeadCustomerQuery(loan, benuSqlLink);
                        break;
                    case "PendingAll":
                        customerQuery = PendingAllCustomerQuery(loan, benuSqlLink);
                        break;
            }
            return customerQuery;
        }

        private static string ApprovedCustomerQuery(LoanProperties loan, string benuSqlLink)
        {
            string approvedQuery = null;
            switch (loan.LoanType)
            {
                case "Personal":
                    approvedQuery = ApprovedPersonalQuery(loan, benuSqlLink);
                    break;
                case "Payday":
                    approvedQuery =  ApprovedPaydayQuery(loan, benuSqlLink);
                    break;
            }

            return approvedQuery;
        }

        private static string ApprovedPersonalQuery(LoanProperties loan, string benuSqlLink)
        {
            string approvedPersonalQuery = null;
            switch (loan.WithBalance)
            {
                case true:
                    approvedPersonalQuery = ApprovedPersonalBalance(loan, benuSqlLink);
                    break;
                case false:
                    approvedPersonalQuery = ApprovedPersonalZeroBalanceQuery(loan, benuSqlLink);
                    break;
            }

            return approvedPersonalQuery;
        }


        private static string ApprovedPaydayQuery(LoanProperties loan, string benuSqlLink)
        {
            string approvedPayDayQuery = null;
            switch (loan.WithBalance)
            {
                case true:
                    approvedPayDayQuery = ApprovedPaydayWithBalance(loan, benuSqlLink);
                    break;
                case false:
                    approvedPayDayQuery = ApprovedPaydayZeroBalanceQuery(loan, benuSqlLink);
                    break;
            }

            return approvedPayDayQuery;
        }

        private static string ApprovedPaydayWithBalance(LoanProperties loan, string benuSqlLink)
        {
            string approvedPayDayQuery = null;
            switch (loan.PastDue)
            {
                case true:
                    approvedPayDayQuery = ApprovedPaydayPastDue(loan, benuSqlLink);
                    break;
                case false:
                    approvedPayDayQuery = ApprovedPaydayWithBalanceQuery(loan, benuSqlLink);
                    break;
            }

            return approvedPayDayQuery;
        }

        private static string ApprovedPaydayPastDue(LoanProperties loan, string benuSqlLink)
        {
            string approvedPersonalQuery = null;
            if (loan.CustomerType == CustomerType.UT_70PlusPayday.ToString())
                approvedPersonalQuery = ApprovedPaydayPastDue70Query(loan, benuSqlLink);
            else
                approvedPersonalQuery = ApprovedPaydayPastDueQuery(loan, benuSqlLink);

            return approvedPersonalQuery;
        }

        private static string ApprovedPersonalBalance(LoanProperties loan, string benuSqlLink)
        {
            string approvedPersonalQuery = null;
            switch (loan.PastDue)
            {
                case true:
                    approvedPersonalQuery = ApprovedPersonalPastDue(loan, benuSqlLink);
                    break;
                case false:
                    approvedPersonalQuery = ApprovedPersonalBalanceQuery(loan, benuSqlLink);
                    break;
            }

            return approvedPersonalQuery;
        }

        private static string ApprovedPersonalPastDue(LoanProperties loan, string benuSqlLink)
        {
            string approvedPersonalQuery = null;
            if (loan.CustomerType == CustomerType.UT_65PlusPersonal.ToString())
                approvedPersonalQuery = ApprovedPersonalPastDue65Query(loan, benuSqlLink);
            else
                approvedPersonalQuery = ApprovedPersonalPastDueQuery(loan, benuSqlLink);

            return approvedPersonalQuery;
        }

        private static string ApprovedPaydayZeroBalanceQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue < 1 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'DEF' " +
                   "AND GETDATE() > DATEADD(DAY,2,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }

        private static string ApprovedPersonalZeroBalanceQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue < 1 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'INS' " +
                   "AND GETDATE() > DATEADD(DAY,2,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }

        private static string ApprovedPaydayWithBalanceQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'DEF' " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";

        }

        private static string ApprovedPaydayPastDueQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'DEF' " +
                   "AND GETDATE() > DATEADD(DAY,2,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }


        private static string ApprovedPersonalBalanceQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'INS' " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }


        private static string ApprovedPersonalPastDueQuery(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'INS' " +
                   "AND GETDATE() > DATEADD(DAY,2,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }
        private static string ApprovedPersonalPastDue65Query(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'INS' " +
                   "AND GETDATE() > DATEADD(DAY,65,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }
        private static string ApprovedPaydayPastDue70Query(LoanProperties loan, string benuSqlLink)
        {
            return "select C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, " +
                   "C.LastName, C.FirstName, SL.email as Username, SL.pwd " +
                   "FROM [" + benuSqlLink + "].[CCOAlternate].[dbo].[storelogin] SL " +
                   "JOIN  Deferred D on SL.ccoid = D.Customer_ID " +
                   "JOIN Contact C on SL.ccoid = C.ID where D.AmountDue > 0 " +
                   "AND SL.State = '" + loan.CustomerState + "' " +
                   "AND C.Location_ID = '" + loan.Location_ID + "' " +
                   "AND D.LoanModule_ID = 'DEF' " +
                   "AND GETDATE() > DATEADD(DAY,70,D.DueDate) " +
                   "AND C.LastName = 'Approved' " +
                   "AND C.PrimaryEmail Not Like '%example.com%' " +
                   "ORDER BY D.OriginationDate DESC";
        }

        private static string TurndownCustomerQuery(LoanProperties loan, string benuSqlLink)
        {
            return $@";WITH LatestLogin (ccoid, email, pwd, rownumber) AS (
                  SELECT sl.ccoid, sl.email, sl.pwd, ROW_NUMBER() OVER (PARTITION BY sl.ccoid ORDER BY sl.storelogin_id desc)
                  FROM [{benuSqlLink}].[CCOAlternate].[dbo].[storelogin] sl 
             ) 
             SELECT C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, C.LastName, C.FirstName, ll.email as Username, ll.pwd 
             FROM Contact c 
             LEFT JOIN LatestLogin ll ON c.ID = ll.ccoid AND ll.rownumber = 1 
             WHERE c.LastName = 'Turndown' 
             AND C.PrimaryEmail LIKE '%{TestConfigManager.Config.CatchAllDomain}%' 
             AND c.State = '{loan.CustomerState}'
             AND c.Location_ID = '{loan.Location_ID}'";
        }

        private static string TurndownLeadCustomerQuery(LoanProperties loan, string benuSqlLink)
        {
            return $@";WITH LatestLogin (ccoid, email, pwd, rownumber) AS (
                  SELECT sl.ccoid, sl.email, sl.pwd, ROW_NUMBER() OVER (PARTITION BY sl.ccoid ORDER BY sl.storelogin_id desc)
                  FROM [{benuSqlLink}].[CCOAlternate].[dbo].[storelogin] sl 
             ) 
             SELECT C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, C.LastName, C.FirstName, ll.email as Username, ll.pwd 
             FROM Contact c 
             LEFT JOIN LatestLogin ll ON c.ID = ll.ccoid AND ll.rownumber = 1 
             WHERE c.LastName = 'Turndown Lead' 
             AND C.PrimaryEmail LIKE '%{TestConfigManager.Config.CatchAllDomain}%' 
             AND c.State = '{loan.CustomerState}'
             AND c.Location_ID = '{loan.Location_ID}'";
        }


        private static string PendingAllCustomerQuery(LoanProperties loan, string benuSqlLink)
        {
            return $@";WITH LatestLogin (ccoid, email, pwd, rownumber) AS (
                  SELECT sl.ccoid, sl.email, sl.pwd, ROW_NUMBER() OVER (PARTITION BY sl.ccoid ORDER BY sl.storelogin_id desc)
                  FROM [{benuSqlLink}].[CCOAlternate].[dbo].[storelogin] sl 
             ) 
             SELECT C.ID Id, C.Birthday DateOfBirth, C.State, C.PrimaryEmail Email, C.SSN SocialSecurityNumber, C.LastName, C.FirstName, ll.email as Username, ll.pwd 
             FROM Contact c 
             LEFT JOIN LatestLogin ll ON c.ID = ll.ccoid AND ll.rownumber = 1 
             WHERE c.LastName = 'Pending All' 
             AND C.PrimaryEmail LIKE '%{TestConfigManager.Config.CatchAllDomain}%' 
             AND c.State = '{loan.CustomerState}'
             AND c.Location_ID = '{loan.Location_ID}'";
        }

        private static string SetCustomerQuery(LoanProperties loan)
        {
            {
                string benuSqlLink = null;
                switch (TestConfigManager.Config.SystemUnderTest)
                {
                    case SystemUnderTest.QA:
                        benuSqlLink = "benusql01.ccoc7test.com";
                        break;
                    case SystemUnderTest.STAGE:
                        benuSqlLink = "ccoc7sql02-test.ccoc7.com";
                        break;
                }

                switch (loan.CustomerLastName)
                {
                    case "Approved":
                        if (loan.MinimumBalance == 0)
                        {
                            return
                                "select count(*) NumOfCustomers from CustomerApplication ca " +
                                "JOIN CustomerApplicationWorkflowProcess cawp ON ca.ID = cawp.CustomerApplication_ID " +
                                "JOIN WorkflowProcess wp ON cawp.Process_ID = wp.ID " +
                                "JOIN Contact c on ca.Contact_ID = c.ID " +
                                "WHERE wp.ProcessType_ID = 'AMT' " +
                                "AND ca.ApplicationWithdrawReason_ID IS NULL " +
                                "AND ca.Entity_ID IS NULL " +
                                "AND ca.TurnDown_ID IS NULL " +
                                "AND ca.Module_ID = '" + loan.Type + "' " +
                                "AND c.State = '" + loan.CustomerState + "' " +
                                "AND c.PrimaryEmail LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%' " +
                                "AND c.LastName like '%" + loan.CustomerLastName + "%' " +
                                "AND ca.Contact_ID NOT IN (select Customer_ID from Deferred where AmountDue > 0) " +
                                "AND cawp.ApplicationStatus_ID = 0";
                        }
                        else
                        {
                            return
                                "select COUNT(*) NumberOfCustomers from Contact c where LastName LIKE '%" + loan.CustomerLastName + "%' " +
                                "AND c.PrimaryEmail LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%' AND EXISTS " +
                                "(SELECT 1 FROM Deferred d WHERE d.Customer_ID = c.ID AND d.LoanModule_ID = '" + loan.Type + "' " +
                                "AND AmountDue > 0)  " +
                                "AND c.State = '" + loan.CustomerState + "'";
                        }
                    case "Turndown":
                        return
                            "select COUNT(*) NumberOfCustomers from Contact c where LastName = '" + loan.CustomerLastName + "' " +
                            "and isNull (MiddleName, '') != 'Utilized' AND c.PrimaryEmail " +
                            "LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%' " +
                            "AND NOT EXISTS (SELECT 1 FROM Deferred d WHERE d.Customer_ID = c.ID) " +
                            "AND c.State = '" + loan.CustomerState + "' ";
                    case "Turndown Lead":
                    case "Pending All":
                        return
                            "select COUNT(*) NumberOfCustomers from Contact c where LastName LIKE '%" + loan.CustomerLastName + "%' " +
                            "and isNull (MiddleName, '') != 'Utilized' AND c.PrimaryEmail " +
                            "LIKE '%" + TestConfigManager.Config.CatchAllDomain + "%' " +
                            "AND NOT EXISTS (SELECT 1 FROM Deferred d WHERE d.Customer_ID = c.ID) " +
                            "AND c.State = '"+ loan.CustomerState +"' ";
                    default:
                        return "HOUSTON WE HAVE A PROBLEM";
                }
            }
        }
    }
}