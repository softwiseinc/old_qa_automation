﻿using SharpCompress.Crypto;

namespace CheckCityOnline
{
    internal class ErrorStrings
    {
        public const string HomePageNotLoaded = "Home page did not open successfully";
        public const string PasswordInputNotFound = "The password input filed was not found on the page.";
        public const string CustomerIdInputNotFound = "The customer ID input filed was not found on the page.";
        public const string CustomerDobInputNotFound = "The customer Dob input filed was not found on the page.";
        public const string SelectStateDropdownIsNotFound = "The Select State dropdown is not found on the page.";
        public const string StoreRegistrationPageNotLoaded = "The store registration page did not load";
        public const string StoreRegistrationButtonNotFound = "The store registration button is not found";
        public const string UserNotLoggedIn = "The user login was not successful";
        public const string ClickToLogin = "The 'Click here to login' link was not successful";
        public const string LostPasswordPageNotLoaded = "The 'Lost Password' page did not load successfully";
        public const string MemberSecurityNotLoaded = "The 'Memeber Security' page did not load successfully";
        public const string ResetMyPasswordNotLoaded = "The 'Reset My Password' page did not load successfully";
        public const string UpdatePasswordNotLoaded = "The 'Update Password' page did not load successfully";
        public const string MemberLoginPageNotLoaded = "The 'Member Login' page did not load successfully";
        public const string NewLoanPageNotLoaded = "The 'New Loan' page did not load successfully";
        public const string ApplyLoanPageNotLoaded = "The 'Apply Loan' page did not load successfully";
        public const string LoanDisclosurePageNotLoaded = "The 'Loan Disclosure' page did not load successfully";
        public const string ThankYouPageNotVisible = "The 'Thank You' screen text is not visible";
        public const string MemberPageNotLoaded = "The 'Member ' page did not load successfully";
        public const string StoreBankEditPageNotVisible = "The 'Store Bank Edit' page did not load successfully";
        public const string StoreProfileEditNotVisible = "The 'Store Profile Edit' page did not load successfully";
        public const string ReferralPageIsNotVisible = "The 'Refer A Friend' screen text is not visible";
        public const string SendEmailReferralButtonIsNotVisible = "The 'Refer A Friend' screen text is not visible";
        public const string ChangeEmailIsNotVisible = "The 'Change Email' screen text is not visible";
        public const string EmailUpdateConfirmationIsNotVisible = "The 'Email Update Confirmation' screen text is not visible";
        public const string UpdateMemberInfoIsNotVisible = "The 'Update info' screen text is not visible";
        public const string DocumentUploadPageNotLoaded = "The 'Document Upload' page did not load successfully";
        public const string EFPPageNotLoaded = "The 'EFP login'Page did not load successfully";
        public const string DateSignedOnLoanDocumentSectionNotLoaded = "The 'Date Signed' is not visible";
        public const string LoanDocumentsHeaderTextIsNotVisible = "The 'Loan Document'header text is not visible";
        public const string EfpLoanApprovalPageDidNotLoad = "The 'Efp Loan Approval'Page did not load";
        public const string EfpBreadcrumbNotVisible = "The 'Efp'Bread crumb is not visible";
        public const string ExtensionTypeScreenTextNotVisible =
            "The 'Extend Loan' text on the Extension Type page is not visibile";

        public const string EfpAdverseActionPdfNotVisible = "The 'Adverse Action Pdf'is not visible";
        public const string EfpAdverseActionPageNotLoaded = "The 'Efp Adverse Action'page did not load";
        public const string DateSignedOnEFPLoanDocumentsPageIsNotVisible = "The 'Date text' is not visible ";
        public const string TurnDownLeadPageIsNotVisible = "The 'Turndown lead page' is not visible ";
        public const string MakePaymentButtonTextIsNotVisible = "The 'Make Payment' Button is not Visible";
        public const string PaymentThankYouPageNotLoaded = "The 'Payment Thank you'Page did not load";
        public const string PaymentTypePageNotLoaded = "The 'Payment Type' page did not load";
        public const string ExtensionMessagePageNotLoaded = "The 'Extension Message' page did not load ";
        public const string PersonalLoanApprovalPageNotVisible =
            "The 'Personal Loan Approval' page did not load successfully";
        public const string CurrentLoanIsNotVisible = "Memberpage text 'Current Loan Status' is not visible";
        public const string ThankYouPaymentNotVisible = "The 'Thank you fo ryour payment' screen text is not visible";
        public const string LoanAtStoreNotVisible = "The 'See you in 'text isn't visible";
        public const string LoanApprovalDateNotMatched = "Loan approval date not matched.";
        public const string RefinancePersonalLoanApprovalNotVisible =
            "The 'Refinance Loan Details' screen text did not loan";
        public const string StoreExtensionMessagePageNotVisible = "The 'Store Extension Message' did not load successfully";
        public const string SuccessfullyAppliedLoan = "Congratulation page is not successfully loaded after applying for loan.";
        public const string StorePaymentAcceptNotVisible = "The 'Store Payment Accept' Bread crumb is not visible";
        public const string DocumentUploadServicesNotVisible = "The 'Document Upload Services' screen text isn't visible";
        public const string StoreExtensionNotVisible = "The 'Store Extension' bread crumb text is not visible";
        public const string ExtensionAcceptNotVisible = "The 'Extension Accept' Page did not load";
        public const string ExtensionTransactPageNotLoaded = "The 'Extension Transact' page did not load";
        public const string PaymentPlanIsNotVisible = "The 'Payment Plan'Text did is not visible";
        public const string StorePaymentPageNotLoaded = "The 'Store Payment' Did not load";
        public const string LoanDetailsPageNotLoaded =
            "The 'Personal loan Details' page did not load successfully";
        public const string LoanLastPaydayNotLoaded =
           "The Last Payday'  did not load successfully";

        public const string PersonalLoanDisclosurePageNotLoaded =
            "The 'Personal Loan Disclosure' page did not load successfully";

        public const string StoreInstallmentPaymentPageNotLoaded =
            "The 'Store Installment Payment' page did not load successfully";

        public const string StoreInstallmentPaymentAcceptPageNotLoaded =
            "The 'Store Installment Payment Accept' page did not load successfully";

        public const string PersonalLoanThankYouPageNotLoaded =
            "The 'Personal Loan Thank You' page did not load successfully";

        public const string CopySuccessMessageNotVisible =
            "The copy success message is not visable on the page after the copy button was pushed";

        public const string MemberIsNotLoggedIn =
            "Customer Logged In menu 'is not visible on the memberdashboard";
        public const string CheckPendingApplicationTileIsNotVisible =
            "The 'Check Pending App Tile'is not visible on the memberdashboard";
        public const string SecurityQuestionIncorrect = "The security question 1 was answered incorrectly";
        public const string PayDayPayOffPageNotLoaded = "The 'PayDay Pay Off' page did not load successfully";

        public const string PayDayTransactionInfoPageNotLoaded =
            "The 'Payday Transaction Info' page did not load successfully ";

        public const string UpdateMemberNotVisible = "The 'Update Member' page did not load successfully";
        public const string UpdatePasswordNotVisible = "The 'Update Password' page did not load successfully";

        public const string UpdateMemberPasswordHasChangeDidNotDisplay =
            "The 'Password has changed' screen text did not load successfully";

        public const string MissingDateOfBirth =
            "The missing date of birth error message was not visible on the screen";

        public const string InvalidDateOfBirth =
            "The Invalid Date of Birth error message was not visible on the screen";

        public const string IsStoreOptionIsNotVisible = "Store or continue online option is not visible";
        public const string EmailRequired = "The email required error message was not visible on the screen";
        public const string InvalidEmailAddress = "The invalid email error message was not visible on the screen";
        public const string PaydayExtensionNotVisible = "The 'Payday Extension' bread crumb was not visible ";
        public const string PaydayPaymentNotVisible = "The 'Payday Payment' bread crumb was not visible ";
        public static string ReferralTermsPageNotLoaded = "The online referral terms and conditions page was not loaded";
        public static string InStoreReferralError = "The in store referral page did not load.";
        public static string UpdateIncomePage = "Update Income page did not load.";
        public static string StoreProfilePageNotVisible = "The 'Store-Profile' Screen text is not visible";
        public static string StoreLoanHistoryPageNotLoaded = "The 'Store-Loan-History' page did not load Successfully ";
        public static string StoreChangePasswordNotLoaded =
            "The 'Store-Change-Password' page did not load successfully";

        public static string LoanDocumentListDropDownNotVisible = "The 'Loan Document List' drop down not visible";
        public static string Efp_LoanDocumentsPageDidNotLoad = "The 'Efp loan document' Page did not load";
        public static string ProfileUpdateSuccessfulNotVisible = "The 'ProfileUpdateSuccessful' test is not visible";
        //Error strings for Application form personal loan
        public const string ApplyPageNotLoaded = "The apply page did not load successfully";
        public const string FirstNameRequired = "The first name required error message was not visible on the screen";
        public const string LastNameRequired = "The last name required error message was not visible on the screen";
        public const string DateOfBirthRequired = "The DOB required error message was not visible on the screen";
        public const string ZipCodeRequired = "The Zip Code required error message was not visible on the screen";
        public const string PersonalLoanRequired = "The Loan type required error message was not visible on the screen";
        public const string MonthlyIncomeRequired = "The monthly income required error message was not visible on the screen";
        public const string SelectPayPeriodRequired = "The select pay period required error message was not visible on the screen";
        public const string SelectDaypaidRequired = "The day paid required error message was not visible on the screen";
        public const string SelectPayDayRequired = "The pay day required error message was not visible on the screen";
        public const string SelectTypeRequired = "The income type required error message was not visible on the screen";
        public const string SelectDayPaidRequired = "The day paid required error message was not visible on the screen";
        public const string SelectIncomeTypeRequired = "The select income type required error message was not visible on the screen";
        public const string NameOfCompanyRequired = "The name of company required error message was not visible on the screen";
        public const string PhoneNumberRequired = "The phone number required error message was not visible on the screen";
        public const string EmployersCityRequired = "The employer's city required error message was not visible on the screen";
        public const string SelectEmployersStateRequired = "The employer's state required error message was not visible on the screen";
        public const string EmployersZipCodeRequired = "The employer's zip code required error message was not visible on the screen";
        public const string SelectYearRequired = "The select year required error message was not visible on the screen";
        public const string SelectMonthRequired = "The select month required error message was not visible on the screen";
        public const string RoutingNumberRequired = "The routing number required error message was not visible on the screen";
        public const string BankAccountNumberRequired = "The bank account number required error message was not visible on the screen";
        public const string ReEnterBankAccountNumberRequired = "The re-enter bank account number required error message was not visible on the screen";
        public const string SelectIdentifcationTypeRequired = "The select identification type required error message was not visible on the screen";
        public const string SelectIdentificationStateRequired = "The select identification state required error message was not visible on the screen";
        public const string IdentificationNumberRequired = "The identification number required error message was not visible on the screen";
        public const string ExpirationDateRequired = "The expiration date required error message was not visible on the screen";
        public const string SocialSecurityNumberRequired = "The social seurity number required error message was not visible on the screen";
        public const string StreetAddressRequired = "The street address required error message was not visible on the screen";
        public const string ContactPhoneRequired = "The contact phone humber required error message was not visible on the screen";
        public const string VerificationCodeRequired = "The verification code required error message was not visible on the screen";
        public const string ClickAgreeTextMessageRequired = "The agree text message required error message was not visible on the screen";
        public const string ContactPasswordRequired = "The contact password required error message was not visible on the screen";
        public const string ClickAgreeTermsRequired = "The agree terms required error message was not visible on the screen";
        public const string ClickAgreeConsentRequired = "The agree consent required error message was not visible on the screen";

        public const string InlineMessageForAudioRecNotLoaded = "In compliance with the state of Utah, we have sent you an audio recording of your loan terms. To finalize your loan, please review the full disclosure below => message is not loaded.";

        public const string SelectMultipleCitiesNotLoaded = "Multiple cities Selectbox is not visible";
        public const string PendingApprovalPageNotLoaded = "The pending approval page did not load";
        public const string MakeMyPaymentPageNotLoaded = "MakeMyPayment page did not open successfully";
        public const string ApplicationNotPending = "Application Not Pendign";
        internal class StoreExtensionsMessage
        {
            public static string FinalizeYourInstallmentPayoffPaymentError = "Finalize Your Installment Payoff Payment page is not found";
            public static string MakePaymentDueDateConfirmationPageError = "Make Payment Due Date Confirmation Page NOT Loaded";
        }
    }

    internal class PersonalInformation
    {
        public static string ZipCode = "84043";
    }
    internal class ApplyLoanErrorString
    {
        public static string FirstName = "First Name is not validated.";
        public static string LastName = "Last Name is not validated.";
        public static string Email = "Email address is invalid.";
        public static string DobEmpty = "Empty DOB is accepted.";
        public static string DobInvalid = "Invalid DOB is not accepted.";
        public static string ZipCodeEmpty = "Empty ZipCode is accepted.";
        public static string ZipCodeInvalid = "Invalid ZipCode is not accepted.";
        public static string MonthlyIncomeInvalid = "Invalid monthly Income is not submitted.";
        public static string BankRoutingNUmberInvalid = "Invalid Routing Number";
        public static string BankAccountInvalid = "Invalid Bank AccountLink is not submitted.";
        public static string PasswordInvalid = "Invalid Password is not submitted.";
    }

    internal class ApplyLoanIncomeInformation
    {
        public const string MonthlyIncome = "1200";
        public const string MonthlyIncomeInvalid = "12000";
        public const string PayPeriod_option_1 = "Every 2 Weeks";
        public const string PayPeriod_option_2 = "Twice a Month";
        public const string PayPeriod_option_3 = "Monthly";
        public const string PayPeriod_option_4 = "Weekly";
        public const string DayOfWeekpaid = "Monday";
        public const string DayOne = "1";
        public const string DayTwo = "15";
        public const string IncomeType = "Employed";
        public const string CompanyName = "Softwise, Inc.";
        public const string EmployerPhoneNumber = "8019612201";
        public const string EmployersCity = "Lehi";
        public const string EmployersState = "Utah";
        public const string EmployersZipCode = "84043";
        public const string LengthOfTimeYears = "7";
        public const string LengthOfTimeMonths = "7";
        public const string LastPayDayLabel = "Last Payday";
    }

    internal class ApplyBankInformation
    {
        public const string RoutingNumber = "021000021";
        public const string BankAccount = "091000019";
        public const string ReBankAccount = "091000019";

        public const string RoutingNumberInvalid = "999999999";
        public const string BankAccountInvalid = "99999999999999999999999999999999";
    }

    internal class IdentificationInformation
    {
        public const string IdentificationNumber = "8888888888888";
        public const string BirthDate = @"01 / 03 / 1969";
        public const string ExpirationDate = @"01 / 03 / 2033";
        public const string SocialSecurity = "078051122";
        public const string SocialSecurityInvalid = "-"; //negative
        public const string State = "Utah";
    }

    internal class SecurityInformation
    {
        public const string IdentificaionNumber = "8888888888888";
        public const string InvalidPassword = "T";
        public const string Password = "Diehard1!";

    }

    internal class LoanAppliedSuccessful
    {
        public const string Congratulation = "RefinanceLoanDetails!";
    }


    internal class LoanAmount
    {
        public const string InvalidSmall = "99";
        public const string Minimum = "100";
        public const string Medium = "500";
        public const string Maximum = "1000";
        public const string InvalidLarge = "1001";
    }

    internal class PersonalLoanAmount
    {
        public const string InvalidSmall = "99";
        public const string Minimum = "100";
        public const string Medium = "500";
        public const string Maximum = "1000";
        public const string InvalidLarge = "1001";
    }

    internal class PrincipalPayment
    {
        public const string Small = "15";
        public const string Minimum = "25";
        public const string Medium = "50";
        public const string Large = "75";
        public const string Maximum = "100";
    }

    internal class PayDownAmount
    {
        public const string Small = "15";
        public const string Minimum = "25";
        public const string Medium = "50";
        public const string Large = "75";
        public const string Maximum = "100";
    }

    internal class InvalidData
    {
        public const string BirthDate = "11/11/1111";
    }

    internal class ElementBuild
    {
        public const string Part1Extend = "//*[contains(@id(),'ctl00_CustomContent_repLoanWidgetOutstanding_ctl";
        public const string Part2Extend = "_hlLoanWidgetOutstandingExtendUrl')]";
        public const string Part1Payable = "//*[@id='ctl00_CustomContent_pnlLoanWidgetOutstandingLoan']/div[";
        public const string Part2Payable = "]/div/div[2]";
        public const string Part1Refinance = "//*[contains(@id(),'ctl00_CustomContent_repLoanWidgetOutstanding_ctl";
        public const string Part2Refinance = "00_hlLoanWidgetOutstandingStoreLoanRefinanceUrl')]";
    }

}