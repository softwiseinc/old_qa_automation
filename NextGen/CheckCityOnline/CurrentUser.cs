﻿using System;
using AutomationResources;
using NUnit.Framework;

namespace CheckCityOnline
{
    public partial class Users
    {
        internal static CurrentUser User { get; private set; }

        public Users()
        {
            var Config = TestConfigManager.Config;
            Config.CatchAllDomain = TestContext.Parameters["CatchAllDomain"]?.ToLower() ?? Config.CatchAllDomain;
        }

        public static class SecurityQuestion
        // last edit by Pat Holman 2/7/2020
        {
            public static string One = "conan";
            public static string Two = "the shining";
            public static string Three = "tacoma";
            public static string Four = "grey";
        }

        public static class Input
        {
            public static string PhoneNumber = "8011943515";
            public static string AccountNumber = "8881";
        }

        public static class Signatures
        {
            public static string PaydayLoanSignature = "Tester Man";
            public static string PersonalLoanSignature = "Tester Man";
            public static string ExtensionSignature = "Tester Man";
            public static string PaymentPlanSignature = "Tester Man";
        }
        public class CurrentUser
        {
            public string Id { get; set; }
            public string Type { get; set; }
            public string RunEnvironment { get; set; }
            public string EnterNewLoanAmount { get; set; }
            public string NewPassword2 { get; set; }
            public string UserName { get; set; }
            public string PhoneNumber { get; set; }
            public string PhoneNumberVerify { get; set; }
            public string CustomerID { get; set; }
            public string CustomerState { get; set; }
            public string PhoneNumberVerifyCode { get; set; }
            public string BankCard { get; set; }
            public string Address { get; set; }
            public string State { get; set; }
            public string City { get; set; }
            public string ZipCode { get; set; }
            public string ReferralEmail { get; set; }
            public string UpdatedEmail { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MiddleName { get; set; }
            public string PrimaryPhone { get; set; }

            public string Signature { get; set; }
            public string CascadesBank { get; set; }
            public string RoutingNumber { get; set; }
            public string AccountNumber { get; set; }
            public string Years { get; set; }
            public string Months { get; set; }
            public string AdverseActionCustomerID { get; set; }
            public string AdverseActionCustomerEmail { get; set; }
            public string Password { get; set; }
            public string BirthDate { get; set; }
            public string InvalidBirthDate { get; set; }
            public string InvalidZipCode { get; set; }
            public string NewPassword { get; set; }
            public string SecurityQuestion1 { get; set; }
            public string SecurityQuestion2 { get; set; }
            public string SecurityQuestion3 { get; set; }
            public string SecurityQuestion4 { get; set; }
            public string PaymentMethodDB { get; set; }
            public string CardType { get; set; }
            public string Last4Digits { get; set; }
            public string PaymentLarge { get; set; }
            public string PaymentSmall { get; set; }
            public string PaymentIncorrect { get; set; }
            public string PaymentMethodAch { get; set; }
            public string DateOfBirth { get; set; }
            public string MonthlyIncome { get; set; }
            public string SelectPayPeriod { get; set; }
            public string NameOfCompany { get; set; }
            public string EmployerCity { get; set; }
            public string EmployerState { get; set; }
            public string EmployerZipcode { get; set; }
            public string EmployerPhone { get; set; }
            public string BankAccountNumber { get; set; }
            public string ReEnterBankAccountNumber { get; set; }
            public string SocialSecurityNumber { get; set; }
            public string IdentificationNumber { get; set; }
            public string AdditionalMonthlyIncome { get; set; }
            public string AdditionalSelectPayPeriod { get; set; }
            public string AdditionalNameOfCompany { get; set; }
            public string AdditionalEmployersCity { get; set; }
            public string AdditionalEmployersState { get; set; }
            public string AdditionalEmployersZipCode { get; set; }
            public string AdditionalPhoneNumber { get; set; }
            public string ExpirationDate { get; set; }
            public string StreetAddress { get; set; }
            public string ContactPhoneNumber { get; set; }
            public string VerificationCode { get; set; }
            public string ContactPassword { get; set; }
            public string ID { get; set; }
        }
    }
}