﻿namespace CheckCityOnline
{
    internal class Url
    {
        internal class Production
        {
            public const string EfpLoanApprovalPage = "http://members.checkcity.com/efp_loanapproval.aspx";
            public const string ThankYouPage = "https://members.checkcity.com/thankyou-payment.aspx";
            public const string MemberLogin = "https://members.checkcity.com/memberlogin.aspx";
            public const string Member = "https://members.checkcity.com/member.aspx";
            public const string Referral = "https://members.checkcity.com/Referral.aspx";
            public const string LostPassword = "https://members.checkcity.com/LostPassword.aspx";
            public const string MemberSecurity = "https://members.checkcity.com/MemberSecurity.aspx";
            public const string ResetMyPassword = "https://members.checkcity.com/ResetMyPassword.aspx";
            public const string NewLoan = "http://members.checkcity.com/NewLoan.aspx";
            public const string ExtensionType = "https://members.checkcity.com/extension-type.aspx";
            public const string LoanDisclosure = "https://members.checkcity.com/Loan_Disclosure.aspx";
            public const string LoanThankYou = "http://members.Checkcity.com/Loan_ThankYou.aspx";
            public const string StoreBankEdit = "https://members.checkcity.com/store-bank-edit.aspx";
            public const string StoreProfileEdit = "https://members.checkcity.com/store-profile-edit.aspx";
            public const string LoanApproval = "https://members.checkcity.com/Loan_Approval.aspx";
            public const string LoanDetails = "https://members.checkcity.com/Loan_Details.aspx";
            public const string PersonalLoanDisclosure = "https://members.checkcity.com/Loan_Disclosure.aspx";
            public const string PersonalLoanThankYou = "https://members.checkcity.com/Loan_ThankYou.aspx";
            public const string PaymentThankYouPage = "https://members.checkcity.com/payment-thankyou.aspx";
            public const string IncomeError = "http://members.checkcity.com/IncomeError.aspx?ReturnUrl=UpdateMember.aspx";
            public const string UpdateMemberInfo = "http://members.checkcity.com/UpdateInfo.aspx?ReturnUrl=UpdateMember.aspx";
            public const string StoreChangePassword = "https://members.checkcity.com/store-change-password.aspx";
            public const string StoreLoanHistory = "https://members.checkcity.com/store-loan-history.aspx";
            public const string StoreExtension = "https://members.checkcity.com/store-extension.aspx";
            public const string ExtensionMessage = "http://members.checkcity.com/extension-message.aspx";
            public const string StoreExtensionMessage = "http://members.checkcity.com/store-extension-message.aspx";
            public const string ExtensionAccept = "https://members.checkcity.com/extension-accept.aspx";
            public const string ExtensionTransact = "https://members.checkcity.com/extension-transact.aspx";
            public const string StorePayment = "http://members.checkcity.com/store-payment.aspx";
            public const string DocumentUploadServices = "https://members.checkcity.com/DocumentUploadServices.aspx";
            public const string loanApply = "http://members.checkcity.com/apply.aspx";
            public const string StoreInstallmentPayment =
                "https://members.checkcity.com/Store-installment-payment.aspx";
            public const string PaymentType = "http://members.checkcity.com/payment-type.aspx";
            public const string EfpPage = "http://members.checkcity.com/EFP.aspx";
            public const string EFPloginPage = "http://members.checkcity.com/efplogin.aspx";
            public const string StoreInstallmentPaymentAccept =
                "https://members.ccoc7.com/Store-installment-payment-accept.aspx";
            public const string PaymentAccept = "https://members.checkcity.com/payment-accept.aspx";

            public const string PayDayPayOff = "http://members.checkcity.com/Payoff.aspx";
            public const string PaymentTransact = "https://members.ccoc7.com/payment-transact.aspx";
            public const string UpdateMember = "http://checkcity.members.com/UpdateMember.aspx";
            public const string UpdateMemberPassword = "http://checkcity.members.com/UpdatePassword.aspx";
            public const string PaydayExtension = "http://Checkcity.members.com/Extension.aspx";
            public const string PaydayPayment = "http://checkcity.members.com/Payment.aspx";
            public const string OnlineReferralTermsAndConditions = "https://checkcity.members.com/docs/online-referral-terms-and-conditions.pdf";
            public const string UpdateEmail = "http://members.checkcity.com/updateemail";
            public const string DocumentUpload = "https://members.checkcity.com/document_upload.aspx";
            public const string StoreProfile = "https://members.checkcity.com/store-profile.aspx";
            public const string Apply = "https://members.checkcity.com/apply.aspx";
            public const string LoanAtStore = "http://members.checkcity.com/Loan_AtStore.aspx";
            public const string EfpLoanDocumentPage = "http://members.checkcity.com/efp_loandocuments.aspx";
            public const string EfpAdverseActionPage = "http://members.checkcity.com/efp_adverseaction.aspx";
            public const string MakeMyPayment = "https://members.checkcity.com/makemypayment.aspx";


        }

        internal class Test
        {
            public const string EfpAdverseActionPage = "http://members.ccoc7.com/efp_adverseaction.aspx";
            public const string PaymentThankYouPage = "https://members.ccoc7.com/payment-thankyou.aspx";
            public const string ExtensionMessage = "https://members.ccoc7.com/extension-message.aspx";
            public const string EfpLoanApprovalPage = "http://members.ccoc7.com/efp_loanapproval.aspx";
            public const string EfpLoanDocumentPage = "http://members.ccoc7.com/efp_loandocuments.aspx";
            public const string EfpPage = "http://members.ccoc7.com/EFP.aspx";
            public const string EFPloginPage = "http://members.ccoc7.com/efplogin.aspx";
            public const string ExtensionTransact = "https://members.ccoc7.com/extension-transact.aspx";
            public const string ExtensionType = "https://members.ccoc7.com/extension-type.aspx";
            public const string MemberLogin = "https://members.ccoc7.com/memberlogin.aspx";
            public const string Member = "http://members.ccoc7.com/member.aspx";
            public const string Referral = "https://members.ccoc7.com/Referral.aspx";
            public const string LostPassword = "https://members.ccoc7.com/LostPassword.aspx";
            public const string MemberSecurity = "https://members.ccoc7.com/MemberSecurity.aspx";
            public const string ResetMyPassword = "https://members.ccoc7.com/ResetMyPassword.aspx";
            public const string NewLoan = "http://members.ccoc7.com/NewLoan.aspx";
            public const string LoanDisclosure = "https://members.ccoc7.com/Loan_Disclosure.aspx";
            public const string LoanThankYou = "http://members.ccoc7.com/Loan_ThankYou.aspx";
            public const string StoreBankEdit = "http://members.ccoc7.com/store-bank-edit.aspx";
            public const string ThankYouPage = "http://members.ccoc7.com/thankyou-payment.aspx";
            public const string StoreProfileEdit = "https://members.ccoc7.com/store-profile-edit.aspx";
            public const string LoanApproval = "https://members.ccoc7.com/Loan_Approval.aspx";
            public const string LoanDetails = "https://members.ccoc7.com/Loan_Details.aspx";
            public const string PersonalLoanDisclosure = "https://members.ccoc7.com/Loan_Disclosure.aspx";
            public const string PersonalLoanThankYou = "https://members.ccoc7.com/Loan_ThankYou.aspx";
            public const string StoreInstallmentPayment = "https://members.ccoc7.com/Store-installment-payment.aspx";
            public const string IncomeError = "http://members.ccoc7.com/IncomeError.aspx?ReturnUrl=UpdateMember.aspx";
            public const string UpdateMemberInfo = "http://members.ccoc7.com/UpdateInfo.aspx?ReturnUrl=UpdateMember.aspx";
            public const string StoreChangePassword = "https://members.ccoc7.com/store-change-password.aspx";
            public const string StoreLoanHistory = "http://members.ccoc7.com/store-loan-history.aspx";
            public const string StoreInstallmentPaymentAccept =
                "https://members.ccoc7.com/Store-installment-payment-accept.aspx";
            public const string StoreExtension = "https://members.ccoc7.com/store-extension.aspx";
            public const string StorePayment = "http://members.ccoc7.com/store-payment.aspx";
            public const string PaymentAccept = "https://members.ccoc7.com/payment-accept.aspx";
            public const string LoanAtStore = "http://members.ccoc7.com/Loan_AtStore.aspx";
            public const string StoreExtensionMessage = "http://members.ccoc7.com/store-extension-message.aspx";
            public const string ExtensionAccept = "https://members.ccoc7.com/extension-accept.aspx";
            public const string DocumentUploadServices = "https://members.ccoc7.com/DocumentUploadServices.aspx";
            public const string loanApply = "http://members.ccoc7.com/apply.aspx?testMode=true";
            public const string PayDayPayOff = "http://members.ccoc7.com/Payoff.aspx";
            public const string PaymentTransact = "https://members.ccoc7.com/payment-transact.aspx";
            public const string UpdateMember = "http://members.ccoc7.com/UpdateMember.aspx";
            public const string UpdateMemberPassword = "http://members.ccoc7.com/UpdatePassword.aspx";
            public const string PaydayExtension = "http://members.ccoc7.com/Extension.aspx";
            public const string PaydayPayment = "http://members.ccoc7.com/Payment.aspx";
            public const string OnlineReferralTermsAndConditions = "http://members.ccoc7.com/docs/online-referral-terms-and-conditions.pdf";
            public const string UpdateEmail = "http://members.ccoc7.com/updateemail";
            public const string DocumentUpload = "http://members.ccoc7.com/document_upload.aspx";
            public const string StoreProfile = "http://members.ccoc7.com/store-profile.aspx";
            public const string Apply = "http://members.ccoc7.com/apply.aspx?testMode=true";
            public const string PaymentType = "https://members.ccoc7.com/payment-type.aspx";
            public const string MakeMyPayment = "https://members.ccoc7.com/makemypayment.aspx";

        }

        internal class Partials
        {
            public const string OnlineReferralTermsAndConditions = "online-referral-terms-and-conditions.aspx";
        }

        internal class Staging

        {
            public const string EfpAdverseActionPage = "https://staging.ccoc7.com/efp_adverseaction.aspx";
            public const string EfpLoanDocumentPage = "http://staging.ccoc7.com/efp_loandocuments.aspx";
            public const string PaymentThankYouPage = "https://staging.ccoc7.com/payment-thankyou.aspx";
            public const string PaymentType = "https://staging.ccoc7.com/payment-type.aspx";
            public const string ExtensionMessage = "https://staging.ccoc7.com/extension-message.aspx";
            public const string EfpLoanApprovalPage = "https://staging.ccoc7.com/efp_loanapproval.aspx";
            public const string EfpPage = "https://staging.ccoc7.com/EFP.aspx";
            public const string EFPLoginPage = "http://staging.ccoc7.com/efplogin.aspx";
            public const string ExtensionTransact = "https://staging.ccoc7.com/extension-transact.aspx";
            public const string ExtensionType = "https://staging.ccoc7.com/extension-type.aspx";
            public const string ThankYouPage = "https://staging.ccoc7.com/thankyou-payment.aspx";
            public const string MemberLogin = "https://staging.ccoc7.com/memberlogin.aspx";
            public const string Member = "https://staging.ccoc7.com/member.aspx";
            public const string Referral = "https://staging.ccoc7.com/Referral.aspx";
            public const string LostPassword = "https://staging.ccoc7.com/LostPassword.aspx";
            public const string MemberSecurity = "https://staging.ccoc7.com/MemberSecurity.aspx";
            public const string ResetMyPassword = "https://staging.ccoc7.com/ResetMyPassword.aspx";
            public const string NewLoan = "https://staging.ccoc7.com/NewLoan.aspx";
            public const string LoanDisclosure = "https://staging.ccoc7.com/LoanDisclosure.aspx";
            public const string LoanThankYou = "http://staging.ccoc7.com/Loan_ThankYou.aspx";
            public const string StoreBankEdit = "https://staging.ccoc7.com/store-bank-edit.aspx";
            public const string StoreProfileEdit = "https://members.checkcity.com/store-profile-edit.aspx";
            public const string LoanApproval = "https://members.ccoc7.com/Loan_Approval.aspx";
            public const string LoanDetails = "https://staging.ccoc7.com/Loan_Details.aspx";
            public const string PersonalLoanDisclosure = "https://staging.ccoc7.com/Loan_Disclosure.aspx";
            public const string PersonalLoanThankYou = "https://staging.ccoc7.com/Loan_ThankYou.aspx";
            public const string StoreInstallmentPayment = "https://staging.ccoc7.com/Store-installment-payment.aspx";
            public const string UpdateMemberInfo = "http://staging.ccoc7.com/UpdateInfo.aspx?ReturnUrl=UpdateMember.aspx";
            public const string IncomeError = "http://staging.ccoc7.com/IncomeError.aspx?ReturnUrl=UpdateMember.aspx";
            public const string StoreChangePassword = "https://staging.ccoc7.com/store-change-password.aspx";
            public const string StoreLoanHistory = "https://staging.ccoc7.com/store-loan-history.aspx";
            public const string StoreExtension = "https://Staging.ccoc7.com/store-extension.aspx";
            public const string StoreExtensionMessage = "http://Staging.ccoc7.com/store-extension-message.aspx";
            public const string ExtensionAccept = "https://staging.ccoc7.com/extension-accept.aspx";
            public const string StorePayment = "http://Staging.ccoc7.com/store-payment.aspx";
            public const string DocumentUploadServices = "https://staging.ccoc7.com/DocumentUploadServices.aspx";
            public const string loanApply = "https://staging.ccoc7.com/apply.aspx?testMode=true";

            public const string StoreInstallmentPaymentAccept =
                "https://staging.ccoc7.com/Store-installment-payment-accept.aspx";

            public const string PaymentAccept = "https://Staging.ccoc7.com/payment-accept.aspx";
            public const string LoanAtStore = "http://staging.ccoc7.com/Loan_AtStore.aspx";
            public const string PayDayPayOff = "http://staging.ccoc7.com/Payoff.aspx";
            public const string PaymentTransact = "https://staging.ccoc7.com/payment-transact.aspx";
            public const string UpdateMember = "http://staging.ccoc7.com/UpdateMember.aspx";
            public const string UpdateMemberPassword = "http://staging.ccoc7.com/UpdatePassword.aspx";
            public const string PaydayExtension = "http://staging.ccoc7.com/Extension.aspx";
            public const string PaydayPayment = "http://staging.ccoc7.com/Payment.aspx";
            public const string OnlineReferralTermsAndConditions = "https://staging.ccoc7.com/docs/online-referral-terms-and-conditions.pdf";
            public const string UpdateEmail = "http://staging.ccoc7.com/updateemail";
            public const string DocumentUpload = "https://staging.ccoc7.com/document_upload.aspx";
            public const string StoreProfile = "https://staging.ccoc7.com/store-profile.aspx";
            public const string Apply = "https://staging.ccoc7.com/apply.aspx?testMode=true";
            public const string MakeMyPayment = "https://staging.ccoc7.com/makemypayment.aspx";
        }
    }
}