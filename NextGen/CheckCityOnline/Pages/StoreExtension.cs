﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using static CheckCityOnline.Elements.InStoreReferral;

namespace CheckCityOnline.Pages 
{
    internal class StoreExtension : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreExtension(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreExtension;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreExtension;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreExtension;
        }

        public bool IsStoreExtensionLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreExtension.BreadCrumb)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'In Store Referral' page loaded successfully");
                Logger.Trace("'In Store Referral' page is loaded. ");
                return isVisible;
            }
        }

        public void AssertStoreExtensionLoaded()
        {
            
            Assert.IsTrue(IsStoreExtensionLoaded, ErrorStrings.StoreExtensionNotVisible);
        }

        public void PickDesiredDueDate()
        {
            ExplicitWait(3);
            WaitForElement(5,Elements.StoreExtension.ExtensionDueDate);
            var extensionDate =
                Driver.FindElement(By.XPath(Elements.StoreExtension.ExtensionDueDate));
            //var date = DateTime.Today.AddDays(14);
            //string strDateOnly = date.ToString("d");
            //enterSevenDayExtensionDate.SendKeys(strDateOnly);
            extensionDate.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter extension seven days from today's date");
        }

        public void AchPaymentMethod()
        {
            WaitForElement(5, Elements.StoreExtension.AchPaymentMethod);
            var paymentMethodAch = Driver.FindElement(By.XPath(Elements.StoreExtension.AchPaymentMethod));
            paymentMethodAch.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Select ACH for the payment method");
        }

        public void ClickContinue()
        {
           WaitForElement(5,Elements.StoreExtension.Continue);
           var button = Driver.FindElement(By.XPath(Elements.StoreExtension.Continue));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click continue ");

        }

        public void ClickCallNextToChangeTheMonth()
        {
            WaitForElement(5, Elements.StoreExtension.CallNext);
            var callNext = Driver.FindElement(By.XPath(Elements.StoreExtension.CallNext));
            callNext.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Call Next button");
        }

        public void ClickDateField()
        {
            WaitForElement(5, Elements.StoreExtension.DateField);
            var dateField = Driver.FindElement(By.XPath(Elements.StoreExtension.DateField));
            dateField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click date field");

        }

        public void ClickPaymentFrequency()
        {
            WaitForElement(5, Elements.StoreExtension.PayFrequency);
            var payFrequency = Driver.FindElement(By.XPath(Elements.StoreExtension.PayFrequency));
            payFrequency.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on pay Frequency");
        }

        public void SelectPayPeriod()
        {
            WaitForElement(5, Elements.StoreExtension.PayPeriod);
            var payPeriod = Driver.FindElement(By.XPath(Elements.StoreExtension.PayPeriod));
            payPeriod.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pay Period");
        }
    }
}
