﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class StoreExtensionMessage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreExtensionMessage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreExtensionMessage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreExtensionMessage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreExtensionMessage;
        }

        public bool IsMakePaymentDueDateConfirmationPageLoaded
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.DueDateConfirmation)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'Due Date Confirmation' text is visible loaded successfully");
                Logger.Trace($"Store Due 'Date Confirmation' Screen Text is loaded. ");
                return isVisible;
            }
        }

        public bool IsFinalizeYourInstallmentPayoffPaymentLoaded
        {
            get
            {

                var isVisible = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.FinalizeYourInstallmentPayoffPayment)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Finalize Your Installment Payoff Payment is visible, page loaded successfully");
                Logger.Trace($"Finalize Your Installment Payoff Payment is visible, page loaded successfully");
                return isVisible;
            }
        }


        public void AssertStoreExtensionMessageLoaded()
        {
            //FinalizeYourInstallmentPayoffPayment
            WaitForElement(5, Elements.StoreExtensionMessage.FinalizeYourInstallmentPayoffPayment);
            Assert.IsTrue(IsFinalizeYourInstallmentPayoffPaymentLoaded, ErrorStrings.StoreExtensionsMessage.FinalizeYourInstallmentPayoffPaymentError);
        }

        public void AssertMakePaymentDueDateConfirmationPageLoaded()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.StoreExtensionMessage.DueDateConfirmation);
            Assert.IsTrue(IsMakePaymentDueDateConfirmationPageLoaded, ErrorStrings.StoreExtensionsMessage.MakePaymentDueDateConfirmationPageError);
        }

        public void ClickYesWithoutMovingTheDueDate()
        {
            WaitForElement(5,Elements.StoreExtensionMessage.MakePaymentDueDateConfirmation.YesWithoutMovingTheDueDate);
            var clickYes =
                Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.MakePaymentDueDateConfirmation.YesWithoutMovingTheDueDate));
            clickYes.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click: Yes, continue without extending the due date. ");
        }

        public void ClickSaveContinue()
        {
            WaitForElement(5, Elements.StoreExtensionMessage.SaveAndContinue);
            var clickSave = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.SaveAndContinue));
            clickSave.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save and Continue");
        }

        public void ClickMakePayment()
        {
            WaitForElement(5, Elements.StoreExtensionMessage.MakePayment);
            var makePayment = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.MakePayment));
            makePayment.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Make Payment Button Clicked");
        }
    }
}
