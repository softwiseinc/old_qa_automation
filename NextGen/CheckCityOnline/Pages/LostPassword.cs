﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using static AutomationResources.Helper.Page;

namespace CheckCityOnline.Pages
{
    internal class LostPassword : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public LostPassword(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LostPassword;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LostPassword;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LostPassword;
        }

        public bool IsInvalidDateOfBirthErrorVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.LostPassword.Error.BirthDateNotFound)).Displayed;
                const string errorText = Elements.LostPassword.Error.DateOfBirthRequiredText;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate that the expected error message is visible: {errorText}.");
                Logger.Trace($"Validate that the expected error message is visible=>{errorText}");
                return isVisible;
            }
        }

        public bool IsDateOfBirthErrorVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.LostPassword.Error.DateOfBirthRequired)).Displayed;
                const string errorText = Elements.LostPassword.Error.DateOfBirthRequiredText;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate that the expected error message is visible: {errorText}.");
                Logger.Trace($"Validate that the expected error message is visible=>{errorText}");
                return isVisible;
            }
        }

        public bool IsLostPasswordPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Lost Password page loaded successfully");
                Logger.Trace($"Lost Password page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertLostPasswordPageLoaded()
        {
            Assert.IsTrue(IsLostPasswordPageLoaded, ErrorStrings.LostPasswordPageNotLoaded);
        }

        public void ValidateEmailAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.LostPassword.Input.EmailInput);
            var emailInputField = Driver.FindElement(By.XPath(Elements.LostPassword.Input.EmailInput));
            var email = emailInputField.GetAttribute("value");
            Assert.AreEqual(user.UserName.ToLower(), email.ToLower());
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Validate the email address has display correctly. ");
        }

        public void EnterDateOfBirth(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.LostPassword.Input.DateOfBirthInput);
            var dobInput = Driver.FindElement(By.XPath(Elements.LostPassword.Input.DateOfBirthInput));
            dobInput.SendKeys(user.BirthDate);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a birthdate in the birthdate input field. ");
        }

        public void ClickSubmitButton()
        {
            WaitForElement(5, Elements.LostPassword.Button.Submit);
            var submitButton = Driver.FindElement(By.XPath(Elements.LostPassword.Button.Submit));
            submitButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Submit button after SSN challenge.");
        }

        public void ClickSubmitButtonAfterSsnChallenge()
        {
            WaitForElement(5, Elements.LostPassword.Button.SubmitAfterSsnChallenge);
            var submitButton = Driver.FindElement(By.XPath(Elements.LostPassword.Button.SubmitAfterSsnChallenge));
            submitButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Submit LinkButton. ");
        }

        public void NavigateToLostPasswordPage(Users.CurrentUser user)
        {
            var memberLoginPage = new MemberLoginPage(Driver, user);
            memberLoginPage.NavigateToLoginPage();
            memberLoginPage.EnterUserName(user);
            memberLoginPage.ClickNextButton();
            memberLoginPage.ClickForgotYourPasswordTextLink();
            AssertLostPasswordPageLoaded();
        }

        public void ClickSubmitWithoutBirthDate()
        {
            WaitForElement(5, Elements.LostPassword.Button.Submit);
            ClickSubmitButton();
            Assert.IsTrue(IsDateOfBirthErrorVisible, ErrorStrings.MissingDateOfBirth);
            var errorText = Elements.LostPassword.Error.DateOfBirthRequiredText;
            Reporter.LogPassingTestStepToBugLogger(Status.Info,$"Error is visible => {errorText}");
        }

        public void ClickSubmitWithWrongBirthDate()
        {

            WaitForElement(5, Elements.LostPassword.Button.Submit);
            var dobInput = Driver.FindElement(By.XPath(Elements.LostPassword.Input.DateOfBirthInput));
            dobInput.SendKeys(InvalidData.BirthDate);
            ClickSubmitButton();
            ExplicitWait(2);
            Assert.IsTrue(IsInvalidDateOfBirthErrorVisible, ErrorStrings.InvalidDateOfBirth);
            var errorText = Driver.FindElement(By.XPath(Elements.LostPassword.Error.BirthDateNotFound)).Text;
            Reporter.LogPassingTestStepToBugLogger(Status.Info,$"Error is visible=> {errorText}");
        }

        public bool SocialSecurityNumberHandler(Users.CurrentUser user)
        {
            bool ssnUsed = false;
            if (ElementVisibilityState(Driver, Elements.MemberSecurity.Input.SocialSecurity))
            {
                var lastFour = Driver.FindElement(By.XPath(Elements.MemberSecurity.Input.SocialSecurity));
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var storeProfilePage = new StoreProfile(Driver, user);
                lastFour.SendKeys(user.SocialSecurityNumber.Substring(user.SocialSecurityNumber.Length - 4));
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Last four numbers of Social Security Number: " + user.SocialSecurityNumber.Substring(user.SocialSecurityNumber.Length - 4));
                ClickSubmitButtonAfterSsnChallenge();
                memberLoginPage.LogInUser(user);
                memberPage.ClickUserNameMenu();
                memberPage.ClickProfile();
                storeProfilePage.ClickChangePassword();
                ssnUsed = true;
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer was not asked for last 4 numbers of Social Security Number");
            }

            return ssnUsed;
        }
    }
}