﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using static CheckCityOnline.Elements.InStoreReferral;

namespace CheckCityOnline.Pages
{
    internal class ExtensionAccept : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ExtensionAccept(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ExtensionAccept;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ExtensionAccept;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ExtensionAccept;
        }

        public bool IsExtensionAcceptLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains("extension-accept.aspx"); ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'In Store Extension accept' page loaded successfully");
                Logger.Trace("'In Store Referral' page is loaded. ");
                return isVisible;
            }
        }

        public void AssertExtensionAcceptLoaded()
        {
            
            Assert.IsTrue(IsExtensionAcceptLoaded, ErrorStrings.ExtensionAcceptNotVisible);
        }
        

        public void SignYourName()
        {
           WaitForElement(5, Elements.ExtensionAccept.SignatureField);
           var signature = Driver.FindElement(By.XPath(Elements.ExtensionAccept.SignatureField));
           signature.SendKeys(Users.Signatures.ExtensionSignature);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Signature ");
        }

        public void ClickMakePayment()
        {
            WaitForElement(5, Elements.ExtensionAccept.MakePayment);
            var makePayment = Driver.FindElement(By.XPath(Elements.ExtensionAccept.MakePayment));
            makePayment.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Make Payment");
        }

        public void FirstPaymentPlanSignature()
        {
           WaitForElement(5, Elements.ExtensionAccept.FirstPaymentPlanSignatureField);
           var paymentPlanSignature =
               Driver.FindElement(By.XPath(Elements.ExtensionAccept.FirstPaymentPlanSignatureField));
           paymentPlanSignature.SendKeys(Users.Signatures.PaymentPlanSignature);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign payment plan ");
        }

        public void ClickPaymentPlanButton()
        {
           WaitForElement(5, Elements.ExtensionAccept.ClickPaymentPlanButton);
           var clickButton = Driver.FindElement(By.XPath(Elements.ExtensionAccept.ClickPaymentPlanButton));
           clickButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Make Payment Button");
        }

        public void SecondPaymentPlanSignature()
        {
            WaitForElement(5, Elements.ExtensionAccept.SecondPaymentPlanSignatureField);
            var secondSignaturePaymentPlan =
                Driver.FindElement(By.XPath(Elements.ExtensionAccept.SecondPaymentPlanSignatureField));
            secondSignaturePaymentPlan.SendKeys(Users.Signatures.PaymentPlanSignature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign Second payment plan document ");
        }

        public void ClickAccept()
        {
            WaitForElement(5, Elements.ExtensionAccept.ClickAcceptButton);
            var clickAccept = Driver.FindElement(By.XPath(Elements.ExtensionAccept.ClickAcceptButton));
            clickAccept.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Accept Button ");
        }


        public void ClickIAgree()
        {
            WaitForElement(5, Elements.ExtensionAccept.ClickIAgreeButton);
            var iAgree = Driver.FindElement(By.XPath(Elements.ExtensionAccept.ClickIAgreeButton));
            iAgree.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click I Agree Button ");
        }
    }
}
