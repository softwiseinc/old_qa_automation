﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;





namespace CheckCityOnline.Pages
{
    internal class ExtensionMessage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ExtensionMessage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ExtensionMessage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ExtensionMessage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ExtensionMessage;
        }

        public bool IsExtensionMessagePageLoaded
        {
            get
            {

                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'extension message' page loaded successfully");
                Logger.Trace($"extension message page loaded. ");
                return isLoaded;
            }
        }

        public void AssertExtensionPageLoaded()
        {
            Assert.IsTrue(IsExtensionMessagePageLoaded, ErrorStrings.ExtensionMessagePageNotLoaded);
        }

        public void ClickYesWithoutMovingTheDueDate()
        {
            WaitForElement(5, Elements.StoreExtensionMessage.MakePaymentDueDateConfirmation.YesWithoutMovingTheDueDate);
            var clickYes =
                Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.MakePaymentDueDateConfirmation.YesWithoutMovingTheDueDate));
            clickYes.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click: Yes, continue without extending the due date. ");
        }

        public void ClickSaveContinue()
        {
            WaitForElement(5, Elements.StoreExtensionMessage.SaveAndContinue);
            var clickSave = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage.SaveAndContinue));
            clickSave.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save and Continue");
        }


        public void ClickNoIWantToExtendDueDate()
        {
            WaitForElement(5, Elements.StoreExtensionMessage.MakePaymentDueDateConfirmation.NoIWantToMoveTheDueDate);
            var clickNoIWantToExtendTheDueDateOption = Driver.FindElement(By.XPath(Elements.StoreExtensionMessage
                .MakePaymentDueDateConfirmation.NoIWantToMoveTheDueDate));
            clickNoIWantToExtendTheDueDateOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click No I want to extend the due date option");
        }
    }
}