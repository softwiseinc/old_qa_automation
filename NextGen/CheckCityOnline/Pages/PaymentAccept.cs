﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class PaymentAccept : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public PaymentAccept(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaymentAccept;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaymentAccept;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaymentAccept;
        }

        public bool IsPaymentAcceptLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info,
                    " Validate the message 'Payment Accept' is loaded. ");
                Logger.Trace($"Payment Accept Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertPaymentAcceptLoaded()
        {
            
            ExplicitWait(3);
            Assert.IsTrue(IsPaymentAcceptLoaded, ErrorStrings.StorePaymentAcceptNotVisible);

        }


        public void ClickMakePayment()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.MakeAPayment);
            var makePaymentButton =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.MakeAPayment));
            makePaymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click make payment");
        }

        public void ClickNextButton()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.Next);
            var makePaymentButton =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.Next));
            makePaymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click make payment");
        }

        public void ClickAcceptButton()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.Accept);
            var acceptButton =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.Accept));
            acceptButton.Click();
        }

        public void ClickIAgree()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.IAgree);
            var acceptButton =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.PaymentAccept.Buttons.IAgree));
            acceptButton.Click();
        }
    }
}
