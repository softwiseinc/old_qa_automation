﻿using System;
using NLog;
using System.Threading;
using AutomationResources;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace CheckCityOnline.Pages
{
    public class BaseApplicationPage
    {
        protected IWebDriver Driver { get; set; }
        public TestEnvironment TestEnvironment { get; } = new TestEnvironment();

        public BaseApplicationPage(IWebDriver driver)
        {
            Driver = driver;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void ExplicitWait(int seconds)
        {
            int waitTime = seconds * 1000;
            PopUpHandler();
            Thread.Sleep(waitTime);
            PopUpHandler();
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                Driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }


  

        public void WaitForElement(int seconds, string elementString)
        {
            //ExplicitWait(seconds);
            PopUpHandler();
            Logger.Trace($"WAITING FOR ELEMENT: {elementString}");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(seconds));
            PopUpHandler();
            var element = By.XPath(elementString);
            var config = TestConfigManager.Config;
            try
            {
                PopUpHandler();
                wait.Until(ExpectedConditions.ElementIsVisible(element));
            }
            catch (Exception e)
            {
                Logger.Error($"ERROR: Element was not found {elementString}");
                Logger.Error(e);
                if(config.GenerateCustomers != true)
                    throw;
            }

            Logger.Trace($"ELEMENT FOUND: '{elementString}' <grin>");
        }

        private void PopUpHandler()
        {
            try
            {
                var isPresent = Driver.FindElement(By.XPath(Elements.MarketingPopup.GiveAway)).Displayed;
                if (isPresent)
                {
                    ExplicitWait(5);
                    Driver.FindElement(By.XPath(Elements.MarketingPopup.GiveAway)).Click();
                }
            }
            catch (Exception)
            {
                Logger.Trace($"test for pop-up");
            }
        }
    }

    public class TestEnvironment
    {
        public string Url { get; set; }
    }
}