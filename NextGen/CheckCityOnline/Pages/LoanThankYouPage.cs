﻿using System;
using System.Diagnostics;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class LoanThankYouPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public LoanThankYouPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanThankYou;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanThankYou;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanThankYou;
        }

        public bool IsPendingApproval
        {
            get
            {
                var isLoaded = Driver.Url.Contains("Pending%20All");
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Current Loan Status: Pending Approval is visible ");
                Logger.Trace($"Current Loan Status: Pending Approval =>{isLoaded}");
                return isLoaded;
            }

        }

        public bool IsLoanThankYouPageLoaded
        {
            get
            {
                var isLoaded = TestEnvironment.Url.Contains("Loan_ThankYou");
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the Thank you page loaded successfully");
                Logger.Trace($"thank you page is loaded=>{isLoaded}");
                return isLoaded;
            }

        }
        public void AssertLoanThankYouPageLoaded()
        {
            try
            {
                ExplicitWait(7);
                Assert.IsTrue(IsLoanThankYouPageLoaded, ErrorStrings.ThankYouPageNotVisible);
            }
            catch (Exception e)
            {
                ExplicitWait(17);
                Assert.IsTrue(IsLoanThankYouPageLoaded, ErrorStrings.ThankYouPageNotVisible);
                var isLoaded = TestEnvironment.Url.Contains("Loan_ThankYou");
                if (!isLoaded)
                {
                    Assert.IsTrue(IsLoanThankYouPageLoaded, ErrorStrings.ThankYouPageNotVisible);
                    Debug.WriteLine(e);
                    throw;
                }
            }
            




        }

        public bool IsCurrentLoanIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ThankYou.ScreenText.CurrentLoanStatusPendingApproval))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
                Logger.Trace("Member page is loaded. ");
                return isVisible;
            }
        }

        public void AssertCurrentLoanIsVisible()
        {
            WaitForElement(10, Elements.ThankYou.ScreenText.CurrentLoanStatusPendingApproval);
            Assert.IsTrue(IsCurrentLoanIsVisible, ErrorStrings.CurrentLoanIsNotVisible);
        }





        public void ClickYourOpinionCounts()
        {
            ExplicitWait(8);
            try
            {
                
                WaitForElement(15, Elements.ThankYou.Button.ClickOpinionCounts);
                var clickOpinionCounts = Driver.FindElement(By.XPath(Elements.ThankYou.Button.ClickOpinionCounts));
                clickOpinionCounts.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the red 'X' to close the survey. ");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Survey window did not show");
            }

        }


        public void ClickEndMySession()
        {
            WaitForElement(6, Elements.ThankYou.Button.EndMySessionNow);
            ExplicitWait(4);
            //Driver.FindElement(By.XPath(Elements.ThankYou.Button.EndMySession)).Click();
            Driver.FindElement(By.XPath(Elements.ThankYou.Button.EndMySessionNow)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the 'End My Session' button. ");
        }

        public string GetLoanAgreementDate()
        {
            WaitForElement(5, Elements.ThankYou.Button.LoanAggreement);
            ExplicitWait(4);
            var loanAgreementText = Driver.FindElement(By.XPath(Elements.ThankYou.Button.LoanAggreement)).Text;
            int first = loanAgreementText.IndexOf("by") + "by".Length;
            int last = loanAgreementText.LastIndexOf(".");
            string str2 = loanAgreementText.Substring(first, last - first);
            DateTime enteredDate = DateTime.Parse(str2.Trim());
            var loanDate = enteredDate.ToString("MM/dd/yyyy");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Loan agreement date is  "+ loanDate);
            return loanDate;
            
        }

        public void ClickAccountMenu()
        {
            WaitForElement(5, Elements.ThankYou.Button.MyAccount);
            ExplicitWait(4);
            Driver.FindElement(By.XPath(Elements.ThankYou.Button.MyAccount)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the 'AccountLink' menu. ");
        }

        public void AssertLoanApprovalDate(string CmpApprovalDate)
        {
            ExplicitWait(5);
            var loanApprovalDate = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.ScreenText.LoanApprovalDate)).Text;
            var  flag = false;
            if (DateTime.Parse(loanApprovalDate) == DateTime.Parse(CmpApprovalDate) )
            {
                flag = true;
            }
            Assert.IsTrue(flag, ErrorStrings.LoanApprovalDateNotMatched);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,  " Validate that the Loan approval date is matched. " + loanApprovalDate);
        }


        public void AssertPendingApproval()
        {
            ClickYourOpinionCounts();
            WaitForElement(5, Elements.ThankYou.ScreenText.CurrentLoanStatus);
            Assert.IsTrue(IsPendingApproval, ErrorStrings.LoanApprovalDateNotMatched);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Application is Pending Approval");
        }

        public void AssertCustomerPendingApproval()
        {
            WaitForElement(5, Elements.LoanApproval.YourAlmostDone);
            Assert.IsTrue(IsPendingApproval, ErrorStrings.ApplicationNotPending);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Application is Pending Approval");
        }

    }
}