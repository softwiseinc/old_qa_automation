﻿using System.Collections.Generic;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class EfpLoanApprovalPage : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public EfpLoanApprovalPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EfpLoanApprovalPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EfpLoanApprovalPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.EfpLoanApprovalPage;
        }

        public bool IsEfpLoanApprovalPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Efp Login page loaded successfully");
                Logger.Trace($"Efp login Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertEfpLoanApprovalPageLoaded()
        {
            Assert.IsTrue(IsEfpLoanApprovalPageLoaded, ErrorStrings.EFPPageNotLoaded);
        }


        public void ClickLoanDocumentsTab()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocuments);
            var loanDocumentsTab =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs
                    .LoanDocuments));
            loanDocumentsTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Loan Doc tab");
        }

        public void ClickCustomerState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.Dropdown.CustomerState);
            var customerStateDropdown = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.Dropdown.CustomerState));
            customerStateDropdown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click On customer state drop down");
        }

        public void SelectUtahAsState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.Dropdown.DropdownStateOptions.StateUtah);
            var selectUtahAsTheState = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.Dropdown.DropdownStateOptions.StateUtah));
            selectUtahAsTheState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select on Utah ");

        }

        public void EnterCustomerID(Users.CurrentUser ccoUserApproval)
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.InputFields.CustomerID);
            var customerIDInputField = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.InputFields.CustomerID));
            customerIDInputField.SendKeys(Users.User.CustomerID);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer ID ");
        }

        public void ClickLookupCustomer()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.Buttons.LookupCustomer);
            var lookupCustomerButton = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.Buttons.LookupCustomer));
            lookupCustomerButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click lookup Customer button");
        }
        
        public bool IsDateSignedIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.AssertItems.DateSigned)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the date signed is visible");
                Logger.Trace($"Date signed is Visible=>{isVisible}");
                return isVisible;
            }
        }
        
        public void AssertDateSignedIsVisible()
        {
            Assert.IsTrue(IsDateSignedIsVisible, ErrorStrings.DateSignedOnLoanDocumentSectionNotLoaded);

        }

        public void ClickLoanId()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.Buttons.LoanIdNumber);
            var loanIDLink = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.Buttons.LoanIdNumber));

            
            ElementVisibilityState(Driver, "//th[contains(text(), 'Loan ID')]//following::td[position()=2]");
            loanIDLink.Click();

            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on loan iD number link");
        }
        public static bool ElementVisibilityState(IWebDriver driver, string element)
        {
            List<IWebElement> elementList = new List<IWebElement>();
            bool isVisible = false;

            elementList.AddRange(driver.FindElements(By.XPath(element)));

            if (elementList.Count > 0)
            {
                isVisible = true;
            }
            return isVisible;
        }
        public void ClickDocumentId()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.LoanDocumentListPopup.Buttons.DocumentId);
            var documentIdLink = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.LoanDocumentListPopup.Buttons.DocumentId));
            documentIdLink.Click();
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on the document ID link");
        }
        public bool IsLoanDocumentHeaderTextIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.AssertItems.LoanDocuments)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the date signed is visible");
                Logger.Trace($"Date signed is Visible=>{isVisible}");
                return isVisible;
            }
        }
        public void AssertLoanDocumentHeaderTextIsVisible()
        {
            Assert.IsTrue(IsLoanDocumentHeaderTextIsVisible, ErrorStrings.LoanDocumentsHeaderTextIsNotVisible);
        }

       
    }
}