﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class IncomeErrorPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public IncomeErrorPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.IncomeError;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.IncomeError;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.IncomeError;
        }

        public bool IsUpdateIncomePageLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.IncomeError.ScreenText.UpdateIncome)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the Update Income page Loaded.");
                Logger.Trace($"Validate the Update Income page Loaded is visable=>{isVisible}");
                return isVisible;
            }
        }

        internal void ValidatePageLoaded()
        {
            WaitForElement(5, Elements.IncomeError.ScreenText.UpdateIncome);
            Assert.IsTrue(IsUpdateIncomePageLoaded, ErrorStrings.UpdateIncomePage);
        }
    }
}