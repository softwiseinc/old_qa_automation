﻿using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using AutomationResources;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using Logger = NLog.Logger;


namespace CheckCityOnline.Tests
{
    public class ApplyLoan : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ApplyLoan(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.loanApply;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.loanApply;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.loanApply;
        }


        public bool IsApplyLoanPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("apply.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Apply Loan page loaded successfully");
                Logger.Trace($"Apply Loan page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsZipErrorVisible
        {
            get
            {
                 var isVisible = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ZipCodeError))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the zip code tool tip is visible");
                Logger.Trace($"Error Tool Tip is visible");
                return isVisible;
            }
        }

        public bool IsMonthlyIncomeVisible
        {
            get
            {
                bool isVisible = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.MonthlyIncome))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate Monthly Income NOT visible");
                Logger.Trace($"Error Montly Income is visible");
                return isVisible;
            }
        }

        public void NavigateToApplyLoanPage()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.FirstName);
            Assert.IsTrue(IsApplyLoanPageLoaded, ErrorStrings.ApplyLoanPageNotLoaded);
        }



        public void FillPersonalInformationFirstName_Valid(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.FirstName);
            var enterFirstName = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.FirstName));
            enterFirstName.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing First Name was cleared then the new First Name is entered: " + user.FirstName);
        }

        public void FillPersonalInformationFirstNameValidationCheck(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.FirstName);
            var enterFirstName = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.FirstName));
            enterFirstName.Clear();
            var flag = false;
            if (enterFirstName.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            enterFirstName.SendKeys(user.FirstName);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that empty 'First Name ' is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.FirstName);
        }

        public void FillPersonalInformationLastName_Valid(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.LastName);
            var enterLastName = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.LastName));
            enterLastName.Clear();
            enterLastName.SendKeys(user.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Last Name was cleared then the new Last Name is entered: " + user.LastName);
        }

        public void FillPersonalInformationLastNameValidationCheck(Users.CurrentUser user)
        {

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.LastName);
            var lastName = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.LastName));
            lastName.Clear();
            var flag = false;
            if (lastName.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            lastName.SendKeys(user.LastName);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that empty 'Last Name ' is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.LastName);
        }


        public void FillPersonalInformationEmail_Valid()
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < 7; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.EmailAddress);
            var enterEmail = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.EmailAddress));
            enterEmail.Clear();
            string randomEmail = builder.ToString().ToLower() + "@nostromorp.com";
            enterEmail.SendKeys(randomEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Email was cleared then the new Email is entered: " + randomEmail);
        }

        public void FillPersonalInformationEmailValidationCheck(Users.CurrentUser user)
        {

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.EmailAddress);
            var emailAddress = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.EmailAddress));
            emailAddress.Clear();

            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < 7; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            emailAddress.SendKeys(builder.ToString().ToLower()); //enter invalid email

            var flag = false;
            if (emailAddress.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            emailAddress.SendKeys(builder.ToString().ToLower() + "@" + TestConfigManager.Config.CatchAllDomain);
            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that invalid Email is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.Email);
        }


        public void FillPersonalInformationDob_Valid(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.DOB);
            var enterDob = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.DOB));
            enterDob.Clear();
            enterDob.SendKeys(Users.User.DateOfBirth);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing DOB was cleared then the new DOB is entered. ");
        }

        public void FillPersonalInformationDobValidationCheck(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.DOB);
            var enterDob = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.DOB));
            enterDob.Clear();
            var flag = false;
            if (enterDob.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that empty 'DOB ' is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.DobEmpty);
        }

        public void FillPersonalInformationInvalidDobCheck(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.DOB);
            var enterDob = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.DOB));
            enterDob.SendKeys(user.InvalidBirthDate);


            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.EmailAddress);
            var emailAddress = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.EmailAddress));
            emailAddress.Click();

            var flag = false;
            if (enterDob.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            enterDob.Clear();
            enterDob.SendKeys(user.InvalidBirthDate);

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Invalid 'DOB ' is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.DobInvalid);
        }

        public void FillPersonalInformationZipCode_Valid(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ZipCode));
            enterZipCode.Clear();
            enterZipCode.SendKeys(Users.User.ZipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing zip code was cleared then the new zip code is entered. ");
        }

        public void FillPersonalInformationZipCodeEmptyValidation(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ZipCode));
            enterZipCode.Clear();

            var flag = false;
            if (enterZipCode.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed)
            {
                flag = true;
            }

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Empty 'Zip code ' is not submitted.");
            Assert.IsTrue(flag, ApplyLoanErrorString.ZipCodeEmpty);


        }

        public void FillPersonalInformationZipCodeInvalidValidation(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            ClickPersonalInformationContinueBtn();

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ZipCode));
            enterZipCode.Clear();
            enterZipCode.SendKeys(user.InvalidZipCode);

            ClickPersonalInformationContinueBtn();
            ExplicitWait(3); // wait for error class

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Invalid 'Zip code ' is not submitted.");
            Assert.IsTrue(IsZipErrorVisible, ApplyLoanErrorString.ZipCodeInvalid);

        }

        public void ClickPersonalInformationContinueBtn()
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ContinueBtn);
            var continueBtn = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ContinueBtn));
            continueBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Continue Button is clicked. ");
        }


        public void ClickPersonalInformationPaydayRadio()
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.PaydayLoan);
            var paydayLoanRadio = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.PaydayLoan));
            paydayLoanRadio.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Payday Loan radio button is clicked. ");
        }

        public void FillIncomeInformationMonthlyIncome_Valid()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.MonthlyIncome);
            var enterAmount = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.MonthlyIncome));
            enterAmount.Clear();
            enterAmount.SendKeys(ApplyLoanIncomeInformation.MonthlyIncome);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing monthly income amount was cleared then the new monthly income amount is entered. ");
        }

        public void ValidateIncomeInformationNotVisible()
        {
            Assert.IsFalse(IsMonthlyIncomeVisible, "Monthly Income IS visible");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Income Information is not visible.");
        }

        public void SelectIncomeInformationPayPeriodOptionOne()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.Payperiod);
            var payperiod = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.Payperiod)));
            var selectElement = new SelectElement(payperiod);
            selectElement.SelectByText(ApplyLoanIncomeInformation.PayPeriod_option_1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The Payperiod " + ApplyLoanIncomeInformation.PayPeriod_option_1 + "is selected.  ");

        }

        public void SelectIncomeInformationPayPeriodOptionTwo()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.Payperiod);
            var payperiod = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.Payperiod)));
            var selectElement = new SelectElement(payperiod);
            selectElement.SelectByText(ApplyLoanIncomeInformation.PayPeriod_option_2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The Payperiod " + ApplyLoanIncomeInformation.PayPeriod_option_2 + "is selected.  ");

        }

        public void SelectIncomeInformationPayPeriodOptionThree()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.Payperiod);
            var payperiod = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.Payperiod)));
            var selectElement = new SelectElement(payperiod);
            selectElement.SelectByText(ApplyLoanIncomeInformation.PayPeriod_option_1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The Payperiod " + ApplyLoanIncomeInformation.PayPeriod_option_3 + "is selected.  ");

        }

        public void SelectIncomeInformationPayPeriodOptionFour()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.Payperiod);
            var payperiod = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.Payperiod)));
            var selectElement = new SelectElement(payperiod);
            selectElement.SelectByText(ApplyLoanIncomeInformation.PayPeriod_option_4);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The Payperiod " + ApplyLoanIncomeInformation.PayPeriod_option_4 + "is selected.  ");
        }

        public void SelectIncomeInformationTwiceAmonthDayOne()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.day_one);
            var dayOne = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.day_one)));
            var selectElement = new SelectElement(dayOne);
            selectElement.SelectByText(ApplyLoanIncomeInformation.DayOne);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                ApplyLoanIncomeInformation.DayOne + "is selected on \"Days of the Month Paid\".  ");
        }

        public void SelectIncomeInformationTwiceAmonthDayTwo()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.day_two);
            var dayTwo = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.day_two)));
            var selectElement = new SelectElement(dayTwo);
            selectElement.SelectByText(ApplyLoanIncomeInformation.DayTwo);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                ApplyLoanIncomeInformation.DayTwo + "is selected on \"Days of the Month Paid\".  ");
        }

        public void SelectIncomeInformationBiWeekly()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.SelBiWeekly);
            var selBiWeekly = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.SelBiWeekly)));
            var selectElement = new SelectElement(selBiWeekly);
            selectElement.SelectByText(ApplyLoanIncomeInformation.DayOfWeekpaid);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                ApplyLoanIncomeInformation.DayOfWeekpaid + "is selected on \"Day of the Week Paid\".  ");
        }

        public void SelectIncomeInformationPayDay()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.SelBiWeeklyDate);
            var selPayDay = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.SelBiWeeklyDate)));
            var selectElement = new SelectElement(selPayDay);
            selectElement.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Weekly Pay day selected ");
        }

        public void SelectIncomeInformationIncomeType()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.IncomeType);
            var incomeType = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.IncomeType)));
            var selectElement = new SelectElement(incomeType);
            selectElement.SelectByText(ApplyLoanIncomeInformation.IncomeType);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                ApplyLoanIncomeInformation.IncomeType + "is selected as income type.  ");
        }

        public void FillIncomeInformationCompanyName()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.NameOfCompany);
            var nameOfCompany = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.NameOfCompany));
            //NameOfCompany.Clear();
            nameOfCompany.SendKeys(ApplyLoanIncomeInformation.CompanyName);
            nameOfCompany.SendKeys(Keys.Enter);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Company Name was cleared then the new company name is entered. ");
        }

        public void FillIncomeInformationPhoneNumber()
        {
            
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpPhoneNumber);
            var empPhoneNumber = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.EmpPhoneNumber));
            empPhoneNumber.Click();
            empPhoneNumber.SendKeys(ApplyLoanIncomeInformation.EmployerPhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Phone Number was cleared then the new Phone Number is entered. ");
        }

        public void FillIncomeInformationCity()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpCity);
            var empCity = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.EmpCity));
            empCity.Clear();
            empCity.SendKeys(ApplyLoanIncomeInformation.EmployersCity);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing City was cleared then the new City is entered. ");
        }

        public void SelectIncomeInformationState()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpState);
            var empState = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.EmpState)));
            var selectElement = new SelectElement(empState);
            selectElement.SelectByText(ApplyLoanIncomeInformation.EmployersState);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,  "is selected as State.");
        }

        public void FillIncomeInformationZipCode()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpZipCode);
            var empZipCode = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.EmpZipCode));
            //EmpZipCode.Clear();
            empZipCode.SendKeys(ApplyLoanIncomeInformation.EmployersZipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Zip Code was cleared then the new Zip Code is entered. ");
        }

        public void SelectIncomeInformationTimeWithEmp_Year()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpTimeYear);
            var empTimeYear = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.EmpTimeYear)));
            var selectElement = new SelectElement(empTimeYear);
            selectElement.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Time (Year) with employer is selected.");
        }

        public void SelectIncomeInformationTimeWithEmp_Month()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.EmpTimeMonth);
            var empTimeMonth = Driver.FindElement((By.XPath(Elements.ApplyLoan.IncomeInformation.EmpTimeMonth)));
            var selectElement = new SelectElement(empTimeMonth);
            selectElement.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Time (Month) with employer is selected.");
        }

        public void ClickYesDirectDeposit()
        {

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.DirectDepositYes);
            var directDepositYes = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.DirectDepositYes));
            directDepositYes.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Direct Deposit radio button 'YES' is clicked. ");
        }
        
        //BANK ACCOUNT
        public void FillBankInformationRouting()
        {
            WaitForElement(5, Elements.ApplyLoan.BankInformation.RoutingNumber);
            var routingNumber = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber));
            // RoutingNumber.Clear();
            routingNumber.SendKeys(ApplyBankInformation.RoutingNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Routing Number was cleared then the new Routing Number is entered. ");
        }

        public void FillBankInformationBankAccount()
        {
            WaitForElement(5, Elements.ApplyLoan.BankInformation.BankAccount);
            var bankAccount = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount));
            //BankAccount.Clear();
            bankAccount.SendKeys(ApplyBankInformation.BankAccount);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Bank AccountLink was cleared then the new Bank AccountLink is entered. ");
        }

        public void FillBankInformationBankAccountCheck()
        {
            WaitForElement(5, Elements.ApplyLoan.BankInformation.ReBankAccount);
            var bankAccount = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.ReBankAccount));
            bankAccount.Clear();
            bankAccount.SendKeys(ApplyBankInformation.ReBankAccount);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Bank AccountLink was cleared then the new Bank AccountLink is entered. ");
        }

        public void SelectBankInformationTimeWithEmp_Year()
        {
            WaitForElement(5, Elements.ApplyLoan.BankInformation.BankAccountOpenYear);
            var bankAccountOpenYear =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.BankInformation.BankAccountOpenYear)));
            var selectElement = new SelectElement(bankAccountOpenYear);
            selectElement.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Time (Year) with Bank account is selected.");
        }

        public void SelectBankInformationTimeWithEmp_Month()
        {
            WaitForElement(5, Elements.ApplyLoan.BankInformation.BankAccountOpenMonth);
            var empTimeMonth = Driver.FindElement((By.XPath(Elements.ApplyLoan.BankInformation.BankAccountOpenMonth)));
            var selectElement = new SelectElement(empTimeMonth);
            selectElement.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Time (Month) with employer is selected.");
        }
        public void SelectAccountTypeChecking()
        {

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.AccountTypeChecking);
            var accountTypeChecking = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.AccountTypeChecking));
            accountTypeChecking.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Radio Button Account Type Checking Selected. ");
        }
        public void SelectHomeRent()
        {

            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.HomeRent);
            var accountTypeChecking = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.HomeRent));
            accountTypeChecking.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Radio Button Home Rent is Selected. ");
        }

        public void UseBankLoanPayment()
        {
            WaitForElement(7, Elements.ApplyLoan.BankInformation.UseBankLoanPayment);
            var useBankLoanPayment =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.BankInformation.UseBankLoanPayment)));
            useBankLoanPayment.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "\"Do you want to use this bank account for your loan payments\" is selected.");
        }

        
        public void BankaccountHandler()
        {
            var user = new Users();
            var routingNumber = user.GetRandomRoutingNumber();
            var accountNumber = user.GetAccountNumber();

            if (Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber)).Text.Length == 0)
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber)).SendKeys(routingNumber);

            if (Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount)).Text.Length == 0)
            {
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount)).Clear();
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.ReBankAccount)).Clear();
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount)).SendKeys(accountNumber);
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.ReBankAccount)).SendKeys(accountNumber);
            }
            SelectBankInformationTimeWithEmp_Year();
            SelectBankInformationTimeWithEmp_Month();
        }
        public void SelectBankTypeChecking()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.BankTypeChecking);
            IWebElement identificationType =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.IdentifyInformation.BankTypeChecking)));
            identificationType.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Identification Type is selected.");
        }

        //Identification Number
        public void SelectDirectDeposit()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.DirectDepositYes);
            IWebElement directDeposit =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.IdentifyInformation.DirectDepositYes)));
            directDeposit.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Identification Type is selected.");
        }

        public void SelectIdentificationType()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.IdentificationType);
            var identificationType =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.IdentifyInformation.IdentificationType)));
            var selectElement = new SelectElement(identificationType);
            selectElement.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Identification Type is selected.");
        }
        public void SelectIdentificationInformationState()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.IdentificationState);
            var identificationState =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.IdentifyInformation.IdentificationState)));
            identificationState.Click();
            var selectElement = new SelectElement(identificationState);
            identificationState.SendKeys(IdentificationInformation.State);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Identification state is selected.");
        }


        public void FillIdentificationNumber()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.IdentificationNumber);
            var identificationNumber =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.IdentifyInformation.IdentificationNumber));
            identificationNumber.Clear();
            identificationNumber.SendKeys(IdentificationInformation.IdentificationNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Identification Number was cleared then the new Identification Number is entered. ");
        }

        public void FillIdentificationExpirationDate()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.ExpirationDate);
            var expirationDate = Driver.FindElement(By.XPath(Elements.ApplyLoan.IdentifyInformation.ExpirationDate));
            expirationDate.Clear();
            expirationDate.SendKeys(IdentificationInformation.ExpirationDate);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Expiry Date was cleared then the new Expiry Date is entered. ");
        }

        public void FillIdentificationSocialSecurity()
        {
            Random r = new Random();
            var genRand = r.Next(0, 999999999);
            while (genRand.ToString().Length < 9)
                genRand = r.Next(0, 999999999);

            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.SocialSecurityNumber);
            var socialSecurityNumber =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.IdentifyInformation.SocialSecurityNumber));
            socialSecurityNumber.Clear();
            ExplicitWait(4);
            socialSecurityNumber.SendKeys(genRand.ToString());
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Social Security Number was cleared then the new Identification Number is entered: " + genRand);
        }

        //Contact information
        public void FillContactInformationStreetAddress()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.streetAddress);
            var streetAddress = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.streetAddress));
            var cityStateZipDisplay =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.cityStateZipDisplay));
            //streetAddress.Clear();
            streetAddress.SendKeys(cityStateZipDisplay.Text);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Street address was cleared then the newStreet address is entered. ");
        }

        public void SelectResidenceLengthYear()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.residenceLengthYear);
            var residenceLengthYear =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.ContactInformation.residenceLengthYear)));
            var selectElement = new SelectElement(residenceLengthYear);
            selectElement.SelectByIndex(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Residence Length Year is selected.");
        }

        public void SelectResidenceLengthMonth()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.residenceLengthMonth);
            var residenceLengthMonth =
                Driver.FindElement((By.XPath(Elements.ApplyLoan.ContactInformation.residenceLengthMonth)));
            var selectElement = new SelectElement(residenceLengthMonth);
            selectElement.SelectByIndex(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Residence Length Month is selected.");
        }

        public void FillContactInformationPhoneNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.phoneNumber);
            var phoneNumber = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.phoneNumber));
            //phoneNumber.Clear();
            phoneNumber.SendKeys(Users.Input.PhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Phone Number was cleared then the Phone Number is entered. ");
        }

        public void FillContactInformationPhoneNumber(string userPhoneNumber)
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.phoneNumber);
            var phoneNumber = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.phoneNumber));
            phoneNumber.SendKeys(userPhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Phone Number was cleared then the Phone Number is entered. ");
        }
        public void ClickContactInformationSendPhoneVerification()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.validatePhoneBtn);
            var validatePhoneBtn = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.validatePhoneBtn));
            validatePhoneBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The existing send code button is clicked.");
        }

        public void ClickContactInformationSendPhoneVerificationCode(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.phoneNumberVerifyCode);
            var phoneNumberVerifyCode =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.phoneNumberVerifyCode));
            //phoneNumberVerifyCode.Clear();
            //phoneNumberVerifyCode.SendKeys(user.PhoneNumberVerifyCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The Phone number verification code is cleared and entered new one.");
        }

        public void ClickContactInformationOptInSms()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.optInSMS);
            var optInSms = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.optInSMS));
            optInSms.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The automated call and sms is accepted.");
        }

        public void ClickContactInformationOptInSmsMarketing()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.optInSMSMarketing);
            var optInSmsMarketing =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.optInSMSMarketing));
            optInSmsMarketing.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The automated call and sma for marketing is accepted.");
        }

        //Security Information
        public void FillSecurityInformationPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyLoan.SecurityInformation.createPassword);
            var createPassword = Driver.FindElement(By.XPath(Elements.ApplyLoan.SecurityInformation.createPassword));
            createPassword.Clear();
            createPassword.SendKeys(SecurityInformation.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "The existing Phone Number was cleared then the Phone Number is entered. ");
        }

        //Submit information
        public void ClickSubmitInformationAgreeConsent()
        {
            WaitForElement(5, Elements.ApplyLoan.SubmitInformation.agreeConsent);
            var agreeConsent = Driver.FindElement(By.XPath(Elements.ApplyLoan.SubmitInformation.agreeConsent));
            agreeConsent.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The Agree Consent button is clicked.");
        }

        public void ClickSubmitInformationAgreeTerms()
        {
            WaitForElement(5, Elements.ApplyLoan.SubmitInformation.agreeTerms);
            var agreeTerms = Driver.FindElement(By.XPath(Elements.ApplyLoan.SubmitInformation.agreeTerms));
            agreeTerms.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The Agree Terms button is clicked.");
        }

        public void ClickSubmitInformationSubmitBtn()
        {
            WaitForElement(5, Elements.ApplyLoan.SubmitInformation.SubmitBtn);
            var submitBtn = Driver.FindElement(By.XPath(Elements.ApplyLoan.SubmitInformation.SubmitBtn));
            submitBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The Submit button is clicked.");
            ExplicitWait(5);
        }

        public void EmptyInputValidationCheck()
        {

            ExplicitWait(1);

            IList<IWebElement> elements = Driver.FindElements(By.XPath(Elements.ApplyLoan.ErrorPath.InputSelect));
            for (int webElement = 0; webElement < elements.Count; webElement++)
            {
                var text = elements[webElement].Text;
                if (text == "" || !elements[webElement].Selected)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info,
                        "Validate that Empty '" + elements[webElement].GetAttribute("id") + "' is not submitted.");

                }
                else
                {
                    Assert.Fail(" Empty field: '" + elements[webElement].GetAttribute("id") + "' is submitted.");
                }
            }

        }

        public void InvalidInputValidationCheck(Users.CurrentUser user)
        {

            ExplicitWait(5);

            IList<IWebElement> elements = Driver.FindElements(By.XPath(Elements.ApplyLoan.ErrorPath.InputSelect));
            foreach (var webElement in elements)
            {
                var elementId = webElement.GetAttribute("id");

                if (elementId == "monthlyIncome")
                {
                    WaitForElement(5, Elements.ApplyLoan.IncomeInformation.MonthlyIncome);
                    var enterAmount = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.MonthlyIncome));
                    enterAmount.Clear();
                    enterAmount.SendKeys(ApplyLoanIncomeInformation.MonthlyIncomeInvalid);
                    Driver.FindElement(By.TagName("body")).Click();

                    ExplicitWait(3);

                    bool flag = enterAmount.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.Input)).Displayed;

                    ExplicitWait(3);

                    Reporter.LogTestStepForBugLogger(Status.Info,
                        "Validate that Invalid 'monthly income ' is not submitted.");
                    Assert.IsTrue(flag, ApplyLoanErrorString.MonthlyIncomeInvalid);
                    enterAmount.Clear();
                    enterAmount.SendKeys(ApplyLoanIncomeInformation.MonthlyIncome);
                }

                if (elementId == "payPeriod")
                {
                    SelectIncomeInformationPayPeriodOptionOne();
                }

                if (elementId == "previousPayday")
                {
                    SelectIncomeInformationBiWeekly();
                }

                if (elementId == "secondPayday")
                {
                    SelectIncomeInformationPayDay();
                }

                if (elementId == "incomeType")
                {
                    SelectIncomeInformationIncomeType();
                }

                if (elementId == "employerName")
                {
                    FillIncomeInformationCompanyName();
                }

                if (elementId == "employerPhone")
                {
                    FillIncomeInformationPhoneNumber();
                }

                if (elementId == "employerCity")
                {
                    FillIncomeInformationCity();
                }

                if (elementId == "employerState")
                {
                    SelectIncomeInformationState();
                }

                if (elementId == "employerZip")
                {
                    FillIncomeInformationZipCode();
                }

                if (elementId == "employerTimeYear")
                {
                    SelectIncomeInformationTimeWithEmp_Year();
                }

                if (elementId == "employerTimeMonth")
                {
                    SelectIncomeInformationTimeWithEmp_Month();
                }

                if (elementId == "routingNumber")
                {
                    WaitForElement(5, Elements.ApplyLoan.BankInformation.RoutingNumber);
                    var routingNumber = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber));
                    routingNumber.Clear();
                    routingNumber.SendKeys(ApplyBankInformation.RoutingNumberInvalid);
                    Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankSectionHeader)).Click();
                    ExplicitWait(5);
                    routingNumber.Click();
                    bool flag = routingNumber.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.RoutingNumberInput)).Displayed;
                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info,
                        "Validate that Invalid 'Routing Number' is not submitted.");
                    Assert.IsTrue(flag, ApplyLoanErrorString.BankRoutingNUmberInvalid);
                    routingNumber.Clear();
                    routingNumber.SendKeys(ApplyBankInformation.RoutingNumber);
                }

                if (elementId == "bankAccount")
                {
                    WaitForElement(5, Elements.ApplyLoan.BankInformation.BankAccount);
                    var bankAccount = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount));
                    bankAccount.Clear();
                    bankAccount.SendKeys(ApplyBankInformation.BankAccountInvalid);
                    Driver.FindElement(By.TagName("body")).Click();

                    ExplicitWait(3);

                    bool flag = bankAccount.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.BankAccountInput)).Displayed;

                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info,
                        "Validate that Invalid 'Bank AccountLink' is not submitted.");
                    Assert.IsTrue(flag, ApplyLoanErrorString.BankAccountInvalid);
                    bankAccount.Clear();
                    bankAccount.SendKeys(ApplyBankInformation.BankAccount);

                    FillBankInformationBankAccountCheck();
                }

                if (elementId == "bankAccountLengthYear")
                {
                    SelectBankInformationTimeWithEmp_Year();
                }

                if (elementId == "bankAccountLengthMonth")
                {
                    SelectBankInformationTimeWithEmp_Month();
                }

                if (elementId == "switch_bankType_right")
                {
                    SelectBankTypeChecking();
                }

                if (elementId == "switch_firstIncome_left")
                {
                    SelectDirectDeposit();
                }
                
                if (elementId == "ddlIdentityType")
                {
                    SelectIdentificationType();
                }
                if (elementId == "identityState")
                {
                    SelectIdentificationInformationState();
                }

                if (elementId == "identityNumber")
                {
                    FillIdentificationNumber();
                }

                if (elementId == "idExpirationDate")
                {
                    FillIdentificationExpirationDate();
                }

                if (elementId == "socialSecurity")
                {
                    Random r = new Random();
                    int genRand = r.Next(1, 999999999);
                    while (genRand.ToString().Length < 9)
                    {
                        genRand = r.Next(1, 999999999);
                    }

                    WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.SocialSecurityNumber);
                    var socialSecurityNumber =
                        Driver.FindElement(By.XPath(Elements.ApplyLoan.IdentifyInformation.SocialSecurityNumber));
                    socialSecurityNumber.Clear();
                    socialSecurityNumber.SendKeys(IdentificationInformation.SocialSecurityInvalid);
                    Driver.FindElement(By.TagName("body")).Click();
                    ExplicitWait(3);

                    var flag = socialSecurityNumber
                        .FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.SocialSecurityInput)).Displayed;

                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info,
                        "Validate that Invalid 'Bank AccountLink' is not submitted.");
                    Assert.IsTrue(flag, ApplyLoanErrorString.BankAccountInvalid);
                    socialSecurityNumber.Clear();
                    socialSecurityNumber.SendKeys(genRand.ToString());
                }

                if (elementId == "streetAddress")
                {
                    FillContactInformationStreetAddress();
                }

                if (elementId == "residenceLengthYear")
                {
                    SelectResidenceLengthYear();
                }

                if (elementId == "residenceLengthMonth")
                {
                    SelectResidenceLengthMonth();
                }

                if (elementId == "phoneNumber")
                {
                    FillContactInformationPhoneNumber(user);
                }

                if (elementId == "verifyPhoneCode")
                {
                    ClickContactInformationSendPhoneVerification();
                }

                if (elementId == "optInSMS")
                {
                    ClickContactInformationOptInSms();
                    ClickContactInformationOptInSmsMarketing();
                }

                if (elementId == "createPassword")
                {

                    WaitForElement(5, Elements.ApplyLoan.SecurityInformation.createPassword);
                    var createPassword =
                        Driver.FindElement(By.XPath(Elements.ApplyLoan.SecurityInformation.createPassword));
                    createPassword.Clear();
                    createPassword.SendKeys(SecurityInformation.InvalidPassword);
                    createPassword.SendKeys(Keys.Tab);
                    //Driver.FindElement(By.TagName("body")).Click();
                    ExplicitWait(3);

                    bool flag = createPassword.FindElement(By.XPath(Elements.ApplyLoan.ErrorPath.CreatePasswordInput)).Displayed;

                    ExplicitWait(3);
                    Reporter.LogTestStepForBugLogger(Status.Info, "Validate that Invalid 'Password' is not submitted.");
                    Assert.IsTrue(flag, ApplyLoanErrorString.PasswordInvalid);
                    createPassword.Clear();
                    createPassword.SendKeys(Elements.DefaultPassword);
                }


                if (elementId == "agreeTerms")
                {
                    ClickSubmitInformationAgreeConsent();
                }

                if (elementId == "agreeConsent")
                {
                    ClickSubmitInformationAgreeTerms();
                }
            }
            Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.DirectDepositYes)).Click();
            Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.AccountTypeChecking)).Click();
            Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.HomeRent)).Click();
            Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.EmailFuturePaymentsYes)).Click();
            Driver.FindElement(By.XPath(Elements.ApplyLoan.Button.ApplySubmit)).Click();
            ExplicitWait(3);
            //TC Pass or Fail
            AssertIsLoanApplySubmitted();

        }



        public bool IsApplyLoanPageCongratsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ApplyLoan.SussessfulSubmitApplyLoan.congratulation)).Displayed;
                
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Apply Loan page loaded successfully");
                Logger.Trace($"Apply Loan page is loaded=>{isVisible}");
                return isVisible;
            }
        }

        
        public void AssertIsLoanApplySubmitted()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.ApplyLoan.SussessfulSubmitApplyLoan.congratulation);
            var congratulation =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.SussessfulSubmitApplyLoan.congratulation));

           // var flag = (congratulation.Text == LoanAppliedSuccessful.Congratulation) ? true : false;

            Reporter.LogTestStepForBugLogger(Status.Info,
                "Validate 'RefinanceLoanDetails' page loaded after successfully loaded.");
            Logger.Trace("Application for loan successfully loaded.");
            Assert.IsTrue(IsApplyLoanPageCongratsVisible, ErrorStrings.SuccessfullyAppliedLoan);
        }

        public void AssertIsPayDayFieldRenamed()
        {

            WaitForElement(10, Elements.ApplyLoan.IncomeInformation.lastPayDay);
            var lastPayDay = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.lastPayDay));

            var flag = (lastPayDay.Text == ApplyLoanIncomeInformation.LastPayDayLabel) ? true : false;

            Reporter.LogTestStepForBugLogger(Status.Info, "Validate 'Last Payday' loaded successfully.");
            Logger.Trace("Last Payday successfully loaded.");
            Assert.IsTrue(flag, ErrorStrings.LoanLastPaydayNotLoaded);
        }


        public void SelectNameOfCompany()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.SelectNameOfCompany);
            var selectCompanyName =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.SelectNameOfCompany));
            selectCompanyName.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the company name in the list");
        }

        public void FillPersonalInformationZipcode_ValidB()
        {
            WaitForElement(5, Elements.ApplyLoan.PersonalInformation.ZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.ApplyLoan.PersonalInformation.ZipCode));
            enterZipCode.SendKeys(PersonalInformation.ZipCode);
        }


        public void LoanTypeOptionHelper()
        {
            var user = Users.GenNewRandomMember(CustomerType.Random);
            var applyLoanPage = new ApplyLoan(Driver, user);

           if(IsElementPresent(By.XPath(Elements.ApplyLoan.PersonalInformation.LoanTypeOption)))
            {


                
           }

        }

        public void SelectYesYouHaveDirectDeposit()
        {
            WaitForElement(5, Elements.ApplyLoan.IncomeInformation.DirectDeposit);
            var yesDirectDeposit = Driver.FindElement(By.XPath(Elements.ApplyLoan.IncomeInformation.DirectDeposit));
            yesDirectDeposit.Click();
            Reporter.LogTestStepForBugLogger(Status.Info, "Click yes for direct deposit");
        }

        public void SelectCheckingAccount()
        {
          WaitForElement(5, Elements.ApplyLoan.BankInformation.CheckingAccount);
          var checkingAccount = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.CheckingAccount));
          checkingAccount.Click();
          Reporter.LogTestStepForBugLogger(Status.Info, "Click checking account ");
        }

        public void DoYouOwnOrRent()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.Rent);
            var rent = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.Rent));
            rent.Click();
            Reporter.LogTestStepForBugLogger(Status.Info, "Click Rent ");
        }

        public void ClickYesAboutFuturePaymentsByEmail()
        {
            WaitForElement(5, Elements.ApplyLoan.ContactInformation.NoticeAboutFuturePayment);
            var futurePayments =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.NoticeAboutFuturePayment));
            futurePayments.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to future payments by email");


        }

        public void ClickWithdrawMyApplication()
        {
            WaitForElement(5, Elements.ApplyLoan.SubmitInformation.Withdraw);
            var withdrawApplication =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.SubmitInformation.Withdraw));
            withdrawApplication.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click withdraw application");
        }

        public void ClickConfirmWithdrawApplicationYes()
        {
            WaitForElement(5, Elements.ApplyLoan.SubmitInformation.ConfirmYes);
            var confirmWithdrawApplication =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.SubmitInformation.ConfirmYes));
            confirmWithdrawApplication.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Confirm withdraw application");
        }
    }
}