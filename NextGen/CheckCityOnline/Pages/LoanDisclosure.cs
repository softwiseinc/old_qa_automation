﻿using System;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NLog;
using NLog.Fluent;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Tests
{
    internal class LoanDisclosure : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public LoanDisclosure(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanDisclosure;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PersonalLoanDisclosure;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanDisclosure;
        }

        public bool IsLoanDisclosurePageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("Loan_Disclosure.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Disclosure page loaded successfully");
                Logger.Trace($"Loan Disclosure page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsPendingApprovalPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("ThankYou=True&TransactionSubmitted");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Loan Pending Approval page loaded successfully");
                Logger.Trace($"Loan Disclosure page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertLoanDisclosurePageLoaded()
        {
            try
            {
                ExplicitWait(3);
                Assert.IsTrue(IsLoanDisclosurePageLoaded, ErrorStrings.LoanDisclosurePageNotLoaded);
            }
            catch (Exception)
            {
                ExplicitWait(3);
                Assert.IsTrue(IsLoanDisclosurePageLoaded, ErrorStrings.LoanDisclosurePageNotLoaded);
            }
        }



        public void EnterNameContractSignature()
        {
            WaitForElement(10, Elements.LoanDisclosure.Input.NameContractSignature);
            var contractSignatureField =
                Driver.FindElement(By.XPath(Elements.LoanDisclosure.Input.NameContractSignature));
            contractSignatureField.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer name for the contract. ");
        }

        public void EnterNameTermsAndDisclosures()
        {
            var disclosureSignatureField =
                Driver.FindElement(By.XPath(Elements.LoanDisclosure.Input.NameTermsAndDisclosures));
            disclosureSignatureField.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer name for the loan disclosure agreement. ");
        }
        public void EnterNameTermsAndDisclosuresUT()
        {
            var disclosureSignatureField =
                Driver.FindElement(By.XPath(Elements.LoanDisclosure.Input.NameTermsAndDisclosures));
            disclosureSignatureField.SendKeys("Signature");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer name for the loan disclosure agreement. ");
        }

        public void ClickUtahDeferredLoanAgreement()
        {
            var agreeButton = Driver.FindElement(By.XPath(Elements.LoanDisclosure.Button.ClickUtahDeferredLoanAgreement));
            agreeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on I Agree LinkButton. ");
        }



        public void ClickIAgreeWyomingAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.IAgree);
            var buttonIAgree = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.IAgree));
            buttonIAgree.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User Clicks 'I Agree' Button for the Wyoming Agreement");
        }

        public void EnterSignatureAgreementTwo(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo));
            enterSignature.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature agreement number two.");

        }
        public void EnterSignatureAgreementThree(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementThree);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementThree));
            enterSignature.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature agreement number three.");
        }

        public void EnterSignatureAgreementFour(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementFour);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementFour));
            enterSignature.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature agreement number four.");
        }

        public void EnterSignatureAgreementFive(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementFive);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementFive));
            enterSignature.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature agreement number five.");
        }
        public void ClickSignLoanAgreementOne()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementOne);
            var loanDisclosureButton = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementOne));
            loanDisclosureButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number one.");
        }

        public void ClickSignLoanAgreementTwo()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementTwo);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementTwo));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number two.");
        }

        public void ClickSignLoanAgreementThree()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementThree);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementThree));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number three.");
        }

        public void ClickSignLoanAgreementFour()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementFour);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementFour));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number four.");
        }

        public void ClickSignLoanAgreementFive()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementFive);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementFive));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number five.");
        }

        
        public bool IsInlineMessageForAudioRecLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.LoanDisclosure.Message.InlineAudioMessage))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate inline message for audio recording of your loan terms is visible. ");
                Logger.Trace($"Inline message for audio recording of your loan terms. =>{ isVisible}");
                return isVisible;
            }
        }

        public void AssertInlineMessageForAudioRecLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsInlineMessageForAudioRecLoaded, ErrorStrings.InlineMessageForAudioRecNotLoaded);

        }

        public void SignTexasCreditServiceAgreement(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementThree);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementThree));
            signTexasCredit.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign loan third agreement ");
        }

        public void SignTexasLoanAgreement(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementFour);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementFour));
            signTexasCredit.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign loan fourth agreement ");
        }

        public void SignTexasACHAuthorization(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementFive);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementFive));
            signTexasCredit.SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign fifth loan agreement ");
        }

        public void ClickSignTexasCreditServiceAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementThree);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementThree));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number three.");
        }

        public void ClickSignTexasLoanAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementFour);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementFour));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number four.");
        }

        public void ClickTexasACHAuthorization()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementFive);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementFive));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks signature agreement button number five.");
        }

        public void ClickIAgreeButton()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.IAgree);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.IAgree));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button.");
        }

        public void ClickSignUtahDeferredLoanAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignUtahDeferredLoanAgreement);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignUtahDeferredLoanAgreement));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign Utah Deferred Loan Agreement' button.");
        }

        public void ClickSignKansasConsumerLoanAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignKansasConsumerLoanAgreement);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignKansasConsumerLoanAgreement));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign Kansas Consumer Loan Agreement' button.");
        }

        public void ClickSignKansasNoticeToConsumerAgreement()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignKansasNoticeToConsumer);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignKansasNoticeToConsumer));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign Kansas Notice To Consumer' button.");
        }
        public void ClickIAgreeButtonOne()
        {
            WaitForElement(15, Elements.Loans.LoanDisclosure.Button.ButtonOne);
            
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.ButtonOne));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button #1.");
        }

        public void ClickIAgreeButtonTwo()
        {
            WaitForElement(15, Elements.Loans.LoanDisclosure.Button.ButtonTwo);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.ButtonTwo)); achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button #2.");
        }

        public void ClickIAgreeButtonThree()
        {
            WaitForElement(15, Elements.Loans.LoanDisclosure.Button.ButtonThree);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.ButtonThree));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button #3.");
        }

        public void ClickIAgreeButtonFour()
        {
            WaitForElement(15, Elements.Loans.LoanDisclosure.Button.ButtonFour);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.ButtonFour));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button #4.");
        }

        public void ClickIAgreeButtonFive()
        {
            WaitForElement(15, Elements.Loans.LoanDisclosure.Button.ButtonFive);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.ButtonFive));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree' button #5.");
        }

        public void SignAlaskaAgreement(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignYourName);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignYourName));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign loan with customer first and last name ");
        }

        public void SignKansasConsumerLoanAgreement(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign loan with customer first and last name ");
        }
        public void SignAlaskaAdvanceDisclosure(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign Alaska Advance Disclosure Agreement with customer first and last name ");
        }

        public void SignAlaskaDeferredDepositAdvanceAgreement(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign Alaska Deferred Deposit Advance Agreement with customer first and last name ");
        }

        public void SignKansasNoticeToConsumer(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign 'Sign Kansas Notice To Consumer' with customer first and last name ");
        }

        public void SignPersonalLoanAgreement(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign loan Agreement with customer first and last name ");
        }

        public void ClickSignWisconsinLoanAgreement()
        {
            WaitForElement(7, Elements.Loans.LoanDisclosure.Button.SignWisconsinLoanAgreement);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignWisconsinLoanAgreement));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign Wisconsin Consumer Loan Agreement' button.");
        }

        public void SignAchAuthorization(Users.CurrentUser customer)
        {
            WaitForElement(7, Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign 'Sign Wisconsin ACH Authorization with customer first and last name ");
        }


        public void ClickAchAuthorization()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignAchAuthorization);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignAchAuthorization));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign ACH Authorization' button.");
        }
        public void ClickWisconsinAchAuthorization()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignWisconsinAchAuthorization);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignWisconsinAchAuthorization));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'Sign ACH Authorization' button.");
        }

        public void SignNevadaPaydayAgreement(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.NevadaSignature);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.NevadaSignature));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign 'Sign Nevada Loan Agreement with customer first and last name ");
        }

        public void ClickNevadaIAgree()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.NevadaIAgreeButton);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.NevadaIAgreeButton));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree, Continue...' button.");
        }

        public void SignLoanDisclosure(Users.CurrentUser customer)
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignNevadaLoanDisclosure);
            var signTexasCredit =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignNevadaLoanDisclosure));
            signTexasCredit.SendKeys(customer.FirstName + " " + customer.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Sign 'Sign Nevada Loan Disclosure Agreement with customer first and last name ");
        }
        public void ClickNevadaDisclosureIAgree()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.NevadaDisclosureIAgreeButton);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.NevadaDisclosureIAgreeButton));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks 'I Agree, Continue...' button.");
        }

        public void AssertLoanPendingApproval()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsPendingApprovalPageLoaded, ErrorStrings.PendingApprovalPageNotLoaded);

        }


    }

}