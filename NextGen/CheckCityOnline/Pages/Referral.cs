﻿using AventStack.ExtentReports;
using NLog;
using OpenQA.Selenium;
using static CheckCityOnline.Elements;
using static CheckCityOnline.Elements.ReferralPage;
using Assert = NUnit.Framework.Assert;

namespace CheckCityOnline.Pages
{
    internal class ReferralPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ReferralPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Referral;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Referral;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Referral;
        }

        public bool IsCopySuccessMessageVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ReferralPage.Assert.CopyLinkSuccess)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the referral link success message is visible after the 'Copy and Paste' button is pushed.");
                Logger.Trace($"Link copy success alert is visable=>{isVisible}");
                return isVisible;
            }
        }

        public bool IsTermsConditionsLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(Url.Partials.OnlineReferralTermsAndConditions);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the online referral terms and conditions pdf is loaded");
                Logger.Trace($"online-referral-terms-and-conditions.pdf=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsSendEmailReferralsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ReferralPage.Button.SendEmailReferrals)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the Send Email Referrals button is visible.");
                Logger.Trace($"Send Email Referrals button is visible =>{isVisible}");
                return isVisible;
            }
        }
        public bool IsReferAFriendVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ReferralPage.ScreenText.ReferAFriend)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the Refer A Friend screen text is visible.");
                Logger.Trace($"Refer a Friend text is visible =>{isVisible}");
                return isVisible;
            }
        }

        public void CopyTheReferralLink(Users.CurrentUser user)
        {
            WaitForElement(5, Button.CopyReferralLink);
            Driver.FindElement(By.XPath(Button.CopyReferralLink)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Click the 'Copy and Paste:<link>' in the 'Share your Referral Coupon' section of the page.");
            Assert.IsTrue(IsCopySuccessMessageVisible, ErrorStrings.CopySuccessMessageNotVisible);
        }

        public void ClickReferAFriend()
        {
            //   //*[@id="userMenu"]/div/a[3]/text()
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButtonOptions.ReferFriend);
            Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.ReferFriend)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Refer A Friend' item on the side menu");
        }

        
        public void AssertReferAFriendIsVisible()
        {
            WaitForElement(5, Elements.ReferralPage.ScreenText.ReferAFriend);
            Assert.IsTrue(IsReferAFriendVisible, ErrorStrings.ReferralPageIsNotVisible);
        }

        public void ReferralViaEmail()
        {
            WaitForElement(5, Elements.ReferralPage.Button.ReferralViaEmail);
            var referralViaEmailBtn = Driver.FindElement(By.XPath(Elements.ReferralPage.Button.ReferralViaEmail));
            referralViaEmailBtn.Click();
            AssertSendEmailReferralButtonIsVisible();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Referral Via Email LinkButton. ");
            
        }
        
        public void AssertSendEmailReferralButtonIsVisible()
        {
            WaitForElement(5, Elements.ReferralPage.Button.SendEmailReferrals);
            Assert.IsTrue(IsSendEmailReferralsVisible, ErrorStrings.SendEmailReferralButtonIsNotVisible);
        }

        public void EnterReferralEmail()
        {
            WaitForElement(5, Elements.ReferralPage.Input.EmailAddress);
            var enterReferralEmail = Driver.FindElement(By.XPath(Elements.ReferralPage.Input.EmailAddress));
            enterReferralEmail.SendKeys(Users.User.ReferralEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter email address. ");
        }

        public void ClickSendEmailReferrals()
        {
           WaitForElement(5, Elements.ReferralPage.Button.SendEmailReferrals);
           var emailsReferralsBtn = Driver.FindElement(By.XPath(Elements.ReferralPage.Button.SendEmailReferrals));
           emailsReferralsBtn.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Send Email Referrals button. ");

        }


        public void ClickCheckCityStoreCustomersLink()
        {
            WaitForElement(5, Link.ClickHereStoreReferral);
            Driver.FindElement(By.XPath(Link.ClickHereStoreReferral)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Store Referral' Link");
        }

        public void ClickTermsAndConditionsLink()
        {
            WaitForElement(5, Elements.MemberPage.ReferAFriend.TermsAndConditions);
            Driver.FindElement(By.XPath(Elements.MemberPage.ReferAFriend.TermsAndConditions)).Click();
            Assert.IsTrue(IsTermsConditionsLoaded, ErrorStrings.ReferralTermsPageNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Terms and Conditions' link.");
        }
    }
}