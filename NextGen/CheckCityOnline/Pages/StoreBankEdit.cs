﻿using System;
using System.Net.NetworkInformation;
using AutomationResources;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Configuration;
using CheckCityOnline.Tests;
using Microsoft.SqlServer.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using static AutomationResources.Helper.Page;


namespace CheckCityOnline.Pages
{
    internal class StoreBankEdit : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public StoreBankEdit(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreBankEdit;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreBankEdit;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreBankEdit;
        }

        public bool IsStoreBankEditVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreBankEdit.ScreenText.ConfirmBankAccount))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store bank edit page is visible. ");
                Logger.Trace($"Store bank edit page is visible =>{ isVisible}");
                return isVisible;
            }
        }


        public void AssertStoreBankEdit()
        {
            WaitForElement(20, Elements.StoreBankEdit.ScreenText.ConfirmBankAccount);
            Assert.IsTrue(IsStoreBankEditVisible, ErrorStrings.StoreBankEditPageNotVisible);
        }


        public void ClickLoanPayments()
        {
            WaitForElement(5, Elements.StoreBankEdit.Button.LoanPaymentsYes);
            var clickLoanPayments = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.LoanPaymentsYes));
            clickLoanPayments.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer clicks Yes or No for loan payment question. ");

        }

        public void ClickConfirmBankAccountWithEmailAchHandlers()
        {
            EmailNoticeYesHandler();
            AchPaymentHandler();
            WaitForElement(5, Elements.StoreBankEdit.Button.ConfirmBankAccount);
            var clickConfirmBankAccount =
                Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.ConfirmBankAccount));
            clickConfirmBankAccount.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer clicks Confirm Bank AccountLink button. ");
        }

        private void AchPaymentHandler()
        {
            try
            {
                Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.ACHLoanPaymentYes)).Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "ACH Yes selected");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "ACH not found");
            }
        }

        private void ClickConfirmBankAccountHandler()
        {
            var Config = TestConfigManager.Config;
            if(Config.RunBrowserStack)
                return;

            var user = new Users();
            var routingNumber = user.GetRandomRoutingNumber();
            var accountNumber = user.GetAccountNumber();
            ExplicitWait(7);
            var routingNumberInput = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber));
            var routingNumberValue = routingNumberInput.GetProperty("value");
            if (routingNumberValue.Length <= 0)
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.RoutingNumber)).SendKeys(routingNumber);

            var accountNumberInputField = Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount));
            var accountNumberValue = accountNumberInputField.GetProperty("value");
            if (accountNumberValue.Length <= 0)
            {
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.BankAccount)).SendKeys(accountNumber);
                Driver.FindElement(By.XPath(Elements.ApplyLoan.BankInformation.ReBankAccount)).SendKeys(accountNumber);
            }
            SelectYear();
        }
        public void SelectYear()
        {
            WaitForElement(5, Elements.StoreBankEdit.DropDown.Button.Years);
            var yearsInputField = Driver.FindElement(By.XPath(Elements.StoreBankEdit.DropDown.Button.Years));
            var yearsInputValue = yearsInputField.GetProperty("value");
            if (yearsInputValue.Length <= 0)
            {
                var howManyYears = Driver.FindElement((By.XPath(Elements.StoreBankEdit.DropDown.Button.Years)));
                var selectElement = new SelectElement(howManyYears);
                selectElement.SelectByText("1");
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Number of years bank account has been open will be selected");
            }
        }


        public void NavigateToStoreBankEditPage(Users.CurrentUser user)
        {
            var memberLoginPage = new MemberLoginPage(Driver, user);
            var memberPage = new MemberPage(Driver, user);
            var storeBankEdit = new StoreBankEdit(Driver, user);

            memberLoginPage.LogInUser(user);
            memberPage.ClickRequestPersonalLoan();
            storeBankEdit.AssertStoreBankEdit();
        }
        public void ClickNoBankAccountHandler()
        {
            WaitForElement(5, Elements.StoreBankEdit.Button.LoanPaymentsNo);
            var button = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.LoanPaymentsNo));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User will click on 'No'. ");
            ExplicitWait(1);
            ClosePopupConfirmNoBankAccount();
        }
        public void ClosePopupConfirmNoBankAccount()
        {
            ExplicitWait(5);
            if (ElementVisibilityState(Driver, Elements.StoreBankEdit.Popup.Button.Close))
            {
                WaitForElement(5, Elements.StoreBankEdit.Popup.Button.Close);
                var button = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Popup.Button.Close));
                button.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Confirm No Bank Account 'No' answer. ");
            }
        }
        
        public void ClickContinueWithoutEnrolling()
        {
            WaitForElement(5, Elements.StoreBankEdit.DropDown.Button.ClickContinueWithOutEnrollingInAutoPay);
            var button =
                Driver.FindElement(By.XPath(Elements.StoreBankEdit.DropDown.Button
                    .ClickContinueWithOutEnrollingInAutoPay));
            button.Click();
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User will Continue by not enrolling in auto pay.");

        }

        public void EnterRoutingNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.StoreBankEdit.Input.RoutingNumber);
            var routingInputField = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.RoutingNumber));
            var routingInputValue = routingInputField.GetProperty("value");
            if (routingInputValue.Length <= 0)
            {
                var enterRouting = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.RoutingNumber));
                if (user.RoutingNumber == null)
                    user.RoutingNumber = CheckCityOnline.Users.GenNewRandomMember(CustomerType.Random).RoutingNumber;
                enterRouting.SendKeys(user.RoutingNumber);
                ExplicitWait(1);
            }
            
        }

        public void EnterAccountNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.StoreBankEdit.Input.BankAccountNumber);
            var bankAccountInputField = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.BankAccountNumber));
            var bankAccountInputValue = bankAccountInputField.GetProperty("value");
            if (bankAccountInputValue.Length <= 0)
            {
                var enterBankAccountNumber = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.BankAccountNumber));
                if (user.BankAccountNumber == null)
                    user.BankAccountNumber =
                        CheckCityOnline.Users.GenNewRandomMember(CustomerType.Random).BankAccountNumber;
                enterBankAccountNumber.SendKeys(user.BankAccountNumber);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Bank account number will be entered");
            }
        }

        public void ReEnterAccountNumber(Users.CurrentUser user)
        {
           WaitForElement(5, Elements.StoreBankEdit.Input.ReEnterBankAccountNumber);
           var bankAccountInputField = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.ReEnterBankAccountNumber));
           var bankAccountInputValue = bankAccountInputField.GetProperty("value");
           if (bankAccountInputValue.Length <= 0)
           {
               var reEnterBankAccountNumber = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Input.ReEnterBankAccountNumber));
               reEnterBankAccountNumber.SendKeys(user.BankAccountNumber);
               Reporter.LogPassingTestStepToBugLogger(Status.Info, "Bank account number will be entered again");
           }
        }

        public void SelectYear(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.StoreBankEdit.DropDown.Button.Years);
            var yearsInputField = Driver.FindElement(By.XPath(Elements.StoreBankEdit.DropDown.Button.Years));
            var yearsInputValue = yearsInputField.GetProperty("value");
            if (yearsInputValue.Length <= 0)
            {
                var howManyYears = Driver.FindElement((By.XPath(Elements.StoreBankEdit.DropDown.Button.Years)));
                var selectElement = new SelectElement(howManyYears);
                selectElement.SelectByText(user.Years);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Number of years bank account has been open will be selected");
            }
        }

        public void ClickYesToUseThisBankAccount()
        {
           WaitForElement(3, Elements.StoreBankEdit.Button.LoanPaymentsYes);
           var loanPayments = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.LoanPaymentsYes));
           loanPayments.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to payments coming out of this bank account");
        }

        public bool TryAddBankingInformation(Users.CurrentUser user)
        {
            bool isBankInfoAdded = false;
            try
            {
                //storeBankEditPage
                AssertStoreBankEdit();
                SelectYear(user);
                EnterRoutingNumber(user);
                ExplicitWait(2);
                EnterAccountNumber(user);
                ExplicitWait(2);
                ReEnterAccountNumber(user);
                ClickLoanPayments();
                ClickConfirmBankAccountWithEmailAchHandlers();
                Reporter.LogTestStepForBugLogger(Status.Info, "Bank info needed");
                isBankInfoAdded = true;
            }
            catch (Exception)
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "Bank info not needed");
            }

            return isBankInfoAdded;
        }

        public void ClickYesFuturePaymentsByEmail()
        {
            IWebElement futurePaymentByEmail = null;
            try
            {
                WaitForElement(5,
                    Elements.StoreBankEdit.ScreenText.WouldYouLikeToReceiveNoticeAboutFuturePaymentsByEmail);
                futurePaymentByEmail =
                    Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.ReceiveNoticeAboutFuturePaymentsByEmail));
                futurePaymentByEmail.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yes to Future payments by email");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "NOT FOUND: Click Yes to Future payments by email");
            }
        }

        public void ClickNoSendMeNoticesByMail()
        {
            WaitForElement(5, Elements.StoreBankEdit.AreYouSure.Buttons.NoSendNoticesByMail);
            var no = Driver.FindElement(By.XPath(Elements.StoreBankEdit.AreYouSure.Buttons.NoSendNoticesByMail));
            no.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click No, send me notices by U.S. Mail");
        }

        public void EmailNoticeYesHandler()
        {
            if (ElementVisibilityState(Driver, Elements.StoreBankEdit.Radio.OptIn))
            {
                var optIn = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Radio.OptIn));
                optIn.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer clicks 'Yes' for email notification.");
            }
            else
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Email notification question was not found");
            }
            
        }

        public void EmailNoticeNoHandler()
        {
            if (ElementVisibilityState(Driver, Elements.StoreBankEdit.Radio.OptOut))
            {
                var optIn = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Radio.OptOut));
                optIn.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer clicks 'No' for email notification.");
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Email notification question was not found");
            ExplicitWait(1);
            ClosePopupConfirmNoPaymentEmail();
        }
        public void ConfirmPayFrequencyHandler()
        {
            if (ElementVisibilityState(Driver, Elements.StoreBankEdit.DropDown.Button.SelectWeeklyDate))
            {
                var selectElement = new SelectElement(Driver.FindElement(By.XPath(Elements.StoreBankEdit.DropDown.Button.SelectWeeklyDate)));
                selectElement.SelectByIndex(2);
                Driver.FindElement(By.XPath("//*[@class='btn-submit']")).Click();
            }
        }

        public void ClickConfirmBankAccount()
        {
            WaitForElement(5, Elements.StoreBankEdit.Button.ConfirmBankAccount);
            var clickConfirmBankAccount =
                Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.ConfirmBankAccount));
            clickConfirmBankAccount.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer clicks Confirm Bank AccountLink button. ");
        }

        public void ClosePopupConfirmNoPaymentEmail()
        {
            if (ElementVisibilityState(Driver, Elements.StoreBankEdit.Popup.Button.LeftSideOption))
            {
                WaitForElement(5, Elements.StoreBankEdit.Popup.Button.LeftSideOption);
                var button = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Popup.Button.LeftSideOption));
                button.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Confirm 'No' Email Notification answer");
            }
        }

        public void ClickCancelButton()
        {
            WaitForElement(5, Elements.StoreBankEdit.Button.Cancel);
            var button = Driver.FindElement(By.XPath(Elements.StoreBankEdit.Button.Cancel));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Cancel Button");
        }
    }
}