﻿using System.Collections.Generic;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class EfpLoanDocumentsPage : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public EfpLoanDocumentsPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EfpLoanDocumentPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EfpLoanDocumentPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.EfpLoanDocumentPage;
        }

        public bool IsEfpLoanDocumentsPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Efp loan document page loaded successfully");
                Logger.Trace($"Efp loan document Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertEfpLoanDocumentsPageLoaded()
        {
            Assert.IsTrue(IsEfpLoanDocumentsPageLoaded, ErrorStrings.Efp_LoanDocumentsPageDidNotLoad);
        }


        public void ClickCustomerState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.Dropdown.CustomerState);
            var customerStateInputField = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.Dropdown.CustomerState));
            customerStateInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Customer State input field ");
        }

        public void SelectUtahAsState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.Dropdown.DropdownStateOptions.StateUtah);
           
            var selectUtahAsTheState = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.Dropdown.DropdownStateOptions.StateUtah));
            selectUtahAsTheState.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select on Utah ");
        }

        public void ClickCustomerId()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.InputFields.CustomerID);
            var customerID =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.InputFields.CustomerID));
            customerID.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Customer ID input field ");
        }

        public void EnterCustomerID(Users.CurrentUser rdpUserApproval)
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpLoanApprovalPage.EfpLoanApprovalTabs.LoanDocumentOptions.InputFields.CustomerID);
            var customerIDInputField = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.LoanDocumentOptions.InputFields.CustomerID));
            customerIDInputField.SendKeys(Users.User.CustomerID);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer ID ");
        }

        public void ClickLookupCustomer()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.Buttons.LookupCustomer);
            var lookupCustomerButton = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.Buttons.LookupCustomer));
            lookupCustomerButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click lookup Customer button");
        }
        public bool IsDateSignedTextIsVisible
        {
            get
            {
                var isVisible =
                    Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.LoanDocumentPage.AssertItems
                        .DateSigned)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the date signed is visible");
                Logger.Trace($"Date signed is Visible=>{isVisible}");
                return isVisible;
            }
        }

        public void AssertDateSignedIsVisibleLoanDocumentsPage()
        {
            Assert.IsTrue(IsDateSignedTextIsVisible, ErrorStrings.DateSignedOnEFPLoanDocumentsPageIsNotVisible);
        }
    }
}
