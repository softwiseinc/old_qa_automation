﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Pages
{
    internal class MemberLoginPage : BaseApplicationPage
    {
        public MemberLoginPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.MemberLogin;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.MemberLogin;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.MemberLogin;
        }

        public bool IsUrlLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsCustomerIdVisible
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.CustomerId)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate customer ID input field is visible on member login page .");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsCustomerDobVisible
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.CustomerDob)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate customer Dob input field is visible on member login page .");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        
        public bool isSelectStateVisible
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.MemberLoginPage.Dropdown.SelectState)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate Select State dropdown is visible on member login page .");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsPasswordInputVisible
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.Password)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate password input field is visible on member login page .");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool isStoreRegistrationVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains("store-registration");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate store-registration Login page loaded successfully");
                Logger.Trace($"page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsLoginSuccessful
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.MemberLoginPage.TopMenu.UserTab)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate user is logged into the Member page.");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsLoginCaliforniaSuccessful
        {
            get
            {
                var isLoaded = Driver.Url.Contains("UpdateInfo_B.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate user is logged into the Member page.");
                Logger.Trace($"Members page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsInvalidEmail
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberLoginPage.ErrorMessage.InvalidEmail))
                    .Displayed;
                var errorText = Driver.FindElement(By.XPath(Elements.MemberLoginPage.ErrorMessage.InvalidEmail)).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate Invalid email error message is visible => {errorText}");
                Logger.Trace($"Invalid email error message is visiable => {errorText}");
                return isVisible;
            }
        }

        public bool IsEmailRequiredErrorVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberLoginPage.ErrorMessage.EmailRequired))
                    .Displayed;
                var errorText = Driver.FindElement(By.XPath(Elements.MemberLoginPage.ErrorMessage.EmailRequired)).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate email required error message visible => {errorText}");
                Logger.Trace($"Validate email required error message visable => {errorText}");
                return isVisible;
            }
        }

        public bool IsUrlPasswordResetLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("True");
                Reporter.LogTestStepForBugLogger(Status.Info, "Loaded with Password reset message. ");
                Logger.Trace($"Home page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void LogInUser(Users.CurrentUser user)
        {
            CustomerRegistrationHandler(user);
            NavigateToLoginPage();
            EnterUserName(user);
            ClickNextButton();
            EnterPassword(user);
            ClickLogInButton(user);
        }

        public void ClickNextButton()
        {
            WaitForElement(5, Elements.MemberLoginPage.Button.Next);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Button.Next)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Next' button.");
        }
        public void EnterUserNameForReset(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Header.BrandLogo);
            WaitForElement(5, Elements.MemberLoginPage.Input.UserName);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.UserName)).SendKeys(user.UpdatedEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a Username/Email value in the user name input field.");
        }
        public void EnterUserName(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Header.BrandLogo);
            WaitForElement(5, Elements.MemberLoginPage.Input.UserName);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.UserName)).SendKeys(user.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a Username/Email value in the user name input field: " + user.UserName);
        }

        public void NavigateToLoginPage()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            Assert.IsTrue(IsUrlLoaded, ErrorStrings.HomePageNotLoaded);
        }

        public void EnterPassword(Users.CurrentUser user)
        {
            if (!CustomerRegistrationHandler(user))
            {
                WaitForElement(5, Elements.MemberLoginPage.Input.Password);
                Assert.IsTrue(IsPasswordInputVisible, ErrorStrings.PasswordInputNotFound);
                Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.Password)).SendKeys(user.Password);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a password value  in the password input field:" + user.Password);
            }
            
        }

        public void EnterNewPassword(Users.CurrentUser user)
        {
            if (!CustomerRegistrationHandler(user))
            {
                WaitForElement(5, Elements.MemberLoginPage.Input.Password);
                Assert.IsTrue(IsPasswordInputVisible, ErrorStrings.PasswordInputNotFound);
                Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.Password)).SendKeys(user.NewPassword);
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a password value  in the password input field:" + user.NewPassword);
            }

        }
        public void EnterCustomerId(Users.CurrentUser user)
        {
            ExplicitWait(4);
            WaitForElement(5, Elements.MemberLoginPage.Input.CustomerId);
            Assert.IsTrue(IsCustomerIdVisible, ErrorStrings.CustomerIdInputNotFound);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.CustomerId)).SendKeys(user.Id);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a customer Id value in the Customer Id input field.");
        }
        
        public void EnterCustomerBirthday(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberLoginPage.Input.CustomerDob);
            Assert.IsTrue(IsCustomerDobVisible, ErrorStrings.CustomerDobInputNotFound);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.CustomerDob)).SendKeys(user.BirthDate);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a customer Date of Birth value in the Customer Dob input field.");
        }

        public void SelectCustomerState(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberLoginPage.Dropdown.SelectState);
            Assert.IsTrue(isSelectStateVisible, ErrorStrings.SelectStateDropdownIsNotFound);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Dropdown.SelectState)).Click();
            var selectElement = new SelectElement(Driver.FindElement(By.XPath(Elements.MemberLoginPage.Dropdown.SelectState)));
            selectElement.SelectByValue(user.State);
        }

        public void ClickSubmitButton()
        {
            WaitForElement(5, Elements.MemberLoginPage.Button.Submit);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Button.Submit)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Submit' button.");
            ExplicitWait(3);
            Assert.IsTrue(isStoreRegistrationVisible, ErrorStrings.StoreRegistrationPageNotLoaded);
        }
        public void ClickLogInButton(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberLoginPage.Button.Login);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Button.Login)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Login' button.");
            SecurityQuestionHandler(user);
            ExplicitWait(3);
            Assert.IsTrue(IsLoginSuccessful, ErrorStrings.UserNotLoggedIn);
        }

        private void SecurityQuestionHandler(Users.CurrentUser user)
        {
            var isMemberSecurity = false;
            try
            {
                isMemberSecurity = Driver.Url.Contains("MemberSecurity");
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Security Question Page IS present");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Security Question Page NOT present");
            }

            if (isMemberSecurity)
            {
                var memberSecurity = new MemberSecurity(Driver, user);
                memberSecurity.AnswerSecurityQuestions();
            }
        }
        private bool CustomerRegistrationHandler(Users.CurrentUser user)
        {
            var isCustomerRegistration = false;
            try
            {
                isCustomerRegistration = Driver.PageSource.Contains("CUSTOMER REGISTRATION");
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "CUSTOMER REGISTRATION Page IS present");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Security Question Page NOT present");
            }

            if (isCustomerRegistration)
            {
                var storeRegistration = new StoreRegistration(Driver, user);
                var memberLoginPage = new MemberLoginPage(Driver, user);
                EnterCustomerId(user);
                EnterCustomerBirthday(user);
                SelectCustomerState(user);
                ClickSubmitButton();
                storeRegistration.EnterCustomerPassword(user);
                storeRegistration.ClickRegisterButton();
                storeRegistration.ClickToLogin();
                memberLoginPage.EnterUserName(user);
                memberLoginPage.ClickNextButton();
                memberLoginPage.EnterPassword(user);
            }

            return isCustomerRegistration;
        }

        public void ClickForgotYourPasswordTextLink()
        {
            WaitForElement(5, Elements.MemberLoginPage.TextLink.ForgotYourPassword);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.TextLink.ForgotYourPassword)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the 'Forgot Your Password' text link.");
        }

        public void AssertMemberLoginPageLoaded()
        {
            Assert.IsTrue(IsUrlPasswordResetLoaded, ErrorStrings.MemberLoginPageNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate that the member login page loaded after password reset. ");
        }

        public void PasswordResetMessage()
        {
            WaitForElement(5, Elements.MemberLoginPage.Message.PasswordSuccessfullyResetMessage);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Password was reset. ");
        }

        public void EmailAddressIsRequired()
        {
            NavigateToLoginPage();
            ClickNextButton();
            Assert.IsTrue(IsEmailRequiredErrorVisible, ErrorStrings.EmailRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: 'Email is Required' message is visible. ");
        }

        public void InvalidEmailAddress()
        {
            string[] emailAddress = { "word", "@@@@", "no@tld@", "this@example.", "....." };

            foreach (string invalidEmail in emailAddress)
            {

                if (invalidEmail != emailAddress[0])
                    Reporter.LogPassingTestStepToBugLogger(Status.Info,
                        "Reload the members page and repeat this test with another invalid email address.");
                NavigateToLoginPage();
                WaitForElement(5, Elements.MemberLoginPage.Input.UserName);
                Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.UserName)).Clear();
                Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.UserName)).SendKeys(invalidEmail);
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                    $"Enter an invalid email address by using the following characters => '{invalidEmail}'");
                ClickNextButton();
                Assert.IsTrue(IsInvalidEmail, ErrorStrings.InvalidEmailAddress);
            }
        }

        public void UseValidEmailAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberLoginPage.Input.UserName);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.Input.UserName)).Clear();
            EnterUserName(user);
            ClickNextButton();
            EnterPassword(user);
            ClickLogInButton(user);
            Assert.IsTrue(IsLoginSuccessful, ErrorStrings.UserNotLoggedIn);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Login successful after invalid email was used then corrected");
        }

        public void LogInWithNewPassword(Users.CurrentUser user)
        {
            NavigateToLoginPage();
            EnterUserName(user);
            ClickNextButton();
            EnterNewPassword(user);
            ClickLogInButton(user);
        }

        public void LogInPage(Users.CurrentUser user)
        {
            NavigateToLoginPage();
            ClickApplyButton();
        }

        public void ClickApplyButton()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.MemberLoginPage.NavLink.ApplyNow);
            Driver.FindElement(By.XPath(Elements.MemberLoginPage.NavLink.ApplyNow)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the 'Apply Now' button.");
        }

        public bool TryRegisterStoreCustomer(Users.CurrentUser customerPendingAll)
        {
            bool isCustomerRegister = false;
            try
            {
                if (Driver.FindElement(By.XPath(Elements.MemberLoginPage.TextLink.CustomerIDRecipt)).Displayed)
                {
                    EnterCustomerId(customerPendingAll);
                    EnterCustomerBirthday(customerPendingAll);
                    SelectCustomerState(customerPendingAll);
                    Reporter.LogTestStepForBugLogger(Status.Info, "Register Customer needed");
                    isCustomerRegister = true;
                }
            }
            catch (Exception)
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "Register Customer not needed");
            }

            return isCustomerRegister;
        }
    }
}