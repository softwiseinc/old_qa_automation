﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class StoreRegistration : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

       public StoreRegistration(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreProfile;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreProfile;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreProfile;
        }

        public bool IsStoreRegistrationVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains("store-registration");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate store-registration Login page loaded successfully");
                Logger.Trace($"page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public bool IsRegisterButtonVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreRegistration.Button.Register))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Register Button is Visible");
                Logger.Trace("Register button is visible");
                return isVisible;
            }
        }

        public bool IsClickToLoginVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreRegistration.ScreenText.ClickToLogin))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'Click here to login' link is Visible");
                Logger.Trace("'Click here to login' is visible");
                return isVisible;
            }
        }

        public bool IsPasswordInputVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreRegistration.Input.Password))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Password input is Visible");
                Logger.Trace("password input is visible");
                return isVisible;
            }
        }

        public void EnterCustomerPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.StoreRegistration.Input.Password);
            Assert.IsTrue(IsPasswordInputVisible, ErrorStrings.PasswordInputNotFound);
            Driver.FindElement(By.XPath(Elements.StoreRegistration.Input.Password)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a customer Password the password input field.");
        }

        public void ClickRegisterButton()
        {
            WaitForElement(5, Elements.StoreRegistration.Button.Register);
            Assert.IsTrue(IsPasswordInputVisible, ErrorStrings.StoreRegistrationButtonNotFound);
            Driver.FindElement(By.XPath(Elements.StoreRegistration.Button.Register)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the Register Button");
        }

        public void ClickToLogin()
        {
            WaitForElement(5, Elements.StoreRegistration.ScreenText.ClickToLogin);
            Assert.IsTrue(IsClickToLoginVisible, ErrorStrings.ClickToLogin);
            Driver.FindElement(By.XPath(Elements.StoreRegistration.ScreenText.ClickToLogin)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the Register Button");
        }
        //*[@id="ctl00_CustomContent_FSubmitRegistration"]
    }
}