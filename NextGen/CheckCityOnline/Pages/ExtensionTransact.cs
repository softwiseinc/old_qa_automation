﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using static CheckCityOnline.Elements.InStoreReferral;

namespace CheckCityOnline.Pages
{
    internal class ExtensionTransact : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ExtensionTransact(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ExtensionTransact;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ExtensionTransact;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ExtensionTransact;
        }

        public bool IsExtensionTransactLoaded
        {
            get
            {
                var isVisible = Driver.Url.Contains("extension-transact.aspx"); ;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'Extension Transact' page loaded successfully");
                Logger.Trace("Extension Transact' page is loaded. ");
                return isVisible;
            }
        }

        public void AssertExtensionTransactLoaded()
        {

            Assert.IsTrue(IsExtensionTransactLoaded, ErrorStrings.ExtensionTransactPageNotLoaded);
        }


        public void ClickIAgreeButton()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ExtensionTransact.Buttons.IAgree);
            var iAgreeButton = Driver.FindElement(By.XPath(Elements.ExtensionTransact.Buttons.IAgree));
            iAgreeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click I Agree");
        }
    }
}
