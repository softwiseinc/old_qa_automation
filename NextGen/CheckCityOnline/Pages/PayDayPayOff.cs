﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using static CheckCityOnline.Elements.PayDayLoan.PayDayPayOff;

namespace CheckCityOnline.Pages
{
    internal class PayDayPayOff : BaseApplicationPage
    {
        public PayDayPayOff(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PayDayPayOff;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PayDayPayOff;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PayDayPayOff;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsPayDayPayOffVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(ScreenText.PayOff))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the PayDay PayOff page loaded successfully");
                Logger.Trace($"PayDay PayOff page is loaded. ");
                return isVisible;
            }
        }

        public void AssertPayDayPayOffIsVisible()
        {
            WaitForElement(10, ScreenText.PayOff);
            Assert.IsTrue(IsPayDayPayOffVisible, ErrorStrings.PayDayPayOffPageNotLoaded);
        }

        public void SelectPaymentInformation()
        {
            WaitForElement(seconds: 10, elementString: PaymentInformation.PaymentFromChecking);
            var bankInfo = Driver.FindElement(By.XPath(xpathToFind: Elements.PayDayLoan.PayDayPayOff.PaymentInformation.PaymentFromChecking));
            var selectElement = new SelectElement(element: bankInfo);
            selectElement.SelectByText(text: Users.User.CascadesBank);
            Reporter.LogPassingTestStepToBugLogger(info: Status.Info, message: "The correct checking account is selected.  ");

        }

        public void EnterWithDrawalDate()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.WithdrawalDate.EnterDate);
            var enterTomorrowsDate =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.WithdrawalDate.EnterDate));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            enterTomorrowsDate.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "method clears date format. ");
            enterTomorrowsDate.SendKeys(strDateOnly);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Tomorrows date is entered in the field. ");
        }

        public void ClickNext()
        {
            WaitForElement(5, Elements.PayDayLoan.PayDayPayOff.Button.Next);
            var nextButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.Button.Next));
            nextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks next ");
        }
    }
}
