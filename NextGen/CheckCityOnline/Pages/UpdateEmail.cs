﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class UpdateEmail : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public UpdateEmail(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UpdateEmail;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UpdateEmail;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UpdateEmail;
        }
        public bool IsUpdateEmailVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateEmail.ScreenText.ChangeEmail))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the change email screen text is visible. ");
                Logger.Trace($"Change Email text is visible. ");
                return isVisible;
            }
        }

        public bool IsEmailUpdateConfirmationVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateEmail.ScreenText.ChangeEmail))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the change email screen text is visible. ");
                Logger.Trace($"Change Email test is visible. ");
                return isVisible;
            }
        }
        public void AssertUpdateEmailIsVisible()
        {
            WaitForElement(5, Elements.UpdateEmail.ScreenText.ChangeEmail);
            Assert.IsTrue(IsUpdateEmailVisible, ErrorStrings.ChangeEmailIsNotVisible);
            
        }

        public void CurrentEmailAddress()
        {
            WaitForElement(5, Elements.UpdateEmail.Input.CurrentEmailAddress);
            var enterEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.CurrentEmailAddress));
            enterEmail.SendKeys(Users.User.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter current email address:" + Users.User.UserName);
        }

        public void EnterNewEmailAddress()
        {
            WaitForElement(5,Elements.UpdateEmail.Input.NewEmailAddress);
            var enterNewEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.NewEmailAddress));
            enterNewEmail.SendKeys(Users.User.UpdatedEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter updated email address");
        }

        //public void AddNewEmailAddress()
        //{
        //    WaitForElement(5, Elements.UpdateEmail.Input.UpdatedEmailAddress);
        //    var enterNewEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.UpdatedEmailAddress));
        //    enterNewEmail.SendKeys("Abraham-LVYMK000@msgnext.com");
        //    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter updated email address");
        //}

        public void ConfirmNewEmailAddress()
        {
            WaitForElement(5, Elements.UpdateEmail.Input.ConfirmEmailAddress);
            var confirmEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.ConfirmEmailAddress));
            confirmEmail.SendKeys(Users.User.UpdatedEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new email address again");
        }

        public void ClickUpdateEmailAddress()
        {
            WaitForElement(5, Elements.UpdateEmail.Button.UpdateEmailAddress);
            var emailBtn = Driver.FindElement(By.XPath(Elements.UpdateEmail.Button.UpdateEmailAddress));
            emailBtn.Click();
            AssertEmailAddressUpdateConfirmationIsVisible();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on 'Update Email Address' button ");
        }

        public void AssertEmailAddressUpdateConfirmationIsVisible()
        {
            WaitForElement(5,Elements.UpdateEmail.ScreenText.EmailUpdateConfirmation);
            Assert.IsTrue(IsEmailUpdateConfirmationVisible, ErrorStrings.EmailUpdateConfirmationIsNotVisible);
        }


        public void ResetToPreviousEmailAddress(Users.CurrentUser user)
        {
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** BEGIN Email CLEANUP - Changing Email back to previous Email ***");
            NavigateToUpdateEmailForReset(user);
            EnterCurrentEmailAddress(user);
            EnterPreviousEmailAddress(user);
            EnterConfirmPreviousEmailAddress(user);
            ClickUpdateEmailAddress();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** END Email CLEANUP - Changed Email back to previous Email ***");
        }

        public void ResetToPreviousUserEmailAddress(Users.CurrentUser user)
        {
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** BEGIN Email CLEANUP - Changing Email back to previous Email ***");
            NavigateToUpdateEmailForReset(user);
            EnterCurrentEmailAddress(user);
            EnterPreviousEmailAddress(user);
            EnterConfirmPreviousEmailAddress(user);
            ClickUpdateEmailAddress();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** END Email CLEANUP - Changed Email back to previous Email ***");
        }


        private void EnterCurrentEmailAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.UpdateEmail.Input.CurrentEmailAddress);
            var emailAddress = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.CurrentEmailAddress));
            emailAddress.SendKeys(user.UpdatedEmail);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter current email address");
        }

        private void EnterPreviousEmailAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.UpdateEmail.Input.NewEmailAddress);
            var previousEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.NewEmailAddress));
            previousEmail.SendKeys(user.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter previous email address");
        }

        private void EnterConfirmPreviousEmailAddress(Users.CurrentUser user)
        {
           WaitForElement(5, Elements.UpdateEmail.Input.ConfirmEmailAddress);
           var confirmPreviousEmail = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.ConfirmEmailAddress));
           confirmPreviousEmail.SendKeys(user.UserName);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter previous email address a second time. ");
        }

        private void NavigateToUpdateEmailForReset(Users.CurrentUser user)
        {
            var memberLoginPage = new MemberLoginPage(Driver, user);
            var memberPage = new MemberPage(Driver, user);
            var updateMemberPage = new UpdateMember(Driver, user);
            var updateEmail = new UpdateEmail(Driver, user);

            memberLoginPage.LogInWithNewPassword(user);
            memberPage.AssertMemberPageLoaded();
            memberPage.ClickProfile();
            updateMemberPage.AssertUpdateMemberPageIsVisible();
            updateMemberPage.ClickChangeEmail();
            updateEmail.AssertUpdateEmailIsVisible();
        }

        public void ClickEmailAddressInputField()
        {
            WaitForElement(5, Elements.UpdateEmail.Input.NewEmailAddress);
            var emailAddressInputField = Driver.FindElement(By.XPath(Elements.UpdateEmail.Input.NewEmailAddress));
            emailAddressInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click email address input field");

        }
    }
}
