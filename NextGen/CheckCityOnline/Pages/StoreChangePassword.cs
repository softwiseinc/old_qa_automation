﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class StoreChangePassword : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreChangePassword(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreChangePassword;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreChangePassword;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreChangePassword;
        }

        public bool IsStoreChangePasswordLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("ResetMyPassword.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store-change-password page loaded successfully");
                Logger.Trace($"Store change password page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertStoreChangePasswordLoaded()
        {
            // //*[@id="logBlock"]/div[1]/div[2]/div/div/div/h2

            //*[@id="aspnetForm"]

            WaitForElement(5, "//*[text()= 'Password must be at least 8 characters and contain at least 3 of 4:']");
            Assert.IsTrue(IsStoreChangePasswordLoaded, ErrorStrings.StoreChangePasswordNotLoaded);

        }


        public void EnterNewPassword(Users.CurrentUser user)
        {
            WaitForElement(10, Elements.StoreChangePassword.InputFields.NewPassword);
            var newPasswordField = Driver.FindElement(By.XPath(Elements.StoreChangePassword.InputFields.NewPassword));
            newPasswordField.SendKeys(user.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "New Password is entered: " + user.NewPassword);

        }

        public void EnterConfirmPassword(Users.CurrentUser user)
        {
           WaitForElement(5, Elements.StoreChangePassword.InputFields.ConfirmPassword);
           var confirmPassword = Driver.FindElement(By.XPath(Elements.StoreChangePassword.InputFields.ConfirmPassword));
           confirmPassword.SendKeys(user.NewPassword);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Password is entered a second time: " + user.NewPassword);
        }

        public void ClickChange()
        {
            WaitForElement(5, Elements.StoreChangePassword.Buttons.Change);
            var changeButton = Driver.FindElement(By.XPath(Elements.StoreChangePassword.Buttons.Change));
            changeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Change button");
        }

        public void ClickSaveChanges()
        {
            WaitForElement(5, Elements.StoreChangePassword.Buttons.SaveChanges);
            var clickSaveChanges = Driver.FindElement(By.XPath(Elements.StoreChangePassword.Buttons.SaveChanges));
            clickSaveChanges.Click();
            Reporter.LogTestStepForBugLogger(Status.Info, "click save changes");
        }
    }
}
