﻿using System.Collections.Concurrent;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class ResetMyPassword : BaseApplicationPage
    {
        public ResetMyPassword(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ResetMyPassword;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ResetMyPassword;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ResetMyPassword;
        }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsResetMyPasswordPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("ResetMyPassword.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Reset My Password page loaded successfully");
                Logger.Trace($"Reset My Password page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        
        public void AssertResetPasswordPageLoaded()
        {
            Assert.IsTrue(IsResetMyPasswordPageLoaded, ErrorStrings.ResetMyPasswordNotLoaded);
        }


        public void ConfirmPasswordInput(string password)
        {
            var passWordConfirm = Driver.FindElement((By.XPath(Elements.ResetMyPassword.Input.ConfirmPassword)));
            passWordConfirm.SendKeys(password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the same password in the password input field to validate the change: " + password);
        }

        
        public void EnterNewPassword(string password)
        {
            WaitForElement(5, Elements.ResetMyPassword.Input.NewPassword);
            Driver.FindElement(By.XPath(Elements.ResetMyPassword.Input.NewPassword)).SendKeys(password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a new password value in the user name input field: " + password);
        }

        public void ClickChangePassword()
        {
            WaitForElement(5, Elements.ResetMyPassword.Button.ChangePassword);
            var changePasswordButton = Driver.FindElement(By.XPath(Elements.ResetMyPassword.Button.ChangePassword));
            changePasswordButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the change password button. ");
        }

        public void UseForgotPasswordLink(Users.CurrentUser user)
        {
            var memberSecurity = new MemberSecurity(Driver, user);
            var memberLoginPage = new MemberLoginPage(Driver, user);
            memberSecurity.NavigateToMemberSecurityPage(user);
            memberLoginPage.LogInWithNewPassword(user);
        }

        public void ResetToPreviousPassword(Users.CurrentUser user)
        {
            var memberPage = new MemberPage(Driver, user);
            var memberLoginPage = new MemberLoginPage(Driver, user);
            var storeProfile = new StoreProfile(Driver, user);


            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** BEGIN PASSWORD CLEANUP - Changing password back to previous password ***");
            memberLoginPage.LogInWithNewPassword(user);
            memberPage.ClickUserNameMenu();
            memberPage.ClickProfile();
            storeProfile.AssertStoreProfileIsVisible();
            storeProfile.ClickChangePassword();
            AssertResetPasswordPageLoaded();
            EnterNewPassword(user.Password);
            ConfirmPasswordInput(user.Password);
            ClickChangePassword();
            //updatePasswordPage.AssertUpdatePasswordPageIsVisible();
            //updatePasswordPage.EnterCurrentPassword(user.NewPassword);
            //updatePasswordPage.EnterNewPassword(user.Password);
            //updatePasswordPage.EnterConfirmPassword(user.Password);
            //updatePasswordPage.ClickUpdatePassword();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** END PASSWORD CLEANUP - Changed password back to previous password ***");
        }
    }
}