﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class ThankYouPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ThankYouPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ThankYouPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ThankYouPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ThankYouPage;
        }

        public bool IsThankYouPageVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.ThankYouPage.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the thank you payment page loaded successfully");
                Logger.Trace($"Thank you Payment page  loaded=>{isVisible}");
                return isVisible;
            }
        }

        public bool IsThankYouForYourPaymentPageVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.ThankYouPage.ThankYouForYourPayment))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'Thank you for your payment!' loaded successfully");
                Logger.Trace($"Thank you for your payment!' page successfully loaded=>{isVisible}");
                return isVisible;
            }
        }

        public void AssertThankYouPage()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.Loans.ThankYouPage.ScreenText);
            Assert.IsTrue(IsThankYouPageVisible, ErrorStrings.ThankYouPageNotVisible);
        }

        public void AssertThankYouForYourPayment()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.Loans.ThankYouPage.ThankYouForYourPayment);
            Assert.IsTrue(IsThankYouForYourPaymentPageVisible, ErrorStrings.ThankYouPageNotVisible);
        }

        public void ClickGotIt()
        {
            WaitForElement(5, Elements.Loans.ThankYouPage.GotItButton);
            var clickGotIt = Driver.FindElement(By.XPath(Elements.Loans.ThankYouPage.GotItButton));
            clickGotIt.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Signature ");
        }
    }
}
