﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class LoanAtStore : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public LoanAtStore(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanAtStore;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanAtStore;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanAtStore;
        }

        public bool IsLoanAtStoreVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.LoanAtStore.ScreenText.FinishMyApp))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the personal loan approval page loaded successfully");
                Logger.Trace($"Personal Loan Approval page is loaded=>{isVisible}");
                return isVisible;
            }
        }

        public void AssertLoanAtStorePage()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.LoanAtStore.ScreenText.FinishMyApp);
            Assert.IsTrue(IsLoanAtStoreVisible, ErrorStrings.LoanAtStoreNotVisible);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Finish my app should be visible");
        }


        public void ClickSendMyApplication()
        {
           WaitForElement(5, Elements.LoanAtStore.Buttons.SendMyApp);
           var sendMyApp = Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.SendMyApp));
           sendMyApp.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Send my app");

        }

        public void ClickGotIt()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.LoanAtStore.Buttons.GotIt);
            var gotIt = Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.GotIt));
            gotIt.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Got it!");
        }

        public void ClickCancel()
        {
           WaitForElement(5, Elements.LoanAtStore.Buttons.Cancel);
           var cancelButton = Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.Cancel));
           cancelButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Cancel button");
        }
    }
}
