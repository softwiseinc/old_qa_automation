﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    public class StoreProfile : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreProfile(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreProfile;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreProfile;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreProfile;
        }

        public bool IsStoreProfileVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.StoreProfile.ProfilePanel)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store-profile page loaded successfully");
                Logger.Trace($"Store profile page is visible");
                return isVisible;
            }
        }

        public void AssertStoreProfileIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsStoreProfileVisible, ErrorStrings.StoreProfilePageNotVisible);
        }

        public void ClickUpdateProfile()
        {
            WaitForElement(5, Elements.UpdateMember.UpdateButton);
            var changeAddress = Driver.FindElement(By.XPath(Elements.UpdateMember.UpdateButton));
            changeAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Update profile button.");
        }
        public bool IsStoreProfileSuccessfulTextIsVisible
        {
            get
            {
                ExplicitWait(4);
                var isVisible = Driver.FindElement(By.Id("ctl00_CustomContent_ErrorMessage")).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store-profile update text is visible");
                Logger.Trace($"Store profile update text is visible");
                return isVisible;
            }
        }

        public void AssertStoreProfileSuccessfulTextIsVisible()
        {
            Assert.IsTrue(IsStoreProfileSuccessfulTextIsVisible, ErrorStrings.ProfileUpdateSuccessfulNotVisible);
        }

        public string FindCustomerId()
        {
            WaitForElement(5, Elements.StoreProfile.ScreenText.CustomerId);
            //string customerId = Driver.FindElement(By.XPath(Elements.StoreProfile.ScreenText.CustomerId)).Text;
            string customerId = Driver.FindElement(By.CssSelector("div.row-fluid:nth-child(3) div.span12 div.center-container:nth-child(5) div.center-profile div.form:nth-child(2) div.data-container:nth-child(2) div.content:nth-child(2) > div.data")).Text;
            customerId = customerId.Substring(0);
            return customerId;
        }

        public void ClickChangePassword()
        {
            WaitForElement(10, Elements.StoreProfile.Buttons.ChangePassword);
            var changePasswordButton = Driver.FindElement(By.XPath(Elements.StoreProfile.Buttons.ChangePassword));
            changePasswordButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click change password button");
        }

        public void UpdateMembersAddress()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersAddress);
            var enterAddress = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersAddress));
            enterAddress.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current address field ");
            //enterAddress.SendKeys(Users.User.Address);
            enterAddress.SendKeys("ApprovedPersonal Lane #45");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's address");
        }

        public void UpdateMembersCity()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersCity);
            var enterCity = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersCity));
            enterCity.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clear the current city in the field");
            enterCity.SendKeys("Salt Lake City");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter City that the customer is in ");
        }
        public void UpdateMembersZipCode()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersZipCode));
            enterZipCode.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current zip code field. ");
            enterZipCode.SendKeys("84601");
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Zip code ");
        }


        public void ClickSaveChanges()
        {
            WaitForElement(5, Elements.StoreProfile.Buttons.SaveChanges);
            var saveChanges = Driver.FindElement(By.XPath(Elements.StoreProfile.Buttons.SaveChanges));
            saveChanges.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click save changes");
        }
    }
}
