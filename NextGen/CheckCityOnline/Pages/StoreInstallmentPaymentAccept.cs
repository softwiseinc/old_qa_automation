﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Pages
{
    internal class StoreInstallmentPaymentAccept : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreInstallmentPaymentAccept(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreInstallmentPaymentAccept;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreInstallmentPaymentAccept;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreInstallmentPaymentAccept;
        }

        public bool IsStoreInstallmentPaymentAccept
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.ScreenText.StoreInstallmentPaymentAccept))
                    .Displayed;

                Reporter.LogTestStepForBugLogger(Status.Info,
                    " Validate the message 'Store Installment Payment Accept' is displayed. ");
                Logger.Trace($"Store Customer Installment Accept Page is loaded=>{isVisible}");
                return isVisible;
            }
        }


        public void AssertStoreInstallmentPaymentAccept()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPaymentAccept.ScreenText.StoreInstallmentPaymentAccept);
            ExplicitWait(3);
            Assert.IsTrue(IsStoreInstallmentPaymentAccept, ErrorStrings.StoreInstallmentPaymentAcceptPageNotLoaded);

        }

        public void ClickMakePayment()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPaymentAccept.Button.Accept);
            var button = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.Button.Accept));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Make Payment button. ");
        }
        public bool IsStorePaymentAcceptVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.Button.Accept))
                    .Displayed;

                Reporter.LogTestStepForBugLogger(Status.Info,
                    " Validate the message 'Finalize Your Installment  Payment' is displayed. ");
                Logger.Trace($"Store Customer Installment Accept Page is loaded=>{isVisible}");
                return isVisible;
            }
        }

        public bool IsOneTimeAcceptPageVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.ScreenText.OneTimePaymentAccept))
                    .Displayed;

                Reporter.LogTestStepForBugLogger(Status.Info,
                    " Validate Screen Text 'ONE-TIME ONLINE PAYMENT AUTHORIZATION' is displayed. ");
                Logger.Trace($"Store Customer Installment Accept Page is loaded=>{isVisible}");
                return isVisible;
            }
        }


        public void AssertStorePaymentAccept()
        {
            WaitForElement(10, Elements.Loans.StoreInstallmentPaymentAccept.Button.Accept);
            Assert.IsTrue(IsStorePaymentAcceptVisible, ErrorStrings.StoreInstallmentPaymentAcceptPageNotLoaded);
        }

        public void ClickMakePaymentButton()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPaymentAccept.Button.MakePayment);
            var paymentButton =
                Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.Button.MakePayment));
            paymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click payment btn. ");
        }

        public void AssertOneTimePayment()
        {
            WaitForElement(10, Elements.Loans.StoreInstallmentPaymentAccept.ScreenText.OneTimePaymentAccept);
            Assert.IsTrue(IsOneTimeAcceptPageVisible, ErrorStrings.StoreInstallmentPaymentAcceptPageNotLoaded);
        }

        public void ClickIAgreeButton()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPaymentAccept.Button.Iagree);
            var iAgreeButton =
                Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept.Button.Iagree));
            iAgreeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click 'I Agree' button");
        }

        public void CompleteRepayWidget()
        {
            ExplicitWait(5);
            var frameDriver = Driver.SwitchTo().Frame("repayIFrame");
            frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.CardholderName)).SendKeys("Dr Peters");
            frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.CardNumber)).SendKeys("4111111111111111");
            frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.SecurityCode)).SendKeys("222");
            var monthSelect = frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.ExpirationMonth));
            
            var monthElement = new SelectElement(monthSelect);
            monthElement.SelectByText("12");
            var yearSelect = frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.ExpirationYear));

            var yearElement = new SelectElement(yearSelect);
            yearElement.SelectByIndex(3);
            frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.ZipCode)).SendKeys("84057");

            frameDriver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPaymentAccept
                .RepayDebitCardWidget.InputFields.PayButton)).Click();
            Driver.SwitchTo().DefaultContent();
        }
    }
}
