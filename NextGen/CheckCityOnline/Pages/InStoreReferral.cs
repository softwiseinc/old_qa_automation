﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using static CheckCityOnline.Elements.InStoreReferral;

namespace CheckCityOnline.Pages
{
    internal class InStoreReferral : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public InStoreReferral(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Referral;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Referral;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Referral;
        }

        public bool IsPageLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.InStoreReferral.Link.ClickHereStoreReferral)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'In Store Referral' page loaded successfully");
                Logger.Trace("'In Store Referral' page is loaded. ");
                return isVisible;
            }
        }

        public void AssertPageLoaded()
        {
            WaitForElement(5, Link.ClickHereStoreReferral);
            Assert.IsTrue(IsPageLoaded, ErrorStrings.InStoreReferralError);
        }
    }
}
