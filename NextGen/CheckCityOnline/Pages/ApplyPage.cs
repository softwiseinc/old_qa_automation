﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using AutomationResources;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Pages
{
    public class ApplyPage : BaseApplicationPage
    {
        public ApplyPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Apply;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Apply;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Apply;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsSelectMultipleCitiesVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectMultipleCities));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate multiple cities selectbox is visible => {selectBoxVisible}");
                Logger.Trace($"Validate multiple cities selectbox is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }

        public void AssertMultipleCitiesLoaded(Users.CurrentUser user)
        {
            ExplicitWait(5);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectMultipleCities));
            Assert.IsTrue(IsSelectMultipleCitiesVisible, ErrorStrings.SelectMultipleCitiesNotLoaded);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Multiple cities selectbox is visible ");

        }
        public void SelectCity()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectCity);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selectedCity")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select City.");
        }

        public bool IsClickAgreeConsentRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.ClickAgreeConsentRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate Agree consent required error is visible => {inputVisible}");
                Logger.Trace($"Validate consent terms required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsClickAgreeTermsRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.ClickAgreeTermRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate Agree terms required error is visible => {inputVisible}");
                Logger.Trace($"Validate Agree terms required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsContactPasswordRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate contact password required error is visible => {inputVisible}");
                Logger.Trace($"Validate contact password required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsClickAgreeTextMessageRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.ClickAgreeTextMessageRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate AgreeTextMessages required error is visible => {inputVisible}");
                Logger.Trace($"Validate AgreeTextMessages required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsVerificationCodeRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate verification code required error is visible => {inputVisible}");
                Logger.Trace($"Validate verification code required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsContactPhoneNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate contact phone number required error is visible => {inputVisible}");
                Logger.Trace($"Validate contact phone number required error is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsSelectLivingMonthRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectLivingMonthRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select living month required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validat livinge month required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectLivingYearRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectLivingYearRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select living year required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select living year required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsStreetAddressRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate street address required error is visible => {inputVisible}");
                Logger.Trace($"Validate street address required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsSocialSecurityNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate SocialSecurity number required error is visible => {inputVisible}");
                Logger.Trace($"Validate SocialSecurity number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsExpirationDateRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate expiration date required error is visible => {inputVisible}");
                Logger.Trace($"Validate expiration date required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsIdentificationNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate identification number required error is visible => {inputVisible}");
                Logger.Trace($"Validate identification number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsSelectIdentificationStateRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectIdStateRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select month required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate identification state required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }

        

        public bool IsSelectIdentificationTypeRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectIdTypeRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select identification type required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate identification type required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectOpenMonthRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectMonthRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select month required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate month required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectOpenYearRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectYearRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select year required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select year required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsReEnterBankAccountNumbeRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                //var errorClass = Driver.FindElement(By.ClassName("error")).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate re-enter bank account number required error is visible => {inputVisible}");
                Logger.Trace($"Validate re-enter bank account number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsBankAccountNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                //var errorClass = Driver.FindElement(By.ClassName("error")).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate bank account number required error is visible => {inputVisible}");
                Logger.Trace($"Validate bank account number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsRoutingNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate routing number required error is visible => {inputVisible}");
                Logger.Trace($"Validate routing number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsSelectMonthRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectMonthRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select month required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate month required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectYearRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectYearRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select year required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select year required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsEmployersZipCodeRequiredErrorVisible
        {
            get
            {
                var inpoutVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select income type required error is visible => {inpoutVisible}");
                Logger.Trace($"Validate select Income type required error  is visible => {inpoutVisible}");
                return inpoutVisible.Displayed;
            }
        }
        public bool IsSelectEmployersStateRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select income type required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select Income type required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsEmployersCityRequiredErrorVisible
        {
            get
            {
                var inpoutVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select income type required error is visible => {inpoutVisible}");
                Logger.Trace($"Validate select Income type required error  is visible => {inpoutVisible}");
                return inpoutVisible.Displayed;
            }
        }
        public bool IsPhoneNumberRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                //var errorClass = Driver.FindElement(By.ClassName("error")).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate phone number required error is visible => {inputVisible}");
                Logger.Trace($"Validate phone number required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsNameOfCompanyRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate name of company required error is visible => {inputVisible}");
                Logger.Trace($"Validate name of company required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsSelectIncomeTypeRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select income type required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select Income type required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectPayDayRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select pay day required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select pay day required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectDayPaidRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
               // var errorClass = selectBoxVisible.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select day paid required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select day paid required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsSelectPayPeriodRequiredErrorVisible
        {
            get
            {
                var selectBoxVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.SelectRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate select pay period required error is visible => {selectBoxVisible}");
                Logger.Trace($"Validate select pay period required error  is visible => {selectBoxVisible}");
                return selectBoxVisible.Displayed;
            }
        }
        public bool IsMonthlyIncomeRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                var errorClass = Driver.FindElement(By.ClassName("error")).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate monthly income required error is visible => {inputVisible}");
                Logger.Trace($"Validate monthly income required error  is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsPersonalLoanRequiredErrorVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.LoanTypeRequired))
                    .Displayed;
                var errorClass = Driver.FindElement(By.ClassName("ltError")).Text;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate Loan type required error is visible => {errorClass}");
                Logger.Trace($"Validate loan type required error  is visible => {errorClass}");
                return isVisible;
            }
        }
        public bool IsZipCodeRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate zipcode required error with red border is visible => {inputVisible}");
                Logger.Trace($"Validate zip code required error with red border is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsDateOfBirthRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate DOB required error with red border is visible => {inputVisible}");
                Logger.Trace($"Validate date of birth required error with red border is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsEmailRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate email required error with red border is visible => {inputVisible}");
                Logger.Trace($"Validate email required error with red border is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsLastNameRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate first name required error with red border is visible => {inputVisible}");
                Logger.Trace($"Validate last name required error with red border is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }
        public bool IsFirstNameRequiredErrorVisible
        {
            get
            {
                var inputVisible = Driver.FindElement(By.XPath(Elements.ApplyPage.ErrorMessage.InputRequired));
                Reporter.LogTestStepForBugLogger(Status.Info,
                    $"Validate first name required error with red border is visible => {inputVisible}");
                Logger.Trace($"Validate first name required error with red border is visible => {inputVisible}");
                return inputVisible.Displayed;
            }
        }

        public void ApplyPersonalLoan(Users.CurrentUser user)
        {
            string applyUrl = null;
            if (user.RunEnvironment == "PROD")
                applyUrl = Url.Production.Apply;
            else if (user.RunEnvironment == "STAGE")
                applyUrl = Url.Staging.Apply;
            else if (user.RunEnvironment == "QA")
                applyUrl = Url.Test.Apply;
            Driver.Navigate().GoToUrl(applyUrl);
            
            EnterFirstName(user);
            EnterLastName(user);
            EnterEmail(user);
            EnterDateOfBirth(user);
            EnterZipCode(user);
            
            if(user.State == "UT")
                ClickPersonalLoan();
            ClickContinueButton();
            
            EnterMonthlyIncome(user);
            SelectPayPeriod();
            SelectDayPaid();
            SelectPayDay();
            SelectIncomeType();
            EnterNameOfCompany(user);
            EnterEmployerPhoneNumber(user);
            EnterEmployersCity(user);
            SelectEmployersState(user);
            EnterEmployersZipCode(user);
            SelectYear();
            SelectMonth();
            SelectFirstIncomeDirectDepositYes();

            AddAdditionalIncome();
            AdditionalEnterMonthlyIncome(user);
            AdditionalSelectPayPeriod();
            AdditionalSelectDayPaid();
            AdditionalSelectPayDay();
            AdditionalSelectIncomeType();
            AdditionalEnterNameOfCompany(user);
            AdditionalEnterPhoneNumber(user);
            AdditionalEnterEmployersCity(user);
            AdditionalSelectEmployersState(user);
            AdditionalEnterEmployersZipCode(user);
            AdditionalChooseDirectDeposit();
            AdditionalSelectYear();
            AdditionalSelectMonth();
            SelectSecondIncomeDirectDepositYes();


            EnterRoutingNumber(user);
            ExplicitWait(2);
            EnterBankAccountNumber(user);
            ExplicitWait(2);
            ReEnterBankAccountNumber(user);
            ExplicitWait(2);
            SelectOpenYear();
            SelectOpenMonth();
            ChooseDirectDeposit();
            ChooseChecking();
            if (user.State == "UT" || user.State == "TX" || user.State == "ID")
                SelectUseThisBankForLoanPayment();


            SelectIdentificationType();
            SelectIdentificationState(user);
            EnterIdentificationNumber(user);

            //if (user.State != "CA")
            EnterExpirationDate(user);

            EnterStreetAddress(user);
            EnterSocialSecurityNumber(user);

            SelectLivingYear();
            SelectLivingMonth();
            ChooseRentOption();


            EnterContactPhoneNumber(user);
            ClickYesNoticeAboutFuturePayments();


            ClickSendVerificationCode();
            EnterVerificationCode(user, true);


            ClickAgreeTextMessages();
            ClickAgreePhoneCalls();

            EnterContactPassword(user);

            ClickAgreeTerms();
            ClickAgreeConsent();

            if (user.State == "CA")
                ClickCaAgree();

            //AdditionalSelectEmployersState(user);
            //SelectEmployersState(user);
            ClickSubmitApplication();
        }


        public void ApplyPayDayLoan(Users.CurrentUser customer)
        {
            //Personal Information
            string applyUrl = null;
            if (customer.RunEnvironment == "PROD")
                applyUrl = Url.Production.Apply;
            else if (customer.RunEnvironment == "STAGE")
                applyUrl = Url.Staging.Apply;
            else if (customer.RunEnvironment == "QA")
                applyUrl = Url.Test.Apply;
            Driver.Navigate().GoToUrl(applyUrl);
            EnterFirstName(customer);
            EnterLastName(customer);
            EnterEmail(customer);
            EnterDateOfBirth(customer);
            EnterZipCode(customer);
            
            //ClickContinueButton();
            if (customer.State == "UT")
                ClickPaydayLoan();
            ClickContinueButton();

            EnterMonthlyIncome(customer);
            SelectPayPeriod();
            SelectDayPaid();
            SelectPayDay();
            SelectIncomeType();
            EnterNameOfCompany(customer);
            EnterEmployerPhoneNumber(customer);
            if (customer.State != "NV")
            {
                EnterEmployersCity(customer);
                SelectEmployersState(customer);
                EnterEmployersZipCode(customer);

                SelectYear();
                SelectMonth();
                SelectFirstIncomeDirectDepositYes();
                ChooseDirectDeposit();
                ChooseChecking();
                AddAdditionalIncome();
                AdditionalEnterMonthlyIncome(customer);
                AdditionalSelectPayPeriod();
                AdditionalSelectDayPaid();
                AdditionalSelectPayDay();
                AdditionalSelectIncomeType();
                AdditionalEnterNameOfCompany(customer);
                AdditionalEnterPhoneNumber(customer);
                AdditionalEnterEmployersCity(customer);
                AdditionalSelectEmployersState(customer);
                AdditionalEnterEmployersZipCode(customer);
                AdditionalSelectYear();
                AdditionalSelectMonth();
                SelectSecondIncomeDirectDepositYes();
            }

            EnterRoutingNumber(customer);
            ExplicitWait(2);
            EnterBankAccountNumber(customer);
            ExplicitWait(2);
            ReEnterBankAccountNumber(customer);
            ExplicitWait(2);

            if (customer.State != "NV")
            {
                SelectOpenYear();
                SelectOpenMonth();
            }
            
            SelectIdentificationType();
            SelectIdentificationState(customer);
            EnterIdentificationNumber(customer);

            if (customer.State != "NV")
                EnterExpirationDate(customer);

            EnterStreetAddress(customer);
            EnterSocialSecurityNumber(customer);

            if (customer.State != "NV")
            {
                SelectLivingYear();
                SelectLivingMonth();
                ChooseRentOption();
            }

            EnterContactPhoneNumber(customer);
            ClickYesNoticeAboutFuturePayments();

            if (customer.State != "NV")
            {
                ClickSendVerificationCode();
                EnterVerificationCode(customer, true);
            }

            ClickAgreeTextMessages();
            ClickAgreePhoneCalls();

            EnterContactPassword(customer);

            ClickAgreeTerms();
            ClickAgreeConsent();

            if (customer.State == "CA")
                ClickCaAgree();

            //AdditionalSelectEmployersState(customer);
            //SelectEmployersState(customer);
            ClickSubmitApplication();

        }

        public void ApplyPayDayLoan(Users.CurrentUser customer, string zipCode)
        {
            //Personal Information
            EnterFirstName(customer);
            EnterLastName(customer);
            EnterEmail(customer);
            EnterDateOfBirth(customer);
            EnterZipCode("84057");

            //ClickContinueButton();
            if (customer.State == "UT")
                ClickPaydayLoan();
            ClickContinueButton();

            EnterMonthlyIncome(customer);
            SelectPayPeriod();
            SelectDayPaid();
            SelectPayDay();
            SelectIncomeType();
            EnterNameOfCompany(customer);
            EnterEmployerPhoneNumber(customer);

            if (customer.State == "UT" || customer.State == "WY" || customer.State == "TX")
            {
                EnterEmployersCity(customer);
                SelectEmployersState(customer);
                EnterEmployersZipCode(customer);
                SelectYear();
                SelectMonth();
                ChooseDirectDeposit();
                AddAdditionalIncome();
                AdditionalEnterMonthlyIncome(customer);
                AdditionalSelectPayPeriod();
                AdditionalSelectDayPaid();
                AdditionalSelectPayDay();
                AdditionalSelectIncomeType();
                AdditionalEnterNameOfCompany(customer);
                AdditionalEnterPhoneNumber(customer);
                AdditionalEnterEmployersCity(customer);
                AdditionalSelectEmployersState(customer);
                AdditionalEnterEmployersZipCode(customer);
                AdditionalSelectYear();
                AdditionalSelectMonth();
                AdditionalChooseDirectDeposit();
            }

            EnterRoutingNumber(customer);
            ExplicitWait(2);
            EnterBankAccountNumber(customer);
            ExplicitWait(2);
            ReEnterBankAccountNumber(customer);
            ExplicitWait(2);

            if (customer.State == "UT" || customer.State == "WY" || customer.State == "TX")
            {
                SelectOpenYear();
                SelectOpenMonth();
            }

            SelectIdentificationType();
            SelectIdentificationState(customer);
            EnterIdentificationNumber(customer);

            if (customer.State == "UT" || customer.State == "WY" || customer.State == "TX")
                EnterExpirationDate(customer);

            EnterStreetAddress(customer);
            EnterSocialSecurityNumber(customer);

            if (customer.State == "UT" || customer.State == "WY" || customer.State == "TX")
            {
                SelectLivingYear();
                SelectLivingMonth();
                ChooseRentOption();
            }

            EnterContactPhoneNumber(customer);

            if (customer.State == "UT" || customer.State == "WY" || customer.State == "TX")
            {
                ClickSendVerificationCode();
                EnterVerificationCode(customer, true);
            }

            ClickAgreeTextMessages();
            ClickAgreePhoneCalls();

            EnterContactPassword(customer);

            ClickAgreeTerms();
            ClickAgreeConsent();

            if (customer.State == "CA")
                ClickCaAgree();

            ClickSubmitApplication();

        }
        public void ClickCaAgree()
        {
            WaitForElement(5, Elements.ApplyPage.Input.CaAgreeConsent);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.CaAgreeConsent)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click CA agreement checkbox.");
        }

        public void ClickSubmitApplication()
        {
            WaitForElement(5, Elements.ApplyPage.Input.SubmitApplication);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.SubmitApplication)).Click();
            ExplicitWait(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Submit Application.");
        }
        public void ClickAgreeConsent()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeConsent);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeConsent)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on checkbox option to agree.");
        }
        public void ClickAgreeConsentRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeConsent);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeConsent));
            Assert.IsTrue(IsClickAgreeConsentRequiredErrorVisible, ErrorStrings.ClickAgreeConsentRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Agree consent terms required error is visible. ");
        }
        public void ClickAgreeTerms()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeTerms);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeTerms)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on checkbox option to agree.");
        }
        public void ClickAgreeTermRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeTerms);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeTerms));
            Assert.IsTrue(IsClickAgreeTermsRequiredErrorVisible, ErrorStrings.ClickAgreeTermsRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Agree terms required error is visible. ");
        }
        public void EnterContactPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.Header.BrandLogo);
            WaitForElement(5, Elements.ApplyPage.Input.ContactPassword);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ContactPassword)).SendKeys(user.ContactPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ContactPassword value in contact password input field: " + user.ContactPassword);
        }
        public void ContactPasswordRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ContactPassword);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ContactPassword));
            Assert.IsTrue(IsContactPasswordRequiredErrorVisible, ErrorStrings.ContactPasswordRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Contact Password  bordered error is visible. ");
        }
        public void ClickAgreePhoneCalls()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreePhoneCalls);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreePhoneCalls)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on checkbox option to agree.");
        }
        
        public void ClickAgreeTextMessages()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeTextMessages);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeTextMessages)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on checkbox option to agree.");
        }
        public void ClickAgreeTextMessagesRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AgreeTextMessages);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AgreeTextMessages));
            Assert.IsTrue(IsClickAgreeTextMessageRequiredErrorVisible, ErrorStrings.ClickAgreeTextMessageRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Agree text message error is visible. ");
        }
        public void EnterVerificationCode(Users.CurrentUser user, bool isStartup)
        {
            if (isStartup)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info,"Invalid phone number use for testing");
            }
            else
            {
                WaitForElement(5, Elements.ApplyPage.Input.VerificationCode);
                Driver.FindElement(By.XPath(Elements.ApplyPage.Input.VerificationCode)).SendKeys(user.VerificationCode);
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                    "Enter a VerificationCode value in contact verification code input field: " + user.VerificationCode);
            }
        }

        public void VerificationCodeRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.VerificationCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.VerificationCode));
            Assert.IsTrue(IsVerificationCodeRequiredErrorVisible, ErrorStrings.VerificationCodeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Verification code  bordered error is visible. ");
        }
        public void ClickSendVerificationCode()
        {
            WaitForElement(5, Elements.ApplyPage.Button.SendVerificationCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Button.SendVerificationCode)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Send Veficication Code.");
            ExplicitWait(2);
        }
        public void EnterContactPhoneNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.ContactPhoneNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ContactPhoneNumber)).SendKeys(user.PhoneNumber + user.ContactPhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ContactPhoneNumber value in contact phone number input field: " + user.PhoneNumber + user.ContactPhoneNumber) ;
            ExplicitWait(2);
        }
        public void ClickYesNoticeAboutFuturePayments()
        {

            WaitForElement(5, Elements.ApplyLoan.ContactInformation.NoticeAboutFuturePayment);
            var noticeAboutFuturePayment = Driver.FindElement(By.XPath(Elements.ApplyLoan.ContactInformation.NoticeAboutFuturePayment));
            try
            {
                noticeAboutFuturePayment.Click();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Nothing to see here");
                
            }
            
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Notice About Future Payment radio button 'YES' is clicked. ");
        }
        public void ContactPhoneNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ContactPhoneNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ContactPhoneNumber));
            Assert.IsTrue(IsContactPhoneNumberRequiredErrorVisible, ErrorStrings.ContactPhoneRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Street address  bordered error is visible. ");
        }

        public void ChooseRentOption()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ChooseDirectDeposit);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ChooseRentOption)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Rent on Own/Rent option.");
        }
        
        public void SelectLivingMonth()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectLivingMonth);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("residenceLengthMonth")));
            oSelect.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Month.");
        }
        public void SelectLivingMonthRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectLivingMonth);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectLivingMonth));
            Assert.IsTrue(IsSelectLivingMonthRequiredErrorVisible, ErrorStrings.SelectMonthRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select living Year Bordered error is visible. ");
        }
        public void SelectLivingYear()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectLivingYear);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("residenceLengthYear")));
            oSelect.SelectByIndex(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Year.");
        }
        public void SelectLivingYearRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectLivingYear);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectLivingYear));
            Assert.IsTrue(IsSelectLivingYearRequiredErrorVisible, ErrorStrings.SelectYearRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select living Year Bordered error is visible. ");
        }
        public void EnterStreetAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.StreetAddress);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.StreetAddress)).SendKeys(user.StreetAddress);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a StreetAddress value in street address input field: " + user.StreetAddress);
        }
        public void StreetAddressRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.StreetAddress);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.StreetAddress));
            Assert.IsTrue(IsStreetAddressRequiredErrorVisible, ErrorStrings.StreetAddressRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Street address  bordered error is visible. ");
        }
        public void EnterSocialSecurityNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.SocialSecurityNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.SocialSecurityNumber)).SendKeys(user.SocialSecurityNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a SocialSecurityNumber value in social security number input field: " + user.SocialSecurityNumber);
        }
        public void SocialSecurityNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.SocialSecurityNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.SocialSecurityNumber));
            Assert.IsTrue(IsSocialSecurityNumberRequiredErrorVisible, ErrorStrings.SocialSecurityNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Social Security number  bordered error is visible. ");
        }
        public void EnterExpirationDate(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.ExpirationDate);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ExpirationDate)).SendKeys(user.ExpirationDate);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ExpirationDate value in expiration date input field: " + user.ExpirationDate);
        }
        public void ExpirationDateRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ExpirationDate);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ExpirationDate));
            Assert.IsTrue(IsExpirationDateRequiredErrorVisible, ErrorStrings.ExpirationDateRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Expiration date bordered error is visible. ");
        }
        public void EnterIdentificationNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.IdentificationNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.IdentificationNumber)).SendKeys(user.IdentificationNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a IdentificationNumber value in identification number input field: " + user.IdentificationNumber);
        }
        public void IdentificationNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.IdentificationNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.IdentificationNumber));
            Assert.IsTrue(IsIdentificationNumberRequiredErrorVisible, ErrorStrings.IdentificationNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Identification Number bordered error is visible. ");
        }
        public void SelectIdentificationState()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIdentificationState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("identityState")));
            oSelect.SelectByText("Utah");
            //oSelect.SelectByValue(Users.User.State);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Identification State.");
        }



        public void SelectIdentificationState(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIdentificationState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("identityState")));
            oSelect.SelectByValue(Users.User.State);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Identification State.");
        }
        public void SelectIdentificationStateRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIdentificationState);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectIdentificationState));
            Assert.IsTrue(IsSelectIdentificationStateRequiredErrorVisible, ErrorStrings.SelectIdentificationStateRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Identification State Bordered error is visible. ");
        }
        public void SelectIdentificationType()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIdentificationType);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("ddlIdentityType")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Identification Type.");
        }
        public void SelectIdentificationTypeRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIdentificationType);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectIdentificationType));
            Assert.IsTrue(IsSelectIdentificationTypeRequiredErrorVisible, ErrorStrings.SelectIdentifcationTypeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Identification Type Bordered error is visible. ");
        }
        public void SelectUseThisBankForLoanPayment()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ChooseBankAccountForPayment);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ChooseBankAccountForPayment)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Choose Use This bank account for your loan payment");
        }
        public void SelectOpenMonth()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectOpenMonth);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("bankAccountLengthMonth")));
            oSelect.SelectByIndex(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in month.");
        }
        public void SelectOpenMonthRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectMonth);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectMonth));
            Assert.IsTrue(IsSelectOpenMonthRequiredErrorVisible, ErrorStrings.SelectMonthRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Open Month Bordered error is visible. ");
        }
        public void SelectOpenYear()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectOpenYear);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("bankAccountLengthYear")));
            oSelect.SelectByIndex(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in years.");
        }
        public void SelectOpenYearRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectOpenYear);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectOpenYear));
            Assert.IsTrue(IsSelectOpenYearRequiredErrorVisible, ErrorStrings.SelectYearRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Open Year Bordered error is visible. ");
        }
        public void ReEnterBankAccountNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.ReEnterBankAccountNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ReEnterBankAccountNumber)).SendKeys(user.ReEnterBankAccountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ReEnterBankAccountNumber value in Re enter bank account number input field: " + user.ReEnterBankAccountNumber);
        }
        public void ReEnterBankAccountNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.RoutingNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.RoutingNumber));
            Assert.IsTrue(IsReEnterBankAccountNumbeRequiredErrorVisible, ErrorStrings.ReEnterBankAccountNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Re-enter banking account number  required bordered error is visible. ");
        }
        public void EnterBankAccountNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.BankAccountNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.BankAccountNumber)).SendKeys(user.BankAccountNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a BankAccountNumber value in Bank AccountLink Number input field: " + user.BankAccountNumber);
        }
        public void BankAccountNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.RoutingNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.RoutingNumber));
            Assert.IsTrue(IsBankAccountNumberRequiredErrorVisible, ErrorStrings.BankAccountNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Banking number  required bordered error is visible. ");
        }
        public void EnterRoutingNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.RoutingNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.RoutingNumber)).SendKeys(user.RoutingNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a RoutingNumber value in Routing Number input field: " + user.RoutingNumber);
        }
        public void RoutingNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.RoutingNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.RoutingNumber));
            Assert.IsTrue(IsRoutingNumberRequiredErrorVisible, ErrorStrings.RoutingNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Routing number  required bordered error is visible. ");
        }
        public void AddAdditionalIncome()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AddAdditionalIncome);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AddAdditionalIncome)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click  add additional income option checkbox.");
        }
        //additonal income starts
        public void AdditionalChooseDirectDeposit()
        {
            WaitForElement(5, Elements.ApplyPage.Input.AdditionalChooseDirectDeposit);
            Driver.FindElement(By.XPath("//*[@id='switch_firstIncome_left']//following::input[13]")).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yes on Direct Deposit option.");
        }
        public void AdditionalSelectMonth()
        {
            WaitForElement(5, Elements.ApplyPage.Select.AdditionalSelectMonth);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerTimeMonthAdd")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in month.");
        }
        public void AdditionalSelectYear()
        {
            WaitForElement(5, Elements.ApplyPage.Select.AdditionalSelectYear);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerTimeYearAdd")));
            oSelect.SelectByIndex(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in years.");
        }
        public void AdditionalEnterEmployersZipCode(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.AdditionalEmployersZipCode);
            var zipInputField = Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalEmployersZipCode));
            zipInputField.Clear();
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalEmployersZipCode)).SendKeys(user.AdditionalEmployersZipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a EmployersZipCode value in employer's zip code input field: " + user.AdditionalEmployersZipCode);
        }
        public void AdditionalSelectEmployersState()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectEmployersState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerStateAdd")));
            oSelect.SelectByText("Utah");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Employer's State.");
        }
        public void AdditionalSelectEmployersState(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectEmployersState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerStateAdd")));
            oSelect.SelectByText(user.AdditionalEmployersState);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Employer's State.");
        }
        public void AdditionalEnterEmployersCity(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.AdditionalEmployersCity);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalEmployersCity)).SendKeys(user.AdditionalEmployersCity);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a EmployersCity value in the employer's city input field: " + user.AdditionalEmployersCity);
        }
        public void AdditionalEnterPhoneNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.AdditionalPhoneNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalPhoneNumber)).SendKeys(user.AdditionalPhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a PhoneNumber value in the phone number input field: " + user.AdditionalPhoneNumber);
        }
        public void AdditionalEnterNameOfCompany(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.AdditionalNameOfCompany);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalNameOfCompany)).SendKeys(user.AdditionalNameOfCompany);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a NameOfCompany value in the name of company input field: " + user.AdditionalNameOfCompany);
        }
        public void AdditionalSelectIncomeType()
        {
            WaitForElement(10, Elements.ApplyPage.Select.AdditionalSelectIncomeType);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("incomeTypeAdd")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Income Type.");
        }
        public void AdditionalSelectPayDay()
        {
            WaitForElement(5, Elements.ApplyPage.Select.AdditionalSelectPayDay);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selBiWeeklyDateAdd")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Pay Day.");
        }

        public void AdditionalSelectDayPaid()
        {
            WaitForElement(5, Elements.ApplyPage.Select.AdditionalSelectDayPaid);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selBiWeeklyAdd")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Day Paid.");
        }
        public void AdditionalSelectPayPeriod()
        {
            WaitForElement(5, Elements.ApplyPage.Select.AdditionalSelectPayPeriod);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("payPeriodAdd")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Pay Period.");
        }
        public void AdditionalEnterMonthlyIncome(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.MonthlyIncome);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.AdditionalMonthlyIncome)).SendKeys(user.AdditionalMonthlyIncome);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a MonthlyIncome value in the monthly income input field: " + user.AdditionalMonthlyIncome);
        }
        //additional income ends

        public void ChooseDirectDeposit()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.ApplyPage.Input.ChooseDirectDeposit);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ChooseDirectDeposit)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yes on Direct Deposit option.");
        }
        public void SelectFirstIncomeDirectDepositYes()
        {
            WaitForElement(5, Elements.ApplyPage.Input.FirstIncomeDepositYes);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.FirstIncomeDepositYes)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yes on 'Do you have direct deposit?' option, for the first income.");
        }
        public void SelectSecondIncomeDirectDepositYes()
        {
            WaitForElement(5, Elements.ApplyPage.Input.SecondIncomeDepositYes);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.SecondIncomeDepositYes)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Yes on 'Do you have direct deposit?' option, for the second income");
        }
        public void ChooseChecking()
        {
            WaitForElement(5, Elements.ApplyPage.Input.ChooseChecking);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ChooseChecking)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Choose Checking Account Type");
        }
        public void SelectMonth()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectMonth);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerTimeMonth")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in month.");
        }
        public void SelectMonthRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectMonth);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectMonth));
            Assert.IsTrue(IsSelectMonthRequiredErrorVisible, ErrorStrings.SelectMonthRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Month  required bordered error is visible. ");
        }
        public void SelectYear()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectYear);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerTimeYear")));
            oSelect.SelectByIndex(3);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Length of Time with Employer in years.");
        }
       
        public void SelectYearRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectYear);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectYear));
            Assert.IsTrue(IsSelectYearRequiredErrorVisible, ErrorStrings.SelectYearRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Select Year  required bordered error is visible. ");
        }
        public void EnterEmployersZipCode(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployersZipCode);
            var zipInputField = Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersZipCode));
            zipInputField.Clear();
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersZipCode)).SendKeys(user.EmployerZipcode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a EmployersZipCode value in employer's zip code input field: " + user.EmployerZipcode);
        }
        public void EmployersZipCodeRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployersZipCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersZipCode));
            Assert.IsTrue(IsEmployersZipCodeRequiredErrorVisible, ErrorStrings.EmployersZipCodeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Employers zip code  required bordered error is visible. ");
        }
        public void SelectEmployersState()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectEmployersState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerState")));
            oSelect.SelectByText("Utah");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Employer's State.");
        }
        public void SelectEmployersState(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectEmployersState);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("employerState")));
            oSelect.SelectByValue(user.EmployerState);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Employer's State.");
        }
        public void SelectEmployersStateRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectEmployersState);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectEmployersState));
            Assert.IsTrue(IsSelectEmployersStateRequiredErrorVisible, ErrorStrings.SelectEmployersStateRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Employers state  required bordered error is visible. ");
        }
        public void EnterEmployersCity(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployersCity);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersCity)).Clear();
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersCity)).SendKeys(user.EmployerCity);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a EmployersCity value in the employer's city input field: " + user.EmployerCity);
        }
        public void EmployersCityRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployersCity);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployersCity));
            Assert.IsTrue(IsEmployersCityRequiredErrorVisible, ErrorStrings.EmployersCityRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Employers city  required bordered error is visible. ");
        }
        public void EnterEmployerPhoneNumber(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployerPhoneNumber);
            if (user.EmployerPhone == null)
                user.EmployerPhone = "0929388382";
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployerPhoneNumber)).SendKeys(user.EmployerPhone);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a PhoneNumber value in the phone number input field: " + user.EmployerPhone);
        }
        public void PhoneNumberRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.EmployerPhoneNumber);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.EmployerPhoneNumber));
            Assert.IsTrue(IsPhoneNumberRequiredErrorVisible, ErrorStrings.PhoneNumberRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Phone number  required bordered error is visible. ");
        }
        public void EnterNameOfCompany(Users.CurrentUser user)
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplyPage.Input.NameOfCompany);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.NameOfCompany)).SendKeys(user.NameOfCompany);
            ExplicitWait(1);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.NameOfCompany)).SendKeys(Keys.ArrowDown);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.NameOfCompany)).SendKeys(Keys.Enter);
            ExplicitWait(1);
                Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a NameOfCompany value in the name of company input field: " + user.NameOfCompany);
        }
        public void NameOfCompanyRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.NameOfCompany);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.MonthlyIncome));
            Assert.IsTrue(IsNameOfCompanyRequiredErrorVisible, ErrorStrings.NameOfCompanyRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Name of company  required bordered error is visible. ");
        }
        public void SelectIncomeType()
        {
            WaitForElement(10, Elements.ApplyPage.Select.SelectIncomeType);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("incomeType")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Income Type.");
        }
        public void SelectIncomeTypeRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectIncomeType);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectIncomeType));
            Assert.IsTrue(IsSelectIncomeTypeRequiredErrorVisible, ErrorStrings.SelectIncomeTypeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Income type  required bordered error is visible. ");
        }
        public void SelectPayDay()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectPayDay);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selBiWeeklyDate")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Pay Day.");
        }
        public void SelectPayDayRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectPayDay);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectPayDay));
            Assert.IsTrue(IsSelectPayDayRequiredErrorVisible, ErrorStrings.SelectPayDayRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Pay day  required bordered error is visible. ");
        }
        public void SelectDayPaid()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectDayPaid);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selBiWeekly")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Day Paid.");
        }
        public void SelectDayPaidRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectDayPaid);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectDayPaid));
            Assert.IsTrue(IsSelectDayPaidRequiredErrorVisible, ErrorStrings.SelectDayPaidRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Day paid  required bordered error is visible. ");
        }
        public void SelectDayOfMonthPaid()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectDayOfMonthPaid);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selMonthlyByDay")));
            oSelect.SelectByIndex(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select day of month paid.");
        }
        public void SelectMonthlyType()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectMonthlyType);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("selMonthlyType")));
            oSelect.SelectByText("Specific Day");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Monthly type.");
        }
        public void SelectPayPeriodMonthly()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectPayPeriod);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("payPeriod")));
            oSelect.SelectByText("Monthly");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Pay Period.");
        }
        public void SelectPayPeriod()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectPayPeriod);
            SelectElement oSelect = new SelectElement(Driver.FindElement(By.Id("payPeriod")));
            oSelect.SelectByIndex(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Select Pay Period.");
        }
        
        public void SelectPayPeriodRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectPayPeriod);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectPayPeriod));
            Assert.IsTrue(IsSelectPayPeriodRequiredErrorVisible, ErrorStrings.SelectPayPeriodRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Pay period required bordered error is visible. ");
        }
        public void EnterMonthlyIncome(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.MonthlyIncome);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.MonthlyIncome)).SendKeys(user.MonthlyIncome);

            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a MonthlyIncome value in the monthly income input field: " + user.MonthlyIncome);
        }
        public void MonthlyIncomeRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.MonthlyIncome);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.MonthlyIncome));
            Assert.IsTrue(IsMonthlyIncomeRequiredErrorVisible, ErrorStrings.MonthlyIncomeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Monthly income required bordered error is visible. ");
        }
        public void ClickPersonalLoan()
        {
            ClickContinueButton();
            WaitForElement(5, Elements.ApplyPage.Input.PersonalLoan);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.PersonalLoan)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Personal Loan.");
        }
        public void ClickPaydayLoan()
        {
            ClickContinueButton();
            ExplicitWait(2);
            WaitForElement(5, Elements.ApplyPage.Input.PaydayLoan);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.PaydayLoan)).Click();
            ClickContinueButton();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Personal Loan.");
        }
        public void PersonalLoanRequired()
        {
            WaitForElement(5, Elements.ApplyPage.Input.PersonalLoan);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.PersonalLoan));
            Assert.IsTrue(IsPersonalLoanRequiredErrorVisible, ErrorStrings.PersonalLoanRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Loan type required error is visible. ");
        }
        public void ClickContinueButton()
        {
            WaitForElement(5, Elements.ApplyPage.Link.ClickContinue);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Link.ClickContinue)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Continue button.");
            //LoanTypeHelper();
        }

        public void EnterZipCode(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.ZipCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ZipCode)).SendKeys(user.ZipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ZipCode value in the zip code input field: " + user.ZipCode);
        }

        public void EnterZipCode(string zipCode)
        {
            WaitForElement(5, Elements.ApplyPage.Input.ZipCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ZipCode)).SendKeys(zipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a ZipCode value in the zip code input field: " + zipCode);
        }

        public void ZipCodeRequired(Users.CurrentUser user)
        {
            ClickContinueButton();
            WaitForElement(5, Elements.ApplyPage.Input.ZipCode);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.ZipCode));
            Assert.IsTrue(IsZipCodeRequiredErrorVisible, ErrorStrings.ZipCodeRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Zip code error is visible. ");
        }
        public void EnterDateOfBirth(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.DateOfBirth);
            var thisDateOfBirth = user.DateOfBirth;
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.DateOfBirth)).SendKeys(user.DateOfBirth);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a DateOfBirth value in the date of birth input field: " + user.DateOfBirth);
        }
        public void DateOfBirthRequired(Users.CurrentUser user)
        {
            ClickContinueButton();
            WaitForElement(5, Elements.ApplyPage.Input.DateOfBirth);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.DateOfBirth));
            Assert.IsTrue(IsDateOfBirthRequiredErrorVisible, ErrorStrings.DateOfBirthRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Date of birth bordered error is visible. ");
        }
        public void EnterEmail(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.Email);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.Email)).SendKeys(user.Email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter an Email value in the email input field: " + user.Email);
        }
        public void EmailRequired(Users.CurrentUser user)
        {
            ClickContinueButton();
            WaitForElement(5, Elements.ApplyPage.Input.Email);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.Email));
            Assert.IsTrue(IsEmailRequiredErrorVisible, ErrorStrings.EmailRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Email bordered error is visible. ");
        }
        public void EnterLastName(Users.CurrentUser user)
        {
            WaitForElement(15, Elements.ApplyPage.Input.LastName);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.LastName)).SendKeys(user.LastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a LastName value in the last name input field: " + user.LastName);
        }

        public void LastNameRequired(Users.CurrentUser user)
        {
           ClickContinueButton();
            WaitForElement(5, Elements.ApplyPage.Input.LastName);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.LastName));
            Assert.IsTrue(IsLastNameRequiredErrorVisible, ErrorStrings.LastNameRequired);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Validate Forced Error: Last name bordered error is visible. ");
        }

        public void EnterFirstName(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.ApplyPage.Input.FirstName);
            Driver.FindElement(By.XPath(Elements.ApplyPage.Input.FirstName)).SendKeys(user.FirstName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "Enter a FirstName value in the first name input field: " + user.FirstName);
        }

         public void FirstNameRequired(Users.CurrentUser user)
         {
            ClickContinueButton();
             WaitForElement(15, Elements.ApplyPage.Input.FirstName);
             //Driver.FindElement(By.XPath(Elements.ApplyPage.Input.FirstName)).SendKeys(user.FirstName);
             Driver.FindElement(By.XPath(Elements.ApplyPage.Input.FirstName));
             Assert.IsTrue(IsFirstNameRequiredErrorVisible, ErrorStrings.FirstNameRequired);
             Reporter.LogPassingTestStepToBugLogger(Status.Info,
                 "Validate Forced Error: First name bordered error is visible. ");
         }
        
         public bool IsApplyLoanPageLoaded
         {
             get
             {
                 var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                 Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Apply Loan page loaded successfully");
                 Logger.Trace($"Apply Loan page is loaded=>{isLoaded}");
                 return isLoaded;
             }
         }

        public void NavigateToApplyLoanPage()
         {
             Driver.Navigate().GoToUrl(TestEnvironment.Url);
             Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
             WaitForElement(5, Elements.ApplyLoan.PersonalInformation.FirstName);
             Assert.IsTrue(IsApplyLoanPageLoaded, ErrorStrings.ApplyLoanPageNotLoaded);
         }

        public void ClickIdentificationStateDropDown()
        {
            WaitForElement(5, Elements.ApplyLoan.IdentifyInformation.IdentificationState);
            var clickIdStateDropDown =
                Driver.FindElement(By.XPath(Elements.ApplyLoan.IdentifyInformation.IdentificationState));
            clickIdStateDropDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Identification state");
        }

        public void LoanTypeHelper()
        {
            try
            {
                ExplicitWait(3);
                var user = Users.GenNewRandomMember(CustomerType.Random);
                //var memberLoginPage = new MemberLoginPage(Driver, user);
                var applyLoanPage = new ApplyPage(Driver, user);
                applyLoanPage.ClickPersonalLoan();
            }
            catch (Exception)
            {
                Debug.WriteLine("Tried to click personal loan option");
            }


        }

        public void SelectTypeBank()
        {
            WaitForElement(5, Elements.ApplyPage.Select.SelectCheckingForBankType);
            var checkingAccount = Driver.FindElement(By.XPath(Elements.ApplyPage.Select.SelectCheckingForBankType));
            checkingAccount.Click();
            Reporter.LogTestStepForBugLogger(Status.Info,"Click Checking account");
        }
    }
}