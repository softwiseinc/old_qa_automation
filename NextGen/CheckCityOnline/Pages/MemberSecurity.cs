﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class MemberSecurity : BaseApplicationPage
    {
        public void AssertMemberSecurityPageLoaded()
        {
            Assert.IsTrue(IsMemberSecurityPageLoaded, ErrorStrings.MemberSecurityNotLoaded);
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemberSecurity(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.MemberSecurity;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.MemberSecurity;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.MemberSecurity;
        }

        public bool IsMemberSecurityPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Security page loaded successfully");
                Logger.Trace($"Member Security page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void EnterFirstSecurityQuestion(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberSecurity.Input.Question1);
            var questionInput = Driver.FindElement(By.XPath(Elements.MemberSecurity.Input.Question1));
            var questionElement = Driver.FindElement(By.XPath(Elements.MemberSecurity.PageText.Question1));
            AnswerSecurityQuestion(user, questionElement, questionInput);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the answer to the first security question. ");
        }

        public void ClickSubmitButton1()
        {
            WaitForElement(5, Elements.MemberSecurity.Button.Submit1);
            var submitButton1 = Driver.FindElement(By.XPath(Elements.MemberSecurity.Button.Submit1));
            submitButton1.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Submit LinkButton  ");
        }

        public void EnterSecondSecurityQuestion(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.MemberSecurity.Input.Question2);
            var questionInput = Driver.FindElement(By.XPath(Elements.MemberSecurity.Input.Question2));
            var questionElement = Driver.FindElement(By.XPath(Elements.MemberSecurity.PageText.Question2));
            AnswerSecurityQuestion(user, questionElement, questionInput);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the answer to the second security question. ");
        }

        public void ClickSubmitButton()
        {
            WaitForElement(5, Elements.MemberSecurity.Button.Submit2);
            var submitButton2 = Driver.FindElement(By.XPath(Elements.MemberSecurity.Button.Submit2));
            submitButton2.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the Submit LinkButton. ");
        }

        private void AnswerSecurityQuestion(Users.CurrentUser user, IWebElement question, IWebElement input)
        {
            var currentQuestion = question.Text;
            switch (currentQuestion)
            {
                case Elements.MemberSecurity.Attribute.SecurityQuestion.Option1:
                    input.SendKeys(user.SecurityQuestion1);
                    break;
                case Elements.MemberSecurity.Attribute.SecurityQuestion.Option2:
                    input.SendKeys(user.SecurityQuestion2);
                    break;
                case Elements.MemberSecurity.Attribute.SecurityQuestion.Option3:
                    input.SendKeys(user.SecurityQuestion3);
                    break;
                case Elements.MemberSecurity.Attribute.SecurityQuestion.Option4:
                    input.SendKeys(user.SecurityQuestion4);
                    break;
                default:
                    Logger.Debug("Security question was not found");
                    break;
            }
        }

        public void NavigateToMemberSecurityPage(Users.CurrentUser user)
        {
            var lostPasswordPage = new LostPassword(Driver, user);
            lostPasswordPage.NavigateToLostPasswordPage(user);
            lostPasswordPage.EnterDateOfBirth(user);
            lostPasswordPage.ClickSubmitButton();
        }

        public void AnswerSecurityQuestions()
        {
            WaitForElement(5, "//*[@id='ctl00_CustomContent_secCreateSubmit']");
            Driver.FindElement(By.XPath(Elements.Security.Question.One)).SendKeys(Users.SecurityQuestion.One);
            Driver.FindElement(By.XPath(Elements.Security.Question.Two)).SendKeys(Users.SecurityQuestion.Two);
            Driver.FindElement(By.XPath(Elements.Security.Question.Three)).SendKeys(Users.SecurityQuestion.Three);
            Driver.FindElement(By.XPath(Elements.Security.Question.Four)).SendKeys(Users.SecurityQuestion.Four);
            Driver.FindElement(By.XPath(Elements.Security.Question.Submit)).Click();
            ExplicitWait(3);
        }
    }
}