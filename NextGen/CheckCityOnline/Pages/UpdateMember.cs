﻿using System;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class UpdateMember : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public UpdateMember(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UpdateMember;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UpdateMember;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UpdateMember;
        }

        public bool IsUpdateMemberVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateMember.MemberInformation))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Update Member loaded successfully");
                Logger.Trace($"Update Member page is loaded. ");
                return isVisible;
            }
        }
        public bool UpdatePasswordHasChange
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdatePassword.ScreenText.ValidatedPassword))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the screen text password has changed loaded successfully");
                Logger.Trace($"Update screen text password has changed loaded. ");
                return isVisible;
            }
        }


        internal void ClickEditIncome(Users.CurrentUser user)
        {
            var incomeErrorPage = new IncomeErrorPage(Driver,user);
            WaitForElement(5,Elements.UpdateMember.Link.EditIncome);
            Driver.FindElement(By.XPath(Elements.UpdateMember.Link.EditIncome)).Click();
            incomeErrorPage.ValidatePageLoaded();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Edit Income Link. ");
        }

        public void AssertUpdateMemberPageIsVisible()
        {

            WaitForElement(5, Elements.UpdateMember.MemberInformation);
            Assert.IsTrue(IsUpdateMemberVisible, ErrorStrings.UpdateMemberNotVisible);

        }

        public void ClickChangePassword()
        {
            WaitForElement(10, Elements.UpdateMember.LinkButton.ChangePassword);
            var changePassword = Driver.FindElement(By.XPath(Elements.UpdateMember.LinkButton.ChangePassword));
            changePassword.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click change password btn ");
        }

        public void ClickChangeEmail()
        {
            WaitForElement(5, Elements.UpdateMember.LinkButton.ChangeEmail);
            var changeEmailBtn = Driver.FindElement(By.XPath(Elements.UpdateMember.LinkButton.ChangeEmail));
            changeEmailBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on change email button");
        }

        public void ClickChangeEmailCA()
        {
            WaitForElement(5, Elements.UpdateMember.LinkButton.ChangeEmailCA);
            var changeEmailBtn = Driver.FindElement(By.XPath(Elements.UpdateMember.LinkButton.ChangeEmailCA));
            changeEmailBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on change email button");
        }
        public void ClickEditPhone()
        {
           WaitForElement(5, Elements.UpdateMember.Link.EditPhone);
           var phoneEdit = Driver.FindElement(By.XPath(Elements.UpdateMember.Link.EditPhone));
           phoneEdit.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on edit phone ");
        }

        public void ClickEditAddress()
        {
            WaitForElement(5, Elements.UpdateMember.LinkButton.ChangeAddress);
            var changeAddress = Driver.FindElement(By.XPath(Elements.UpdateMember.LinkButton.ChangeAddress));
            changeAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on edit address.");
        }
        public void ClickMailingAddress()
        {
            WaitForElement(5, Elements.UpdateMember.LinkButton.ChangeAddress);
            var changeAddress = Driver.FindElement(By.XPath(Elements.UpdateMember.LinkButton.ChangeAddress));
            changeAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on edit address.");
        }
        public void ClickUpdateProfile()
        {
            WaitForElement(5, Elements.UpdateMember.UpdateButton);
            var changeAddress = Driver.FindElement(By.XPath(Elements.UpdateMember.UpdateButton));
            changeAddress.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Update profile button.");
        }
        public void ClickProfileMenuItem()
        {
            WaitForElement(5, Elements.UpdateMember.ScreenText.MemberProfile);
            var profileMenuItem = Driver.FindElement(By.XPath(Elements.UpdateMember.ScreenText.MemberProfile));
            profileMenuItem.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on profile menu item.");
        }
        

        public void EnterCurrentPassword(Users.CurrentUser user)
        {

            WaitForElement(5, Elements.UpdatePasswordNV.Input.OldPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePasswordNV.Input.OldPassword)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a current password value in the user name input field.");
        }

        public void EnterNewPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.UpdatePassword.Input.NewPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.NewPassword)).SendKeys(user.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a new password value in the user name input field.");
        }

        public void EnterConfirmPassword(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.UpdatePassword.Input.NewPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.ConfirmPassword)).SendKeys(user.NewPassword);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the new password to confirm the value in the New Password Again Field.");
        }

        public void ClickUpdatePassword()
        {
            WaitForElement(5, Elements.UpdatePassword.Button.UpdatePassword);
            var passwordButton = Driver.FindElement(By.XPath(Elements.UpdatePassword.Button.UpdatePassword));
            passwordButton.Click();
           // AssertUpdateMemberPasswordHasChangedIsVisible();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click update password btn ");
        }

        public void AssertUpdateMemberPasswordHasChangedIsVisible()
        {
            WaitForElement(10, Elements.UpdatePassword.ScreenText.ValidatedPassword);
            ExplicitWait(3);
            Assert.IsTrue(UpdatePasswordHasChange, ErrorStrings.UpdateMemberPasswordHasChangeDidNotDisplay);
        }
    }

}