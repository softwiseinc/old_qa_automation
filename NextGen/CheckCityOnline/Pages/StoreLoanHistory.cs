﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using System;
namespace CheckCityOnline.Pages
{
    internal class StoreLoanHistory : BaseApplicationPage

    {
        public StoreLoanHistory(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreLoanHistory;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreLoanHistory;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreLoanHistory;
        }


        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsStoreLoanHistoryVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains("store-loan-history.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store-Loan-History bread crumb visible");
                Logger.Trace($"Store change password page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertStoreLoanHistoryIsVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsStoreLoanHistoryVisible, ErrorStrings.StoreLoanHistoryPageNotLoaded);

        }

        public void ClickTerms()
        {
            WaitForElement(5, Elements.Loan.LoanHistory.Buttons.Terms);
            var termsLink = Driver.FindElement(By.XPath(Elements.Loan.LoanHistory.Buttons.Terms));
            termsLink.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click terms");
        }

        public bool IsLoanDocumentListDropDownVisible
        {
            get
            {
                var isLoaded =
                    Driver.FindElement(By.XPath(Elements.Loan.LoanHistory.LoanDocumentListDropDown.ScreenText)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the store-Loan-History bread crumb visible");
                Logger.Trace($"Store change password page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertLoanDocumentListDropDownVisible()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsLoanDocumentListDropDownVisible, ErrorStrings.LoanDocumentListDropDownNotVisible);

        }


        public void ClickProfile()
        {
            WaitForElement(5, Elements.Loan.LoanHistory.LeftMenu.Profile);
            var profileLink = Driver.FindElement(By.XPath(Elements.Loan.LoanHistory.LeftMenu.Profile));
            profileLink.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Profile on Left Hand Menu");
        }
    }
}
