﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    public class PersonalLoanDisclosure : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public PersonalLoanDisclosure(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PersonalLoanDisclosure;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PersonalLoanDisclosure;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PersonalLoanDisclosure;
        }

        public bool IsPersonalLoanDisclosurePageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the personal loan disclosure page loaded successfully");
                Logger.Trace($"Personal Loan Disclosure page is loaded=>{isLoaded}");
                return isLoaded;
            }

        }

        public void AssertPersonalLoanDisclosure()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.Loans.LoanDisclosure.ScreenText.ReviewAndSign);
            Assert.IsTrue(IsPersonalLoanDisclosurePageLoaded, ErrorStrings.PersonalLoanDisclosurePageNotLoaded);

        }

        public void EnterSignatureAgreementOne()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementOne));
            enterSignature.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature on the loan disclosure section.");
        }

        public void ClickSignatureAgreementOne()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementOne);
            var loanDisclosureButton = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementOne));
            loanDisclosureButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Sign Loan Disclosure button");
        }

        public void EnterSignatureAgreementTwo()
        {
            WaitForElement(5, Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo);
            var enterSignature = Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Input.SignatureAgreementTwo));
            enterSignature.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters signature on the ACH agreement section.");

        }

        public void ClickAgreementTwo()
        {

            WaitForElement(5, Elements.Loans.LoanDisclosure.Button.SignatureAgreementTwo);
            var achAgreementButton =
                Driver.FindElement(By.XPath(Elements.Loans.LoanDisclosure.Button.SignatureAgreementTwo));
            achAgreementButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Sign ACH Agreement button");

        }

        public void AssertLoanDisclosurePageLoaded()
        {
            throw new System.NotImplementedException();
        }
    }
}