﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace CheckCityOnline.Pages
{
    internal class PaymentThankYou : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public PaymentThankYou(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaymentThankYouPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaymentThankYouPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaymentThankYouPage;
        }

        public bool IsPaymentThankYouPageLoaded
        {
            get
            {

                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the 'payment Thank you' page loaded successfully");
                Logger.Trace($"payment thank you page loaded. ");
                return isLoaded;
            }
            
        }

        public void AssertPaymentThankYouPageLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsPaymentThankYouPageLoaded, ErrorStrings.PaymentThankYouPageNotLoaded);
        }

        public void ClickGotItButton()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.PaymentThankYou.Buttons.GotItButton);
            var gotIt = Driver.FindElement(By.XPath(Elements.PaymentThankYou.Buttons.GotItButton));
            gotIt.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on got it button");
        }



    }
}