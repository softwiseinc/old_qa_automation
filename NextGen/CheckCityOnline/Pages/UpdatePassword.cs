﻿using System.Security;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class UpdatePassword : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public UpdatePassword(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UpdateMemberPassword;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UpdateMemberPassword;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UpdateMemberPassword;
        }

        public bool IsUpdatePasswordVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdatePassword.ScreenText.ChangePassword))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the Update Member Password loaded successfully");
                Logger.Trace($"Update Member Password page is loaded. ");
                return isVisible;
            }
        }
        public bool UpdatePasswordHasChange
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdatePassword.ScreenText.ValidatedPassword))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the screen text password has changed loaded successfully");
                Logger.Trace($"Update screen text password has changed loaded. ");
                return isVisible;
            }
        }
        public void AssertUpdatePasswordPageIsVisible()
        {

            WaitForElement(5, Elements.UpdatePassword.ScreenText.ChangePassword);
            Assert.IsTrue(IsUpdatePasswordVisible, ErrorStrings.UpdatePasswordNotVisible);

        }

        public void EnterCurrentPassword(Users.CurrentUser user)
        {

            WaitForElement(5, Elements.UpdatePassword.Input.CurrentPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.CurrentPassword)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a current password value in the user name input field.");
        }

        public void EnterNewPassword(string password)
        {
            WaitForElement(5, Elements.UpdatePassword.Input.NewPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.NewPassword)).SendKeys(password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a new password value in the user name input field: " + password);
        }

        public void EnterConfirmPassword(string password)
        {
            WaitForElement(5, Elements.UpdatePassword.Input.ConfirmPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.ConfirmPassword)).SendKeys(password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the new password to confirm the value in the New Password Again Field: " + password);
        }

        public void EnterCurrentPassword(string password)
        {

            WaitForElement(5, Elements.UpdatePassword.Input.CurrentPassword);
            Driver.FindElement(By.XPath(Elements.UpdatePassword.Input.CurrentPassword)).SendKeys(password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a current password value in the user name input field: " + password);
        }
        
        public void ClickUpdatePassword()
        {
            WaitForElement(5, Elements.UpdatePassword.Button.UpdatePassword);
            var passwordButton = Driver.FindElement(By.XPath(Elements.UpdatePassword.Button.UpdatePassword));
            passwordButton.Click();
            AssertUpdateMemberPasswordHasChangedIsVisible();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click update password btn ");

        }

        
        public void AssertUpdateMemberPasswordHasChangedIsVisible()
        {
            WaitForElement(10, Elements.UpdatePassword.ScreenText.ValidatedPassword);
            ExplicitWait(3);
            Assert.IsTrue(UpdatePasswordHasChange, ErrorStrings.UpdateMemberPasswordHasChangeDidNotDisplay);
        }

        public void ClickLogout()
        {
            WaitForElement(5, Elements.UpdatePassword.Button.Logout);
            var logoutButton = Driver.FindElement(By.XPath(Elements.UpdatePassword.Button.Logout));
            logoutButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click logout btn ");
        }


        public void ResetToPreviousPassword(Users.CurrentUser user)
        {
            var memberLoginPage = new MemberLoginPage(Driver, user);
            var memberPage  = new MemberPage(Driver, user);
            var storeProfile = new StoreProfile(Driver, user);
            memberPage.ClickUserNameMenu();
            memberPage.ClickProfile();
            storeProfile.ClickChangePassword();
            AssertUpdatePasswordPageIsVisible();
            EnterCurrentPassword(user.NewPassword);
            EnterNewPassword(user.Password);
            EnterConfirmPassword(user.Password);
            ClickUpdatePassword();
        }
        public void LoginUsingNewPassword(Users.CurrentUser user)
        {
            var memberLoginPage = new MemberLoginPage(Driver, user);
            var memberPage = new MemberPage(Driver, user);
            memberPage.LogOut();
            memberLoginPage.LogInWithNewPassword(user);
            memberPage.AssertMemberDashboardLoaded();
        }
    }
}
