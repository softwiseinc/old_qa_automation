﻿using System;
using System.Collections.Generic;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    public class LoanApprovalPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public LoanApprovalPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanApproval;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanApproval;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanApproval;
        }

        public bool IsLoanApprovalPageVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.LoanApproval.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the personal loan approval page loaded successfully");
                Logger.Trace($"Personal Loan Approval page is loaded=>{isVisible}");
                return isVisible;
            }
        }

        public bool IsLoanTurnDownLeadPageVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.LoanApproval.ScreenText.TurnDownLead))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the personal loan approval turn down lead page loaded successfully");
                Logger.Trace($"Personal Loan Approval page is loaded=>{isVisible}");
                return isVisible;
            }
        }
        
        public void AssertLoanApprovalPage()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.LoanApproval.ScreenText);
            Assert.IsTrue(IsLoanApprovalPageVisible, ErrorStrings.PersonalLoanApprovalPageNotVisible);
        }
        
        public bool IsRefinancePersonalLoanApprovalVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.LoanApproval.ScreenText.RefinanceLoanDetails))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the personal loan approval page loaded successfully");
                Logger.Trace($"Personal Loan Approval page is loaded=>{isVisible}");
                return isVisible;
            }
        }
        
        public void AssertRefinanceLoanApproval()
        {
            ExplicitWait(7);
            WaitForElement(5, Elements.Loans.LoanApproval.ScreenText.RefinanceLoanDetails);
            Assert.IsTrue(IsRefinancePersonalLoanApprovalVisible, ErrorStrings.RefinancePersonalLoanApprovalNotVisible);
        }


        public void AssertPersonalLoanTurnDownLead()
        {
            WaitForElement(20, Elements.Loans.LoanApproval.ScreenText.TurnDownLead);
            Assert.IsTrue(IsLoanTurnDownLeadPageVisible, ErrorStrings.PersonalLoanApprovalPageNotVisible);
        }
        public bool IsPersonalLoanTurnDownVisible
        {
            get
            {
                var isVisible = Driver
                    .FindElement(By.XPath(Elements.Loans.LoanApproval.ScreenText.TurnDown)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the personal loan approval Turn down page loaded successfully");
                Logger.Trace($"Personal Loan Approval page is loaded=>{isVisible}");
                return isVisible;
            }
        }

        public void AssertPersonalLoanTurnDown()
        {
            WaitForElement(5, Elements.Loans.LoanApproval.ScreenText.TurnDown);
            Assert.IsTrue(IsPersonalLoanTurnDownVisible, ErrorStrings.PersonalLoanApprovalPageNotVisible);
        }


        public void AssertStoreOptionHasLoaded()
        {
            WaitForElement(10, Elements.LoanApproval.FinishYourApp);
            Assert.IsTrue(IsStoreOptionVisible, ErrorStrings.IsStoreOptionIsNotVisible);
        }
        public bool IsStoreOptionVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.LoanApproval.FinishYourApp))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
                Logger.Trace("Member page is loaded. ");
                return isVisible;
            }
        }
        public void ClickInStoreOption()
        {
            WaitForElement(5, Elements.LoanApproval.Buttons.InStore);
            var inStoreGetCash = Driver.FindElement(By.XPath(Elements.LoanApproval.Buttons.InStore));
            inStoreGetCash.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Store Option ");
            ExplicitWait(4);
        }

        public void EnterLoanAmount()
        {
            WaitForElement(10, Elements.Loans.LoanApproval.Input.EnterAmount);
            var amountEntered = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Input.EnterAmount));
            amountEntered.SendKeys(PersonalLoanAmount.Medium);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter amount for loan");
        }
        public void EnterLoanAmount(int loanAmount)
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.Loans.LoanApproval.Input.EnterAmount);
            var amountEntered = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Input.EnterAmount));
            amountEntered.SendKeys(loanAmount.ToString());
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter amount for loan");
        }
        public void ClickGetAmount()
        {
            WaitForElement(5, Elements.Loans.LoanApproval.Button.GetAmount);
            var clickGetAmountButton = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Button.GetAmount));
            clickGetAmountButton.Click();
            ExplicitWait(5);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on get amount button");

        }

        public void Extend()
        {
            WaitForElement(5, Elements.Loans.LoanApproval.Button.Extend);
            var extendButton = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Button.Extend));
            extendButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click extend button");
        }

        //personal loan paying down 
        public void EnterPayDownAmount()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.Loans.LoanApproval.Input.EnterPayDownAmount);
            var payDownAmount =
                Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Input.EnterPayDownAmount));
            payDownAmount.Click();
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the pay down input field");
            payDownAmount.SendKeys(PayDownAmount.Minimum);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter pay down amount. ");
        }

        //Personal loan borrow more amount 
        public void EnterNewLoanAmount()
        {
            WaitForElement(5, Elements.Loans.LoanApproval.Input.NewLoanAmount);
            var newLoanAmount = Driver.FindElement(By.Id("newLoanAmount"));
            newLoanAmount.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "click on the input field");
            ExplicitWait(2);
            newLoanAmount.SendKeys(PersonalLoanAmount.Minimum);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "enter the new amount to add to current loan");
        }

        //Personal Loan PayDown using Checking account on file 
        public void ClickUpdate()
        {
            WaitForElement(5, Elements.Loans.LoanApproval.Button.Update);
            var achCheckBox = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Button.Update));
            achCheckBox.Click();
            ExplicitWait(7);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click yes to using checking account on file.");
        }
        
        public void ClickContinue()
        {
            ExplicitWait(7);
            WaitForElement(5, Elements.Loans.LoanApproval.Button.Continue);
            var clickContinue = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Button.Continue));
            clickContinue.Click();
            ExplicitWait(7);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Continue");
        }

        
        public void ClickOnlineButton()
        {
            WaitForElement(5, Elements.LoanApproval.Buttons.Online);
            var onlineButton = Driver.FindElement(By.XPath(Elements.LoanApproval.Buttons.Online));
            onlineButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Online Button");
        }

        public void ClickLogout()
        {
            WaitForElement(5, Elements.LoanApproval.Link.Logout);
            Driver.FindElement(By.XPath(Elements.LoanApproval.Link.Logout)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Logout Link in Header");
        }

        public void ClickAccountLink()
        {
            WaitForElement(5, Elements.LoanApproval.Link.Logout);
            Driver.FindElement(By.XPath(Elements.LoanApproval.Link.Logout)).Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Logout Link in Header");
        }

        public bool TryFinishYourApplication()
        {
            bool isFinishApp = false;
            try
            {
                if (Driver.FindElement(By.XPath(Elements.LoanApproval.FinishYourApp)).Displayed)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "Finish Your Application page is displayed");
                    isFinishApp = true;
                }
            }
            catch (Exception)
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "Finish Your Application page is not displayed");
            }

            return isFinishApp;
        }

        public bool GenerateCustomerStatusHelper(IWebDriver driver, Users.CurrentUser customer)
        {
            if (driver.Url.Contains("apply.aspx"))
                return false;
            bool isCustomerCreated = false;
            switch (customer.LastName)
            {
                case "Turndown Lead":
                    isCustomerCreated = driver.PageSource.Contains(Elements.LoanApproval.ScreenTextOnPage.DeclinedAtThisTime);
                    break;
                case "Turndown":
                    isCustomerCreated = driver.PageSource.Contains(Elements.LoanApproval.ScreenTextOnPage.TurnedDown);
                    break;
                case "Pending All":
                    isCustomerCreated =
                        (driver.PageSource.Contains(Elements.LoanApproval.ScreenTextOnPage.YourAlmostDone) ||
                         driver.PageSource.Contains(Elements.LoanApproval.ScreenTextOnPage.FinishYourApplication));
                    break;
                case "Approved":
                    isCustomerCreated = driver.PageSource.Contains(Elements.LoanApproval.ScreenTextOnPage.Congratulations);
                    break;
            }

            return isCustomerCreated;
        }

        public void ClickAccount()
        {
            WaitForElement(5, Elements.LoanApproval.Link.Account);
            var clickAccount = Driver.FindElement(By.XPath(Elements.LoanApproval.Link.Account));
            clickAccount.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click account link");
        }

        public void EnterNevadaLoanAmount(int loanAmount)
        {
            ExplicitWait(5);
            WaitForElement(5, Elements.Loans.LoanApproval.Input.NevadaLoanAmount);
            var amountEntered = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.Input.NevadaLoanAmount));
            amountEntered.Clear();
            amountEntered.SendKeys(loanAmount.ToString());
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Nevada Loan Amount");
        }
    }
}