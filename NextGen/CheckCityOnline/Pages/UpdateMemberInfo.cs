﻿using System;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Pages
{
    internal class UpdateMemberInfo : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public UpdateMemberInfo(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.UpdateMemberInfo;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.UpdateMemberInfo;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.UpdateMemberInfo;
        }

        public bool IsMemberInfoUpdateVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.ScreenText.UpdateInfoBreadCrumb))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the update info bread crumb is visible. ");
                Logger.Trace("update info bread crumb text is visible. ");
                return isVisible;
            }
        }

        public bool IsMemberInfoVisible {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.ScreenText.UpdateInfoText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate that info Member info text is visible. ");
                Logger.Trace("member info text is visible. ");
                return isVisible;
            }
        }
        public bool IsPhoneNumberVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.ScreenText.PhoneNumber))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the SMS was sent to google voice number 801-893-2295");
                Logger.Trace("Validate the SMS was sent to google voice number 801-893-2295");
                return isVisible;
            }
        }
        public void AssertMemberInfoUpdateIsVisible()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.ScreenText.UpdateInfoBreadCrumb);
            Assert.IsTrue(IsMemberInfoUpdateVisible, ErrorStrings.UpdateMemberInfoIsNotVisible);
        }

        public void AssertMemberInfoIsVisible()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.ScreenText.UpdateInfoText);
            Assert.IsTrue(IsMemberInfoVisible, ErrorStrings.UpdateMemberInfoIsNotVisible);
        }

        public void NewPhoneNumber()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.Phone);
            var newNumber = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.Phone));
            newNumber.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clear current phone number. ");
            newNumber.SendKeys(Users.User.PhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "New phone number is entered");
        }
        public void UpdatePhoneNumber()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.NewPhone);
            var newNumber = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.NewPhone));
            newNumber.Clear();
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clear current phone number. ");
            newNumber.SendKeys("‪8018932295");
            ExplicitWait(2);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "New phone number is entered");
            var sendCodeButton = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Button.SendCode));
            sendCodeButton.Click();
            ExplicitWait(2);
            Assert.IsTrue(IsPhoneNumberVisible, "ERROR: The SMS was NOT sent to google voice number 801-893-2295");
            var saveChanges = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Button.SaveChanges));
            saveChanges.Click();
            ExplicitWait(2);
        }


       

        public void ClickSubmit()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Button.Submit);
            var submitBtn = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Button.Submit));
            submitBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Submit button. ");
        }
        public void ClickMembersSubmit()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Button.MembersSubmit);
            var submitBtn = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Button.MembersSubmit));
            submitBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Submit button. ");
        }

        public void UpdateAddress()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.Address);
            var enterAddress = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.Address));
            enterAddress.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current address field ");
            enterAddress.SendKeys(Users.User.Address);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's address");
        }

        public void UpdateMembersAddress()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersAddress);
            var enterAddress = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersAddress));
            enterAddress.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current address field ");
            //enterAddress.SendKeys(Users.User.Address);
            enterAddress.SendKeys("ApprovedPersonal Lane #45");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer's address");
        }


        public void UpdateCity()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.City);
            var enterCity = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.City));
            enterCity.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clear the current city in the field");
            enterCity.SendKeys(Users.User.City);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter City that the customer is in ");
        }

        public void UpdateMembersCity()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersCity);
            var enterCity = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersCity));
            enterCity.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clear the current city in the field");
            enterCity.SendKeys("Salt Lake City");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter City that the customer is in ");
        }

        public void UpdateState()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.State);
            var state = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.State));
            var selectElement = new SelectElement(state);
            selectElement.SelectByText(Users.User.State);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select state the customer is in.");
        }
        public void UpdateMembersState()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersState);
            var state = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersState));
            //state.Clear();
            //Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the state field. ");
            state.SendKeys("UT");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter State.");
        }

        public void UpdateZipCode()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.ZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.ZipCode));
            enterZipCode.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current zip code field. ");
            enterZipCode.SendKeys(Users.User.ZipCode);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Zip code ");
        }
        public void UpdateMembersZipCode()
        {
            WaitForElement(5, Elements.UpdateMemberInfo.Input.MembersZipCode);
            var enterZipCode = Driver.FindElement(By.XPath(Elements.UpdateMemberInfo.Input.MembersZipCode));
            enterZipCode.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clears the current zip code field. ");
            enterZipCode.SendKeys("84121");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter Zip code ");
        }
    }
}