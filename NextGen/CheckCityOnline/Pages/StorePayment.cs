﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class StorePayment : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StorePayment(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StorePayment;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StorePayment;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StorePayment;
        }

        public bool IsStorePaymentLoaded
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.PayDayLoan.StorePayments.MakePaymentHeadline)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'Store Payment' page loaded successfully");
                Logger.Trace("'In Store Payment' page is loaded. ");
                return isVisible;
            }
        }

        public void AssertStorePaymentLoaded()
        {
            ExplicitWait(3);
            Assert.IsTrue(IsStorePaymentLoaded, ErrorStrings.StorePaymentPageNotLoaded);
        }

        public string FindPayOffAmount()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.ScreentText.PayOffAmount);
            string amount = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.ScreentText.PayOffAmount)).Text;
            amount = amount.Substring(1);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Loan Payoff amount is found and store as a variable");
            return amount;
        }

        public void EnterPayoffAmount(string amount)
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.PaymentAmount.EnterAmount);
            Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.PaymentAmount.EnterAmount)).SendKeys(amount);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters the full amount to pay off the loan.");
        }

        public void SelectPaymentMethodAch()
        {
           WaitForElement(5, Elements.Loans.StoreInstallmentPayment.SelectYourPaymentMethod.Ach);
           var paymentMethodAch =
               Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.SelectYourPaymentMethod.Ach));
           paymentMethodAch.Click();

           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the payment method");
        }

        public void SelectPaymentAch()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.Button.AchRadio);
            var paymentMethodAch =
                Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.Button.AchRadio));
            paymentMethodAch.Click();

            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the payment method");
        }

        

        public void ClickNextButton()
        {
            WaitForElement(5, Elements.PayDayLoan.StorePayments.Next);
            var continueButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.StorePayments.Next));
            continueButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click continue button");
        }

        public void EnterPaymentAmount()
        {
           WaitForElement(5, Elements.PayDayLoan.StorePayments.PaymentAmount);
            Driver.FindElement(By.XPath(Elements.PayDayLoan.StorePayments.PaymentAmount)).SendKeys(PrincipalPayment.Medium);
        }
    }
}
