﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class EfpAdverseAction : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public EfpAdverseAction(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EfpAdverseActionPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EfpAdverseActionPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.EfpAdverseActionPage;
        }

        public bool IsEfpAdverseActionPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Efp Login page loaded successfully");
                Logger.Trace($"Efp login Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }


        public void AssertEfpAdverseActionPageLoaded()
        {
           Assert.IsTrue(IsEfpAdverseActionPageLoaded, ErrorStrings.EfpAdverseActionPageNotLoaded);
        }


        public void SelectCustomerState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.StateDropDownList.StateUtah);
            var stateDropDown = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage
                .StateDropDownList.StateUtah));
            stateDropDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click state drop down select Utah");
        }


        
        public void ClickLookupCustomerId()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.Buttons.LookupCustomer);
            var lookupCustomerButton = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpLoanApprovalPage
                .EfpLoanApprovalTabs.AdverseActionsOptions.Buttons.LookupCustomer));
            lookupCustomerButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on lookup button");
        }

        public void ClickTurnDownAdversePDFLink()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.AdverseActionTable.AdverseActionPDF.AdverseActionPdf);
            var adverseActionPdf = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage
                .AdverseActionTable.AdverseActionPDF.AdverseActionPdf));
            adverseActionPdf.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Adverse Action PDF is Visible");
        }



        public bool IsEfpAdverseActionPdfVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.AdverseActionTable.AdverseActionPDF.AdverseActionPdf)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Efp Adverse Action PDF is Visible");
                Logger.Trace($"Efp adverse action pdf is visible=>{isVisible}");
                return isVisible;
            }
        }
        
        
        public void EfpAssertAdverseActionPdfIsVisible()
        {
            
            Assert.IsTrue(IsEfpAdverseActionPdfVisible, ErrorStrings.EfpAdverseActionPdfNotVisible);



        }

        public void EnterCustomerId(string customerId)
        {
           WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.InputFields.CustomerID);
           var customerIdInputField =
               Driver.FindElement(
                   By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpAdverseActionPage.InputFields.CustomerID));
           customerIdInputField.SendKeys(customerId);
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter customer ID");
        }
    }
}
