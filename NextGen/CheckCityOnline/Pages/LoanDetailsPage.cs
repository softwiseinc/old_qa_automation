﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    public class LoanDetailsPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public LoanDetailsPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.LoanDetails;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.LoanDetails;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.LoanDetails;
        }

        public bool IsLoanDetailsPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the personal loan details page loaded successfully");
                Logger.Trace($"Personal Loan Details page is loaded=>{isLoaded}");
                return isLoaded;
            }

        }
        
        public void AssertLoanDetails()
        {
            WaitForElement(15, Elements.Loans.PersonalLoanDetails.ScreenText.LoanDetails);
            Assert.IsTrue(IsLoanDetailsPageLoaded, ErrorStrings.LoanDetailsPageNotLoaded);

        }

        public void ClickContinue()
        {
            WaitForElement(15, Elements.Loans.PersonalLoanDetails.Button.Continue);
            var clickContinue = Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.Continue));
            clickContinue.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Get Amount Now button");
        }

        public void ClickContinue(int waitTime)
        {
            ExplicitWait(waitTime);
            WaitForElement(5, Elements.Loans.PersonalLoanDetails.Button.Continue);
            
            var clickContinue = Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.Continue));
            clickContinue.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Get Amount Now button");
        }
        //Personal loan pay down btn click
        public void ClickPayAmountNow()
        {
            
            WaitForElement(5, Elements.Loans.PersonalLoanDetails.Button.PayAmountNow);
            var btnPayNow = Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.PayAmountNow));
            btnPayNow.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click pay amount now btn ");
        }

        public void ClickPencilToEditLoanAmount()
        {
           WaitForElement(3, Elements.Loans.PersonalLoanDetails.Button.EditLoanAmount);
           var editLoanAmount =
               Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.EditLoanAmount));
           editLoanAmount.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click pencil to edit amount requested");
        }

        public void EnterNewLoanAmount()
        {
            WaitForElement(3, Elements.Loans.PersonalLoanDetails.InputField.AmountField);
            var amountField =
                Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.InputField.AmountField));
            amountField.SendKeys(Users.User.EnterNewLoanAmount);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new amount");
        }

        public void EnterNewPersonalLoanAmount()
        {
            WaitForElement(3, Elements.Loans.PersonalLoanDetails.InputField.AmountField);
            var amountField =
                Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.InputField.AmountField));
            amountField.SendKeys("100");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new amount");
        }

        public void EnterNewLoaningAmount()
        {
            WaitForElement(3, Elements.Loans.PersonalLoanDetails.InputField.AmountField);
            var amountField =
                Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.InputField.AmountField));
            amountField.SendKeys("475");
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new amount");
        }
        public void ClickConfirmNewLoanAmount()
        {
           WaitForElement(3,Elements.Loans.PersonalLoanDetails.Button.ConfirmNewLoanAmount);
           var confirmNewLoanAmount =
               Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.ConfirmNewLoanAmount));
           confirmNewLoanAmount.Click();
        }

        public void ClickExtendNow()
        {
           WaitForElement(5, Elements.Loans.PersonalLoanDetails.Button.ExtendNow);
           var extendNow = Driver.FindElement(By.XPath(Elements.Loans.PersonalLoanDetails.Button.ExtendNow));
           extendNow.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Extend Now");
        }
    }
}

