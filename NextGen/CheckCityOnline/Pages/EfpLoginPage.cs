﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;


namespace CheckCityOnline.Pages
{
    internal class EfpLoginPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public EfpLoginPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EFPloginPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EFPLoginPage;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.EFPloginPage;
        }

        public bool IsEfpLoginPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("efplogin.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Efp Login page loaded successfully");
                Logger.Trace($"Efp login Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        


        
        public void LogInUser(Users.CurrentUser user)
        {
            NavigateToEfpLoginPage();
            EnterUserName(user);
            EnterPassWord(user);
            //ClickState();
            //SelectUtahAsState();
            ClickLogin();
        }

        public void ClickState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.DropDown.State);
            var stateDropdown = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.DropDown.State));
            stateDropdown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click State ");
        }

        public void SelectUtahAsState()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.DropDown.DropdownOptions.Utah);
            var stateOption = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.DropDown.DropdownOptions.Utah));
            stateOption.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Utah as the state");
        }
        public void NavigateToEfpLoginPage()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            Assert.IsTrue(IsEfpLoginPageLoaded, ErrorStrings.EFPPageNotLoaded);
        }
        private void EnterUserName(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.EfpPageLoginPage.InputFields.UserID);
            var usernameInputField = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.InputFields.UserID));
            usernameInputField.SendKeys(Users.User.UserName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter user name");
        }
        private void EnterPassWord(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.EfpPageLoginPage.InputFields.Password);
            var usernamePasswordInputField =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.InputFields.Password));
            usernamePasswordInputField.SendKeys(Users.User.Password);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the users password");
        }

        private void ClickLogin()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.Buttons.Login);
            var loginButton = Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.Buttons.Login));
            loginButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on login button");
        }

    }
}