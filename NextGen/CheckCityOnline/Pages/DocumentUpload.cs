﻿using System.Threading;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace CheckCityOnline.Pages
{
    internal class DocumentUpload : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public DocumentUpload(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DocumentUpload;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DocumentUpload;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DocumentUpload;
        }

        public bool IsDocumentUploadPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the document upload page loaded. ");
                Logger.Trace($"document upload page haas loaded=>{ isLoaded}");
                return isLoaded;

            }
        }

        public void AssertDocumentUpload()
        {
            ExplicitWait(3);
            WaitForElement(10, Elements.Loans.DocumentUpload.ScreenText.AlmostDone);
            Assert.IsTrue(IsDocumentUploadPageLoaded, ErrorStrings.DocumentUploadPageNotLoaded);
        }


        public void ClickUploadDocument()
        {
            WaitForElement(5, Elements.Loans.DocumentUpload.Button.Browse);
            var upload = Driver.FindElement(By.XPath(Elements.Loans.DocumentUpload.Button.Browse));
            //upload.SendKeys(@"C:\Users\rpalmer\Pictures\alien");
            upload.Click();

        }

        public void ClickSubmit()
        {
            WaitForElement(10, Elements.Loans.DocumentUpload.Button.Submit);
            var submit = Driver.FindElement(By.XPath(Elements.Loans.DocumentUpload.Button.Submit));
            submit.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click button");
        }

        public void ClickAccount()
        {
            WaitForElement(5, Elements.Loans.DocumentUpload.Button.AccountLink);
            var accountButton = Driver.FindElement(By.XPath(Elements.Loans.DocumentUpload.Button.AccountLink));
                accountButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the account link next to logout link ");
        }
    }
}
