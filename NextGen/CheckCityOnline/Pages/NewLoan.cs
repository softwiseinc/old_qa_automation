﻿using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Tests
{
    internal class NewLoan : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public NewLoan(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.NewLoan;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.NewLoan;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.NewLoan;
        }

        public bool IsNewLoanPageLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the New Loan page loaded successfully");
                Logger.Trace($"New Loan page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public void AssertNewLoanPageLoaded()
        {
            WaitForElement(5, Elements.NewLoan.EnterLoanAmount.LoanAmount);
            Assert.IsTrue(IsNewLoanPageLoaded, ErrorStrings.NewLoanPageNotLoaded);

        }

        public void EnterLoanAmount()
        {
            WaitForElement(5, Elements.NewLoan.EnterLoanAmount.LoanAmount);
            var enterAmount = Driver.FindElement(By.XPath(Elements.NewLoan.EnterLoanAmount.LoanAmount));
            enterAmount.Clear();
            enterAmount.SendKeys(LoanAmount.Medium);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The existing amount was cleared then the new amount is entered. ");
        }

        public void SelectBankAccount()
        {
            WaitForElement(5, Elements.NewLoan.MakePaymentFrom.CheckingAccount);
            var bankInfo = Driver.FindElement((By.XPath(Elements.NewLoan.MakePaymentFrom.CheckingAccount)));
            var selectElement = new SelectElement(bankInfo);
            selectElement.SelectByText(Users.User.CascadesBank);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The correct checking account is selected.  ");

        }

        public void EnterNameSignature()
        {
            WaitForElement(5, Elements.NewLoan.Input.SignHere);
            var signatureInput = Driver.FindElement(By.XPath(Elements.NewLoan.Input.SignHere));
            signatureInput.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Customer enters their name. ");
        }


        public void ClickIAgreeContinue()
        {
            WaitForElement(5, Elements.NewLoan.Button.ClickIAgreeContinueButton);
            var clickButton = Driver.FindElement(By.XPath(Elements.NewLoan.Button.ClickIAgreeContinueButton));
            clickButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the I agree button. ");

        }
    }
}