﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using AventStack.ExtentReports;
using CheckCityOnline.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace CheckCityOnline.Pages
{
    internal class ExtensionType : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        //private object enterSevenDayExtensionDate;

        public ExtensionType(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.ExtensionType;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.ExtensionType;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.ExtensionType;
        }

        public bool ExtensionTypeScreenTextVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ExtensionType.ScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Extend loan is visible on the extension type page. ");
                Logger.Trace($"Extend loan is visible =>{ isVisible}");
                return isVisible;
            }
        }


        public void AssertExtensionType()
        {
            WaitForElement(20, Elements.ExtensionType.ScreenText);
            Assert.IsTrue(ExtensionTypeScreenTextVisible, ErrorStrings.ExtensionTypeScreenTextNotVisible);
        }

        public void PickDesiredDueDate()
        {
            ExplicitWait(3);
            string myxpath;
            myxpath = "//*[@class='calweek'][1]/a";
            IWebElement targetpath;
            targetpath = null;

            List<IWebElement> elementList = new List<IWebElement>();
            if (AutomationResources.Helper.Page.ElementVisibilityState(Driver, myxpath))
            {
                for (int i = 1; i < 6; i++)
                {
                    myxpath = "//*[@class='calweek'][" + i + "]/a";
                    var calDays = Driver.FindElements(By.XPath(myxpath));
                    foreach (var day in calDays)
                    {
                        try
                        {
                            day.Click();
                        }
                        catch (Exception)
                        {
                            //Debug.WriteLine("try next");
                        }
                    }
                }
            }
        }

        public void AchPaymentMethod()
        {
            WaitForElement(5, Elements.ExtensionType.PaymentMethodACH);
            var paymentMethodAch = Driver.FindElement(By.XPath(Elements.ExtensionType.PaymentMethodACH));
            paymentMethodAch.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select ACH for the payment method");
        }

        public void ClickContinue()
        {
            WaitForElement(5, Elements.ExtensionType.Continue);
            var button = Driver.FindElement(By.XPath(Elements.ExtensionType.Continue));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click continue ");

        }

        public void ClickCallNextToChangeTheMonth()
        {
            WaitForElement(5, Elements.ExtensionType.CallNext);
            var callNext = Driver.FindElement(By.XPath(Elements.ExtensionType.CallNext));
            callNext.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Call Next button");
        }

        public void ClickDateField()
        {
            WaitForElement(5, Elements.ExtensionType.DateField);
            var dateField = Driver.FindElement(By.XPath(Elements.ExtensionType.DateField));
            dateField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click date field");

        }

        public void ClickPaymentFrequency()
        {
            WaitForElement(5, Elements.ExtensionType.PayFrequency);
            var payFrequency = Driver.FindElement(By.XPath(Elements.ExtensionType.PayFrequency));
            payFrequency.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on pay Frequency");
        }

        public void SelectPayPeriod()
        {
            WaitForElement(5, Elements.ExtensionType.PayPeriod);
            var payPeriod = Driver.FindElement(By.XPath(Elements.ExtensionType.PayPeriod));
            payPeriod.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select Pay Period");
        }


        public void ClickNext()
        {
            WaitForElement(5, Elements.ExtensionType.ClickNext);
            var clickNextButton = Driver.FindElement(By.XPath(Elements.ExtensionType.ClickNext));
            clickNextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Next button");
        }


        public bool PaymentPlanTextVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.ExtensionType.PaymentPlanScreenText))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Extend loan is visible on the extension type page. ");
                Logger.Trace($"Extend loan is visible =>{ isVisible}");
                return isVisible;
            }
        }
        
        public void AssertPaymentPlanOptionIsVisible()
        {

            Assert.IsTrue(PaymentPlanTextVisible, ErrorStrings.PaymentPlanIsNotVisible);


        }

        public void ClickSelectDateButton()
        {
            WaitForElement(5, Elements.ExtensionType.SelectDateButton);
            var dateButton = Driver.FindElement(By.XPath(Elements.ExtensionType.SelectDateButton));
            dateButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the select date button");

        }
    }




}
