﻿using System.Diagnostics;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{

    internal class StoreInstallmentPayment : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public StoreInstallmentPayment(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreInstallmentPayment;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreInstallmentPayment;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreInstallmentPayment;
        }

        public bool IsStoreCustomerInstallmentPaymentVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.ScreentText.StoreCustomerInstallmentPayment)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, " Validate the message 'Store Customer Installment Payment' is displayed. ");
                Logger.Trace($"Store Customer Installment Payment Page is loaded=>{isVisible}");
                return isVisible;
            }
        }


        public void AssertStoreCustomerInstallmentPayment()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.ScreentText.StoreCustomerInstallmentPayment);
            ExplicitWait(3);
            Assert.IsTrue(IsStoreCustomerInstallmentPaymentVisible, ErrorStrings.StoreInstallmentPaymentPageNotLoaded);
        }

        public void SelectAchPaymentMethod()
        {
            var button = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.SelectYourPaymentMethod.Ach));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User selects the payment method to be by his account on file(Ach). ");
        }

        public string FindPayOffAmount()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.ScreentText.PayOffAmount);
            string amount = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.ScreentText.PayOffAmount)).Text;
            amount = amount.Substring(1);
            Debug.WriteLine(amount.Length.ToString());
            if (amount.Length > 6)
                amount = "999";
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Loan Payoff amount is found and store as a variable");
            return amount;

        }
        
        public void EnterPaymentAmount(string amount)
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.PaymentAmount.EnterAmount);
            Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.PaymentAmount.EnterAmount)).SendKeys(amount);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User enters the full amount to pay off the loan.");
        }

        public void ClickContinue()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.Button.Continue);
            var button = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.Button.Continue));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks on continue. ");

        }

        public void ClickAchPaymentMethod()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.Button.PaymentMethodAch);
            var achButton =
                Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.Button.PaymentMethodAch));
            achButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Current checking account on file is selected");
        }

        public void PaymentAmount()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.Input.PaymentAmount);
            Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.Input.PaymentAmount)).SendKeys(PrincipalPayment.Minimum);

        }

        public void ClickContinueButton()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.Button.Continue);
            var continueButton = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.Button.Continue));
            continueButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click continue btn ");
        }


        public void SelectDebitCardMethod()
        {
            WaitForElement(5, Elements.Loans.StoreInstallmentPayment.SelectYourPaymentMethod.Debit);
            var button = Driver.FindElement(By.XPath(Elements.Loans.StoreInstallmentPayment.SelectYourPaymentMethod.Debit));
            button.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,
                "User selects the payment method to be by his account on file(debit). ");
        }
    }
}
