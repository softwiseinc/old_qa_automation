﻿
using System;
using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace CheckCityOnline.Pages
{
    internal class PaydayExtension : BaseApplicationPage
    {

        public PaydayExtension(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaydayExtension;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaydayExtension;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaydayExtension;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsPaydayExtensionVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayExtension.ScreenText.StoreCustomerExtension))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Extend loan bread crumb is visible ");
                Logger.Trace($"PayDay Extension page is loaded. ");
                return isVisible;
            }
        }

        public void AssertPaydayExtensionIsVisible()
        {
            WaitForElement(10, Elements.PayDayLoan.PaydayExtension.ScreenText.StoreCustomerExtension);
            Assert.IsTrue(IsPaydayExtensionVisible, ErrorStrings.PaydayExtensionNotVisible);

        }

        public void SelectPaymentOption()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayExtension.PaymentInformation.PaymentFromChecking);
            var bankInfo = Driver.FindElement(By.XPath(xpathToFind: Elements.PayDayLoan.PaydayExtension.PaymentInformation.PaymentFromChecking));
            bankInfo.Click();
            //var selectElement = new SelectElement(element: bankInfo);
            //selectElement.SelectByText(text: Users.User.CascadesBank);
            Reporter.LogPassingTestStepToBugLogger(info: Status.Info, message: "The correct checking account is selected.  ");

        }

        public void ClickContinue()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayExtension.Button.Continue);
            var continueButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayExtension.Button.Continue));
            
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click the continue button ");
        }

        public void EnterPrincipalPayment()
        {
            WaitForElement(10, Elements.PayDayLoan.PaydayExtension.Input.PrincipalPayment);
            var optionalPayment =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayExtension.Input.PrincipalPayment));
            optionalPayment.Clear();
            optionalPayment.SendKeys(PrincipalPayment.Minimum);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Principal payment amount is entered into this field ");

        }

        public void EnterDate()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayExtension.Input.DesiredDueDate);
            var desiredDueDate = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayExtension.Input.DesiredDueDate));
            var date = DateTime.Today.AddDays(7);
            string strDateOnly = date.ToString("d");
            desiredDueDate.SendKeys(strDateOnly);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "The new due date will be in 7 days");
        }
    }
}
