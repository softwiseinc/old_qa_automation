﻿using System;
using System.Linq;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CheckCityOnline.Pages
{
    internal class EFPPage : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public EFPPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
             if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.EfpPage;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.EfpPage;
            else if (user.RunEnvironment == "QA")
                 TestEnvironment.Url = Url.Test.EfpPage;
        }

        
        public bool IsEfpBreadCrumbVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains("efp_loanapproval.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the efp page loaded");
                Logger.Trace($"efp page loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        
        public void AssertEfpPageLoaded()
        {
            
            Assert.IsTrue(IsEfpBreadCrumbVisible, ErrorStrings.EfpBreadcrumbNotVisible);

        }

        public void ClickLoanApprovalTab()
        { 
           ExplicitWait(2);
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpTabs.LoanApproval);
            var clickLoanApprovalTab =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpTabs.LoanApproval));
            clickLoanApprovalTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Loan Approval tab");
        }

        public void ClickLoanDocumentsTab()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpTabs.LoanDocuments);
            var loanDocumentsTab =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpTabs.LoanDocuments));
            loanDocumentsTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Loan Doc tab");
        }

        public void ClickAdverseActionTab()
        {
            WaitForElement(5, Elements.EfpPageLoginPage.EfpPage.EfpTabs.AdverseActions);
            var AdverseActionTab =
                Driver.FindElement(By.XPath(Elements.EfpPageLoginPage.EfpPage.EfpTabs.AdverseActions));
            AdverseActionTab.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Adverse Action Tab");

        }
    }
}