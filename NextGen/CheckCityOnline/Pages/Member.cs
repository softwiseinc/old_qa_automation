﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using AutomationResources;
using OpenQA.Selenium.Firefox;
using static CheckCityOnline.Users;
using static AutomationResources.Helper.Page;


namespace CheckCityOnline.Pages
{
    public class MemberPage : BaseApplicationPage
    {
        public MemberPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.Member;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.Member;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.Member;
        }


        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        internal void ClickRequestPaydayLoan()
        {
            WaitForElement(5, Elements.MemberPage.Button.ClickRequestPaydayLoan);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Request Payday loan button. ");
            var clickButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickRequestPaydayLoan));
            clickButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click request payday loan");
        }

        public bool IsMemberDashboardLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("member.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
                Logger.Trace("Member page is loaded. ");
                return isLoaded;
            }
        }

        public bool IsCustomerLoginRegistrationLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("memberlogin.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
                Logger.Trace("Member page is loaded. ");
                return isLoaded;
            }
        }

        public bool IsCustomerDashboardLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains("member.aspx");
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
                Logger.Trace("Member page is loaded. ");
                return isLoaded;
            }
        }

        public void AssertMemberPageLoaded()
        {
           ExplicitWait(7);
            Assert.IsTrue(IsMemberDashboardLoaded, ErrorStrings.MemberLoginPageNotLoaded);
        }

        public void AssertMemberDashboardLoaded()
        {
            ExplicitWait(7);
            Assert.IsTrue(IsCustomerDashboardLoaded, ErrorStrings.MemberLoginPageNotLoaded);
        }



        public bool IsPendingPaydayLoanTileVisible
       {
           get
           {
               var isLoaded = Driver.FindElement(By.Id("ctl00_CustomContent_lblPendingStoreLoanType")).Displayed;
               Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member page loaded successfully");
               Logger.Trace("Member page is loaded. ");
               return isLoaded;
           }
       }


       public void AssertPendingPaydayLoanTileIsVisible()
       {

           Assert.IsTrue(IsPendingPaydayLoanTileVisible, ErrorStrings.MemberPageNotLoaded);

       }





        public void IfExtendIsNeededHelper()
       {
           
           
           List<IWebElement> elementList = new List<IWebElement>();
           elementList.AddRange(Driver.FindElements(By.XPath(Elements.MemberPage.Button.ClickPaydayMakePayment)));
           if (elementList.Count > 0)
           {
               //var extensionMessagePage = new ExtensionMessage(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               //extensionMessagePage.AssertExtensionPageLoaded();
               //extensionMessagePage.ClickNoIWantToExtendDueDate();

               var memberPage = new MemberPage(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               memberPage.MakePaydayLoanPayment();

                var extensionMessagePage = new ExtensionMessage(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
                extensionMessagePage.AssertExtensionPageLoaded();
                extensionMessagePage.ClickYesWithoutMovingTheDueDate();

                var paymentTypePage = new PaymentType(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
                paymentTypePage.AssertPaymentTypePageIsLoaded();
                paymentTypePage.EnterAmount();
                paymentTypePage.SelectPaymentTypeAch();
                paymentTypePage.ClickNext();

                var paymentAcceptPage = new PaymentAccept(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
                paymentAcceptPage.AssertPaymentAcceptLoaded();
                paymentAcceptPage.ClickAcceptButton();

                var paymentTransactPage = new paymentTransact(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
                paymentTransactPage.AssertPayDayTransactionInfoIsVisible();
                paymentTransactPage.ClickAgreeToTransaction();

                var thankYouPage = new PaymentThankYou(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
                thankYouPage.AssertPaymentThankYouPageLoaded();
                thankYouPage.ClickGotItButton();
                
                memberPage.AssertMemberPageLoaded();
                

           }
           else
           {

               var memberPage = new MemberPage(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               memberPage.ClickExtend();

               var extensionTypePage = new ExtensionType(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               extensionTypePage.AssertExtensionType();
               extensionTypePage.ClickDateField();
               //extensionTypePage.ClickCallNextToChangeTheMonth();
               extensionTypePage.PickDesiredDueDate();
               extensionTypePage.AchPaymentMethod();
               extensionTypePage.ClickNext();

               var extensionAcceptPage =
                   new ExtensionAccept(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               extensionAcceptPage.AssertExtensionAcceptLoaded();
               extensionAcceptPage.ClickAccept();

               var extensionTransactPage = new ExtensionTransact(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               extensionTransactPage.AssertExtensionTransactLoaded();
               extensionTransactPage.ClickIAgreeButton();

               var paymentThankYouPage = new PaymentThankYou(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
               paymentThankYouPage.AssertPaymentThankYouPageLoaded();
               paymentThankYouPage.ClickGotItButton();

               memberPage.AssertMemberPageLoaded();
           }

           
       }

       public bool TryRequestPersonalLoanPresent()
       {
            bool isPresent = false;
            try
            {
                ClickRequestPersonalLoan();
                Reporter.LogTestStepForBugLogger(Status.Info, "Clicked on request personal loan button");
                isPresent = true;
            }
            catch (Exception e)
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "Request personal loan button was not found");
            }

            return isPresent;
       }
        public bool IsRequestPersonalLoanBtnPresent()
        {
           return  IsElementPresent(By.XPath(Elements.MemberPage.Button.ClickRequestPersonalLoan));
        }
        









        public bool MakePaymentButtonTextIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickPaydayMakePayment))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Make Payment is visible on member page page. ");
                Logger.Trace($"Extend loan is visible =>{ isVisible}");
                return isVisible;
            }
        }
        
        public bool AssertMakePaymentButtonTextIsVisible()
        {
            WaitForElement(5, Elements.MemberPage.Button.ClickPaydayMakePayment);
            Assert.IsTrue(MakePaymentButtonTextIsVisible, ErrorStrings.MakePaymentButtonTextIsNotVisible);
            return true;
        }
        public void ClickRequestPersonalLoan()
        {
            if(!ElementVisibilityState(Driver, Elements.MemberPage.Button.ClickRequestPersonalLoan))
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "TEST USER NOT ABLE TO CREATE LOAN, TRY AGAIN");
            }
            WaitForElement(5, Elements.MemberPage.Button.ClickRequestPersonalLoan);
            var clickPlButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickRequestPersonalLoan));
            clickPlButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on request Personal Loan");

        }

    
        public void ClickPaymentPersonaLoan()
        {

            WaitForElement(5, Elements.MemberPage.Button.ClickMakePaymentPersonalLoan);
            var paymentButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickMakePaymentPersonalLoan));
            paymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks on the make payment on a personal loan");

        }


        public void ClickProfile()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButtonOptions.Profile);
            var profileMenuItem = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.Profile));
            profileMenuItem.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks Side bar menu item 'Profile'");
        }

        public void ClickUserNameMenu()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButton);
            var dropDown = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButton));
            dropDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks Top bar User Drop Down Menu'");
        }
        public void ClickPayOff()
        {
            WaitForElement(10, Elements.MemberPage.PaymentPopupOption.PayOff);
            var payOffPaydayButton = Driver.FindElement(By.XPath(Elements.MemberPage.PaymentPopupOption.PayOff));
            payOffPaydayButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "User clicks payoff. ");
        }

        public void PaymentApprovedIsDisplayed()
        {
            WaitForElement(10, Elements.MemberPage.ScreenText.PaymentApproved);
            if (Driver.FindElement(By.XPath(Elements.MemberPage.ScreenText.PaymentApproved)).Displayed)

            {
                var paymentButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickPaydayExtend));
                paymentButton.Click();
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method searches to see if user can make a payment on a particular loan");
        }

        //public bool ClickExtend()
        //{
        //    ExplicitWait(3);
        //    var eligibleLoan = LookForExtendableLoan(Elements.MemberPage.Button.Extend);
        //    if (eligibleLoan != null)
        //    {
        //        WaitForElement(5, eligibleLoan);
        //        Driver.FindElement(By.XPath(eligibleLoan)).Click();
        //        return true;
        //    }
        //    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method clicks extend for a payday loan");
        //    return false;

        //}
        public bool ClickEligibleLoan(int neverDay)
        {
            ExplicitWait(3);
            foreach (var element in Driver.FindElements(By.XPath("//*[@class='d-b-s-l-b']")))
            {
                var dueDate = element.FindElement(By.ClassName("d-b-s-cap-date")).Text;
                var dayOfMonth = dueDate.Substring(dueDate.Length - 7, 2);
                dayOfMonth = dayOfMonth.Replace(@"/", "");
                if (dayOfMonth != neverDay.ToString())
                {
                    foreach (var loanBoxElement in element.FindElements(By.ClassName("d-b-s-btn")))
                    {
                        var boxElementTextText = loanBoxElement.Text;
                        if (boxElementTextText == "EXTEND")
                        {
                            loanBoxElement.Click();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        //public string LookForExtendableLoan(string currentLoan)
        //{
        //    var counter = 0;
        //    string currentLoanText = null;
        //    while (currentLoanText == null)
        //        try
        //        {
        //            currentLoanText = Driver.FindElement(By.XPath(currentLoan)).Text;
        //            if (currentLoanText == "EXTEND")
        //                return currentLoan;
        //        }
        //        catch (Exception e)
        //        {
        //            counter = counter + 1;
        //            if (counter < 10)
        //                currentLoan = $"{ElementBuild.Part1Extend}0{counter}{ElementBuild.Part2Extend}";
        //            else
        //                currentLoan = $"{ElementBuild.Part1Extend}{counter}{ElementBuild.Part2Extend}";
        //            Logger.Trace(e);
        //        }
        //    Reporter.LogPassingTestStepToBugLogger(Status.Info, "Checks to see if a payday loan can be extended. ");
        //    return null;
        //}

        public bool MakePaydayLoanExtension()
        {
            var eligibleLoan = LookForPayableLoan(Elements.MemberPage.Button.ClickPaydayExtend, "Extension");
            if (eligibleLoan != null)
            {
                WaitForElement(5, eligibleLoan);
                Driver.FindElement(By.XPath(eligibleLoan)).Click();
                return true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method clicks make payment for a Payday loan ");
            return false;
        }

        public bool MakePaydayLoanPayment()
        {
            var eligibleLoan = LookForPayableLoan(Elements.MemberPage.Button.ClickPaydayMakePayment, "Payment");
            var extendLoan = Driver.FindElement(By.XPath(Elements.MemberPage.Button.Extend));
            if (eligibleLoan != null)
            {
                WaitForElement(5, eligibleLoan);
                Driver.FindElement(By.XPath(eligibleLoan)).Click();
                return true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method clicks make payment for a Payday loan ");
            return false;
        }

        public bool MakePaymentPersonalLoan()
        {
            var eligibleLoan = LookForPayableLoan(Elements.MemberPage.Button.ClickMakePaymentPersonalLoan, "Payment");
            if (eligibleLoan != null)
            {
                WaitForElement(5, eligibleLoan);
                Driver.FindElement(By.XPath(eligibleLoan)).Click();
                return true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method clicks make payment for a Payday loan ");
            return false;
        }

        public string LookForPayableLoan(string currentLoan, string loanAction)
        {
            var counter = 0;
            var timeout = 0;
            string currentLoanText = null;
            while (currentLoanText == null)
                try
                {
                    currentLoanText = Driver.FindElement(By.XPath(currentLoan)).Text;
                    switch (loanAction)
                    {
                        case "Extend":
                            if (currentLoanText == "EXTEND")
                                return currentLoan;
                            break;
                        case "Payment":
                            if (currentLoanText == "MAKE PAYMENT")
                                return currentLoan;
                            break;
                    }
                    if (timeout >= 10)
                        break;

                }
                catch (Exception e)
                {
                    counter = counter + 1;
                    timeout += 1;
                    if (timeout > 10)
                    {
                        Logger.Error("no loan found");
                        throw;
                    }

                    //if (counter < 10)
                    //    currentLoan = $"{counter + 1}";
                    //// $"{ElementBuild.Part1Payable}{counter+1}{ElementBuild.Part2Payable}";
                }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Checks to see if payment can be made for a payday loan. ");
            
            return null;
        }


        public void ClickMakePayment()
        {
            WaitForElement(5, Elements.MemberPage.Button.ClickMakePaymentPersonalLoan);
            var paymentButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickMakePaymentPersonalLoan));
            paymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Make a payment on a Personal loan");
        }

        public void ClickPayment()
        {
           WaitForElement(10, Elements.MemberPage.PaymentPopupOption.MakePayment);
            var paymentButton = Driver.FindElement(By.XPath(Elements.MemberPage.PaymentPopupOption.MakePayment));
            paymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "clicks make a payment on a Payday loan");
        }

        public string LookForRefinanceableLoan(string currentLoan)
        {
            var counter = 0;
            string currentLoanText = null;
            while (currentLoanText == null)
                try
                {
                    currentLoanText = Driver.FindElement(By.XPath(currentLoan)).Text;
                    if (currentLoanText == "REFINANCE")
                        return currentLoan;
                }
                catch (Exception e)
                {
                    counter = counter + 1;
                    if (counter < 10)
                        currentLoan = $"{ElementBuild.Part1Refinance}0{counter}{ElementBuild.Part2Refinance}";
                    else
                        currentLoan = $"{ElementBuild.Part1Refinance}{counter}{ElementBuild.Part2Refinance}";
                    Logger.Trace(e);
                }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Checks to see if loan can be refinanced. ");
            return null;
        }

        //Personal loan extend feature after clicking refinance you are giving the option to click on extend
        public void RefinanceExtend()
        {
            WaitForElement(5, Elements.MemberPage.Button.RefinanceExtend);
            var refiExtend = Driver.FindElement(By.XPath(Elements.MemberPage.Button.RefinanceExtend));
            refiExtend.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clicks extend on a Personal loan ");

        }

        //Personal loan refinance option focusing on borrow 
        public bool Refinance()
        {
            WaitForElement(5, Elements.MemberPage.Button.Refinance);
            var eligibleLoan = LookForRefinanceableLoan(Elements.MemberPage.Button.Refinance);
            if (eligibleLoan != null)
            {
                WaitForElement(5, eligibleLoan);
                Driver.FindElement(By.XPath(eligibleLoan)).Click();
                return true;
            }
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Method clicks refinance");
            return false;
            
        }

        //Personal loan borrow feature after clicking refinance you are giving the option to click on borrow
        public void RefinanceBorrow()
        {
            WaitForElement(5, Elements.MemberPage.Button.RefinanceBorrow);
            var refiBorrowButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.RefinanceBorrow));
            refiBorrowButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on borrow btn ");
        }

        
        //Personal loan pay down feature after clicking refinance you are giving the option to click on pay down
        public void RefinancePayDown()
        {
            WaitForElement(5, Elements.MemberPage.Button.RefinancePayDown);
            var refiPayDownButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.RefinancePayDown));
            refiPayDownButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on refinance to process a pay down personal loan ");
        }

        public void ClickReferAFriend()
        {
            WaitForElement(5, Elements.MemberPage.AccountPages.ClickReferAFriend);
            var referAFriendBtn = Driver.FindElement(By.XPath(Elements.MemberPage.AccountPages.ClickReferAFriend));
            referAFriendBtn.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click on Refer a Friend btn");
        }

        public void ClickDropdownReferAFriend()
        {
            WaitForElement(5, Elements.MemberPage.Button.ReferFriend);
            var referAFriendDropDown = Driver.FindElement(By.XPath(Elements.MemberPage.Button.ReferFriend));
            referAFriendDropDown.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Refer a Friend Dropdown menu item");
        }
        public void ClickLoanHistory()
        {
           WaitForElement(10, Elements.MemberPage.TopMenu.MenuButtonOptions.LoanHistory);
           var loanHistoryButton = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.LoanHistory));
           loanHistoryButton.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Loan History");
        }

        public void ClickUploadDocs()
        {
           WaitForElement(5, Elements.MemberPage.Button.UploadDocs);
           var uploadDocs = Driver.FindElement(By.XPath(Elements.MemberPage.Button.UploadDocs));
           uploadDocs.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Upload Docs");

        }

        public void ClickLoanTerms()
        {
           
            WaitForElement(5, Elements.StoreLoanHistory.Terms);
           var terms = Driver.FindElement(By.XPath(Elements.StoreLoanHistory.Terms));
           terms.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click loan terms");
        }

        public void ClickTableRow()
        {
           WaitForElement(5, Elements.StoreLoanHistory.LoanDocumentListPopup.TableRow);
           var tableRow = Driver.FindElement(By.XPath(Elements.StoreLoanHistory.LoanDocumentListPopup.TableRow));
           tableRow.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Table Row for Loan Documents ");
        }

        public void ClickClose()
        {
           WaitForElement(5, Elements.StoreLoanHistory.LoanDocumentListPopup.LoanDocumentsPopup.Close);
           var close = Driver.FindElement(By.XPath(Elements.StoreLoanHistory.LoanDocumentListPopup.LoanDocumentsPopup
               .Close));
           close.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click close");
        }

        public void ClickExtend()
        {
           WaitForElement(5, Elements.MemberPage.Button.Extend);
           var extend = Driver.FindElement(By.XPath(Elements.MemberPage.Button.Extend));
           extend.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click extend");
        }

        public void ClickPendingApplication()
        {
            ExplicitWait(2);
            WaitForElement(5, Elements.MemberPage.Button.ClickCheckPendingApplication);
            var pendingApplication =
                Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickCheckPendingApplication));
            pendingApplication.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Check Pending Application ");
        }

        public void ClickYourOpinionCounts()
        {
            ExplicitWait(8);
            try
            {

                WaitForElement(5, Elements.ThankYou.Button.ClickOpinionCounts);
                var clickOpinionCounts = Driver.FindElement(By.XPath(Elements.ThankYou.Button.ClickOpinionCounts));
                clickOpinionCounts.Click();
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on the red 'X' to close the survey. ");
            }
            catch (Exception)
            {
                Reporter.LogPassingTestStepToBugLogger(Status.Info, "Survey window did not show");
            }
        }

        public bool IsCheckPendingVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.ScreenText.CheckPendingApplication))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Check pending app tile is visible");
                Logger.Trace("check pending app is visible. ");
                return isVisible;
            }
        }

        public bool IsMemberLoggedIn
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButton))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the member logged in menu is visible");
                Logger.Trace("Validate the member logged in menu is visible");
                return isVisible;
            }
        }
        public bool IsBringTheFollowingToStore
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.ScreenText.BringTheFollowingToStore))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Check pending app tile is visible");
                Logger.Trace("check pending app is visible. ");
                return isVisible;
            }
        }

        public void AssertCheckPendingApplication()
        {
            WaitForElement(5, Elements.MemberPage.ScreenText.CheckPendingApplication);
            Assert.IsTrue(IsCheckPendingVisible, ErrorStrings.CheckPendingApplicationTileIsNotVisible);
        }

        public void AssertBringTheFollowingToStore()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.MemberPage.ScreenText.BringTheFollowingToStore);
            Assert.IsTrue(IsBringTheFollowingToStore, ErrorStrings.CheckPendingApplicationTileIsNotVisible);
        }

        public bool TryCheckPendingApplication()
        {
            bool isPending = false;
            try
            {
                AssertCheckPendingApplication();
                isPending = true;
            }
            catch (Exception e)
            {
                isPending = false;
            }

            return isPending;
        }
        
        public bool IsPersonalLoanVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.TypesOfLoans.PersonalLoan))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Personal loan type is visible on member page page. ");
                Logger.Trace($"Personal loan Type is visible =>{ isVisible}");
                return isVisible;
            }

        }
        
        public bool IsMakePaymentVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.MemberPage.ScreenText.MakePaymentTile))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Make Payment is visible on member page page. ");
                Logger.Trace($"Extend loan is visible =>{ isVisible}");
                return isVisible;
            }
        }

        public void ExtendCustomerPaydayLoan()
        {
            var memberPage = new MemberPage(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
            memberPage.ClickExtend();

            var extensionTypePage = new ExtensionType(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
            extensionTypePage.AssertExtensionType();
            extensionTypePage.ClickDateField();
            //extensionTypePage.ClickCallNextToChangeTheMonth();
            extensionTypePage.PickDesiredDueDate();
            extensionTypePage.AchPaymentMethod();
            extensionTypePage.ClickNext();

            var extensionAcceptPage =
                new ExtensionAccept(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
            extensionAcceptPage.AssertExtensionAcceptLoaded();
            extensionAcceptPage.ClickAccept();

            var extensionTransactPage = new ExtensionTransact(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
            extensionTransactPage.AssertExtensionTransactLoaded();
            extensionTransactPage.ClickIAgreeButton();

            var paymentThankYouPage = new PaymentThankYou(Driver, GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance));
            paymentThankYouPage.AssertPaymentThankYouPageLoaded();
            paymentThankYouPage.ClickGotItButton();
        }

        public void LogOut()
        {
            WaitForElement(5, Elements.MemberPage.Button.Logout);
            var logoutButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.Logout));
            logoutButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info,"Click logout button");
        }

        public void ClickRefinance()
        {
            WaitForElement(5, Elements.MemberPage.Button.Refinance);
            var refinanceButton = Driver.FindElement(By.XPath(Elements.MemberPage.Button.Refinance));
            refinanceButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Refinance Button ");
        }

        public void AssertCustomerLoggedIn()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButton);
            Assert.IsTrue(IsMemberLoggedIn, ErrorStrings.MemberIsNotLoggedIn);
        }

        public void ClickCustomerMenuButton()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButton);
            var loggedInMenuButton = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButton));
            loggedInMenuButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on logged in Menu");
        }

        public void ClickProfileUpdated()
        {
           WaitForElement(5, Elements.MemberPage.TopMenu.MenuButtonOptions.Profile);
           var profile = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.Profile));
           profile.Click();
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click profile options");
        }

        public void ClickProfileNV()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButtonOptions.ProfileNV);
            var profile = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.ProfileNV));
            profile.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click profile options");
        }

        public void ClickDashboard()
        {
            WaitForElement(5, Elements.MemberPage.TopMenu.MenuButtonOptions.Dashboard);
            var makeMyPaymentButton =
                Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.Dashboard));
            makeMyPaymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click dropdown Dashboard");
        }

        public void MakePayment()
        {
            var customerPersonalWithBalance = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
            var memberLoginPage = new MemberLoginPage(Driver, customerPersonalWithBalance);
            var count = 1;
            while (!AutomationResources.Helper.Page.ElementVisibilityState(Driver,
                Elements.MemberPage.Button.ClickMakePaymentPersonalLoan))
            {
                ClickCustomerMenuButton();
                ClickDropdownLogout();
                memberLoginPage.LogInUser(customerPersonalWithBalance);
                customerPersonalWithBalance = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                memberLoginPage = new MemberLoginPage(Driver, customerPersonalWithBalance);
                if (++count > 5)
                    break;


            }
            WaitForElement(5, Elements.MemberPage.Button.ClickMakePaymentPersonalLoan);
            var makeMyPaymentButton =
                Driver.FindElement(By.XPath(Elements.MemberPage.Button.ClickMakePaymentPersonalLoan));
            makeMyPaymentButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click make my payment ");
        }
        public void ClickDropdownLogout()
        {
            WaitForElement(10, Elements.MemberPage.TopMenu.MenuButtonOptions.Logout);
            var loanHistoryButton = Driver.FindElement(By.XPath(Elements.MemberPage.TopMenu.MenuButtonOptions.Logout));
            loanHistoryButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click dropdown logout menu item");
        }
    }
}