﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace CheckCityOnline.Pages
{
    class PaydayPayment : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public PaydayPayment(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaydayPayment;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaydayPayment;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaydayPayment;
        }
        public bool IsPaydayPaymentIsVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.ScreenText.Payment))
                    .Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Payment bread crumb is Visible");
                Logger.Trace("Payment bread crumb loaded is loaded. ");
                return isVisible;
            }
        }

        public void AssertPaydayPaymentIsVisible()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.ScreenText.Payment);
            Assert.IsTrue(IsPaydayPaymentIsVisible, ErrorStrings.PaydayPaymentNotVisible);
        }

        public void SelectPaymentTypeAch()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.PaymentType.ACH);
            var bankInfo = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.PaymentType.ACH));
            var selectElement = new SelectElement(bankInfo);
            selectElement.SelectByText(Users.User.CascadesBank);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select the back account that is on file. ");
        }

        public void EnterPaymentDate()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Input.PaymentDate);
            var enterTomorrowsDate =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.WithdrawalDate.EnterDate));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            enterTomorrowsDate.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "method clears date format. ");
            enterTomorrowsDate.SendKeys(strDateOnly);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Tomorrows date is entered in the field. ");
        }

        public void EnterAmount()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Input.PaymentAmount);
            var amount = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.Input.PaymentAmount));
            amount.SendKeys(PrincipalPayment.Small);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Payment amount is entered into amount field ");
        }

        public void ClickNext()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Button.Next);
            var nextButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.Button.Next));
            nextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clicks on next btn ");
        }
    }
}
