﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class StoreProfileEdit : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public StoreProfileEdit(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.StoreProfileEdit;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.StoreProfileEdit;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.StoreProfileEdit;
        }

        public bool IsStoreProfileEditPageLoaded
        {
            get
            {
                var isLoaded = TestEnvironment.Url.Contains("store-profile-edit");
                Reporter.LogTestStepForBugLogger(Status.Info,
                    "Validate the store profile edit page loaded successfully");
                Logger.Trace($"thank you page is loaded=>{isLoaded}");
                return isLoaded;
            }

        }
        public void AssertStoreProfileEditPageLoaded()
        {

            Assert.IsTrue(IsStoreProfileEditPageLoaded, ErrorStrings.StoreProfilePageNotVisible);

        }

        public void ClickEmailAddressInputField()
        {
            WaitForElement(5, Elements.StoreProfileEdit.InputFields.EmailAddress);
            var emailAddressInputField = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.InputFields.EmailAddress));
            emailAddressInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click email address input field");
        }

        public void ClickSaveChanges()
        {
            WaitForElement(5, Elements.StoreProfileEdit.Buttons.SaveChanges);
            var saveChangesButton = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.Buttons.SaveChanges));
            saveChangesButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Save changes button");
        }

        public void EnterNewEmailAddress()
        {
           WaitForElement(5, Elements.StoreProfileEdit.InputFields.EmailAddress);
           var newEmailAddress = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.InputFields.EmailAddress));
           newEmailAddress.Clear();
           newEmailAddress.SendKeys("fakeNews@msgnext.com");
           Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new email address");
        }
        private void EnterPreviousEmailAddress(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.StoreProfileEdit.InputFields.EmailAddress);
            var previousEmailAddress = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.InputFields.EmailAddress));
            previousEmailAddress.Clear();
            previousEmailAddress.SendKeys(user.Email);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter users previous email address");
        }


        public void ResetToPreviousEmailAddress(Users.CurrentUser user)
        {
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** BEGIN Email CLEANUP - Changing Email back to previous Email ***");
            var storeProfile = new StoreProfile(Driver, user);
            var storeProfileEditPage = new StoreProfileEdit(Driver, user);

            //StoreProfile
            storeProfile.AssertStoreProfileIsVisible();
            storeProfile.ClickUpdateProfile();

            //StoreProfileEdit
            storeProfileEditPage.AssertStoreProfileEditPageLoaded();
            storeProfileEditPage.ClickEmailAddressInputField();
            storeProfileEditPage.EnterPreviousEmailAddress(user);
            storeProfileEditPage.EmailNoticeHandler();
            storeProfileEditPage.ClickSaveChanges();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "*** END Email CLEANUP - Changed Email back to previous Email ***");
        }


        private void NavigateToStoreProfileEditPage(Users.CurrentUser user)
        {
            var memberPage = new MemberPage(Driver, user);
            var storeProfile = new StoreProfile(Driver, user);
            var storeProfileEditPage = new StoreProfileEdit(Driver, user);

            //StoreProfile
            storeProfile.AssertStoreProfileIsVisible();
            storeProfile.ClickUpdateProfile();

            //StoreProfileEdit
            storeProfileEditPage.AssertStoreProfileEditPageLoaded();
            storeProfileEditPage.ClickEmailAddressInputField();
            storeProfileEditPage.EnterNewEmailAddress();
            storeProfileEditPage.ClickSaveChanges();

        }

        public void EnterNewPhoneNumber(Users.CurrentUser user)
        {
           WaitForElement(5, Elements.StoreProfileEdit.InputFields.PhoneNumber);
           var phoneNumberInputField = Driver.FindElement(By.Id("ctl00_CustomContent_newPhone"));
            phoneNumberInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on phone number field");
            phoneNumberInputField.SendKeys(user.PhoneNumber);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter new phone number");


        }

        public void ClickCancel()
        {
            WaitForElement(5, Elements.StoreProfileEdit.Buttons.Cancel);
            var emailAddressInputField = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.Buttons.Cancel));
            emailAddressInputField.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click Cancel Button");
        }

        private bool IsNoticeOptionVisible()
        {
            ExplicitWait(2);
            List<IWebElement> buttonList = new List<IWebElement>();
            buttonList.AddRange(Driver.FindElements(By.XPath(Elements.StoreProfileEdit.Buttons.YesForFuturePayments)));
            if (buttonList.Count > 0)
                return true;
            return false;
        }
        public void EmailNoticeHandler()
        {
            ExplicitWait(2);
            if (IsNoticeOptionVisible())
            {
                ExplicitWait(3);
                Reporter.LogTestStepForBugLogger(Status.Info, "Email Opt-In question is visible");
                var yesRadioButton = Driver.FindElement(By.XPath(Elements.StoreProfileEdit.Buttons.YesForFuturePayments));
                yesRadioButton.Click();
                Reporter.LogTestStepForBugLogger(Status.Info, "Click Yes for Future Payment Notification");
            }
        }
    }
}
