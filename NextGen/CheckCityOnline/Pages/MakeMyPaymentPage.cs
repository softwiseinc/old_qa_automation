﻿using System;
using System.Linq;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace CheckCityOnline.Pages
{
    internal class MakeMyPaymentPage : BaseApplicationPage
    {
        public MakeMyPaymentPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.MakeMyPayment;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.MakeMyPayment;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.MakeMyPayment;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsUrlLoaded
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Member Login page loaded successfully");
                Logger.Trace($"MakeMyPayment page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        public bool IsLoanPastDue
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.Loans.LoanApproval.ScreenText.PastDue)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate a loan is Past Due");
                Logger.Trace($"A past due lown is visable=>{isVisible}");
                return isVisible;
            }
        }

        public void NavigatToMakeMyPaymentLogin()
        {
            NavigateToMakeMyPaymentUrl();
        }

        private void NavigateToMakeMyPaymentUrl()
        {
            Driver.Navigate().GoToUrl(TestEnvironment.Url);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, $"In a browser, go to url=>{TestEnvironment.Url}");
            Assert.IsTrue(IsUrlLoaded, ErrorStrings.MakeMyPaymentPageNotLoaded);
        }

        public void EnterLoginInformation(Users.CurrentUser user)
        {
            EnterLastName(user.LastName);
            EnterBirthdate(user.BirthDate);
            EnterLastFourSsn(user.SocialSecurityNumber);
        }

        private void EnterLastName(string lastName)
        {
            WaitForElement(5, Elements.MakeMyPaymentsPage.Input.LastName);
            Driver.FindElement(By.XPath(Elements.MakeMyPaymentsPage.Input.LastName)).SendKeys(lastName);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a User Last Name value in the user Last Name input field.");
        }
        private void EnterBirthdate(string birthdate)
        {
            WaitForElement(5, Elements.MakeMyPaymentsPage.Input.Birthdate);
            Driver.FindElement(By.XPath(Elements.MakeMyPaymentsPage.Input.Birthdate)).SendKeys(birthdate);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter a User Birthdate value in the Birthdate input field.");
        }
        private void EnterLastFourSsn(string ssn)
        {
            string lastFourSsn = ssn.Substring(ssn.Length - 4);
            WaitForElement(5, Elements.MakeMyPaymentsPage.Input.LastFourSsn);
            Driver.FindElement(By.XPath(Elements.MakeMyPaymentsPage.Input.LastFourSsn)).SendKeys(lastFourSsn);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Enter the last for numbers of the users SSN value Last 4 of SSN input field.");
        }

        public void ClickLoginButton()
        {
            WaitForElement(5, Elements.MakeMyPaymentsPage.Button.Login);
            var loginButton = Driver.FindElement(By.XPath(Elements.MakeMyPaymentsPage.Button.Login));
            loginButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click on Longin Button");
        }

        public void AssertLoanIsPastDue()
        {
            ExplicitWait(3);
            WaitForElement(5, Elements.Loans.LoanApproval.ScreenText.PastDue);
            Assert.IsTrue(IsLoanPastDue, ErrorStrings.PersonalLoanApprovalPageNotVisible);
        }
    }
}
