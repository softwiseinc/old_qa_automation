﻿using AventStack.ExtentReports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using OpenQA.Selenium;

namespace CheckCityOnline.Pages
{
    internal class paymentTransact : BaseApplicationPage
    {
        public paymentTransact(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaymentTransact;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaymentTransact;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaymentTransact;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();



        public bool IsPaymentTransactVisible
        {
            get
            {
                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Payday Transact page loaded successfully");
                Logger.Trace($"PayDay Transaction Info page is loaded. ");
                return isLoaded;
            }
        }

        public void AssertPayDayTransactionInfoIsVisible()
        {
            ExplicitWait(2);
            Assert.IsTrue(IsPaymentTransactVisible, ErrorStrings.PayDayTransactionInfoPageNotLoaded);
        }


        public void SignYourName()
        {
            WaitForElement(5, Elements.PayDayLoan.PaymentTransact.Input.EnterSignature);
            var signHere =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PaymentTransact.Input.EnterSignature));
            signHere.SendKeys(Users.User.Signature);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Signature is entered in signature field");
        }

        public void ClickAgreeToTransaction()
        {
            WaitForElement(5, Elements.PayDayLoan.PaymentTransact.Button.ClickAgreeToTransaction);
            var agreeButton =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PaymentTransact.Button.ClickAgreeToTransaction));
            agreeButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Agree button is clicked. ");

        }


        public void ClickNext()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayExtension.Button.Continue);
            var continueButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayExtension.Button.Continue));
            continueButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Click continue btn ");
        }
    }
}
