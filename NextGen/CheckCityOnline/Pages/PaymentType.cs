﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;


namespace CheckCityOnline.Pages
{
    internal class PaymentType : BaseApplicationPage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public PaymentType(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.PaymentType;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.PaymentType;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.PaymentType;
        }

        public bool IsPaymentTypePageLoaded
        {
            get
            {

                var isLoaded = Driver.Url.Contains(TestEnvironment.Url);
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the 'extension message' page loaded successfully");
                Logger.Trace($"extension message page loaded. ");
                return isLoaded;
            }
        }


        public void AssertPaymentTypePageIsLoaded()
        {
            Assert.IsTrue(IsPaymentTypePageLoaded, ErrorStrings.PaymentTypePageNotLoaded);
        }

        public void SelectPaymentTypeAch()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.PaymentType.ACH);
            var bankInfo = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.PaymentType.ACH));
            bankInfo.Click();
            //var selectElement = new SelectElement(bankInfo);
            //selectElement.SelectByText(Users.User.CascadesBank);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Select ACH as Payment Type. ");
        }

        public void EnterPaymentDate()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Input.PaymentDate);
            var enterTomorrowsDate =
                Driver.FindElement(By.XPath(Elements.PayDayLoan.PayDayPayOff.WithdrawalDate.EnterDate));
            var date = DateTime.Today.AddDays(1);
            string strDateOnly = date.ToString("d");
            enterTomorrowsDate.Clear();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "method clears date format. ");
            enterTomorrowsDate.SendKeys(strDateOnly);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Tomorrows date is entered in the field. ");
        }

        public void EnterAmount()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Input.PaymentAmount);
            var amount = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.Input.PaymentAmount));
            amount.SendKeys(PrincipalPayment.Medium);
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Payment amount is entered into amount field ");
        }

        public void ClickNext()
        {
            WaitForElement(5, Elements.PayDayLoan.PaydayPayment.Button.Next);
            var nextButton = Driver.FindElement(By.XPath(Elements.PayDayLoan.PaydayPayment.Button.Next));
            nextButton.Click();
            Reporter.LogPassingTestStepToBugLogger(Status.Info, "Clicks on next btn ");
        }


    }
}