﻿using AutomationResources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CheckCityOnline
{
    public partial class Users
    {
        public static CurrentUser GetProdTestingApprovePaydayUT()
        {
            User = new CurrentUser
            {
                RunEnvironment = "PROD",
                LastName = "Approved",
                UserName = "TestingApprovePaydayUT@checkcity.com",
                Address = "3650 Ruckman Circle",
                City = "Orem",
                State = "Utah",
                ZipCode = "84057",
                PhoneNumber = "8677919099",
                Password = @"TestApproved50",
                SocialSecurityNumber = "529-00-6000",
                BirthDate = @"12/24/1936"
            };
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.STAGE)
            {
                User = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
            }
            return User;
        }

        public static CurrentUser GetProdTestingApprovePersonalUT()
        {
            User = new CurrentUser
            {
                RunEnvironment = "PROD",
                LastName = "Approved",
                UserName = "TestingApprovePersonalUT@checkcity.com",
                Address = "1776 Brownton Road",
                City = "Spanish Fork",
                State = "Utah",
                ZipCode = "84660",
                PhoneNumber = "8677919099",
                Password = @"TestApproved45",
                SocialSecurityNumber = "529-00-5000",
                BirthDate = @"12/24/1932"
            };
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.STAGE)
            {
                User = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                User.RunEnvironment = "STAGE";
            }
            return User;
        }

        public static CurrentUser GetProdTestingDebtUser()
        {
            User = new CurrentUser
            {
                //payday loans only
                RunEnvironment = "PROD",
                LastName = "Adams",
                UserName = "TestingApprovePaydayUT@checkcity.com",
                Address = "3650 Ruckman Circle",
                City = "Orem",
                State = "Utah",
                ZipCode = "84057",
                PhoneNumber = "5559500283",
                Password = @"TestApproved50",
                SocialSecurityNumber = "529-00-6213",
                BirthDate = @"10/14/1944"
            };
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.STAGE)
            {
                User.RunEnvironment = "STAGE";
                User.LastName = "Debt-LastName";
                User.UserName = "Brandi-Debt@megnext.com";
                User.Address = "1544 S 1220 W ";
                User.City = "Vernal";
                User.State = "Utah";
                User.ZipCode = "84078";
                User.PhoneNumber = "8018932295";
                User.Password = @"Diehard1!";
                User.SocialSecurityNumber = "646-09-4740";
                User.BirthDate = $"9/8/1991";
            }
            return User;
        }
        public static CurrentUser GetQaUser001()
        {
            User = new CurrentUser
            {
                //payday loans only
                RunEnvironment = "QA",
                UserName = "qa3@nostromorp.com",
                ReferralEmail = "qa11@nostromorp.com",
                UpdatedEmail = "qa12@nostromorp.com",
                Signature = "Ellen Ripley",
                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84604",
                PhoneNumber = "8012543973",
                Password = @"Facehugger1!",
                BirthDate = @"01 / 03 / 1967",
                CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Facehugger2!"
            };
            return User;
        }
        public static CurrentUser GetQaUserCCO()
        {
            User = new CurrentUser
            {
                //payday loans only
                RunEnvironment = "QA",
                UserName = "rpd",
                ReferralEmail = "qa11@nostromorp.com",
                UpdatedEmail = "qa12@nostromorp.com",
                Signature = "Mark",
                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84604",
                PhoneNumber = "8012543973",
                Password = @"rdp1",
                BirthDate = @"01 / 03 / 1967",
                CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Facehugger2!"
            };
            return User;
        }
        public static CurrentUser GetQaUserRDPLoanApproval()
        {
            User = new CurrentUser
            {
                //payday loans only
                RunEnvironment = "STAGE",
                CustomerState = "Utah",
                AdverseActionCustomerID = "U10-0006460",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com",
                CustomerID = "UTO-124956834",
                UserName = "approval",
                ReferralEmail = "qa11@nostromorp.com",
                UpdatedEmail = "qa12@nostromorp.com",
                Signature = "Mark",
                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84604",
                PhoneNumber = "8012543973",
                Password = @"cco",
                BirthDate = @"01 / 03 / 1967",
                CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Facehugger2!"
            };
            return User;
        }
        public static CurrentUser GetQaApplyLoan()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa2@nostromorp.com",
                Email = "qa2@nostromorp.com",
                FirstName = "Shaw",
                LastName = "Approved",
                BirthDate = @"07/04/1974",
                InvalidBirthDate = @"09/09/9999",

                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84113 ",
                InvalidZipCode = "111111111",
                PhoneNumber = "8011543973",
                PhoneNumberVerify = "9999999999",
                PhoneNumberVerifyCode = "0000",
                Password = @"Facehugger1!",
            };
            return User;
        }
        public static CurrentUser GetQaApplyPendingLoan()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "Aidan-nkhfgsf@nostromorp.com",
                Email = "Aidan-nkhfgsf@nostromorp.com",
                FirstName = "Aidan ",
                LastName = "Pending All",
                BirthDate = @"12/03/1999",
                InvalidBirthDate = @"09/09/9999",
                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84113 ",
                InvalidZipCode = "111111111",
                PhoneNumber = "8011543973",
                PhoneNumberVerify = "9999999999",
                PhoneNumberVerifyCode = "0000",
                Password = @"Diehard!",
            };
            return User;
        }
        public static CurrentUser GetQaApplyLoan03()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa3@nostromorp.com",
                Email = "qa11@nostromorp.com",
                FirstName = "Ravi",
                LastName = "Karmacharya",
                BirthDate = @"01 / 03 / 1967",
                InvalidBirthDate = @"09 / 09 / 9999",

                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84111 ",
                InvalidZipCode = "111111111",
                PhoneNumber = "8011543973",
                PhoneNumberVerify = "9999999999",
                PhoneNumberVerifyCode = "0000",
                Password = @"Facehugger1!",

                /* CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                 NewPassword = "Facehugger2!"*/
            };
            return User;
        }
        public static CurrentUser GetQaUser002()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "Dannyb@softwiseonline.com",
                Signature = "Guy Test",
                Password = @"qjuv6553QJ",
                BirthDate = @"01 / 03 / 1967",
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "qjuv6553QJ"
            };
            return User;
        }

        public static CurrentUser GetQaUser003()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa4500@nostromorp.com",
                PhoneNumber = "8011348977",
                Signature = "Trevor Turndown Lead",
                Password = @"Diehard1!",
                BirthDate = @"11/24/1972",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                NewPassword = "Diehard2!",
                Years = "1",
                Months = "1"
            };
            return User;
        }

        public static CurrentUser GetQaUser004()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa3111@nostromorp.com",
                Signature = "Kris Turndown",
                Password = @"Diehard1!",
                BirthDate = @"06/06/1972",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                Years = "1",
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetQaUser005()
        {
            User = new CurrentUser
            {
                //California state customer
                RunEnvironment = "QA",
                UserName = "qa10@nostromorp.com",
                Signature = "Bill Clay",
                Password = @"Diehard1!",
                BirthDate = @"11/01/1979",
                RoutingNumber = "123206024",
                AccountNumber = "9517777",
                Years = "1",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetQaUser006()
        {
            User = new CurrentUser
            {
                //NV state customer
                RunEnvironment = "QA",
                UserName = "qa8973c@nostromorp.com",
                Signature = "Amanda Approbato",
                Password = @"Diehard1!",
                BirthDate = @"04/01/1959",
                EnterNewLoanAmount = "444",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                Years = "1",
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetQaUser007()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa10@nostromorp.com",
                UpdatedEmail = "qa100@nostromorp.com",
                Signature = "Bill Clay",
                Password = @"Diehard1!",
                PhoneNumber = "8012548810",
                Address = "123 Nakatomi Plaza",
                City = "Beverly Hills",
                State = "California",
                ZipCode = "90210 ",
                BirthDate = @"11/01/1979",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                Years = "1",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetQaUser008()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "qa9260a@nostromorp.com",
                UpdatedEmail = "qa9260aa@nostromorp.com",
                Signature = "Whitney Approved",
                Password = @"Diehard1!",
                BirthDate = @"10/12/1973",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                Years = "1",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetQaUser009()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                UserName = "Qa226y@nostromorp.com",
                UpdatedEmail = "Qa2261@nostromorp.com",
                Signature = "Renn Approved",
                Password = @"Diehard1!",
                BirthDate = @"11/23/1972",
                SocialSecurityNumber = "585-11-1303",
                RoutingNumber = "123206024",
                AccountNumber = "95151",
                Years = "1",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;


        }

        public static CurrentUser GetQaPersonalLoanUser()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Signature = "Rajesh Kr Das",
                FirstName = "Jane",
                LastName = "Doe",
                Email = "QaPersonalLoan009@nostromorp.com", //change email on each successful test run
                DateOfBirth = @"08/09/1976", //change DOB on each successful test run
                ZipCode = "84121",
                MonthlyIncome = "3200",
                EmployerCity = "Lehi",
                PhoneNumber = "8017090070",
                NameOfCompany = "Carson Meats",
                EmployerZipcode = "84043",
                RoutingNumber = "111301737",
                BankAccountNumber = "001234567890",
                ReEnterBankAccountNumber = "001234567890",
                IdentificationNumber = "213A321DE",
                SocialSecurityNumber = "1769071143", //change SSN on each successful test run
                AdditionalMonthlyIncome = "2000",
                AdditionalEmployersCity = "Lehi",
                AdditionalPhoneNumber = "8017090070",
                AdditionalNameOfCompany = "Carson Meats",
                AdditionalEmployersZipCode = "84043",
                ExpirationDate = "11/08/2021",
                StreetAddress = "Salt Lake City, UT 84121",
                ContactPassword = "Diehard1!",
                ContactPhoneNumber = "1111111121",
                VerificationCode = "0000"
            };
            return User;
        }

        public static CurrentUser GetMultipleCitiesUser()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Signature = "Rajesh Kr Das",
                FirstName = "John",
                LastName = "Doe",
                Email = "zipcodemultiplecities@rajeshdas.work", //change email on each successful test run
                DateOfBirth = @"08/09/1976", //change DOB on each successful test run
                ZipCode = "84604",
                StreetAddress = "Cosmo Cougar 146 W. 1940 N"
            };
            return User;
        }

        public static CurrentUser GetQaPersonalLoanNegativeUser()
        {
            User = new CurrentUser
            {
                RunEnvironment = "QA",
                Signature = "Rajesh Kr Das",
                FirstName = "Rajesh",
                LastName = "Kumar Das",
                State = "Utah",
                Email = "QaPersonalLoanNegative0008@nostromorp.com", //change email on each successful test run
                DateOfBirth = @"07/09/1973", //change DOB on each successful test run
                ZipCode = "84121",
                MonthlyIncome = "1200",
                EmployerCity = "Lehi",
                PhoneNumber = "8017090070",
                //CustomerRole = CustomerType.UT_Turndown,
                NameOfCompany = "Carson Meats",
                EmployerZipcode = "84043",
                RoutingNumber = "124000054",
                BankAccountNumber = "001234567890",
                ReEnterBankAccountNumber = "001234567890",
                IdentificationNumber = "213A321DE",
                SocialSecurityNumber = "098131220", //change SSN on each successful test run
                AdditionalMonthlyIncome = "2000",
                AdditionalEmployersCity = "Lehi",
                AdditionalPhoneNumber = "8017090070",
                AdditionalNameOfCompany = "Carson Meats",
                AdditionalEmployersZipCode = "84043",
                ExpirationDate = "11/08/2021",
                StreetAddress = "Salt Lake City, UT 84121",
                ContactPassword = "Timp1234",
                ContactPhoneNumber = "1111111111",
                VerificationCode = "0000"
            };
            return User;
        }

        public static CurrentUser GetStagingUser001()
        {
            User = new CurrentUser
            {
                //Personal loan only
                RunEnvironment = "STAGE",
                UserName = "qa457@nostromorp.com",
                Signature = "Me Approved",
                Password = @"Diehard1!",
                BirthDate = @"04/08/1972",
                RoutingNumber = "123206024",
                AccountNumber = "951566",
                Years = "1",
                EnterNewLoanAmount = "444",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Diehard2!",
                NewPassword2 = "Diehard3!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser002()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "Stg110@nostromorp.com",
                Signature = "Willow Pending All",
                Password = @"Diehard1!",
                BirthDate = @"11/22/1972",
                RoutingNumber = "123206024",
                AccountNumber = "11122",
                Years = "1",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser003()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "stg4477@nostromorp.com",
                Signature = "Alfred TurnDown Lead",
                Password = @"Diehard1!",
                BirthDate = @"05/12/1975",
                RoutingNumber = "123206024",
                AccountNumber = "951888",
                Years = "1",
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser004()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "stg4488@nostromorp.com",
                Signature = "Gordan Turndown",
                Password = @"Diehard1!",
                BirthDate = @"11/27/1972",
                RoutingNumber = "123206024",
                AccountNumber = "959999",
                Years = "1",
                //CascadesBank = "Checking AccountLink# 654987 (Bank Of The Cascades)",
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser005()
        {
            //payday loan only customer 
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "qa4@nostromorp.com",
                UpdatedEmail = "qa881@nostromorp.com",
                ReferralEmail = "qa11@nostromorp.com",
                Signature = "Ellen Ripley",
                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84103 ",
                PhoneNumber = "8012548810",
                Password = @"Facehugger1!",
                BirthDate = @"11/17/1978",
                RoutingNumber = "123206024",
                AccountNumber = "34230000",
                Years = "1",
                CascadesBank = "Checking AccountLink# 34230000 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Facehugger2!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser006()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "qa6@nostromorp.com",
                UpdatedEmail = "qa6666@nostromorp.com",
                ReferralEmail = "qa11@nostromorp.com",
                Signature = "Jen Anderson",
                Address = "123 Xenomorph Way",
                City = "Las Vegas",
                State = "Nevada",
                ZipCode = "89103 ",
                PhoneNumber = "8011548810",
                Password = @"Diehard1!",
                BirthDate = @"05/06/1982",
                RoutingNumber = "123206024",
                AccountNumber = "34230000",
                Years = "1",
                CascadesBank = "Checking AccountLink# 34230000 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Diehard2!"
            };
            return User;
        }

        public static CurrentUser GetStagingUser007()
        {
            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                UserName = "qa6@nostromorp.com",
                UpdatedEmail = "qa6666@nostromorp.com",
                ReferralEmail = "qa11@nostromorp.com",
                Signature = "Ellen Ripley",
                Address = "123 Xenomorph Way",
                City = "Las Vegas",
                State = "Nevada",
                ZipCode = "89103 ",
                PhoneNumber = "8011548810",
                Password = @"Diehard1!",
                BirthDate = @"05/06/1982",
                RoutingNumber = "123206024",
                AccountNumber = "34230000",
                Years = "1",
                CascadesBank = "Checking AccountLink# 34230000 (Bank Of The Cascades)",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Diehard2!"
            };
            return User;

        }


        public static CurrentUser GetProdUser001()
        {
            User = new CurrentUser
            {
                RunEnvironment = "PROD",
                UserName = "Raystantz@checkcity.com",
                Signature = "Ray Stantz",
                Password = "Ghostbusters1!",
                SecurityQuestion1 = Users.SecurityQuestion.One,
                SecurityQuestion2 = Users.SecurityQuestion.Two,
                SecurityQuestion3 = Users.SecurityQuestion.Three,
                SecurityQuestion4 = Users.SecurityQuestion.Four,
                NewPassword = "Ghostbusters1!"
            };
            return User;
        }

        public static CurrentUser GenNewRandomMember(CustomerType customerType)
        {
            var lastName = SetLastName(customerType);
            var stateCode = StateCodes.GetState(customerType);
            var userHelp = new Users();
            var routingNumber = userHelp.GetRandomRoutingNumber();
            var firstName = userHelp.GetRandomFirstName();
            lastName = lastName.Contains("Random") ? userHelp.GetRandomLastName() : lastName;
            var accountNum = userHelp.GetAccountNumber();
            var birthDate = userHelp.RandomBirthDate();
            // we do not want dup ssn in the db
            var ssn = userHelp.RandomUniqueSsn();
            var emailFirstName = SanitizeString(firstName);
            var email = emailFirstName + "@" + TestConfigManager.Config.CatchAllDomain;
            var monthlyIncome = userHelp.RandomFromRange(2323, 5000);
            var sMonthlyIncome = monthlyIncome.ToString();
            var contactZip = PostalCode.GetRandomZipCode(stateCode);
            var contactState = customerType.ToString().Substring(0, 2);
            var employer = userHelp.GetRandomEmployer(stateCode);
            var employerZip = PostalCode.GetRandomZipCode(stateCode);
            var employerCity = userHelp.GetCityByZipcode(employerZip);
            var employerState = customerType.ToString().Substring(0, 2);

            User = new CurrentUser
            {
                RunEnvironment = "STAGE",
                FirstName = firstName,
                LastName = lastName,
                MiddleName = customerType.ToString(),
                State = contactState,
                Email = email,
                Signature = firstName + " " + lastName,
                DateOfBirth = birthDate,
                ZipCode = contactZip,
                MonthlyIncome = sMonthlyIncome,
                EmployerPhone = "0" + userHelp.RandomDigits(9),
                NameOfCompany = employer,
                EmployerZipcode = employerZip,
                EmployerCity = employerCity,
                EmployerState = employerState,
                RoutingNumber = routingNumber,
                BankAccountNumber = accountNum,
                ReEnterBankAccountNumber = accountNum,
                IdentificationNumber = Guid.NewGuid().GetHashCode().ToString("X"),
                SocialSecurityNumber = ssn.ToString(),
                AdditionalMonthlyIncome = "2000",
                AdditionalEmployersCity = "Washington D.C.",
                AdditionalEmployersState = "Washington D.C.",
                AdditionalPhoneNumber = "8009238282",
                AdditionalNameOfCompany = "U.S. Census Bureau",
                AdditionalEmployersZipCode = "20233",
                ExpirationDate = "11/11/2021",
                StreetAddress = customerType + " Drive",
                ContactPassword = "Diehard1!",
                ContactPhoneNumber = "0" + userHelp.RandomDigits(9), //3853252391 - valid GoogleVoice
                SecurityQuestion1 = SecurityQuestion.One,
                SecurityQuestion2 = SecurityQuestion.Two,
                SecurityQuestion3 = SecurityQuestion.Three,
                SecurityQuestion4 = SecurityQuestion.Four,
                VerificationCode = "2222",
                AdverseActionCustomerEmail = "Wefawosal@nostromorp.com"
            };
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.STAGE)
                User.RunEnvironment = "STAGE";
            User.Type = customerType.ToString();

            return User;
        }

        private object RandomUniqueSsn()
        {
            var userHelp = new Users();
            string newSsn;
            do
            {
                var first3 = RandomDigits(3);
                var mid2 = RandomDigits(2);
                var last4 = RandomDigits(4);
                newSsn = first3 + mid2 + last4;
            } while (userHelp.SsnNotUnique(newSsn) || newSsn.Length < 9);
            return newSsn;
        }

        public static CurrentUser GetInvalidUser()
        {
            var userHelp = new Users();
            User = new CurrentUser
            {
                RunEnvironment = TestConfigManager.Config.SystemUnderTest.ToString(),
                UserName = "username@" + TestConfigManager.Config.CatchAllDomain,
                Email = "username@" + TestConfigManager.Config.CatchAllDomain,
                FirstName = userHelp.GetRandomFirstName(),
                LastName = userHelp.GetRandomLastName(),
                BirthDate = @"01 / 03 / 1967",
                InvalidBirthDate = @"09 / 09 / 9999",

                Address = "123 Xenomorph Way",
                City = "Salt Lake City",
                State = "Utah",
                ZipCode = "84103 ",
                DateOfBirth = "07/25/1974",
                InvalidZipCode = "111111111",
                PhoneNumber = "1012543973",
                PhoneNumberVerify = "9999999999",
                PhoneNumberVerifyCode = "0000",
                Password = @"Facehugger1!",

            };
            if (AutomationResources.TestConfigManager.Config.SystemUnderTest == SystemUnderTest.PROD)
                User.RunEnvironment = "PROD";
            return User;
        }

        // Helper Functions 

        private string GetCityByZipcode(string contactZip)
        {
            try
            {
                var apiUri = "http://ziptasticapi.com/" + contactZip;
                var zipData = HttpWeb.GetFromWebApi(apiUri);
                var zipValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(zipData);
                return zipValues["city"].ToUpperInvariant();
            }
            catch (Exception)
            {
                return "MYTOWN";
            }
        }
        private string GetStateByZipcode(string contactZip)
        {
            var apiUri = "http://ziptasticapi.com/" + contactZip;
            var zipData = HttpWeb.GetFromWebApi(apiUri);
            var zipValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(zipData);
            return zipValues["state"].ToUpperInvariant();
        }
        private bool SsnNotUnique(string newSsn)
        {
            var database = new Database();
            var aQuery = "if exists (select * from Contact where SSN = '" + newSsn + "') select 'False' " +
                         "else select 'True'";
            string isUnique = database.ExecuteSingleQuery(aQuery);
            if (isUnique.Contains("True"))
                return false;
            return true;
        }

        public string GetRandomRoutingNumber()
        {
            string[] routingNum = new string[] { "051000017", "041001039", "124001545", "124000737", "124302150", "124002971", "124000054" };
            var random = new Random(Guid.NewGuid().GetHashCode());
            return routingNum[random.Next(0, 5)];
        }

        public string GetAccountNumber()
        {
            var accountNumber = DateTime.Now.Ticks.ToString();
            accountNumber = accountNumber.Remove(0, 2);
            return accountNumber;
        }
        public string GetRandomFirstName()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var firstName = Name.First[random.Next(0, Name.First.Length - 1)];
            int stringLength = random.Next(5, 10);
            return firstName + " " + RandomString(stringLength).ToLower();
        }

        public static string SanitizeString(string dirtyString)
        {
            string pattern = " *[\\~#%&*{}/:<>?^$@!()-,;_|\"-]+ *";
            string replacement = @"";
            Regex regEx = new Regex(pattern);
            string sanitized = regEx.Replace(dirtyString, replacement);
            return sanitized.Replace(" ", "-");
        }

        public string GetRandomLastName()
        {
            var aQuery = "select top 1 LastName from Contact where LastName is not NULL order by NEWID()";
            var database = new Database();
            var lastName = database.ExecuteSingleQuery(aQuery);
            lastName = SanitizeString(lastName);
            return lastName;
        }

        public string GetRandomEmployer(State stateCode)
        {

            var aQuery = "SELECT top 1 c.CompanyName, c.State FROM Contact c WHERE coalesce(c.CompanyName, '') " +
                         "NOT IN('NULL', '') AND CompanyName LIKE '%[a-z]%' and c.State = '" + stateCode + "' and len(c.Zip) = 5 ORDER BY newID()";

            var database = new Database();
            var employerName = database.ExecuteSingleQuery(aQuery);
            if (employerName.Length <= 1)
                employerName = "Federal Government";
            return employerName;
        }

        public static int GetAvailableCustomerCount(CustomerType customerType)
        {
            LoanProperties loan = new LoanProperties
            {
                CustomerState = SetStateStringCode(customerType),
                CustomerLastName = SetLastName(customerType),
                PastDue = PastDue(customerType),
                Type = SetLoanType(customerType),
                MinimumBalance = 1
            };
            if (customerType.ToString().ToLower().Contains("zero"))
                loan.MinimumBalance = 0;

            var aQuery = SetCustomerQuery(loan);


            var database = new Database();
            var count = database.ExecuteSingleQuery(aQuery);
            // ReSharper disable once PossibleInvalidOperationException
            return (int)(int.TryParse(count, out var i) ? i : (int?)null);
        }

        private static string SetLoanType(CustomerType customerType)
        {
            if (customerType.ToString().Contains("Payday"))
                return "DEF";
            return "INS";
        }

        private static bool PastDue(CustomerType customerType)
        {
            if (customerType.ToString().Contains("PastDue"))
                return true;
            return false;
        }


        private static string SetLastName(CustomerType customerType)
        {
            var type = customerType.ToString();
            var lastName = "Approved";

            if (type.Contains("Turndown") && !type.Contains("TurndownLead"))
                lastName = "Turndown";

            if (type.Contains("TurndownLead"))
                lastName = "Turndown Lead";

            if (type.Contains("PendingAll"))
                lastName = "Pending All";

            return lastName;
        }

        private static string SetStateStringCode(CustomerType customerType)
        {
            return customerType.ToString().Substring(0, 2);
        }

        public string RandomDigits(int length)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            string s = string.Empty;
            for (int i = 0; i < length; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }


        public string RandomBirthDate()
        {

            DateTime now = DateTime.Now;
            var exclude = new HashSet<int>() { 5, 7, 17, 23 };
            var range = Enumerable.Range(7565, 30720).Where(i => !exclude.Contains(i));

            var rand = new System.Random();
            int index = rand.Next(0, 100 - exclude.Count);
            var days = range.ElementAt(index);
            days = (days * -1);
            var dNewDate = now.AddDays(days);
            var sNewDate = $"{dNewDate:MM/dd/yyyy}";
            return sNewDate;
        }

        public int RandomFromRange(int min, int max)
        {
            DateTime now = DateTime.Now;
            var exclude = new HashSet<int>() { 5, 7, 17, 23 };
            var range = Enumerable.Range(min, max).Where(i => !exclude.Contains(i));

            var rand = new System.Random();
            int index = rand.Next(0, 100 - exclude.Count);
            return range.ElementAt(index);
        }

        public string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString().ToUpper();
        }

        public static void GetAvailableCustomers(int count, CustomerType customerType)
        {
            var loan = new LoanProperties();
            loan.CustomerType = customerType.ToString();
            string state = String.Empty;
            var sliceString = customerType.ToString().Split('_');
            var type = sliceString[1];
            var configuration = TestConfigManager.Config;

            switch (type)
            {
                case "ApprovedPersonalWithBalance":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "UT":
                            CustomerManager.UT_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "ID":
                            CustomerManager.ID_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "WI":
                            CustomerManager.WI_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "TX":
                            CustomerManager.TX_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "MO":
                            CustomerManager.MO_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;

                    }
                    break;

                case "ApprovedPersonalPastDue":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = true;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "UT":
                            CustomerManager.UT_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "ID":
                            CustomerManager.ID_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "WI":
                            CustomerManager.WI_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "TX":
                            CustomerManager.TX_ApprovedPersonalWithBalance = CacheHelper(loan);
                            break;
                        case "MO":
                            CustomerManager.MO_ApprovedPersonalPastDue = CacheHelper(loan);
                            break;
                    }
                    break;
                case "ApprovedPaydayWithBalance":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Payday";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "AL":
                            CustomerManager.AL_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                    }
                    break;

                case "ApprovedPaydayZeroBalance":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = false;
                    loan.LoanType = "Payday";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "AL":
                            CustomerManager.AL_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_ApprovedPaydayZeroBalance = CacheHelper(loan);
                            break;
                    }
                    break;
                

                case "ApprovedPaydayPastDue":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = true;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Payday";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "AL":
                            CustomerManager.AL_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_ApprovedPaydayWithBalance = CacheHelper(loan);
                            break;
                    }
                    break;



                case "Turndown":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "Turndown";
                    loan.WithBalance = false;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "MO":
                            CustomerManager.MO_Turndown = CacheHelper(loan);
                            break;
                        case "AL":
                            CustomerManager.AL_Turndown = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_Turndown = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_Turndown = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_Turndown = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_Turndown = CacheHelper(loan);
                            break;
                        case "ID":
                            CustomerManager.ID_Turndown = CacheHelper(loan);
                            break;
                        case "WI":
                            CustomerManager.WI_Turndown = CacheHelper(loan);
                            break;
                        case "TX":
                            CustomerManager.TX_Turndown = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_Turndown = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_Turndown = CacheHelper(loan);
                            break;
                    }
                    break;


                case "TurndownLead":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "TurndownLead";
                    loan.WithBalance = false;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "MO":
                            CustomerManager.MO_TurndownLead = CacheHelper(loan);
                            break;
                        case "AL":
                            CustomerManager.AL_TurndownLead = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_TurndownLead = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_TurndownLead = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_TurndownLead = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_TurndownLead = CacheHelper(loan);
                            break;
                        case "ID":
                            CustomerManager.ID_TurndownLead = CacheHelper(loan);
                            break;
                        case "WI":
                            CustomerManager.WI_TurndownLead = CacheHelper(loan);
                            break;
                        case "TX":
                            CustomerManager.TX_TurndownLead = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_TurndownLead = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_TurndownLead = CacheHelper(loan);
                            break;
                    }
                    break;

                case "PendingAll":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = false;
                    loan.CustomerLastName = "PendingAll";
                    loan.WithBalance = false;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    switch (state)
                    {
                        case "MO":
                            CustomerManager.AL_PendingAll = CacheHelper(loan);
                            break;
                        case "AL":
                            CustomerManager.AL_PendingAll = CacheHelper(loan);
                            break;
                        case "UT":
                            CustomerManager.UT_PendingAll = CacheHelper(loan);
                            break;
                        case "WY":
                            CustomerManager.WY_PendingAll = CacheHelper(loan);
                            break;
                        case "AK":
                            CustomerManager.AK_PendingAll = CacheHelper(loan);
                            break;
                        case "KS":
                            CustomerManager.KS_PendingAll = CacheHelper(loan);
                            break;
                        case "ID":
                            CustomerManager.ID_PendingAll = CacheHelper(loan);
                            break;
                        case "WI":
                            CustomerManager.WI_PendingAll = CacheHelper(loan);
                            break;
                        case "TX":
                            CustomerManager.TX_PendingAll = CacheHelper(loan);
                            break;
                        case "CA":
                            CustomerManager.CA_Turndown = CacheHelper(loan);
                            break;
                        case "HI":
                            CustomerManager.HI_PendingAll = CacheHelper(loan);
                            break;
                    }
                    break;

                case "65PlusPersonal":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = true;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Personal";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    CustomerManager.UT_65PlusPersonal = CacheHelper(loan);
                    break;

                case "70PlusPayday":
                    loan.CustomerState = loan.CustomerType.Substring(0, 2);
                    loan.PastDue = true;
                    loan.CustomerLastName = "Approved";
                    loan.WithBalance = true;
                    loan.LoanType = "Payday";
                    state = loan.CustomerType.Substring(0, 2);
                    loan.Location_ID = state + "O";
                    CustomerManager.UT_70PlusPayday = CacheHelper(loan);
                    break;
            }
        }

        private static List<object> CacheHelper(LoanProperties loan)
        {
            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.PROD)
            {
                return null;
            }
            List<object> tempList;
            string pathFileName;
            var fileName = loan.CustomerType;
            var configuration = TestConfigManager.Config;
            pathFileName = configuration.RootPath + $"{configuration.SystemUnderTest.ToString()}//{configuration.Developer}//"+ fileName + @".json";
            
            if (configuration.BuildCustomerCache)
            {
                tempList = FetchCustomerList(loan);
                CustomerManager.WriteToJsonFile(pathFileName, tempList, false);
            }

            var fileString = CustomerManager.ReadFromJsonFile(pathFileName);
            tempList = JsonConvert.DeserializeObject<List<object>>(fileString);
            return tempList;
        }

        private static List<object> FetchCustomerList(LoanProperties loan)
        { 
            var customerQuery = BuildCustomerQuery(loan);
            var customerList = ExecuteCustomerQuery(customerQuery);
            Debug.WriteLine(loan.CustomerType + " ==> " + customerList.Count);
            return customerList;
        }

        //private static List<object> FetchCustomersWithAgedLoans(LoanType loanType, string agingTime)
        //{

        //    var customerQuery = BuildAgedLoanCustomerQuery(loanType, agingTime);
        //    var customerList = ExecutePendingLoanCustomerQuery(customerQuery);
        //    return customerList;
        //}
        //private static List<object> FetchCustomersWithPendingApplication()
        //{
        //    var customerQuery = BuildLoanPendingCustomerQuery();
        //    var customerList = ExecutePendingLoanCustomerQuery(customerQuery);
        //    return customerList;
        //}

        //private static List<object> FetchTestCustomer(int count, string lastName)
        //{
        //    var customerQuery = BuildCustomerQuery(count, lastName);
        //    var customerList = ExecuteCustomerQuery(customerQuery);
        //    return customerList;
        //}

        //public static List<object> FetchCustomersWithLoans(int count, LoanType loanType)
        //{
        //    var customerLoanQuery = BuildCustomerLoanQuery(count, loanType);
        //    var customerList = ExecuteCustomerQuery(customerLoanQuery);
        //    return customerList;
        //}

        public static CurrentUser GetCustomer(CustomerType customerType)
        {
            switch (customerType)
            {
                case CustomerType.UT_70PlusPayday:
                    return ParseRandomCustomer(CustomerManager.UT_70PlusPayday);
                case CustomerType.UT_65PlusPersonal:
                    return ParseRandomCustomer(CustomerManager.UT_65PlusPersonal);
                case CustomerType.UT_Turndown:
                    return ParseRandomCustomer(CustomerManager.UT_Turndown);
                case CustomerType.UT_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPaydayPastDue);
                case CustomerType.UT_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPaydayWithBalance);
                case CustomerType.UT_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPaydayZeroBalance);
                case CustomerType.UT_ApprovedPersonalPastDue:
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPersonalPastDue);
                case CustomerType.UT_ApprovedPersonalWithBalance: 
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPersonalWithBalance);
                case CustomerType.UT_ApprovedPersonalZeroBalance:
                    return ParseRandomCustomer(CustomerManager.UT_ApprovedPersonalZeroBalance);
                case CustomerType.UT_PendingAll:
                    return ParseRandomCustomer(CustomerManager.UT_PendingAll);
                case CustomerType.UT_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.UT_TurndownLead);
                case CustomerType.WY_Turndown:
                    return ParseRandomCustomer(CustomerManager.WY_Turndown);
                case CustomerType.WY_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.WY_ApprovedPaydayPastDue);
                case CustomerType.WY_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.WY_ApprovedPaydayWithBalance);
                case CustomerType.WY_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.WY_ApprovedPaydayZeroBalance);
                case CustomerType.WY_PendingAll:
                    return ParseRandomCustomer(CustomerManager.WY_PendingAll);
                case CustomerType.WY_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.WY_TurndownLead);
                case CustomerType.TX_Turndown:
                    return ParseRandomCustomer(CustomerManager.TX_Turndown);
                case CustomerType.TX_ApprovedPersonalPastDue:
                    return ParseRandomCustomer(CustomerManager.TX_ApprovedPersonalPastDue);
                case CustomerType.TX_ApprovedPersonalWithBalance:
                    return ParseRandomCustomer(CustomerManager.TX_ApprovedPersonalWithBalance);
                case CustomerType.TX_ApprovedPersonalZeroBalance:
                    return ParseRandomCustomer(CustomerManager.TX_ApprovedPersonalZeroBalance);
                case CustomerType.TX_PendingAll:
                    return ParseRandomCustomer(CustomerManager.TX_PendingAll);
                case CustomerType.TX_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.TX_TurndownLead);
                case CustomerType.ID_Turndown:
                    return ParseRandomCustomer(CustomerManager.ID_Turndown);
                case CustomerType.ID_ApprovedPersonalPastDue:
                    return ParseRandomCustomer(CustomerManager.ID_ApprovedPersonalPastDue);
                case CustomerType.ID_ApprovedPersonalWithBalance:
                    return ParseRandomCustomer(CustomerManager.ID_ApprovedPersonalWithBalance);
                case CustomerType.ID_ApprovedPersonalZeroBalance:
                    return ParseRandomCustomer(CustomerManager.ID_ApprovedPersonalZeroBalance);
                case CustomerType.ID_PendingAll:
                    return ParseRandomCustomer(CustomerManager.ID_PendingAll);
                case CustomerType.ID_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.ID_TurndownLead);
                case CustomerType.KS_Turndown:
                    return ParseRandomCustomer(CustomerManager.KS_Turndown);
                case CustomerType.KS_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.KS_TurndownLead);
                case CustomerType.KS_PendingAll:
                    return ParseRandomCustomer(CustomerManager.KS_PendingAll);
                case CustomerType.KS_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.KS_ApprovedPaydayPastDue);
                case CustomerType.KS_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.KS_ApprovedPaydayWithBalance);
                case CustomerType.KS_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.KS_ApprovedPaydayZeroBalance);
                case CustomerType.WI_ApprovedPersonalPastDue:
                    return ParseRandomCustomer(CustomerManager.WI_ApprovedPersonalPastDue);
                case CustomerType.WI_ApprovedPersonalWithBalance:
                    return ParseRandomCustomer(CustomerManager.WI_ApprovedPersonalWithBalance);
                case CustomerType.WI_ApprovedPersonalZeroBalance:
                    return ParseRandomCustomer(CustomerManager.WI_ApprovedPersonalZeroBalance);
                case CustomerType.WI_PendingAll:
                    return ParseRandomCustomer(CustomerManager.WI_PendingAll);
                case CustomerType.WI_Turndown:
                    return ParseRandomCustomer(CustomerManager.WI_Turndown);
                case CustomerType.WI_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.WI_TurndownLead);
                case CustomerType.AK_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.AK_TurndownLead);
                case CustomerType.AK_PendingAll:
                    return ParseRandomCustomer(CustomerManager.AK_PendingAll);
                case CustomerType.AK_Turndown:
                    return ParseRandomCustomer(CustomerManager.AK_Turndown);
                case CustomerType.AK_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.AK_ApprovedPaydayWithBalance);
                case CustomerType.AK_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.AK_ApprovedPaydayZeroBalance);
                case CustomerType.AK_ApprovedPaydayPastDue: 
                    return ParseRandomCustomer(CustomerManager.AK_ApprovedPaydayPastDue);
                case CustomerType.CA_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.CA_TurndownLead);
                case CustomerType.CA_PendingAll:
                    return ParseRandomCustomer(CustomerManager.CA_PendingAll);
                case CustomerType.CA_Turndown:
                    return ParseRandomCustomer(CustomerManager.CA_Turndown);
                case CustomerType.CA_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.CA_ApprovedPaydayWithBalance);
                case CustomerType.CA_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.CA_ApprovedPaydayZeroBalance);
                case CustomerType.CA_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.CA_ApprovedPaydayPastDue);
                case CustomerType.AL_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.AL_TurndownLead);
                case CustomerType.AL_PendingAll:
                    return ParseRandomCustomer(CustomerManager.AL_PendingAll);
                case CustomerType.AL_Turndown:
                    return ParseRandomCustomer(CustomerManager.AL_Turndown);
                case CustomerType.AL_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.AL_ApprovedPaydayWithBalance);
                case CustomerType.AL_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.AL_ApprovedPaydayZeroBalance);
                case CustomerType.AL_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.AL_ApprovedPaydayPastDue);
                case CustomerType.MO_ApprovedPersonalPastDue:
                    return ParseRandomCustomer(CustomerManager.MO_ApprovedPersonalPastDue);
                case CustomerType.MO_ApprovedPersonalWithBalance:
                    return ParseRandomCustomer(CustomerManager.MO_ApprovedPersonalWithBalance);
                case CustomerType.MO_ApprovedPersonalZeroBalance:
                    return ParseRandomCustomer(CustomerManager.MO_ApprovedPersonalZeroBalance);
                case CustomerType.MO_PendingAll:
                    return ParseRandomCustomer(CustomerManager.MO_PendingAll);
                case CustomerType.MO_Turndown:
                    return ParseRandomCustomer(CustomerManager.MO_Turndown);
                case CustomerType.MO_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.MO_TurndownLead);
                case CustomerType.HI_TurndownLead:
                    return ParseRandomCustomer(CustomerManager.HI_TurndownLead);
                case CustomerType.HI_PendingAll:
                    return ParseRandomCustomer(CustomerManager.HI_PendingAll);
                case CustomerType.HI_Turndown:
                    return ParseRandomCustomer(CustomerManager.HI_Turndown);
                case CustomerType.HI_ApprovedPaydayWithBalance:
                    return ParseRandomCustomer(CustomerManager.HI_ApprovedPaydayWithBalance);
                case CustomerType.HI_ApprovedPaydayZeroBalance:
                    return ParseRandomCustomer(CustomerManager.HI_ApprovedPaydayZeroBalance);
                case CustomerType.HI_ApprovedPaydayPastDue:
                    return ParseRandomCustomer(CustomerManager.HI_ApprovedPaydayPastDue);
            }
            throw new System.InvalidOperationException("Not Enough Users Create to meet your needs");
        }

        private static CurrentUser ParseRandomCustomer(List<object> customer)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var randomCustomer = customer[random.Next(0, customer.Count)];
            var returnValue = (CurrentUser)JsonConvert.DeserializeObject<CheckCityOnline.Users.CurrentUser>(randomCustomer.ToString());
            var birthDate = Convert.ToDateTime(returnValue.DateOfBirth);
            var dob = Convert.ToDateTime(returnValue.DateOfBirth);
            returnValue.BirthDate = birthDate.Date.ToString("MM-dd-yyyy");
            returnValue.DateOfBirth = dob.Date.ToString("MM-dd-yyyy");
            return returnValue;
        }

        private static List<object> ExecutePendingLoanCustomerQuery(string customerQuery)
        {
            List<object> customerList = new List<object>();
            var database = new Database();
            var connect = database.BuildConnectionString();
            SqlConnection databaseConnection = new System.Data.SqlClient.SqlConnection(connect);
            databaseConnection.Open();
            SqlCommand cmd = new SqlCommand(customerQuery, databaseConnection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.HasRows)
            {
                while (reader.Read())
                {
                    var customer = new CurrentUser();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var columnName = reader.GetName(i);
                        var columnValue = reader[i].ToString();
                        switch (columnName)
                        {
                            case "Email": customer.Email = columnValue; break;
                            case "Password": customer.ContactPassword = columnValue; break;
                            case "Id": customer.Id = columnValue; break;
                            case "DateOfBirth": customer.DateOfBirth = columnValue.Substring(0, columnValue.Length - 12); break;
                            case "State": customer.State = columnValue; break;
                            case "UserName": customer.UserName = columnValue; break;
                        }
                    }

                    switch (TestConfigManager.Config.SystemUnderTest)
                    {
                        case SystemUnderTest.QA:
                            customer.RunEnvironment = "QA";
                            break;
                        case SystemUnderTest.STAGE:
                            customer.RunEnvironment = "STAGE";
                            break;
                    }
                    customer = ExtendCustomerAttributes(customer);
                    customerList.Add(customer);
                }
                reader.NextResult();
            }
            return customerList;
        }

        private static List<object> ExecuteCustomerQuery(string customerQuery)
        {
            List<object> customerList = new List<object>();
            var database = new Database();
            var connect = database.BuildConnectionString();
            SqlConnection databaseConnection = new System.Data.SqlClient.SqlConnection(connect);
            @databaseConnection.Open();
            SqlCommand cmd = new SqlCommand(customerQuery, databaseConnection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.HasRows)
            {
                while (reader.Read())
                {
                    var customer = new CurrentUser();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var columnName = reader.GetName(i);
                        var columnValue = reader[i].ToString();
                        switch (columnName)
                        {
                            case "Id": customer.Id = columnValue; break;
                            case "UserName": customer.UserName = columnValue; break;
                            case "FirstName": customer.FirstName = columnValue; break;
                            case "LastName": customer.LastName = columnValue; break;
                            case "Email": customer.Email = columnValue; break;
                            case "Signature": customer.Signature = columnValue; break;
                            case "DateOfBirth": customer.DateOfBirth = columnValue; break;
                            case "ZipCode": customer.ZipCode = columnValue; break;
                            case "MonthlyIncome": customer.MonthlyIncome = columnValue; break;
                            case "EmployersCity": customer.EmployerCity = columnValue; break;
                            case "NameOfCompany": customer.NameOfCompany = columnValue; break;
                            case "PhoneNumber": customer.PhoneNumber = columnValue; break;
                            case "EmployersZipCode": customer.EmployerZipcode = columnValue; break;
                            case "RoutingNumber": customer.RoutingNumber = columnValue; break;
                            case "BankAccountNumber": customer.BankAccountNumber = columnValue; break;
                            case "IdentificationNumber": customer.IdentificationNumber = columnValue; break;
                            case "SocialSecurityNumber": customer.SocialSecurityNumber = columnValue; break;
                            case "AdditionalMonthlyIncome": customer.AdditionalMonthlyIncome = columnValue; break;
                            case "AdditionalEmployersCity": customer.AdditionalEmployersCity = columnValue; break;
                            case "AdditionalPhoneNumber": customer.AdditionalPhoneNumber = columnValue; break;
                            case "AdditionalNameOfCompany": customer.AdditionalNameOfCompany = columnValue; break;
                            case "AdditionalEmployersZipCode": customer.AdditionalEmployersZipCode = columnValue; break;
                            case "ExpirationDate": customer.ExpirationDate = columnValue; break;
                            case "StreetAddress": customer.StreetAddress = columnValue; break;
                            case "ContactPassword": customer.ContactPassword = columnValue; break;
                            case "ContactPhoneNumber": customer.ContactPhoneNumber = columnValue; break;
                            case "VerificationCode": customer.VerificationCode = columnValue; break;
                            case "PrimaryPhone": customer.PrimaryPhone = columnValue; break;
                            case "State": customer.State = columnValue; break;
                            case "Password": customer.Password = columnValue; break;
                            case "AdverseActionCustomerID":
                                customer.AdverseActionCustomerID = columnValue;
                                break;
                            case "AdverseActionCustomerEmail":
                                customer.AdverseActionCustomerEmail = columnValue;
                                break;
                        }
                    }
                    switch (TestConfigManager.Config.SystemUnderTest)
                    {
                        case SystemUnderTest.QA:
                            customer.RunEnvironment = "QA";
                            break;
                        case SystemUnderTest.STAGE:
                            customer.RunEnvironment = "STAGE";
                            break;
                    }
                    ExtendCustomerAttributes(customer);
                    customerList.Add(customer);
                }
                reader.NextResult();
            }
            return customerList;
        }

        private static CurrentUser ExtendCustomerAttributes(CurrentUser customer)
        {
            customer.Years = "11";
            customer.Months = "11";
            customer.BirthDate = customer.DateOfBirth;
            customer.Password = customer.ContactPassword;
            if (customer.UserName == null)
                customer.UserName = customer.Email;
            if (customer.Password == "" || customer.Password == null)
                customer.Password = "Diehard1!";
            customer.NewPassword = "Diehard2!";
            customer.Signature = customer.FirstName + " " + customer.LastName;
            customer.SecurityQuestion1 = Users.SecurityQuestion.One;
            customer.SecurityQuestion2 = Users.SecurityQuestion.Two;
            customer.SecurityQuestion3 = Users.SecurityQuestion.Three;
            customer.SecurityQuestion4 = Users.SecurityQuestion.Four;
            customer.PaymentMethodDB = "Bank Card";
            customer.PaymentMethodAch = "ACH";
            customer.CardType = "Visa";
            customer.Last4Digits = "9370";
            customer.PaymentLarge = "100";
            customer.PaymentSmall = "25";
            customer.PaymentIncorrect = ".25";
            return customer;
        }

        public static CurrentUser GetReleaseTestUser(CustomerTypeReleaseTest releaseTestCustomerType, TestConfiguration config)
        {
            var customer = new Users.CurrentUser();

            switch (releaseTestCustomerType)
            {
                case CustomerTypeReleaseTest.UT_PendingAll:
                    switch (config.SystemUnderTest)
                    {
                        case SystemUnderTest.PROD:
                            customer.UserName = "TestingApprovePersonalUT@checkcity.com";
                            customer.Email = "TestingApprovePersonalUT@checkcity.com";
                            customer.Password = "TestApproved45";
                            break;
                        case SystemUnderTest.QA:
                        case SystemUnderTest.STAGE:
                            customer.UserName = "TestingApprovePersonalUT@msgnext.com";
                            customer.Email = "TestingApprovePersonalUT@msgnext.com";
                            customer.Password = "Diehard1!";
                            break;
                    }
                    break;
                case CustomerTypeReleaseTest.TX_ApprovedInstallment:
                    switch (config.SystemUnderTest)
                    {
                        case SystemUnderTest.PROD:
                            customer.UserName = "TestingApproveInstallmentTX@checkcity.com";
                            customer.Email = "TestingApproveInstallmentTX@checkcity.com";
                            customer.Password = "TestApproved55";
                            break;
                        case SystemUnderTest.QA:
                        case SystemUnderTest.STAGE:
                            customer.UserName = "TestingApproveInstallmentTX@msgnext.com";
                            customer.Email = "TestingApproveInstallmentTX@msgnext.com";
                            customer.Password = "Diehard1!";
                            break;
                    }
                    break;
            }
            customer.RunEnvironment = config.SystemUnderTest.ToString();
            return customer;
        }
    }
}
