﻿using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using CheckCityOnline.Pages;

namespace CheckCityOnline.Tests
{
    internal class DocumentUploadServices : BaseApplicationPage
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public DocumentUploadServices(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            if (user.RunEnvironment == "PROD")
                TestEnvironment.Url = Url.Production.DocumentUploadServices;
            else if (user.RunEnvironment == "STAGE")
                TestEnvironment.Url = Url.Staging.DocumentUploadServices;
            else if (user.RunEnvironment == "QA")
                TestEnvironment.Url = Url.Test.DocumentUploadServices;
        }

        public bool IsDocumentUploadVisible
        {
            get
            {
                var isVisible = Driver.FindElement(By.XPath(Elements.DocumentUploadServices.ScreenText.DocumentUploadServices)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, " Validate the message 'Document Upload Services' is displayed. ");
                Logger.Trace($"Document Upload Services loaded =>{isVisible}");
                return isVisible;
            }
        }


        public void AssertDocumentUploadService()
        {
            WaitForElement(5, Elements.DocumentUploadServices.ScreenText.DocumentUploadServices);
            ExplicitWait(3);
            Assert.IsTrue(IsDocumentUploadVisible, ErrorStrings.DocumentUploadServicesNotVisible);
        }

    }
}
