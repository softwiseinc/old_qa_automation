﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using AutomationResources;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
// ReSharper disable TooWideLocalVariableScope

namespace CheckCityOnline.Tests
{
    public partial class BaseTest
    {
        public static int ApprovedPersonalHelper(int customerCount, CustomerType customerType)
        {
            var customer = Users.GenNewRandomMember(customerType);
            var loanAmount = customerCount + 1;
            if (customerType.ToString().ToLower().Contains("zero"))
            {
                if (CreatePersonalZeroBalanceCustomer(customer))
                    customerCount += 1;
            }
            else
            {
                if (CreatePersonalLoanCustomer(null, customer, loanAmount))
                    customerCount += 1;
            }
            DebugAndLogger("Customer Type: " + customerType);
            DebugAndLogger("Email: " + customer.Email);
            DebugAndLogger(customer.FirstName + " " + customer.LastName + " " + customer.State + " " + customer.ZipCode + " Customer info ");
            DebugAndLogger("Customer count: " + customerCount);
            return customerCount;
        }

        public static int ApprovedPaydayHelper(int customerCount, CustomerType customerType)
        {
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            var driver = webDriverFactory.Create(BrowserType.Chrome);
            var customer = Users.GenNewRandomMember(customerType);
            driver.Manage().Window.Maximize();
            try
            {
                if (CreatePaydayLoanCustomer(driver, customer, customerCount))
                {
                    customerCount += 1;
                    DebugAndLogger("Customer Type: " + customerType);
                    DebugAndLogger("Email: " + customer.Email);
                    DebugAndLogger(customer.FirstName + " " + customer.LastName + " " + customer.State + " " + customer.ZipCode + " Customer info ");
                    DebugAndLogger("Customer count: " + customerCount);
                }
            }
            catch (Exception)
            {
                DebugAndLogger("**********************************************************************************************");
                DebugAndLogger($"Try again on the next loop go-a-round.");
                DebugAndLogger("**********************************************************************************************");
                driver?.Quit();
                Logger.Trace("Browser stopped successfully.");
            }

            
            
            return customerCount;
        }

        private static Users.CurrentUser SetRunEnvironment(Users.CurrentUser customer)
        {
            switch (TestConfigManager.Config.SystemUnderTest)
            {
                case SystemUnderTest.QA:
                    customer.RunEnvironment = "QA";
                    break;
                case SystemUnderTest.STAGE:
                    customer.RunEnvironment = "STAGE";
                    break;
            }

            return customer;
        }


        public static bool CreatePaydayLoanCustomer(IWebDriver driver, Users.CurrentUser customer, int approvedPaydayCount)
        {
            var testConfiguration = TestConfigManager.Config;
            bool isLoanCreated = false;
            customer = SetRunEnvironment(customer);
            var loanApproval = new LoanApprovalPage(driver, customer);
            var loanDetails = new LoanDetailsPage(driver, customer);
            var loanDisclosure = new LoanDisclosure(driver, customer);
            var loanThankYou = new LoanThankYouPage(driver, customer);

            PaydayLoanCustomerCreate(driver, customer);
            if (customer.LastName.Contains("Approved") && !customer.Type.Contains("Zero"))
            {
                if (driver.Url.Contains("Loan_Approval.aspx")) 
                {

                    var loanAmount = 500 + approvedPaydayCount;
                    if (loanAmount >= 1000)
                        loanAmount = 499;
                    loanApproval.EnterLoanAmount(loanAmount);
                    loanApproval.ClickGetAmount();
                    loanDetails.ClickContinue(7);
                    loanDetails.ExplicitWait(3);
                    

                    switch (customer.State)
                    {
                        case "WY":
                        case "HI":
                        case "AL":
                            loanDisclosure.ClickIAgreeButtonOne();
                            loanDisclosure.ExplicitWait(4);
                            break;
                        case "AK":
                        case "CA":
                        case "MO":
                            loanDisclosure.ClickIAgreeButtonOne();
                            loanDisclosure.ExplicitWait(4);
                            loanDisclosure.ClickIAgreeButtonTwo();
                            loanDisclosure.ExplicitWait(4);
                            break;
                        case "KS":
                            if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.QA)
                            {
                                loanDisclosure.ClickIAgreeButtonOne();
                                loanDisclosure.ExplicitWait(4);
                                loanDisclosure.ClickIAgreeButtonTwo();
                                loanDisclosure.ExplicitWait(4);
                            }
                            else
                            {
                                loanDisclosure.SignKansasConsumerLoanAgreement(customer);
                                loanDisclosure.ClickSignKansasConsumerLoanAgreement();
                                loanDisclosure.ExplicitWait(4);
                                loanDisclosure.SignKansasNoticeToConsumer(customer);
                                loanDisclosure.ClickSignKansasNoticeToConsumerAgreement();
                                loanDisclosure.ExplicitWait(4);
                            }
                            
                            break;
                        default:
                            loanDisclosure.ClickIAgreeButton();
                            break;
                    }
                   
                    loanDisclosure.ExplicitWait(3);
                    loanThankYou.ClickYourOpinionCounts();
                    List<IWebElement> elementList = new List<IWebElement>();
                    elementList.AddRange(driver.FindElements(By.XPath("//*[contains(@class,'ls-signed')]")));
                    if (elementList.Count > 0)
                        isLoanCreated = true;
                    loanThankYou.ClickEndMySession();
                }

                if (driver.Url.Contains("NewLoan.aspx"))
                {
                    var loanAmount = 100;
                    switch (customer.State)
                    {
                        case "NV":
                            loanApproval.EnterNevadaLoanAmount(loanAmount);
                            loanApproval.EnterNevadaLoanAmount(loanAmount);
                            loanDisclosure.SignNevadaPaydayAgreement(customer);
                            loanDisclosure.ExplicitWait(2);
                            loanDisclosure.ClickNevadaIAgree();
                            loanDisclosure.ExplicitWait(7);
                            loanDisclosure.SignLoanDisclosure(customer);
                            loanDisclosure.ClickNevadaDisclosureIAgree();
                            loanDisclosure.ExplicitWait(4);
                            loanDisclosure.AssertLoanPendingApproval();
                            isLoanCreated = true;
                            break;
                    }
                }
            }
            else
            {
                isLoanCreated = loanApproval.GenerateCustomerStatusHelper(driver, customer);
            }
            if (testConfiguration.GenerateCustomers)
            {
                driver?.Quit();
                Logger.Trace("Browser stopped successfully.");
            }
            return isLoanCreated;
        }

        public static bool CreatePaydayZeroBalanceCustomer(IWebDriver driver, Users.CurrentUser customer,
            int customerCount)
        {
            customer = SetRunEnvironment(customer);
            CreatePaydayLoanCustomer(driver, customer, customerCount);
            return true;
        }

        public static bool CreatePersonalLoanCustomer(IWebDriver driver, Users.CurrentUser customer,
            int approvedPersonalCount)
        {
            var testConfiguration = TestConfigManager.Config;
            var loanApproval = new LoanApprovalPage(driver, customer);
            var loanDetails = new LoanDetailsPage(driver, customer);
            var loanDisclosure = new LoanDisclosure(driver, customer);
            var loanThankYou = new LoanThankYouPage(driver, customer);
            bool isLoanCreated = false;
            
            customer = SetRunEnvironment(customer);
            PersonalLoanCustomerCreate(driver, customer);
            if (customer.LastName.Contains("Approved") && !customer.Type.Contains("Zero"))
            {
                if (driver.Url.Contains("Loan_Approval.aspx"))
                {
                    
                    if (customer.LastName == "Approved")
                    {
                        var loanAmount = 500 + approvedPersonalCount;
                        if (loanAmount >= 1000)
                            loanAmount = 499;

                        loanApproval.EnterLoanAmount(loanAmount);
                        loanApproval.ClickGetAmount();
                        loanDetails.ClickContinue(10);
                        loanDisclosure.ExplicitWait(5);

                        switch (customer.State)
                        {
                            case "WI":
                                loanDisclosure.SignPersonalLoanAgreement(customer);
                                loanDisclosure.ClickIAgreeButtonOne();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.SignAchAuthorization(customer);
                                loanDisclosure.ClickIAgreeButtonTwo();
                                break;
                            case "KS":
                                loanDisclosure.SignKansasConsumerLoanAgreement(customer);
                                loanDisclosure.ClickSignKansasConsumerLoanAgreement();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.SignKansasNoticeToConsumer(customer);
                                loanDisclosure.ClickSignKansasNoticeToConsumerAgreement();
                                break;

                            case "CA":
                            case "NV":
                            case "AK":
                                loanDisclosure.ClickIAgreeButtonOne();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonTwo();
                                break;

                            case "TX":
                                loanDisclosure.ClickIAgreeButtonOne();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonTwo();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonThree();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonFour();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonFive();
                                loanDisclosure.ExplicitWait(5);
                                break;
                            default:
                                loanDisclosure.ClickIAgreeButtonOne();
                                loanDisclosure.ExplicitWait(5);
                                loanDisclosure.ClickIAgreeButtonTwo();
                                break;
                        }

                        loanDisclosure.ExplicitWait(5);
                        loanThankYou.ClickYourOpinionCounts();
                        List<IWebElement> elementList = new List<IWebElement>();
                        elementList.AddRange(driver.FindElements(By.XPath("//*[contains(@class,'ls-signed')]")));
                        if (elementList.Count > 0)
                            isLoanCreated = true;
                        loanThankYou.ClickEndMySession();
                        Logger.Trace("Loan Created");
                    }
                }
            }
            else
            {
                isLoanCreated = loanApproval.GenerateCustomerStatusHelper(driver, customer);
            }

            if (testConfiguration.GenerateCustomers)
            {
                driver?.Quit();
                Logger.Trace("Browser stopped successfully.");
            }
            return isLoanCreated;
        }




        public static bool CreatePersonalZeroBalanceCustomer(Users.CurrentUser customer)
        {
            customer = SetRunEnvironment(customer);
            var driver = PersonalLoanCustomerCreate(customer);
            driver?.Quit();
            Logger.Trace("Browser stopped successfully.");
            return true;
        }

        protected static IWebDriver PersonalLoanCustomerCreate(Users.CurrentUser customer)
        {
            customer = SetRunEnvironment(customer);
            var driver = CreateWebDriver();
            var memberLoginPage = new MemberLoginPage(driver, customer);
            var applyPage = new ApplyPage(driver, customer);
            memberLoginPage.LogInPage(customer);

            try
            {
                applyPage.ApplyPersonalLoan(customer);
            }
            catch (Exception e)
            {
                Logger.Trace("user create failed." + e);
                driver?.Quit();
                return null;
            }
            return driver;
        }

        protected static void PersonalLoanCustomerCreate(IWebDriver driver, Users.CurrentUser customer)
        {
            customer = SetRunEnvironment(customer);
            var memberLoginPage = new MemberLoginPage(driver, customer);
            var applyPage = new ApplyPage(driver, customer);
            memberLoginPage.LogInPage(customer);
            applyPage.ApplyPersonalLoan(customer);
        }

        protected static IWebDriver PaydayLoanCustomerCreate(Users.CurrentUser customer)
        {
            customer = SetRunEnvironment(customer);
            var driver = CreateWebDriver();
            var memberLoginPage = new MemberLoginPage(driver, customer);
            var applyPage = new ApplyPage(driver, customer);
            memberLoginPage.LogInPage(customer);
            try
            {
                applyPage.ApplyPayDayLoan(customer);
            }
            catch (Exception)
            {
                Logger.Trace("user create failed.");
                driver?.Quit();
                return null;
            }

            return driver;
        }

        protected static void PaydayLoanCustomerCreate(IWebDriver driver, Users.CurrentUser customer)
        {
            customer = SetRunEnvironment(customer);
            var memberLoginPage = new MemberLoginPage(driver, customer);
            var applyPage = new ApplyPage(driver, customer);
            memberLoginPage.LogInPage(customer);
            applyPage.ApplyPayDayLoan(customer);
        }

        private static IWebDriver CreateWebDriver()
        {
            var factory = new WebDriverFactory(null,null);
            IWebDriver driver = factory.Create(BrowserType.Chrome);
            driver.Manage().Window.Maximize();
            return driver;
        }

        private static int CreateNewCustomers()
        {
            var count = 0;
            if (!string.IsNullOrEmpty(TestContext.Parameters["GenerateCustomers"]))
                TestConfigManager.Config.GenerateCustomers = bool.Parse(TestContext.Parameters["GenerateCustomers"]);

            if (!string.IsNullOrEmpty(TestContext.Parameters["RemoteUrl"]))
                TestConfigManager.Config.RemoteUrl = TestContext.Parameters["RemoteUrl"];

            if (!string.IsNullOrEmpty(TestContext.Parameters["RunHeadless"]))
                TestConfigManager.Config.RunHeadless = TestContext.Parameters["RunHeadless"];

            if (!string.IsNullOrEmpty(TestContext.Parameters["CatchAllDomain"]))
                TestConfigManager.Config.CatchAllDomain = TestContext.Parameters["CatchAllDomain"];

            if (!string.IsNullOrEmpty(TestContext.Parameters["GenerateCustomersCount"]))
            {
                TestConfigManager.Config.GenerateCustomersCount = Int32.Parse(TestContext.Parameters["GenerateCustomersCount"]);
                count = TestConfigManager.Config.GenerateCustomersCount;
            }
            else
            {
                count = TestConfigManager.Config.GenerateCustomersCount;
            }

            AutomationResources.WebDriverFactory.GenerateCustomersHandler();
            if (TestConfigManager.Config.GenerateCustomers.ToString().ToLower() == "true")
            {
                PendingAllGenerator();
                ApprovedPersonalWithBalanceGenerator();
                ApprovedPersonalZeroBalanceGenerator();
                ApprovedPaydayWithBalanceGenerator();
                ApprovedPaydayZeroBalanceGenerator();
                TurndownLeadGenerator();
                TurndownGenerator();

            }
            return count;
        }

        private static bool ApprovedPaydayZeroBalanceGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while (count <= configuration.ApprovedPaydayZeroBalanceGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("ApprovedPaydayZeroBalance"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} ApprovedPaydayZeroBalanceGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayZeroBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            
                            else
                            {
                                DebugAndLogger("Oops! Try once more : ApprovedPaydayZeroBalanceGenerator : " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayZeroBalanceGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more ApprovedPaydayZeroBalanceGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayZeroBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                }
                count++;
            }
            DebugAndLogger($"-----------------[ApprovedPaydayZeroBalance Customer Generation Done! ]--------------------");
            return true;
        }

        private static bool ApprovedPersonalWithBalanceGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;

            while (count <= configuration.ApprovedPersonalWithBalanceGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                   customer = Users.GenNewRandomMember(customerType);
                   if (customerType.ToString().Contains("ApprovedPersonalWithBalance"))
                   {
                       DebugAndLogger($"---------------[ Loop count: {count} ApprovedPersonalWithBalanceGenerator: Customer type: {customer.Type} ]------------------");
                       driver = webDriverFactory.Create(BrowserType.Chrome);
                       driver.Manage().Window.Maximize();
                       try
                       {
                           if (CreatePersonalLoanCustomer(driver, customer, configuration.ApprovedPersonalWithBalanceGenerator))
                               CustomerActionDebugLog(customerType, customer);
                           else
                           {
                               DebugAndLogger("Oops! Try once more ApprovedPersonalWithBalanceGenerator: " + customerType);
                               driver = RestartDriver(driver, webDriverFactory);
                               customer = Users.GenNewRandomMember(customerType);
                               if (CreatePersonalLoanCustomer(driver, customer, configuration.ApprovedPersonalWithBalanceGenerator))
                                   CustomerActionDebugLog(customerType, customer);
                           }
                       }
                       catch (Exception)
                       {
                           DebugAndLogger("Try once more ApprovedPersonalWithBalanceGenerator: " + customerType);
                           driver = RestartDriver(driver, webDriverFactory);
                           customer = Users.GenNewRandomMember(customerType);
                           if (CreatePersonalLoanCustomer(driver, customer, configuration.ApprovedPersonalWithBalanceGenerator))
                               CustomerActionDebugLog(customerType, customer);
                           else
                               driver?.Quit();
                       }
                   }
                }
                count++;
            }
            DebugAndLogger($"-----------------[ApprovedPersonalWithBalance Customer Generation Done! ]--------------------");
            return true;
        }

        private static bool ApprovedPersonalZeroBalanceGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while (count <= configuration.ApprovedPersonalZeroBalanceGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("ApprovedPersonalZeroBalance"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} ApprovedPersonalZeroBalanceGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePersonalLoanCustomer(driver, customer, configuration.ApprovedPersonalZeroBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                            {
                                DebugAndLogger("Oops! Try once more ApprovedPersonalZeroBalanceGenerator: " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePersonalLoanCustomer(driver, customer, configuration.ApprovedPersonalZeroBalanceGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more ApprovedPersonalZeroBalanceGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPersonalZeroBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                }
                count++;
            }
            DebugAndLogger($"-----------------[ApprovedPersonalZeroBalance Customer Generation Done! ]--------------------");
            return true;
        }
        private static bool ApprovedPaydayWithBalanceGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while (count <= configuration.ApprovedPaydayWithBalanceGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("ApprovedPaydayWithBalance"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} ApprovedPaydayWithBalanceGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayWithBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                            {
                                DebugAndLogger("Oops! Try once more ApprovedPaydayWithBalanceGenerator: " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayWithBalanceGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more ApprovedPaydayWithBalanceGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.ApprovedPaydayWithBalanceGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                }
                count++;
            }
            DebugAndLogger($"-----------------[ApprovedPaydayWithBalance Customer Generation Done! ]--------------------");
            return true;
        }

        private static bool TurndownLeadGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while (count <= configuration.TurndownLeadGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("TurndownLead"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} TurndownLeadGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownLeadGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                            {
                                DebugAndLogger("Oops! Try once more TurndownLeadGenerator: " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownLeadGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more TurndownLeadGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownLeadGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                }
                count++;
            }
            DebugAndLogger($"-----------------[PendingAll Customer Generation Done! ]--------------------");
            return true;
        }

        private static bool TurndownGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while(count <= configuration.TurndownGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("Turndown") && !customerType.ToString().Contains("TurndownLead"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} TurndownGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                            {
                                DebugAndLogger("Oops! Try once more TurndownGenerator: " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more TurndownGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.TurndownGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                    
                }
                count++;
            }
            DebugAndLogger($"---------------[Turndown Customer Generation Done! ]------------------");
            return true;
        }

        private static bool PendingAllGenerator()
        {
            var configuration = TestConfigManager.Config;
            Users.CurrentUser customer = null;
            WebDriverFactory webDriverFactory = new WebDriverFactory();
            IWebDriver driver = null;
            int count = 1;
            while (count <= configuration.PendingAllGenerator)
            {
                foreach (var customerType in Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>())
                {
                    customer = Users.GenNewRandomMember(customerType);
                    if (customerType.ToString().Contains("PendingAll"))
                    {
                        DebugAndLogger($"---------------[ Loop count: {count} PendingAllGenerator: Customer type: {customer.Type} ]------------------");
                        driver = webDriverFactory.Create(BrowserType.Chrome);
                        driver.Manage().Window.Maximize();
                        try
                        {
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.PendingAllGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                            {
                                DebugAndLogger("Oops! Try once more PendingAllGenerator: " + customerType);
                                driver = RestartDriver(driver, webDriverFactory);
                                customer = Users.GenNewRandomMember(customerType);
                                if (CreatePaydayLoanCustomer(driver, customer, configuration.PendingAllGenerator))
                                    CustomerActionDebugLog(customerType, customer);
                            }
                        }
                        catch (Exception)
                        {
                            DebugAndLogger("Try once more PendingAllGenerator: " + customerType);
                            driver = RestartDriver(driver, webDriverFactory);
                            customer = Users.GenNewRandomMember(customerType);
                            if (CreatePaydayLoanCustomer(driver, customer, configuration.PendingAllGenerator))
                                CustomerActionDebugLog(customerType, customer);
                            else
                                driver?.Quit();
                        }
                    }
                }
                count++;
            }
            DebugAndLogger($"-----------------[PendingAll Customer Generation Done! ]--------------------");
            return true;
        }

        private static IWebDriver RestartDriver(IWebDriver driver, WebDriverFactory webDriverFactory)
        {
            driver?.Quit();
            driver = webDriverFactory.Create(BrowserType.Chrome);
            driver.Manage().Window.Maximize();
            return driver;
        }

        private static void CustomerActionDebugLog(CustomerType customerType, Users.CurrentUser customer)
        {
            DebugAndLogger("Customer Type: " + customerType);
            DebugAndLogger("Email: " + customer.Email);
            DebugAndLogger(customer.FirstName + " " + customer.LastName + " " + customer.State + " " + customer.ZipCode +
                           " Customer info ");
            DebugAndLogger("Customer Count: " + Users.GetAvailableCustomerCount(customerType));
        }

        private static void DebugAndLogger(string debugString)
        {
            Debug.WriteLine(debugString);
            Logger.Debug(debugString);
        }

        private static void LoadTestCustomerLists(int count)
        {
            if (AutomationResources.TestConfigManager.Config.SystemUnderTest == SystemUnderTest.PROD)
                return;
            foreach (CustomerType customerType in Enum.GetValues(typeof(CustomerType)))
            {
                if (customerType != CustomerType.Random)
                    Users.GetAvailableCustomers(count, customerType);
            }
        }

        private static void SetupReporter()
        {
            Reporter.StartReporter();
            var testContext = TestContext.CurrentContext;
            Reporter.AddTestCaseMetadataToHtmlReport(testContext);
        }
        private void TakeScreenshotForTestFailure()
        {
            if (ScreenCapture != null)
            {
                ScreenCapture.CreateScreenCaptureIfTestFailed();
                Reporter.ReportTestOutcome(ScreenCapture.ScreenCaptureFilePath);
            }
            else
            {
                Reporter.ReportTestOutcome("");
            }
        }

        private void StopBrowser()
        {
            if (Driver == null)
                return;
            Driver.Quit();
            Driver = null;
            Logger.Trace("Browser stopped successfully.");
        }

        public static object CreateTurndownCustomer(IWebDriver driver, Users.CurrentUser customer)
        {

            var applyLoanPage = new ApplyLoan(driver, customer);

            applyLoanPage.NavigateToApplyLoanPage();
            //Personal Information
            applyLoanPage.FillPersonalInformationFirstName_Valid(customer);
            applyLoanPage.FillPersonalInformationLastName_Valid(customer);
            applyLoanPage.FillPersonalInformationEmail_Valid();
            applyLoanPage.FillPersonalInformationDob_Valid(customer);
            applyLoanPage.FillPersonalInformationZipCode_Valid(customer);
            applyLoanPage.ClickPersonalInformationContinueBtn();
            applyLoanPage.ClickPersonalInformationPaydayRadio();
            applyLoanPage.ClickPersonalInformationContinueBtn();

            //income information
            applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
            applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
            applyLoanPage.SelectIncomeInformationBiWeekly();
            applyLoanPage.SelectIncomeInformationPayDay();
            applyLoanPage.SelectIncomeInformationIncomeType();
            applyLoanPage.FillIncomeInformationCompanyName();
            applyLoanPage.FillIncomeInformationPhoneNumber();
            applyLoanPage.FillIncomeInformationCity();
            applyLoanPage.SelectIncomeInformationState();
            applyLoanPage.FillIncomeInformationZipCode();
            applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
            applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();
            applyLoanPage.SelectYesYouHaveDirectDeposit();

            //Bank Information
            applyLoanPage.FillBankInformationRouting();
            applyLoanPage.FillBankInformationBankAccount();
            applyLoanPage.FillBankInformationBankAccountCheck();
            applyLoanPage.SelectBankInformationTimeWithEmp_Year();
            applyLoanPage.SelectBankInformationTimeWithEmp_Month();
            applyLoanPage.SelectCheckingAccount();

            //Identification Number
            applyLoanPage.SelectIdentificationType();
            applyLoanPage.SelectIdentificationInformationState();
            applyLoanPage.FillIdentificationNumber();
            applyLoanPage.FillIdentificationExpirationDate();
            applyLoanPage.FillIdentificationSocialSecurity();

            //Contact Information
            applyLoanPage.FillContactInformationStreetAddress();
            applyLoanPage.SelectResidenceLengthYear();
            applyLoanPage.SelectResidenceLengthMonth();
            applyLoanPage.DoYouOwnOrRent();
            applyLoanPage.FillContactInformationPhoneNumber(customer);
            applyLoanPage.ClickContactInformationSendPhoneVerification();
            applyLoanPage.ClickYesAboutFuturePaymentsByEmail();

            applyLoanPage.ClickContactInformationOptInSms();
            applyLoanPage.ClickContactInformationOptInSmsMarketing();

            //Security Information
            applyLoanPage.FillSecurityInformationPassword(customer);
            // remove the next line after bug fix
            applyLoanPage.SelectIncomeInformationState();
            //Submit Information
            applyLoanPage.ClickSubmitInformationAgreeConsent();
            applyLoanPage.ClickSubmitInformationAgreeTerms();
            applyLoanPage.ClickSubmitInformationSubmitBtn();
            return true;
        }
    }
}
