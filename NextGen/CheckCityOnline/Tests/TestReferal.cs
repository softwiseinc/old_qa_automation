﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            
            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            //[Ignore("Test Broke", Until = "2020-03-10 12:00:00Z")]
            [TestCase(TestName = "ReferralPage_TermsConditions_8255_CA")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void ReferralPage_TermsConditions_8255_CA()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.CA_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var referralPage = new ReferralPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
                memberPage.AssertCustomerLoggedIn();
                memberPage.ClickUserNameMenu();
                referralPage.ClickReferAFriend();
                referralPage.ClickTermsAndConditionsLink();
            }

            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            // [Ignore("Test Broke", Until = "2020-03-10 12:00:00Z")]
            [TestCase(TestName = "ReferralPage_TermsConditions_8256_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void ReferralPage_TermsConditions_8256_UT()
            {
                //var user = Users.GetStagingUser005();
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var referralPage = new ReferralPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
                memberPage.AssertCustomerLoggedIn();
                memberPage.ClickUserNameMenu();
                referralPage.ClickReferAFriend();
                referralPage.ClickTermsAndConditionsLink();
            }
        }
    }
}