﻿using AutomationResources;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NUnit.Framework;


namespace CheckCityOnline.Tests
{
    partial class AllTests
    {
        private partial class AllTestsCases
        {
            
            [Test]
            [Category("ReleaseTest")]
            [TestCase(TestName = "GetPaydayLoanUT_ReleaseTest")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void GetPaydayLoanUT_ReleaseTest()
            {
                
                var configuration = TestConfigManager.Config;
                //Users.CurrentUser pendingAllCustomer = Users.GetReleaseTestUser(CustomerTypeReleaseTest.UT_PendingAll, configuration);
                var pendingUtPaydayLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, pendingUtPaydayLoanCustomer);
                var memberPage = new MemberPage(Driver, pendingUtPaydayLoanCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, pendingUtPaydayLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, pendingUtPaydayLoanCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, pendingUtPaydayLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, pendingUtPaydayLoanCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, pendingUtPaydayLoanCustomer);

                memberLoginPage.LogInUser(pendingUtPaydayLoanCustomer);
                Reporter.LogPassingTestStepToBugLogger(Status.Pass, "User is login was successful: " + pendingUtPaydayLoanCustomer.UserName);
                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();
                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();


                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                //memberPage.AssertMemberPageLoaded();
            }
        }
    }
}