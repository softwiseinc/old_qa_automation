﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "MemberPage_Approved_000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberPage_Approved_000()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
            }

            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "MemberPage_Turndown_000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberPage_Turndown_000()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_Turndown);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
            }

            [Test]
            [Category("RegressionTest")]
            [Repeat(2)]
            [TestCase(TestName = "MemberPage_TurndownLead_000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberPage_TurndownLead_000()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_TurndownLead);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
            }

            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "MemberPage_PendingAll_000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberPage_PendingAll_000()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_PendingAll);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
            }

            [Test]
            [Category("BrowserStack")]
            [Category("Regression")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "Loan_Application_Personal_Loan_Monthly_QA")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            public void Loan_Application_Personal_Loan_Monthly_QA()
            {
                var user = Users.GenNewRandomMember(CustomerType.UT_ApprovedPersonalWithBalance);
                //var memberLoginPage = new MemberLoginPage(Driver, user);
                var applyLoanPage = new ApplyPage(Driver, user);

                applyLoanPage.NavigateToApplyLoanPage();
                //memberLoginPage.LogInPage(user);
                //personal information
                applyLoanPage.EnterFirstName(user);
                applyLoanPage.EnterLastName(user);
                applyLoanPage.EnterEmail(user);
                applyLoanPage.EnterDateOfBirth(user);
                applyLoanPage.EnterZipCode(user);
                applyLoanPage.ClickPersonalLoan();
                applyLoanPage.ClickContinueButton();


                //income information
                applyLoanPage.EnterMonthlyIncome(user);
                applyLoanPage.SelectPayPeriod();
                applyLoanPage.SelectPayPeriodMonthly();
                applyLoanPage.SelectMonthlyType();
                applyLoanPage.SelectDayOfMonthPaid();
                applyLoanPage.SelectIncomeType();
                applyLoanPage.EnterNameOfCompany(user);
                applyLoanPage.EnterEmployerPhoneNumber(user);
                applyLoanPage.EnterEmployersCity(user);
                applyLoanPage.SelectEmployersState();
                applyLoanPage.EnterEmployersZipCode(user);
                applyLoanPage.SelectYear();
                applyLoanPage.SelectMonth();
                applyLoanPage.ChooseDirectDeposit();
                applyLoanPage.AddAdditionalIncome();

                //Add additional income
                applyLoanPage.AdditionalEnterMonthlyIncome(user);
                applyLoanPage.AdditionalSelectPayPeriod();
                applyLoanPage.AdditionalSelectDayPaid();
                applyLoanPage.AdditionalSelectPayDay();
                applyLoanPage.AdditionalSelectIncomeType();
                applyLoanPage.AdditionalEnterNameOfCompany(user);
                applyLoanPage.AdditionalEnterPhoneNumber(user);
                applyLoanPage.AdditionalEnterEmployersCity(user);
                applyLoanPage.AdditionalSelectEmployersState();
                applyLoanPage.AdditionalEnterEmployersZipCode(user);
                applyLoanPage.AdditionalSelectYear();
                applyLoanPage.AdditionalSelectMonth();
                applyLoanPage.AdditionalChooseDirectDeposit();

                //Bank Information
                applyLoanPage.EnterRoutingNumber(user);
                applyLoanPage.EnterBankAccountNumber(user);
                applyLoanPage.ReEnterBankAccountNumber(user);
                applyLoanPage.SelectOpenYear();
                applyLoanPage.SelectOpenMonth();
                applyLoanPage.SelectTypeBank();

                //Id Information
                applyLoanPage.SelectIdentificationType();
                applyLoanPage.SelectIdentificationState(user);
                applyLoanPage.EnterIdentificationNumber(user);
                applyLoanPage.EnterExpirationDate(user);
                applyLoanPage.EnterSocialSecurityNumber(user);

                //Contact Information
                applyLoanPage.EnterStreetAddress(user);
                applyLoanPage.SelectLivingYear();
                applyLoanPage.SelectLivingMonth();
                applyLoanPage.ChooseRentOption();
                applyLoanPage.EnterContactPhoneNumber(user);
                applyLoanPage.ClickSendVerificationCode();
                //applyLoanPage.EnterVerificationCode(user,false);
                applyLoanPage.ClickAgreeTextMessages();
                applyLoanPage.ClickAgreePhoneCalls();

                //Security Information
                applyLoanPage.EnterContactPassword(user);

                //Submit Information
                applyLoanPage.ClickAgreeTerms();
                applyLoanPage.ClickAgreeConsent();
                applyLoanPage.ClickSubmitApplication();
            }

            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "Rajesh_ZipCodeMulitpleCities_9261")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            // Last edit by Pat Holman 2/27/2020
            public void Rajesh_ZipCodeMulitpleCities_9261()
            {
                var user = Users.GetMultipleCitiesUser();
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var applyPage = new ApplyPage(Driver, user);
                memberLoginPage.LogInPage(user);
                //personal information
                applyPage.EnterFirstName(user);
                applyPage.EnterLastName(user);
                applyPage.EnterEmail(user);
                applyPage.EnterDateOfBirth(user);
                applyPage.EnterZipCode(user);
                applyPage.ClickPersonalLoan();
                applyPage.ClickContinueButton();


                //Contact Information
                applyPage.EnterStreetAddress(user);
                applyPage.AssertMultipleCitiesLoaded(user);
                applyPage.SelectCity();
            }


            [Test]
            [Category("BrowserStack")]
            [Category("RegressionCCO")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ChangePasswordFromProfile_9398_NV")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void ChangePasswordFromProfile_9398_NV()
            {
                
                //var user = Users.GetNevadaCustomer();
                var user = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var updateMember = new UpdateMember(Driver, user);
                var updatePasswordPage = new UpdatePassword(Driver, user);
                var resetMyPassword = new ResetMyPassword(Driver, user);

                memberLoginPage.LogInUser(user);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickProfileNV();

                updateMember.AssertUpdateMemberPageIsVisible();
                updateMember.ClickChangePassword();

                updatePasswordPage.AssertUpdatePasswordPageIsVisible();
                updatePasswordPage.EnterCurrentPassword(user.Password);
                updatePasswordPage.EnterNewPassword(user.NewPassword);
                updatePasswordPage.EnterConfirmPassword(user.NewPassword);
                updatePasswordPage.ClickUpdatePassword();
                updatePasswordPage.AssertUpdateMemberPasswordHasChangedIsVisible();

                updatePasswordPage.LoginUsingNewPassword(user);

                //Re-Set test case i.e. change PW back to the previous password
                updatePasswordPage.ResetToPreviousPassword(user);
                memberPage.LogOut();
            }


            [Test]
            [Category("RegressionTest")]
            //[Category("RepairNeeded")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "UpdateCustomerEmailFromProfile_8244_UT")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void UpdateCustomerEmailFromProfile_8244_UT()
            {
                var user = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var updateMemberPage = new UpdateMember(Driver,user);
                user.UpdatedEmail = "updated@email.com";
                if (user.Email.StartsWith("qa"))
                    user = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);

                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var storeProfile = new StoreProfile(Driver, user);
                var storeProfileEditPage = new StoreProfileEdit(Driver, user);

                //memberLoginPage
                memberLoginPage.LogInUser(user);

                //memberPage
                memberPage.AssertMemberDashboardLoaded();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickProfile();


                //StoreProfile
                storeProfile.AssertStoreProfileIsVisible();
                storeProfile.ClickUpdateProfile();
                //StoreProfileEdit
                storeProfileEditPage.AssertStoreProfileEditPageLoaded();
                storeProfileEditPage.ClickEmailAddressInputField();
                storeProfileEditPage.EnterNewEmailAddress();
                storeProfileEditPage.EmailNoticeHandler();
                storeProfileEditPage.ClickSaveChanges();


                // change email back after test runs.
                storeProfileEditPage.ResetToPreviousEmailAddress(user);
                storeProfileEditPage.AssertStoreProfileEditPageLoaded();
                
            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "UpdateCustomerPhoneNumberFromProfile_7807_UT")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void UpdateCustomerPhoneNumberFromProfile_7807_UT()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                var storeProfilePage = new StoreProfile(Driver, approvedCustomer);
                var updateMemberPage = new UpdateMember(Driver, approvedCustomer);
                var updateMemberInfoPage = new UpdateMemberInfo(Driver, approvedCustomer);

                memberLoginPage.LogInUser(approvedCustomer);
                memberPage.AssertMemberDashboardLoaded();

                memberPage.ClickCustomerMenuButton();
                memberPage.ClickProfile();
                
                //StoreProfile
                storeProfilePage.AssertStoreProfileIsVisible();
                storeProfilePage.ClickUpdateProfile();
                
            }


            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "UpdateCustomersAddressFromProfile_7407_UT")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void UpdateCustomersAddressFromProfile_7407_UT()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_Turndown);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeProfilePage = new StoreProfile(Driver, withPersonalLoanCustomer);
               
                var updateMemberInfoPage = new UpdateMemberInfo(Driver, withPersonalLoanCustomer);

                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage
                memberPage.AssertCustomerLoggedIn();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickProfile();

                //updateMemberPage
               storeProfilePage.AssertStoreProfileIsVisible();
               storeProfilePage.ClickUpdateProfile();
               storeProfilePage.UpdateMembersZipCode();
               storeProfilePage.ClickSaveChanges();
                
            }
        }
    }
}