﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using AutomationResources;
using AventStack.ExtentReports;
using BrowserStack;
using CheckCityOnline.Pages;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;


namespace CheckCityOnline.Tests
{
    public partial class BaseTest
    {
        public TestConfiguration Config;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        protected IWebDriver Driver { get; private set; }
        public TestContext TestContext { get; set; }
        private ScreenCapture ScreenCapture { get; set; }
        protected string profile;
        protected string environment;
        private Local browserStackLocal;

        public BaseTest(string profile, string environment)
        {
            this.profile = profile;
            this.environment = environment;
        }
        [OneTimeSetUp]
        public void SetupForEveryTestRun()
        {
            Logger.Debug("*************[ One Time Setup ]*************");

            var configuration = TestConfigManager.Config;
            SetupReporter();
            
            var count = CreateNewCustomers();
            if (!configuration.ReleaseTest)
                LoadTestCustomerLists(count);
        }
        
        [SetUp]
        public void SetupForEverySingleTestMethod()
        {
            Logger.Debug("*************[ TEST STARTED ]*************");
            var testContext = TestContext.CurrentContext;
            Config = TestConfigManager.Config;
            if (Config.SystemUnderTest == SystemUnderTest.PROD)
            {
                if (!testContext.Test.MethodName.Contains("FinalCountDown"))
                {
                    throw new Exception(@"This Test can't be run on production");
                }
            }
            
            Reporter.AddTestCaseMetadataToHtmlReport(testContext);
            var factory = new WebDriverFactory(profile, environment);
            Driver = factory.Create(BrowserType.Chrome);
            Driver.Manage().Window.Maximize();
            ScreenCapture = new ScreenCapture(Driver, testContext);
            Config = TestConfigManager.Config;
            //Debug.WriteLine(Config.SystemUnderTest);
            //Reporter.LogTestStepForBugLogger(Status.Info, "*************[ TEST STARTED ON " + Config.SystemUnderTest +" ]*************");
            Debug.WriteLine(testContext.Test.MethodName);


        }

        [TearDown]
        public void TearDownForEverySingleTestMethod()
        {
            var testContext = TestContext.CurrentContext;
            Config = TestConfigManager.Config;


            Logger.Debug(GetType().FullName + " started a method tear down");
            try
            {
                TakeScreenshotForTestFailure();
            }
            catch (Exception e)
            {
                Logger.Error(e.Source);
                Logger.Error(e.StackTrace);
                Logger.Error(e.InnerException);
                Logger.Error(e.Message);
            }
            finally
            {
                StopBrowser();
                Logger.Debug(testContext.Test.Name);
                Logger.Debug("*************[ TEST STOPPED ]*************\n");
            }
        }
    }
}