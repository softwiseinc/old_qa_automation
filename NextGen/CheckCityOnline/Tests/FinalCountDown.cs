﻿using System;
using System.Data;
using AutomationResources;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using MongoDB.Bson.Serialization.Serializers;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("FinalCountDown")]
            [TestCase(TestName = "FinalCountDown_login")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_login()
            {
                var prodTestingApprovePaydayUT = Users.GetProdTestingApprovePaydayUT();
                var memberLoginPage = new MemberLoginPage(Driver, prodTestingApprovePaydayUT);
                var memberPage = new MemberPage(Driver, prodTestingApprovePaydayUT);
                memberLoginPage.LogInUser(prodTestingApprovePaydayUT);
                memberLoginPage.NavigateToLoginPage();
            }
            [Test]
            [Category("FinalCountDown")]
            [TestCase(TestName = "FinalCountDown_MakeMyPayment_Login")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_MakeMyPayment_Login()
            {
                var prodTestingApprovePaydayUT = Users.GetProdTestingDebtUser();
                var memberLoginPage = new MemberLoginPage(Driver, prodTestingApprovePaydayUT);
                var makeMyPaymentPage = new MakeMyPaymentPage(Driver, prodTestingApprovePaydayUT);
                makeMyPaymentPage.NavigatToMakeMyPaymentLogin();
                makeMyPaymentPage.EnterLoginInformation(prodTestingApprovePaydayUT);
                makeMyPaymentPage.ClickLoginButton();
                makeMyPaymentPage.AssertLoanIsPastDue();
            }

            [Test]
            [Category("FinalCountDown")]
            [TestCase(TestName = "FinalCountDown_SimpleNavigation")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_SimpleNavigation()
            {
                var prodTestingApprovePaydayUT = Users.GetProdTestingApprovePaydayUT();
                var memberLoginPage = new MemberLoginPage(Driver, prodTestingApprovePaydayUT);
                var memberPage = new MemberPage(Driver, prodTestingApprovePaydayUT);
                var storeProfilePage = new StoreProfile(Driver, prodTestingApprovePaydayUT);
                var storeProfileEditPage = new StoreProfileEdit(Driver, prodTestingApprovePaydayUT);
                var storeLoanHistoryPage = new StoreLoanHistory(Driver, prodTestingApprovePaydayUT);
                memberLoginPage.LogInUser(prodTestingApprovePaydayUT);
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickProfile();
                storeProfilePage.ClickUpdateProfile();
                storeProfileEditPage.ClickCancel();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickLoanHistory();
                storeLoanHistoryPage.ClickProfile();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickUploadDocs();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickDashboard();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickDropdownReferAFriend();
                memberPage.ExplicitWait(7);
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickDropdownLogout();
            }
            [Test]
            [Category("FinalCountDown")]
            [TestCase(TestName = "FinalCountDown_WithdrawCompletedApplication")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_WithdrawCompletedApplication()
            {
                var User = Users.GenNewRandomMember(CustomerType.UT_ApprovedPersonalWithBalance);
                if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.STAGE)
                    User.RunEnvironment = "STAGE";
                else if (TestConfigManager.Config.SystemUnderTest == SystemUnderTest.PROD)
                    User.RunEnvironment = "PROD";
                else if(TestConfigManager.Config.SystemUnderTest == SystemUnderTest.QA)
                    User.RunEnvironment = "QA";

                
                var applyPage = new ApplyLoan(Driver, User);
                applyPage.NavigateToApplyLoanPage();
                applyPage.FillPersonalInformationFirstName_Valid(User);
                applyPage.FillPersonalInformationLastName_Valid(User);
                applyPage.FillPersonalInformationEmail_Valid();
                applyPage.FillPersonalInformationDob_Valid(User);
                applyPage.FillPersonalInformationZipCode_Valid(User);
                applyPage.ClickPersonalInformationContinueBtn();
                applyPage.ClickPersonalInformationPaydayRadio();
                applyPage.ClickPersonalInformationContinueBtn();
                applyPage.FillIncomeInformationMonthlyIncome_Valid();
                applyPage.SelectIncomeInformationPayPeriodOptionTwo();
                applyPage.SelectIncomeInformationTwiceAmonthDayOne();
                applyPage.SelectIncomeInformationTwiceAmonthDayTwo();
                applyPage.SelectIncomeInformationIncomeType();
                applyPage.FillIncomeInformationCompanyName();
                applyPage.FillIncomeInformationPhoneNumber();
                applyPage.FillIncomeInformationCity();
                applyPage.SelectIncomeInformationState();
                applyPage.FillIncomeInformationZipCode();
                applyPage.SelectBankInformationTimeWithEmp_Year();
                applyPage.SelectBankInformationTimeWithEmp_Month();
                applyPage.ClickYesDirectDeposit();
                applyPage.FillBankInformationRouting();
                applyPage.BankaccountHandler();
                applyPage.SelectBankInformationTimeWithEmp_Year();
                applyPage.SelectBankInformationTimeWithEmp_Month();
                applyPage.SelectAccountTypeChecking();
                applyPage.SelectDirectDeposit();
                applyPage.SelectIdentificationInformationState();
                applyPage.FillIdentificationNumber();
                applyPage.FillIdentificationExpirationDate();
                applyPage.FillIdentificationSocialSecurity();
                applyPage.FillContactInformationStreetAddress();
                applyPage.SelectResidenceLengthYear();
                applyPage.SelectResidenceLengthMonth();
                applyPage.DoYouOwnOrRent();
                applyPage.FillContactInformationPhoneNumber("8013611124");
                applyPage.ClickContactInformationSendPhoneVerification();
                //applyPage.ClickYesAboutFuturePaymentsByEmail();
                applyPage.ClickContactInformationOptInSms();
                applyPage.ClickContactInformationOptInSmsMarketing();
                applyPage.FillSecurityInformationPassword(User);
                applyPage.ClickSubmitInformationAgreeConsent();
                applyPage.ClickSubmitInformationAgreeTerms();
                applyPage.ClickWithdrawMyApplication();
                applyPage.ClickConfirmWithdrawApplicationYes();
            }

            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "FinalCountDown_ApplyLoanInvalidZip_EMPTY")]
            [Author("Pat Holman", "pholman@softwise.com")]
            // Last edit by Pat Holman 1/17/2020
            public void FinalCountDown_ApplyLoanInvalidZip_EMPTY()
            {
                var user = Users.GetInvalidUser();
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                applyLoanPage.FillPersonalInformationFirstNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationLastNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);

                applyLoanPage.FillPersonalInformationZipCodeEmptyValidation(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                try
                {
                    applyLoanPage.ClickPersonalInformationPaydayRadio();
                    applyLoanPage.ClickPersonalInformationContinueBtn();
                }
                catch (Exception)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "Select Radio Button if seen");
                }
                applyLoanPage.ExplicitWait(3);
                applyLoanPage.ValidateIncomeInformationNotVisible();
            }
        }
    }
}