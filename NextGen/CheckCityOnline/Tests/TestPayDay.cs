﻿using System.Collections.Generic;
using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {

        private partial class AllTestsCases
        {

            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "GetPersonalLoan_0000")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void GetPersonalLoan_0000()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(approvedCustomer);
                storeBankEditPage.EnterAccountNumber(approvedCustomer);
                storeBankEditPage.EnterAccountNumber(approvedCustomer);
                storeBankEditPage.ReEnterAccountNumber(approvedCustomer);
                storeBankEditPage.SelectYear(approvedCustomer);
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();
                


                // loanApproval
                var personalLoanApprovalPage = new LoanApprovalPage(Driver, approvedCustomer);
                personalLoanApprovalPage.AssertLoanApprovalPage();
                personalLoanApprovalPage.EnterLoanAmount();
                personalLoanApprovalPage.ClickGetAmount();
                //personalLoanApprovalPage.ExplicitWait(10);

                                

                //loanDetails
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedCustomer);
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //loanDisclosurePage
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedCustomer);
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
               
                loanDisclosurePage.ClickIAgreeButtonTwo();

                //loanThankYou
                var loanThankYouPage = new LoanThankYouPage(Driver, approvedCustomer);
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                //loanThankYouPage.ExplicitWait(10);
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }

            
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_UT")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_UT()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType:CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);
                
                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.ClickYesFuturePaymentsByEmail();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
               // memberPage.AssertMemberPageLoaded();
            }

            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_WY")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_WY()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.WY_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.ClickYesFuturePaymentsByEmail();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }

            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPersonalLoan_8139_WI")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPersonalLoan_8139_WI()
            {
                var approvedPersonalCustomer = Users.GetCustomer(customerType: CustomerType.WI_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPersonalCustomer);
                var memberPage = new MemberPage(Driver, approvedPersonalCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPersonalCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPersonalCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPersonalCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPersonalCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPersonalCustomer);

                memberLoginPage.LogInUser(approvedPersonalCustomer);

                //MemberPage
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.SignPersonalLoanAgreement(approvedPersonalCustomer);
                loanDisclosurePage.ClickSignWisconsinLoanAgreement();
                loanDisclosurePage.ExplicitWait(3);
                loanDisclosurePage.SignAchAuthorization(approvedPersonalCustomer);
                loanDisclosurePage.ClickWisconsinAchAuthorization();


                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }

            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_CA")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_CA()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.CA_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();
                
                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_KS")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_KS()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.KS_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.SignPersonalLoanAgreement(approvedPaydayCustomer);
                loanDisclosurePage.ClickSignKansasConsumerLoanAgreement();
                loanDisclosurePage.SignKansasNoticeToConsumer(approvedPaydayCustomer);
                loanDisclosurePage.ClickSignKansasNoticeToConsumerAgreement();
                

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_10444_AK")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_10444_AK()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.AK_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();
                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();
 
                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_HI")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_HI()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.HI_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();

                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();
                


                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();


                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                ////member
                // memberPage.AssertMemberPageLoaded();
            }
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPayday_7188_AL")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPayday_7188_AL()
            {
                var approvedPaydayCustomer = Users.GetCustomer(customerType: CustomerType.AL_ApprovedPaydayZeroBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();
                storeBankEdit.EmailNoticeYesHandler();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();
                
                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

            }

            [Test]

            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("SmokeTest")]
            [TestCase(TestName = "ShowStoreOptionPage_9622")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void ShowStoreOptionPage_9622()
            {
                var pendingAllCustomer = Users.GenNewRandomMember(CustomerType.UT_PendingAll);
                var applyLoanPage = new ApplyLoan(Driver, pendingAllCustomer);
                applyLoanPage.NavigateToApplyLoanPage();
                var loanApprovalPage = new LoanApprovalPage(Driver, pendingAllCustomer);
                var memberPage = new MemberPage(Driver, pendingAllCustomer);
                var loanAtStorePage = new LoanAtStore(Driver, pendingAllCustomer);

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationLastName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(pendingAllCustomer);
                //applyLoanPage.FillPersonalInformationZipCode_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationZipcode_ValidB();
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
                applyLoanPage.SelectIncomeInformationBiWeekly();
                applyLoanPage.SelectIncomeInformationPayDay();
                applyLoanPage.SelectIncomeInformationIncomeType();
                applyLoanPage.FillIncomeInformationCompanyName();
                //applyLoanPage.SelectNameOfCompany();
                applyLoanPage.FillIncomeInformationPhoneNumber();
                applyLoanPage.FillIncomeInformationCity();
                applyLoanPage.SelectIncomeInformationState();
                applyLoanPage.FillIncomeInformationZipCode();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();
                applyLoanPage.ClickYesDirectDeposit();

                //Bank Information
                applyLoanPage.FillBankInformationRouting();
                applyLoanPage.FillBankInformationBankAccount();
                applyLoanPage.FillBankInformationBankAccountCheck();
                applyLoanPage.SelectBankInformationTimeWithEmp_Year();
                applyLoanPage.SelectBankInformationTimeWithEmp_Month();
                applyLoanPage.SelectAccountTypeChecking();

                //Identification Number
                applyLoanPage.SelectDirectDeposit();
                applyLoanPage.SelectIdentificationType();
                applyLoanPage.SelectIdentificationInformationState();
                applyLoanPage.FillIdentificationNumber();
                applyLoanPage.FillIdentificationExpirationDate();
                applyLoanPage.FillIdentificationSocialSecurity();

                //Contact Information
                applyLoanPage.FillContactInformationStreetAddress();
                applyLoanPage.SelectResidenceLengthYear();
                applyLoanPage.SelectResidenceLengthMonth();
                applyLoanPage.FillContactInformationPhoneNumber(pendingAllCustomer);
                applyLoanPage.ClickContactInformationSendPhoneVerification();
                applyLoanPage.ClickContactInformationOptInSms();
                applyLoanPage.ClickContactInformationOptInSmsMarketing();
                applyLoanPage.DoYouOwnOrRent();
                applyLoanPage.ClickYesAboutFuturePaymentsByEmail();

                //Security Information
                applyLoanPage.FillSecurityInformationPassword(pendingAllCustomer);

                //Submit Information
                applyLoanPage.ClickSubmitInformationAgreeConsent();
                applyLoanPage.ClickSubmitInformationAgreeTerms();
                applyLoanPage.ClickSubmitInformationSubmitBtn();


                loanApprovalPage.AssertStoreOptionHasLoaded();
                loanApprovalPage.ClickInStoreOption();

                loanAtStorePage.AssertLoanAtStorePage();
                loanAtStorePage.ClickSendMyApplication();
                loanAtStorePage.ClickGotIt();

                memberPage.AssertPendingPaydayLoanTileIsVisible();
                

            }

            [Test]
            //[Category("Ignore")]
            [Category("SmokeTest")]
            //[Ignore("Test Broke", Until = "2021-03-12 12:00:00Z")]
            [TestCase(TestName = "StoreOptionClickCancel_9657")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void StoreOptionClickCancel_9657()
            {
                var pendingAllCustomer = Users.GenNewRandomMember(CustomerType.UT_PendingAll);
                var applyLoanPage = new ApplyLoan(Driver, pendingAllCustomer);
                applyLoanPage.NavigateToApplyLoanPage();
                var loanApprovalPage = new LoanApprovalPage(Driver, pendingAllCustomer);
                var memberPage = new MemberPage(Driver, pendingAllCustomer);
                var loanAtStorePage = new LoanAtStore(Driver, pendingAllCustomer);

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationLastName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(pendingAllCustomer);
                //applyLoanPage.FillPersonalInformationZipCode_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationZipcode_ValidB();
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
                applyLoanPage.SelectIncomeInformationBiWeekly();
                applyLoanPage.SelectIncomeInformationPayDay();
                applyLoanPage.SelectIncomeInformationIncomeType();
                applyLoanPage.FillIncomeInformationCompanyName();
                //applyLoanPage.SelectNameOfCompany();
                applyLoanPage.FillIncomeInformationPhoneNumber();
                applyLoanPage.FillIncomeInformationCity();
                applyLoanPage.SelectIncomeInformationState();
                applyLoanPage.FillIncomeInformationZipCode();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();
                applyLoanPage.ClickYesDirectDeposit();

                //Bank Information
                applyLoanPage.FillBankInformationRouting();
                applyLoanPage.FillBankInformationBankAccount();
                applyLoanPage.FillBankInformationBankAccountCheck();
                applyLoanPage.SelectBankInformationTimeWithEmp_Year();
                applyLoanPage.SelectBankInformationTimeWithEmp_Month();
                applyLoanPage.SelectAccountTypeChecking();

                //Identification Number
                applyLoanPage.SelectDirectDeposit();
                applyLoanPage.SelectIdentificationType();
                applyLoanPage.SelectIdentificationInformationState();
                applyLoanPage.FillIdentificationNumber();
                applyLoanPage.FillIdentificationExpirationDate();
                applyLoanPage.FillIdentificationSocialSecurity();

                //Contact Information
                applyLoanPage.FillContactInformationStreetAddress();
                applyLoanPage.SelectResidenceLengthYear();
                applyLoanPage.SelectResidenceLengthMonth();
                applyLoanPage.SelectHomeRent();
                applyLoanPage.FillContactInformationPhoneNumber(pendingAllCustomer);
                applyLoanPage.ClickContactInformationSendPhoneVerification();
                applyLoanPage.ClickYesAboutFuturePaymentsByEmail();
                //applyLoanPage.ClickContactInformationSendPhoneVerificationCode(pendingAllCustomer);
                applyLoanPage.ClickContactInformationOptInSms();
                applyLoanPage.ClickContactInformationOptInSmsMarketing();

                //Security Information
                applyLoanPage.FillSecurityInformationPassword(pendingAllCustomer);

                //Submit Information
                applyLoanPage.ClickSubmitInformationAgreeConsent();
                applyLoanPage.ClickSubmitInformationAgreeTerms();
                applyLoanPage.ClickSubmitInformationSubmitBtn();


                loanApprovalPage.AssertStoreOptionHasLoaded();
                loanApprovalPage.ClickInStoreOption();

                loanAtStorePage.AssertLoanAtStorePage();
                loanAtStorePage.ClickCancel();
                memberPage.LogOut();
            }

            [Test]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("SmokeTest")]
            [TestCase(TestName = "ShowStoreOptionPagePickOnline_9647")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void ShowStoreOptionPagePickOnline_9647()
            {
                var pendingAllCustomer = Users.GenNewRandomMember(CustomerType.UT_PendingAll);
                var applyLoanPage = new ApplyLoan(Driver, pendingAllCustomer);
                applyLoanPage.NavigateToApplyLoanPage();
                var loanApprovalPage = new LoanApprovalPage(Driver, pendingAllCustomer);
                var documentUploadPage = new DocumentUpload(Driver, pendingAllCustomer);
                var memberPage = new MemberPage(Driver, pendingAllCustomer);
                var loanAtStorePage = new LoanAtStore(Driver, pendingAllCustomer);

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationLastName_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(pendingAllCustomer);
                //applyLoanPage.FillPersonalInformationZipCode_Valid(pendingAllCustomer);
                applyLoanPage.FillPersonalInformationZipcode_ValidB();
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
                applyLoanPage.SelectIncomeInformationBiWeekly();
                applyLoanPage.SelectIncomeInformationPayDay();
                applyLoanPage.SelectIncomeInformationIncomeType();
                applyLoanPage.FillIncomeInformationCompanyName();
                //applyLoanPage.SelectNameOfCompany();
                applyLoanPage.FillIncomeInformationPhoneNumber();
                applyLoanPage.FillIncomeInformationCity();
                applyLoanPage.SelectIncomeInformationState();
                applyLoanPage.FillIncomeInformationZipCode();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();
                applyLoanPage.SelectDirectDeposit();
                

                //Bank Information
                applyLoanPage.FillBankInformationRouting();
                applyLoanPage.BankaccountHandler();
                applyLoanPage.SelectAccountTypeChecking();
                //Identification Number
                applyLoanPage.SelectIdentificationType();
                applyLoanPage.SelectIdentificationInformationState();
                applyLoanPage.FillIdentificationNumber();
                applyLoanPage.FillIdentificationExpirationDate();
                applyLoanPage.FillIdentificationSocialSecurity();

                //Contact Information
                applyLoanPage.FillContactInformationStreetAddress();
                applyLoanPage.SelectResidenceLengthYear();
                applyLoanPage.SelectResidenceLengthMonth();
                applyLoanPage.FillContactInformationPhoneNumber(pendingAllCustomer);
                applyLoanPage.ClickContactInformationSendPhoneVerification();
                //applyLoanPage.ClickContactInformationSendPhoneVerificationCode(pendingAllCustomer);
                applyLoanPage.ClickContactInformationOptInSms();
                applyLoanPage.ClickContactInformationOptInSmsMarketing();
                applyLoanPage.DoYouOwnOrRent();
                applyLoanPage.ClickYesAboutFuturePaymentsByEmail();
                
                //Security Information
                applyLoanPage.FillSecurityInformationPassword(pendingAllCustomer);

                //Submit Information
                applyLoanPage.ClickSubmitInformationAgreeConsent();
                applyLoanPage.ClickSubmitInformationAgreeTerms();
                applyLoanPage.ClickSubmitInformationSubmitBtn();


                loanApprovalPage.AssertStoreOptionHasLoaded();
                loanApprovalPage.ClickOnlineButton();

                documentUploadPage.AssertDocumentUpload();


            }

            [Test]
            [Category("SmokeTest")]
            //[Ignore("Test Broke", Until = "2020-03-28 12:00:00Z")]
            [TestCase(TestName = "Rajesh_InlineMessageOnDisclosure_8937_UT")]
            [Author("Rajesh Kr Das", "rdas@softwise.com")]
            public void Rajesh_InlineMessageOnDisclosure_8937_UT()
            {
                var approvedPaydayCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedPaydayCustomer);
                var memberPage = new MemberPage(Driver, approvedPaydayCustomer);
                var storeBankEdit = new StoreBankEdit(Driver, approvedPaydayCustomer);
                var personalLoanApprovalPage = new LoanApprovalPage(Driver, approvedPaydayCustomer);
                var personalLoanDetailsPage = new LoanDetailsPage(Driver, approvedPaydayCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, approvedPaydayCustomer);
                var thankYouPage = new LoanThankYouPage(Driver, approvedPaydayCustomer);

                memberLoginPage.LogInUser(approvedPaydayCustomer);

                //MemberPage
                memberPage.ClickRequestPaydayLoan();

                //StoreBankEdit
                storeBankEdit.AssertStoreBankEdit();
                storeBankEdit.ClickConfirmBankAccountWithEmailAchHandlers();


                //NewLoan
                personalLoanApprovalPage.AssertLoanApprovalPage();
                personalLoanApprovalPage.EnterLoanAmount();
                personalLoanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                personalLoanDetailsPage.AssertLoanDetails();
                personalLoanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.AssertInlineMessageForAudioRecLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                //LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
            }

            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "PayDayPayOff_7269")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void PayDayPayOff_7269()
            {
                var customerWithActiveLoan = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customerWithActiveLoan);
                var memberPage = new MemberPage(Driver, customerWithActiveLoan);
                var storeExtensionMessagePage = new StoreExtensionMessage(Driver, customerWithActiveLoan);
                var storePaymentPage = new StorePayment(Driver, customerWithActiveLoan);
                var storePaymentAcceptPage = new StoreInstallmentPaymentAccept(Driver, customerWithActiveLoan);
                var thankYouPaymentPage = new ThankYouPage(Driver, customerWithActiveLoan);

                var customerTryCount = 0;

                //memberPageLoginPage
                memberLoginPage.LogInUser(customerWithActiveLoan);

                //memberPage
                while (Helper.Page.ElementVisibilityState(Driver, "//span[contains(@id, 'ctl00_CustomContent')][text()='Personal']") && customerTryCount < 3)
                {

                    customerTryCount += 1;
                    Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                    memberPage.LogOut();
                    memberLoginPage.LogInUser(customerWithActiveLoan);
                    memberPage.AssertMemberPageLoaded();
                    if (customerTryCount >= 3)
                        Assert.IsTrue(false, "Could not find a customer");
                }
                if (!Helper.Page.ElementVisibilityState(Driver, Elements.MemberPage.Button.ClickPaydayMakePayment))
                    memberPage.ExtendCustomerPaydayLoan();

                memberPage.MakePaydayLoanPayment();

                //Store Extension Message
                storeExtensionMessagePage.AssertMakePaymentDueDateConfirmationPageLoaded();
                storeExtensionMessagePage.ClickYesWithoutMovingTheDueDate();


                //Store Payment
                storePaymentPage.AssertStorePaymentLoaded();
                storePaymentPage.SelectPaymentAch();
               
                var payoffAmount = storePaymentPage.FindPayOffAmount();
                storePaymentPage.EnterPayoffAmount(payoffAmount);
                storePaymentPage.ClickNextButton();

                //Store payment Accept
                storePaymentAcceptPage.AssertStorePaymentAccept();
                storePaymentAcceptPage.ClickMakePayment();

                storePaymentAcceptPage.AssertOneTimePayment();
                storePaymentAcceptPage.ClickIAgreeButton();

                //Thank you payment page
                thankYouPaymentPage.AssertThankYouPage();
                thankYouPaymentPage.ClickGotIt();
            }
               
            


            [Test]
           // [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("Regression")]
            [TestCase(TestName = "ExtendPayday_7274_QA")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void ExtendPayday_7274_QA()
            {
                
                
                var paydayCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, paydayCustomer);
                var memberPage = new MemberPage(Driver, paydayCustomer);
                var extensionTypePage = new ExtensionType(Driver, paydayCustomer);
                var extensionAccept = new ExtensionAccept(Driver, paydayCustomer);
                var extensionTransactPage = new ExtensionTransact(Driver, paydayCustomer);
                var thankYouPage = new ThankYouPage(Driver, paydayCustomer);

                //memberLoginPage
                memberLoginPage.LogInUser(paydayCustomer);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickExtend();

                extensionTypePage.AssertExtensionType();
                extensionTypePage.ClickSelectDateButton();
                //extensionTypePage.ClickDateField();
                //extensionTypePage.ClickCallNextToChangeTheMonth();
                extensionTypePage.PickDesiredDueDate();
                extensionTypePage.AchPaymentMethod();
                extensionTypePage.ClickNext();


                extensionAccept.AssertExtensionAcceptLoaded();
                extensionAccept.ClickAccept();
                
                extensionTransactPage.AssertExtensionTransactLoaded();
                extensionTransactPage.ClickIAgreeButton();

                thankYouPage.AssertThankYouPage();
                thankYouPage.ClickGotIt();

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickDropdownLogout();
                
            }

            [Test]
            //[Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
             [Category("Smoke")]
            [Category("Regression")]
            [TestCase(TestName = "ExtendPayday70DayPaymentPlan_7625")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void ExtendPayday70DayPaymentPlan_7625()
            {
                // to fix we need a payday customer with a loan 70 days or older 
                var paymentPlanLoan = Users.GetCustomer(CustomerType.UT_70PlusPayday);
                var memberLoginPage = new MemberLoginPage(Driver, paymentPlanLoan);
                var memberPage = new MemberPage(Driver, paymentPlanLoan);
                var extensionTypePage = new ExtensionType(Driver, paymentPlanLoan);
                var extensionAcceptPage = new ExtensionAccept(Driver, paymentPlanLoan);
                var extensionTransactPage = new ExtensionTransact(Driver, paymentPlanLoan);
                var thankYouPage = new ThankYouPage(Driver, paymentPlanLoan);
                
                //memberLoginPage
                memberLoginPage.LogInUser(paymentPlanLoan);

                memberPage.ClickExtend();
                
                extensionTypePage.AssertExtensionType();
                extensionTypePage.ClickDateField();
                extensionTypePage.ClickCallNextToChangeTheMonth();
                extensionTypePage.PickDesiredDueDate();
                extensionTypePage.AchPaymentMethod();
                extensionTypePage.ClickNext();
                extensionTypePage.AssertPaymentPlanOptionIsVisible();
                extensionTypePage.ClickPaymentFrequency();
                extensionTypePage.SelectPayPeriod();
                extensionTypePage.ClickNext();

                extensionAcceptPage.AssertExtensionAcceptLoaded();
                extensionAcceptPage.FirstPaymentPlanSignature();
                extensionAcceptPage.ClickAccept();
                extensionAcceptPage.SecondPaymentPlanSignature();
                extensionAcceptPage.ClickAccept();

                extensionTransactPage.AssertExtensionTransactLoaded();
                extensionTransactPage.ClickIAgreeButton();

                thankYouPage.AssertThankYouPage();
                thankYouPage.ClickGotIt();
                
                memberPage.AssertMemberPageLoaded();
                
            }

            [Test]
            [Category("RegressionTest")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PaydayPaymentAch_7676")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void PaydayPaymentAch_7676()
            {
                var withPayDayLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPayDayLoanCustomer);
                var memberPage = new MemberPage(Driver, withPayDayLoanCustomer);
                var storeExtensionMessagePage = new StoreExtensionMessage(Driver, withPayDayLoanCustomer);
                var storePaymentPage = new StorePayment(Driver, withPayDayLoanCustomer);
                var storePaymentAcceptPage = new StoreInstallmentPaymentAccept(Driver, withPayDayLoanCustomer);
                var thankYouPaymentPage = new ThankYouPage(Driver,withPayDayLoanCustomer);
                var customerTryCount = 0;
                //memberLoginPage 
                memberLoginPage.LogInUser(withPayDayLoanCustomer);

                
                memberPage.AssertMemberDashboardLoaded();

                
                while (Helper.Page.ElementVisibilityState(Driver, "//span[contains(@id, 'ctl00_CustomContent')][text()='Personal']") && customerTryCount < 3)
                {
                    
                    customerTryCount += 1;
                    Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                    memberPage.LogOut();
                    memberLoginPage.LogInUser(withPayDayLoanCustomer);
                    memberPage.AssertMemberPageLoaded();
                    if (customerTryCount >= 3)
                        Assert.IsTrue(false, "Could not find a customer");
                }
                if (!Helper.Page.ElementVisibilityState(Driver, Elements.MemberPage.Button.ClickPaydayMakePayment))
                     memberPage.ExtendCustomerPaydayLoan();
                
                memberPage.MakePaydayLoanPayment();

                //Store Extension Message
                storeExtensionMessagePage.AssertMakePaymentDueDateConfirmationPageLoaded();
                storeExtensionMessagePage.ClickYesWithoutMovingTheDueDate();


                //Store Payment
                storePaymentPage.AssertStorePaymentLoaded();
                storePaymentPage.SelectPaymentAch();
                storePaymentPage.EnterPaymentAmount();
                storePaymentPage.ClickNextButton();

                //Store payment Accept
                storePaymentAcceptPage.AssertStorePaymentAccept();
                storePaymentAcceptPage.ClickMakePayment();

                storePaymentAcceptPage.AssertOneTimePayment();
                storePaymentAcceptPage.ClickIAgreeButton();

                //Thank you payment page
                thankYouPaymentPage.AssertThankYouPage();
                thankYouPaymentPage.ClickGotIt();
            }
        }

    }
}
