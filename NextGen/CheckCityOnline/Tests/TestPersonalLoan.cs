﻿using System;
using AutomationResources;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "LoginExistingLoanCustomer_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void LoginExistingLoanCustomer_UT()
            {
                var personalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, personalLoanCustomer);
                memberLoginPage.LogInUser(personalLoanCustomer);
            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "LoginExistingLoanCustomer_ID")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void LoginExistingLoanCustomer_ID()
            {
                var personalLoanCustomer = Users.GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, personalLoanCustomer);
                memberLoginPage.LogInUser(personalLoanCustomer);
            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "PersonalLoanCancel_ID")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void PersonalLoanCancel_ID()
            {
                var customer = Users.GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var memberPage = new MemberPage(Driver, customer);
                var storeBankEdit = new StoreBankEdit(Driver, customer);

                memberLoginPage.LogInUser(customer);
                memberPage.ClickRequestPersonalLoan();
                storeBankEdit.ClickCancelButton();
                memberPage.AssertMemberDashboardLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickDropdownLogout();
            }

            [Test]
            [Category("FinalCountDown")]
            [Category("BrowserStack")]
            [TestCase(TestName = "FinalCountDown_PaydayLoanCancel_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_PaydayLoanCancel_UT()
            {
                var customer = Users.GetProdTestingApprovePaydayUT();
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var memberPage = new MemberPage(Driver, customer);
                var storeBankEdit = new StoreBankEdit(Driver, customer);

                memberLoginPage.LogInUser(customer);
                memberPage.ClickRequestPaydayLoan();
                storeBankEdit.ClickCancelButton();
                memberPage.AssertMemberDashboardLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickDropdownLogout();
            }

            [Test]
            [Category("FinalCountDown")]
            [Category("BrowserStack")]
            [TestCase(TestName = "FinalCountDown_PersonalLoanCancel_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void FinalCountDown_PersonalLoanCancel_UT()
            {
                var customer = Users.GetProdTestingApprovePersonalUT();
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var memberPage = new MemberPage(Driver, customer);
                var storeBankEdit = new StoreBankEdit(Driver, customer);

                memberLoginPage.LogInUser(customer);
                memberPage.ClickRequestPersonalLoan();
                storeBankEdit.ClickCancelButton();
                memberPage.AssertMemberDashboardLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickDropdownLogout();
            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "LoginExistingLoanCustomer_TX")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void LoginExistingLoanCustomer_TX()
            {
                var personalLoanCustomer = Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, personalLoanCustomer);
                memberLoginPage.LogInUser(personalLoanCustomer);
            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "LoginExistingLoanCustomer_WY")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void LoginExistingLoanCustomer_WY()
            {
                var personalLoanCustomer = Users.GetCustomer(CustomerType.WY_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, personalLoanCustomer);
                memberLoginPage.LogInUser(personalLoanCustomer);
            }

            [Test]
            [Category("SmokeTest")]
            // [Ignore("Test Broke", Until = "2020-03-15 12:00:00Z")]
            [TestCase(TestName = "NewLoanRequestPersonalLoan_7208_TX")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanRequestPersonalLoan_7208_TX()
            {
                var txCustomer = Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, txCustomer);
                var memberPage = new MemberPage(Driver, txCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, txCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, txCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, txCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, txCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, txCustomer);
                memberLoginPage.LogInUser(txCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //loanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //loanDisclosurePage for texas customer
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                loanDisclosurePage.ClickIAgreeButtonThree();
                loanDisclosurePage.ClickIAgreeButtonFour();
                loanDisclosurePage.ClickIAgreeButtonFive();


                //loanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            // [Ignore("Test Broke", Until = "2020-03-15 12:00:00Z")]
            [TestCase(TestName = "IdahoCustomerRequestAInstallmentLoan_10145")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void IdahoCustomerRequestAInstallmentLoan_10145()
            {
                var idahoCustomer = Users.GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, idahoCustomer);
                var memberPage = new MemberPage(Driver, idahoCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, idahoCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, idahoCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, idahoCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, idahoCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, idahoCustomer);
                memberLoginPage.LogInUser(idahoCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(idahoCustomer);
                storeBankEditPage.EnterAccountNumber(idahoCustomer);
                storeBankEditPage.SelectYear();
                storeBankEditPage.ClickYesToUseThisBankAccount();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.ClickYesFuturePaymentsByEmail();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();
                

                //LoanApproval
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //loanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //loanDisclosurePage for Idaho customer
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                //loanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "CACustomerRequestPaydayLoan_10443")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void CACustomerRequestPaydayLoan_10443()
            {
                var californiaCustomer = Users.GetCustomer(CustomerType.CA_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, californiaCustomer);
                var memberPage = new MemberPage(Driver, californiaCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, californiaCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, californiaCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, californiaCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, californiaCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, californiaCustomer);
                memberLoginPage.LogInUser(californiaCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPaydayLoan();
                
                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(californiaCustomer);
                storeBankEditPage.EnterAccountNumber(californiaCustomer);
                storeBankEditPage.ReEnterAccountNumber(californiaCustomer);
                storeBankEditPage.SelectYear();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //loanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //loanDisclosurePage for Idaho customer
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                
                //loanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }

           

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "KSOCustomerRequestPaydayLoan_10445")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void KSOCustomerRequestPaydayLoan_10445()
            {
                var KansasCustomer = Users.GetCustomer(CustomerType.KS_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, KansasCustomer);
                var memberPage = new MemberPage(Driver, KansasCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, KansasCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, KansasCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, KansasCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, KansasCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, KansasCustomer);
                memberLoginPage.LogInUser(KansasCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(KansasCustomer);
                storeBankEditPage.EnterAccountNumber(KansasCustomer);
                storeBankEditPage.SelectYear();
                storeBankEditPage.ClickYesToUseThisBankAccount();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //loanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //loanDisclosurePage for Idaho customer
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();


                //loanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }




            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPersonalLoan_8139_UT")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPersonalLoan_8139_UT()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, withPersonalLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, withPersonalLoanCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.EmailNoticeYesHandler();                             
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //LoanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosurePage
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                //LoanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }


            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPersonalLoan_8139_MO")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPersonalLoan_8139_MO()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.MO_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, withPersonalLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, withPersonalLoanCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //LoanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosurePage
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                loanDisclosurePage.ClickIAgreeButtonTwo();

                //LoanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }
            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "NewLoanRequestPersonalLoan_8139_ID")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void NewLoanRequestPersonalLoan_8139_ID()
            {
                //var user = Users.GetStagingUser001();
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.ID_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var loanDetailsPage = new LoanDetailsPage(Driver, withPersonalLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, withPersonalLoanCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage has loaded;
                memberPage.ClickRequestPersonalLoan();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //LoanDetails
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosurePage
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                loanDisclosurePage.ClickIAgreeButtonTwo();

                //LoanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }
            
            [Test]
            [Category("SmokeTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "RefinancePayDownOnPersonalLoanUtah_10472")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void RefinancePayDownOnPersonalLoanUtah_10472()
            {


                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, withPersonalLoanCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage has loaded;
                //memberPage has loaded;
                memberPage.ClickRefinance();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickYesToUseThisBankAccount();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.AssertRefinanceLoanApproval();
                loanApprovalPage.EnterPayDownAmount();
                loanApprovalPage.ClickUpdate();
                loanApprovalPage.ClickContinue();

                //LoanDisclosurePage
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                //LoanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }

            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "RefinanceBorrowOnPersonalLoanUtah_10473")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void RefinanceBorrowOnPersonalLoanUtah_10473()
            {

                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var loanDisclosurePage = new LoanDisclosure(Driver, withPersonalLoanCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                //memberPage has loaded;
                memberPage.ClickRefinance();

                //StoreBankEdit
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //LoanApproval
                loanApprovalPage.AssertRefinanceLoanApproval();
                loanApprovalPage.EnterNewLoanAmount();
                loanApprovalPage.ClickUpdate();
                loanApprovalPage.ClickContinue();

                //LoanDisclosurePage
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                //LoanThankYou
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                loanThankYouPage.ClickEndMySession();

            }


            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PersonalLoanAchLoanPayOff_7737_TX")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanAchLoanPayOff_7737_TX()
            {
                var txCustomer = Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);
                //var txCustomer = Users.GetQaUser006();
                var memberLoginPage = new MemberLoginPage(Driver, txCustomer);
                var memberPage = new MemberPage(Driver, txCustomer);
                var storeInstallmentPayment = new StoreInstallmentPayment(Driver, txCustomer);
                var storeExtensionMessagePage = new StoreExtensionMessage(Driver, txCustomer);
                var thankYouPaymentPage = new ThankYouPage(Driver, txCustomer);

                //memberLoginPage
                memberLoginPage.LogInUser(txCustomer);

                //memberPage
                memberPage.ClickPaymentPersonaLoan();

                //StoreInstallmentPayment
                storeInstallmentPayment.AssertStoreCustomerInstallmentPayment();
                storeInstallmentPayment.SelectAchPaymentMethod();
                string payOffAmount = storeInstallmentPayment.FindPayOffAmount();
                storeInstallmentPayment.EnterPaymentAmount(payOffAmount);
                storeInstallmentPayment.ClickContinue();

                //Store Extension Message
                storeExtensionMessagePage.AssertStoreExtensionMessageLoaded();
                storeExtensionMessagePage.ClickMakePayment();

                //Thank you payment
                thankYouPaymentPage.AssertThankYouForYourPayment();
            }

            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PersonalLoanAchLoanPayOff_7737_UT")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanAchLoanPayOff_7737_UT()
            {
                var customerPersonalWithBalance = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customerPersonalWithBalance);
                var memberPage = new MemberPage(Driver, customerPersonalWithBalance);
                var storeInstallmentPayment = new StoreInstallmentPayment(Driver, customerPersonalWithBalance);
                var storeExtensionMessagePage = new StoreExtensionMessage(Driver, customerPersonalWithBalance);
                var storeInstallmentPaymentAccept =
                    new StoreInstallmentPaymentAccept(Driver, customerPersonalWithBalance);
                var thankYouPaymentPage = new ThankYouPage(Driver, customerPersonalWithBalance);

                //memberLoginPage
                memberLoginPage.LogInUser(customerPersonalWithBalance);

                //memberPage
                memberPage.MakePaymentPersonalLoan();

                //StoreInstallmentPayment
                storeInstallmentPayment.AssertStoreCustomerInstallmentPayment();
                storeInstallmentPayment.SelectAchPaymentMethod();
                string payOffAmount = storeInstallmentPayment.FindPayOffAmount();
                storeInstallmentPayment.EnterPaymentAmount(payOffAmount);
                storeInstallmentPayment.ClickContinue();

                //Store Extension Message
                storeExtensionMessagePage.AssertStoreExtensionMessageLoaded();
                storeExtensionMessagePage.ClickMakePayment();

                //Thank you payment
                thankYouPaymentPage.AssertThankYouForYourPayment();
            }


            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "PersonalLoanDebitCardLoanPayOff_7737_UT")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanDebitCardLoanPayOff_7737_UT()
            {
                var customerPersonalWithBalance = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customerPersonalWithBalance);
                var memberPage = new MemberPage(Driver, customerPersonalWithBalance);
                var storeInstallmentPayment = new StoreInstallmentPayment(Driver, customerPersonalWithBalance);
                var storeExtensionMessagePage = new StoreExtensionMessage(Driver, customerPersonalWithBalance);
                var storeInstallmentPaymentAccept =
                    new StoreInstallmentPaymentAccept(Driver, customerPersonalWithBalance);
                var thankYouPaymentPage = new ThankYouPage(Driver, customerPersonalWithBalance);

                //memberLoginPage
                memberLoginPage.LogInUser(customerPersonalWithBalance);

                //memberPage
                memberPage.AssertMemberDashboardLoaded();
                memberPage.MakePayment();
                

                //StoreInstallmentPayment
                storeInstallmentPayment.AssertStoreCustomerInstallmentPayment();
                storeInstallmentPayment.SelectDebitCardMethod();
                string payOffAmount = storeInstallmentPayment.FindPayOffAmount();
                storeInstallmentPayment.EnterPaymentAmount(payOffAmount);
                storeInstallmentPayment.ClickContinue();

                //store installment payment accept
                storeInstallmentPaymentAccept.AssertStoreInstallmentPaymentAccept();
                storeInstallmentPaymentAccept.CompleteRepayWidget();
                
                //Thank you payment
                thankYouPaymentPage.AssertThankYouForYourPayment();
            }


            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "PersonalLoanAchPayment_7768")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanAchPayment_7768()
            {
                var customerPersonal = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customerPersonal);
                var memberPage = new MemberPage(Driver, customerPersonal);
                var storeInstallmentPayment = new StoreInstallmentPayment(Driver, customerPersonal);
                var storeInstallmentPaymentAccept = new StoreInstallmentPaymentAccept(Driver, customerPersonal);
                var thankYouPaymentPage = new ThankYouPage(Driver, customerPersonal);

                //memberLoginPage
                memberLoginPage.LogInUser(customerPersonal);

                //memberPage
                memberPage.ClickMakePayment();

                //storeInstallmentPayment
                storeInstallmentPayment.AssertStoreCustomerInstallmentPayment();
                storeInstallmentPayment.ClickAchPaymentMethod();
                storeInstallmentPayment.PaymentAmount();
                storeInstallmentPayment.ClickContinueButton();

                //storeInstallmentPaymentAccept
                storeInstallmentPaymentAccept.AssertStoreInstallmentPaymentAccept();
                storeInstallmentPaymentAccept.ClickMakePaymentButton();

                //Thank you payment page
                thankYouPaymentPage.AssertThankYouForYourPayment();
            }



            //[Test]
            //[Category("Regression")]
            //[TestCase(TestName = "PersonalLoanRefinanceBorrow_7782_QA")]
            //[Author("Ryan Palmer", "Rpalmer@softwise.com")]
            //public void PersonalLoanRefinanceBorrow_7782_QA()
            //{

            //    var user = Users.GetQaUser006();
            //    var memberLoginPage = new MemberLoginPage(Driver, user);
            //    var memberPage = new MemberPage(Driver, user);
            //    var storeBankEditPage = new StoreBankEdit(Driver, user);
            //    var loanApprovalPage = new LoanApprovalPage(Driver, user);
            //    var loanDetailsPage = new LoanDetailsPage(Driver, user);
            //    var loanDisclosurePage = new LoanDisclosure(Driver, user);
            //    var loanThankYouPage = new LoanThankYouPage(Driver, user);

            //    //memberLoginPage
            //    memberLoginPage.LogInUser(user);


            //    //memberPage
            //    memberPage.Refinance();// I will need to change this to be Refinance 


            //    //storeBankEditPage
            //    storeBankEditPage.AssertStoreBankEdit();
            //    storeBankEditPage.ClickLoanPayments();//yes
            //    storeBankEditPage.ClickConfirmBankAccount();

            //    //LoanApprovalPage
            //    loanApprovalPage.AssertRefinanceLoanApproval();
            //    loanApprovalPage.EnterLoanAmount();
            //    loanApprovalPage.ClickUpdate();
            //    loanApprovalPage.ClickContinue();


            //    //LoanDetailsPage
            //    loanDetailsPage.AssertLoanDetails();
            //    loanDetailsPage.ClickContinue();

            //    //LoanDisclosurePage
            //    loanDisclosurePage.AssertLoanDisclosurePageLoaded();
            //    loanDisclosurePage.EnterSignatureLoanDisclosures(user);
            //    loanDisclosurePage.ClickSignLoanAgreement();
            //    loanDisclosurePage.EnterSignatureAchAgreement(user);
            //    loanDisclosurePage.ClickSignAchAgreement();

            //    //LoanThankYouPage
            //    loanThankYouPage.AssertLoanThankYouPageLoaded();
            //    loanThankYouPage.ClickYourOpinionCounts();
            //    loanThankYouPage.ClickEndMySession();

            //}

            //[Test]
            //[Category("Regression")]
            //[TestCase(TestName = "PersonalLoanRefinanceExtend_7783_QA")]
            //[Author("Ryan Palmer", "Rpalmer@softwise.com")]
            //public void PersonalLoanRefinanceExtend_7783_QA()
            //{

            //    var user = Users.GetQaUser006();
            //    var memberLoginPage = new MemberLoginPage(Driver, user);
            //    var memberPage = new MemberPage(Driver, user);
            //    var storeBankEditPage = new StoreBankEdit(Driver, user);
            //    var loanApprovalPage = new LoanApprovalPage(Driver, user);
            //    var loanDetailsPage = new LoanDetailsPage(Driver, user);
            //    var loanDisclosurePage = new LoanDisclosure(Driver, user);
            //    var loanThankYouPage = new LoanThankYouPage(Driver, user);


            //    //memberLoginPage
            //    memberLoginPage.LogInUser(user);


            //    //memberPage
            //    memberPage.Refinance();




            //        //storeBankEditPage
            //        storeBankEditPage.AssertStoreBankEdit();
            //        storeBankEditPage.ClickLoanPayments();//yes
            //        storeBankEditPage.ClickConfirmBankAccount();


            //        //LoanApprovalPage
            //        loanApprovalPage.AssertRefinanceLoanApproval();
            //        loanApprovalPage.ClickContinue();

            //        //LoanDetailsPage
            //        loanDetailsPage.AssertLoanDetails();
            //        loanDetailsPage.ClickExtendNow();

            //        //LoanDisclosurePage
            //        loanDisclosurePage.AssertLoanDisclosurePageLoaded();
            //        loanDisclosurePage.EnterSignatureLoanDisclosures(user);
            //        loanDisclosurePage.ClickSignLoanAgreement();
            //        loanDisclosurePage.EnterSignatureAchAgreement(user);
            //        loanDisclosurePage.ClickSignAchAgreement();

            //        //LoanThankYou
            //        loanThankYouPage.AssertLoanThankYouPageLoaded();
            //        loanThankYouPage.ClickYourOpinionCounts();
            //        loanThankYouPage.ClickEndMySession();



            //}
            //[Test]
            //[Category("Regression")]
            //[TestCase(TestName = "PersonalLoanRefinancePayDown_7784_QA")]
            //[Author("Ryan Palmer", "Rpalmer@softwise.com")]
            //public void PersonalLoanRefinancePayDown_7784_QA()
            //{
            //    var user = Users.GetQaUser006();
            //    var memberLoginPage = new MemberLoginPage(Driver, user);
            //    var memberPage = new MemberPage(Driver, user);
            //    var storeBankEditPage = new StoreBankEdit(Driver, user);
            //    var loanApprovalPage = new LoanApprovalPage(Driver, user);
            //    var loanDetailsPage = new LoanDetailsPage(Driver, user);
            //    var loanDisclosurePage = new PersonalLoanDisclosure(Driver, user);
            //    var loanThankYouPage = new LoanThankYouPage(Driver, user);

            //    memberLoginPage
            //    memberLoginPage.LogInUser(user);

            //    memberPage
            //    memberPage.Refinance();


            //    storeBankEditPage
            //    storeBankEditPage.AssertStoreBankEdit();
            //    storeBankEditPage.ClickLoanPayments();//yes
            //    storeBankEditPage.ClickConfirmBankAccount();

            //    LoanApprovalPage
            //    loanApprovalPage.AssertRefinanceLoanApproval();
            //    loanApprovalPage.EnterPayDownAmount();
            //    loanApprovalPage.ClickUpdate();
            //    loanApprovalPage.ClickContinue();

            //    LoanDetailsPage
            //    loanDetailsPage.AssertLoanDetails();
            //    loanDetailsPage.ClickPayAmountNow();

            //    LoanDisclosurePage
            //    loanDisclosurePage.AssertPersonalLoanDisclosure();
            //    loanDisclosurePage.EnterSignatureLoanDisclosures();
            //    loanDisclosurePage.ClickSignLoanAgreement();
            //    loanDisclosurePage.EnterSignatureAchAgreement();
            //    loanDisclosurePage.ClickSignAchAgreement();

            //    LoanThankYouPage
            //    loanThankYouPage.AssertLoanThankYouPageLoaded();
            //    loanThankYouPage.ClickYourOpinionCounts();
            //    loanThankYouPage.ClickEndMySession();

            //}

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "PersonalLoanTurnDownLead_7951")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanTurnDownLead_7951()
            {
                var turndownLeadCustomer = Users.GetCustomer(CustomerType.UT_TurndownLead);
                var memberLoginPage = new MemberLoginPage(Driver, turndownLeadCustomer);
                var memberPage = new MemberPage(Driver, turndownLeadCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, turndownLeadCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, turndownLeadCustomer);

                //memberLoginPage
                memberLoginPage.LogInUser(turndownLeadCustomer);

                //memberPage
                memberPage.AssertCustomerLoggedIn();
                memberPage.ClickRequestPersonalLoan();

                //storeBankEditPage
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(turndownLeadCustomer);
                storeBankEditPage.EnterAccountNumber(turndownLeadCustomer);
                storeBankEditPage.ReEnterAccountNumber(turndownLeadCustomer);
                storeBankEditPage.SelectYear(turndownLeadCustomer);
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //loanApprovalPage
                loanApprovalPage.AssertPersonalLoanTurnDownLead();

            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "PersonalLoanTurnDown_7962")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanTurnDown_7962()
            {
                var turnDownCustomer = Users.GetCustomer(CustomerType.UT_Turndown);
                var memberLoginPage = new MemberLoginPage(Driver, turnDownCustomer);
                var memberPage = new MemberPage(Driver, turnDownCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, turnDownCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, turnDownCustomer);

                //memberLoginPage
                memberLoginPage.LogInUser(turnDownCustomer);

                //memberPage
                memberPage.AssertCustomerLoggedIn();
                memberPage.ClickRequestPersonalLoan();

                //storeBankEditPage
                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(turnDownCustomer);
                storeBankEditPage.EnterAccountNumber(turnDownCustomer);
                storeBankEditPage.ReEnterAccountNumber(turnDownCustomer);
                storeBankEditPage.SelectYear(turnDownCustomer);
                storeBankEditPage.ClickLoanPayments();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                //personalLoanApprovalPage
                loanApprovalPage.AssertPersonalLoanTurnDown();
            }

            [Test]
            [Category("Ignore")]
            [Category("BrowserStack")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "PersonalLoanPendingStatusBanner_7963")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PersonalLoanPendingStatusBanner_7963()
            {
                var customerPendingAll = Users.GetCustomer(CustomerType.UT_PendingAll);
                var memberLoginPage = new MemberLoginPage(Driver, customerPendingAll);
                var memberPage = new MemberPage(Driver, customerPendingAll);
                var documentUpload = new DocumentUpload(Driver, customerPendingAll);
                var storeBankEditPage = new StoreBankEdit(Driver, customerPendingAll);
                var loanApprovalPage = new LoanApprovalPage(Driver, customerPendingAll);
                var thankyouPage = new LoanThankYouPage(Driver, customerPendingAll);

                memberLoginPage.LogInUser(customerPendingAll);

                if (memberPage.TryRequestPersonalLoanPresent())
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "TryRequestPersonalLoanPresent TRUE");
                    storeBankEditPage.TryAddBankingInformation(customerPendingAll);
                }

                if (memberLoginPage.TryRegisterStoreCustomer(customerPendingAll))
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "TryRegisterStoreCustomer TRUE");

                }

                if (loanApprovalPage.TryFinishYourApplication())
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "TryFinishYourApplication TRUE");
                    loanApprovalPage.ClickInStoreOption();
                    Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.SendMyApp)).Click();
                    loanApprovalPage.ExplicitWait(3);
                    Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.GotIt)).Click();
                    memberPage.AssertBringTheFollowingToStore();
                }

                if (memberPage.TryCheckPendingApplication())
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "TryCheckPendingApplication TRUE");
                    Driver.FindElement(By.XPath(Elements.MemberPage.ScreenText.CheckPendingApplication)).Click();
                    documentUpload.AssertDocumentUpload();
                }

                if (loanApprovalPage.TryFinishYourApplication())
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "TryFinishYourApplication TRUE");
                    loanApprovalPage.ClickInStoreOption();
                    Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.SendMyApp)).Click();
                    loanApprovalPage.ExplicitWait(3);
                    Driver.FindElement(By.XPath(Elements.LoanAtStore.Buttons.GotIt)).Click();
                    memberPage.AssertBringTheFollowingToStore();
                }
            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "PaydayLoanPendingAllStatus_9559")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void PaydayLoanPendingAllStatus_9559()
            {
                var customerPendingAll = Users.GetCustomer(CustomerType.UT_PendingAll);
                var memberLoginPage = new MemberLoginPage(Driver, customerPendingAll);
                var memberPage = new MemberPage(Driver, customerPendingAll);
                var loanThankYouPage = new LoanThankYouPage(Driver, customerPendingAll);
                var documentUploadPage = new DocumentUpload(Driver, customerPendingAll);

                //memberLoginPage
                memberLoginPage.LogInUser(customerPendingAll);

                //memberPage
                memberPage.AssertMemberPageLoaded();
                memberPage.ClickPendingApplication();
                documentUploadPage.AssertDocumentUpload();
            }

            [Test]
            [Category("JustOne")]
            [TestCase(TestName = "JustOne")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void JustOne()
            {
                Reporter.LogTestStepForBugLogger(Status.Info, "JustOne");
                Assert.IsTrue(true, "True That.");
                Reporter.LogPassingTestStepToBugLogger(Status.Pass, "Pass That!");
            }
        }
    }
}