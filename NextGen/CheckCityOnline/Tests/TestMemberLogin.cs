﻿using System.Diagnostics;
using AutomationResources;
using AventStack.ExtentReports;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("BrowserStack")]
            [Category("Smoke")]
            [TestCase(TestName = "SimpleLoginApprovedCustomer")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void SimpleLoginApprovedCustomer()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                //memberLoginPage.ExplicitWait(10);
                memberLoginPage.LogInUser(approvedCustomer);
                var elementVisibilityState = Helper.Page.ElementVisibilityState(Driver, "//*[@id='loginTab']/span[3]/a[2]");
                Reporter.LogPassingTestStepToBugLogger(Status.Pass, "Ta-Da: " + elementVisibilityState);
                
            }

            [Test]
            [Category("BrowserStack")]
            [Category("AcceptanceTest")]
            [TestCase(TestName = "EmailAddressIsRequired_7777")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void EmailAddressIsRequired_7777()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.EmailAddressIsRequired();
            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "InvalidEmailAddress_8202")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void InvalidEmailAddress_8202()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.InvalidEmailAddress();
            }
            
            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "InvalidThenValidEmail_8198_CA")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void InvalidThenValidEmail_8198_CA()
            {
                var validCustomer  = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, validCustomer);
                memberLoginPage.InvalidEmailAddress();
                memberLoginPage.UseValidEmailAddress(validCustomer);
            }

            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "InvalidThenValidEmail_8198_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void InvalidThenValidEmail_8198_UT()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                memberLoginPage.InvalidEmailAddress();
                memberLoginPage.UseValidEmailAddress(withPersonalLoanCustomer);
            }

            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "MemberLogin_7084")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberLogin_7084()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
            }

          
            [Test]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "Reset password_7085_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void ResetPassword_7085_UT()
            {
                var user = Users.GetCustomer(CustomerType.UT_TurndownLead);
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var lostPassword = new LostPassword(Driver, user);
                var memberSecurity = new MemberSecurity(Driver, user);
                var resetMyPassword = new ResetMyPassword(Driver, user);
                
                //Load member login page
                memberLoginPage.NavigateToLoginPage();
                memberLoginPage.EnterUserName(user);
                memberLoginPage.ClickNextButton();
                memberLoginPage.ClickForgotYourPasswordTextLink();

                //checking to see if the Lost Password page loaded 
                lostPassword.AssertLostPasswordPageLoaded();
                lostPassword.ValidateEmailAddress(user);
                lostPassword.EnterDateOfBirth(user);
                lostPassword.ClickSubmitButton();
                var ssnChallenge =  lostPassword.SocialSecurityNumberHandler(user);


                if (!ssnChallenge)
                {
                    //Checking to see if the MemberSecurity page loaded
                    memberSecurity.AssertMemberSecurityPageLoaded();
                    memberSecurity.EnterFirstSecurityQuestion(user);
                    memberSecurity.ClickSubmitButton1();
                    memberSecurity.EnterSecondSecurityQuestion(user);
                    memberSecurity.ClickSubmitButton();
                }


                //checking to see if we are on resetMyPassword page
                resetMyPassword.AssertResetPasswordPageLoaded();
                resetMyPassword.EnterNewPassword(user.NewPassword);
                resetMyPassword.ConfirmPasswordInput(user.NewPassword);
                resetMyPassword.ClickChangePassword();

                //Re-Set test case i.e. change PW back to the previous password
                resetMyPassword.ResetToPreviousPassword(user);
            }

            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "Ravi_Funding_Status_HandlingIn_Customer_Dashboard_PersonalLoan_6371")]
            [Author("Ravi", "ravik@softwise.com")]
            public void Ravi_FundingStatusHandlingInCustomerDashboard_PersonalLoan_6371()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, approvedCustomer);
                var applyLoan = new ApplyLoan(Driver, approvedCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedCustomer);
                var loanDisclosure = new LoanDisclosure(Driver, approvedCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, approvedCustomer); memberLoginPage.LogInUser(approvedCustomer);
                if (memberPage.IsRequestPersonalLoanBtnPresent())
                {
                    memberPage.ClickRequestPersonalLoan();
                    storeBankEditPage.AssertStoreBankEdit();
                    storeBankEditPage.EmailNoticeYesHandler();
                    storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();
                    loanApprovalPage.ExplicitWait(3);
                    loanApprovalPage.EnterLoanAmount(1000);
                    loanApprovalPage.ClickGetAmount();
                    loanApprovalPage.ClickContinue();
                    loanDisclosure.ClickIAgreeButtonOne();
                    loanDisclosure.ClickIAgreeButtonTwo();
                    loanThankYouPage.ClickYourOpinionCounts();
                    var loanApprovedDate = loanThankYouPage.GetLoanAgreementDate();
                    loanThankYouPage.ClickAccountMenu();
                    loanThankYouPage.AssertLoanApprovalDate(loanApprovedDate);
                }

            }

            [Test]
            [Category("SmokeTest")]
            [TestCase(TestName = "Ravi_Funding_Status_HandlingIn_Customer_Dashboard_PayDayLoan_6371")]
            [Author("Ravi", "ravik@softwise.com")]
            public void Ravi_Funding_Status_HandlingIn_Customer_Dashboard_PayDayLoan_6371()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, approvedCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, approvedCustomer);
                var loanDisclosure = new LoanDisclosure(Driver, approvedCustomer);
                var loanThankYouPage = new LoanThankYouPage(Driver, approvedCustomer);
                memberLoginPage.LogInUser(approvedCustomer);
                memberPage.AssertMemberDashboardLoaded();
                memberPage.ClickRequestPaydayLoan();
                storeBankEditPage.AssertStoreBankEdit();
                //storeBankEditPage.ClickContinueWithoutEnrolling();
                storeBankEditPage.EmailNoticeYesHandler();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount(1000);
                loanApprovalPage.ClickGetAmount();
                loanApprovalPage.ClickContinue();
                loanDisclosure.AssertLoanDisclosurePageLoaded();
                loanDisclosure.ClickIAgreeButtonOne();
                loanThankYouPage.AssertLoanThankYouPageLoaded();
                loanThankYouPage.ClickYourOpinionCounts();
                var loanApprovedDate = loanThankYouPage.GetLoanAgreementDate();
                loanThankYouPage.ClickAccountMenu();
                loanThankYouPage.AssertLoanApprovalDate(loanApprovedDate);
            }
        }
    }
}