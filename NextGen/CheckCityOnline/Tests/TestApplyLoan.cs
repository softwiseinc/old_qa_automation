﻿using System;
using AutomationResources;
using AventStack.ExtentReports;
using NUnit.Framework;
using NUnit.Framework.Internal.Execution;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("BrowserStack")]
            [Category("Regression")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "ApplyLoanPositive_QA")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            public void ApplyLoanPositive_QA()
            {
                var user = Users.GenNewRandomMember(CustomerType.Random);
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(user);
                applyLoanPage.FillPersonalInformationLastName_Valid(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);
                applyLoanPage.FillPersonalInformationZipCode_Valid(user);
                
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.LoanTypeOptionHelper();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
                applyLoanPage.SelectIncomeInformationBiWeekly();
                applyLoanPage.SelectIncomeInformationPayDay();
                applyLoanPage.SelectIncomeInformationIncomeType();
                applyLoanPage.FillIncomeInformationCompanyName();
                applyLoanPage.FillIncomeInformationPhoneNumber();
                applyLoanPage.FillIncomeInformationCity();
                applyLoanPage.SelectIncomeInformationState();
                applyLoanPage.FillIncomeInformationZipCode();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();

                //Bank Information
                applyLoanPage.FillBankInformationRouting();
                applyLoanPage.FillBankInformationBankAccount();
                applyLoanPage.FillBankInformationBankAccountCheck();
                applyLoanPage.SelectBankInformationTimeWithEmp_Year();
                applyLoanPage.SelectBankInformationTimeWithEmp_Month();

                //Identification Number
                applyLoanPage.SelectDirectDeposit();
                applyLoanPage.SelectIdentificationInformationState();
                applyLoanPage.FillIdentificationNumber();
                applyLoanPage.FillIdentificationExpirationDate();
                applyLoanPage.FillIdentificationSocialSecurity();

                //Contact Information
                applyLoanPage.FillContactInformationStreetAddress();
                applyLoanPage.SelectResidenceLengthYear();
                applyLoanPage.SelectResidenceLengthMonth();
                applyLoanPage.FillContactInformationPhoneNumber(user);
                applyLoanPage.ClickContactInformationSendPhoneVerification();
               
                applyLoanPage.ClickContactInformationOptInSms();
                applyLoanPage.ClickContactInformationOptInSmsMarketing();

                //Security Information
                applyLoanPage.FillSecurityInformationPassword(user);

                //Submit Information
                applyLoanPage.ClickSubmitInformationAgreeConsent();
                applyLoanPage.ClickSubmitInformationAgreeTerms();
                applyLoanPage.ClickSubmitInformationSubmitBtn();

                //TC Pass or Fail
                applyLoanPage.AssertIsLoanApplySubmitted();

            }

            //How often are you paid? - Twice a Month
            [Test]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("Regression")]
            [TestCase(TestName = "ApplyLoanTwiceMonth_QA")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]
            public void ApplyLoanTwiceMonth_QA()
            {
                var user = Users.GenNewRandomMember(CustomerType.Random);
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(user);
                applyLoanPage.FillPersonalInformationLastName_Valid(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);
                applyLoanPage.FillPersonalInformationZipCode_Valid(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionTwo();

                applyLoanPage.SelectIncomeInformationTwiceAmonthDayOne();
                applyLoanPage.SelectIncomeInformationTwiceAmonthDayTwo();

                applyLoanPage.SelectIncomeInformationIncomeType();
                applyLoanPage.FillIncomeInformationCompanyName();
                applyLoanPage.FillIncomeInformationPhoneNumber();
                applyLoanPage.FillIncomeInformationCity();
                applyLoanPage.SelectIncomeInformationState();
                applyLoanPage.FillIncomeInformationZipCode();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Year();
                applyLoanPage.SelectIncomeInformationTimeWithEmp_Month();

                //Bank Information
                applyLoanPage.FillBankInformationRouting();
                applyLoanPage.FillBankInformationBankAccount();
                applyLoanPage.FillBankInformationBankAccountCheck();
                applyLoanPage.SelectBankInformationTimeWithEmp_Year();
                applyLoanPage.SelectBankInformationTimeWithEmp_Month();

                //Identification Number
                applyLoanPage.SelectDirectDeposit();
                applyLoanPage.SelectIdentificationInformationState();
                applyLoanPage.FillIdentificationNumber();
                applyLoanPage.FillIdentificationExpirationDate();
                applyLoanPage.FillIdentificationSocialSecurity();

                //Contact Information
                applyLoanPage.FillContactInformationStreetAddress();
                applyLoanPage.SelectResidenceLengthYear();
                applyLoanPage.SelectResidenceLengthMonth();
                applyLoanPage.FillContactInformationPhoneNumber(user);
                applyLoanPage.ClickContactInformationSendPhoneVerification();
               // applyLoanPage.ClickContactInformationSendPhoneVerificationCode(user);
                applyLoanPage.ClickContactInformationOptInSms();
                applyLoanPage.ClickContactInformationOptInSmsMarketing();

                //Security Information
                applyLoanPage.FillSecurityInformationPassword(user);

                //Submit Information
                applyLoanPage.ClickSubmitInformationAgreeConsent();
                applyLoanPage.ClickSubmitInformationAgreeTerms();
                applyLoanPage.ClickSubmitInformationSubmitBtn();

                //TC Pass or Fail
                applyLoanPage.AssertIsLoanApplySubmitted();

            }


            [Test]
            [Category("RegressionTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "ApplyLoanInvalidZip_EMPTY")]
            [Author("Pat Holman", "pholman@softwise.com")]
            // Last edit by Pat Holman 1/17/2020
            public void ApplyLoanInvalidZip_EMPTY()
            {
                var user = Users.GetInvalidUser();
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                applyLoanPage.FillPersonalInformationFirstNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationLastNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);

                applyLoanPage.FillPersonalInformationZipCodeEmptyValidation(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                try
                {
                    applyLoanPage.ClickPersonalInformationPaydayRadio();
                    applyLoanPage.ClickPersonalInformationContinueBtn();
                }
                catch (Exception)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info,"Select Radio Button if seen");
                }
                applyLoanPage.ExplicitWait(3);
                applyLoanPage.ValidateIncomeInformationNotVisible();
            }

            [Test]
            [Category("Ignore")]
            [Category("BrowserStack")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "ApplyLoanInvalidZip")]
            [Author("Pat Holman", "pholman@softwise.com")]
            // Last edit by Pat Holman 1/17/2020
            public void ApplyLoanInvalidZip()
            {
                var user = Users.GetInvalidUser();
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();
                
                // Enter valid data for fields before the zip code
                applyLoanPage.FillPersonalInformationFirstNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationLastNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationEmailValidationCheck(user);
                applyLoanPage.FillPersonalInformationDob_Valid(user);

                // enter an invalid error message
                applyLoanPage.FillPersonalInformationZipCodeInvalidValidation(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                try
                {
                    applyLoanPage.ClickPersonalInformationPaydayRadio();
                    applyLoanPage.ClickPersonalInformationContinueBtn();
                }
                catch (Exception)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "Select Radio Button if seen");
                }
                applyLoanPage.ExplicitWait(3);
                applyLoanPage.ValidateIncomeInformationNotVisible();
            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "ApplyLoanInvalid_LongNames")]
            [Author("Pat Holman", "pholman@softwise.com")]
            // Last edit by Pat Holman 1/17/2020 
            //issues with amount of characters allowed on the name of the apply now page
            // 10401 added 07/06/2020
            public void ApplyLoanInvalid_LongNames()
            {
                var user = Users.GetInvalidUser();
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();
                
                user.FirstName = LoremIpsum.Words(100);
                user.LastName = LoremIpsum.Words(100);

                applyLoanPage.FillPersonalInformationFirstNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationLastNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);
                applyLoanPage.FillPersonalInformationZipCode_Valid(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();
                try
                {
                    applyLoanPage.ClickPersonalInformationPaydayRadio();
                    applyLoanPage.ClickPersonalInformationContinueBtn();
                }
                catch (Exception)
                {
                    Reporter.LogTestStepForBugLogger(Status.Info, "Select Radio Button if seen");
                }
                applyLoanPage.ExplicitWait(3);
                applyLoanPage.ValidateIncomeInformationNotVisible();
            }

            [Test]
            [Category("BrowserStack")]
            [TestCase(TestName = "ApplyLoanNegativeTesting_Income_Information")]
            [Author("Pat Holman", "pholman@softwise.com")]
            // Last edit by Pat Holman 1/17/2020
            public void ApplyLoanNegativeTesting_Income_Information()
            {
                var user = Users.GenNewRandomMember(CustomerType.UT_ApprovedPaydayZeroBalance);
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                // fill in personal information to get to the Income information
                applyLoanPage.FillPersonalInformationFirstNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationLastNameValidationCheck(user);
                applyLoanPage.FillPersonalInformationEmailValidationCheck(user);
                applyLoanPage.FillPersonalInformationDob_Valid(user);
                applyLoanPage.FillPersonalInformationZipCode_Valid(user);
                
                // Income information section
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickSubmitInformationSubmitBtn();
                applyLoanPage.EmptyInputValidationCheck();
                applyLoanPage.InvalidInputValidationCheck(user);
            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [TestCase(TestName = "Ravi_Renamed_Recent_Payday_Field_9125")]
            [Author("Ravi Kazi Karmacharya", "ravik@softwise.com")]

            public void Ravi_Renamed_Recent_Payday_Field_9125()
            {
                var user = Users.GenNewRandomMember(CustomerType.UT_ApprovedPaydayWithBalance);
                var applyLoanPage = new ApplyLoan(Driver, user);
                applyLoanPage.NavigateToApplyLoanPage();

                //Personal Information
                applyLoanPage.FillPersonalInformationFirstName_Valid(user);
                applyLoanPage.FillPersonalInformationLastName_Valid(user);
                applyLoanPage.FillPersonalInformationEmail_Valid();
                applyLoanPage.FillPersonalInformationDob_Valid(user);
                applyLoanPage.FillPersonalInformationZipCode_Valid(user);
                applyLoanPage.ClickPersonalInformationContinueBtn();
                applyLoanPage.ClickPersonalInformationPaydayRadio();
                applyLoanPage.ClickPersonalInformationContinueBtn();

                //income information
                applyLoanPage.FillIncomeInformationMonthlyIncome_Valid();
                applyLoanPage.SelectIncomeInformationPayPeriodOptionOne();
                applyLoanPage.SelectIncomeInformationBiWeekly();
                applyLoanPage.SelectIncomeInformationPayDay();

                //TC Pass or Fail
               applyLoanPage.AssertIsPayDayFieldRenamed();

            }
        }
    }
}