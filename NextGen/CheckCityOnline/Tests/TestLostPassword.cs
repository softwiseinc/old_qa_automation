﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NavigateToLostPasswordPage_8200")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NavigateToLostPasswordPage_8200()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var lostPasswordPage = new LostPassword(Driver, approvedCustomer);
                lostPasswordPage.NavigateToLostPasswordPage(approvedCustomer);
            }


            [Test]
            [Category("AcceptanceTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "LostPassword_SubmitWithWrongBirthDate_7749")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void LostPassword_SubmitWithWrongBirthDate_7749()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var lostPasswordPage = new LostPassword(Driver, approvedCustomer);
                lostPasswordPage.NavigateToLostPasswordPage(approvedCustomer);
                lostPasswordPage.ClickSubmitWithWrongBirthDate();

            }
        }
    }
}

