﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("SmokeTest")]
            [Category("BrowserStack")]
            [TestCase(TestName = "MemberSecurity_8260")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void MemberSecurity_8260()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberSecurityPage = new MemberSecurity(Driver, approvedCustomer);
                memberSecurityPage.NavigateToMemberSecurityPage(approvedCustomer);
            }

        }
    }
}

