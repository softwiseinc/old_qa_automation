﻿using System.Collections.Generic;
using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using static CheckCityOnline.Users;
using CustomerType = AutomationResources.CustomerType;

namespace CheckCityOnline.Tests
{
    [TestFixture]
    internal partial class AllTests
    {
        [TestFixture("single", "")]
        //[TestFixture("single", "ie")]
        //[TestFixture("single", "firefox")]
        //[TestFixture("single", "safari")]
        private partial class AllTestsCases : BaseTest
        {
            //All other partial classes are part of this class
            public AllTestsCases(string profile, string environment) : base(profile, environment) { }

            [Test]
           // [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            //bug is failing this story 
            [TestCase(TestName = "EditPersonalLoanAmount_8834_TX")]
            public void EditPersonalLoanAmount_8834_TX()
            {
                //var user = Users.GetQaUser006();
                var user = Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);
                //var user = Users.GetCustomer(CustomerType.TX_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var storeBankEditPage = new StoreBankEdit(Driver, user);
                var personalLoanApprovalPage = new LoanApprovalPage(Driver, user);
                var personalLoanDetailPage = new LoanDetailsPage(Driver, user);

                memberLoginPage.LogInUser(user);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickRequestPersonalLoan();

                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.EnterRoutingNumber(user);
                storeBankEditPage.EnterAccountNumber(user);
                storeBankEditPage.ReEnterAccountNumber(user);
                storeBankEditPage.SelectYear(user);
                storeBankEditPage.ClickYesToUseThisBankAccount();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                personalLoanApprovalPage.AssertLoanApprovalPage();
                personalLoanApprovalPage.EnterLoanAmount();
                personalLoanApprovalPage.ClickGetAmount();

                personalLoanDetailPage.AssertLoanDetails();
                personalLoanDetailPage.ClickPencilToEditLoanAmount();
                personalLoanDetailPage.EnterNewPersonalLoanAmount();
                personalLoanDetailPage.ClickConfirmNewLoanAmount();
                
            }
            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "LoginEfpPageRDPAsUser_10219")]
            public void LoginEfpPageRDPAsUser_10219()
            {
                var ccoUser = Users.GetQaUserCCO();
                var efpLoginPage = new EfpLoginPage(Driver, ccoUser);
                var efpPage = new EFPPage(Driver, ccoUser);

                efpLoginPage.LogInUser(ccoUser);
                efpPage.AssertEfpPageLoaded();
            }
            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "LoginInCCOApprovalPage_10225")]
            public void LoginInCCOApprovalPage_10225()
            {
                var rdpUserApproval = Users.GetQaUserRDPLoanApproval();
                var efpLoginPage = new EfpLoginPage(Driver, rdpUserApproval);
                var efpPage = new EFPPage(Driver, rdpUserApproval);
                var efpLoanApprovalPage = new EfpLoanApprovalPage(Driver, rdpUserApproval);

                efpLoginPage.LogInUser(rdpUserApproval);
                efpPage.AssertEfpPageLoaded();
                efpPage.ClickLoanApprovalTab();

                efpLoanApprovalPage.AssertEfpLoanApprovalPageLoaded();
            }
            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "EFP_LoginLoanDocumentsPage_10447")]
            public void EFP_LoginLoanDocumentsPage_10447()
            {
                var rdpUserApproval = Users.GetQaUserRDPLoanApproval();
                var efpLoginPage = new EfpLoginPage(Driver, rdpUserApproval);
                var efpPage = new EFPPage(Driver, rdpUserApproval);
                var efpLoanDocumentsPage = new EfpLoanDocumentsPage(Driver, rdpUserApproval);

                efpLoginPage.LogInUser(rdpUserApproval);
                efpPage.AssertEfpPageLoaded();
                efpPage.ClickLoanDocumentsTab();

                efpLoanDocumentsPage.AssertEfpLoanDocumentsPageLoaded();
            }


            [Test]
            [Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "EFP_ShowLoanDocumentsOnLoanApprovalPage_10226")]
            public void EFP_ShowLoanDocumentsOnLoanApprovalPage_10226()
            {
                var rdpUserApproval = Users.GetQaUserRDPLoanApproval();
                var efpLoginPage = new EfpLoginPage(Driver, rdpUserApproval);
                var efpPage = new EFPPage(Driver, rdpUserApproval);
                var efpLoanApprovalPage = new EfpLoanApprovalPage(Driver, rdpUserApproval);

                efpLoginPage.LogInUser(rdpUserApproval);

                efpPage.AssertEfpPageLoaded();

                efpPage.ClickLoanApprovalTab();


                efpLoanApprovalPage.AssertEfpLoanApprovalPageLoaded();
                efpLoanApprovalPage.ClickLoanDocumentsTab();
                efpLoanApprovalPage.ClickCustomerState();
                efpLoanApprovalPage.SelectUtahAsState();
                efpLoanApprovalPage.EnterCustomerID(rdpUserApproval);
                efpLoanApprovalPage.ClickLookupCustomer();
                efpLoanApprovalPage.AssertDateSignedIsVisible();


            }


            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "EFP_LoadLoanDocumentFromLoanDocumentSection_10235")]
            public void EFP_LoadLoanDocumentFromLoanDocumentSection_10235()
            {
                var rdpUserApproval = Users.GetQaUserRDPLoanApproval();
                var efpLoginPage = new EfpLoginPage(Driver, rdpUserApproval);
                var efpPage = new EFPPage(Driver, rdpUserApproval);
                var efpLoanDocumentPage = new EfpLoanDocumentsPage(Driver, rdpUserApproval);
               
                efpLoginPage.LogInUser(rdpUserApproval);

                efpPage.AssertEfpPageLoaded();
                efpPage.ClickLoanDocumentsTab();

                efpLoanDocumentPage.AssertEfpLoanDocumentsPageLoaded();
                efpLoanDocumentPage.ClickCustomerState();
                efpLoanDocumentPage.SelectUtahAsState();
                efpLoanDocumentPage.ClickCustomerId();
                efpLoanDocumentPage.EnterCustomerID(rdpUserApproval);
                efpLoanDocumentPage.ClickLookupCustomer();
                efpLoanDocumentPage.AssertDateSignedIsVisibleLoanDocumentsPage();



            }

            [Test]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "AdverseActionDocumentEFPPage_10497")]
            public void AdverseActionDocumentEFPPage_10497()
            {

                var customer = CheckCityOnline.Users.GenNewRandomMember(CustomerType.UT_Turndown);
                CheckCityOnline.Tests.BaseTest.CreateTurndownCustomer(Driver, customer);
                var loanApprovalPage = new CheckCityOnline.Pages.LoanApprovalPage(Driver, customer);
                //assert turndown 
                loanApprovalPage.AssertPersonalLoanTurnDown();
                //navigateToCustomerProfilePage
                NavigateToCustomerProfilePage(customer);
                //CopyCustomerId
                var storeProfilePage = new StoreProfile(Driver, customer);
                var customerId = storeProfilePage.FindCustomerId();
                //LoginEfpPage
                LoginEfpPage();
                //NavigateToAdverseActionEfpPage
                NavigateToAdverseActionEfpPage(customer);
                //EnterCustomerId
                EnterCustomerId(customer, customerId);
                var efpAdverseActionPage = new EfpAdverseAction(Driver, customer);
                efpAdverseActionPage.EfpAssertAdverseActionPdfIsVisible();
                
            }

            private void EnterCustomerId(CurrentUser customer, string customerId)
            {
                var efpAdverseActionPage = new EfpAdverseAction(Driver, customer);
                //efpAdverseActionPage.SelectCustomerState();
                efpAdverseActionPage.EnterCustomerId(customerId);
                efpAdverseActionPage.ClickLookupCustomerId();
            }

            private void NavigateToAdverseActionEfpPage(CurrentUser customer)
            {
               var efpPage = new EFPPage(Driver, customer);
               var efpAdverseActionPage = new EfpAdverseAction(Driver, customer);
               efpPage.ClickAdverseActionTab();
               efpAdverseActionPage.AssertEfpAdverseActionPageLoaded();

            }

            private void LoginEfpPage()
            {
                var rdpUserApproval = Users.GetQaUserRDPLoanApproval();
                var efpLoginPage = new EfpLoginPage(Driver, rdpUserApproval);
                var efpPage = new EFPPage(Driver, rdpUserApproval);
                var efpPageApprovalPage = new EfpLoanApprovalPage(Driver, rdpUserApproval);

                efpLoginPage.LogInUser(rdpUserApproval);
                efpPageApprovalPage.AssertEfpLoanApprovalPageLoaded();

            }

            private void NavigateToCustomerProfilePage(CurrentUser customer)
            {
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var memberPage = new CheckCityOnline.Pages.MemberPage(Driver, customer);
                loanApprovalPage.ClickAccount();

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickUserNameMenu();
                memberPage.ClickProfile();
            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "EditPersonalLoanAmount_8834_UT")]
            public void EditPersonalLoanAmount_8834_UT()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPersonalLoanCustomer);
                var memberPage = new MemberPage(Driver, withPersonalLoanCustomer);
                var storeBankEditPage = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var personalLoanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                var personalLoanDetailPage = new LoanDetailsPage(Driver, withPersonalLoanCustomer);

                memberLoginPage.LogInUser(withPersonalLoanCustomer);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickRequestPersonalLoan();

                storeBankEditPage.AssertStoreBankEdit();
                storeBankEditPage.ClickYesToUseThisBankAccount();
                storeBankEditPage.ClickConfirmBankAccountWithEmailAchHandlers();

                personalLoanApprovalPage.AssertLoanApprovalPage();
                personalLoanApprovalPage.EnterLoanAmount();
                personalLoanApprovalPage.ClickGetAmount();

                personalLoanDetailPage.AssertLoanDetails();
                personalLoanDetailPage.ClickPencilToEditLoanAmount();
                personalLoanDetailPage.EnterNewLoaningAmount();
                personalLoanDetailPage.ClickConfirmNewLoanAmount();

            }

            
            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "NavigateToUploadDocumentPage_9056")]
            public void NavigateToUploadDocumentPage_9056()
            {
                var approvedCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, approvedCustomer);
                var memberPage = new MemberPage(Driver, approvedCustomer);
                var documentUploadServices = new DocumentUploadServices(Driver, approvedCustomer);

                memberLoginPage.LogInUser(approvedCustomer);
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickUploadDocs();
                documentUploadServices.AssertDocumentUploadService();
            }
            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "NavigateToLoanHistoryPage_10493")]
            public void NavigateToLoanHistoryPage_10493()
            {
                var user = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance); ;
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var storeLoanHistoryPage = new StoreLoanHistory(Driver, user);

                memberLoginPage.LogInUser(user);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickLoanHistory();

                storeLoanHistoryPage.AssertStoreLoanHistoryIsVisible();
                
                
            }

            [Test]
            [Category("BrowserStack")]
            [Category("RegressionTest")]
            //[Category("Ignore")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [TestCase(TestName = "NavigateToLoanDocumentListDropDown_10494")]
            public void NavigateToLoanDocumentListDropDown_10494()
            {
                var user = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var storeLoanHistoryPage = new StoreLoanHistory(Driver, user);

                memberLoginPage.LogInUser(user);

                memberPage.AssertMemberPageLoaded();
                memberPage.ClickCustomerMenuButton();
                memberPage.ClickLoanHistory();

                storeLoanHistoryPage.AssertStoreLoanHistoryIsVisible();
                storeLoanHistoryPage.ClickTerms();
            }



            [Test]
            [Category("RegressionTest")]
            [TestCase(TestName = "PaydayPaymentNoIWantToExtendTheDueDate_10252")]
            [Category("Ignore")]
            [Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
             public void PaydayPaymentNoIWantToExtendTheDueDate_10252()
            {
                var withPayDayLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, withPayDayLoanCustomer);
                var memberPage = new MemberPage(Driver, withPayDayLoanCustomer);
                var extensionMessagePage = new ExtensionMessage(Driver, withPayDayLoanCustomer);
                var extensionTypePage = new ExtensionType(Driver, withPayDayLoanCustomer);
                var extensionAcceptPage = new ExtensionAccept(Driver, withPayDayLoanCustomer);
                var extensionTransactPage = new ExtensionTransact(Driver, withPayDayLoanCustomer);
                var paymentThankYouPage = new PaymentThankYou(Driver, withPayDayLoanCustomer);

                //memberLoginPage 
                memberLoginPage.LogInUser(withPayDayLoanCustomer);

                memberPage.AssertMemberPageLoaded();
                

                List<IWebElement> elementList = new List<IWebElement>();
                elementList.AddRange(Driver.FindElements(By.XPath(Elements.MemberPage.Button.ClickPaydayMakePayment)));
                if (elementList.Count > 0)
                {
                    extensionMessagePage.AssertExtensionPageLoaded();
                    extensionMessagePage.ClickNoIWantToExtendDueDate();

                    memberPage.AssertMemberPageLoaded();
                }
                else
                {
                    memberPage.ClickExtend();

                    extensionTypePage.AssertExtensionType();
                    extensionTypePage.ClickSelectDateButton();
                    //extensionTypePage.ClickDateField();
                    //extensionTypePage.ClickCallNextToChangeTheMonth();
                    extensionTypePage.PickDesiredDueDate();
                    extensionTypePage.AchPaymentMethod();
                    extensionTypePage.ClickNext();

                    extensionAcceptPage.AssertExtensionAcceptLoaded();
                    extensionAcceptPage.ClickAccept();

                    extensionTransactPage.AssertExtensionTransactLoaded();
                    extensionTransactPage.ClickIAgreeButton();

                    paymentThankYouPage.AssertPaymentThankYouPageLoaded();
                    paymentThankYouPage.ClickGotItButton();

                    memberPage.AssertMemberPageLoaded();
                }
                
            }


             [Test]
             [Category("Regression")]
             [Category("BrowserStack")]
             [TestCase(TestName = "NewLoanApplyNowPersonal_TX")]
             [Author("Pat Holman", "pholman@softwise.com")]
             public void NewLoanApplyNowPersonal_TX()
             {
                 var customer = Users.GenNewRandomMember(CustomerType.TX_ApprovedPersonalWithBalance);
                 var memberLoginPage = new MemberLoginPage(Driver, customer);
                 var applyPage = new ApplyPage(Driver, customer);
                 var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                 var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                 var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                 var thankYouPage = new LoanThankYouPage(Driver, customer);

                 memberLoginPage.LogInPage(customer);
                 applyPage.ApplyPersonalLoan(customer);

                 //NewLoan
                 loanApprovalPage.AssertLoanApprovalPage();
                 loanApprovalPage.EnterLoanAmount();
                 loanApprovalPage.ClickGetAmount();

                 //PersonalLoanDetailsPage
                 loanDetailsPage.AssertLoanDetails();
                 loanDetailsPage.ClickContinue();

                //loanDisclosurePage for texas customer
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                loanDisclosurePage.ClickIAgreeButtonThree();
                loanDisclosurePage.ClickIAgreeButtonFour();
                loanDisclosurePage.ClickIAgreeButtonFive();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                 thankYouPage.ClickYourOpinionCounts();
                 thankYouPage.ClickEndMySession();
            }


            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPersonal_ID")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPersonal_ID()
            {
                var customer = Users.GenNewRandomMember(CustomerType.ID_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPersonalLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }

            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPersonal_MO")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPersonal_MO()
            {
                var customer = Users.GenNewRandomMember(CustomerType.MO_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPersonalLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPersonal_UT")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPersonal_UT()
            {
                var customer = Users.GenNewRandomMember(CustomerType.UT_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPersonalLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPersonal_WI")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPersonal_WI()
            {
                var customer = Users.GenNewRandomMember(CustomerType.WI_ApprovedPersonalWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPersonalLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.SignPersonalLoanAgreement(customer);
                loanDisclosurePage.ClickSignWisconsinLoanAgreement();
                loanDisclosurePage.ExplicitWait(3);
                loanDisclosurePage.SignAchAuthorization(customer);
                loanDisclosurePage.ClickWisconsinAchAuthorization();


                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }

            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_UT")]
            [Author("Pat Holman", "NewLoanpholman@softwise.com")]
            public void ApplyNowPayday_UT()
            {
                var customer = Users.GenNewRandomMember(CustomerType.UT_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                
                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }

            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_AK")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_AK()
            {
                var customer = Users.GenNewRandomMember(CustomerType.AK_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);
                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

                memberLoginPage.NavigateToLoginPage();
            }

            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_AL")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_AL()
            {
                var customer = Users.GenNewRandomMember(CustomerType.AL_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);
                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);
                
                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();

            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_CA")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_CA()
            {
                var customer = Users.GenNewRandomMember(CustomerType.CA_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();
                loanDisclosurePage.ClickIAgreeButtonTwo();
                //loanDisclosurePage.ClickUtahDeferredLoanAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_HI")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_HI()
            {
                var customer = Users.GenNewRandomMember(CustomerType.HI_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();


                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_KS")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_KS()
            {
                var customer = Users.GenNewRandomMember(CustomerType.KS_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.SignPersonalLoanAgreement(customer);
                loanDisclosurePage.ClickSignKansasConsumerLoanAgreement();
                loanDisclosurePage.SignKansasNoticeToConsumer(customer);
                loanDisclosurePage.ClickSignKansasNoticeToConsumerAgreement();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }
            [Test]
            [Category("Regression")]
            [Category("BrowserStack")]
            [TestCase(TestName = "NewLoanApplyNowPayday_WY")]
            [Author("Pat Holman", "pholman@softwise.com")]
            public void NewLoanApplyNowPayday_WY()
            {
                var customer = Users.GenNewRandomMember(CustomerType.WY_ApprovedPaydayWithBalance);
                var memberLoginPage = new MemberLoginPage(Driver, customer);
                var applyPage = new ApplyPage(Driver, customer);
                var loanApprovalPage = new LoanApprovalPage(Driver, customer);
                var loanDetailsPage = new LoanDetailsPage(Driver, customer);
                var loanDisclosurePage = new LoanDisclosure(Driver, customer);
                var thankYouPage = new LoanThankYouPage(Driver, customer);

                memberLoginPage.LogInPage(customer);
                applyPage.ApplyPayDayLoan(customer);

                //NewLoan
                loanApprovalPage.AssertLoanApprovalPage();
                loanApprovalPage.EnterLoanAmount();
                loanApprovalPage.ClickGetAmount();

                //PersonalLoanDetailsPage
                loanDetailsPage.AssertLoanDetails();
                loanDetailsPage.ClickContinue();

                //LoanDisclosure
                loanDisclosurePage.AssertLoanDisclosurePageLoaded();
                loanDisclosurePage.ClickIAgreeButtonOne();

                ////LoanThankYou
                thankYouPage.AssertLoanThankYouPageLoaded();
                thankYouPage.ClickYourOpinionCounts();
                thankYouPage.ClickEndMySession();
            }

            [Test]
            [TestCase(TestName = "RequestLoanOnlineSelectPickUpLoanAtStoreLocation_9310")]
            [Category("BrowserStack")]
            [Category("SmokeTest")]
            [Author("Ryan Palmer", "rpalmer@softwise.com")]
            public void RequestLoanOnlineSelectPickUpLoanAtStoreLocation_9310()
            {
                var pdaCsutomer = Users.GetCustomer(CustomerType.UT_PendingAll);
                var memberLoginPage = new MemberLoginPage(Driver, pdaCsutomer);
                var memberPage = new MemberPage(Driver, pdaCsutomer);
                var storeBankEdit = new StoreBankEdit(Driver, pdaCsutomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, pdaCsutomer);
               // var loanAtStorePage = new Loan;
            }

           

        }
    }
}