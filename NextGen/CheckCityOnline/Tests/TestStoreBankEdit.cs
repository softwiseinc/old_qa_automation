﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;


namespace CheckCityOnline.Tests
{

    partial class AllTests
    {
        private partial class AllTestsCases
        {
           

            [Test]
            [Category("Confirm Bank AccountLink")]
            [Category("BrowserStack")]
            [TestCase(TestName = "ConfirmBankAccountNoEmailNoAch_7728_UT")]
            [Author("Ryan Palmer", "Rpalmer@softwise.com")]
            public void ConfirmBankAccountNoEmailNoAch_7728_UT()
            {
                var withPersonalLoanCustomer = Users.GetCustomer(CustomerType.UT_ApprovedPersonalWithBalance);
                var storeBankEdit = new StoreBankEdit(Driver, withPersonalLoanCustomer);
                var loanApprovalPage = new LoanApprovalPage(Driver, withPersonalLoanCustomer);
                storeBankEdit.NavigateToStoreBankEditPage(withPersonalLoanCustomer);
                storeBankEdit.ClickNoBankAccountHandler();
                storeBankEdit.EmailNoticeNoHandler();
                storeBankEdit.ClickConfirmBankAccount();
                storeBankEdit.ConfirmPayFrequencyHandler();
                loanApprovalPage.AssertLoanApprovalPage();
            }
        }
    }
}