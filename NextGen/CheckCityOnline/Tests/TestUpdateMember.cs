﻿using AutomationResources;
using CheckCityOnline.Pages;
using NUnit.Framework;

namespace CheckCityOnline.Tests
{
    internal partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("BrowserStack")]
            //[Ignore("Test Broke", Until = "2022-01-30 12:00:00Z")]
            [Category("RegressionTest")]
            [TestCase(TestName = "EditIncome_7804_CA")]
            [Author("Phil Ivey", "pivey@softwise.com")]
            public void EditIncome_7804_CA()
            {
                // get user object
                var user = Users.GetQaUser007();
                // create page objects
                var memberLoginPage = new MemberLoginPage(Driver, user);
                var memberPage = new MemberPage(Driver, user);
                var updateMemberPage = new UpdateMember(Driver, user);

                memberLoginPage.LogInUser(user);
                memberPage.ClickUserNameMenu();
                memberPage.ClickProfile();
                updateMemberPage.ClickEditIncome(user);
            }}
    }
}