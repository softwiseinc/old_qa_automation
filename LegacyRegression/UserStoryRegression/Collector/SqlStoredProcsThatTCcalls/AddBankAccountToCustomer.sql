/**********************************************************
AddBankAccountToCustomer
**********************************************************/

print 'AddBankAccountToCustomer'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('AddBankAccountToCustomer')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure AddBankAccountToCustomer
end
go

create procedure AddBankAccountToCustomer
@ContactID TID,
@Location_ID varchar(4),
@RoutingAccountNum TRoutingAccountNum out

as
set NoCount on
set Ansi_Warnings off

-- clear the session
declare @SessionID TSessionID, @ComputerName varchar(2000)
 exec GetSessionID @SessionID out
 select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
 delete Session where ID = @SessionID and Computer_Name = @ComputerName

begin
--declare @SessionID TSessionID, @ComputerName varchar(2000)
exec GetSessionID @SessionID out
select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- todo: change the 'CORP' entry to a value appropriate to the target database
insert into Session
(ID, User_ID, Location_ID, Computer_Name)
values(@SessionID, 'OWN', @Location_ID, 'SWC7QATC01');
end






--try
 declare
 --Required, the Contact or Customer ID this account belongs to.
-- @RoutingAccountNum TRoutingAccountNum,  --Required, the bank account to associate this customer with.
 @NextRouteNum TRoutingID,
 @RandAcctNum TRoutingAccountNum,
 @Type_ID TShortID,
 @Complete TLogical = 0,  --Required
 @IsClosed TLogical = 0,  --Required
 @RoutingDigits Integer = 9, --Required, 9 = United States, 8 = Canadian default is 9.
 @User_ID TUserID,
 @Notes varchar(120),
 @Counter int,  --Required, the user that created this association.
 @printLine varchar(75),
 @Results int,
 @EndCount int
 
set @Type_ID = 'CHK'
	
		--set @ContactID = '001-0000101'
		set @NextRouteNum = (SELECT TOP 1 RoutingID FROM Bank ORDER BY NEWID())-- randomly picks from bank table
		set @RandAcctNum = (select convert(numeric(12,0),rand() * 999999999)) --randomly creates 9 digit number
		set @RoutingAccountNum = @NextRouteNum + @RandAcctNum
		set  @Notes = 'ContactID=' + @ContactID + ' CCC Testing'
		set @printLine = 'number for ' + CONVERT(varchar(25),@ContactID) + ' is ' + @RoutingAccountNum + '  ExtID=' + Convert(varchar(3),@Counter)
		print @printLine
		
	exec @Results =	BankAccountCreateAPI
		  @NextRouteNum , --Conditionally Required, if you supply the RoutingAccountNum parameter then this is not required.
		  @RandAcctNum,  --Conditionally Required, if you supply the RoutingAccountNum parameter then this is not required.
		  @RoutingAccountNum,  --Conditionally Required, if you supply the RoutingID and AccountNumber parameters then this parameter is not required.
		  @Type_ID,  --Required, see the BankAccountType table for more information.
		  @DateAccountOpened = null,  --Not Required
		  @DateEntered = null,  --Not Required.  Date the account was entered into the system.  If not supplied defaults to today.
		  @User_ID = 'OWN',  --Required, The user that entered this account.
		  @ApprovedBy = null,  --Not Required, The user that approved (verified) this account.
		  @Complete = 0,  --Required
		  @IsClosed = 0,  --Required
		  @RoutingDigits =  @RandAcctNum,  --CONVERT(int,@RandAcctNum), --Required, 9 = United States, 8 = Canadian default is 9.
		  @Notes = @Notes
  
		--if @@Error <> 0 or @Results <> 0 goto ErrorProcBankAcctCreate
  
	exec @Results = ContactAccountCreateAPI
			@ContactID,
			@RoutingAccountNum,  --Required, the bank account to associate this customer with.
			@User_ID   --Required, the user that created this association.
			
			
 exec GetSessionID @SessionID out
 select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
 delete Session where ID = @SessionID and Computer_Name = @ComputerName

--finally
  SuccessProc:
  return 0

--except
  ErrorProc:
  print 'AddBankAccountToCustomer Failed'
  return 1

--end
go

grant execute on AddBankAccountToCustomer to Public
go