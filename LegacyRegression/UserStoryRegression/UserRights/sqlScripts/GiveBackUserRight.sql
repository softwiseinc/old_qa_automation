
--  GiveBackUserRight Select * from userdata

print 'GiveBackUserRight'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('GiveBackUserRight')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure GiveBackUserRight
end
go

create procedure GiveBackUserRight @LocationID Varchar(8), @TellerID varchar(8),@Right_ID int,@RightIsActive int out 
as
set NoCount on
set Ansi_Warnings off

declare
@HasLocation bit

set @LocationID = (select REPLACE(@LocationID, ' ', ''))
set @TellerID = (select REPLACE(@TellerID, ' ', ''))

set @HasLocation = 0
--try
			if exists
				( Select User_ID from UserLocation where User_ID = @TellerID and Location_ID = @LocationID)
				begin
					set @HasLocation = 1
				end
				
			if(@HasLocation = 1)
				begin
					if exists (Select * from UserRight where User_ID = @TellerID and Right_ID = @Right_ID)
						begin
							delete from UserRight where User_ID = @TellerID and Right_ID = @Right_ID
						end
				end
				
		if ((Select Right_ID from UserRight where User_ID = @TellerID and Right_ID = @Right_ID) = NULL)   --  Select * from UserRight
			begin
				set @RightIsActive = 1
			end
		else
			set @RightIsActive = 0

--finally
  SuccessProc:
  return 0

--except
  ErrorProc:
  print 'GiveBackUserRight Failed'
  return 1

--end
go

grant execute on GiveBackUserRight to Public
go

