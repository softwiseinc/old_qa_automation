

/****** Object:  StoredProcedure [dbo].[ContactAccountCreateAPI]    Script Date: 10/01/2014 09:04:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[ContactAccountCreateAPI]
	@ContactID TID, --Required, the Contact or Customer ID this account belongs to.
  @RoutingAccountNum TRoutingAccountNum,  --Required, the bank account to associate this customer with.
  @User_ID TUserID  --Required, the user that created this association.

as
	set NoCount on
	Set ansi_Warnings off
	--try
    declare
      @Now DateTime

    select @Now = GetDate()

		begin transaction
    --try

      if not exists (select * from BankAccount where RoutingAccountNum = @RoutingAccountNum)
      begin
        RaisError('ContactAccountCreateAPI: The bank account provided (%s) does not exist in the BankAccount table.', 16, -1, @RoutingAccountNum)
        goto ErrorTransaction
      end

      if not exists(select * from ContactAccount where Contact_ID = @ContactID and RoutingAccountNum = @RoutingAccountNum)
      begin
        insert into ContactAccount
        (
          Contact_ID,
          RoutingAccountNum,
          Since,
          User_ID
        )
        values
        (
          @ContactID,
          @RoutingAccountNum,
          @Now,
          @User_ID
        )
        if @@Error <> 0 goto ErrorTransaction
      end

      goto SuccessTransaction

    --except
      ErrorTransaction:
      if @@trancount > 0 rollback transaction
      goto ErrorProc

    --finally
      SuccessTransaction:
      commit transaction
    --end

  --finally
    SuccessProc:
    return 0

  --except
    ErrorProc:
    print 'ContactAccountCreateAPI Failed';
    return 1

  --end

GO


