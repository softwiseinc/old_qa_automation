/**********************************************************
  CreateSession
**********************************************************/

print 'CreateSession'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('CreateSession')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure CreateSession
end
go

create procedure CreateSession
@LoactionID varchar(3),
@ComputerName varchar(25),
@SessionID TSessionID out

as
set NoCount on
set Ansi_Warnings off
--try
 begin
--declare @SessionID TSessionID
exec GetSessionID @SessionID out
--select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- todo: change the 'CORP' entry to a value appropriate to the target database
insert into Session
(ID, User_ID, Location_ID, Computer_Name)
values(@SessionID, 'MAN', @LoactionID, @ComputerName);
end


--finally
  SuccessProc:
  return 0

--except
  ErrorProc:
  print 'CreateSession Failed'
  return 1

--end
go

grant execute on CreateSession to Public
go