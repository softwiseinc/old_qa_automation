--  TakeAwayUserRight Select * from userdata

print 'TakeAwayUserRight'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('TakeAwayUserRight')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure TakeAwayUserRight
end
go

create procedure TakeAwayUserRight @LocationID Varchar(8), @TellerID varchar(8),@Right_ID int,@RightIsDeactive int out 
as
set NoCount on
set Ansi_Warnings off

declare
@HasLocation bit

set @LocationID = (select REPLACE(@LocationID, ' ', ''))
set @TellerID = (select REPLACE(@TellerID, ' ', ''))

set @HasLocation = 0
--try
			if exists
				( Select User_ID from UserLocation where User_ID = @TellerID and Location_ID = @LocationID)
				begin
					set @HasLocation = 1
				end
				
			if(@HasLocation = 1)
				begin
					if not exists (Select * from UserRight where User_ID = @TellerID and Right_ID = @Right_ID)
						begin
							insert into UserRight  (User_ID,Right_ID,Allowed)values(@TellerID,@Right_ID,0)-- Debt Date
						end
				end
				
		if exists (Select * from UserRight where User_ID = @TellerID and Right_ID = @Right_ID)
			begin
				set @RightIsDeactive = 1
			end
		else
			set @RightIsDeactive = 0

--finally
  SuccessProc:
  return 0

--except
  ErrorProc:
  print 'TakeAwayUserRight Failed'
  return 1

--end
go

grant execute on TakeAwayUserRight to Public
go

