print 'DebtValidationOptSet'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('DebtValidationOptSet')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure DebtValidationOptSet
end
go

-- test Debt Validation Proc
-- returns '' if the validation is successful else @ERROR_MSG
create procedure DebtValidationOptSet
    @DebtorID TID,
    @ERROR_MSG varchar(128) out
as
    set NoCount on
    set ansi_warnings off
Declare @IsSet int

if
  exists
  (Select * from ContactCommunicationOptOut where Contact_ID = @DebtorID)
  return 0
  else
    set @ERROR_MSG = 'Communication Opt Out for Debtor ' + @DebtorID + ' is not set.'
    return 1
go

grant execute on DebtValidationOptSet to public
go
