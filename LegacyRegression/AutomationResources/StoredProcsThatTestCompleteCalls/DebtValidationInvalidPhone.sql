/**********************************************************
  DebtValidationInvalidPhone
**********************************************************/

print 'DebtValidationInvalidPhone'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('DebtValidationInvalidPhone')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure DebtValidationInvalidPhone
end
go

-- test Debt Validation Proc
-- returns '' if the validation is successful else @ERROR_MSG
create procedure DebtValidationInvalidPhone
    @DebtorID TID,
    @ERROR_MSG varchar(128) out
as
    set NoCount on
    set ansi_warnings off

if
  exists
  (Select * from ContactPhone where Contact_ID = @DebtorID and Status_ID = 'INV')
  begin
  set @ERROR_MSG = 'Debtor ' + @DebtorID + ' has at least one invalid phone number'     return 1
	end
  else
     return 0 
go

grant execute on DebtValidationInvalidPhone to public
go
