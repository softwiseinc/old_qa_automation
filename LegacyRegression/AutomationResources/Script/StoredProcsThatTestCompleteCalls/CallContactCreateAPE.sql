/*
begin
declare @SessionID TSessionID, @ComputerName varchar(2000)
exec GetSessionID @SessionID out
select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- todo: change the 'CORP' entry to a value appropriate to the target database
insert into Session
(ID, User_ID, Location_ID, Computer_Name)
values(@SessionID, 'OWN', '005', 'phili-o980-7');
end
*/

/**********************************************************
CallContactCreateAPI
**********************************************************/

print 'CallContactCreateAPI'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('CallContactCreateAPI')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure CallContactCreateAPI
end
go

create procedure CallContactCreateAPI
@FirstName TShortName,
@LastName TShortName,
@Location_ID TLocationID,
@ContactID TID out

as
set NoCount on
set Ansi_Warnings off
--try
/*
begin
declare @SessionID TSessionID, @ComputerName varchar(2000)
exec GetSessionID @SessionID out
select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- todo: change the 'CORP' entry to a value appropriate to the target database
insert into Session
(ID, User_ID, Location_ID, Computer_Name)
values(@SessionID, 'MAN', '001', 'SWC7QATC01');
end
*/
 
 
set @FirstName = (SELECT REPLACE(@FirstName, ' ', ''))
set @LastName = (SELECT REPLACE(@LastName, ' ', ''))
  
Declare
 --@Location_ID TLocationID = '001', 
  @IsCustomer TLogical = 1, --Required, Values (0, 1) if true this is an customer otherwise it is an Employer, Company, or Client.
  @IsEmployer TLogical = 0, --Required, if true this is an Employer otherwise it is an Customer, Company, or Client.
  @IsCompany TLogical = 0, --Required, if true this is an Company otherwise it is an Customer, Employer, or Client.  
  @IsClient TLogical = 0, ----Required, if true this is an Client otherwise it is an Customer, Employer or Company.  
  @CompanyName TLongName = null, --Required, if this is a company (IsCompany = 1) or is an Employer then the Company name is required.  If it is a customer or client then it is not.  
 -- @FirstName TShortName = 'Test', --Required, if this is a Customer or Client.  
  --@LastName TShortName = 'StatusPhone1', --Required, if this is a Customer or Client.  
  @MiddleName TShortName = null, --Not Required  
  @User_ID TUserID = 'MAN', --Not Required, the user that entered this contact.  
  @AddressLine1 TAddress = '654 Three Street', --Not Required
  @AddressLine2 TAddress = null, --Not Required  
  @City TCity = 'Provo', --Not Required  
  @State TState = 'UT', --Not Required
  @Zip TZipCode = 84653, --Not Required  
  @SSN TSSN = '585-45-2345', --Not Required  
  @DateEntered DateTime = null,  --Not Required  
  @ExternalID TExternalID = null,  --Not Required, some external identifier for this customer
  @Primary_ContactMethod_ID TShortID = 'MAI',  --Required, The primary contact method see the ContactMethod table for more info.
  @Address_Type_ID TShortID = null,  --Not Required, see the AddressType table for more values.
  @Address_Status_ID TShortID = 'NEW', --Not Required, see the AddressStatus table for more values.
  @Address_Number Integer = null,  --Not Required, if the customer has more than one address to fill in, Use the Contact Address Add API.
  @PrimaryPhone TPhone = null,  --Not Required
  @Birthday DateTime = null,  --Not Required
  @Collection_Location_ID TLocationID = null, --Not Required, Assigned collection location for this debtor.
  @DebtDefaultDate DateTime = null, --Not Required, the last date a new debt was created for this debtor.
  @DebtAmount Money = null,  --Not Required, the current total outstanding debt in the collector for this debtor.
  @IsDebtor TLogical = 0,  --Not Required, if this customer is a debtor or has outstanding balance in the collector then 1 else 0.
  @DebtReturnDate DateTime = null, --Not Required, the last date collateral was returned for this customer.
  @NextReviewDate TDate = null, --Not Required, the next date we plan to review this debtor in the collector.
  @LastWorkedDate DateTime = null, --Not Required, Date this debtor was last worked in the collector.
  @IsActiveMilitary TLogical = 0, --Required 0 = no, 1 = yes.
  @DoNotContact TLogical = 0, --Required 0 = no, 1 = yes.
  --@ContactID TID --out --Required the new customer ID
  @Last4 int = (select convert(numeric(12,0),rand() * 9999)),
  @AddNum int = (select convert(numeric(12,0),rand() * 999)),
  @Result bit
  
  set @SSN = '58545-'+convert(varchar(4),@Last4)
  set @AddressLine1 = convert(varchar(3),@AddNum)+'4 Three Street'



 exec @Result = ContactCreateAPI 
 @Location_ID, 
  @IsCustomer, 
  @IsEmployer, 
  @IsCompany, 
  @IsClient, 
  @CompanyName, 
  @FirstName, 
  @LastName, 
  @MiddleName, 
  @User_ID, 
  @AddressLine1, 
  @AddressLine2, 
  @City, 
  @State, 
  @Zip, 
  @SSN, 
  @DateEntered, 
  @ExternalID, 
  @Primary_ContactMethod_ID, 
  @Address_Type_ID, 
  @Address_Status_ID, 
  @Address_Number, 
  @PrimaryPhone, 
  @Birthday, 
  @Collection_Location_ID, 
  @DebtDefaultDate, 
  @DebtAmount, 
  @IsDebtor, 
  @DebtReturnDate, 
  @NextReviewDate, 
  @LastWorkedDate, 
  @IsActiveMilitary, 
  @DoNotContact, 
  @ContactID out
  
  if(@Result <> 0)
  begin	
	set @ContactID = 'There was an error'
	goto ErrorProc
  end
  
-- set @ContactID = (select REPLACE(@ContactID, ' ', '')) 
print '@ContactID'
  
  
  
  --finally
  SuccessProc:
  return 0

--except
  ErrorProc:
  print 'CallContactCreateAPI Failed'
  return 1

--end
go

grant execute on CallContactCreateAPI to Public
go
 /* 
--  declare @SessionID TSessionID, @ComputerName varchar(2000)
 exec GetSessionID @SessionID out
 select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
 delete Session where ID = @SessionID and Computer_Name = @ComputerName
 
 */