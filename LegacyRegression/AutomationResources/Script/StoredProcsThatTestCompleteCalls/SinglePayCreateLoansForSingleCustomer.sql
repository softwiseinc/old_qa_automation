
/****** Object:  StoredProcedure [dbo].[SinglePayCreateLoansForSingleCustomer]    Script Date: 01/20/2016 14:12:12 ******/


print 'SinglePayCreateLoansForSingleCustomer'
go

if
  exists
  (
    select * from SysObjects
      where ID = Object_ID('SinglePayCreateLoansForSingleCustomer')
        and ObjectProperty(ID, 'IsProcedure') = 1
  )
begin
  drop procedure SinglePayCreateLoansForSingleCustomer
end
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




Create procedure [dbo].[SinglePayCreateLoansForSingleCustomer]
@zCollateral varchar(3) = NULL,
@Customer_ID TID,
@SessionID TSessionID,
@DeferredID TID output 
as

  If(@zCollateral is NULL)
	set @zCollateral = 'CHK'
	
  -- declare with variable names and types used in the when and then statements of the feature
  
--  begin
--declare @SessionID TSessionID, @ComputerName varchar(2000),@LocationID TLocationID = '001'
--exec GetSessionID @SessionID out
--select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
---- todo: change the 'CORP' entry to a value appropriate to the target database
--insert into Session
--(ID, User_ID, Location_ID, Computer_Name)
--values(@SessionID, 'OWN', @LocationID, 'SWC7QATC01');
--end

  declare
    @Result int
 --   , @CustomerID TID
, @LoanTypeID TShortID
, @RequestAmount Money
, @UseACHPayout TLogical
--, @LocationID TLocationID
, @OriginationDate Date
, @FundingDate Date
, @DueDate Date
, @PickupFlag TLogical
, @TotalDue Money
, @RateAmount Money
, @FlatAmount Money
, @TotalFees Money
, @ACHPayoutRoutingAccountNum TRoutingAccountNum
, @LoanID TID
, @CollateralTypeID TShortID
, @CollateralValue Money
, @CollateralLocation TLocationID
, @RoutingAccountNum TRoutingAccountNum
, @CheckNumber TCheckNum
, @CollateralDate Date
, @LeaveForReviewByCSR TLogical
, @AddQueuedSale TLogical
, @varName varchar(1000)
, @varValue varchar(1000)
, @storyAssertMessage varchar(150)
, @D_PaymentLateFees money
, @D_PayOffLateFee money
, @D_AllLateFees money
,@LocationID varchar(3)

--, @SessionID TSessionID


--exec GetSessionID @SessionID out

  Select * from Contact where ID = '001-0000261'
  if(@Customer_ID is NULL)
	set @Customer_ID = (SELECT TOP 1 ID FROM Contact   where IsCustomer = 1 and Location_ID = @LocationID ORDER BY NEWID())-- randomly picks from bank table

  
  -- convert each scenario to a set, exec, assert block
    select
    @Customer_ID = @Customer_ID,  
    @LoanTypeID = 'SPC', 
    @RequestAmount = '320.00', 
    @UseACHPayout = 0, 
    @LocationID = (select Location_ID from Session where ID = @SessionID), 
    @OriginationDate=   null,  
    @FundingDate = @OriginationDate, 
    @DueDate = null, 
    @PickupFlag = null, 
    @TotalDue = null, 
    @RateAmount = null, 
    @FlatAmount = null, 
    @TotalFees = null, 
    @ACHPayoutRoutingAccountNum = null, 
    @LoanID = null, 
    @CollateralTypeID = @zCollateral, 
    @CollateralValue = '352.00', 
    @CollateralLocation = null, 
    @RoutingAccountNum = null, 
    @CheckNumber = '3405', 
    @CollateralDate =   null, 
    @LeaveForReviewByCSR = null, 
    @AddQueuedSale = null
    
    
   set @OriginationDate= (select DATEADD(day, -14, (select System_Date from Location where ID = @LocationID)))  
   set @CollateralDate =   (select DATEADD(day, 14, @OriginationDate))
   set @CheckNumber = (select convert(numeric(12,0),rand() * 9999))
   
   set @RequestAmount =   (convert(money, (select convert(numeric(12,0),rand() * 999))))
   set @CollateralValue = (convert(money,(@RequestAmount * .1 )) + @RequestAmount)

  
  exec @Result = SinglePaymentLoanCreateAPI
    @Customer_ID, 
    @LoanTypeID, 
    @RequestAmount, 
    @UseACHPayout, 
    @LocationID out, 
    @OriginationDate out, 
    @FundingDate out, 
    @DueDate out, 
    @PickupFlag out, 
    @TotalDue out, 
    @RateAmount out, 
    @FlatAmount out, 
    @TotalFees out, 
    @ACHPayoutRoutingAccountNum out, 
    @LoanID out, 
    @CollateralTypeID out, 
    @CollateralValue out, 
    @CollateralLocation out, 
    @RoutingAccountNum out, 
    @CheckNumber, 
    @CollateralDate out, 
    @LeaveForReviewByCSR, 
    @AddQueuedSale
    
    set @DeferredID = @LoanID
    
  --  declare @SessionID TSessionID, @ComputerName varchar(2000)
-- exec GetSessionID @SessionID out
-- select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- delete Session where ID = @SessionID and Computer_Name = @ComputerName
    
--go

grant execute on SinglePayCreateLoansForSingleCustomer to Public
go