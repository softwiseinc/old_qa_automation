

-----STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE----
-----STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE---------STEP ONE----

---   ABOUT  MIN -----------

/*

-------------------------------------------------------------------------------------THIS SCRIPT CREATES TABLE NewTestingStateCusID, THEN UPDATES CONTACT ADDRESS


llllllllllllllllllllllllllllllllll
*/
begin
declare @SessionID TSessionID, @ComputerName varchar(2000)
exec GetSessionID @SessionID out
select @ComputerName = hostname from Master.dbo.sysprocesses where spid = @@spid
-- todo: change the 'CORP' entry to a value appropriate to the target database
insert into Session
(ID, User_ID, Location_ID, Computer_Name)
values(@SessionID, 'OWN', '001', '10.127.192.99');
end

/*
llllllllllllllllllllllllllllllllllllllllll 
ccc7sqltest.checkcity.local=10.127.192.99 --qa UtahTest server 10.127.192.99   ---    
staging Utah3 server 10.127.192.130  -- 
swdpqabuild01

 */
go

if
	exists
	(
		select * from SysObjects
			where ID = Object_ID('NewTestingStateCusID')
				and ObjectProperty(ID, 'IsUserTable') = 1
	)
begin
	drop table NewTestingStateCusID
end
go

create table NewTestingStateCusID   --  select * from NewTestingStateCusID
(
	ID INT NOT NULL identity PRIMARY KEY, Customer_ID TID, State varchar(3),Zip TZipcode,DEDescripion varchar(16), TestEmail varchar(129)
)
go
// DEDescripion = 
grant Delete, Insert, References, Select, Update on NewTestingStateCusID to public
go

go

if
	exists
	(
		select * from SysObjects
			where ID = Object_ID('DecisionType')
				and ObjectProperty(ID, 'IsUserTable') = 1
	)
begin
	drop table DecisionType
end
go

create table DecisionType   --  select * from NewTestingStateCusID  ------  select * from DecisionType
(
	ID INT NOT NULL identity PRIMARY KEY, DecisionTypeDescription varchar(16),NumberOut int,DecisionTypeCount int)
go

grant Delete, Insert, References, Select, Update on DecisionType to public
go
------------------
if
	exists
	(
		select * from SysObjects
			where ID = Object_ID('EmailDomains')
				and ObjectProperty(ID, 'IsUserTable') = 1
	)
begin
	drop table EmailDomains
end
go

create table EmailDomains   
(ID INT NOT NULL identity PRIMARY KEY, EmailDomain varchar(129))
go

grant Delete, Insert, References, Select, Update on EmailDomains to public
go
------------
 set nocount ON
  Declare @CusID TID,@ExistingEmail TEmailAddress,@SSN TSSN ,@FirstName TShortName,@LastName TShortName
  ,@Domain varchar(25)  -- 'ivey.pro' 'nostromorp.com' 'brianjohnson.work' 'bendarling.work' 'msgnext.com' 'rajeshdas.work' 'ravikarmacharya.work'
  ,@OverAllCount int = 1,@AtIndex int,@FirstPart varchar(100),@LastPart varchar(20),@StringLen int, @NewEmail varchar(129), @DashIndex int, @SSN1 int, @SSN2 int,@SSN3 int
  , @NewSSN TSSN, @TopNumLoans int,@NewLastName TShortName,@CurrentState varchar(2),@NewState varchar(2),@LoanCount int = 0,@DomainCount int,@DomainID int,@NumOfDomains int,@NumOfStates int
  ,@StateCounter int, @CustomerZip TZipCode, @NewZip varchar(5),@NewCity TCity


Insert into EmailDomains (EmailDomain) values('ivey.pro')
Insert into EmailDomains (EmailDomain) values('nostromorp.com')
Insert into EmailDomains (EmailDomain) values('brianjohnson.work')
Insert into EmailDomains (EmailDomain) values('bendarling.work')
Insert into EmailDomains (EmailDomain) values('msgnext.com')



Insert into DecisionType (DecisionTypeDescription,NumberOut,DecisionTypeCount) values('Approved',100,0)  
Insert into DecisionType (DecisionTypeDescription,NumberOut,DecisionTypeCount) values('Pending All',50,0)
Insert into DecisionType (DecisionTypeDescription,NumberOut,DecisionTypeCount) values('Turndown',50,0) 
Insert into DecisionType (DecisionTypeDescription,NumberOut,DecisionTypeCount) values('Turndown Lead',50,0) --- 250 x 5 = 1250  * 5 = 6,250





select @TopNumLoans = sum(NumberOut) from DecisionType DecisionType  --  select sum(NumberOut) from DecisionType DecisionType current 4482

Declare @State Table (ID INT NOT NULL identity PRIMARY KEY, State varchar(2))
Insert into @State (State) values('UT')  --
Insert into @State (State) values('TX')  -- 
Insert into @State (State) values('WY')  -- 
Insert into @State (State) values('CA')  -- 
Insert into @State (State) values('NV')
Insert into @State (State) values('ID')
Insert into @State (State) values('MO')
Insert into @State (State) values('HI')
Insert into @State (State) values('KS')
Insert into @State (State) values('AK')
Insert into @State (State) values('WI')


/*
	sum NumberOut = Number of loan types = 250
	sum count(*) @State = Number of States = 6
	@NumOfDomains = 7
	 --- 250 x 6 = 1500  * 7 = 10500                (426--852--1278--1704--2130--2556--2982)
	 10500 customer records to work with 

*/

select @NumOfStates = count(*) from @State
select @NumOfDomains = count(*) from EmailDomains

set @DomainCount = 1

	while @DomainCount <=@NumOfDomains
	BEGIN
	set @Domain = (select EmailDomain from EmailDomains where ID = @DomainCount)
	set @StateCounter = 1		
			while @StateCounter <= @NumOfStates
			BEGIN
			Select @NewState = State from @State where ID = @StateCounter
			update DecisionType set DecisionTypeCount = 0
			set @OverAllCount = 1
			DECLARE Customer_Cursor  CURSOR FOR
			select Distinct top (cast(@TopNumLoans as int)) c.ID ,c.State,c.Zip 
			---------------------------18867
			--select Distinct top 71 c.ID ,c.State,c.Zip 
			---------------------------18867
			--select Distinct count(*)      
			-----------c.ID ,c.State,c.Zip 
			---------------------------628193
			from Contact c
					left join Deferred d on c.ID = d.Customer_ID and d.AmountDue = 0
					where  c.State = 'UT' 
								and c.State is not NULL
								and c.SSN like '%-%' and c.SSN is not NULL
								and  c.LastName not in ('Approved','PendingAll','OpenLoan','Turndown','TurndownLead')
								and c.LastName is not NULL and c.FirstName is not NULL
								and exists (select Value from ContactIdentification ci WHERE ci.Contact_ID = C.ID and Type_ID = 'DL')
								and  exists (select State from ContactIdentification ci WHERE ci.Contact_ID = C.ID and Type_ID = 'DL')
								and  exists (select Phone from ContactEmployer ce WHERE ce.Contact_ID = C.ID)
								and  exists (select 1 from ContactEmployer ce WHERE ce.Contact_ID = C.ID)
								and exists (select Contact_ID from ContactAccount ca where ca.Contact_ID = C.ID group by Contact_ID having Count(*) = 1)
								and exists (select Contact_ID, Count(*) numOfJobs from ContactEmployer ce where ce.Contact_ID = C.ID group by Contact_ID having Count(*) = 1)
								and  exists (select 1 from ContactAccount ca WHERE ca.Contact_ID = C.ID)  ---IF THE CONTACTACCOUNT TABLdE HAS THE ID FROM THE CONTACT IT EXIST AND WILL NOT BE SELECTED
								and  exists (select 1 from ContactAddress cadd WHERE cadd.Contact_ID = C.ID and IsPrimary = 1)
								and not exists (Select 1 from Turndown T where T.Customer_ID = C.ID)
								and not exists (Select 1 from DebtorDebt dd join Debt d on dd.Debt_ID = d.ID where dd.IsActive = 1 and d.Amount > 0 and dd.Debtor_ID = C.ID) 
								and not exists (Select 1 from NewTestingStateCusID ntscid where ntscid.Customer_ID = C.ID)

															

					OPEN Customer_Cursor 
					FETCH NEXT FROM Customer_Cursor   --Start the cursor

					INTO @CusID,@CurrentState,@CustomerZip
	
					WHILE @@FETCH_STATUS = 0  --while there is a loaded record, keep processing
				BEGIN
		
					select  top 1 @NewLastName = DecisionTypeDescription from DecisionType where DecisionTypeCount < NumberOut
					print '---Number '+cast(@OverAllCount as varchar)+' This is the current customer INFO --- CusID = ' + @CusID + ' Current State = '+@CurrentState+' Current zip = '+@CustomerZip

					-----------------------------Change STATE----------------------------------------------
					if(@NewState = 'UT')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and City In ('Fort Worth','Dallas','Plano','Arlington','Grapevine','Houston','Baytown','Pasadena','Sugar Land'
					,'Missouri City', 'Austin','San Antonio','Amarillo','Lubbock','Odessa','Beaumont','Galveston','Corpus Christi','Victoria','Temple','Waco','Wichita Falls','El Paso'
					,'Odessa','Midland') and State = @NewState  and Zip is not NULL and City is not NULL order by NewID()
					end
					if(@NewState = 'TX')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end
					
					if(@NewState = 'WY')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'CA')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'NV')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'ID')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'MO')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'HI')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'KS')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'AK')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					if(@NewState = 'WI')
					begin
					select Top 1 @NewZip = Zip,@NewCity = City from Contact where IsCustomer = 1 and State = @NewState and Zip is not NULL and City is not NULL order by NewID()
					end

					update ContactAddress set State = @NewState where Contact_ID = @CusID and IsPrimary = 1 
					update ContactAddress Set Zip = @NewZip where Contact_ID = @CusID and IsPrimary = 1 
					update ContactAddress set City = @NewCity where Contact_ID = @CusID and IsPrimary = 1 
					select @NewCity = City, @NewZip = Zip,@NewState = State from ContactAddress where Contact_ID = @CusID and IsPrimary = 1 
			
					print '------------------------------After update Customer ID = '+@CusID+' New State is '+@NewState+' New Zip is '+@NewZip+' ------New City IS '+@NewCity
					insert into NewTestingStateCusID (Customer_ID,State,Zip,DEDescripion,TestEmail) values (@CusID,@NewState,@NewZip,@NewLastName,@Domain)

					update DecisionType set DecisionTypeCount = DecisionTypeCount + 1 where DecisionTypeDescription = @NewLastName
					-- SELECT TOP DecisionTypeDescription from DecisionType where DecisionTypeCount < NumberOut
			
					set @OverAllCount = @OverAllCount +1
					print''
					FETCH NEXT FROM Customer_Cursor  INTO  @CusID,@CurrentState,@CustomerZip  --fetch next record
						
					END
					CLOSE Customer_Cursor    --close and deallocate
					DEALLOCATE Customer_Cursor 
			set @StateCounter = @StateCounter + 1	
			END	--while @StateCounter <= @NumOfStates
		set @DomainCount = @DomainCount + 1		
		END --while @DomainCount <=@NumOfDomains	

		
		
