-- #################[ run this script once at the very end of a restore]########################
USE [ColoradoTest]
GO
/****** Object:  User [softwiseapi_user]    Script Date: 5/4/2021 4:35:51 PM ******/
DROP USER [softwiseapi_user]
GO

CREATE USER [softwiseapi_user] FOR LOGIN [softwiseapi_user]
GO
ALTER ROLE [db_datareader] ADD MEMBER [softwiseapi_user]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [softwiseapi_user]
GO
-- ###################[ API / CCO login user ]
ALTER USER WebUser WITH LOGIN=WebUser
GO