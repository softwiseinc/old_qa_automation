-- ####################################[ Pass #1 Traform customers Primary Email Address ]########################################################

ALTER TABLE Contact DISABLE TRIGGER ALL
ALTER TABLE ContactEmailAddress DISABLE TRIGGER ALL
SET NOCOUNT ON
DECLARE cursor_fix CURSOR FOR 
  SELECT --TOP 3
  c.ID c_ID,
  cea.Contact_ID cea_Contact_ID,
  c.Name c_name,
  c.FirstName c_first_name,
  c.LastName c_last_name,
  c.PrimaryEmail c_Primary_email,
  cea.Email cea_Email, 
  cea.IsPrimary cea_IsPrimary
  FROM Contact c
  JOIN ContactEmailAddress cea on cea.Contact_ID = C.ID
  WHERE C.ID = cea.Contact_ID and (c.FirstName is not null or c.LastName is not null) and cea.IsPrimary = 0
 

  DECLARE 
  @CID TID,
  @CEA_CONTACT_ID TID,
  @C_NAME VARCHAR(50),
  @C_FIRST_NAME VARCHAR(50),
  @C_LAST_NAME VARCHAR(50),
  @C_PRIMARYEMAIL VARCHAR(50),
  @CEA_EMAIL VARCHAR(50),
  @CEA_ISPRIMARY INT
  DECLARE @alphabet varchar(26) = 'abcdefghijklmnopqrstuvwxyz'
  DECLARE @NEW_PRIMARYEMAIL varchar(255)
  DECLARE @Cursor_Counter int
  DECLARE @lastName VARCHAR(30)

OPEN cursor_fix

FETCH NEXT FROM cursor_fix --Start the cursor  
INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
  
WHILE @@FETCH_STATUS = 0 
  BEGIN 
	  SET @lastName = 
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) 

      set @NEW_PRIMARYEMAIL = @lastName + '@nostromorp.com'
	  PRINT 'Primary Email Update: ' + @C_PRIMARYEMAIL + ' : '+ @NEW_PRIMARYEMAIL + ' - ' + cast(@CID as varchar) 
	  UPDATE ContactEmailAddress set Email = @NEW_PRIMARYEMAIL where Contact_ID = @CID and IsPrimary = 1
	  UPDATE Contact set PrimaryEmail = @NEW_PRIMARYEMAIL, name = @lastName + ', ' + @C_FIRST_NAME, LastName = @lastName where ID = @CEA_CONTACT_ID 
      FETCH NEXT FROM cursor_fix 
	  INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
      SET @Cursor_Counter = @Cursor_Counter + 1 
  END
  CLOSE cursor_fix --close and deallocate    
  DEALLOCATE cursor_fix 
ALTER TABLE ContactEmailAddress ENABLE TRIGGER ALL
ALTER TABLE Contact ENABLE TRIGGER ALL

-- ####################################[ Pass #2 Traform customers NON-Primary Email Address ]########################################################
ALTER TABLE Contact DISABLE TRIGGER ALL
ALTER TABLE ContactEmailAddress DISABLE TRIGGER ALL
SET NOCOUNT ON
DECLARE cursor_fix CURSOR FOR 
  SELECT --TOP 3
  c.ID c_ID,
  cea.Contact_ID cea_Contact_ID,
  c.Name c_name,
  c.FirstName c_first_name,
  c.LastName c_last_name,
  c.PrimaryEmail c_Primary_email,
  cea.Email cea_Email, 
  cea.IsPrimary cea_IsPrimary
  FROM Contact c
  JOIN ContactEmailAddress cea on cea.Contact_ID = C.ID
  WHERE C.ID = cea.Contact_ID and (c.FirstName is not null or c.LastName is not null) and cea.IsPrimary = 0
 

OPEN cursor_fix

FETCH NEXT FROM cursor_fix --Start the cursor  
INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
  
WHILE @@FETCH_STATUS = 0 
  BEGIN 
	  SET @lastName = 
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) 

      set @NEW_PRIMARYEMAIL = @lastName + '@nostromorp.com'
      PRINT 'Other Email: ' + ' other-'+ @NEW_PRIMARYEMAIL + ' - ' + cast(@CID as varchar) 
      UPDATE ContactEmailAddress set Email = 'other-' + @NEW_PRIMARYEMAIL where Contact_ID = @CID and IsPrimary = 0
	  FETCH NEXT FROM cursor_fix 
	  INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
      SET @Cursor_Counter = @Cursor_Counter + 1 
  END
  CLOSE cursor_fix --close and deallocate    
  DEALLOCATE cursor_fix 
ALTER TABLE ContactEmailAddress ENABLE TRIGGER ALL
ALTER TABLE Contact ENABLE TRIGGER ALL

-- ####################################[ Pass #3 Traform All Other contacts Email Address ]########################################################
ALTER TABLE Contact DISABLE TRIGGER ALL
ALTER TABLE ContactEmailAddress DISABLE TRIGGER ALL
SET NOCOUNT ON
DECLARE cursor_fix CURSOR FOR 
  SELECT --TOP 3
  c.ID c_ID,
  cea.Contact_ID cea_Contact_ID,
  c.Name c_name,
  c.FirstName c_first_name,
  c.LastName c_last_name,
  c.PrimaryEmail c_Primary_email,
  cea.Email cea_Email, 
  cea.IsPrimary cea_IsPrimary
  FROM Contact c
  JOIN ContactEmailAddress cea on cea.Contact_ID = C.ID
  WHERE C.ID = cea.Contact_ID and (c.FirstName is null or c.LastName is null)
 

OPEN cursor_fix

FETCH NEXT FROM cursor_fix --Start the cursor  
INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
  
WHILE @@FETCH_STATUS = 0 
  BEGIN 
	  SET @lastName = 
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
		substring(@alphabet, convert(int, rand()*26), 1) +
        substring(@alphabet, convert(int, rand()*26), 1) 

      set @NEW_PRIMARYEMAIL = @lastName + '@nostromorp.com'
	  PRINT 'ContactEmailAddress Other Contacts Email: ' + @C_PRIMARYEMAIL + ' : '+ @NEW_PRIMARYEMAIL + ' - ' + cast(@CID as varchar) 
      UPDATE ContactEmailAddress set Email = @NEW_PRIMARYEMAIL where Contact_ID = @CID 
	  IF @CEA_ISPRIMARY = 1  
		Begin
			PRINT '    ==>  Contact Primary Email: ' + @C_PRIMARYEMAIL + ' : '+ @NEW_PRIMARYEMAIL + ' - ' + cast(@CID as varchar) 
			UPDATE Contact set PrimaryEmail = @NEW_PRIMARYEMAIL where ID = @CEA_CONTACT_ID
		End
      FETCH NEXT FROM cursor_fix 
	  INTO @CID, @CEA_CONTACT_ID, @C_NAME, @C_FIRST_NAME, @C_LAST_NAME, @C_PRIMARYEMAIL, @CEA_EMAIL, @CEA_ISPRIMARY
      SET @Cursor_Counter = @Cursor_Counter + 1 
  END
  CLOSE cursor_fix --close and deallocate    
  DEALLOCATE cursor_fix 
ALTER TABLE ContactEmailAddress ENABLE TRIGGER ALL
ALTER TABLE Contact ENABLE TRIGGER ALL

-- ####################################[ Pass #4 clean up any email not trasformed ]########################################################
ALTER TABLE ContactEmailAddress DISABLE TRIGGER ALL
delete from ContactEmailAddress where Email not like '%nostromorp.com%'
ALTER TABLE ContactEmailAddress ENABLE TRIGGER ALL

--select * from ContactEmailAddress where email not like '%nostromorp.com%'