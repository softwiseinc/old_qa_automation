-- ##################################[ Pass #1 Trasform all Primary phone numbers ]###############################
ALTER TABLE Contact DISABLE TRIGGER ALL
ALTER TABLE ContactPhone DISABLE TRIGGER ALL
SET NOCOUNT ON
DECLARE cursor_fix CURSOR FOR 
  SELECT --TOP 3
  c.ID c_ID,
  cp.Contact_ID cp_Contact_ID,
  c.PrimaryPhone c_PrimaryPhone, 
  cp.Phone cp_Phone, 
  cp.IsPrimary cp_IsPrimary
  FROM Contact c
  JOIN ContactPhone cp on cp.Contact_ID = C.ID
  where cp.IsPrimary = 1

  DECLARE 
  @CID TID,
  @CP_CONTACT_ID TID,
  @C_PRIMARYPHONE VARCHAR(13),
  @CP_PHONE VARCHAR(13),
  @CP_ISPRIMARY INT,
  @Cursor_Counter INT = 1 

OPEN cursor_fix

FETCH NEXT FROM cursor_fix --Start the cursor  
INTO @CID, @CP_CONTACT_ID, @C_PRIMARYPHONE, @CP_PHONE, @CP_ISPRIMARY
WHILE @@FETCH_STATUS = 0 
  BEGIN 
      PRINT  CAST (@Cursor_Counter AS VARCHAR) + ' IDs: ' + @CID + ' : '+ @CP_CONTACT_ID + ' -- ' + 'PHONEs' + @C_PRIMARYPHONE + ' : '+ @CP_PHONE + ' -- IsPrimary: ' + CAST (@CP_ISPRIMARY AS VARCHAR)
      UPDATE ContactPhone set Phone = '(801)893-2295' where Contact_ID = @CID 
	  update Contact set PrimaryPhone = '(801)893-2295' where ID = @CP_CONTACT_ID
      FETCH NEXT FROM cursor_fix 
        INTO @CID, @CP_CONTACT_ID, @C_PRIMARYPHONE, @CP_PHONE, @CP_ISPRIMARY
        SET @Cursor_Counter = @Cursor_Counter + 1 
  END
  CLOSE cursor_fix --close and deallocate    
  DEALLOCATE cursor_fix 
ALTER TABLE ContactPhone ENABLE TRIGGER ALL
ALTER TABLE Contact ENABLE TRIGGER ALL

-- ##################################[ Pass #2 Trasform all Non-Primary phone numbers ]###############################
ALTER TABLE Contact DISABLE TRIGGER ALL
ALTER TABLE ContactPhone DISABLE TRIGGER ALL
SET NOCOUNT ON
DECLARE cursor_fix CURSOR FOR 
  SELECT --TOP 3
  c.ID c_ID,
  cp.Contact_ID cp_Contact_ID,
  c.PrimaryPhone c_PrimaryPhone, 
  cp.Phone cp_Phone, 
  cp.IsPrimary cp_IsPrimary
  FROM Contact c
  JOIN ContactPhone cp on cp.Contact_ID = C.ID
  where cp.IsPrimary = 0


OPEN cursor_fix

FETCH NEXT FROM cursor_fix --Start the cursor  
INTO @CID, @CP_CONTACT_ID, @C_PRIMARYPHONE, @CP_PHONE, @CP_ISPRIMARY
WHILE @@FETCH_STATUS = 0 
  BEGIN 
      PRINT  CAST (@Cursor_Counter AS VARCHAR) + ' IDs: ' + @CID + ' : '+ @CP_CONTACT_ID + ' -- ' + 'PHONEs' + @C_PRIMARYPHONE + ' : '+ @CP_PHONE + ' -- IsPrimary: ' + CAST (@CP_ISPRIMARY AS VARCHAR)
      UPDATE ContactPhone set Phone = '(801)893-2295' where Contact_ID = @CID 
      FETCH NEXT FROM cursor_fix 
        INTO @CID, @CP_CONTACT_ID, @C_PRIMARYPHONE, @CP_PHONE, @CP_ISPRIMARY
        SET @Cursor_Counter = @Cursor_Counter + 1 
  END
  CLOSE cursor_fix --close and deallocate    
  DEALLOCATE cursor_fix 
ALTER TABLE ContactPhone ENABLE TRIGGER ALL
ALTER TABLE Contact ENABLE TRIGGER ALL

-- ##################################[ Pass #3 Delete any remaining phone numbers ]###############################
ALTER TABLE ContactPhone DISABLE TRIGGER ALL
delete from ContactPhone where Phone not like '%(801)893-2295%'
ALTER TABLE ContactPhone ENABLE TRIGGER ALL
